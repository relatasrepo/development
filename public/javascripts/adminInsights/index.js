var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

relatasApp.service('share', function () {
    return {}
});

relatasApp.controller("header_controller", function($scope){

});

relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {
    getLiuProfile($scope, $http, share,$rootScope,function (response) {

        share.initGetInsights({
            emailId:"all",
            fullName:"All team members",
            noPicFlag:"All",
            nameNoImg:"All"
        })
    });
    getErrorMessages($rootScope,$http,"opportunity");
});

relatasApp.controller("insights", function($scope,$http,share,$rootScope){

    getTeam($scope,$http,share);
    closeAllDropDownsAndModals($scope,".list");

    $http.get("/admin/get/all/companies")
        .success(function (response) {
            $scope.names = _.map(response,function (el) {
                if(el.companyName && el.companyName !== "*"){
                    return el.companyName
                }
            });

            $scope.names = _.compact($scope.names);

            $scope.selectedCompany = $scope.names[0];
        });

    $scope.dateRanges = [{
        name:"7 days",
        value:7,
        selected:"selected"
    },{
        name:"14 days",
        value:14,
        selected:""
    },{
        name:"30 days",
        value:30,
        selected:""
    },{
        name:"180 days",
        value:180,
        selected:""
    }]

    $scope.defaultRange = moment().subtract(1,"week");

    share.initGetInsights = function(defaultUser){
        $scope.defaultUser = defaultUser;
        getInsights($scope,$http,share,$rootScope,$scope.defaultRange,defaultUser.emailId);
    }

    $scope.getInsightsFor = function (user) {

        if(user === "all"){
            user = {
                emailId:"all",
                fullName:"All team members",
                noPicFlag:"All",
                nameNoImg:"All"
            }
        }

        $scope.defaultUser = user;
        $scope.selection = user;
        getInsights($scope,$http,share,$rootScope,$scope.defaultRange,user.emailId)
    }

    $scope.getCompany = function(company){
        if(company){
            getTeam($scope,$http,share,company,function (user) {
                $scope.defaultUser = user;
                getInsights($scope,$http,share,$rootScope,$scope.defaultRange,user.emailId,user.companyId)
            });
        }
    }

    $scope.getForRange = function (range) {
        resetAllDtRangeSelection($scope);
        range.selected = "selected";
        $scope.defaultRange = moment().subtract(range.value,"day");
        getInsights($scope,$http,share,$rootScope,$scope.defaultRange,$scope.defaultUser.emailId)
    }

});

function getInsights($scope,$http,share,$rootScope,startDate,emailId,companyId) {
    var url = "/insights/admin/report";

    if(startDate){
        url = fetchUrlWithParameter(url, "startDate", moment(startDate).toString());
    }
    if(emailId){
        url = fetchUrlWithParameter(url, "emailId", emailId);
    }

    if(companyId){
        url = fetchUrlWithParameter(url, "companyId", companyId);
    }

    $http.get(url)
        .success(function (response) {

            share.primaryCurrency = "USD";
            $rootScope.primaryCurrency = "USD";
            share.currenciesObj = {};

            if(response.companyDetails.currency){
                response.companyDetails.currency.forEach(function (el) {
                    share.currenciesObj[el.symbol] = el;
                    if(el.isPrimary){
                        share.primaryCurrency = el.symbol;
                    }
                });
            }

            $rootScope.primaryCurrency = share.primaryCurrency;

            response.Data.sort(function (o1, o2) {
                return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
            });

            $scope.shortfall = 0;
            $scope.totalDealValueAtRisk = 0;
            $scope.dealsAtRiskValue = 0;
            $scope.dealsAtRiskCount = 0;
            var dealsAtRiskImprovement = 0;
            var ltImprovement = 0;
            var interactionsImprovement = 0;

            if(response && response.Data && response.Data[0] && response.Data[response.Data.length-1].pipelineVelocity) {
                $scope.shortfall = response.Data[response.Data.length-1].pipelineVelocity.shortfall;
            }

            if(response && response.Data && response.Data[0]) {

                if(response.Data[response.Data.length-2] && !response.Data[response.Data.length-2].dealsAtRiskImpact){
                    response.Data[response.Data.length-2].dealsAtRiskImpact = 0;
                }

                if(response.Data[response.Data.length-1] && !response.Data[response.Data.length-1].dealsAtRiskImpact){
                    response.Data[response.Data.length-1].dealsAtRiskImpact = 0;
                }

                $scope.dealsAtRiskValue = getAmountInThousands(Math.abs(response.Data[response.Data.length-1].dealsAtRiskImpact),2,share.primaryCurrency == "INR");

                var endImp = 0;
                var startImp = 0;

                if(response.Data[response.Data.length-1] && response.Data[response.Data.length-1].dealsAtRiskImpact){
                    endImp = response.Data[response.Data.length-1].dealsAtRiskImpact
                }
                if(response.Data[0] && response.Data[0].dealsAtRiskImpact){
                    startImp = response.Data[0].dealsAtRiskImpact
                }

                $scope.totalDealValueAtRisk = endImp - startImp;
            }

            if(response && response.Data && response.Data[0] && response.Data[response.Data.length-1].dealsAtRisk) {
                $scope.dealsAtRiskCount = response.Data[response.Data.length-1].dealsAtRisk;
            }

            if($scope.totalDealValueAtRisk<0){
                $scope.totalDealValueAtRisk = getAmountInThousands(Math.abs($scope.totalDealValueAtRisk),2,share.primaryCurrency == "INR");
                $scope.totalDealValueAtRisk = "-"+$scope.totalDealValueAtRisk
            } else {
                $scope.totalDealValueAtRisk = getAmountInThousands($scope.totalDealValueAtRisk,2,share.primaryCurrency == "INR");
            }

            $scope.shortfall = Math.abs($scope.shortfall);
            $scope.shortfall = getAmountInThousands($scope.shortfall,2,share.primaryCurrency == "INR");

            var labels = getLabels(response.Data);
            var lt_series = [],
                lt_series_impact = [],
                dr_series = [],
                dr_at_series = [],
                opre_series = [],
                int_all_series = [],
                int_opp_series = [],
                totalActiveUsers = [],
                mobActiveUsers = [],
                uniqueMobLogins = [],
                uniqueWebLogins = [],
                totalMobLogins = [],
                totalWebLogins = [],
                totalRegUsers = [],
                pv_series = [];

            _.each(response.Data,function (el) {
                lt_series.push(el.losingTouch);
                lt_series_impact.push(el.losingTouchImpact);
                dr_series.push(el.dealsAtRisk);
                if(el.dealsAtRiskAction){
                    dr_at_series.push(el.dealsAtRiskAction.length);
                } else {
                    dr_at_series.push(0);
                }

                opre_series.push(el.oppREConversion);
                if(el.pipelineVelocity) {
                    pv_series.push(el.pipelineVelocity.shortfall?Math.abs(el.pipelineVelocity.shortfall):0);
                }

                if(el.allInteractions){
                    int_all_series.push(el.allInteractions)
                } else {
                    int_all_series.push(0)
                }

                if(el.allInteractions){
                    int_opp_series.push(el.oppInteractions)
                } else {
                    int_opp_series.push(0)
                }

                if(el.totalActiveUsers){
                    totalActiveUsers.push(el.totalActiveUsers)
                } else {
                    totalActiveUsers.push(0)
                }

                if(el.totalRegUsers){
                    totalRegUsers.push(el.totalRegUsers)
                } else {
                    totalRegUsers.push(0)
                }

                if(el.totalWebLogins){
                    totalWebLogins.push(el.totalWebLogins)
                } else {
                    totalWebLogins.push(0)
                }

                if(el.totalMobLogins){
                    totalMobLogins.push(el.totalMobLogins)
                } else {
                    totalMobLogins.push(0)
                }
                if(el.uniqueMobLogins){
                    uniqueMobLogins.push(el.uniqueMobLogins)
                } else {
                    uniqueMobLogins.push(0)
                }

                if(el.uniqueWebLogins){
                    uniqueWebLogins.push(el.uniqueWebLogins)
                } else {
                    uniqueWebLogins.push(0)
                }
                if(el.mobActiveUsers){
                    mobActiveUsers.push(el.mobActiveUsers)
                } else {
                    mobActiveUsers.push(0)
                }
            });

            var dealsAtRisk = {
                labels: labels,
                series: [{data: dr_series}]
            };

            var ltSeries = {
                labels: labels,
                series: [{data: lt_series_impact},{data: lt_series}]
            };

            var interactions = {
                labels: labels,
                series: [{data: int_opp_series},{data: int_all_series}]
            };

            dealsAtRisk.series.push({data:dr_at_series});

            var pipelineVelocity = {
                labels: labels,
                series: [{data: pv_series}]
            };

            var oppRec = {
                labels: labels,
                series: [{data: opre_series}]
            };

            var webReports = {
                labels: labels,
                series: [
                    {className:"totalActiveUsers",data: totalActiveUsers},
                    {className:"uniqueWebLogins",data: uniqueWebLogins},
                    {className:"totalWebLogins",data: totalWebLogins}]
            };

            var mobileReports = {
                labels: labels,
                series: [
                    {className:"totalActiveUsers",data: mobActiveUsers},
                    {className:"uniqueWebLogins",data: uniqueMobLogins}, //Using same class as weblogins for CSS
                    {className:"totalMobLogins",data: totalMobLogins}]
            };

            $scope.interactionsCount = int_all_series[int_all_series.length-1];

            drawLineChart("#losingTouch",ltSeries,lt_series[0],lt_series[lt_series.length-1],$scope,share);
            drawLineChart("#dealsAtRisk",dealsAtRisk,dr_series[0],dr_series[dr_series.length-1],$scope,share);
            // drawLineChart("#oppRec",oppRec,opre_series[0],opre_series[opre_series.length-1],$scope,share);
            drawLineChart("#pipelineVelocity",pipelineVelocity,pv_series[0],pv_series[pv_series.length-1],$scope,share);
            drawLineChart("#interactions",interactions,0,0,$scope,share);
            drawLineChart("#webReports",webReports,0,0,$scope,share);
            drawLineChart("#mobileReports",mobileReports,0,0,$scope,share);
            drawTable($scope,$http);

            var toInt = int_all_series[int_all_series.length-1];
            var fromInt = int_all_series[0];
            interactionsImprovement = calculatePercentageChange(toInt,fromInt);
            var webImprovement = calculatePercentageChange(totalWebLogins[totalWebLogins.length-1],totalWebLogins[0]),
            mobImprovement= calculatePercentageChange(totalMobLogins[totalMobLogins.length-1],totalMobLogins[0]);

            var to = lt_series[lt_series.length-1];
            var from = lt_series[0];

            var toImpact = lt_series_impact[0];
            var fromImpact = lt_series_impact[lt_series_impact.length-1];

            $scope.ltImpact = fromImpact - toImpact;
            $scope.ltImpact = getAmountInThousands(Math.abs($scope.ltImpact.toFixed(2)),2,share.primaryCurrency == "INR")
            ltImprovement = calculatePercentageChange(to,from);

            dealsAtRiskImprovement = calculatePercentageChange(dr_series[dr_series.length-1]?dr_series[dr_series.length-1]:0,dr_series[0]?dr_series[0]:0);

            if(isNaN(dealsAtRiskImprovement)){
                dealsAtRiskImprovement = 0;
            }

            if(isNaN(ltImprovement)){
                ltImprovement = 0;
            }

            if(isNaN(interactionsImprovement)){
                interactionsImprovement = 0;
            }
            if(isNaN(webImprovement)){
                webImprovement = 0;
            }

            if(isNaN(mobImprovement)){
                mobImprovement = 0;
            }

            if(Math.abs(dealsAtRiskImprovement) === Infinity){
                dealsAtRiskImprovement = 100;
            }

            if(Math.abs(ltImprovement) === Infinity){
                ltImprovement = 100;
            }

            if(Math.abs(interactionsImprovement) === Infinity){
                interactionsImprovement = 100;
            }

            if(Math.abs(webImprovement) === Infinity){
                webImprovement = 100;
            }
            if(Math.abs(mobImprovement) === Infinity){
                mobImprovement = 100;
            }

            $scope.webImprovement = webImprovement+"%";
            $scope.mobImprovement = mobImprovement+"%";

            $scope.dealsAtRiskImprovement = dealsAtRiskImprovement+"%";
            $scope.ltImprovement = ltImprovement+"%";
            $scope.interactionsImprovement = interactionsImprovement+"%";

            if(fromInt>toInt){
                $scope.interactionsImprovement = "-"+interactionsImprovement+"%";
            }

        });
}

function getLabels(data){

    var dtFormat = "D MMM YY";

    var dates_label = _.map(data,function (el) {
        return moment(el.date).format(dtFormat);
    });

    return dates_label;
}

function resetAllDtRangeSelection($scope) {
    _.each($scope.dateRanges,function (dt) {
        dt.selected = ""
    })
}

function drawTable($scope,$http) {
    $scope.tableRows = [];

    $scope.tableRows.push({
        name: "Deals At Risk",
        start: 1,
        end: 10,
        percentageChange: "90%"
    })

    $scope.tableRows.push({
        name: "Opp Recommendations Conversion",
        start: 35,
        end: 10,
        percentageChange: "71.43%"
    })

    $scope.tableRows.push({
        name: "Losing Touch",
        start: 99,
        end: 58,
        percentageChange: "41.41%"
    })

    $scope.tableRows.push({
        name: "Pipeline Velocity",
        start: 1,
        end: 10,
        percentageChange: "9%"
    })
}

function drawLineChart(id,data,start,end,$scope,share) {

    var colorSelectorLine = ".admin-insights "+id+" .ct-line";
    var colorSelectorPoint = ".admin-insights "+id+" .ct-point";
    var strokeColor = "rgb(47, 183, 47)"

    $scope[id.replace(/[^a-zA-Z ]/g,'')+"Impact"] = start-end;
    $scope[id.replace(/[^a-zA-Z ]/g,'')+"Today"] = end;

    var options = {
        width: '325px',
        axisX: {
            showGrid: false,
            labelInterpolationFnc: function(value) {
                return value;
            }
        },
        axisY: {
            labelInterpolationFnc: function(value) {
                return getAmountInThousands(value,2,share.primaryCurrency == "INR");
            }
        },
        plugins: [
            Chartist.plugins.tooltip({
                transformTooltipTextFnc:function (value) {
                    if(value){
                        return getAmountInThousands(parseFloat(value),2,share.primaryCurrency == "INR")
                    }
                }
            })
        ]
    };

    if(id === "#dealsAtRisk"){
        colorSelectorLine = ".admin-insights "+id+" .ct-series-a .ct-line";
        colorSelectorPoint = ".admin-insights "+id+" .ct-series-a .ct-point";

        if(end>start){
            $scope.dealsCaret = "down";
            strokeColor = "rgb(215, 2, 6)";
        } else {
            $scope.dealsCaret = "up";
        }
    }

    if(id === "#losingTouch"){
        if(end>start){
            $scope.ltCaret = "down";
            strokeColor = "rgb(215, 2, 6)";
        } else {
            $scope.ltCaret = "up";
        }
    }

    if(id === "#oppRec"){
        if(end<start){
            strokeColor = "rgb(215, 2, 6)";
        }
    }

    if(id === "#pipelineVelocity"){
        if(start<end){
            strokeColor = "rgb(215, 2, 6)";
            $scope.pvCaret = "down"
        } else {
            $scope.pvCaret = "up"
        }
    }

    /* Now we can specify multiple responsive settings that will override the base settings based on order and if the media queries match. In this example we are changing the visibility of dots and lines as well as use different label interpolations for space reasons. */
    var responsiveOptions = [
        ['screen and (min-width: 641px) and (max-width: 1024px)', {
            showPoint: false,
            axisX: {
                labelInterpolationFnc: function(value) {
                    return 'Week ' + value;
                }
            }
        }],
        ['screen and (max-width: 640px)', {
            showLine: false,
            axisX: {
                labelInterpolationFnc: function(value) {
                    return 'W' + value;
                }
            }
        }]
    ];

    new Chartist.Line(id, data, options, responsiveOptions);

    setTimeOutCallback(10,function () {
        $(colorSelectorLine).css({stroke: strokeColor});
        $(colorSelectorPoint).css({stroke: strokeColor});
    });
}

function getTeam($scope,$http,share,companyName,callback){

    var url = '/admin/company/users';
    if(companyName){
        url = url+"?companyName="+companyName
    }

    $http.get(url)
        .success(function (response) {
            $scope.team = [];
            $scope.selection = {
                emailId:"all",
                fullName:"All team members",
                noPicFlag:"All",
                nameNoImg:"All",
                companyId:response.companyId
            };

            if(response && response.SuccessCode && response.Data && response.Data.length>0){
                $scope.team = buildTeamProfiles(response.Data,response.companyId);
                // $scope.selection = $scope.team[0];
                share.selectedUserFromList = $scope.selection;
            }

            if(callback){
                callback($scope.selection)
            }
        });
}

function buildTeamProfiles(data,companyId) {
    var team = [];

    _.each(data,function (el) {

        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            imageUrl:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            lastName:el.lastName,
            firstName:el.firstName,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase(),
            companyId:companyId
        })
    });

    return team;
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showVerticalList = false;
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                $scope.showSourceList = false;
                $scope.fy = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}

