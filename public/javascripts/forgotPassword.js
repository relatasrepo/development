
$(document).ready(function(){
var forgotFlag = false;
    $("#submit").on("click",function(){
            var emailId = $("#emailId").val();
                emailId = emailId.trim();
        var data = {
            emailId:emailId
        }
        if(emailId != '' && validateEmailField(emailId)){

            $.ajax({
                url:'/sendPasswordResetToken',
                type:'POST',
                datatype:'JSON',
                data:data,
                success:function(result){
                    if(result){
                        forgotFlag = true;
                        showMessagePopUp("An email has been sent to "+emailId+". Please check your mail to reset your password.",'success')
                    }
                    else{
                        showMessagePopUp("Either your Relatas account was created through LinkedIn authentication. Please try to login through LinkedIn on relatas.com <br> OR this email id doesn't exist in the database.",'error',true);
                    }
                }
            })
        }
        else{
            showMessagePopUp("Please enter valid email id",'error');
        }
    });

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    function showMessagePopUp(message,msgClass,isHtml)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'9%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        //setTimeout(function(){
        if(isHtml){
            $("#message").html(message)
        }else
        $("#message").text(message)
        //},1000);
        $("#message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
        if(forgotFlag){
            forgotFlag = false;
            window.location.replace('/');
        }
    });

});
