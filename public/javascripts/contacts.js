
$(document).ready(function() {

    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var selectedContactsArray = [];
    var eArr = [];
    var interactionUserArr = [];

    var totalContactsLength = 0;
    var AWScredentials,massMailCount,s3bucket,imageBucket,userProfile,docName,docUrl,docId,imageUrl,imageName,table;

    var headerImage = false,contactsSelectFlag = false,contactsSelectFlag2 = true;

    var emailStrArr = $("#emails_check").attr('class');
    if(checkRequired(emailStrArr))
         eArr = removeNull(JSON.parse(emailStrArr));

    var interactedEmailLength = eArr.length;
    var selectedContactsLength = 0;

    getAWSCredentials();
    getUserProfile();
    validateMMC();

    $("#userMessage").jqte();
    function getAWSCredentials(){
        $.ajax({
            url:'/getAwsCredentials',
            type:'GET',
            datatye:'JSON',
            success:function(credentials){
                AWScredentials = credentials;
                AWS.config.update(credentials);

                //AWS.config.region = "us-east-1";
                AWS.config.region = credentials.region;

                s3bucket = new AWS.S3({ params: {Bucket:credentials.bucket} });
                imageBucket = new AWS.S3({ params: {Bucket:credentials.imageBucket} });
                docUrl = 'https://'+AWScredentials.bucket+'.s3.amazonaws.com/';
                imageUrl = 'https://'+AWScredentials.imageBucket+'.s3.amazonaws.com/';

            }
        })
    }

    function validateMMC(){

        $.ajax({
            url:'/validateMMC',
            type:'GET',
            datatye:'JSON',
            success:function(response){

                getMassMailCount()
            }
        })
    }

    function getMassMailCount(){
        $.ajax({
            url:'/getMassMailCount',
            type:'GET',
            datatye:'JSON',
            success:function(accountDetails){

                if(accountDetails == 'error'){
                    massMailCount =0;
                }
                else{
                    massMailCount = accountDetails.count;

                }

                getTotalContacts();
            }
        })
    }

    $("#upload-button-img").on("click",function(){
        $("#selectContactsFile").trigger("click")
    })
    $("#uploadInput").on("click",function(){
        $("#selectContactsFile").trigger("click")
    })

    $("#selectContactsFile").on("change",function(){
        $("#uploadInput").val($("#selectContactsFile")[0].files[0].name || '')
    })
    $("#uploadButton").on("click",function(){
        if($("#selectContactsFile")[0].files[0]){
            $("#changeProfileImgForm").submit()
        }else showMessagePopUp("Please select file to upload",'error')
    })

    $("#filterTable").on("click",function(){
        var textToFilter = $("#searchInput").val().trim()
        if(checkRequired(table)){
            if(textToFilter == ''){
                table.draw()
            }else
            table.search(textToFilter).draw();
        }
    })
    $("#searchInput").change(function(){
        var textToFilter = $("#searchInput").val().trim()
        if(checkRequired(table)){
            table.search(textToFilter).draw();
        }
    })
var cFlag = false;
    $("#saveContact").on("click",function(){
        var contactEmailId = $("#contactEmailId").val()
        var contactFirstName = $("#contactFirstName").val()
        var contactLastName = $("#contactLastName").val()

        if(checkRequired(contactEmailId.trim()) && checkRequired(contactFirstName.trim()) && checkRequired(contactLastName.trim())){
            if(validateEmailField(contactEmailId)){
                var contactDetails = {
                       emailId:contactEmailId.trim(),
                       firstName:contactFirstName,
                       lastName:contactLastName
                }
                $.ajax({
                    url:'/createContact',
                    type:'POST',
                    datatype:'JSON',
                    data:contactDetails,
                    success:function(response){
                        if(response == 'exist'){
                            showMessagePopUp("Contact already exists. Please use the search feature on top.",'error')
                        }else
                        if(response){
                            $("#contactEmailId").val('')
                            $("#contactFirstName").val('')
                            $("#contactLastName").val('')
                            cFlag = true;
                            showMessagePopUp("Contact added successfully.",'success')

                        }
                        else{
                            showMessagePopUp("Error occurred. Please try again",'error')
                        }
                    }
                })
            }
            else{
                showMessagePopUp("Please enter valid Email id.",'error')
            }
        }
        else{
            showMessagePopUp("Please enter contact details.",'error')
        }
    })

    function validateEmailField(email)
    {
        var emailText = email;
        var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
        if (pattern.test(emailText))
        {

            return true;
        }
        else
        {

            return false;
        }
    }
    getUploadContactsStatus()
function getUploadContactsStatus(){
    $.ajax({
        url: '/uploadContactStatusMsg',
        type: 'GET',
        datatype: 'JSON',
        traditional: true,
        success: function (status) {
            clearUploadContactsStatus()
            if(checkRequired(status)){
                if(status == true){

                }
                else{
                    if(checkRequired(status.msg)){
                        showMessagePopUp(status.msg,status.status,true);
                        clearUploadContactsStatus()
                    }
                }
            }

        },
        error: function (event, request, settings) {


        },
        timeout: 20000
    })
}
    function clearUploadContactsStatus(){
        $.ajax({
            url: '/clearUploadContactStatusMsg',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (status) {


            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(result){
                    identifyMixPanelUser(result);
                }
                if(result.emailId == 'sudip@relatas.com' || result.emailId == 'forsudip@gmail.com'){
                    contactsSelectFlag = true;
                }

                $('#profilePic').attr("src", result.profilePicUrl);
                imagesLoaded("#profilePic",function(instance,img) {
                    if(instance.hasAnyBroken){
                        $('#profilePic').attr("src","/images/default.png");
                    }
                });

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }

    function getTotalContacts() {
        $.ajax({
            url: '/contacts/filter/total',
            type: 'GET',
            datatype: 'JSON',
            async:false,
            traditional: true,
            success: function (contacts) {
                interactionUserArr = [];
                if (checkRequired(contacts)) {
                    applyDataTable();
                    totalContactsLength = 0;

                    if(contacts.nonRelatas && contacts.nonRelatas.length){
                        totalContactsLength += contacts.nonRelatas.length
                    }
                    if(contacts.relatas && contacts.relatas.length){
                        totalContactsLength += contacts.relatas.length;
                    }
                    if(contacts.linkedin && contacts.linkedin.length){
                        totalContactsLength += contacts.linkedin.length;
                    }
                    if(contacts.total){
                        if(contacts.total < totalContactsLength){
                            var diff = totalContactsLength-contacts.total
                            if(contacts.nonRelatas && contacts.nonRelatas.length){

                                if(contacts.nonRelatas.length >= diff){
                                    contacts.nonRelatas.splice(0,diff);
                                }
                            }
                            else if(contacts.linkedin && contacts.linkedin.length){

                                if(contacts.linkedin.length >= diff){
                                    contacts.linkedin.splice(0,diff);
                                }
                            }
                            else if(contacts.relatas && contacts.relatas.length){
                                if(contacts.relatas.length >= diff){
                                    contacts.relatas.splice(0,diff);
                                }
                            }
                        }
                    }

                    if(contacts.nonRelatas && contacts.nonRelatas.length){
                        totalContactsLength += contacts.nonRelatas.length
                        displayContacts(contacts.nonRelatas);
                    }
                    if(contacts.relatas && contacts.relatas.length){
                        totalContactsLength += contacts.relatas.length;
                        getContactDetails(contacts.relatas);
                    }
                    if(contacts.linkedin && contacts.linkedin.length){
                        totalContactsLength += contacts.linkedin.length;
                        displayLinkedinContacts(contacts.linkedin);
                    }
                }else applyDataTable();
            },
            error: function (event, request, settings) {
                applyDataTable();
            },
            timeout: 20000
        })
    }

    function displayContacts(contacts) {
        var rowArr = [];
        var length = contacts.length - 1;
        if (checkRequired(contacts[0])) {
          contacts.forEach(function(obj,index){
              (function(contact,i){
                  setTimeout(function(){
                      rowArr.push(displayOtherContactsNext(contact,i));
                      if(i == length){
                          addRowsToTable(rowArr)
                      }
                  },0)
              }) (obj,index)
          })
        }
    }

    function getContactDetails(relatasUserContacts){
         var rowArr = [];
         var length = relatasUserContacts.length - 1;
        _.each(relatasUserContacts,function(obj,index){
            (function(o,i){
                rowArr.push(getContactDetailsAjax(o,i))
                if(i == length){
                    addRowsToTable(rowArr)
                    interactionUserArr.forEach(function(id){
                        interactionsCount(id);
                    })
                }
            })(obj,index)
        });
    }

    function getContactDetailsAjax(relatasUserContact, index) {

        if (checkRequired(relatasUserContact.personId)) {
            var dateAdded = relatasUserContact.addedDate ? getDateFormat(relatasUserContact.addedDate) : '';
            var profileImage = '/getImage/' + relatasUserContact.personId._id;
            var personName = relatasUserContact.personId.firstName + ' ' + relatasUserContact.personId.lastName || '';
            var companyName = relatasUserContact.personId.companyName || '';
            var designation = relatasUserContact.personId.designation || '';
            var location = relatasUserContact.personId.location || '';
            var personEmail = relatasUserContact.personId.emailId || '';
            var skype = relatasUserContact.personId.skypeId || 'Not Available';
            var summaryUrl = '/interactions/summary/' + relatasUserContact.personId._id;
            var msgUrl = '/messages/all/' + relatasUserContact.personId._id;
            var personPhone = relatasUserContact.personId.mobileNumber || 'Not Available';
            var timesInteracted = relatasUserContact.count || 0;
            var lastInteracted = relatasUserContact.lastInteracted ? getDateFormat(relatasUserContact.lastInteracted) : '';
            var userUrl = '/' + getValidUniqueUrl(relatasUserContact.personId.publicProfileUrl)
            interactionUserArr.push(relatasUserContact.personId._id)
            return [
                '<span style="display: none">0</span>',
                '<span><input value="0" conType="relatasUser"  personName=' + personName.replace(/\s/g, '-') + ' emailId=' + personEmail + ' userId=' + relatasUserContact.personId._id + ' id="selectedContact" type="checkbox"></span>',
                '<span><img src="/images/relatas_20x20.png"/></span>',
                '<span><div><a target="_blank" href=' + userUrl + '><img id=' + 'pImg' + relatasUserContact.personId._id + ' src=' + profileImage + ' style="width:30px; height:30px;border:none"></a></div></span>',
                '<span title=' + personName.replace(/\s/g, '&nbsp;') + '><a style="color:#2C3539" target="_blank" href=' + userUrl + '>' + getTextLength(personName, 8) + '</a></span>',
                '<a href=' + msgUrl + '><img style="width: 20px;" title="Click to See messages exchanged" src="/images/interaction/icon_rel_mssg.png"></a>',
                '<span><a style="color:#333333" href=' + summaryUrl + ' id=' + 'interaction' + relatasUserContact.personId._id + '>' + timesInteracted + '</a></span>',
                '<span>' + dateAdded + '</span>',
                '<span>' + lastInteracted + '</span>',
                '<span title=' + designation.replace(/\s/g, '&nbsp;') + '>' + getTextLength(designation, 8) + '</span>',
                '<span title=' + companyName.replace(/\s/g, '&nbsp;') + '>' + getTextLength(companyName, 8) + '</span>',
                '<span title=' + location.replace(/\s/g, '&nbsp;') + '>' + getTextLength(location, 8) + '</span>',
                '<span><button class="btn btn-xs blue-btn"  name=' + personName.replace(/\s/g, '&nbsp;') + ' userId=' + relatasUserContact.personId._id + ' emailId=' + personEmail + ' skypeId=' + skype + ' phone=' + personPhone + ' conType="registered" id="showMoreButton" trId=' + 'showMoreTrRelatasUser' + relatasUserContact.personId._id + '>Connect</button></span>',
                '<span><div class="close" contactId=' + relatasUserContact._id + ' id="deleteContact">&times</div></span>'
            ]

        } else {
            return displayOtherContactsNext(relatasUserContact, index);
        }
    }


    function getValidUniqueUrl(uniqueName){
        var url = window.location+''.split('/');
        var patt = new RegExp(url[2]);
        if(patt.test(uniqueName)){
            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i+1];
                }
            }
            return uniqueName;
        }else {
            return uniqueName;
        }
    }

    function addRowsToTable(rowArr){
        table.rows.add( rowArr ).draw();
    }

    function displayLinkedinContacts(nonRelatasContacts) {
        var rowArr = [];
        var length = nonRelatasContacts.length - 1
        _.each(nonRelatasContacts, function (obj,index) {
            (function (con,i){
                setTimeout(function(){
                    rowArr.push(displayLContacts(con,i))
                    if(i == length){
                        addRowsToTable(rowArr)
                    }
                },0)
            })(obj,index)

        })
    }

    function displayLContacts(nonRelatasContacts,k,callback){


        var personName = nonRelatasContacts.personName || '';
        var personEmail = nonRelatasContacts.personEmailId || '';

        var skype = nonRelatasContacts.skypeId || 'Not Available';

        var personPhone = nonRelatasContacts.mobileNumber || 'Not Available';
        var companyName =  nonRelatasContacts.companyName || '';
        var designation =  nonRelatasContacts.designation || '';
        var timesInteracted = nonRelatasContacts.count || 0;
        var lastInteracted = nonRelatasContacts.lastInteracted ? getDateFormat(nonRelatasContacts.lastInteracted) : '';
        var dateAdded = nonRelatasContacts.addedDate ? getDateFormat(nonRelatasContacts.addedDate) : '';

       return [
            '<span><span style="display: none">2</span></span>',
            '<span><input value="2" conType="linkedin"  personName='+personName.replace(/\s/g, '-')+' emailId='+personEmail+' userId="" id="selectedContact" type="checkbox"></span>',
            '<span><img  src="/images/linkedin_20x20.png"/></span>',
            '<span><div style="width: 5%"><img style="border:none" src="/img/user.png"/></div></span>',
            '<span title='+personName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(personName,8)+ '</span>',
            '',
            '<span>'+timesInteracted+'</span>',
            '<span>'+dateAdded+'</span>',
            '<span>'+lastInteracted+'</span>',
            '<span title='+designation.replace(/\s/g, '&nbsp;')+'>'+getTextLength(designation,8)+'</span>',
            '<span title='+companyName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(companyName,8)+'</span>',
            '<span></span>',
            '<span><button class="btn btn-xs blue-btn"  name='+personName.replace(/\s/g, '&nbsp;')+' emailId='+personEmail+' skypeId='+skype+' phone='+personPhone+' conType="linkedin" id="showMoreButton" trId='+'showMoreTrLinkedin'+k+'>Connect</button></span>',
            '<span><div class="close" contactId='+nonRelatasContacts._id+' id="deleteContact">&times</div></span>'
        ]

      }


    function displayOtherContacts(contact,i){
       displayOtherContactsNext(contact,i)
    }
    function displayOtherContactsNext(contact,i){
        var personName = contact.personName || '';
        var personEmail = contact.personEmailId || 'Not Available';

        var skype = contact.skypeId || 'Not Available';
        var personPhone = checkRequired(contact.mobileNumber) ? contact.mobileNumber : 'Not Available';
        var timesInteracted = contact.count || 0;
        var lastInteracted = contact.lastInteracted ? getDateFormat(contact.lastInteracted) : '';
        var dateAdded = contact.addedDate ? getDateFormat(contact.addedDate) : '';

        return [
            '<span style="display: none">1</span>',
            '<span><input value="1" conType="nonRelatasUser" personName='+personName.replace(/\s/g, '-')+' emailId='+personEmail+' userId="" id="selectedContact" type="checkbox"></span>',
            '<span><img src="/images/relatas_20x20.png"/></span>',
            '<span><div style="width: 5%"><img style="border:none" src="/img/user.png"/></div></span>',
            '<span title='+personName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(personName,8)+ '</span>',
            '',
            '<span>'+timesInteracted+'</span>',
            '<span>'+dateAdded+'</span>',
            '<span>'+lastInteracted+'</span>',
            '<span></span>',
            '<span></span>',
            '<span></span>',
            '<span><button class="btn btn-xs blue-btn" name='+personName.replace(/\s/g, '&nbsp;')+' emailId='+personEmail+' skypeId='+skype+' phone='+personPhone+' conType="nonRegistered" id="showMoreButton" trId='+'showMoreTrNonUser'+i+'>Connect</button></span>',
            '<span><div class="close" contactId='+contact._id+' id="deleteContact">&times</div></span>'
        ]
    }

    function interactionsCount(otherUserId){
        getAllInteractionDetails(otherUserId);
    }

    function getAllInteractionDetails(userId2){
        $.ajax({
            url: '/getInteractions/all/count/'+userId2,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (interactions) {
               if(checkRequired(interactions) && interactions.length > 0){
                   $("#interaction"+userId2).text(interactions[0].count)
               }
            }
        })
    }

   var popoverSource = null;
    var butElement;
    $("body").on("click","#showMoreButton",function(){
        if(popoverSource != null)
        {
            popoverSource.popover('destroy');
            popoverSource = null;
        }
         var trId = $(this).attr("trId")
        var skype = $(this).attr("skypeId")
        var email = $(this).attr("emailId")
        var phoneNumber = $(this).attr("phone")
        var name = $(this).attr("name")
        var userId = $(this).attr("userId")
        var conType = $(this).attr("conType")

        var html = _.template($("#showMore-popup").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverSource =  $(this)

        $(this).popover('show');
        $(".popover").css({width:'100%'})
        $(".popover").css({"margin-left":'-20%'})


          if(email == 'Not'){
              $("#email").text('Not Available')
          }
          else{
              $("#email").text(email)
          }

            var link = 'skype:'+skype+'?call';
            if(skype == 'Not'){
                $("#skype").text('Not Available')
            }
            else
                $("#skype").html('<a href='+link+' target="_blank">'+skype+'</a>')
        if(phoneNumber == 'Not'){
            $("#phone").text('Not Available')
        }
        else
            $("#phone").text(phoneNumber)


        butElement = $(this);
        $("#send-showMore").attr("email",email)

    });

    $("body").on("click","#closePopup-showMore",function(){

        popoverSource.popover('destroy');
    })
    $("body").on("click","#send-showMore",function(){
        var email = $(this).attr("email")

        var checkBox = $("#selectedContact",butElement.closest('tr'));
        var emailId = checkBox.attr('emailId');

        if(!checkBox.prop('checked')){
            checkBox.prop('checked',true);
            selectedContact(checkBox,true);
        }

        popoverSource.popover('destroy');
    });

    var deleteObj;
    var element2;

    $("body").on("click","#deleteContact",function(){
        var contactId = $(this).attr("contactId");
        var row = $(this).closest("tr").get(0);
        var userId = userProfile._id;
        var obj = {
             userId:userId,
             contactId:contactId
        }
        deleteObj = obj;
        var element =  $(this);
        element2 = element;


        if(popoverSource != null)
        {
            popoverSource.popover('destroy');
            popoverSource = null;
            //return;
        }
        var docId = $(this).attr("docId");
        //$(".popover").remove();
        var html = _.template($("#delete-popup").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });

        $(this).popover('show');
        $('.arrow').hide()
        popoverSource = $(this);



    })

    $("body").on("click","#deleteDoc",function(){
        popoverSource.popover('destroy');
      if(checkRequired(deleteObj)){
          if(checkRequired(deleteObj.contactId) && checkRequired(deleteObj.userId)){
              $.ajax({
                  url:'/deleteContact',
                  type:'POST',
                  datatype:'JSON',
                  data:deleteObj,
                  success:function(response){
                      deleteObj = null
                      if(response){
                          showMessagePopUp("Contact Successfully deleted.",'success')
                          if(checkRequired(table)){
                              table.row(element2.parents('tr')).remove().draw();
                          }
                      }
                      else{
                          showMessagePopUp("Deleting contact failed.",'error')
                      }
                  }
              })
          }
      }
    })

    $("body").on("click","#closePopup-delete",function(){
        // $(".popover").remove();
        popoverSource.popover('destroy');
    })

    $("body").on("change","#selectedContact",function(){
        selectedContact($(this),true);
    });
    var shFlag = false;
    var msgFlag = true;
    function selectedContact(element,type){

        if(element.prop('checked')){
            var personName = element.attr('personName')

            var name = checkRequired(personName) ? personName.replace(/-/g,' ') : '';

            var selectedContact = {
                personName:name,
                personEmailId:element.attr('emailId'),
                personId:element.attr('userId'),
                conType:element.attr('conType')
            }
            var mmc = massMailCount;
            var contactCount = selectedContactsArray.length+1
            var mmc2 = mmc+contactCount;

            if(mmc2 <= 99 || contactsSelectFlag){
                addOrRemoveContact(selectedContact,'selected',type,element)
            }
            else{

                if(type)
                   showMessagePopUp("You have crossed your weekly limit of 99 messages. If you need to send more please mail us at hello@relatas.com.",'tip');
                else{
                   if(msgFlag){
                       msgFlag = false;
                       var msg = "Currently Relatas allows you to send 99 mails / week. You may send mail to 99 contacts now and more the coming week.";
                       if(!preSelectMsgFlag){
                            $("#message2").text(msg);
                       }else
                       showMessagePopUp(msg,'tip');
                   }
                }
                element.attr("checked",false);
                contactsSelectFlag2 = false;
            }
        }
        else{

            addOrRemoveContact(element.attr('emailId'),'unSelected',type,element)
        }
    }
    var preSelectMsgFlag = true;
    function addOrRemoveContact(selectedContact,event,type,element){

        if(validateEmailField(selectedContact.personEmailId)){

            if(event == 'selected'){

                selectedContactsArray.push(selectedContact);
                $("#contactsSelected").text('('+selectedContactsArray.length+') Contacts Selected ');
                selectedContactsLength = selectedContactsArray.length;
            }
            else{
                selectedContactsArray = $.grep(selectedContactsArray, function(e){
                    return e.personEmailId != selectedContact;
                });
                $("#contactsSelected").text('('+selectedContactsArray.length+') Contacts Selected ');
                selectedContactsLength = selectedContactsArray.length;
                contactsSelectFlag2 = true;
            }
            if(!shFlag){
                $("#shw").trigger('click');
                shFlag = true;
            }
            var msg = "We have pre-selected the "+selectedContactsArray.length+" contacts that you haven't interacted with in the past 90 days. Send them a greeting or a general message. If you wish to send the message to more people please select the same below.";

            if(!type){
                if(preSelectMsgFlag){
                    preSelectMsgFlag = false;
                    showMessagePopUp(msg,'tip');
                }
                else $("#message").text(msg);
            }
        }
        else{
            element.attr("checked",false);
            showMessagePopUp('The contact you selected, does not have valid email id.','error');
        }
    }

    function getAllDocuments(element){
        $.ajax({
            url:'/myDocuments',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(documents) {
                showUploadedDocsPopup(documents.documents,element)
            }
        })
    }
    function clickUpload(){

        $("#selectMessageDoc").click();
    }
    function showUploadedDocsPopup(documents,element){

            var html = _.template($("#uploaded-docs-popup").html())();
            element.popover({
                title: "",
                html: true,
                content: html,
                container: 'body',
                placement: 'left',
                id: "myPopover"
            });
            popoverSource =  element

            element.popover('show');
            var docsHtml = '';
        if(checkRequired(documents[0])) {
            for (var i in documents) {
                var docId = documents[i]._id;
                var documentName = documents[i].documentName;
                docsHtml += '<tr><td style="cursor:pointer" documentId=' + docId + ' documentName=' + documentName.replace(/\s/g, '&nbsp;') + '>' + documentName + '</td></tr>';
            }

        }
        else{
            docsHtml += '<tr><td>No uploaded documents</td></tr>';
        }
        $("#uploadedDocTable").html(docsHtml)
    }
    $("body").on("click","#closePopup-uploaded-docs",function(){

        popoverSource.popover('destroy');

    });
    $("body").on("click","#uploadMessageDoc-popup",function(){

        $("#selectMessageDoc").trigger("click");
    });
    $("body").on("click","#uploadedDocTable td",function(){
        var documentId = $(this).attr("documentId");
        var docName = $(this).attr("documentName");
        if(checkRequired(documentId) && checkRequired(docName)){
            docId = documentId;
            $("#docTitle").removeClass('invisible')
            $("#docTitleImg").removeClass('invisible')
            $("#docProgress").addClass('invisible')
            $("#docTitle").text(docName)
            if(checkRequired(popoverSource)){
                popoverSource.popover('destroy');
            }
        }

    });

    $("#uploadMessageDoc").on("click",function(){
            var element = $(this)
        getAllDocuments(element)
          //$("#selectMessageDoc").trigger("click");
    })
var uploadFlag = true;

    $("#selectMessageDoc").on("change",function(){

        if($("#selectMessageDoc")[0].files[0]){

            var file = $("#selectMessageDoc")[0].files[0];
            if (file) {
                if(checkRequired(popoverSource)){
                    popoverSource.popover('destroy');
                }

                var dateNow = new Date();
                var d = dateNow.getDate();
                var m = dateNow.getMonth();
                var y = dateNow.getFullYear();
                var hours = dateNow.getHours();
                var min = dateNow.getMinutes();
                var sec = dateNow.getSeconds();
                var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
                var dIdentity = timeStamp+'_'+file.name;
                var docNameTimestamp = dIdentity.replace(/\s/g,'');
                docName = file.name;
                $("#docProgress").css({display:'block'});
                $("#docProgress").removeClass('invisible')
                uploadFlag = false;
                var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
                var request = s3bucket.putObject(params);
                request.on('httpUploadProgress', function (progress) {

                    $("#docProgress").val(Math.round(progress.loaded / progress.total *100))
                });
                request.send(function (err, data) {
                    if(err){
                        uploadFlag = true;
                        console.log(err)
                        showMessagePopUp("Doc upload failed",'error')
                        //alert('Doc upload failed')
                    }
                    else{

                        uploadFlag = true;
                        var emails = $("#shareEmails").val();
                        docUrl = docUrl+docNameTimestamp;
                        var docD = {
                            documentName:docName,
                            documentUrl:docUrl,
                            awsKey:docNameTimestamp,
                            participants:emails+',h',
                            invitationId:'no',
                            sharedDate:new Date()
                        }

                        $.ajax({
                            url:'/uploadDocument',
                            type:'POST',
                            datatype:'JSON',
                            data:docD,
                            traditional:true,
                            success:function(result){
                                if(checkRequired(result._id)){
                                    docId = result._id
                                    $("#docTitle").removeClass('invisible')
                                    $("#docTitleImg").removeClass('invisible')
                                    $("#docProgress").addClass('invisible')
                                    $("#docTitle").text(result.documentName)
                                }

                            }
                        })
                    }
                });
            } else {
                showMessagePopUp("Please select file to upload.",'error')
                //alert('Please select file to upload.');
            }
        }
    })


    $('#selectHeaderImage').on('change',function(){


        var file = $(this)[0].files[0];


        if (file) {

            var url = URL.createObjectURL(this.files[0]);
            var img = new Image;
            img.onload = function() {
                if(img.width <=600 && img.height <= 200){
                    uploadHeaderImage(file);
                }
                else showMessagePopUp("Your current image size is :"+img.width+'px x '+img.height+'px. Recommended image size is 600px x 200px.');

            };
            img.src = url;

        } else {
            showMessagePopUp("Please select file to upload.",'error')

        }
    })

    function uploadHeaderImage(file){
        var dateNow = new Date();
        var d = dateNow.getDate();
        var m = dateNow.getMonth();
        var y = dateNow.getFullYear();
        var hours = dateNow.getHours();
        var min = dateNow.getMinutes();
        var sec = dateNow.getSeconds();
        var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
        var dIdentity = timeStamp+'_'+file.name;
        var docNameTimestamp = dIdentity.replace(/\s/g,'');
        imageName = file.name;
        $("#showMoreMsgImage").val(file.name)
        var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
        var request = imageBucket.putObject(params);
        request.on('httpUploadProgress', function (progress) {
            var pr = Math.round(progress.loaded / progress.total *100);
            $("#showMoreMsgImage").val('('+pr+'%)'+file.name);
            if(pr >= 100){
                $("#showMoreMsgImage").val(file.name);
            }
        });
        request.send(function (err, data) {
            if(err){
                uploadFlag = true;
                headerImage = false;
                showMessagePopUp("Image upload failed",'error')

            }
            else{
                uploadFlag = true;
                var emails = 'as';
                imageName = file.name
                imageUrl = imageUrl+docNameTimestamp;
                headerImage = true;
            }
        });
        // $("#docProgress").css({display:'block'});
    }


    var reloadFalg = false;
    $("#sendDocMessage").on("click",function(){

        var mmc = massMailCount;
        var mmc2 = mmc+selectedContactsArray.length;
        if(mmc2 <= 99 || contactsSelectFlag){

            var msg = $("#userMessage").val().trim()
            var msgSubject =  $("#showMoreMsgSubject").val().trim()
            var shareEmails ='';
            var selectedDate ='noDate';
            var sec = 'noSec';
            if(checkRequired(selectedContactsArray[0])){
                for(var i in selectedContactsArray){
                    shareEmails +=''+selectedContactsArray[i].personEmailId+',';

                }
            }
            if(checkRequired(msg) && checkRequired(msgSubject) && checkRequired(selectedContactsArray[0])){
                if(checkRequired(docId)){
                    mixpanelTrack("Share Document");
                    mixpanelIncrement("# Doc S");
                    mixpanelTrack("Send mail");
                    var existNumber = 0;
                    var failedNumber = 0;
                    var successNumber = 0;
                    for(var j in selectedContactsArray){
                        var msgNew = msg;
                        if(selectedContactsArray[j].conType == 'linkedin' || selectedContactsArray[j].conType == 'nonRelatasUser'){
                           // msgNew = 'Hi '+selectedContactsArray[j].personName+'<br>'+msg;
                        }
                        else msgNew = msg

                        var details = {
                            docId:docId,
                            userId:selectedContactsArray[j].personId || '',
                            relatasContact:selectedContactsArray[j].conType == 'linkedin' ? false : true,
                            shareEmails:selectedContactsArray[j].personEmailId,
                            personName:selectedContactsArray[j].personName || '',
                            userMsg:msgNew,
                            sharedOn:new Date(),
                            expiryDate:selectedDate,
                            expirySec:sec,
                            subject:msgSubject,
                            mmcCount:mmc2,
                            createMsg:true,
                            responseFlag:true,
                            headerImage:headerImage,
                            imageName:imageName,
                            imageUrl:imageUrl
                        }
                        $.ajax({
                            url:'/shareDocument',
                            type:'POST',
                            datatype:'JSON',
                            data:details,
                            success:function(docShareInfo){
                                if(docShareInfo == 'exist'){
                                    successNumber++;
                                }else
                                if(docShareInfo){
                                    successNumber++;
                                    reloadFalg = true;
                                }
                                else{
                                    failedNumber++;
                                }
                                $("#message").html(""+successNumber+" messages have been sent successfully.<br>"+failedNumber+" messages failed.")
                            }
                        })
                        if(i == selectedContactsArray.length-1){
                            reloadFalg = true;
                            setTimeout(function(){
                                showMessagePopUp(successNumber+" messages have been sent successfully.<br>"+failedNumber+" messages failed.",'success',true);
                            },1000)
                        }
                    }
                }
                else{
                    var massMailArr = [];
                    mixpanelTrack("Send mail");
                    for(var k in selectedContactsArray){
                        var msgNew2 = msg;

                        //msgNew2 = 'Hi '+selectedContactsArray[k].personName+'<br>'+msg;

                        var msgObj = {
                            senderEmailId:userProfile.emailId,
                            userId:selectedContactsArray[k].personId || '',
                            relatasContact:selectedContactsArray[k].conType == 'linkedin' ? false : true,
                            senderName:userProfile.firstName+' '+userProfile.lastName,
                            receiverEmailId:selectedContactsArray[k].personEmailId,
                            receiverName:selectedContactsArray[k].personName,
                            message:msgNew2,
                            mmcCount:mmc2,
                            responseFlag:true,
                            headerImage:headerImage,
                            imageName:imageName,
                            imageUrl:imageUrl
                        }

                        massMailArr.push(msgObj)
                    }


                    if(checkRequired(massMailArr[0])){
                        var stringMsg = JSON.stringify(massMailArr);

                        $.ajax({
                            url:'/sendEmailMessage',
                            type:'POST',
                            datatype:'JSON',
                            data:{data:stringMsg,subject:msgSubject},
                            success:function(response){

                                if(response){
                                    reloadFalg = true;
                                    showMessagePopUp("Your message has been sent successfully.",'success');
                                }
                                else{
                                    showMessagePopUp("Your message has not been sent. Please try again.",'error');
                                }
                            }
                        })
                    }

                }
            }
            else{
                showMessagePopUp("Either you have forgotten to select contacts or not entered Subject OR message. Please try again.",'error');
            }
        }
        else{
            showMessagePopUp("You have crossed your weekly limit of 99 messages. If you need to send more please mail us at hello@relatas.com.")

        }

    });

    jQuery.nl2br = function(varTest){
        return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
    };
    function showMessagePopUp(message,msgClass,isHtml)
    {

            var html = _.template($("#message-popup").html())();
            $("#user-name").popover({
                title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
                html: true,
                content: html,
                container: 'body',
                placement: 'bottom',
                id: "myPopover"
            });
            $("#user-name").popover('show');
            $(".arrow").addClass("invisible");
            popoverSource = $("#user-name");
            if(msgClass == 'error'){
                $(".popover").addClass('popupError')
                $("#popupTitle").text('ERROR')
            }else
            if(msgClass == 'success'){
                $(".popover").addClass('popupSuccess')
                $("#popupTitle").text('SUCCESS')
            }else
            if(msgClass == 'tip'){
                $(".popover").addClass('popupTip')
                $("#popupTitle").text('TIP')
            }
            //setTimeout(function(){
            if(isHtml){

                $("#message").html(message)
            }else{
                $("#message").text(message)
            }
        $("#closePopup-message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
        if(reloadFalg){
            reloadFalg = false;
            window.location.reload()
        }
        if(cFlag){
            cFlag = false;
            window.location.reload()
        }
    });

    function applyDataTable(){
         table = $('#contactsTable').DataTable({
             "dom": '<"top"iflp<"clear">>',
             "fnDrawCallback": function(oSettings) {
                 if(checkRequired(table)){
                     if (table.rows() < 11) {
                         $('.dataTables_paginate').hide();
                     }
                     else  $('.dataTables_paginate').show();
                 }else
                 if ($('#contactsTable tr').length < 11) {
                     $('.dataTables_paginate').hide();
                 }
                 else  $('.dataTables_paginate').show();
             },
             "order": [[ 0, "asc" ]],
             "createdRow": function ( row, data, index ) {
                 if(contactsSelectFlag2 && eArr.length > 0){
                     var checkBox = $("#selectedContact",row)
                     var emailId = checkBox.attr('emailId');
                     var isExist = false;
                     for(var email=0; email<eArr.length; email++){
                        if(eArr[email] == emailId){
                            isExist = true;
                        }
                        else{
                            if(!isExist)
                              isExist = false;
                        }
                     }
                     if(isExist){
                         eArr.splice(email,1);

                     }else{
                         checkBox.attr('checked',true);
                         selectedContact(checkBox,false);
                     }

                     if(table.page.info().recordsTotal == totalContactsLength){
                         var actualSelectCount = totalContactsLength - interactedEmailLength;
                         if(actualSelectCount != selectedContactsLength){
                             var diff = selectedContactsLength - actualSelectCount;
                             $("#message").append("<hr><span style='font-size: 11px;color: rgb(195, 195, 195);'>The difference of "+diff+" contacts in dashboard v/s the number above is because of the data discripancy in your Google Calendar and Google Contacts</span>");

                         }
                     }

                 }

             },
             "oLanguage": {
                 "sEmptyTable": "No Contacts"
             }
         });
    }

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }
    function removeNull(arr){
        var newArr = []
        for(var ele=0; ele<arr.length; ele++){
            if(checkRequired(arr[ele]))
                    newArr.push(arr[ele])
        }
        return newArr;
    }

    function getTextLength(text,maxLength){

        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    function getDateFormat(date){
        var dateObj = new Date(date)
        return dateObj.getDate()+' '+monthNameSmall[dateObj.getMonth()]+','+dateObj.getFullYear()
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
    $("#shw").click(function(){
        $(this).hide('slow');
        $("#show-hide-doc").hide();
        $("#show-hide").slideToggle();
    });
    $("#show-hide2").on("click",function(){
        $("#shw").slideToggle('slow');
        $("#show-hide-doc").hide()
        $("#show-hide").slideToggle();
    })
    $("#shw-doc").click(function(){
        $("#show-hide").hide()
        $("#show-hide-doc").toggle();
    });

    $("#showMoreMsgImage").on("click",function(){
        $("#selectHeaderImage").trigger("click");
    })

    function sidebarHeight(){
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height':sideHeight})
    }

});