
$(document).ready(function() {

    var userProfile,s3bucket,imageBucket,docName,docUrl,imageUrl,AWScredentials;
    var monthNameFull = ['January','February','March','April','May','June','July','August','September','October','November','December']
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']

    var docObj = [];
    var image = [];

    getAWSCredentials();
    $("#eventDescription").jqte();
    function getAWSCredentials(){
        $.ajax({
            url:'/getAwsCredentials',
            type:'GET',
            datatye:'JSON',
            success:function(credentials){
                AWScredentials = credentials;
                AWS.config.update(credentials);

                //AWS.config.region = "us-east-1";
                AWS.config.region =  credentials.region;

                s3bucket = new AWS.S3({ params: {Bucket:credentials.bucket} });
                imageBucket = new AWS.S3({ params: {Bucket:credentials.imageBucket} });
                docUrl = 'https://'+AWScredentials.bucket+'.s3.amazonaws.com/';
                imageUrl = 'https://'+AWScredentials.imageBucket+'.s3.amazonaws.com/';

            }
        })
    }

    var windowUrl = ""+window.location+"/location";
    var pageUrl = windowUrl.split('/')[3];

    if(pageUrl == 'createAnEvent'){
        getUserProfile();
        getStoredSession();
        $("#createEvent").val("CREATE EVENT");
    }
    else{
        $("#createEvent").val("UPDATE EVENT");
        getUserProfile();
    }


    $("[data-toggle='tooltip']").tooltip();

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(validateRequired(result.firstName)){
                    identifyMixPanelUser(result)
                    if( result.profilePicUrl.charAt(0) == '/' || result.profilePicUrl.charAt(0) == 'h'){

                        $('#profilePic').attr("src",result.profilePicUrl);
                    }else $('#profilePic').attr("src","/"+result.profilePicUrl);
                }

                if(validateRequired(result.firstName)){
                    $("#yourName").val(result.firstName+' '+result.lastName);
                    $("#yourEmail").val(result.emailId);

                    $("#yourName").attr("readonly",true);
                    $("#yourEmail").attr("readonly",true);

                }

                if(pageUrl != 'createAnEvent'){
                    getRequestedEvent()
                }

                imagesLoaded("#profilePic",function(instance,img) {
                    if(instance.hasAnyBroken){
                        $('#profilePic').attr("src","/images/default.png");
                    }
                });

            },
            error: function (event, request, settings) {


            },
            timeout: 20000
        })
    }
    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }


var eventEdit;
    function getRequestedEvent(){
        $.ajax({
            url:'/getRequestedEventEdit',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(thisEvent){
                if(thisEvent == false){

                }
                else if(validateRequired(thisEvent)){
                    eventEdit = thisEvent;
                    var start = new Date(thisEvent.startDateTime);
                    var end = new Date(thisEvent.endDateTime);
                    thisEvent.startDateTime = start.getFullYear()+'/'+getValidTime(start.getMonth()+1)+'/'+getValidTime(start.getDate())+' '+getValidTime(start.getHours())+':'+getValidTime(start.getMinutes());
                    thisEvent.endDateTime = end.getFullYear()+'/'+getValidTime(end.getMonth()+1)+'/'+getValidTime(end.getDate())+' '+getValidTime(end.getHours())+':'+getValidTime(end.getMinutes());
                    bindData(thisEvent)
                }
            }
        })
    }

   function getValidTime(num){
       if(num < 10)
             return '0'+num
       else
             return num
   }

    $("#createEvent").on("click",function(){
        var event = getEventData();

        if(validateEventDetails(event)){
            event.startDateTime = new Date(event.startDateTime);
            event.endDateTime = new Date(event.endDateTime);
            if(event.startDateTime >=  event.endDateTime){
                showMessagePopUp("Please select End date greater than Start date.",'error')
            }
            else{
                if(pageUrl == 'createAnEvent'){
                    createEvent(event)
                }
                else{
                    event.creatorId = eventEdit.createdBy.userId;
                    event.eventId = eventEdit._id;
                    createEvent(event)
                }
            }

        }
        else{

        }
    })

    $("#startDateTime").datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        minDate: new Date()
    });

    $("#endDateTime").datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        minDate: new Date()
    });


    $("#startDateTimeImage").datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        minDate: new Date(),
        onClose: function (dp,$input) {
            $("#startDateTime").val($("#startDateTimeImage").val())
        }
    });

    $("#endDateTimeImage").datetimepicker({
        dayOfWeekStart: 1,
        lang: 'en',
        minDate: new Date(),
        onClose: function (dp,$input) {
            $("#endDateTime").val($("#endDateTimeImage").val())
        }
    });
    function clearSession(){

        $.ajax({
            url:'/clearSavedEventDetails',
            type:'GET',
            datatpe:'JSON',
            success:function(response){

            }
        })
    }
    function storeToSession(){
        var data = getEventData()
        $.ajax({
            url:'/saveEventDetails',
            type:'POST',
            datatpe:'JSON',
            data:data,
            success:function(response){

            }
        })
    }

    function bindData(response){
        if(validateRequired(response)){
            if(validateRequired(response.eventName))
                $("#eventName").val(response.eventName)

            if(validateRequired(response.eventLocationUrl))
                $("#eventLocationUrl").val(response.eventLocationUrl)

            if(validateRequired(response.startDateTime))
                $("#startDateTime").val(response.startDateTime)

            if(validateRequired(response.endDateTime))
                $("#endDateTime").val(response.endDateTime)

             if(validateRequired(response.image)){
                 image = response.image;
                 $("#eventImage").val(response.image.imageName)
             }


            if(validateRequired(response.docs)){
                docObj = response.docs;
                $("#eventAgenda").val(response.docs[0].documentName)
            }


            if(validateRequired(response.locationName))
                $("#locationName").val(response.locationName)

            if(validateRequired(response.eventDescription))
                $("#eventDescription").jqteVal(response.eventDescription || '')

            if(validateRequired(response.eventCategory))
                $("#eventCategory").val(response.eventCategory)

            if(validateRequired(response.speakerName))
                $("#speakerName").val(response.speakerName)

            if(validateRequired(response.speakerDesignation))
                $("#speakerDesignation").val(response.speakerDesignation)

            if(validateRequired(response.accessType)){
                var accessType = response.accessType;
                if(accessType == 'public'){
                    $("#publicAccess").attr('checked',true);
                    $("#privateAccess").attr('checked',false);
                }
                else{
                    $("#publicAccess").attr('checked',false);
                    $("#privateAccess").attr('checked',true);
                }
                $("#eventMap").val(response.eventMap)

            }

            if(validateRequired(response.locationType)){
                var locationType = response.locationType;
                if(locationType == 'offline'){
                    $("#offline").attr('checked',true);
                    $("#online").attr('checked',false);
                }
                else{
                    $("#offline").attr('checked',false);
                    $("#online").attr('checked',true);
                }
            }

            if(validateRequired(response.eventLocation)){
                var locationType2 = response.eventLocation;
                if(locationType2 == 'Online'){
                    $("#offline").attr('checked',false);
                    $("#online").attr('checked',true);
                    $("#locationName").val('Online');

                }
                else{
                    var lUrl = $("#eventLocationUrl").val().trim();
                    if(validateRequired(lUrl)){
                        lUrl = lUrl.split('/');
                        var lName = lUrl[5];
                        $("#locationName").val(response.locationName || lName.replace(/\+/g, ' '));
                    }
                    $("#offline").attr('checked',true);
                    $("#online").attr('checked',false);


                }
            }

        }
    }

    function getStoredSession(){
        $.ajax({
            url:'/getSavedEventDetails',
            type:'GET',
            datatpe:'JSON',

            success:function(response){
                bindData(response)

            }
        })
    }
    function getEventData(){
        var eventData = {
            eventName:$("#eventName").val().trim(),
            eventLocationUrl:$("#eventLocationUrl").val().trim(),
            startDateTime:$("#startDateTime").val().trim(),
            endDateTime:$("#endDateTime").val().trim(),
            userName:$("#yourName").val().trim(),
            userEmailId:$("#yourEmail").val().trim(),
            eventCategory:$("#eventCategory").val(),
            locationName:$("#locationName").val().trim(),
            accessType:$("#publicAccess").prop('checked') ? 'public' : 'private',
            locationType:$("#offline").prop('checked') ? 'offline' : 'online',
            eventDescription:$("#eventDescription").val().trim(),
            speakerName:$("#speakerName").val(),
            speakerDesignation:$("#speakerDesignation").val(),
            createdDate:new Date(),
            docs:docObj,
            image:image
        }

        return eventData;
    }

    $("#online").on("change",function(){
        if($("#online").prop('checked')){
            $("#locationName").val("Online");
            $("#locationName").attr("readonly",true);
        }
        else{
            $("#locationName").val("");
        }
    })
    $("#offline").on("change",function(){
        if($("#offline").prop('checked')){
            $("#locationName").val("");
            $("#locationName").attr("readonly",false);
        }
        else{
            $("#locationName").val("");
        }
    });
    $("#eventLocationUrl").on("focusout",function(){
        if($("#offline").prop('checked')) {
            var lUrl = $("#eventLocationUrl").val().trim();
            if(validateRequired(lUrl)){
                lUrl = lUrl.split('/');
                var lName = lUrl[5];
                $("#locationName").val(lName.replace(/\+/g, ' '));
            }
        }
    });
var updateEventFlag = false;
    function createEvent(event){
        $("#createEvent").attr("disabled", true);
        event.docs = JSON.stringify(docObj);
        event.image = JSON.stringify(image);
        $.ajax({
            url:'/createEvent',
            type:'POST',
            datatype:'JSON',
            data:event,
            traditional:true,
            success:function(response){
                $("#createEvent").attr("disabled", false);
                if(response == 'loginRequired'){
                    storeToSession();
                    $("#eventsBody").slideToggle();
                    $("#lightBoxPopup").slideToggle();
                }else
                if(response == 'invalidUrl'){

                    showMessagePopUp("<span>Please provide valid location URL.<br> Click <a href='https://www.google.co.in/maps/place/Bengaluru,+Karnataka/@12.9539974,77.6309395,11z/data=!3m1!4b1!4m2!3m1!1s0x3bae1670c9b44e6d:0xf8dfc3e8517e4fe0' title='https://www.google.co.in/maps/place/Bengaluru,+Karnataka/@12.9539974,77.6309395,11z/data=!3m1!4b1!4m2!3m1!1s0x3bae1670c9b44e6d:0xf8dfc3e8517e4fe0' target='_blank'> here </a> to see valid url.</span>",'error',true);
                }else
                if(!response){
                    if(pageUrl == 'createAnEvent') {
                        showMessagePopUp('An error occurred while creating event. Please try again.', 'error');
                    }
                    else{
                        showMessagePopUp('An error occurred while updating event. Please try again.', 'error');
                    }
                }
                else{
                    if(pageUrl == 'createAnEvent') {
                        mixpanelTrack("Create Event");
                        var event = response;
                        var today = new Date()
                        clearSession()
                        clearAllFields();
                        showMessagePopUp('Event has been successfully created.','success');
                    }
                    else{
                        clearSession()
                        updateEventFlag = true;
                        showMessagePopUp("Event has been successfully updated.",'success');
                    }

                }
            },
            error: function (event, request, settings) {
                var status = event.status;
                switch(status){
                    case 4010:
                        console.error("Please specify all fields are required");
                        showMessagePopUp("All fields are mandatory",'error')
                        break;

                    default :
                        //TODO : render the 404 error page
                        console.error("render the 404 err page");
                        showMessagePopUp("An error occurred while creating the event. Please try again.",'error')
                }
            }
        })
    }
    var uploadFlag = false;
    $("#eventImage").on("focus",function(){
        $("#eventImageSelect").trigger("click");
    });

    $("#eventAgenda").on("focus",function(){
        $("#eventAgendaSelect").trigger("click");
    });

    $('#eventAgendaSelect').on('change',function(){


        var file = $(this)[0].files[0];
        if (file) {
            docObj = []
            var dateNow = new Date();
            var d = dateNow.getDate();
            var m = dateNow.getMonth();
            var y = dateNow.getFullYear();
            var hours = dateNow.getHours();
            var min = dateNow.getMinutes();
            var sec = dateNow.getSeconds();
            var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
            var dIdentity = timeStamp+'_'+file.name;
            var docNameTimestamp = dIdentity.replace(/\s/g,'');
            docName = file.name;
            $("#eventAgenda").val(file.name)
            uploadFlag = false;
            var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
            var request = s3bucket.putObject(params);
            request.on('httpUploadProgress', function (progress) {
                var pr = Math.round(progress.loaded / progress.total *100);
                $("#eventAgenda").val('('+pr+'%)'+file.name);
                if(pr == 100){
                    $("#eventAgenda").val(file.name);
                }
            });
            request.send(function (err, data) {
                if(err){
                    uploadFlag = true;

                    showMessagePopUp("Doc upload failed",'error')

                }
                else{

                    uploadFlag = true;
                    var emails = 'as'
                    docUrl = docUrl+docNameTimestamp;
                    var docD = {
                        documentName:docName,
                        documentUrl:docUrl,
                        awsKey:docNameTimestamp,
                        access:'public',
                        participants:emails+',h',
                        invitationId:'no',
                        sharedDate:new Date()
                    }

                    $.ajax({
                        url:'/uploadDocument',
                        type:'POST',
                        datatype:'JSON',
                        data:docD,
                        traditional:true,
                        success:function(result){
                           if(validateRequired(result)){
                               docObj.push({
                                   documentName:result.documentName,
                                   documentUrl:result.documentUrl,
                                   documentId:result._id,
                                   addedOn:new Date()
                               })
                           }

                        }
                    })
                }
            });

        } else {
            showMessagePopUp("Please select file to upload.",'error')

        }
    })

    $('#eventImageSelect').on('change',function(){


        var file = $(this)[0].files[0];


        if (file) {
            var url = URL.createObjectURL(this.files[0]);
            var img = new Image;
            img.onload = function() {
                if(img.width <=200 && img.height <= 200){
                    uploadEventImage(file);
                }
                else showMessagePopUp("Your current image size is :"+img.width+'px x '+img.height+'px. Recommended image size is 200px x 200px.');

            };
            img.src = url;

        } else {
            showMessagePopUp("Please select file to upload.",'error')

        }
    })

    function uploadEventImage(file){
        var dateNow = new Date();
        var d = dateNow.getDate();
        var m = dateNow.getMonth();
        var y = dateNow.getFullYear();
        var hours = dateNow.getHours();
        var min = dateNow.getMinutes();
        var sec = dateNow.getSeconds();
        var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
        var dIdentity = timeStamp+'_'+file.name;
        var docNameTimestamp = dIdentity.replace(/\s/g,'');
        docName = file.name;
        $("#eventImage").val(file.name)
        var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
        var request = imageBucket.putObject(params);
        request.on('httpUploadProgress', function (progress) {
            var pr = Math.round(progress.loaded / progress.total *100);
            $("#eventImage").val('('+pr+'%)'+file.name);
            if(pr == 100){
                $("#eventImage").val(file.name);
            }
        });
        request.send(function (err, data) {
            if(err){
                uploadFlag = true;

                showMessagePopUp("Doc upload failed",'error')

            }
            else{
                image = []
                uploadFlag = true;
                var emails = 'as'
                imageUrl = imageUrl+docNameTimestamp;
                image.push({
                    awsKey:docNameTimestamp,
                    imageUrl:imageUrl,
                    imageName:file.name,
                    addedOn:new Date()
                });
            }
        });
    }

    function validateEventDetails(event){
        if(!validateRequired(event.eventName)){
            showMessagePopUp("Please provide Event name.",'error')
            return false;
        }
        else if(!isUrl(event.eventLocationUrl)){
            showMessagePopUp("Please provide valid Url.",'error')
            return false;
        }
        else if(!validateRequired(event.startDateTime)){
            showMessagePopUp("Please provide Event start date/ time.",'error')
            return false;
        }
        else if(!validateRequired(event.endDateTime)){
            showMessagePopUp("Please provide Event end date/ time.",'error')
            return false;
        }
        else if(!validateRequired(event.userName)){
            showMessagePopUp("Please provide your name.",'error')
            return false;
        }
        else if(!validateRequired(event.userEmailId)){
            showMessagePopUp("Please provide your email id.",'error')
            return false;
        }
        else if(!validateRequired(event.eventCategory) || event.eventCategory == 'Select Category'){
            showMessagePopUp("Please select Event category.",'error')
            return false;
        }
        else if(!validateRequired(event.locationName)){
            showMessagePopUp("Please provide location name.",'error')
            return false;
        }
        else if(!validateRequired(event.eventDescription)){
            showMessagePopUp("Please provide event description.",'error')
            return false;
        }
        else if(!validateEmailField(event.userEmailId)){
            showMessagePopUp("Please provide valid email id.",'error')
            return false;
        }else return true;
    }
    function isUrl(s) {
        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
        return regexp.test(s);
    }




    function clearAllFields(){
        docObj = [];
        image = [];
        $("#eventName").val(''),
            $("#eventLocationUrl").val(''),
            $("#startDateTime").val(''),
            $("#endDateTime").val(''),
            $("#eventImage").val(''),
            $("#eventAgenda").val(''),
            $("#eventCategory").val('Select Category'),
            $("#speakerDesignation").val(''),
            $("#speakerName").val(''),
            $("#locationName").val(''),
            $("#eventDescription").jqteVal(''),
            $("#publicAccess").attr('checked',true),
            $("#offline").attr('checked',true)
            getAWSCredentials();
    }


    $('#loginUsingLinkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn");

        window.location.replace('/linkedinLoginFromCreateEventPage');
    });
    $('#login-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn");

        window.location.replace('/linkedinLoginFromCreateEventPage');
    });

    $('#sign-up-with-linkedin').on('click',function(){
        mixpanelTrack("Signup LinkedIn");

        window.location.replace('/linkedinLoginFromCreateEventPage');
    });

    $('#loginUsingGoogle').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenCreateEventPage');
    });

    $('#login-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenCreateEventPage');
    });
    $('#sign-up-with-google').on('click',function(){
        mixpanelTrack("Signup Google")

        window.location.replace('/getGoogleTokenCreateEventPage');
    });

    $('#loginFormButton').on('click',function(){
        var emailId1 = $("#emailId2").val();
        var password1 = $("#password").val();

        if (validateRequired(emailId1) && validateRequired(password1)) {
            emailId1 = emailId1.toLowerCase();
            var credentials={
                emailId:emailId1,
                password:password1
            }

            $.ajax({
                url:'/loginFromPublicProfile',
                type:'POST',
                datatype:'JSON',
                data:credentials,
                traditional:true,
                success:function(result){
                    if (result) {
                        getUserProfile()
                        $("#eventsBody").slideToggle();
                        $("#lightBoxPopup").slideToggle();

                    }
                    else{
                        showMessagePopUp("Looks like either your email or password is incorrect. Please try again.",'error')
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
        else{
            showMessagePopUp("Please enter your credentials",'error')

        }
    });

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    function showMessagePopUp(message,msgClass,isHtml)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'20%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        //setTimeout(function(){
        if(isHtml){
            $("#message").html(message)
        }
        else{
            $("#message").text(message)
        }
        //},1000);
        $("#closePopup-message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
        if(updateEventFlag){
            window.location.replace('/event/'+eventEdit._id);
        }
    });

    // Function to validate required field
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function sidebarHeight(){
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height':sideHeight})
    }
    $("#shw").click(function(){
        $("#show-hide").slideToggle();
    });

    function getEventDateFormat(date){
        var day = date.getDate()
        var format = day+''+getDayString(day)+' '+monthNameSmall[date.getMonth()] +' | '+getTimeFormatWithMeridian(date)+' ('+getTimeZone()+')';
        return format;
    }
    function getTimeZone(){
        var start = new Date()
        var zone = start.toTimeString().replace(/[()]/g,"").split(' ');

        if(zone.length > 3){
            var tz = '';
            for(var i=2; i<zone.length; i++){

                tz += zone[i].charAt(0);
            }
            return tz || zone[2]
        }
        else return zone[2]

    }
    function getDayString(day) {
        if (day == 1 || day == 21 || day == 31) {
            return 'st';
        }
        else if (day == 2 || day == 22 || day == 23) {
            return 'nd';
        }
        else if (day == 3 || day == 23) {
            return 'rd';
        }
        else return 'th';
    };
    function getTimeFormatWithMeridian(date){
        var hrs = date.getHours();
        var min = date.getMinutes();
        var meridian;
        if(hrs > 12){
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if(hrs == 12){
                meridian = 'PM'
            }else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0"+min;
        };
        var time = hrs+":"+min+" "+meridian;
        return time;
    }

    setTimeout(function(){
        sidebarHeight()
    },1200)

    $("#lightBoxClose").on("click",function(){
        $("#eventsBody").slideToggle();
        $("#lightBoxPopup").slideToggle();
    })

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
});