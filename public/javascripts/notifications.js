$(document).ready(function(){

    var userProfile,userTimezone;
    var table;
    var removeFlag = false;
    var popoverSource = null;
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    applyDataTable();
    getUserProfile();
    getReceivedCalendarPasswordRequests();
    getRedDocs();
    getNonReadDocs();
    getUnReadMessages();

    function updateNotificationCount(num){
        var current = $("#notificationsCount").text();
         num = parseInt(current)+num;
        $("#notificationsCount").text(num);
    }

    $("#userMessage").jqte();
    function getUnReadMessages(){
        $.ajax({
            url: '/messages/unRead/messages',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if(checkRequired(result) && result != false && checkRequired(result.messages) && checkRequired(result.messages.length)){
                    var messages = result.messages;
                    updateNotificationCount(messages.length);
                    for(var i=0; i<messages.length; i++){

                        var msgUrl = '/messages/all/'+messages[i].senderId+'?id='+messages[i]._id;
                        var eventDate = getDateFormat(messages[i].sentOn);
                        var titleId = 'title'+messages[i]._id;
                        var nameId = 'name'+messages[i].senderId;
                        var butClass = 'button'+messages[i]._id;
                        var imgClass = 'image'+messages[i]._id;
                        addRowsToTable([
                            '<img style="width: 19px;cursor: pointer" src="/images/send_mail2.png" class='+imgClass+'>',
                            '<a href='+msgUrl+'><span style="color: rgb(51, 56, 77);cursor: pointer" class='+titleId+'></span></a>',
                            '<span id="sortDate" value='+messages[i].sentOn+'>'+eventDate+'</span>',
                            '<span class='+nameId+' id='+messages[i]._id+'></span>',
                            '<a href='+msgUrl+' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;cursor:pointer;border: 3px solid #03a2ea;" class='+butClass+'>  &nbsp;&nbsp;&nbsp; Reply &nbsp;&nbsp;&nbsp; </a>'
                        ]);
                        appendData(messages[i]);
                    }

                    if(checkRequired(result.users) && result.users.length > 0){
                       var users = result.users;
                        for(var j=0; j<users.length; j++){
                            var senderName = users[j].firstName + ' ' + users[j].lastName;
                            $(".name"+users[j]._id).attr('title',senderName);
                            $(".name"+users[j]._id).text(getTextLength(senderName,20));
                            $(".name"+users[j]._id).attr('profile',JSON.stringify(users[j]));
                        }
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }
    function appendData(message){
        $(".title"+message._id).html(getTextLength(message.subject || '',30));
        $(".title"+message._id).attr('title',message.subject || '');
        $(".button"+message._id).attr('message',JSON.stringify(message));
        $(".title"+message._id).attr('message',JSON.stringify(message));
        $(".image"+message._id).attr('message',JSON.stringify(message));
    }
    var profile,message,mElement;

    $("body").on("click", "#messageAction", function () {
        showMessageDetails($(this));
    });
    $("body").on("click", "#messageActionImage", function () {
        showMessageDetails($(this));
    });
    $("body").on("click", "#messageActionTitle", function () {
        showMessageDetails($(this));
    });

    function showMessageDetails(element){
        mElement = element;
        message = JSON.parse(element.attr('message'));
        profile = JSON.parse($("#"+message._id).attr('profile'));
        $("#subject").text(message.subject || '');
        $("#actual-message").html(message.message);
        $("#messageDetails").show();
        if(checkRequired(message.subject)){
            if(message.subject.substr(0, 2) != 'RE'){
                $("#MsgSubject").val('RE: '+message.subject || '');
            }
            else $("#MsgSubject").val(message.subject || '');
        }

        updateMessageReadStatus(message._id);
        sidebarHeight();
    }

    $("#sendMessage").on('click',function(){
        var massMailArr = [];
        var msg = $("#userMessage").val().trim();
        var subject = $("#MsgSubject").val();
        if(checkRequired(msg)){
            //msg = 'Hi '+profile.firstName+'<br>'+msg;

            subject = checkRequired(subject) ? subject : message.subject

            var msgObj = {
                senderEmailId:userProfile.emailId,
                userId:profile._id || '',
                relatasContact: true,
                senderName:userProfile.firstName+' '+userProfile.lastName,
                receiverEmailId:profile.emailId,
                receiverName:profile.firstName+' '+profile.lastName,
                message:msg,
                responseFlag:true,
                headerImage:false
            }
            massMailArr.push(msgObj)
            if(checkRequired(massMailArr[0])){
                var stringMsg = JSON.stringify(massMailArr);

                $.ajax({
                    url:'/sendEmailMessage',
                    type:'POST',
                    datatype:'JSON',
                    data:{data:stringMsg,subject:subject},
                    success:function(response){

                        if(response){
                            mixpanelTrack("Send mail");
                            showMessagePopUp("Your message has been sent successfully.",'success');
                            table.row(mElement.parents('tr')).remove().draw();
                            $("#MsgSubject").val('')
                            $("#messageDetails").hide();
                            $("#userMessage").jqteVal("");
                        }
                        else{
                            showMessagePopUp("Your message has not been sent. Please try again.",'error');
                        }
                    }
                })
            }
        }else showMessagePopUp("Please enter your reply message.",'error')
    })

    function updateMessageReadStatus(messageId) {
        $.ajax({
            url: '/messages/update/readStatus/'+messageId,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (requests) {

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function sidebarHeight() {
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height': sideHeight})
    }

    function getReceivedCalendarPasswordRequests() {
        $.ajax({
            url: '/getReceivedCalendarPasswordRequests',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (requests) {
                if (requests == false) {

                }
                else if(checkRequired(requests)){
                    if(requests.length > 0){
                        updateNotificationCount(requests.length)
                        for(var req=0; req<requests.length; req++){
                            displayCalendarPasswordRequests(requests[req]);
                        }
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function displayCalendarPasswordRequests(request) {
        if (checkRequired(request)) {
            $.ajax({
                url: '/getUserBasicInfo/' + request.requestFrom.userId,
                type: 'GET',
                datatype: 'JSON',
                traditional: true,
                success: function (profile) {
                    if (checkRequired(profile)) {
                        var senderName = profile.firstName + ' ' + profile.lastName;
                        var name = profile.firstName + '-' + profile.lastName;

                        var title = getTextLength("Calendar password request",30);
                        var eventDate = getDateFormat(request.requestDate);

                        addRowsToTable([
                            '<img src="/images/relatas_20x20.png">',
                            '<a style="color: rgb(51, 56, 77);cursor:pointer" title="Calendar password request">'+title+'</a>',
                            '<span id="sortDate" value='+request.requestDate+'>'+eventDate+'</span>',
                            '<span title='+senderName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(senderName, 20)+'</span>',
                            '<a id="calPassReq" requestId=' + request._id + ' requestByEmailId=' + profile.emailId + ' requestByName=' + name + ' requestByUserId=' + profile._id + ' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;cursor:pointer;border: 3px solid #03a2ea;">  &nbsp;&nbsp;&nbsp; Aprove &nbsp;&nbsp;&nbsp; </a>'
                        ]);
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
    }

    function getNonConfirmedEvents() {
        $.ajax({
            url: '/events/nonConfirmedEvents',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if(checkRequired(result)){
                    var today = new Date();
                    today.setHours(0,0,0,0);
                    if(result.length > 0){


                        for(var event=0; event<result.length; event++){

                            if(new Date(result[event].startDateTime) >= today){
                                updateNotificationCount(1);
                                var title = result[event].eventName;
                                var eventUrl = '/event/'+result[event]._id;
                                var RSVPEventUrl = '/event/addToCalendar/'+result[event]._id+'/'+userProfile._id;
                                var eventDate = getDateFormat(result[event].startDateTime);
                                var creator = result[event].createdBy.userName;
                                addRowsToTable([
                                    '<img src="/images/event_new.png">',
                                    '<a style="color: rgb(51, 56, 77);" href='+eventUrl+' title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,30)+'</a>',
                                    '<span id="sortDate" value='+result[event].startDateTime+'>'+eventDate+'</span>',
                                    '<span title='+creator.replace(/\s/g, '&nbsp;')+'>'+ getTextLength(creator,20)+'</span>',
                                    '<a href=' + RSVPEventUrl + ' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;border: 3px solid #03a2ea;">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RSVP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a>'
                                ]);
                            }
                        }
                    }
                }else{

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function getRedDocs() {
        $.ajax({
            url: '/documents/sharedDocRed',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                   
                if(checkRequired(result)){
                    if(result.length > 0){
                        updateNotificationCount(result.length)
                        for(var doc=0; doc<result.length; doc++){
                            for(var share=0; share<result[doc].sharedWith.length; share++){
                                if(result[doc].sharedWith[share].accessed){
                                    var title = result[doc].documentName;
                                    var docUrl = '/docs';
                                    var isoDate = result[doc].sharedWith[share].lastAccessed;
                                    var docDate = getDateFormat(isoDate);
                                    var sharedBy = result[doc].sharedWith[share].firstName;
                                    addRowsToTable([
                                        '<img src="/images/mailer/icon_pdf_new.png">',
                                        '<a style="color: rgb(51, 56, 77);" href='+docUrl+' title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,30)+'</a>',
                                        '<span id="sortDate" value='+isoDate+'>'+docDate+'</span>',
                                        '<span title='+sharedBy.replace(/\s/g, '&nbsp;')+'>'+getTextLength(sharedBy,20)+'</span>',
                                        '<a href=' + docUrl + ' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;border: 3px solid #03a2ea;">  &nbsp;&nbsp;&nbsp; Analytics &nbsp;&nbsp;&nbsp; </a>'
                                    ]);
                                }
                            }
                        }
                    }
                }else{

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function getNonReadDocs() {
        $.ajax({
            url: '/documents/sharedWithMeNonRead',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if(checkRequired(result)){
                    if(result.length > 0){
                        updateNotificationCount(result.length)
                        for(var doc=0; doc<result.length; doc++){
                            var title = result[doc].documentName;
                            var docUrl = '/readDocument/'+result[doc]._id;
                            var isoDate = result[doc].sharedWith[0].sharedOn || result[doc].sharedDate;
                            var docDate = getDateFormat(isoDate);
                            var sharedBy = result[doc].sharedBy.firstName;
                            addRowsToTable([
                                '<img src="/images/mailer/icon_pdf_new.png">',
                                '<a style="color: rgb(51, 56, 77);" href='+docUrl+' title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,30)+'</a>',
                                '<span id="sortDate" value='+isoDate+'>'+docDate+'</span>',
                                '<span title='+sharedBy.replace(/\s/g, '&nbsp;')+'>'+getTextLength(sharedBy,20)+'</span>',
                                '<a href=' + docUrl + ' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;border: 3px solid #03a2ea;">  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Read &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a>'
                            ]);
                        }
                    }
                }else{

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // Functions to get new invitations & count
    function getNewInvitationsCount() {
        $.ajax({
            url: '/newInvitationsCount',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {  // result contains invitations count and new invitations array

                if (result.invitations.length > 0) {
                    var today = new Date();
                    today.setHours(0,0,0,0);
                    var invitations = removeDuplicates_id(result.invitations);

                    for(var invi=0; invi<invitations.length; invi++){
                        var maxSlot = invitations[invi].scheduleTimeSlots.reduce(function (a, b) {
                            return new Date(a.start.date) > new Date(b.start.date) ? a : b;
                        });
                        if(new Date(maxSlot.start.date) >= today){
                            var title = maxSlot.title;
                            var invitationUrl = '/meeting/'+invitations[invi].invitationId;
                            var acceptUrl = '/meeting/accept/'+invitations[invi].invitationId;
                            var requestDate = getDateFormat(invitations[invi].scheduledDate);
                            var sentBy = invitations[invi].senderName;
                            var addRow = true;
                            if(invitations[invi].suggested && invitations[invi].suggestedBy){
                                if(invitations[invi].suggestedBy.userId != userProfile._id){
                                    if(invitations[invi].suggestedBy.userId == invitations[invi].senderId){

                                    }else{
                                        if(invitations[invi].selfCalendar){
                                            var rFName = invitations[invi].toList[0].receiverFirstName || '';
                                            var rLName = invitations[invi].toList[0].receiverLastName || '';

                                            sentBy = rFName+' '+rLName;
                                        }else{
                                            sentBy = invitations[invi].to.receiverName;
                                        }
                                    }
                                }else addRow = false;
                            }else {
                                if(invitations[invi].senderId == userProfile._id){
                                    addRow = false;
                                }
                            }

                            if(addRow){
                                updateNotificationCount(1);
                                addRowsToTable([
                                    '<img src="/images/icon_calendar_red.png">',
                                    '<a style="color: rgb(51, 56, 77);" href='+invitationUrl+' title='+title.replace(/\s/g, '&nbsp;')+'>'+getTextLength(title,30)+'</a>',
                                    '<span id="sortDate" value='+invitations[invi].scheduledDate+'>'+requestDate+'</span>',
                                    '<span title='+sentBy.replace(/\s/g, '&nbsp;')+'>'+getTextLength(sentBy,20)+'</span>',
                                    '<a href=' + acceptUrl + ' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;border: 3px solid #03a2ea;">  &nbsp;&nbsp; Confirm Meeting &nbsp;&nbsp; </a>'
                                ]);
                            }
                        }
                    }
                }
                else {

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(checkRequired(userProfile) && checkRequired(userProfile.timezone) && checkRequired(userProfile.timezone.name)){
                    userTimezone = userProfile.timezone.name;
                }
                else{
                    var tz = jstz.determine();
                    userTimezone = tz.name();
                }
                getPendingToDo(userTimezone);
                getNewInvitationsCount();
                getNonConfirmedEvents();
                if(result){
                    identifyMixPanelUser(result);
                }
                $('#profilePic').attr("src", result.profilePicUrl);
                imagesLoaded("#profilePic", function (instance, img) {
                    if (instance.hasAnyBroken) {
                        $('#profilePic').attr("src", "/images/default.png");
                    }
                });
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // Function to get user todo items info
    function getPendingToDo(timezone) {
        $.ajax({
            url: '/meetings/todo/pending?filter=pending&timezone='+timezone,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if(result && checkRequired(result) && result.length > 0){
                    for(var i=0; i<result.length; i++){
                        var invitationUrl = '/meeting/'+result[i].invitationId;
                        var requestDate = getDateFormat(result[i].toDo.createdDate);
                        updateNotificationCount(1);
                        addRowsToTable([
                            '<img style="width: 18px" src="/images/todo1.jpg">',
                            '<a style="color: rgb(51, 56, 77);" href='+invitationUrl+' title='+result[i].toDo.actionItem.replace(/\s/g, '&nbsp;')+'>'+getTextLength(result[i].toDo.actionItem,30)+'</a>',
                            '<span id="sortDate" value='+result[i].toDo.createdDate+'>'+requestDate+'</span>',
                            '<span title='+result[i].senderName.replace(/\s/g, '&nbsp;')+'>'+getTextLength(result[i].senderName,20)+'</span>',
                            '<a href='+invitationUrl+' style="color:#FFFFFF !important;background-color: #03a2ea; font-size:12px;border-radius:7px;text-align: center;text-decoration: none;border: 3px solid #03a2ea;">  &nbsp;&nbsp; Go To Meeting &nbsp;&nbsp; </a>'
                        ]);
                    }
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    var element;

    $("body").on("click", "#calPassReq", function () {
        var requestedByUserId = $(this).attr("requestByUserId");
        var requestedByName = $(this).attr("requestByName");
        element = $(this);
        var requestedByEmailId = $(this).attr("requestByEmailId");
        var requestId = $(this).attr("requestId");
        if (popoverSource != null) {
            popoverSource.popover('destroy');
            popoverSource = null;
            return;
        }

        var html = _.template($("#calPass-popup").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'right',
            id: "myPopover"
        });

        $(this).popover('show');
        popoverSource = $(this)
        $("#calPass-submit").attr("requestedByUserId", requestedByUserId);
        $("#calPass-submit").attr("requestedByName", requestedByName);
        $("#calPass-submit").attr("requestedByEmailId", requestedByEmailId);
        $("#calPass-submit").attr("requestId", requestId);

    })

    $("body").on("click", "#closePopup-calPass", function () {
        popoverSource.popover('destroy');
        popoverSource = null;
    })

    $("body").on("change", "#notApproved", function () {
        if ($(this).prop("checked")) {
            var userName = userProfile.firstName;
            var message = 'Sorry, ' + userName + ' has declined to share his calendar password with you, at this point in time.'
            $("#messageCalPass").val(message)
            messageFlag = true;
        }
        else {
            $("#messageCalPass").val('')
        }
    })
    jQuery.nl2br = function (varTest) {
        return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
    };
    $("body").on("change", "#approve", function () {
        if ($(this).prop("checked")) {
            $("#messageCalPass").val('')
            messageFlag = false;
        }

    })

    var messageFlag = false;
    $("body").on("click", "#calPass-submit", function () {
        var requestedByUserId = $(this).attr("requestedByUserId");
        var requestId = $(this).attr("requestId");
        var requestedByName = $(this).attr("requestedByName") || '';
        var requestedByEmailId = $(this).attr("requestedByEmailId");
        requestedByName = requestedByName.replace(/-/g, ' ');
        var calMsg = $("#messageCalPass").val();
        if (messageFlag) {
            calMsg = $.nl2br(calMsg);
        } else if (checkRequired(calMsg)) {
            calMsg = "Message from " + userProfile.firstName + ' ' + userProfile.lastName + " below:\n" + calMsg;
            calMsg = $.nl2br(calMsg);
        }
        var accepted = false;
        if ($("#approve").prop("checked")) {
            accepted = true;
        }
        popoverSource.popover('destroy');
        popoverSource = null;
        if (checkRequired(userProfile.profilePrivatePassword)) {
            var resObj = {
                requestedByUserId: requestedByUserId,
                requestedByName: requestedByName,
                requestedByEmailId: requestedByEmailId,
                publicProfileUrl: userProfile.publicProfileUrl,
                profilePrivatePassword: userProfile.profilePrivatePassword,
                emailId: userProfile.emailId,
                name: userProfile.firstName + ' ' + userProfile.lastName,
                requestId: requestId,
                accepted: accepted,
                msg: calMsg || ''
            };

            if (checkRequired(resObj)) {
                $.ajax({
                    url: '/calendarPasswordResponse',
                    type: 'POST',
                    datatype: 'JSON',
                    data: resObj,
                    success: function (response) {
                        if (response == 'noMsg') {
                            showMessagePopUp("Please provide message.", 'error');
                        } else if (response) {
                            showMessagePopUp("Your response for this request successfully sent.", 'success');
                            removeFlag = true;
                        }
                        else {
                            showMessagePopUp("Error occurred please try again.", 'error');
                        }
                    }
                })
            }
            else showMessagePopUp("Error occurred please try again.", 'error');
        }
        else {
            showMessagePopUp("You didn't set password for your calendar.", 'error');
        }
    });

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }

    function applyDataTable(){
        table = $('#notificationsTable').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "fnDrawCallback": function(oSettings) {

            },
            "oLanguage": {
                "sEmptyTable": "No Notifications"
            },
            "order": [[ 0, "asc" ]],
            "columns": [
                null,
                null,
                { "orderDataType": "dom-value" },
                null,
                null
            ]
        });

    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('#sortDate', td).attr("value");
        } );
    }

    function getTextLength(text,maxLength){

        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    function getDateFormat(date){
        var dateObj = new Date(date)
        return dateObj.getDate()+' '+monthNameSmall[dateObj.getMonth()]+','+dateObj.getFullYear()
    }

    function removeDuplicates_id(arr){

        var end = arr.length;

        for(var i = 0; i < end; i++)
        {
            for(var j = i + 1; j < end; j++)
            {
                if(arr[i]._id == arr[j]._id)
                {
                    var shiftLeft = j;
                    for(var k = j+1; k < end; k++, shiftLeft++)
                    {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for(var i = 0; i < end; i++){
            whitelist[i] = arr[i];
        }
        //whitelist.reverse();
        return whitelist;
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }

    function showMessagePopUp(message, msgClass) {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#closePopup-message", function () {
        $("#user-name").popover('destroy');
        if (removeFlag) {
            removeFlag = false;
            if(checkRequired(table) && checkRequired(element)){
                table.row(element.parents('tr')).remove().draw();
            }
        }
    });
    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }
});
