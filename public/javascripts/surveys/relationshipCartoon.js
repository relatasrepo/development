/**
 * Created by naveen on 1/27/16.
 */


var relationshipCartoonApp = angular.module('relationshipCartoonApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);


relationshipCartoonApp.controller("relationshipCartoon", function($scope,$http){

    $scope.myvalue = false;

    $scope.gameform = true;
    $scope.male = false;
    $scope.female = false;
    $scope.error = false;
    $scope.relatasVideo = false;
    $scope.shareView = "View";
    $scope.errorGender = false;
    $scope.errorName = false;

    $scope.calRelationshipCartoon = function(){

        var mailRandom = [$scope.name + " receives multiple emails a day! \n Most of them are not SPAM!\n"+$scope.name +" is busy ! \n Don't send "+$scope.name+" a mail today !",
            $scope.name + " interacts majorly over emails ! \n"+$scope.name +" is productive ! \n Why don't you send "+$scope.name+" a mail today !",
            $scope.name + " sends more than 30 emails a day ! \n"+$scope.name +" might have sent one to you today. \n Why don't you email "+$scope.name+" today !"];

        var callRandom = [$scope.name + "  spends most time on calls ! \n"+$scope.name +" takes calls everywhere ! \n Give "+$scope.name+" a call now\n" +$scope.name+" has been waiting for your call!",
            $scope.name + " does more than 30 calls a day ! \n"+$scope.name +" might just give you a call today ! \n Why don't you call "+$scope.name+" now!"];

        var meetingRandom = [$scope.name + " does more than 30 meetings a day ! \n We have no clue who " +$scope.name + " meets ! \n Even "+$scope.name+" doesn't know who he meets ! \n Don't send "+$scope.name+" a meeting invite today!"];

        var randomGuess = [$scope.name + " interacts majorly over emails ! \n"+$scope.name +" is productive ! \n Why don't you send "+$scope.name+" a mail today!"
        ];

        var gender = $scope.gender;

        if($scope.name && $scope.gender){

            $scope.myvalue = true;
            $scope.gameform = false;
            $scope.error = false;
            $scope.relatasVideo = true;
            $scope.shareView = "Share";
            $scope.errorGender = false;
            $scope.errorName = false;

            if(gender == "male"){
                $scope.male = true;
                $scope.female = false;
            } else if(gender == "female"){
                $scope.female = true;
                $scope.male = false;
            }

        } else {

             if(!$scope.name) {
                $scope.error = false;
                $scope.errorGender = false;
                $scope.errorName = true;
            } else if(!$scope.gender){
                $scope.error = false;
                $scope.errorGender = true;
                $scope.errorName = false;
            }
            $scope.myvalue = false;
            $scope.gameform = true;
        }

        if(($scope.emailsNumber>30) && ($scope.callsNumber<=30 || $scope.meetingsNumber<=30)){
            $scope.maxUse = 'Emails';
            $scope.message = mailRandom[Math.floor(Math.random() * mailRandom.length)];
            $scope.facebook = encodeURI($scope.message);
            if(gender == "male"){
                $scope.img = "http://relatas.com/images/maleV1.png";

            } else {
                $scope.img = "http://relatas.com/images/femaleV1.png";
            }

        } else if (($scope.callsNumber>30) && ($scope.emailsNumber<=30 || $scope.meetingsNumber<=30)) {
            $scope.maxUse = 'Phone calls'
            $scope.message = callRandom[Math.floor(Math.random() * callRandom.length)];
            $scope.facebook = encodeURI($scope.message);
            if(gender == "male"){
                $scope.img = "http://relatas.com/images/maleV1.png";

                } else {
                $scope.img = "http://relatas.com/images/femaleV1.png";
                }

        } else if (($scope.meetingsNumber>30) && ($scope.emailsNumber<=30 || $scope.callsNumber<=30)) {

            $scope.maxUse = 'Meetings'
            $scope.message = meetingRandom[Math.floor(Math.random() * meetingRandom.length)];


            $scope.facebook = encodeURI($scope.message);
            if(gender == "male"){
                $scope.img = "http://relatas.com/images/maleV1.png";
            } else {
                $scope.img = "http://relatas.com/images/femaleV1.png";
            }
        } else {
            $scope.maxUse = 'Emails'
            $scope.message = randomGuess[Math.floor(Math.random() * randomGuess.length)];

            $scope.facebook = encodeURI($scope.message);
            if(gender == "male"){
                $scope.img = "http://relatas.com/images/maleV1.png";
            } else {
                $scope.img = "http://relatas.com/images/femaleV1.png";
            }
        }


        mixpanel.track('Relationship Cartoon', {
            'Name' : $scope.name,
            'Email' : $scope.email,
            'Gender': $scope.gender,
            'Number of Emails':$scope.emailsNumber,
            'Number of Calls': $scope.callsNumber,
            'Number of Meetings':$scope.meetingsNumber
        });


        var obj = {};
        obj.name = $scope.name;
        obj.email = $scope.email;
        obj.gender = $scope.gender;
        obj.emailsNumber = $scope.emailsNumber;
        obj.callsNumber = $scope.callsNumber;
        obj.meetingsNumber = $scope.meetingsNumber;

        $http.post('/admin/survey/relationshipCartoon',obj)
            .success(function(response){
                // console.log(response);
            });

        //
        //console.log(obj);
        //
        //$http.post("/relationship/cartoon/data",obj)
        //    .success(function(response){
        //        if(response.SuccessCode){
        //
        //        }
        //        else {
        //            toastr.error("Error! Hashtag not Added.");
        //        }
        //    });

    }


    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

});

