function fetchCommitForQuarter($scope,$http,share,$rootScope,type,currentQtrSelection,user,navigating){

    if(share && share.userId){
        $rootScope.editAccess = share.userId == user.userId
    }

    var startDate = null;
    if(currentQtrSelection){
        startDate = currentQtrSelection.start
    } else {
        setQuarterDateRange($scope,$http,share);
    }

    if(!navigating){
        $scope.graphLoading = true
        setTimeOutCallback(2000,function () {
            quarterlyCommitHistory($scope,$rootScope,$http,share,user.userId);
        })
    }

    fetchCurrentQtrCommits($scope,$http,share,$rootScope,user.userId,startDate)
}

function fetchTeamCommitForQuarter($scope,$http,share,$rootScope,users,startDate) {

    var url = '/review/commits/quarter/team';

    if(users){
        url = fetchUrlWithParameter(url,"hierarchylist",users)
    }

    if(startDate){
        url = fetchUrlWithParameter(url,"startDate",startDate)
    }

    if(!share.quarterCommitCutOff){
        share.setQuarterDateRange();
    }

    $http.get(url)
        .success(function (response) {

            $scope.quarterlyCommits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                var selfCommitValue = 0;

                if(response.commitCurrentQuarter.quarter.userCommitAmount){
                    selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount;
                }

                var oppsToDisplay = response.opps;

                if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                    oppsToDisplay = response.commitCurrentQuarter.opportunities
                }

                var oppValue = getOppValInCommitStageForSelectedQtr(oppsToDisplay,response.commitQuarter,share.primaryCurrency,share.currenciesObj)

                share.setDateRange(response.commitQuarter.start,moment(response.commitQuarter.start).add(2,"month"),"qtr")

                $scope.quarterlyCommits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                getTargetsAndAchievements($http,users,startDate,"qtr",function (insights) {
                    if(insights){
                        $scope.quarterlyCommits.graph = setGraphValues(insights,response,share,"qtr");
                    }
                })
            }
        });
}

function fetchCurrentQtrCommits($scope,$http,share,$rootScope,userId,startDate){
    var url = '/review/commits/quarter';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    if(startDate){
        url = fetchUrlWithParameter(url,"startDate",startDate)
    }

    $http.get(url)
        .success(function (response) {

            $scope.quarterlyCommits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                var editAccess = $rootScope.editAccess;

                if(response.editAccess && $rootScope.editAccess) {
                    editAccess = true;
                } else {
                    editAccess = false;
                }

                var oppsToDisplay = response.opps;

                if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                    oppsToDisplay = response.commitCurrentQuarter.opportunities
                }

                var selfCommitValue = 0;

                if(response.commitCurrentQuarter.quarter.userCommitAmount){
                    selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount;
                }

                var oppValue = getOppValInCommitStageForSelectedQtr(oppsToDisplay,response.commitQuarter,share.primaryCurrency,share.currenciesObj)

                $scope.quarterlyCommits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                getTargetsAndAchievements($http,userId,startDate,"qtr",function (insights) {
                    if(insights){
                        $scope.quarterlyCommits.graph = setGraphValues(insights,response,share,"qtr");
                    }
                })
            }

        })
}

function setQuarterDateRange($scope,$http,share){

    var url = "/review/get/quarter/cutoff"
    $http.get(url)
        .success(function (response) {
            $scope.currentQtrSelection = {
                start:response.startOfQuarter,
                end:response.endOfQuarter
            }

            share.quarterCommitCutOff = response;

            var start = share.quarterCommitCutOff.startOfQuarter,
                end = share.quarterCommitCutOff.endOfQuarter

            $scope.dataForRange = moment(start).format('MMM')+"-"+moment(moment(start).add(1,"month")).format('MMM')+"-"+moment(moment(start).add(2,"month")).format('MMM YY');
        });
}

function initializeQuarterlyCommitScopesAndMethods($scope,$rootScope,$http,share){

    $scope.saveQuarterlyCommits = function (commits) {

        if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
            $http.post("/review/meeting/update/commit/value/quarterly",{commitValue:commits.selfCommitValue})
                .success(function (response) {
                    if(response.SuccessCode){
                        toastr.success("Commits successfully updated")
                        quarterlyCommitHistory($scope,$rootScope,$http,share)
                    } else {
                        toastr.error("Commits not updated. Please try again later")
                    }
                });
        } else {
            toastr.error("Please enter only positive numbers for opportunity amount")
        }
    }

    $scope.updateQuarterlyStoreValue = function (value) {
        value.selfCommitValue = parseFloat(value.selfCommitValueFormat)
    }

    $scope.showOppsQuarterly = function (stage) {
        closeAllOppTables($scope);

        $scope.weekSelection = stage.rangeFormatted;
        $scope.currentWeek = true;
        if(share.commitStage){
            $rootScope.commitStage = share.commitStage;
        } else if(stage.response.commitStage){
            $rootScope.commitStage = stage.response.commitStage;
        }

        $scope.stageSelection = $rootScope.commitStage
        paintOppsTableQtr($scope,$rootScope,$http,share,stage.opps,stage.response.commitQuarter)
    }
}

function navigateQuarterlyCommits($scope,$http,share,$rootScope,currentQtrSelection,type,member) {

    var start = share.quarterCommitCutOff.startOfQuarter,
        end = share.quarterCommitCutOff.endOfQuarter

    if(type == 'prev' && currentQtrSelection){
        start = moment(currentQtrSelection.start).subtract(3,"month")
        end = moment(currentQtrSelection.end).subtract(3,"month")
    }

    if(type == 'next' && currentQtrSelection){
        start = moment(currentQtrSelection.start).add(3,"month")
        end = moment(currentQtrSelection.end).add(3,"month")
    }

    var qtrCommitCutOff = new Date(share.quarterCommitCutOff.startOfQuarterCutOff);
    if(qtrCommitCutOff<new Date()){
        qtrCommitCutOff = new Date(moment(qtrCommitCutOff).add(3,"month"))
    }

    if(new Date(start) >= qtrCommitCutOff){
        $scope.nextButtonDisabled = true;
    } else {
        $scope.nextButtonDisabled = false;
    }

    $scope.dataForRange = moment(start).format('MMM')+"-"+moment(moment(start).add(1,"month")).format('MMM')+"-"+moment(moment(start).add(2,"month")).format('MMM YY');

    $scope.currentQtrSelection = {
        start:moment(start).toISOString(),
        end:moment(end).toISOString()
    }

    if($scope.viewingTeamData){
        share.fetchTeamCommitForQuarter(member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[],new Date(start))
    } else {
        fetchCommitForQuarter($scope,$http,share,$rootScope,type,$scope.currentQtrSelection,member,"navigating")
    }
}

function quarterlyCommitHistory($scope,$rootScope,$http,share,userId){

    var url = '/review/quarterly/commits/history';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    $http.get(url)
        .success(function (response) {
            $scope.actionloadingData = false;
            var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.commitStage;
            commitColName = commitColName+" stage ($)"
            var relatasCommitCol = [commitColName],
                selfCommitCol = ['Self commit'],
                oppCreatedCol = ['Opp created (count)'],
                oppWonAmountCol = ['Opp won ($)'],
                oppWonCol = ['Opp won (count)'],
                commitsMadeObj = {}

            response.commits.sort(function (o1, o2) {
                return new Date(o1.commitForDate) > new Date(o2.commitForDate) ? 1 : new Date(o1.commitForDate) < new Date(o2.commitForDate) ? -1 : 0;
            });

            response.oppsConversion.created.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.oppsConversion.oppsWon.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            _.each(response.commits,function (el,index) {
                commitsMadeObj[index] = el;
                selfCommitCol.push(el.quarter.userCommitAmount)
                relatasCommitCol.push(el.quarter.relatasCommitAmount)
            });

            _.each(response.oppsConversion.created,function (el) {
                oppCreatedCol.push(el.count)
            });

            _.each(response.oppsConversion.oppsWon,function (el) {
                oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
                oppWonCol.push(el.count)
            });

            setTimeOutCallback(100,function () {
                $scope.graphLoading = false;
            })

            var colors = {
                "Self commit": '#638ca6',
                "Opp created (count)": '#3498db',
                'Opp won (count)': '#8ECECB',
                'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
            }

            colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

            var axes = {
            }

            axes[commitColName] = 'y2'
            axes["Opp won ($)"] = 'y2'
            axes["Self commit"] = 'y2'

            var chart = c3.generate({
                bindto: ".pipeline-comparison",
                data: {
                    // columns: [oppWonCol,oppCreatedCol,relatasCommitCol,selfCommitCol,oppWonAmountCol],
                    columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol],
                    axes: axes,
                    type: 'bar',
                    types: {
                        'Opp created (count)': 'spline',
                        "Opp won (count)": 'spline'
                    },
                    colors:colors,
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    onclick: function(e) {
                        $scope.$apply(function () {
                            closeAllOppTables($scope);
                            $rootScope.commitStage = share.commitStage;
                            displayOppsQuarterly(e,$scope,share,response,commitsMadeObj)
                        });
                    }
                },
                axis: {
                    y2: {
                        label: {
                            text:'Opp amount',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    y: {
                        label: {
                            text:'No. of Opps',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    x : {
                        tick: {
                            fit: true
                        }
                    }
                },
                tooltip: {
                    format: {
                        title: function (d) {
                            if(commitsMadeObj[d]){
                                var startDate = commitsMadeObj[d].commitForDate
                                var dateRange = moment(startDate).format('MMM')+"-"+moment(moment(startDate).add(1,"month")).format('MMM')+"-"+moment(moment(startDate).add(2,"month")).format('MMM YY')
                                return dateRange;
                            }
                        }
                    }
                }
            });
        })
}

function displayOppsQuarterly(event,$scope,share,response,commitsMadeObj){

    $scope.opps = [];
    $scope.currentWeek = false;
    $scope.weekByWeek = true;
    $scope.commitsByUsers = [];
    $scope.loadOppsForWeek = true;
    $scope.weekByWeek = true;
    $scope.loadingOpps = true;

    var opps = commitsMadeObj[event.index].opportunities

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.ngmReq = response.netGrossMarginReq;
            op.amountWithNgm = op.amountNonNGM?op.amountNonNGM:op.amount;

            if(response.netGrossMarginReq){
                op.amountWithNgm = (op.amountWithNgm*op.netGrossMargin)/100
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment(commitsMadeObj[event.index].commitForDate))) && new Date(op.closeDate) <= (new Date(commitsMadeObj[event.index].commitForDate))){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            if(response.commitStage == op.stageName){
                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2);

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

function initializeQuarterlyCommitScopesAndMethodsForTeam($scope,$http,share,$rootScope) {
    share.fetchTeamCommitForQuarter = function (users,startDate) {
        fetchTeamCommitForQuarter($scope,$http,share,$rootScope,users,startDate)
    }

    $scope.showOppsQuarter = function (stage) {
        closeAllOppTables($scope);

        $scope.weekSelection = stage.rangeFormatted;
        $scope.currentWeek = true;
        $rootScope.commitStage = share.commitStage;
        $scope.stageSelection = $rootScope.commitStage

        paintOppsTableQtr($scope,$rootScope,$http,share,stage.opps,stage.response.commitQuarter)
    }

    $scope.getTeamCommitsQuarter = function (data) {

        $scope.selectionType = data.dateRange;
        var startDate = moment(data.response.commitQuarter.start).startOf("month"),
            end = moment(moment(startDate).add(2,"month")).endOf("month");

        $scope.commitsByUsers = [];
        $scope.currentWeek = false;
        var userHashMap = share.companyUsersDictionary;
        var oppList = data.opps;

        var opportunitiesObj = {};

        if(oppList && oppList.length>0){
            _.each(oppList,function (op) {
                if(opportunitiesObj[op.userEmailId]){
                    opportunitiesObj[op.userEmailId].push(op)
                } else {
                    opportunitiesObj[op.userEmailId] = [];
                    opportunitiesObj[op.userEmailId].push(op)
                }
            })
        }

        if(data.response.commitsCurrentRaw && data.response.commitsCurrentRaw.length>0){

            _.each(data.response.commitsCurrentRaw,function (el) {

                el.profile = userHashMap[el.userId];
                el.value = el.quarter.userCommitAmount
                if(isNumber(parseFloat(el.value))){
                    el.valueSort = parseFloat(el.value)
                }

                el.value = getAmountInThousands(el.value,2)
                el.emailId = el.profile.emailId;
                el.commitDateTime = moment(el.date).format(standardDateFormat());
                el.oppsValueUnderCommitStage = 0;
                el.oppsValueUnderCommitStageSort = 0;

                $scope.commitsByUsers.push(el)
            })
        }

        var nonExistingUsers = _.differenceBy(share.teamList,_.map($scope.commitsByUsers,"emailId"))
        if(nonExistingUsers && nonExistingUsers.length>0){
            _.each(nonExistingUsers,function (el) {
                $scope.commitsByUsers.push({
                    value:0,
                    valueSort:0,
                    profile:share.team[el],
                    commitDateTime:"-",
                    oppsValueUnderCommitStage:0,
                    oppsValueUnderCommitStageSort:0
                })
            })
        }

        $scope.commitValueSelected = 0;
        $scope.oppValueSelected = 0;
        _.each($scope.commitsByUsers,function (el) {

            var oppsListForUser = [];
            if(opportunitiesObj[el.profile.emailId]){
                oppsListForUser = opportunitiesObj[el.profile.emailId]
            }

            if(oppsListForUser.length>0){
                _.each(oppsListForUser,function (op) {
                    if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                        if(op.amount && isNumber(el.oppsValueUnderCommitStageSort)){
                            op.oppForCommit = true;
                            el.oppsValueUnderCommitStageSort = el.oppsValueUnderCommitStageSort+op.amount
                        }
                    }
                })
            }

            el.oppsValueUnderCommitStage = getAmountInThousands(el.oppsValueUnderCommitStageSort,2);
            el.selected = true;
            $scope.selectedUserValues(el);
        })

    }
}

function getOppValInCommitStageForSelectedQtr(opps,commitDt,primaryCurrency,currenciesObj) {

    var oppVal = 0,
        startDate = moment(commitDt.start).startOf("month"),
        end = moment(moment(startDate).add(2,"month")).endOf("month");

    _.each(opps,function (op) {
        op.convertedAmt = op.amountNonNGM;
        op.convertedAmtWithNgm = op.amount

        if(op.currency && op.currency !== primaryCurrency){
            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amountNonNGM/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            } else {
                op.convertedAmtWithNgm = op.convertedAmt;
            }

            op.convertedAmt = parseFloat(op.convertedAmt)
        }
        if(new Date(op.closeDate)>=new Date(startDate) && new Date(op.closeDate)<=new Date(end)){
            oppVal = oppVal+op.convertedAmtWithNgm;
        }
    })

    return oppVal

}

function paintOppsTableQtr($scope,$rootScope,$http,share,opps,commitDt) {

    $scope.opps = [];
    $scope.currentWeek = true;
    $scope.commitsByUsers = [];

    var startDate = moment(commitDt.start).startOf("month"),
        end = moment(moment(startDate).add(2,"month")).endOf("month");

    if(opps && opps.length>0){
        _.each(opps,function (op) {
            op.topLine = op.amountNonNGM?getAmountInThousands(op.amountNonNGM,2,share.primaryCurrency=="INR"):getAmountInThousands(op.amount,2,share.primaryCurrency=="INR")
            op.amountWithNgm = op.amount

            if(!op.amountNonNGM && (op.netGrossMargin || op.netGrossMargin == 0)){
                op.amountWithNgm = parseFloat(((op.amount*op.netGrossMargin)/100).toFixed(2))
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2);
            op.age = moment().diff(moment(op.createdDate), 'day');
            op.closeDateSort = moment(op.closeDate).unix()
            op.closeDate = moment(op.closeDate).format(standardDateFormat());
            op.account = fetchCompanyFromEmail(op.contactEmailId)
            op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

            $scope.opps.push(op)
        })
    }
}