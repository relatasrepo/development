var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash','ngCsv','textAngular']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){

            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope,$http,share,$rootScope) {

    getLiuProfile($scope, $http, share,$rootScope,function (response) {

        if(response.SuccessCode) {
            
            if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                timezone = response.Data.timezone.name;
            }

            var fyMonth = "April";
            if (response.companyDetails.fyMonth) {
                fyMonth = response.companyDetails.fyMonth
            }

            response.Data.fyMonth = fyMonth;

            share.loadInsights();
            share.liuDetails(response.Data);
            share.setUserId(response.Data._id);
            share.setUserEmailId(response.Data.emailId);
            share.companyDetails = response.companyDetails
            share.setCompanyDetails(response.companyDetails, response.Data)
        }
    })

});

relatasApp.controller("topContextMenu", function ($scope, $http,$rootScope,share){

    $scope.toggleHierarchyList = function () {
        $scope.showHierarchyList = !$scope.showHierarchyList
    }

    share.setCompanyDetails = function (companyDetails,liu) {

        $scope.hierarchyList = ["Org. Hierarchy"]

        if(companyDetails && companyDetails.isRevenueHierarchySetup){
            $scope.hierarchyList = ["Org. Hierarchy","Rev. Hierarchy"]
        }

        $rootScope.hierarchySelection = $scope.hierarchyList[0];

        $scope.liu = liu;

        var accessControlProd = [],accessControlRegion = [],accessControlVertical = [];

        if(share.liuData){
            _.each(share.liuData.productTypeOwner,function (el) {
                accessControlProd.push({name:el})
            })
            _.each(share.liuData.verticalOwner,function (el) {
                accessControlVertical.push({name:el})
            })
            _.each(share.liuData.regionOwner,function (el) {
                accessControlRegion.push({region:el})
            })
        }

        $rootScope.currency = 'USD';
        
        if(!$scope.companyDetails){
            $scope.companyDetails = companyDetails;
        }

        if(companyDetails && companyDetails.currency){

            if(companyDetails && companyDetails.currency){

                _.each(companyDetails.currency,function (el) {
                    if(el.isPrimary){
                        $rootScope.currency = el.symbol
                    }
                })
            }
        }

        if(companyDetails && companyDetails.productList && companyDetails.productList.length>0){
            share.productList(companyDetails.productList)

        } else {
            share.productList(accessControlProd)
        }

        if(companyDetails && companyDetails.sourceList && companyDetails.sourceList.length>0){
            share.sourceList(companyDetails.sourceList)
        }

        if(companyDetails && companyDetails.geoLocations && companyDetails.geoLocations.length>0){
            share.geoLocations(companyDetails.geoLocations)

        } else {
            share.geoLocations(accessControlRegion)
        }

        if(companyDetails && companyDetails.verticalList && companyDetails.verticalList.length>0){
            share.verticalList(companyDetails.verticalList)
        } else {
            share.verticalList(accessControlVertical)
        }
    }

    share.productList = function (productList) {
        $scope.disableProductList = 'disable';
        if(productList && productList.length>0){
            $scope.disableProductList = '';
        }
        share.displayProducts(productList)
    }

    share.displayProducts = function (productList) {
        $scope.productTypes = _.cloneDeep(productList);
        $scope.productTypes.push({
            name:"All products"
        })
    }

    share.verticalList = function (verticalList) {
        $scope.disableVerticalList = 'disable';
        if(verticalList && verticalList.length>0){
            $scope.disableVerticalList = '';
        }

        share.displayVertical(verticalList)
    }

    share.displayVertical = function (verticalList) {
        $scope.verticals = _.cloneDeep(verticalList);
        $scope.verticals.push({
            name:"All verticals"
        })
    }

    share.geoLocations = function (geoLocations) {
        $scope.geoLocations = _.cloneDeep(geoLocations);

        $scope.geoLocations.push({
            region:$scope.locationSelected
        })
    }

    share.sourceList = function (sourceList) {
        $scope.disableProductList = 'disable';
        if(sourceList && sourceList.length>0){
            $scope.disableProductList = '';
        }

        share.displaySource(sourceList)
    }

    share.displaySource = function (sourceList) {
        $scope.sources = _.cloneDeep(sourceList);
        $scope.sources.push({
            name:"All sources"
        })
    }

    $scope.switchHierarchy = function (filter) {
        $rootScope.hierarchySelection = filter;
        filter = filter.toLowerCase();

        if(_.includes(filter,"rev")){
            $scope.getTeam("/company/user/revenue/hierarchy")
        } else if(_.includes(filter,"prod")){
            $scope.getTeam("/company/user/product/hierarchy")
        } else {
            $scope.getTeam()
        }
        $scope.showHierarchyList = false;
    }

    var allProfiles= null;
    $scope.getTeam = function (url) {
        $scope.ifProducts = false;

        if(!url){
            url = '/company/user/hierarchy'
        }

        $scope.ifTeam = true;

        $http.get(url)
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data,response.listOfMembers)

                    if($scope.team.length == 1){
                        $scope.selection = $scope.team[0];
                    } else {

                        $scope.selection = {
                            nameNoImg:"All",
                            fullName: "All Team Members",
                            noPicFlag:true
                        }
                    }

                    if($rootScope.orgHead){

                        $scope.selection = {
                            nameNoImg:"All",
                            fullName: "All Team Members",
                            noPicFlag:true
                        }
                    }

                    share.setUserEmailId($scope.selection.emailId)

                    var usersDictionary = {};

                    if(response.companyMembers.length>0){
                        allProfiles = buildTeamProfilesWithLiu_team(response.companyMembers)
                        _.each(allProfiles.team,function (member) {
                            usersDictionary[member.emailId] = member
                        })
                    }

                    $scope.usersDictionary = usersDictionary;

                    function checkLiuData(){
                        if(share.liuData){

                            var tz = share.liuData.timezone && share.liuData.timezone.name?share.liuData.timezone.name:"UTC";
                            var qtrDetailsObj = setQuarter(share.companyDetails.fyMonth,tz);

                            $scope.autoQuarter = qtrDetailsObj.currentQuarter;

                            $scope.quarters = [];
                            _.each(qtrDetailsObj.array,function (qtr,index) {
                                if(new Date(qtr.start)<= new Date()) {
                                    $scope.quarters.push({
                                        quarter:"quarter"+(index+1),
                                        name:moment(qtr.start).format("MMM")+"-"+moment(qtr.end).format("MMM") +" "+moment(qtr.start).format("YYYY"),
                                        start:qtr.start,
                                        end:qtr.end
                                    })
                                }
                            });

                            $scope.quarters.sort(function (o1, o2) {
                                return new Date(o1.start) > new Date(o2.start) ? 1 : new Date(o1.start) < new Date(o2.start) ? -1 : 0;
                            });

                            var firstQtr = $scope.quarters[0].start;
                            var prevQtr = new Date(moment(firstQtr).subtract(1,"year"))

                            $scope.quarters.push({
                                start:prevQtr,
                                name:"FY- "+moment(prevQtr).format("YYYY"),
                                quarter:"PrevFY"
                            })

                            _.each($scope.quarters,function (q) {
                                if(q.quarter == $scope.autoQuarter){
                                    $scope.quarter = q
                                    $scope.quarterSelected = q.name
                                }
                            });

                            share.teamMembers = $scope.team;
                            share.teamMembersDictionary = usersDictionary;

                            share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,$scope.autoQuarter)

                        } else {
                            setTimeOutCallback(1000,function () {
                                checkLiuData();
                            })
                        }
                    }

                    checkLiuData();
                    share.setTeamMembers(usersDictionary);
                } else {
                    share.resetTeamRank()
                }
            });
    }

    share.loadInsights = function () {
        $scope.getTeam();
    }
    
    $scope.users = [];

    $scope.getOpportunities = function (user,filter) {
        $scope.teamRankLoading = true;
        $scope.team = share.teamMembers;

        $scope.selectFromList = false;
        if(user == 'all'){
            $scope.selection = {
                nameNoImg:"All",
                fullName: "All Team Members",
                noPicFlag:true
            }

        } else {
            $scope.selection = user
        }

        if(user == "all"){

            share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,$scope.quarter.quarter)
        } else {

            var userHashMap = share.teamMembersDictionary;
            $scope.team = share.teamMembers;
            var selectedUser = userHashMap[user.emailId]
            share.showUsers([selectedUser],selectedUser,allProfiles.allCompanyUserIds,$scope.quarter.quarter,"exceptionalAccess")
        }
    }

    $scope.usersWithTargets = "With targets";
    $scope.allUsers = "All users";
    $scope.withTargetSet = "With targets";

    $scope.showAll = function (selection) {
        $scope.showAllList = false;
        $scope.usersWithTargets = selection;
        if(selection == $scope.allUsers){
            $scope.targetClass = "active";
            share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,$scope.quarter.quarter,null,null,location,vertical,product,true)
        } else {
            $scope.targetClass = "";
            share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,$scope.quarter.quarter,null,null,location,vertical,product,false)
        }
    }
    
    $scope.getOpportunitiesForQuarter = function (quarter) {
        $scope.showQuarterList = false;
        $scope.quarterClass = "active";
        $scope.quarter = quarter;
        $scope.quarterSelected = quarter.name;
        $scope.team = share.teamMembers;

        var withTargets = $scope.usersWithTargets == $scope.allUsers?true:false;
        share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,quarter.quarter,null,null,location,vertical,product,withTargets)
    }
    
    share.resetFiltersToDefault = function () {
        $scope.productSelected = "All products";
        $scope.verticalSelected = "All verticals";
        $scope.locationSelected = "All regions";
        $scope.dateRange = "All close dates";
        $scope.companySelected = "All companies";
        $scope.contactSelected = "All contacts";
        $scope.sourceSelected = "All sources";
    }

    share.resetFiltersToDefault();

    var location = null,vertical = null,product = null,source = null;
    $scope.filterOpportunities = function (item,filterType) {

        $scope.team = share.teamMembers;

        if(filterType === "vertical"){
            $scope.verticalSelected = item.name;
            $scope.showVerticalList = false;
        }

        if(filterType === "product"){
            $scope.productSelected = item.name;
            $scope.showProductList = false;
        }

        if(filterType === "location"){
            $scope.locationSelected = item.region;
            $scope.showLocationList = false;
        }

        if(filterType === "company"){
            $scope.companySelected = item.name;
            $scope.showCompanyList = false;
        }

        if(filterType === "source"){
            $scope.sourceSelected = item.name;
            $scope.showSourceList = false;
        }

        location = $scope.locationSelected;
        vertical = $scope.verticalSelected;
        product = $scope.productSelected;
        source = $scope.sourceSelected;

        if($scope.verticalSelected == "All verticals"){
            vertical = null;
            $scope.verticalClass = "";
        } else {
            $scope.verticalClass = "active"
            vertical = $scope.verticalSelected;
        }

        if($scope.productSelected == "All products"){
            product = null;
            $scope.productClass = ""
        } else {
            product = $scope.productSelected;
            $scope.productClass = "active"
        }

        if( $scope.locationSelected == "All regions"){
            location = null;
            $scope.locationClass = ""
        } else {
            location = $scope.locationSelected;
            $scope.locationClass = "active"
        }

        if( $scope.sourceSelected == "All sources"){
            source = null;
            $scope.sourceClass = ""
        } else {
            source = $scope.sourceSelected;
            $scope.sourceClass = "active"
        }

        var withTargets = $scope.usersWithTargets == $scope.allUsers?true:false;
        share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,$scope.quarter.quarter,null,null,location,vertical,product,withTargets,source);

    };

    closeAllDropDownsAndModals($scope,".list");

    $scope.showAllDateRange = function () {
        $scope.dateRange = "All close dates";
        $scope.showDateRangePicker = false;
    }
    
    $scope.registerDatePickerId = function(id){

        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            // minDate: $scope.startDt && id == "endDt"?new Date($scope.startDt):new Date(),
            onSelectDate: function (dp, $input){

                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                
                $scope.$apply(function () {
                    $scope[id] = moment(dp).tz(timezone).format("DD MMM YYYY");
                    $scope.dateRange = "Before "+$scope[id]
                    $scope.showDateRangePicker = false;
                    $scope.productSelected = "All products";
                    $scope.locationSelected = "All regions";
                });
            }
        });
    }
    
    share.displayCompanies = function (companies) {
        companies.unshift({name:"All companies"})
        $scope.companies = _.unionBy(companies,'name')
    }

    share.displayContacts = function (contacts) {
        $scope.contacts = _.unionBy(contacts,"emailId");
        $scope.contacts.unshift({name:"All contacts",emailId:"All contacts"})
    }

    $scope.arrayToConvert = [{a: 1, b:2}, {a:3, b:4}, {a:3, b:4, c:4}]
    // $scope.arrayToConvert = share.forCsv
    $scope.csvHeaderList = ["UserEmail","Target","Achievment","#Opp Closed","Avg Deal Size","Avg Sales Cycle","Inter/ $won","Inter with Imp Contacts","Overall Interactions"]
    $scope.csvFilename = "team_rank";

    $scope.convertToCsv = function () {
        // console.log(share.forCsv)
    }

});

relatasApp.controller("team_summary", function ($scope,$http,$rootScope,share){

    share.initSummaryGraph = function (companies,products,verticals,regions) {

        var companyData = groupAndChainForTeamSummary(companies)
        var productData = groupAndChainForTeamSummary(products)
        var verticalData = groupAndChainForTeamSummary(verticals)
        var regionData = groupAndChainForTeamSummary(regions)

        if(companyData.length>0){
            $scope.companyAmount = getAmountInThousands(_.sumBy(companyData,"amount"),2);
        }

        if(regionData.length>0){
            $scope.regionAmount = getAmountInThousands(_.sumBy(regionData,"amount"),2);
        }

        if(productData.length>0){
            $scope.productAmount = getAmountInThousands(_.sumBy(productData,"amount"),2);
        }

        if(verticalData.length>0){
            $scope.verticalAmount = getAmountInThousands(_.sumBy(verticalData,"amount"),2);
        }

        summaryChart(companyData,".company-chart",shadeGenerator(48,101,154,companyData.length,40));
        summaryChart(regionData,".region-chart",shadeGenerator(71,89,129, regionData.length,20));
        summaryChart(productData,".product-chart",shadeGenerator(0,115,76,productData.length,15));
        summaryChart(verticalData,".vertical-chart",shadeGenerator(250,158,70, verticalData.length,20));

        $scope.companies = companyData.length>3?companyData.slice(0,3):companyData;
        $scope.regions = regionData.length>3?regionData.slice(0,3):regionData;
        $scope.products = productData.length>3?productData.slice(0,3):productData;
        $scope.verticals = verticalData.length>3?verticalData.slice(0,3):verticalData;

    }
});

relatasApp.controller("top_stats", function ($scope,$http,$rootScope,share){

    $scope.topStats = [];
    for(var i =0;i < 6;i++){

        var header = "Won";
        var fy = 50;
        var qs = 50;
        var today = 72;
        var className = "high";

        if(i==1){
            today = 8000;
            qs = 7000;
            header = "Pipeline"
        }

        if(i==2){
            today = 32;
            qs = 27;
            header = "Deals at Risk"
            className = "high"
        }

        if(i==3){
            today = 2;
            qs = 5;
            header = "Opp Created"
            className = "high"
        }

        if(i==5){
            today = 12;
            qs = 10;
            header = "Companies Interacted"
        }

        if(i==5){
            today = 17.8;
            qs = 15.8;
            header = "Avg Interactions/Opp"
        }

        if(today>qs){
            className = "low";
        }

        $scope.topStats.push({
            header:header,
            fy:fy,
            qs:qs,
            today:today,
            class:className
        })
    };
})

relatasApp.controller("team_rank", function ($scope,$http,$rootScope,share){

    $scope.sortReverse = false;
    $scope.sortType = "index";
    $scope.teamRankLoading = true;

    share.resetTeamRank = function () {
        $scope.teamRank = []
    }

    share.showUsers = function (team,liuData,allCompanyUserIds,forQuarter,filterType,filter,location,vertical,product,showAll,source) {

        $scope.teamRankLoading = true;
        $scope.teamRank = [];

        var userIds = _.map(team,"userId");
        var url = '/pulse/team/analysis';

        if(liuData.orgHead){
            url = fetchUrlWithParameter(url,"isOrgHead", true)
            url = fetchUrlWithParameter(url,"hierarchylist", allCompanyUserIds)
        } else if(userIds && userIds.length>0){
            url = fetchUrlWithParameter(url,"hierarchylist", userIds)
        }

        url = fetchUrlWithParameter(url,"netGrossMargin", $rootScope.companyDetails.netGrossMargin)
        url = fetchUrlWithParameter(url,"domain", liuData.emailId);

        // forQuarter = "quarter1";

        if(forQuarter){
            url = fetchUrlWithParameter(url,"quarter", forQuarter);
        }

        if(filterType && filterType == "exceptionalAccess"){

            var regionAccess = liuData.regionOwner && liuData.regionOwner.length>0?liuData.regionOwner:null;
            var productAccess = liuData.productTypeOwner && liuData.productTypeOwner.length>0?liuData.productTypeOwner:null;
            var verticalAccess = liuData.verticalOwner && liuData.verticalOwner.length>0?liuData.verticalOwner:null;
            
            url = fetchUrlWithParameter(url,"filterType", filterType);
            if(regionAccess){
                url = fetchUrlWithParameter(url,"regionAccess", regionAccess);
            }

            if(productAccess){
                url = fetchUrlWithParameter(url,"productAccess", productAccess);
            }

            if(verticalAccess){
                url = fetchUrlWithParameter(url,"verticalAccess", verticalAccess);
            }

        }

        if(location){
            url = fetchUrlWithParameter(url,"location", location);
        }

        if(vertical){
            url = fetchUrlWithParameter(url,"vertical", vertical);
        }

        if(product){
            url = fetchUrlWithParameter(url,"product", product);
        }

        if(source){
            url = fetchUrlWithParameter(url,"source", source);
        }

        $http.get(url)
            .success(function (response) {
                $scope.teamRankLoading = false;
                share.forCsv = [];
                $scope.allTeamData = [];
                $scope.quarterRange = response.allQuarters?response.allQuarters:null;

                var companies = [],products = [],verticals = [],regions = [];

                _.each(team,function (tm,i) {

                    var obj = {
                        designation:tm.designation,
                        emailId:tm.emailId,
                        fullName:tm.fullName,
                        image:tm.image,
                        userId:tm.userId,
                        children:tm.children,
                        noPicFlag:true,
                        nameNoImg:tm.fullName.substr(0,2).toUpperCase()
                    };

                    processData(response.Data,obj,i,companies,products,verticals,regions,$scope.quarterRange);

                    if(showAll){
                        $scope.allTeamData.push(obj)
                    } else {
                        if(obj.target || obj.won || (obj.cumulativeData && obj.cumulativeData.wonAmount)){
                            $scope.allTeamData.push(obj)
                        }
                    }

                });

                share.teamList= $scope.allTeamData;

                var labelWithDates = enumerateDaysBetweenDates($scope.quarterRange.start,$scope.quarterRange.end);
                var label = _.map(labelWithDates,"month");
                var order = orderOfQuarterMonths(label);
                share.initSummaryGraph(companies,products,verticals,regions);

                _.each($scope.allTeamData,function (tm,i) {

                    tm.pieChartId = "pie-"+i;
                    tm.pieChartId2 = "pie_2-"+i;
                    tm.pieChartId3 = "pie_3-"+i;
                    tm.lineChart = "line-chart-"+i;
                    tm.lineChart2 = "line-chart-achievement-"+i;
                    tm.lineChart2ToolTip = "team-custom-tooltip-"+i;
                    tm.lineChart3ToolTip = "conver-custom-tooltip-"+i;
                    // tm.showTooltip = true;

                    setTimeOutCallback(100,function () {
                        drawLineChart($scope,share,tm.monthlyCreated,tm.monthlyClosed,"."+tm.lineChart,labelWithDates,label,order);
                    });

                    setTimeOutCallback(100,function () {
                        drawLineChart($scope,share,tm.targetsArray,tm.monthlyOppWon,"."+tm.lineChart2,labelWithDates,label,order,'achievement');
                    });

                    setTimeOutCallback(500,function () {
                        $scope.drawRelevanceChart(tm)
                    });
                })

                $scope.teamRank = getRanking($scope.allTeamData);
            })
    }

    $scope.getDetails = function (member) {
        // share.getDetails(member);
        // share.reviewUser(member);
        // share.getAllTasks(member);
    }

    $scope.displayToolTip = function (member) {
        member.showTooltip = true;

        if(member && member.targetsArray){

            member.targetsArray.sort(function (o1, o2) {
                return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
            });

            _.each(member.targetsArray,function (el) {
                el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
            })
        }

        if(member && member.monthlyOppWon){

            member.monthlyOppWon.sort(function (o1, o2) {
                return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
            });

            _.each(member.monthlyOppWon,function (el) {
                el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
            })
        }
    }

    $scope.displayToolTip2 = function (member) {
        member.showTooltip2 = true;

        if(member && member.monthlyCreated){

            member.monthlyCreated.sort(function (o1, o2) {
                return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
            });

            _.each(member.monthlyCreated,function (el) {
                el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
            })
        }

        if(member && member.monthlyClosed){

            member.monthlyClosed.sort(function (o1, o2) {
                return o1.sort > o2.sort ? 1 : o1.sort < o2.sort ? -1 : 0;
            });

            _.each(member.monthlyClosed,function (el) {
                el.valueWithCommas = numberWithCommas(el.value.r_formatNumber(2));
            })
        }
    }

    $scope.drawAchievementChart = function (member,index,label) {

        function checkId() {
            var ctx = document.getElementById(member.pieChartId2);
            if(ctx){
                pieChart(member,$scope)
            } else {
                setTimeOutCallback(1000,function () {
                    checkId()
                })
            }
        }

        checkId();
    }

    $scope.drawRelevanceChart = function (member,index,label) {

        function isIdPresent() {
            var ctx = document.getElementById(member.pieChartId);
            if(ctx){

                wholePie(member);

            } else {
                setTimeOutCallback(1000,function () {
                    isIdPresent()
                })
            }
        }

        isIdPresent();
    }

});

relatasApp.controller("teamAnalysis",function ($scope,$http,share,$rootScope) {

    var index = 0;
    $scope.dataForQuarter = function (quarter) {
        var url = '/pulse/team/analysis?hierarchylist='+quarter.userId+'&quarter='+quarter.quarter;
        url = fetchUrlWithParameter(url,"netGrossMargin", $rootScope.companyDetails.netGrossMargin);

        setTimeOutCallback(200*index,function () {
            callApi(url,$scope,$http,index++,quarter)
        });

    }

    function initializeQuarters(userId) {
        $scope.quarters = getQuartersArray(userId)
    }

    share.closeCommitView = function () {
        $scope.isSideBarOpen = !$scope.isSideBarOpen
    }

    // $scope.isSideBarOpen = true;

    share.getDetails = function (member) {
        $scope.isSideBarOpen = true;
        initializeQuarters(member.userId);
        $scope.member = member;
        // share.reviewUser(member)
    }

    $scope.close = function () {
        $scope.isSideBarOpen = !$scope.isSideBarOpen
        share.clearAllData();
        // share.clearSelectedMember()
    }

});

function processData(data,obj,i,companies,products,verticals,regions,quarterRange){

    _.each(data,function (el) {

        if(obj.userId == el.userId){

            if(quarterRange){

                obj.monthlyCreated = [];
                obj.monthlyClosed = [];

                if(el.monthlyCreated){
                    obj.monthlyCreated = groupAndChainForCR(el.monthlyCreated,quarterRange)
                }

                if(el.monthlyClosed){
                    obj.monthlyClosed = groupAndChainForCR(el.monthlyClosed,quarterRange)
                }

                obj.conversionRate = "0 %";
                if(obj.monthlyCreated.length>0 && obj.monthlyClosed.length>0){
                    obj.conversionRate = ((_.sumBy(obj.monthlyClosed,'value')/_.sumBy(obj.monthlyCreated,'value'))*100).r_formatNumber(2)+" %"
                }

                if(!obj.monthlyCreated.length && !obj.monthlyClosed.length){
                    obj.conversionRate = "0 %"
                }

                if(!obj.monthlyCreated.length && obj.monthlyClosed.length){
                    obj.conversionRate = "100 %"
                }
            }

            obj.monthlyOppWon = groupAndChainForAchievement (el.monthlyOppWon)
            obj.targetsArray = groupAndChainForAchievement (el.targets)

            if(companies && products && verticals && regions){
                if(el.companies){
                    companies.push(el.companies);
                }

                if(el.products){
                    products.push(el.products);
                }

                if(el.products){
                    verticals.push(el.verticals);
                }

                if(el.regions){
                    regions.push(el.regions);
                }
            }

            obj.target = el.targetsCount;
            obj.won = el.wonAmount;
            obj.wonDeals = el.wonDeals;
            obj.lostAmount = el.lostAmount;
            obj.gap = obj.target - (obj.won + obj.lostAmount);
            obj.interactions = el.interactionsCountAll;
            obj.interactionsCountWon = el.interactionsCountWon;

            obj.pipeline = el.totalOppAmount;

            obj.oneWayInteractionCount = el.oneWayInteractionCount

            obj.interactionsWithOppContacts = el.interactionsOppContactsForQtr;
            obj.oneWayInteractionsWithOppContacts = el.oneWayInteractionsOppContactsForQtr;

            obj.gapNum = obj.gap;
            obj.openValue = el.totalOppAmount-el.wonAmount;

            if(el.wonAmount && el.wonDeals){
                var num = (el.wonAmount/el.wonDeals).r_formatNumber(2)
                obj.averageDealSize = el.wonAmount/el.wonDeals>0?numberWithCommas(num):0;
                obj.averageDealSize_sort = parseFloat(num);
            } else {
                obj.averageDealSize = "0";
                obj.averageDealSize_sort = 0
            }

            if(el.daysToWinCloseDeal){
                obj.avgSalesCycle = (el.daysToWinCloseDeal/el.wonDeals).r_formatNumber(2);
                obj.avgSalesCycle_sort = el.daysToWinCloseDeal/el.wonDeals;
            } else {
                obj.avgSalesCycle_sort = 0;
                obj.avgSalesCycle = "-";
            }

            obj.daysToWinCloseDeal = el.daysToWinCloseDeal

            obj.interactionsOverall = el.allInteractionsCount;

            if(el.contactRelevance){
                obj.contactRelevance = el.contactRelevance;
            }

            if(el.interactionsCountWon && el.wonAmount){
                // el.wonAmount = el.wonAmount/1000
                obj.interactionsPerDeal = (el.interactionsCountWon/(el.wonAmount/1000)).r_formatNumber(2);
            } else {
                obj.interactionsPerDeal = 0;
            }

            if(obj.interactionsPerDeal === "NaN"){
                obj.interactionsPerDeal = 0;
            }
        }

        if(obj.won && obj.target){
            obj.achievementNumber = parseFloat(((obj.won/obj.target)*100).r_formatNumber(2));
            obj.achievementPercentage = obj.achievementNumber+" %";
        } else if(obj.won && !obj.target){
            obj.achievementNumber = 100;
            // obj.achievementPercentage = "100 %";
        } else if(!obj.won && obj.target){
            obj.achievementNumber = 0;
            obj.achievementPercentage = "0 %";
        } else {
            obj.achievementNumber = 0;
            obj.achievementPercentage = "0 %";
        }

        var interactionsNumbers = [],dealNumbers = [],dealNumbersWithoutTarget = [],interactionsAndDeals = [];
        interactionsNumbers.push(obj.interactions)
        interactionsNumbers.push(obj.interactionsOverall)
        interactionsNumbers.push(obj.interactionsCountWon)

        dealNumbers.push(obj.openValue)
        dealNumbers.push(obj.pipeline)
        dealNumbers.push(obj.target)
        dealNumbers.push(obj.won)

        dealNumbersWithoutTarget.push(obj.openValue)
        dealNumbersWithoutTarget.push(obj.pipeline)
        dealNumbersWithoutTarget.push(obj.won)

        var minDeals = Math.min.apply( null, dealNumbers );
        var maxDeals = Math.max.apply( null, dealNumbers );

        var minDeals_v = Math.min.apply( null, dealNumbersWithoutTarget );
        var maxDeals_v = Math.max.apply( null, dealNumbersWithoutTarget );

        obj.wonPercentage = "0 %";
        if(obj.won && obj.pipeline){
            obj.wonPercentage = ((obj.won/obj.pipeline)*100).r_formatNumber(2)+" %"
        }

        if(!obj.target && !obj.won){
            obj.targetStyle = {'width':0+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.wonStyle = {'width':0+'%',background: '#8ECECB',height:'inherit',"max-width":"100%"}
        } else {
            obj.targetStyle = {'width':Math.abs(scaleBetween(obj.target,1,obj.won))+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.wonStyle = {'width':Math.abs(scaleBetween(obj.won,0,obj.target))+'%',background: '#8ECECB',height:'inherit',"max-width":"100%"}
        }

        if(el.userId == "cumulativeData"){
            //Cumulative Data doesn't include Liu's data.
            obj.cumulativeData = el;
            obj.ifExceptionalAccess = true;
            var userWon = obj.won;
            var eaWon = obj.cumulativeData.wonAmount?obj.cumulativeData.wonAmount:0;
            var total = userWon+eaWon;
            var target = obj.target;

            obj.eaWon = eaWon;
            obj.eaWithCommas = numberWithCommas(eaWon.r_formatNumber(2))

            obj.targetStyle = {'width':Math.abs(scaleBetween(obj.target,1,total))+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
            obj.userWonStyle = {'width':Math.abs(scaleBetween(userWon,1,target))+'%',height:'inherit',"max-width":"100%"}
            obj.eaWonStyle = {'width':Math.abs(scaleBetween(eaWon,1,target))+'%',height:'inherit',"max-width":"100%"}

            if(total && target){
                obj.achievementNumber = parseFloat(((obj.won/obj.target)*100).r_formatNumber(2));
                obj.achievementPercentage = obj.achievementNumber+" %";
            } else if(total && !obj.target){
                obj.achievementNumber = 100;
                obj.achievementPercentage = ((total/1)*100).r_formatNumber(2)+" %";

            } else if(!total && target){
                obj.achievementNumber = 0;
                obj.achievementPercentage = "0 %";
            } else {
                obj.achievementNumber = 0;
                obj.achievementPercentage = "0 %";
            }

            if(!total && !target){
                obj.targetStyle = {'width':0+'%',background: '#FE9E83',height:'inherit',"max-width":"100%"}
                obj.userWonStyle = {'width':'0%',height:'inherit',"max-width":"100%"}
                obj.eaWonStyle = {'width':'0%',height:'inherit',"max-width":"100%"}

            }

        } else {
            obj.ifExceptionalAccess = false;
        }

        if(obj.interactionsPerDeal || obj.interactionsOverall){
            obj.interactionsPerDealStyle = {'width':scaleBetween(parseFloat(obj.interactionsPerDeal),1,obj.interactionsOverall)+'%',background: '#F4D03F',height:'inherit'}
            obj.interactionsOverallStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(!obj.interactionsOverall){
            obj.interactionsOverallStyle = {'width':0+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(obj.interactionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(!obj.interactionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionStyle = {'width':100+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(obj.interactionsWithOppContacts && obj.interactionsOverall){
            obj.interactionsWithOppContactsStyle = {'width':scaleBetween(parseFloat(obj.interactionsWithOppContacts),1,obj.interactionsOverall)+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
        } else {
            obj.interactionsWithOppContactsStyle = {'width':0+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
        }

        if(obj.interactionsPerDeal && obj.interactionsOverall){
            obj.interactionsDealOverallRatio = (parseFloat(obj.interactionsPerDeal)/obj.interactionsOverall).r_formatNumber(2)+" %"
        } else {
            obj.interactionsDealOverallRatio = "0 %"
        }

        if(obj.interactionsWithOppContacts && obj.interactionsOverall){
            obj.interactionsProductivityRatio = ((parseFloat(obj.interactionsWithOppContacts)/obj.interactionsOverall)*100).r_formatNumber(2)+" %";
        } else {
            obj.interactionsProductivityRatio = "0 %"
        }

        if(obj.oneWayInteractionsWithOppContacts && obj.oneWayInteractionCount){
            obj.oneWayInteractionsProductivityRatio = ((parseFloat(obj.oneWayInteractionsWithOppContacts)/obj.oneWayInteractionCount)*100).r_formatNumber(2)+" %"
            obj.oneWayInteractionsWithOppContactsStyle = {'width':scaleBetween(parseFloat(obj.oneWayInteractionsWithOppContacts),1,obj.oneWayInteractionCount)+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
            obj.productivityMeasure_sort = parseFloat((parseFloat(obj.oneWayInteractionsWithOppContacts)/obj.oneWayInteractionCount))*100
        } else {
            obj.oneWayInteractionsProductivityRatio = "0 %"
            obj.oneWayInteractionsWithOppContactsStyle = {'width':0+'%',background: '#F4D03F',height:'inherit',"max-width":"100%"}
            obj.productivityMeasure_sort = 0;
        }

        obj.targetStyle_v = {'height':scaleBetween(obj.target,minDeals,maxDeals)+'%',background: '#FE9E83',width:'inherit'}
        obj.wonStyle_v = {'height':scaleBetween(obj.won,minDeals_v,maxDeals_v)+'%',background: '#8ECECB',width:'inherit'}
        obj.pipelineStyle_v = {'height':scaleBetween(obj.pipeline,minDeals_v,maxDeals_v)+'%',background: '#767777',width:'inherit'}

        var randomNumber = Math.floor(Math.random() * 22) + 80;

        if(randomNumber<75){
            obj.bestCaseClass = "low"
        } else {
            obj.bestCaseClass = "high"
        }

        obj.targetAnalysisNumber = randomNumber+"%";
        if(randomNumber<=50 || i == 2){
            obj.targetLikelihood = "Low"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: '#e74c3c',height:'inherit'}
        }

        if(randomNumber>50 && randomNumber<=80){
            obj.targetLikelihood = "Average"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(randomNumber>80){
            obj.targetLikelihood = "High"
            obj.targetAnalysisStyle = {'width':scaleBetween(randomNumber,1,100)+'%',background: '#2ecc71',height:'inherit'}
        }

        if(i == 6){
            obj.targetAnalysisNumber = 61+"%";
            obj.bestCaseClass = "low"
            obj.targetLikelihood = "Average"
            obj.targetAnalysisStyle = {'width':scaleBetween(60,1,100)+'%',background: 'rgb(244, 159, 63)',height:'inherit'}
        }

        if(i == 3){
            obj.targetAnalysisNumber = 10+"%";
            obj.bestCaseClass = "low"
            obj.targetLikelihood = "Low"
            obj.targetAnalysisStyle = {'width':scaleBetween(10,1,100)+'%',background: '#e74c3c',height:'inherit'}
        }

        obj.nQtarget = Math.floor(Math.random() * 50) + 100;
        obj.nQPipeline = Math.floor(Math.random() * 25) + 100;

        obj.nextQtrGap = obj.nQtarget - obj.nQPipeline;
        obj.nextQtr = obj.nextQtrGap

        if(obj.gap>0){
            obj.gapClass = "high"
            obj.gap = "-"+obj.gap
        } else {
            obj.gapClass = "low"
            obj.gap = "+"+-(obj.gap)
        }

        if(obj.nextQtrGap>0){
            obj.nextQtrGapClass = "high"
            obj.nextQtrGap = "-"+obj.nextQtrGap
        } else {
            obj.nextQtrGapClass = "low"
            obj.nextQtrGap = "+"+-(obj.nextQtrGap)
        }

        obj.wonWithCommas = numberWithCommas(parseFloat(obj.won).r_formatNumber(2))
        obj.targetWithCommas = numberWithCommas(parseFloat(obj.target).r_formatNumber(2))
        obj.openValueWithCommas = numberWithCommas(parseFloat(obj.openValue).r_formatNumber(2))
        obj.lostAmountWithCommas = numberWithCommas(parseFloat(obj.lostAmount).r_formatNumber(2))

    })
}

function getRanking(team){
    var rankingProperty = "achievementNumber";
    team.sort(function (o1, o2) {
        return o2[rankingProperty] > o1[rankingProperty] ? 1 : o2[rankingProperty] < o1[rankingProperty] ? -1 : 0;
    });

    var appendAtLast = [];
    var filteredData = team.filter(function (tm) {
        if(tm.target != 0 || tm.won != 0){
            return tm
        } else {
            appendAtLast.push(tm)
        }
    });

    filteredData.push.apply(filteredData, appendAtLast)

    _.each(filteredData,function (tm,index) {
        tm.index = index+1
    });

    return filteredData;
}

function callApi(url,$scope,$http,index,quarter) {

    $http.get(url)
        .success(function (response) {
            $scope[quarter.quarter] = {
                userId:quarter.userId
            };
            processData(response.Data,$scope[quarter.quarter],index)

        });
}

function buildTeamProfiles(data,listOfMembers) {

    var childrenForTeamMember = {}

    if(listOfMembers && listOfMembers.length>0){
        _.each(listOfMembers,function (el) {
            childrenForTeamMember[el.userEmailId] = el;
        })
    }

    var team = [];
    var liu = {}
    _.each(data,function (el) {
        var children = {};

        if(childrenForTeamMember[el.emailId]){
            children = childrenForTeamMember[el.emailId];
            children.teamMatesEmailId.push(el.emailId)
            children.teamMatesUserId.push(el._id)
        } else {
            children = {
                userEmailId:el.emailId,
                teamMatesEmailId:[el.emailId],
                teamMatesUserId:[el._id]
            }
        }

        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                children:children,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            };
        } else {
            team.push({
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                children:children,
                noPicFlag:true,
                nameNoImg:el.firstName.substr(0,2).toUpperCase()
            })
        }

    });

    team.unshift(liu);
    return team;
}

function buildTeamProfilesWithLiu_team(data) {
    var team = [];
    var allCompanyUserIds = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            regionOwner:el.regionOwner,
            productTypeOwner:el.productTypeOwner,
            verticalOwner:el.verticalOwner,
            orgHead:el.orgHead,
            hierarchyParent:el.hierarchyParent,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        });

        allCompanyUserIds.push(el._id)
    });

    return {team:team,allCompanyUserIds:allCompanyUserIds};
}

function closeAllDropDownsAndModals($scope,id,isModal) {

    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showVerticalList = false;
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                $scope.showQuarterList = false;
                $scope.showAllList = false;
                $scope.showSourceList = false;
                $scope.selectFromList = false;
                $scope.isDisplayFilter = false;
                $scope.selectDates = false;
                $scope.openPriorityList = false;
                $scope.openNewStatusList = false;
                $scope.showHierarchyList = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function wholePie(data){

    // var important = ['Important', 0],
        var dms = ['Decision Maker', 0],
        infls = ['Influencers', 0],
        partners = ['Partners', 0],
        others = ['Others', 0]

    if(data.contactRelevance){
        // important = ['Important', data.contactRelevance.importantInteractions]
        dms = ['Decision Maker', data.contactRelevance.decisionMakersInteractions]
        infls = ['Influencers', data.contactRelevance.influencersInteractions]
        partners = ['Partners', data.contactRelevance.partnersInteractions]
        others = ['Others', data.contactRelevance.othersInteractions]
    }

    var pattern = ['#fadad0','#d1c0d1','#60a7b8','#c9deeb']
    var chart = c3.generate({
        bindto: '#'+data.pieChartId,
        data: {
            columns: [dms,infls,partners,others],
            type : 'pie'
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            }
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: 55,
            width:55
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                var left = parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))
                return {top: top, left: -125}
            }
        }
    });
}

function pieChart(data,$scope){

    var column = 0;
    var pattern =  {
        User:"#7DDED6"
    }

    if(data.won && data.target){
        column = parseFloat(((data.won/data.target)*100).r_formatNumber(2));
    } else if(data.won && !data.target){
        column = 100;
    } else if(!data.won && data.target){
        column = 0;
    } else {
        column = 0;
    }

    if(column>30 && column<65){
        pattern = {
            User:"rgb(244, 159, 63)"
        }
    }

    if(column<30){
        pattern.User = "#e74c3c"
    }

    if(column >= 100){
        pattern.User = '#7DDED6'
    }

    var arrayMap = [['User', column]];

    if (data.hasOwnProperty('cumulativeData')) {

        function id3Present() {
            var element = document.getElementById(data.pieChartId3);

            if(element){
                drawOverlappingChart(data,$scope)
            } else {
                id3Present()
            }
        }

        id3Present()
    } else {
        var ele = document.getElementById(data.pieChartId3)
        ele.style.display = "none";
    }

    var chart = c3.generate({
        bindto: '#'+data.pieChartId2,
        data: {
            columns: arrayMap,
            colors: pattern,
            type: 'gauge'
        },
        tooltip: {
            // contents: toolTipCustom,
            show:false
        },
        gauge: {
            label: {
                format: function(value, ratio) {
                    return ""
                },
                show: false
            },
            min: 0,
            max: 100,
            width: 6
        },
        size: {
            height: 75,
            width:75
        }
    });

}

function drawOverlappingChart(data,$scope) {

    var pattern = {
        'Excp':'#7DDED6'
    }

    var column = 0;
    if(data.cumulativeData.wonAmount && data.target){
        column = parseFloat(((data.cumulativeData.wonAmount/data.target)*100).r_formatNumber(2));
    } else if(data.won && !data.target){
        column = 100;
    } else if(!data.won && data.target){
        column = 0;
    } else {
        column = 0;
    }

    if(column>30 && column<60){
        pattern = {
            "Excp":"rgb(244, 159, 63)"
        }
    }

    if(column<30){
        pattern = {"Excp":"#e74c3c"}
    }

    if(column >= 100){
        pattern = {"Excp":'#7DDED6'}
    }

    var arrayMap = [['Excp',column]]

    $scope.$watch(function(){

        var chart2 = c3.generate({
            bindto: '#'+data.pieChartId3,
            data: {
                columns: arrayMap,
                colors: pattern,
                type: 'gauge'
            },
            gauge: {
                label: {
                    format: function(value, ratio) {
                        return ""
                    },
                    show: false
                },
                min: 0,
                max: 100,
                width: 4
            },
            size: {
                height: 55,
                width:55
            }
        });
    });

}

function getCurrentQuarter(data) {

    var quarters = data.array;
    var now = new Date();
    var currentMonth = now.getMonth();
    var currentQuarter = null;

    _.each(quarters,function (q) {

        var dateStart = new Date(q.start)
        var dateEnd = new Date(q.end)
        var qStartMonth = dateStart.getMonth();
        var qEndMonth = dateEnd.getMonth();
        if(qStartMonth <= currentMonth && qEndMonth >= currentMonth){
            currentQuarter = q
        }
    });

    var quarter = null;
    for (var property in data.obj) {
        if (data.obj.hasOwnProperty(property)) {
            var qtr = data.obj[property];

            if(currentQuarter && qtr && currentQuarter.start == qtr.start){
                quarter = property;
            }
        }
    }

    return quarter;
}

function switchSelectionCss(switchTo,$scope) {

    var selectors = ["locationClass","productClass","verticalClass","quarterClass"]

    _.each(selectors,function (selector) {
        $scope[switchTo] = "active";
        if(selector != switchTo){
            $scope[selector] = "";
        }
    })
}

function getQuartersArray(userId) {
    var year = new Date().getFullYear();
    return [
        {
            name:"Apr-Jun "+year,
            quarter:"quarter1",
            upperCase:"Quarter1",
            userId:userId
        },
        {
            name:"Jul-Sep "+year,
            quarter:"quarter2",
            upperCase:"Quarter2",
            userId:userId
        },
        {
            name:"Oct-Dec "+year,
            quarter:"quarter3",
            upperCase:"Quarter3",
            userId:userId
        },
        {
            name:"Jan-Mar "+(year+1),
            quarter:"quarter4",
            upperCase:"Quarter4",
            userId:userId
        }
    ];
}

function drawLineChart($scope,share,series,series2,className,labelWithDates,label,order,achievement) {

    var showArea = false;
    var grids = {
        x: {
            show: true
        },
        y: {
            show: true
        }
    }

    //This is for filling non existing months with zero values.
    var existingMonths_created = _.map(series,"meta");
    var nonExistingMonths_created = _.difference(label, existingMonths_created)

    var existingMonths_closed = _.map(series2,"meta");
    var nonExistingMonths_closed = _.differenceBy(label, existingMonths_closed);


    _.each(nonExistingMonths_created,function (el) {
        series.push({meta:el,value:0})
    });

    _.each(nonExistingMonths_closed,function (el) {
        series2.push({meta:el,value:0})
    });

    var seriesOneSorted = [];
    var seriesTwoSorted = [];

    _.each(order,function (or) {

        _.each(series,function (se) {
            if(se.meta == or.month){
                se.sort = or.sort;
                seriesOneSorted.push(se)
            }
        });

        _.each(series2,function (se2) {
            if(se2.meta == or.month){
                se2.sort = or.sort;
                seriesTwoSorted.push(se2)
            }
        });

    });

    if(achievement){
        showArea = true;
        convertToCumulativeData(seriesOneSorted);
        convertToCumulativeData(seriesTwoSorted);
    }

    var seriesArray = [seriesOneSorted];

    if(seriesOneSorted && seriesTwoSorted){
        seriesArray = [ seriesOneSorted, seriesTwoSorted]
    }

    var chart = new Chartist.Line(className, {
        labels: label,
        series: seriesArray
    }, {
        low: 0,
        showArea: showArea,
        plugins: [
            // tooltip
        ],
        width: seriesOneSorted.length>3?'175px':'150px',
        height: seriesOneSorted.length>3?'125px':'85px'
    });

    chart.on('draw', function(data) {

        if(data.type === "label" && seriesOneSorted.length>3){
            data.element.remove();

        }

        if(!achievement && data.type === 'grid' && data.index !== 0 && seriesOneSorted.length>3) {
            data.element.remove();
        }

        if(achievement){
            if(data.type === 'grid' && data.index !== 0) {
                data.element.remove();
            }
        }
    });
}

function convertToCumulativeData(data){

    _.sortBy(data, [function(o) { return o.sort; }]);

    var cumulative = 0;
    _.each(data,function (el) {
        cumulative = cumulative+el.value;
        el.value = cumulative;
    });

    _.each(data,function (el) {

        if(el.value){
            el.value = parseFloat(el.value.r_formatNumber(2))
        }
    });
}

function getColors(baseColor,numOfColors){
    return Please.make_color({
        golden: true,
        base_color: baseColor,
        saturation:0.55,
        colors_returned: numOfColors,
        format: 'hex'
    });

}

function groupAndChainForTeamSummary(data) {

    var totalAmount = 0;
    var group = _
        .chain(_.flatten(data))
        .groupBy(function (el) {
            if(el && el.name){
                return el.name;
            } else if((el && el.name == null) || (el && !el.name && el.amount)) {
                return "Others"
            }
        })
        .map(function(values, key) {

            if(checkRequired(key)){
                var amount = _.sumBy(values, 'amount');
                totalAmount = amount;

                return {
                    nameTruncated:getTextLength(key,10),
                    name:key,
                    amount:amount,
                    amountWithCommas:getAmountInThousands(amount,3)
                }
            }
        })
        .value();

    var sortProperty = "amount";

    group.sort(function (o1, o2) {
        return o2[sortProperty] > o1[sortProperty] ? 1 : o2[sortProperty] < o1[sortProperty] ? -1 : 0;
    });

    return group;

}

function groupAndChainForCR(data,quarterRange) {

    var cleanData = [];

    if(data.length>0){
        _.each(data,function (el) {
            if(new Date(el)>= new Date(quarterRange.start) && new Date(el) <= new Date(quarterRange.end)){
                cleanData.push(el)
            }
        })
    }

    var group = _
        .chain(cleanData)
        .groupBy(function (el) {
           return moment(el).format("MMM");
        })
        .map(function(values, key) {
            return {
                meta:key,
                value:values.length
            }
        })
        .value();

    return group;

}

function groupAndChainForAchievement(data) {

    var group = _
        .chain(data)
        .groupBy(function (el) {
           return moment(el.date).format("MMM");
        })
        .map(function(values, key) {

            var amount = _.sumBy(values, 'amount');
            return {
                meta:key,
                value:amount,
                sortDate:values && values[0]?moment(values[0].date).clone().startOf('month'):null
            }
        })
        .value();

    return group;

}

function summaryChart(data,className,pattern,height,width){

    var columns = _.map(data,function (el) {
        return [el.name,el.amount]
    });

    if(!height && !width){
        height = 125;
        width = 125;
    }

    var thickness = 0.12*height;

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        // pie: {
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:thickness
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: height,
            width:width
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function orderOfQuarterMonths(months){

    var order = [];
    _.each(months,function (mon,i) {
        order.push({
            month:mon,
            sort:i+1
        })
    })

    return order;

}

relatasApp.directive('searchedContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="contactList.length>0">' +
        '<div ng-repeat="item in contactList track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchedContacts1', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="assigneeBucket.length>0">' +
        '<div ng-repeat="item in assigneeBucket track by $index"> ' +
        '<div class="clearfix cursor" ng-click="selectAssignee(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

function pickDateTime($scope,minDate,maxDate,id,timezone,scopeSelector,timeRequired,callback){

    var settings = {
        value:"",
        timepicker:timeRequired?true:false,
        validateOnBlur:false,
        onSelectDate: function (dp, $input){
            $(".xdsoft_datetimepicker").hide();

            $scope.$apply(function () {
                var timezone = timezone?timezone:"UTC";
                if(scopeSelector){
                    $scope[scopeSelector] = moment(dp).tz(timezone).format();
                    $scope[scopeSelector+'Formatted'] = moment(dp).tz(timezone).format("DD MMM YYYY");
                }

                if(callback){
                    callback(dp)
                }
            });
        }
    }

    if(minDate){
        settings.minDate = minDate;
    }

    $(id).datetimepicker(settings);
}

relatasApp.service('searchContacts', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

var tooltip = Chartist.plugins.tooltip();
