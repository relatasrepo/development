
relatasApp.controller("weeklyReview",function ($scope,$http,share,$rootScope){

    // Testing. Remove this
    $scope.close = function () {
        share.clearAllData();
        share.closeCommitView()
    }

    share.setCssForWeeklyReview = function () {
        $rootScope.forWeeklyReview = "forWeeklyReview"
    }
    share.removeCssForWeeklyReview = function () {
        $rootScope.forWeeklyReview = ""
    }

    closeAllDropDownsAndModals($scope,".list");
    closeAllDropDownsAndModals($scope,".date-selection");

    getOppStages();
    share.clearAllData = function () {
        closeAllOppTables($scope);
        share.clearAllTeamCommits()
        $scope.viewingTeamData = false;
        $(".commit-switch button").removeClass("selected");
        $(".commit-switch button:first-child").addClass("selected");
        $(".pipeline-comparison").empty();
        $(".switch").show(100);
    }

    share.clearSelectedMember = function () {
        $scope.member = {};
        $scope.commitsByUsers = []
    }

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    
    $scope.updateStoreValue = function (value) {
        value.selfCommitValue = parseFloat(value.selfCommitValueFormat)
    }

    $scope.formatPlaceHolder = function (value,numbersOnly) {

        if(numbersOnly){
            value.selfCommitValueFormat = value.selfCommitValue
        } else {
            value.selfCommitValueFormat = getAmountInThousands(value.selfCommitValue,2)
        }
    }

    $scope.sortType = 'closeDateSort';
    $scope.sortReverse = false;

    $scope.closeTable = function () {
        closeAllOppTables($scope)
    }
    
    $scope.showPrevCommits = function (stage) {
        showPrevCommits($scope,$rootScope,$http,share,stage)
    }

    $scope.showOppsForStage = function (stage,prev) {
        closeAllOppTables($scope);
        $scope.weekByWeek = false;
        $scope.prevAndCurrentWeek = true;

        $scope.weekSelection = $scope.dataForRange;
        $scope.stageSelection = stage.relatasStage

        if($rootScope.fySelection){
            $scope.weekSelection = $rootScope.fySelection
        } else {
            $scope.weekSelection = $scope.dataForRange;
        }

        var oppsFiltered = [],
            oppsFlattened = []

        $scope.dontShowRisk = false

        if(stage.relatasStage != "Close Won" && stage.relatasStage != "Close Lost"){
            $scope.dontShowRisk = true
        }

        if(prev){
            _.each($scope.commitsPrev.opportunities, function (oppObj) {
                _.each(oppObj.opps, function (op) {
                    oppsFlattened.push(op)
                })
            })

            oppsFiltered = oppsFlattened.filter(function (op) {
                return op.stageName == stage.relatasStage
            })
        } else {
            _.each($scope.commitsCurrent, function (op) {
                oppsFlattened.push(op)
            })

            oppsFiltered = oppsFlattened.filter(function (op) {
                return op.relatasStage == stage.relatasStage
            })
        }

        paintOppsTable($scope,$rootScope,$http,share,oppsFiltered,true)
    }

    $scope.hoverIn = function (element) {
        element.displayToolTip = true
    }

    $scope.hoverOut = function (element) {
        element.displayToolTip = false
    }

    var userId = null;

    share.reviewUser = function (user) {

        loadTeamMembers();
        if(user.children && user.children.teamMatesEmailId){
            share.teamList = user.children.teamMatesEmailId
        } else {
            share.teamList = []
        }

        $scope.loadOppsForWeek = false;
        $scope.weekByWeek = false;
        $scope.opps = [];
        setTimeOutCallback(2000,function () {
            share.clearAllData();
        });
        closeAllOppTables($scope)

        if(user){
            $scope.member = user;
        }

        if(user){
            user.nameTruncate = getTextLength(user.fullName,12)
            userId = user.userId;
        }

        // dealsAtRisk($scope,$http,share,userId)

        if(share && share.userId){
            $rootScope.editAccess = share.userId == userId
        }

        $scope.graphLoading = true;

        setTimeOutCallback(2000,function () {

            var url = "/review/past/commits";

            if(user && user.userId){
                url = fetchUrlWithParameter(url,"userId",userId)
            }

            $http.get(url)
                .success(function (response) {
                    if(response.SuccessCode){
                        $rootScope.fySelection = "FY "+moment(response.Data.fyRange.start).year()+"-"+moment(response.Data.fyRange.end).year()
                    }

                    setRangeSelection ($scope,response)
                    pipelineComparison($scope,$rootScope,$http,share,response)
                });
        })

        userReportsInit($scope,$http,userId);
        checkOppStagesLoaded(userId);
    }

    subMenu($scope)

    var allTemplates = ["fm","pr","co","pc","reports","dp"]
    $scope[allTemplates[1]]=true;
    // selectItem($scope,$http,share,$rootScope,"Pipeline review")
    selectItem($scope,$http,share,$rootScope,"Commits")

    $scope.selectItem = function (selection) {

        if(selection.shortHand == "pr"){
            var nextWeekMoment = moment().add(1,'week');
            var nextstartOfWeek = moment(nextWeekMoment).startOf('isoweek').format('DD MMM')
            var nextendOfWeek = moment(nextWeekMoment).endOf('isoweek').format('DD MMM')
            $scope.dataForRange = nextstartOfWeek+"-"+nextendOfWeek
        } else {
            var startOfWeek = moment().startOf('isoweek').format('DD MMM')
            var endOfWeek = moment().endOf('isoweek').format('DD MMM')

            $scope.dataForRange = startOfWeek+"-"+endOfWeek;
        }

        selectItem($scope,$http,share,$rootScope,selection.name);
        loadTemplate(selection,$scope,allTemplates)
    }

    $scope.registerDp = function () {
        $scope.registerDatePickerId("startDate")
        $scope.registerDatePickerId("endDate")
    }

    $scope.getData = function (filter) {
        $scope.opps = [];
        $scope.loadOppsForWeek = false;
        $scope.weekByWeek = false;

        var url = "/review/past/commits"
        if($scope.startDateCache && $scope.endDateCache){
            url = "/review/past/commits?dateMin="+$scope.startDateCache+"&dateMax="+$scope.endDateCache
        } else {
            // toastr.warning("No start and end date found. Showing for default date range.")
        }

        if(userId){
            url = url+"&userId="+userId
        }

        if(filter){
            url = fetchUrlWithParameter(url,"filter",filter)
        }

        $http.get(url)
            .success(function (response) {
                setRangeSelection ($scope,response,$scope.startDateCache,$scope.endDateCache)
                pipelineComparison($scope,$rootScope,$http,share,response)
            });
    }

    $scope.registerDatePickerId = function(id){

        $('#'+id).datetimepicker({
            timepicker:false,
            maxDate:new Date(moment().endOf("isoweek")),
            validateOnBlur:false,
            closeOnDateSelect:true,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope[id+"Cache"] = dp;
                    $scope[id] = moment(dp).format("DD MMM YYYY")
                });
            }
        });
    }

    function checkOppStagesLoaded(userId){
        if(share.opportunityStages){
            pipelineReview($scope,$rootScope,$http,share,userId);
            processPipelineValues($scope,share,$http,userId);
        } else {
            setTimeOutCallback(1000,function () {
                checkOppStagesLoaded()
            })
        }
    }

    $scope.closeList = function (member) {
        share.reviewUser(member);
        share.getAllTasks(member);
        $scope.member = member;
        $scope.selectFromList = false;
        share.clearTeamCommits()
    }

    $scope.viewTeamCommits = function (member) {
        closeAllOppTables($scope)
        $scope.viewingTeamData = true;
        $(".switch").hide(500);
        share.getTeamCommitData(member)
    }

    $("body").on('click', 'button', function (e) {
        $(".commit-switch button").removeClass("selected");
        $(this).addClass("selected");
    });

    $scope.viewSelectedUserCommits = function () {
        $scope.viewingTeamData = false;
        $scope.exceptionalAccess = false;
        $(".switch").show(500);
    }

    $scope.viewExceptionalAccess = function (member) {
        closeAllOppTables($scope)
        $scope.exceptionalAccess = true;
        $scope.viewingTeamData = false;
        $(".switch").hide(500);
        $scope.stageSelection = share.commitStage;

        $http.get("/opportunities/by/exceptional/access?accessControl=true&userId="+member.userId)
            .success(function (response) {
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    paintOppsTable($scope,$rootScope,$http,share,response.Data)
                }
            });
    }

    $scope.saveCommits = function (commits) {
        var obj = {
            week:0,
            month:0,
            quarter:0
        }

        var alphanumericFound = false;

        _.each(commits,function (co) {
            if(co.selfCommitValue && String(co.selfCommitValue).match(/[a-z]/i)) {
                alphanumericFound = true;
            } else {
                obj[co.commitFor] = co.selfCommitValue
            }
        })
        
        if(alphanumericFound){
            toastr.error("Please enter only numbers for opportunity amount")
        } else {

            $http.post("/review/meeting/update/commit/value",obj)
                .success(function (response) {
                    if(response.SuccessCode){
                        toastr.success("Commits successfully updated")
                        share.reviewUser();
                    } else {
                        toastr.error("Commits not updated. Please try again later")
                    }
                });
        }

    }

    function loadTeamMembers(){
        
        if(window.localStorage['teamMembers']){
            try {
                $scope.team = JSON.parse(window.localStorage['teamMembers']);
            } catch(err){
                setTimeOutCallback(1000,function () {
                    loadTeamMembers()
                })
            }
        } else {
            setTimeOutCallback(1000,function () {
                loadTeamMembers()
            })
        }
    }
    
});

relatasApp.controller("teamCommit",function ($scope,$http,share,$rootScope) {

    $scope.closeCommitTable = function () {
        $scope.commitsByUsers = [];
    }

    share.clearTeamCommits = function () {
        $scope.commitsByUsers = [];
    }

    $scope.sortType = 'profile.fullName';
    $scope.sortReverse = false;

    $scope.getTeamCommits = function (data) {

        $scope.selectionType = data.rangeType +data.range;
        $scope.commitsByUsers = [];
        $scope.currentWeek = false;

        try{
            var teamMembers = JSON.parse(window.localStorage['teamMembers']),
                userHashMap = {};

            _.each(teamMembers,function (tm) {
                userHashMap[tm.userId] = tm;
            });

        }catch (err){
            console.log("err",err)
        }

        if(share.commitsCurrentRaw && share.commitsCurrentRaw.length>0){
            _.each(share.commitsCurrentRaw,function (el) {
                el.profile = userHashMap[el.userId];

                if(data.rangeType == "weekly"){
                    el.value = el.week.userCommitAmount
                }

                if(data.rangeType == "monthly"){
                    el.value = el.month.userCommitAmount
                }

                if(data.rangeType == "qtr"){
                    el.value = el.quarter.userCommitAmount
                }

                el.value = getAmountInThousands(el.value,2)
                el.emailId = el.profile.emailId;

                $scope.commitsByUsers.push(el)
            })
        }

        var nonExistingUsers = _.differenceBy(share.teamList,_.map($scope.commitsByUsers,"emailId"))
        if(nonExistingUsers && nonExistingUsers.length>0){
            _.each(nonExistingUsers,function (el) {
                $scope.commitsByUsers.push({
                    value:0,
                    profile:share.team[el]
                })
            })
        }
    }

    share.getTeamCommitData = function (member) {
        $scope.commitsByUsers = []
        loadTeamMembers(member)
    }

    $scope.showPrevCommits = function (stage) {
        $scope.commitsByUsers = [];
        showPrevCommits($scope,$rootScope,$http,share,stage)
    }

    $scope.closeTable = function () {
        closeAllOppTables($scope)
        $scope.commitsByUsers = [];
    }

    share.clearAllTeamCommits= function () {
        closeAllOppTables($scope);
    }

    function loadTeamMembers(member){

        $scope.selectedMember = member;
        if(window.localStorage['teamMembers']){
            try {
                pipelineReviewForTeam($scope,$rootScope,$http,share,member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[])
            } catch(err){
                setTimeOutCallback(1000,function () {
                    loadTeamMembers()
                })
            }
        } else {
            setTimeOutCallback(1000,function () {
                loadTeamMembers()
            })
        }
    }

    function pipelineReviewForTeam($scope,$rootScope,$http,share,userIds){

        $scope.pipelineReview = [];

        var url = "/review/meeting/team/pipeline"
        if(userIds){
            url = fetchUrlWithParameter(url,"hierarchylist",userIds)
        }

        $scope.chLoading = true;

        $http.get(url)
            .success(function (response) {
                var insightsUrl = "/insights/team/current/month/stats";

                if(userIds){
                    insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
                }

                $http.get(insightsUrl)
                    .success(function (insightsResponse) {
                        processCurrentCommitValues($scope,$rootScope,share,response,insightsResponse);
                    });
            });
    }

});

function showPrevCommits($scope,$rootScope,$http,share,stage){
    var oppsFlattened = [];
    closeAllOppTables($scope);
    if(stage.opportunities && stage.opportunities.length>0){
        _.each(stage.opportunities,function (data) {

            if(data.opps){
                _.each(data.opps,function (op) {
                    oppsFlattened.push(op)
                })
            } else {
                oppsFlattened.push(data)
            }
        })
    }

    $scope.weekSelection = stage.rangeFormatted;
    $scope.currentWeek = true;
    $rootScope.commitStage = share.commitStage;
    $scope.stageSelection = $rootScope.commitStage

    paintOppsTable($scope,$rootScope,$http,share,oppsFlattened)

}

function closeAllOppTables($scope){
    $scope.currentWeek = false;
    $scope.prevAndCurrentWeek = false;
    $scope.exceptionalAccess = false;
    $scope.weekByWeek = false;
    $scope.opps = [];
}

function pipelineReview($scope,$rootScope,$http,share,userId){

    $scope.pipelineReview = [];

    var url = "/review/meeting/pipeline"
    if(userId){
        url = url+"?userId="+userId
    }
    $scope.chLoading = true;
    $http.get(url)
        .success(function (response) {
            var insightsUrl = "/insights/current/month/stats";

            if(userId){
                insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userId)
            }

            $http.get(insightsUrl)
                .success(function (insightsResponse) {
                    processCurrentCommitValues($scope,$rootScope,share,response,insightsResponse);
                });
        });
}

function setRangeSelection ($scope,response,startOfWeek,endOfWeek){

     if(response && response.Data.dateMin && response.Data.dateMax){
        startOfWeek = moment(response.Data.dateMin).format('DD MMM');
        endOfWeek = moment(response.Data.dateMax).format('DD MMM')

    } else if(!startOfWeek && !endOfWeek){
         startOfWeek = moment().startOf('isoweek').format('DD MMM');
         endOfWeek = moment().endOf('isoweek').format('DD MMM')
     } else {
        startOfWeek = moment(startOfWeek).startOf('isoweek').format('DD MMM');
        endOfWeek = moment(endOfWeek).endOf('isoweek').format('DD MMM')
    }

    $scope.selectDates = false;
    $scope.rangeSelection = startOfWeek+"-"+endOfWeek;
}

function processCurrentCommitValues($scope,$rootScope,share,response,insightsResponse){

    $scope.commitHistory = [];
    $scope.chLoading = false;

    var weekStartDateTime = getCommitDayTimeEnd(share.companyDetails);
    var weekEndDateTime = moment(weekStartDateTime).add(1,'week');

    var startOfWeek = moment(weekStartDateTime).format('DD MMM')
    var endOfWeek = moment(weekEndDateTime).format('DD MMM')
    var currentMonth = moment().format('MMM')

    var startOfMonth = moment().startOf('month')
    var endOfMonth = moment().endOf('month')

    var commitsArray = [1,2,3];
    var opportunitiesClosingThisWeek = [],
        opportunitiesClosingThisMonth = [],
        opportunitiesClosingThisQtr = [];

    $rootScope.commitStage = response.commitStage;
    share.commitStage = response.commitStage;
    share.commitsPrevRaw = response.commitsPrevRaw
    share.commitsCurrentRaw = response.commitsCurrentRaw

    var thisWeekCommitVal = 0
        ,thisMonthCommitVal = 0
        ,thisQtrCommitVal = 0

    _.each(response.opps,function (op) {
        if(op.relatasStage == response.commitStage){

            if(new Date(op.closeDate)>= (new Date(response.qStart)) && new Date(op.closeDate) <= (new Date(response.qEnd))){
                thisQtrCommitVal = thisQtrCommitVal+parseFloat(op.amount);
                opportunitiesClosingThisQtr.push(op)
            }

            if(new Date(op.closeDate)>= (new Date(startOfMonth)) && new Date(op.closeDate) <= (new Date(endOfMonth))){
                thisMonthCommitVal = thisMonthCommitVal+parseFloat(op.amount);
                opportunitiesClosingThisMonth.push(op)
            }

            if(new Date(op.closeDate)>= new Date(weekStartDateTime) && new Date(op.closeDate) <= new Date(weekEndDateTime)){
                thisWeekCommitVal = thisWeekCommitVal+parseFloat(op.amount);
                opportunitiesClosingThisWeek.push(op)
            }
        }
    });

    $scope.dataForRange = startOfWeek+"-"+endOfWeek;
    var rangeFormatted = startOfWeek+"-"+endOfWeek;

    var mon_wonAmount = 0,
        mon_targetAmount = 0,
        qtr_wonAmount = 0,
        qtr_targetAmount = 0

    if(insightsResponse.Data && insightsResponse.Data.month[0] && insightsResponse.Data.month[0].target) {
        mon_targetAmount = insightsResponse.Data.month[0].target
    }

    if(insightsResponse.Data && insightsResponse.Data.month[0] && insightsResponse.Data.month[0].won){
        mon_wonAmount = insightsResponse.Data.month[0].won
    }

    if(insightsResponse.Data && insightsResponse.Data.qtr[0] && insightsResponse.Data.qtr[0].target) {
        qtr_targetAmount = insightsResponse.Data.qtr[0].target
    }

    if(insightsResponse.Data && insightsResponse.Data.qtr[0] && insightsResponse.Data.qtr[0].won){
        qtr_wonAmount = insightsResponse.Data.qtr[0].won
    }

    var mon_relatasCommit = 0,
        mon_selfCommit = 0,
        qtr_relatasCommit = 0,
        qtr_selfCommit = 0;

    if(response && response.commitsCurrent && response.commitsCurrent){
        mon_relatasCommit = response.commitsCurrent.month.relatasCommitAmount;
        mon_selfCommit = response.commitsCurrent.month.userCommitAmount;

        qtr_relatasCommit = response.commitsCurrent.quarter.relatasCommitAmount;
        qtr_selfCommit = response.commitsCurrent.quarter.userCommitAmount;
    }

    var mon_allValues = [mon_wonAmount,mon_targetAmount,thisMonthCommitVal,mon_selfCommit],
        qtr_allValues = [qtr_wonAmount,qtr_targetAmount,thisQtrCommitVal,qtr_selfCommit]

    var mon_max = _.max(mon_allValues),
        qtr_max = _.max(qtr_allValues)


    var mon_targetStyle = {background:"#f4ada7",width:scaleBetween(mon_targetAmount,0,mon_max)+"%"},
        mon_achievementStyle = {background:"#8ECECB",width:scaleBetween(mon_wonAmount,0,mon_max)+"%"},
        mon_relatasCommitStyle = {background:oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true),width:scaleBetween(thisMonthCommitVal,0,mon_max)+"%"},
        mon_selfCommitStyle = {background:"#648ca6",width:scaleBetween(mon_selfCommit,0,mon_max)+"%"}

    var qtr_targetStyle = {background:"#f4ada7",width:scaleBetween(qtr_targetAmount,0,qtr_max)+"%"},
        qtr_achievementStyle = {background:"#8ECECB",width:scaleBetween(qtr_wonAmount,0,qtr_max)+"%"},
        qtr_relatasCommitStyle = {background:oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true),width:scaleBetween(thisQtrCommitVal,0,qtr_max)+"%"},
        qtr_selfCommitStyle = {background:"#648ca6",width:scaleBetween(qtr_selfCommit,0,qtr_max)+"%"}

    var backgroundColor = {background:oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)}
    var opportunities = response.commitsCurrent.opportunities;

    _.each(commitsArray,function (t,index) {
        var header = "Commit"
        var range = "( "+startOfWeek+"-"+endOfWeek+" )"
        var ccv = thisWeekCommitVal,
            scv = 0
        var noGraph = true;
        var target = {},
            commit = {},
            achievement = {},
            self = {}

        var targetVal = 0,
            achievementVal = 0,
            relatasCommitVal = 0,
            selfCommitVal = 0,
            rangeType = "weekly",
            commitFor = "week"

        if(response && response.commitsCurrent && response.commitsCurrent.week && response.commitsCurrent.week.userCommitAmount){
            scv = response.commitsCurrent.week.userCommitAmount
        } else {
            scv = 0
        }

        if(index == 0){
            opportunities = opportunitiesClosingThisWeek;
        }

        if(index == 1){

            opportunities = opportunitiesClosingThisMonth;
            header = "Commit"
            range = "("+currentMonth+")"
            ccv = thisMonthCommitVal
            rangeFormatted = currentMonth

            if(response && response.commitsCurrent && response.commitsCurrent.month && response.commitsCurrent.month.userCommitAmount){
                scv = response.commitsCurrent.month.userCommitAmount
            } else {
                scv = 0
            }

            targetVal = mon_targetAmount
            relatasCommitVal = thisMonthCommitVal
            selfCommitVal = mon_selfCommit
            achievementVal = mon_wonAmount

            target = mon_targetStyle
            commit = mon_relatasCommitStyle
            self = mon_selfCommitStyle
            achievement = mon_achievementStyle

            noGraph = false
            commitFor = "month"
            rangeType = "monthly"
        }

        if(index == 2){
            targetVal = qtr_targetAmount
            relatasCommitVal = thisQtrCommitVal
            selfCommitVal = qtr_selfCommit
            achievementVal = qtr_wonAmount
            opportunities = opportunitiesClosingThisQtr;

            var append = ""
            _.each(response.monthsInQuarters,function (month,index) {

                if(index == 0 ){
                    append = month.substr(0,1)
                } else {
                    append = append +"-"+month.substr(0,1)
                }
            })

            if(response && response.commitsCurrent && response.commitsCurrent.quarter && response.commitsCurrent.quarter.userCommitAmount){
                scv = response.commitsCurrent.quarter.userCommitAmount
            } else {
                scv = 0
            }

            rangeFormatted = append
            header = "Commit"
            range = "("+append+")"
            ccv = thisQtrCommitVal
            noGraph = false
            target = qtr_targetStyle
            commit = qtr_relatasCommitStyle
            self = qtr_selfCommitStyle
            achievement = qtr_achievementStyle
            commitFor = "quarter"
            rangeType = "qtr"
        }

        $scope.commitHistory.push({
            rangeFormatted:rangeFormatted,
            editAccess:$rootScope.editAccess,
            idForHover:"idForHover"+index,
            idForTooltip:"idForTooltip"+index,
            targetVal:getAmountInThousands(targetVal,2),
            relatasCommitVal:getAmountInThousands(relatasCommitVal,2),
            selfCommitVal:getAmountInThousands(selfCommitVal,2),
            commitFor:commitFor,
            header:header,
            range:range,
            noGraph:noGraph,
            noGraphClass:noGraph?"set-width":"",
            currentCommitValue:getAmountInThousands(ccv,2),
            selfCommitValue:scv,
            selfCommitValueFormat:getAmountInThousands(scv,2),
            borderRight:index !=2?"border-right":"",
            target:target,
            self:self,
            commit:commit,
            achievement:achievement,
            rangeType:rangeType,
            achievementVal:getAmountInThousands(achievementVal,2),
            displayToolTip:false,
            opportunities:opportunities,
            backgroundColor:backgroundColor
        })
    })
}

function processPipelineValues($scope,share,$http,userId){

    $http.get("/review/pipeline/status?userId="+userId)
        .success(function (response) {

            $scope.commitsPrev = response.commitsPrev
            $scope.commitsCurrent= response.opps

            var data = getAmountByStage(response.opps)

            var prevWeekOpps = [];

            if(response && response.commitsPrev) {

                _.each(response.commitsPrev.opportunities,function (data) {
                    _.each(data.opps,function (op) {
                        prevWeekOpps.push(op)
                    })
                })

                var prevWeekData = getAmountByStage(prevWeekOpps,true)

            } else {

                prevWeekData = {
                    array:[],
                    map:{}
                }

                _.each(share.opportunityStages,function (st) {
                    prevWeekData.array.push({
                        stage:st,
                        amount:0
                    })

                    prevWeekData.map[st] = 0;
                })
            }

            var widthOfBox = {width: 100/share.opportunityStages.length+"%"};

            _.each(share.opportunityStages,function (st,index) {

                var growthStatus = ""

                if(prevWeekData.map[st.name] && data.map[st.name]){

                    if(data.map[st.name]>prevWeekData.map[st.name]){
                        growthStatus = "fa-caret-up"
                    } else if(data.map[st.name]<prevWeekData.map[st.name]){
                        growthStatus = "fa-caret-down"
                    } else if(data.map[st.name] == prevWeekData.map[st.name]){
                        growthStatus = ""
                    }
                }

                if(prevWeekData.map[st.name] && !data.map[st.name]){
                    growthStatus = "fa-caret-down"
                }

                if(!prevWeekData.map[st.name] && data.map[st.name]){
                    growthStatus = "fa-caret-up"
                }

                $scope.pipelineReview.push({
                    widthOfBox:widthOfBox,
                    growthStatus:growthStatus,
                    relatasStage:st.name,
                    prevAmount:prevWeekData.map[st.name]?getAmountInThousands(prevWeekData.map[st.name],2):0,
                    currentAmount:data.map[st.name]?getAmountInThousands(data.map[st.name],2):0,
                    // prevAmount:pa,
                    // currentAmount:ca,
                    isLast:share.opportunityStages.length == index+1?"last":"",
                    isFirst: index == 0?"first":""
                })
            })
        })
}

function getAmountByStage(opps,forCommits){

    var data = [];
    var byStage = 'relatasStage';

    if(forCommits){
        byStage = 'stageName';
    }

    if(checkRequired(opps)){

       data =  _
            .chain(opps)
            .groupBy(byStage)
            .map(function(values,key) {
                return {
                    stage:key,
                    amount:_.sumBy(values,function (va) {
                        return parseFloat(va.amount)
                    })
                }
            })
            .value();
    }

    var dataMap = {}

    _.each(data,function (el) {
        dataMap[el.stage] = el.amount
    })

    return {
        array:data,
        map:dataMap
    }

}

function userReportsInit($scope,$http,userId){
    
    var url = "/review/all/opps"
    
    if(userId){
        url = url+"?userId="+userId
    }

    $scope.companyOpps = []
    $scope.regionsOpps = [];
    $scope.regionsOpps = [];
    $scope.companyAmount = 0;
    $scope.topCompanies = [];

    var regions = [],
        products = [],
        companies = [],
        verticals = []
    
    $http.get(url)
        .success(function (response) {

            if(response.SuccessCode && response.Data && response.Data.length>0){
                
                _.each(response.Data,function (op) {

                    var companyName = fetchCompanyFromEmail(op.contactEmailId)
                    if(companyName){
                        op.company = companyName;
                    } else {
                        op.company = "Others";
                    }

                    op.amount = parseFloat(op.amount)

                    if(op.company){
                        companies.push({
                            name:op.company,
                            nameTruncate:op.company,
                            amount:op.amount
                        });
                    }

                    products.push({
                        name:op.productType,
                        nameTruncate:op.productType,
                        amount:op.amount
                    });

                    verticals.push({
                        name:op.vertical,
                        nameTruncate:op.vertical,
                        amount:op.amount
                    });

                    if(op.geoLocation){
                        regions.push({
                            name:op.geoLocation.town,
                            nameTruncate:op.geoLocation.town,
                            amount:op.amount
                        });
                    }

                    op.name = op.company;

                    op.nameTruncated = getTextLength(op.opportunityName,20);
                    op.amountWithCommas = getAmountInThousands(op.amount,2);
                    op.closeDateSort = moment(op.closeDate).unix()
                    op.closeDate = moment(op.closeDate).format(standardDateFormat());
                    $scope.companyOpps.push(op)

                });

                var companyData = groupAndChainForTeamSummary(companies)
                var verticalData = groupAndChainForTeamSummary(verticals)
                var regionData = groupAndChainForTeamSummary(regions)
                var productData = groupAndChainForTeamSummary(products)

                $scope.topCompanies = companyData.length>3?companyData.slice(0,3):companyData;
                $scope.topRegions = regionData.length>3?regionData.slice(0,3):regionData;
                $scope.topProducts = productData.length>3?productData.slice(0,3):productData;
                $scope.topVertical = verticalData.length>3?verticalData.slice(0,3):verticalData;
                
                $scope.companyAmount = getAmountInThousands(_.sumBy(companyData,"amount"),2)
                $scope.verticalAmount = getAmountInThousands(_.sumBy(verticalData,"amount"),2)
                $scope.regionAmount = getAmountInThousands(_.sumBy(regionData,"amount"),2)
                $scope.productAmount = getAmountInThousands(_.sumBy(productData,"amount"),2)

                setTimeOutCallback(2000,function () {
                    summaryChart(companyData,".user-company-chart",shadeGenerator(48,101,154,companyData.length,40),100,100);
                    summaryChart(regionData,".user-region-chart",shadeGenerator(71,89,129, regionData.length,20),100,100);
                    summaryChart(productData,".user-product-chart",shadeGenerator(0,115,76,productData.length,15),100,100);
                    summaryChart(verticalData,".user-vertical-chart",shadeGenerator(250,158,70, verticalData.length,20),100,100);
                })

            }

        })
}

function selectItem($scope,$http,share,$rootScope,name){
    $scope.templateHeader = name;
    menuToggleSelection(name,$scope.subMenu)
}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function subMenu($scope){

    $scope.subMenu = [{
        name:"Commits",
        shortHand:"pr",
        selected:"selected"
    },{
        name:"Pipeline review",
        shortHand:"pc",
        selected:""
    },
    //     {
    //     name:"Funnel movement",
    //     shortHand:"fm",
    //     selected:""
    // }
    //     ,
    {
        name:"Discussion points",
        shortHand:"dp",
        selected:""
    }
    //     ,{
    //     name:"Reports",
    //     shortHand:"reports",
    //     selected:""
    // }
    ]
}

function loadTemplate(selection,$scope,allTemplates) {

    if(selection.shortHand != "pr"){
        $(".commit-switch").hide()
    } else {
        $(".commit-switch").show()
    }

    _.each(allTemplates,function (tp) {
        if(selection.shortHand == tp){
            $scope[tp] = true;
        } else {
            $scope[tp] = false;
        }
    })
}

function pipelineComparison($scope,$rootScope,$http,share,response){

    var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.Data.commitStage;
    commitColName = commitColName+" stage"
    var relatasCommitCol = [commitColName],
        selfCommitCol = ['Self commit'],
        oppCreatedCol = ['Opp created (count)'],
        oppWonAmountCol = ['Opp won ($)'],
        oppWonCol = ['Opp won (count)']

    setTimeOutCallback(100,function () {
        $scope.graphLoading = false;
    })

    formatCommitData(relatasCommitCol,selfCommitCol,response,share)
    formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response)

    var relatasCommitColLength = relatasCommitCol.length,
        prefilledDataRelatasCommit = []

    _.each(relatasCommitCol,function (re,index) {
        if(index == relatasCommitColLength-1 && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
            prefilledDataRelatasCommit.push(_.sumBy(response.Data.oppsInStageClosingThisWeek,"amount"))
        } else {
            prefilledDataRelatasCommit.push(re)
        }
    })

    var colors = {
        "Self commit": '#638ca6',
        "Opp created (count)": '#3498db',
        'Opp won (count)': '#8ECECB',
        'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
    }

    colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

    var axes = {
    }

    // axes["Opp created (count)"] = 'y2'
    // axes["Opp won (count)"] = 'y2'

    axes[commitColName] = 'y2'
    axes["Opp won ($)"] = 'y2'
    axes["Self commit"] = 'y2'

    var chart = c3.generate({
        bindto: ".pipeline-comparison",
        data: {
            columns: [oppWonCol,oppCreatedCol,prefilledDataRelatasCommit,selfCommitCol,oppWonAmountCol],
            axes: axes,
            type: 'bar',
            types: {
                'Opp created (count)': 'spline',
                "Opp won (count)": 'spline'
            },
            colors:colors,
            bar: {
                width: {
                    ratio: 0.5
                }
            },
            onclick: function(e) {
                $scope.$apply(function () {
                    closeAllOppTables($scope);
                    $rootScope.commitStage = share.commitStage;
                    displayOpps(e,$scope,$rootScope,$http,share,response)
                });
            }
        },
        axis: {
            y2: {
                label: {
                    text:'Opp amount',
                    position: 'outer-center'
                },
                show: true
            },
            y: {
                label: {
                    text:'No. of Opps',
                    position: 'outer-center'
                },
                show: true
            },
            x : {
                tick: {
                    fit: true
                }
            }
        },
        tooltip: {
            format: {
                title: function (d) {

                    if(share.weekByIndexNumberForCommits[d]){
                        // var date = share.weekByIndexNumberForCommits[d].date;
                        var date = buildDateObj(share.weekByIndexNumberForCommits[d].weekYear);

                        date = moment(date).day(share.companyDetails.commitDay);
                        if(new Date(date)> new Date()){
                            date = moment(date).subtract(1,'week')
                        }

                        var dateRange = moment(date).format('DD MMM')+"-"+moment(endDate).format('DD MMM')
                        return dateRange;
                    }
                }
            }
        }
    });
}

function buildDateObj(weekYear) {
    var year = weekYear.substr(-4);
    var week = weekYear.split(year)[0]
    return new Date(moment().week(week).year(year));
}

function dealsAtRisk($scope,$http,share,userId){

    var url = "/review/opps/at/risk";
    if(userId){
        url = url+"?userId="+userId
    }

    share.dealsAtRiskMap = {
        loaded:false,
        data:{}
    };

    $http.get(url)
        .success(function (response) {
            share.dealsAtRiskMap.loaded = true;

            if(response.SuccessCode && response.Data && response.Data.length>0){
                if(response.Data[0].deals.length>0){
                    _.each(response.Data[0].deals,function (deal) {
                        share.dealsAtRiskMap.data[deal.opportunityId] = deal
                    })
                }
            }
        })
}

function displayOpps(event,$scope,$rootScope,$http,share,response){
    $scope.loadOppsForWeek = true;
    $scope.weekByWeek = true;
    $scope.loadingOpps = true;
    var oppsFlatten = [];

    // var currentWeekYear = moment().week()+""+moment().year()
    // var nextWeekMoment = moment().add(1,'week');
    var nextWeekMoment = getCommitDayTimeEnd(share.companyDetails);
    var currentWeekYear = moment(nextWeekMoment).week()+""+moment(nextWeekMoment).year()

    if(share.weekByIndexNumberForCommits[event.index]){
        var weekYear = share.weekByIndexNumberForCommits[event.index].weekYear
        var date = share.weekByIndexNumberForCommits[event.index].date
        date = moment(date).add(1,"day") //Compensating timezone issues.
        $scope.weekSelection = moment(date).startOf('isoweek').format('DD MMM')+"-"+moment(date).endOf('isoweek').format('DD MMM');
        var opps = share.oppsCommitedByWeek[weekYear];

        if(opps && opps.length>0) {
            _.each(opps, function (oppObj) {
                _.each(oppObj.opps, function (op) {
                    oppsFlatten.push(op)
                })
            })

            if(response.Data && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
                oppsFlatten = response.Data.oppsInStageClosingThisWeek
            }

            paintOppsTable($scope,$rootScope,$http,share,_.uniqBy(oppsFlatten,"opportunityId"))
        } else {

            if(parseFloat(currentWeekYear) == parseFloat(weekYear)){
                if(response.Data && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
                    oppsFlatten = response.Data.oppsInStageClosingThisWeek
                }
            }

            paintOppsTable($scope,$rootScope,$http,share,_.uniqBy(oppsFlatten,"opportunityId"))
        }
    }
}

function paintOppsTable($scope,$rootScope,$http,share,opps,forPrevCurrent) {

    $scope.opps = [];
    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;
    var weekStartDateTime = getCommitDayTimeEnd(share.companyDetails);
    var weekEndDateTime = moment(weekStartDateTime).add(1,'week');

    if(opps && opps.length>0){
        _.each(opps,function (op) {
            op.closingThisWeek = {};
            op.ngmReq = ngmReq;
            op.amountWithNgm = op.amountNonNGM?op.amountNonNGM:op.amount;

            if(ngmReq){
                op.amountWithNgm = (op.amountWithNgm*op.netGrossMargin)/100
            }

            if(new Date(op.closeDate)>= (new Date(moment(weekStartDateTime))) && new Date(op.closeDate) <= (new Date(weekEndDateTime))){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            if($rootScope.commitStage == op.stageName || forPrevCurrent){
                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2);

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

function formatCommitData(relatasCommitCol,selfCommitCol,response,share){

    var rangeType = 'week';
    share.oppsCommitedByWeek = {}
    share.weekByIndexNumberForCommits = {}

    if(response.Data && response.Data.commits){
        response.Data.commits.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.commits,function (el,index) {

            share.weekByIndexNumberForCommits[index] = {
                weekYear:el.commitWeekYear,
                date:el.date
            };
            share.oppsCommitedByWeek[el.commitWeekYear] = el.opportunities;

            relatasCommitCol.push(el[rangeType].relatasCommitAmount)
            selfCommitCol.push(el[rangeType].userCommitAmount)
        })
    }
}

function formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response){
    if(response.Data && response.Data.oppsConversion){

        response.Data.oppsConversion.created.sort(function (o1, o2) {
            // return parseFloat(o1.weekYear) > parseFloat(o2.weekYear) ? 1 : parseFloat(o1.weekYear) < parseFloat(o2.weekYear) ? -1 : 0;
            return new Date(buildDateObj(o1.weekYear)) > new Date(buildDateObj(o2.weekYear)) ? 1 : new Date(buildDateObj(o1.weekYear)) < new Date(buildDateObj(o2.weekYear)) ? -1 : 0;
        });

        response.Data.oppsConversion.oppsWon.sort(function (o1, o2) {
            // return parseFloat(o1.weekYear) > parseFloat(o2.weekYear) ? 1 : parseFloat(o1.weekYear) < parseFloat(o2.weekYear) ? -1 : 0;
            return new Date(buildDateObj(o1.weekYear)) > new Date(buildDateObj(o2.weekYear)) ? 1 : new Date(buildDateObj(o1.weekYear)) < new Date(buildDateObj(o2.weekYear)) ? -1 : 0;
        });

        _.each(response.Data.oppsConversion.oppsWon,function (el) {
            oppWonCol.push(el.count)
            oppWonAmountCol.push(el.amount?el.amount:0)
        })

        _.each(response.Data.oppsConversion.created,function (el) {
            oppCreatedCol.push(el.count)
        })
    }
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function groupAndChainForTeamSummary(data) {

    var totalAmount = 0;
    var group = _
        .chain(_.flatten(data))
        .groupBy(function (el) {
            if(el && el.name){
                return el.name;
            } else if((el && el.name == null) || (el && !el.name && el.amount)) {
                return "Others"
            }
        })
        .map(function(values, key) {

            if(checkRequired(key)){
                var amount = _.sumBy(values, 'amount');
                totalAmount = amount;

                return {
                    nameTruncated:getTextLength(key,10),
                    name:key,
                    amount:amount,
                    amountWithCommas:getAmountInThousands(amount,3)
                }
            }
        })
        .value();

    var sortProperty = "amount";

    group.sort(function (o1, o2) {
        return o2[sortProperty] > o1[sortProperty] ? 1 : o2[sortProperty] < o1[sortProperty] ? -1 : 0;
    });

    return group;

}
