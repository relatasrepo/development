var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

var contactContextEmailId = getParams(window.location.href).emailId

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){

            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope, $http, share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.appConfig = response.appConfig;

                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }

                if(response.companyDetails && response.companyDetails.opportunitySettings){
                    $rootScope.isProductRequired = response.companyDetails.opportunitySettings.productRequired;
                    $rootScope.isRegionRequired = response.companyDetails.opportunitySettings.regionRequired;
                    $rootScope.isVerticalRequired = response.companyDetails.opportunitySettings.verticalRequired;
                }

                $rootScope.companyDetails = response.companyDetails
                var regionOwner = response.Data.regionOwner
                var verticalOwner = response.Data.verticalOwner
                var productTypeOwner = response.Data.productTypeOwner

                $rootScope.hasExcpAccess = false;
                if((response.Data.regionOwner && response.Data.regionOwner.length>0) || (response.Data.productTypeOwner && response.Data.productTypeOwner.length>0) || (response.Data.verticalOwner && response.Data.verticalOwner.length>0)){
                    $rootScope.hasExcpAccess = true;
                }

                // var regionOwner = response.Data.regionOwner.slice(0,3)
                // var verticalOwner = response.Data.verticalOwner.slice(0,3)
                // var productTypeOwner = response.Data.productTypeOwner.slice(0,3)

                var regions = response.Data.regionOwner && response.Data.regionOwner.length>0?"Regions: "+regionOwner.join(',')+" \n": ''
                var products = response.Data.productTypeOwner && response.Data.productTypeOwner.length>0?"Products: "+productTypeOwner.join(',')+" \n": ''
                var verticals = response.Data.verticalOwner && response.Data.verticalOwner.length>0?"Verticals: "+verticalOwner.join(',')+" \n": ''

                $rootScope.orgHead = response.Data.orgHead;
                $rootScope.netGrossMargin = response.companyDetails.netGrossMargin;

                $rootScope.accessHelpTxt = "You have access to view  \n"+regions+products+verticals
                $rootScope.notificationForReportingManager = response.companyDetails.notificationForReportingManager;
                $rootScope.notificationForOrgHead = response.companyDetails.notificationForOrgHead;

                if($rootScope.notificationForOrgHead){
                    $rootScope.mailOrgHead = true;
                }

                if($rootScope.notificationForReportingManager){
                    $rootScope.mailRm = true;
                }

                response.Data.userId = response.Data._id;
                share.liuData = response.Data
            }
            else{

            }
        }).error(function (data) {
    })
});

relatasApp.controller("topContextMenu", function ($scope, $http,$rootScope,share){

    share.productList = function (productList) {
        $scope.disableProductList = 'disable';
        if(productList && productList.length>0){
            $scope.disableProductList = '';
        }

        share.displayProducts(productList)
    }

    share.displayProducts = function (productList) {
        $scope.productTypes = _.cloneDeep(productList);
        $scope.productTypes.push({
            name:"All products"
        })
    }

    share.verticalList = function (verticalList) {
        $scope.disableVerticalList = 'disable';
        if(verticalList && verticalList.length>0){
            $scope.disableVerticalList = '';
        }

        share.displayVertical(verticalList)
    }

    share.displayVertical = function (verticalList) {
        $scope.verticals = _.cloneDeep(verticalList);
        $scope.verticals.push({
            name:"All verticals"
        })
    }

    share.geoLocations = function (geoLocations) {
        $scope.geoLocations = _.cloneDeep(geoLocations);

        $scope.geoLocations.push({
            region:$scope.locationSelected
        })
    }

    $scope.openOpportunityForm = function () {
        share.openOppForm();
    }
    
    $scope.getOpportunityWithAccess = function (liu) {
        if(!liu){
            share.fetchOpportunities(share.getUserId(),'accessControl',null)
            share.getTargets(share.getUserId(),'accessControl')
            share.setCompanyDetails(null,'accessControl')
        } else {
            share.fetchOpportunities(share.getUserId(),null,null)
            share.getTargets(share.getUserId(),null)
            share.setCompanyDetails()
        }
    }

    $scope.getTeam = function () {
        $scope.ifTeam = !$scope.ifTeam;
        $scope.ifProducts = false;

        var url = '/company/user/hierarchy/insights';

        $http.get(url)
            .success(function (response) {

                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfiles(response.Data);
                    $scope.selection = $scope.team[0];
                    share.selection = {};
                    share.selection.fullName = "Show all team members"
                    share.team = $scope.team;
                    share.setUserEmailId($scope.selection.emailId)

                    var usersDictionary = {};

                    if(response.companyMembers.length>0){
                        var companyMembers = buildTeamProfilesWithLiu(response.companyMembers)

                        _.each(companyMembers,function (member) {
                            usersDictionary[member.emailId] = member
                        })
                    }
                }
            });
    }

    $scope.getTeam();

    $scope.users = [];
    $scope.getOpportunities = function (user,filter) {
        share.resetFiltersToDefault();

        var userIds = [];

        if(user == "all"){
            userIds = _.map($scope.team,"userId")
        } else {
            userIds = user.userId
        }

        if(share.liuData && share.liuData.orgHead){
            share.getTargets(userIds,share.liuData.companyId);
        } else {
            // share.getTargets(userIds);
        }

        $scope.selectFromList = false;
        if(user == 'all'){
            $scope.selection = {
                nameNoImg:"All",
                fullName: "All Team Members",
                noPicFlag:true
            }
            share.fetchOpportunities(_.map($scope.team,"userId"))
        } else {
            $scope.selection = user
            share.fetchOpportunities(userIds)
        }
    }
    
    share.resetFiltersToDefault = function () {
        $scope.productSelected = "All products";
        $scope.verticalSelected = "All verticals";
        $scope.locationSelected = "All regions";
        $scope.dateRange = "All close dates";
        $scope.companySelected = "All companies";
        $scope.contactSelected = "All contacts";
    }

    share.resetFiltersToDefault()

    $scope.filterOpportunities = function (item,filterType) {

        if(filterType === "vertical"){
            $scope.verticalSelected = item.name;
            $scope.showVerticalList = false;
            $scope.productSelected = "All products";
            $scope.locationSelected = "All regions";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            share.fetchOpportunities($scope.selection.userId,{type:filterType,vertical:$scope.verticalSelected},true)
        }

        if(filterType === "product"){
            $scope.productSelected = item.name;
            $scope.showProductList = false;
            $scope.locationSelected = "All regions";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            share.fetchOpportunities($scope.selection.userId,{type:filterType,productType:$scope.productSelected},true)
        }

        if(filterType === "location"){
            $scope.productSelected = "All products";
            $scope.dateRange = "All close dates";
            $scope.companySelected = "All companies"
            $scope.contactSelected = "All contacts";
            $scope.locationSelected = item.region;
            $scope.showLocationList = false;
            share.fetchOpportunities($scope.selection.userId,{type:filterType,location:$scope.locationSelected},true)
        }

        if(filterType === "company"){
            $scope.productSelected = "All products";
            $scope.dateRange = "All close dates";
            $scope.locationSelected = "All regions";
            $scope.contactSelected = "All contacts";
            $scope.companySelected = item.name;
            $scope.showCompanyList = false;
            share.fetchOpportunities($scope.selection.userId,{type:filterType,company:$scope.companySelected},true)
        }

        if(filterType === "contact"){
            $scope.productSelected = "All products";
            $scope.dateRange = "All close dates";
            $scope.locationSelected = "All regions";
            $scope.companySelected = "All companies";
            $scope.contactSelected = getTextLength(item.name,15);
            $scope.showContactList = false;
            var resetContext = false;
            if(contactContextEmailId){
                resetContext = true;
            }
            contactContextEmailId = null;
            share.fetchOpportunities($scope.selection.userId,{type:filterType,name:item.emailId,resetContext:resetContext},true)
        }

    };

    closeAllDropDownsAndModals($scope,".list");

    $scope.showAllDateRange = function () {
        $scope.dateRange = "All close dates";
        share.fetchOpportunities($scope.selection.userId,{type:'date',date:"All close dates"},true)
        $scope.showDateRangePicker = false;
    }
    
    $scope.registerDatePickerId = function(id){

        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            // minDate: $scope.startDt && id == "endDt"?new Date($scope.startDt):new Date(),
            onSelectDate: function (dp, $input){

                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)
                
                $scope.$apply(function () {
                    $scope[id] = moment(dp).tz(timezone).format("DD MMM YYYY");
                    $scope.dateRange = "Before "+$scope[id]
                    share.fetchOpportunities($scope.selection.userId,{type:'date',date:dp},true)
                    $scope.showDateRangePicker = false;
                    $scope.productSelected = "All products";
                    $scope.locationSelected = "All regions";
                });
            }
        });
    }

    // $scope.amountRange = "All amounts";
    
    share.displayCompanies = function (companies) {
        companies.unshift({name:"All companies"})
        $scope.companies = _.unionBy(companies,'name')
    }

    share.displayContacts = function (contacts) {
        $scope.contacts = _.unionBy(contacts,"emailId");

        $scope.contacts.unshift({name:"All contacts",emailId:"All contacts"})
    }

});

relatasApp.controller("liu_hierarchy", function($scope, $http, share,$rootScope) {


    $scope.getDataFor = function (user) {
        if(user == "all"){
            share.selection = {fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
        } else {
            share.selection = user;
        }

        $scope.selection = share.selection;

        share.loadGraphs();
    }

    $scope.getLiuHierarchy = function (hierarchy) {

        var url = '/company/user/hierarchy/insights';

        if(hierarchy && hierarchy !== "Org. Hierarchy"){
            url = '/company/users/for/hierarchy';
            url = fetchUrlWithParameter(url,"hierarchyType",hierarchy.replace(/[^A-Z0-9]+/ig, "_"))
        }

        share.selectedHierarchy = hierarchy;

        $http.get(url)
            .success(function (response) {
                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    // $scope.team = buildTeamProfiles(response.Data)
                    $scope.team = buildAllTeamProfiles(response.companyMembers)

                    if($scope.team.length>1){
                        $scope.selection = {fullName:"Show all team members",emailId:"Show all team members",nameNoImg:"All",noPicFlag:true};
                    } else {
                        $scope.selection = $scope.team[0];
                    }

                    // $scope.selection = $scope.team[0];

                    share.selection = $scope.selection

                    share.team = $scope.team;
                    var usersDictionary = {};

                    share.teamChildren = {};
                    _.each(response.listOfMembers,function (el) {
                        share.teamChildren[el.userEmailId] = el.teamMatesEmailId
                    });

                    if(response.companyMembers.length>0){
                        var companyMembers = buildAllTeamProfiles(response.companyMembers)

                        _.each(companyMembers,function (member) {
                            usersDictionary[member.emailId] = member
                        });

                        share.teamMembers = companyMembers;
                    }


                    share.loadGraphs()
                    share.usersDictionary = usersDictionary;
                }
            });
    }

    $scope.getLiuHierarchy("All Access");

});

function buildAllTeamProfiles(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            noPicFlag:true,
            nameNoImg:el.firstName.substr(0,2).toUpperCase()
        })
    });

    return team;
}

relatasApp.controller("prediction", function ($scope,$http,$rootScope,share){

    $scope.sortType = 'amount';
    $scope.sortReverse = false;

    $scope.sortTable = function (item) {

        var sorType = "amount";

        if(item == "Opportunity Name"){
            sorType = "opportunityName"
        }

        if(item == "Company"){
            sorType = "account"
        }
        if(item == "Stage"){
            sorType = "stageName"
        }
        if(item == "Opp Owner"){
            sorType = "userEmailId"
        }

        if(item == "Selling To"){
            sorType = "contactEmailId"
        }

        if(item == "Contact Name"){
            sorType = "personEmailId"
        }

        if(item == "Type"){
            sorType = "prospect_customer"
        }

        if(item == "Contact Owner"){
            sorType = "ownerEmailId"
        }

        if(item == "closeDate"){
            sorType = "closeDate"
        }

        $scope.sortReverse = !$scope.sortReverse;
        $scope.sortType = sorType;
    }

    $scope.getDataForQtr = function (qtr) {
        var userIds = [];
        if(share.selection && share.selection.fullName === "Show all team members"){
            _.each(share.team,function (tm) {
                userIds.push(tm.userId);
            });
        } else {
            if(share.selection){
                userIds.push(share.selection.userId)
            } else {
                userIds.push(share.liuData.userId)
            }
        }

        var url = "/pulse/forecast/for/quarter?quarter="+qtr;

        if(userIds){
            url = fetchUrlWithParameter(url, "hierarchylist", userIds);
        }

        $scope.noDataFound = false;

        $http.get(url)
            .success(function (response) {
                $scope.opps = response;

                var won = 0;
                $scope.opps.forEach(function (op) {
                    op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat());
                    op.account = fetchCompanyFromEmail(op.contactEmailId);

                    op.regions = null;

                    if(op.geoLocation && op.geoLocation.town){
                        op.regions = op.geoLocation.town
                    }

                    if(op.stageName == "Close Won"){
                        won = won+op.amount;
                    }

                    if(op.geoLocation && op.geoLocation.zone){
                        if(op.regions){
                            op.regions = op.regions+", "+op.geoLocation.zone
                        } else {
                            op.regions = op.geoLocation.zone
                        }
                    }

                    op.amount = parseFloat(op.amount.toFixed(2));
                });

                $scope.opps = _.uniq($scope.opps,"_id");
                $scope.noDataFound = $scope.opps.length===0;
            })
    }

    share.loadGraphs = function () {

        var userIds = [];
        $scope.opps = [];

        if(share.selection && share.selection.fullName === "Show all team members"){
            _.each(share.team,function (tm) {
                userIds.push(tm.userId);
            });
        } else {
            if(share.selection){
                userIds.push(share.selection.userId)
            } else {
                userIds.push(share.liuData.userId)
            }
        }

        var url = "/pulse/forecast";

        if(userIds){
            url = fetchUrlWithParameter(url, "hierarchylist", userIds);
        }

        $scope.loadingDealsQtr1 = true;
        $scope.loadingDealsQtr2 = true;
        $scope.loadingDealsQtrC = true;
        $scope.loadingDealsQtrNext = true;

        $(".ct-chart-q1").empty();
        $(".ct-chart-q2").empty();
        $(".ct-chart-qcurrent").empty();
        $(".ct-chart-qnext").empty();

        setTimeOutCallback(100,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","qc-2"))
                .success(function (response) {
                    $scope.qtrQ1 = response.qtr;
                    $scope.loadingDealsQtr1 = false;
                    drawChart(response,".ct-chart-q1",$scope);
                });
        });

        setTimeOutCallback(300,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","qc-1"))
                .success(function (response) {
                    $scope.qtrQ2 = response.qtr;
                    $scope.loadingDealsQtr2 = false;
                    drawChart(response,".ct-chart-q2",$scope);
                });
        });

        setTimeOutCallback(500,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","current"))
                .success(function (response) {
                    $scope.qtrCurrent = response.qtr;
                    $scope.loadingDealsQtrC = false;
                    drawChart(response,".ct-chart-qcurrent",$scope);
                });
        });

        setTimeOutCallback(800,function () {
            $http.get(fetchUrlWithParameter(url,"quarter","next"))
                .success(function (response) {
                    $scope.qtrNext = response.qtr;
                    $scope.loadingDealsQtrNext = false;
                    drawChart(response,".ct-chart-qnext",$scope);
                });
        });
    }

});

function drawChart(result,id,$scope) {

    if(result && result.data && result.data.length>0){
        result = result.data;
    }

    var label = [],
        won = [],
        target = [],
        commits = [],
        pipeline = [];

    if(result && result.length>0){
        result.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(result,function (el) {
            label.push(moment(el.date).format("MMM YY"));
            won.push(el.won)
            pipeline.push(el.pipeline)
            target.push(el.target)
            commits.push(el.commits)
        })
    }


    var pipelineFilled = [];

    if(id == ".ct-chart-qnext"){
        pipelineFilled = getCumulativeOfArray(pipeline)
    } else {
        pipelineFilled = arrayFiller(pipeline)
    }

    var commitsFilledCumulative = getCumulativeOfArray(commits);
    var wonCumulative = getCumulativeOfArray(won);

    var t = target[target.length-1],
        w = wonCumulative[wonCumulative.length-1],
        c = commitsFilledCumulative[commitsFilledCumulative.length-1],
        p = pipelineFilled[pipelineFilled.length-1];

    var wonPerc = "0%";
    var commitPerc = "0%";
    if(w>t && t == 0){
        wonPerc = "100%";
    } else if(w>t){
        wonPerc = calculatePercentage(w,t==0?1:t)+"%";
    } else {
        wonPerc = calculatePercentage(w,t);
        if(!isNaN(wonPerc)){
            wonPerc = wonPerc+"%"
        } else {
            wonPerc = "0%"
        }
    }

    if(c>t && t == 0){
        commitPerc = "100%"
    } else if(c>t){
        commitPerc = calculatePercentage(c,t==0?1:t)+"%";
    } else {
        commitPerc = calculatePercentage(c,t);
        if(!isNaN(commitPerc)){
            commitPerc = commitPerc+"%"
        } else {
            commitPerc = "0%"
        }
    }

    c = getAmountInThousands(c,2);
    t = getAmountInThousands(t,2);
    w = getAmountInThousands(w,2);
    p = getAmountInThousands(p,2);

    var showLabel = false;
    if(id == ".ct-chart-q1"){
        showLabel = true;
        $scope.t1 = t;
        $scope.w1 = w;
        $scope.wp1 = wonPerc;
        $scope.p1 =p;
        $scope.c1 = c;
        $scope.cp1 = commitPerc;
    }

    if(id == ".ct-chart-q2"){
        $scope.t2 = t;
        $scope.w2 = w;
        $scope.wp2 = wonPerc;
        $scope.p2 =p;
        $scope.c2 = c;
        $scope.cp2 = commitPerc;
    }

    if(id == ".ct-chart-qnext"){
        $scope.tnext = t;
        $scope.wnext = w;
        $scope.wpnext = wonPerc;
        $scope.pnext =p;
        $scope.cnext = c;
        $scope.cpnext = commitPerc;
    }

    if(id == ".ct-chart-qcurrent"){
        $scope.tcurrent = t;
        $scope.wcurrent = w;
        $scope.wpcurrent = wonPerc;
        $scope.pcurrent =p;
        $scope.ccurrent = c;
        $scope.cpcurrent = commitPerc;
    }

    new Chartist.Line(id, {
        labels: label,
        series: [wonCumulative,getCumulativeOfArray(target,true),pipelineFilled,commitsFilledCumulative]
    }, {
        low: 0,
        showArea: true,
        axisY:{
            // showLabel: showLabel,
            // showLabel: false,
            labelInterpolationFnc: function(value){
                return getAmountInThousands(value,2);
            },
            // type: Chartist.FixedScaleAxis,
            // ticks: [10000, 30000, 40000, 80000, 100000]
        },
        axisX:{
            showLabel:false
        },
        plugins: [
            Chartist.plugins.tooltip()
        ],
        width: '250px',
        height: '150px'
    });
}

function arrayFiller(arr) {
    for (var i = 1; i < arr.length; i++) {
        if(arr[i] === 0){
            arr[i] = arr[i-1];
        }
    }

    return arr;
}

function getCumulativeOfArray(arr,testing){
    var new_array = arr.concat(); //Copy initial array incase dont want to pollute original array.
    for (var i = 1; i < arr.length; i++) {
        new_array[i] = new_array[i-1] + arr[i];

    }

    return new_array;
}

relatasApp.controller("oppTargets", function ($scope,$http,$rootScope,share){

    share.getTargets = function (user,filter) {

        $scope.loadingTargets = true;
        var url = fetchUrlWithParameter("/opportunities/by/month/year","userId", user)

        if(filter && filter == 'accessControl'){
            url = fetchUrlWithParameter(url+"&accessControl="+true)
        }

        if(filter && filter != 'accessControl'){
            url = fetchUrlWithParameter(url+"&companyId="+filter)
        }

        setTimeOutCallback(3000,function () {
            drawTargets($scope,$http,url,share)
        })
    }

});

function drawTargets($scope,$http,url,share){
    $http.get(url)
        .success(function (response) {

            var quarters = [
                {
                    quarter:1,
                    startMonth:0,
                    endMonth:2
                },{
                    quarter:2,
                    startMonth:3,
                    endMonth:5
                },{
                    quarter:3,
                    startMonth:6,
                    endMonth:8
                },{
                    quarter:4,
                    startMonth:9,
                    endMonth:11
                }
            ];

            var targetForFy = 0;
            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = _.map(response.Data,"target")

            var values = _.flatten(_.concat(total,won,lost,target))

            var min = Math.min.apply( null, values );
            var max = Math.max.apply( null, values );

            var pipeline = 0;
            var currentQuarter = {};

            _.each(quarters,function (q) {
                if (q.startMonth <= new Date().getMonth() && q.endMonth >= new Date().getMonth()) {
                    currentQuarter = q;
                }
            });

            currentQuarter.values = [];
            currentQuarter.target = 0;
            currentQuarter.pipeline = 0;
            currentQuarter.won = 0;
            currentQuarter.lost = 0;

            var currentMonth = monthNames[new Date().getMonth()]

            $scope.cqHeader = monthNames[currentQuarter.startMonth].substr(0,3)+" - "+monthNames[currentQuarter.endMonth].substr(0,3) +" "+new Date().getFullYear();

            _.each(response.Data,function (value) {

                targetForFy = targetForFy+value.target;

                var thisMonth = monthNames[new Date(value.sortDate).getMonth()];

                if(thisMonth == currentMonth){
                    value.highLightCurrentMonth = true
                    // value.highLightCurrentMonth = "bold"
                }

                pipeline = pipeline+value.openValue

                if (currentQuarter.startMonth <= new Date(value.sortDate).getMonth() && currentQuarter.endMonth >= new Date(value.sortDate).getMonth()){
                    currentQuarter.target = currentQuarter.target+parseFloat(value.target)
                    currentQuarter.won = currentQuarter.won+value.won
                    currentQuarter.pipeline = currentQuarter.pipeline+value.openValue
                    currentQuarter.lost = currentQuarter.lost+value.lost
                }

                value.heightWon = {'height':scaleBetween(value.won,min,max)+'%'}
                value.heightLost = {'height':scaleBetween(value.lost,min,max)+'%'}
                value.heightTotal = {'height':scaleBetween(value.openValue,min,max)+'%'}
                value.heightTarget = {'height':scaleBetween(value.target,min,max)+'%'}

                value.won = numberWithCommas(value.won.r_formatNumber(2));
                value.lost = numberWithCommas(value.lost.r_formatNumber(2));
                value.openValue = numberWithCommas(value.openValue.r_formatNumber(2));
                value.target = numberWithCommas(value.target.r_formatNumber(2));

            });

            share.setTargetForFy(targetForFy)

            var allValues = [currentQuarter.pipeline,currentQuarter.lost,currentQuarter.won,currentQuarter.target]

            var cqMin = Math.min.apply( null, allValues );
            var cqMax = Math.max.apply( null, allValues );

            $scope.cqTarget = numberWithCommas(currentQuarter.target.r_formatNumber(2))
            // $scope.cqLost = {'width':scaleBetween(currentQuarter.lost,cqMin,cqMax)+'%',background: '#6dc3b8'}
            $scope.cqWonStyle = {'width':scaleBetween(currentQuarter.won,cqMin,cqMax)+'%',background: '#368e82'}
            $scope.cqWon = numberWithCommas(currentQuarter.won.r_formatNumber(2))
            $scope.cqPipeline = numberWithCommas(currentQuarter.pipeline.r_formatNumber(2))

            if(currentQuarter.won && !currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#368e82'}
            }

            if(currentQuarter.target){
                $scope.cqWonPercentage = ((currentQuarter.won/currentQuarter.target)*100).r_formatNumber(2)+'%'
            } else {
                $scope.cqWonPercentage = "-"
            }

            if(currentQuarter.won>currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#368e82'}
                $scope.cqWonPercentage = 100+'%'
            }

            var t = _.sum(target);
            var w = _.sum(won);
            var g = t - w;

            var valArr = [];
            valArr.push(t,w,g,pipeline)

            var vmin = Math.min.apply( null, valArr );
            var vmax = Math.max.apply( null, valArr );

            $scope.target = {'width':scaleBetween(t,vmin,vmax)+'%',background: '#6dc3b8'}
            $scope.pipeline = {'width':scaleBetween(pipeline,vmin,vmax)+'%',background: '#767777'}
            $scope.won = {'width':scaleBetween(w,vmin,vmax)+'%',background: '#368e82'}
            $scope.gap = {'width':scaleBetween(g,vmin,vmax)+'%',background: '#e74c3c'}

            if(t){
                $scope.wonPercentage = ((w/t)*100).r_formatNumber(2)+'%'
            } else {
                $scope.wonPercentage = "-"
            }

            if(w>t){
                $scope.won = {'width':100+'%',background: '#368e82'}
                $scope.wonPercentage = 100+'%'
            }

            $scope.targetCount = numberWithCommas(t.r_formatNumber(2));
            $scope.wonCount = numberWithCommas(w.r_formatNumber(2));
            $scope.gapCount = numberWithCommas(g.r_formatNumber(2));
            $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2));

            $scope.targetGraph = response.Data;

            $scope.targetGraph.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            $scope.loadingTargets = false;
        })
}

function buildTeamProfiles(data) {
    var team = [];
    var liu = {}
    _.each(data,function (el) {
        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id
            };
        } else {
            team.push({
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id
            })
        }
    });

    team.unshift(liu);
    return team;
}

function buildTeamProfilesWithLiu(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id
        })
    });

    return team;
}

function autoInitGoogleLocationAPI(share){

    function initAutocomplete() {

        var autocomplete2 = new google.maps.places.Autocomplete(
            (document.getElementById('autocompleteCity')),
            {types:['(cities)']});

        autocomplete2.addListener('place_changed', function(){
            share.setTown($("#autocompleteCity").val())
            return false;
        });
    }

    window.initAutocomplete = initAutocomplete;
}

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue){
    if(parameterValue instanceof Array)
        if(parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if(parameterValue != undefined && parameterValue != null){
        if(baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl+= parameterName + "=" + parameterValue
    }

    return baseUrl
}

function searchResults($scope,$http,keywords,share,searchService,type) {

    var selector = "showResults"+type;
    var typeSelector = type+"s"

    if(keywords && keywords.length > 2){

        searchService.search(keywords).success(function(response){

            if(response.SuccessCode){
                $scope[selector] = true;
                processSearchResults($scope,$http,response.Data,type);
            } else {
                $scope[selector] = false;
                $scope[typeSelector] = [];

                var obj = {
                    fullName: '',
                    name: '',
                    image: '/getContactImage/' + null + '/' + null,
                    emailId:keywords
                }

                $scope[typeSelector].push(obj)

            }
        }).error(function(){
            console.log('error');
        });
    } else {
        $scope[selector] = false;
        $scope[typeSelector] = [];
    }
}

function removeRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp._id
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    var hashtag = type;
    var relation = "decision_maker"

    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    $http.post('/opportunities/remove/people',{type:type,contact:contact,opportunityId:opportunityId})
        .success(function (response) {
            if(response.SuccessCode){
                updateRelationship($http,contact,null,"decisionmaker_influencer",relation)
                deleteHashtag($http,contact.contactId,hashtag,contact.emailId)
                var rmIndex = $scope.opp[typeSelector].indexOf(contact);
                $scope.opp[typeSelector].splice(rmIndex, 1);
            }
        });
}

function addRecipient($scope,$http,contact,type){

    var opportunityId = $scope.opp?$scope.opp._id:null;
    var selector = "showResults"+type;
    var typeSelector = type+"s";

    $scope.partner = '';
    var hashtag = type;
    var relation = "decision_maker"
    if(type === 'partners'){
        hashtag = "partner"
    } else if(type === 'decisionMakers'){
        hashtag = "decisionMaker"
    } else if(type === 'influencers'){
        hashtag = "influencers"
        relation = "influencer"
    }

    addHashtag ($http,contact._id,hashtag)
    updateRelationship($http,contact,relation,"decisionmaker_influencer");

    if(validateEmail(contact.emailId) && opportunityId){
        $http.post('/opportunities/add/people',{type:typeSelector,contact:contact,opportunityId:opportunityId})
            .success(function (response) {
                if(response.SuccessCode){
                    $scope.opp[typeSelector].push(contact)
                    $scope[selector] = false;
                } else {

                    $scope[selector] = false;
                }
            });
    } else {

        $scope[typeSelector] = [];

        if(!$scope.opp){
            $scope.opp = {}
        }

        $scope.opp[typeSelector] = [];
        $scope.opp[typeSelector].push(contact)

        $scope[selector] = false;
    }
}

function addHashtag ($http,p_id,hashtag){
    var str = hashtag.replace(/[^\w\s]/gi, '');
    var obj = { contactId:p_id, hashtag: str};
    $http.post("/api/hashtag/new",obj)
        .success(function (response) {

        });
}

function processSearchResults($scope,$http,response,type) {

    var typeSelector = type+"s";
    var contactsArray = response;
    $scope[typeSelector] = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;
                obj.mobileNumber = contactsArray[i].mobileNumber;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName,
                    mobileNumber: contactsArray[i].mobileNumber
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;
            obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;
            obj.type = type

            if(!userExists(obj.emailId) && validateEmail(obj.emailId)){
                $scope[typeSelector].push(obj)
            }

            function userExists(username) {
                return $scope[typeSelector].some(function(el) {
                    return el.emailId === username;
                });
            }
        }
    }
}

function updateRelationship($http,contact,relation,relationKey,relationType){

    if(contact.contactId){
        var id = contact.contactId
    } else {
        id = contact._id
    }

    var reqObj = {contactId:id,type:relation,relationKey:relationKey,relation:relationType};

    $http.post('/opportunities/contacts/update/reltionship/type',reqObj)
        .success(function(response){

        });
}

function recalculateOppValues(stage) {
    _.each(stage.values,function (val) {
        stage.sumOfAmount = stage.sumOfAmount+val.amount;
    });

    var x = Math.floor(stage.sumOfAmount * 100) / 100;
    stage.sumOfAmount = x.r_formatNumber(2);
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showVerticalList = false;
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

relatasApp.directive('searchResultsContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResultscontact">' +
        '<div ng-repeat="item in contacts track by $index"> ' +
        '<div class="clearfix cursor" ng-click="selectContact(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function deleteHashtag($http,p_id,hashtag,contactEmailId) {

    if(p_id){
        $http.get("/api/opportunities/hashtag/delete?contactId=" + p_id + "&hashtag=" + hashtag+"&contactEmailId="+contactEmailId+"&type="+hashtag)
            .success(function (response) {
                if (response.SuccessCode) {
                    // toastr.success("Hashtag Deleted");
                }
                else {
                    // toastr.error("Hashtag Delete Failed. Please try again later");
                }
            });
    }
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function attachComponents (existingComponent,newComponent){
    return existingComponent?existingComponent+newComponent:newComponent
}

function sendEmail($scope,$http,subject,body,prevMessage,share,userId,emailId,mobileNumber,liu,contactDetails,internalMailRecName){

    var respSentTimeISO = moment().format();

    if(!checkRequired(subject)){
        // toastr.error("Please enter the subject.")
    }
    else if(!checkRequired(body)){
        // toastr.error("Please enter the message.")
    }
    else{

        if(prevMessage && prevMessage.dataObj != null){
            body = body+'\n\n\n'+prevMessage.dataObj.bodyContent;
            $scope.reminder = prevMessage.dataObj.compose_email_remaind
        }

        var obj = {
            email_cc:$scope.add_cc.length>0?$scope.add_cc:null,
            receiverEmailId:internalMailRecName?emailId:$scope.cEmailId,
            receiverName:internalMailRecName,
            message:body,
            subject:subject,
            receiverId:"",
            docTrack:true,
            trackViewed:true,
            remind:$scope.reminder,
            respSentTime:respSentTimeISO,
            isLeadTrack:false,
            newMessage:prevMessage?false:true
        };

        if(internalMailRecName){
            obj.receiverEmailId = emailId
            obj.receiverName = internalMailRecName
            obj.receiverId = ''
        }

        if(prevMessage && prevMessage != null && prevMessage.updateReplied){
            obj.updateReplied = true;
            obj.refId = prevMessage.dataObj.interaction.refId
        }

        //Used for Outlook
        if(prevMessage && prevMessage.dataObj && prevMessage.dataObj.interaction && prevMessage.dataObj.interaction.refId){
            obj.id = prevMessage.dataObj.interaction.refId
        }

        $http.post("/messages/send/email/single/web",obj)
            .success(function(response){
                if(response.SuccessCode){

                }
                else{
                }
            })
    }
}

function drawPrediction (ctx,labels,data,noScaleLabels,onlyTarget) {

    ctx.height = 200;

    new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: '',
                data: data,
                borderWidth: 1,
                borderColor:!onlyTarget?"#368e82":"#fc913a",
                backgroundColor:!onlyTarget?"rgba(109, 195, 184, 0.44)":"rgba(252, 145, 58, 0.51)"
            }]
        },
        options: noScaleLabels?optionsNoLabels:options

    });
}

var options = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:false,
                scaleOverride:true,
                steps: 10,
                stepValue: 6,
                max: 100 //max value for the chart is 60
            },
            gridLines: {
                display: false
            }
        }],
        xAxes: [{
            display: false,
            gridLines: {
                display: false
            }
        }]
    },
    elements: {
        point: {
            radius: 0
        }
    },
    legend: {
        display: false,
        labels: {
            fontColor: 'rgb(255, 99, 132)'
        }
    },
    responsive: true,
    maintainAspectRatio: true
}

var optionsNoLabels = {
    scales: {
        yAxes: [{
            display: false,
            ticks: {
                beginAtZero:false,
                scaleOverride:true,
            steps: 10,
            stepValue: 6,
            max: 100 //max value for the chart is 60
            },
            gridLines: {
                display: false
            }
        }],
        xAxes: [{
            display: false,
            gridLines: {
                display: false
            }
        }]
    },
    elements: {
        point: {
            radius: 0
        }
    },
    legend: {
        display: false,
        labels: {
            fontColor: 'rgb(255, 99, 132)'
        }
    },
    responsive: true,
    maintainAspectRatio: true
}

var styles = [
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "/images/m1.png",
        width: 53
    }
]
