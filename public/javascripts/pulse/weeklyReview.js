
var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','angular-loading-bar','ngLodash','textAngular']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

relatasApp.service('share', function () {
    return {
        isOk:false,
        meetingOb:null,
        userId:null,
        selectedParticipantName:"",
        getIsOk:function(){
            return this.isOk;
        },
        setUserId:function(userId){
            this.userId = userId;
        },
        liuDetails:function(user){
            this.liuData = user;
        },
        getUserId:function(){
            return this.userId
        },
        setUserEmailId:function(emailId){
            this.userEmailId = emailId;
        },
        setTeamMembers:function(members){
            this.team = members;
        },
        setTargetForFy:function(value){
            this.targetForFy = value;
        },
        getTeamMembers:function(){
            return this.team;
        },
        getLiuUserEmailId:function(){
            return this.userEmailId;
        },
        setSelectedParticipantName:function(name){
            this.selectedParticipantName = name;
        },
        getSelectedParticipantName:function(){
            return this.selectedParticipantName;
        },
        setTown:function(geoLocationTown){

            this.geoLocationTown = geoLocationTown;
        },
        getTown:function(){
            return this.geoLocationTown;
        }
    };
});

relatasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

relatasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }

        else toastr.error("Please enter search content")
    };
});

var timezone;
relatasApp.controller("logedinUser", function ($scope,$http,share,$rootScope) {

    getLiuProfile($scope, $http, share,$rootScope,function (response) {
        if(response.SuccessCode) {

            if (checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)) {
                timezone = response.Data.timezone.name;
            }

            var fyMonth = "April";
            if (response.companyDetails.fyMonth) {
                fyMonth = response.companyDetails.fyMonth
            }

            response.Data.fyMonth = fyMonth;

            share.liuDetails(response.Data);
            share.setUserId(response.Data._id);
            share.setUserEmailId(response.Data.emailId);
            share.companyDetails = response.companyDetails
            share.setCompanyDetails(response.companyDetails, response.Data)
            share.primaryCurrency = "USD";
            share.currenciesObj = {};

            share.companyDetails.currency.forEach(function (el) {
                share.currenciesObj[el.symbol] = el;
                if(el.isPrimary){
                    share.primaryCurrency = el.symbol;
                }
            });

            share.loadTeamCommits()
        }
    })

});

relatasApp.controller("topContextMenu", function ($scope, $http,$rootScope,share){

    $scope.currentMonth = moment().format("MMM YYYY")
    $scope.toggleHierarchyList = function () {
        $scope.showHierarchyList = !$scope.showHierarchyList
    }

    share.setCompanyDetails = function (companyDetails,liu) {

        $scope.hierarchyList = ["Org. Hierarchy"]

        if(companyDetails && companyDetails.isRevenueHierarchySetup){
            $scope.hierarchyList = ["Org. Hierarchy","Rev. Hierarchy","All Hierarchy"]
        }

        $rootScope.hierarchySelection = $scope.hierarchyList[0];
        
        $scope.liu = liu;

        var accessControlProd = [],accessControlRegion = [],accessControlVertical = [];

        if(share.liuData){
            _.each(share.liuData.productTypeOwner,function (el) {
                accessControlProd.push({name:el})
            })
            _.each(share.liuData.verticalOwner,function (el) {
                accessControlVertical.push({name:el})
            })
            _.each(share.liuData.regionOwner,function (el) {
                accessControlRegion.push({region:el})
            })
        }

        $rootScope.currency = "USD"
        if(companyDetails && companyDetails.currency){
            _.each(companyDetails.currency,function (el) {
                if(el.isPrimary){
                    $rootScope.currency = el.symbol
                }
            })
        }

        if(companyDetails && companyDetails.currency){

            _.each(companyDetails.currency,function (el) {
                if(el.isPrimary){
                    $rootScope.currency = el.name
                }
            })
        }

    }

    $scope.switchHierarchy = function (filter) {
        $rootScope.hierarchySelection = filter;
        filter = filter.toLowerCase();

        if(_.includes(filter,"rev")){
            $scope.getTeam("/company/user/revenue/hierarchy")
        } else if(_.includes(filter,"prod")){
            $scope.getTeam("/company/user/product/hierarchy")
        } else if(_.includes(filter,"all")){
            $scope.getTeam("/company/user/all/hierarchy")
        } else {
            $scope.getTeam()
        }
        $scope.showHierarchyList = false;
    }

    var allProfiles= null;

    $scope.getTeam = function (url) {
        $scope.ifProducts = false;

        if(!url){
            url = '/company/user/hierarchy'
        }

        url = fetchUrlWithParameter(url,"forCommits",true)

        $scope.ifTeam = true;

        $http.get(url)
            .success(function (response) {

                $scope.team = [];
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    $scope.team = buildTeamProfilesWithCommits(response.Data,response.listOfMembers,share)

                    share.teamAndChildren = {};

                    _.each($scope.team,function (el) {
                        share.teamAndChildren[el.emailId] = el;
                    })

                    if($scope.team.length == 1){
                        $scope.selection = $scope.team[0];
                    } else {

                        $scope.selection = {
                            nameNoImg:"All",
                            fullName: "All Team Members",
                            noPicFlag:true
                        }
                    }

                    if($rootScope.orgHead){

                        $scope.selection = {
                            nameNoImg:"All",
                            fullName: "All Team Members",
                            noPicFlag:true
                        }
                    }

                    share.setUserEmailId($scope.selection.emailId)

                    var usersDictionary = {},
                        usersIdDictionary = {};

                    if(response.companyMembers.length>0){
                        allProfiles = buildTeamProfilesWithLiu_team(response.companyMembers)
                        _.each(allProfiles.team,function (member) {
                            usersIdDictionary[member.userId] = member
                            usersDictionary[member.emailId] = member
                        })
                    }

                    share.companyUsersDictionary = usersIdDictionary;

                    $scope.usersDictionary = usersDictionary;

                    function checkLiuData(){
                        if(share.liuData){

                            var tz = share.liuData.timezone && share.liuData.timezone.name?share.liuData.timezone.name:"UTC";
                            $scope.autoQuarter = getCurrentQuarter(setQuarter(share.companyDetails.fyMonth,tz));

                            $scope.quarterSelected = "Apr-Jun";

                            $scope.quarter = {
                                quarter:"quarter1",
                                name:"Apr-Jun"
                            };

                            $scope.quarters = [
                                {
                                    name:"Apr-Jun",
                                    quarter:"quarter1"
                                },
                                {
                                    name:"Jul-Sep",
                                    quarter:"quarter2"
                                },
                                {
                                    name:"Oct-Dec",
                                    quarter:"quarter3"
                                },
                                {
                                    name:"Jan-Mar",
                                    quarter:"quarter4"
                                }
                            ]

                            _.each($scope.quarters,function (q) {
                                if(q.quarter == $scope.autoQuarter){
                                    $scope.quarter = q
                                    $scope.quarterSelected = q.name
                                }
                            });

                            window.localStorage['teamMembers'] = JSON.stringify($scope.team);
                            window.localStorage['teamMembersDictionary'] = JSON.stringify(usersDictionary);
                            share.showUsers($scope.team,share.liuData,allProfiles.allCompanyUserIds,$scope.autoQuarter)

                        } else {
                            setTimeOutCallback(1000,function () {
                                checkLiuData();
                            })
                        }
                    }

                    checkLiuData();
                    share.setTeamMembers(usersDictionary);
                } else {
                    share.resetTeamRank()
                }
            });
    }

    share.loadTeamCommits = function () {
        $scope.getTeam()
    }

    $scope.users = [];

    $scope.usersWithTargets = "With targets";
    $scope.allUsers = "All users";
    $scope.withTargetSet = "With targets";

    closeAllDropDownsAndModals($scope,".list");

    $scope.registerDatePickerId = function(id){

        $('#'+id).datetimepicker({
            timepicker:false,
            validateOnBlur:false,
            // minDate: $scope.startDt && id == "endDt"?new Date($scope.startDt):new Date(),
            onSelectDate: function (dp, $input){

                dp = new Date(dp)
                dp.setHours(23)
                dp.setMinutes(59)
                dp.setSeconds(59)

                $scope.$apply(function () {
                    $scope[id] = moment(dp).tz(timezone).format("DD MMM YYYY");
                    $scope.dateRange = "Before "+$scope[id]
                    $scope.showDateRangePicker = false;
                    $scope.productSelected = "All products";
                    $scope.locationSelected = "All regions";
                });
            }
        });
    }

});

relatasApp.controller("weeklyReview",function ($scope,$http,share,$rootScope){

    // Testing. Remove this
    share.resetNavigation = function () {
        $scope.currentSelection = null;
        $scope.currentMonthSelection = null;
        $scope.currentQtrSelection = null;
    }

    $scope.toggleOppsClosingInTimeFrame = function (opps) {
        toggleOppsClosingInTimeFrame($scope,opps,$scope.viewMode)
    }

    share.setQuarterDateRange = function () {
        setQuarterDateRange($scope,$http,share)
    }

    $scope.getAllCommitCutOffDates = function () {

        $http.get("/review/get/all/commit/cutoff")
            .success(function (response) {
                if(response){
                    $rootScope.weeklyCommitCutOff = "Commit Close Date: "+moment(response.week).tz("UTC").format(standardDateFormat())
                    $rootScope.monthlyCommitCutOff= "Commit Close Date: "+moment(response.month).tz("UTC").format(standardDateFormat())
                    $rootScope.quarterlyCommitCutOff= "Commit Close Date: "+moment(response.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
                }
            })
    }

    $scope.getAllCommitCutOffDates();

    $scope.close = function () {
        share.clearAllData();
        share.closeCommitView()
        $scope.currentSelection = null;
        $scope.currentMonthSelection = null;
        $scope.currentQtrSelection = null;
    }

    share.setDateRange = function (start,end,mode) {

        if(start){

            if(mode == "qtr"){
                // $scope.dataForRange = moment(start).format('MMM') +"-"+moment(end).format('MMM YY')
            } else {
                $scope.currentMonthSelection = {
                    start:start,
                    end:end
                }

                $scope.dataForRange = moment(start).format('MMM')
            }

        } else {
            setDateRange();
        }
    }

    function setDateRange(){

        if(share.companyDetails){
            var weekStartDateTime = moment().startOf("isoWeek");
            var weekEndDateTime = moment().endOf("isoWeek");

            var startOfWeek = moment(weekStartDateTime).format('DD MMM')
            var endOfWeek = moment(weekEndDateTime).format('DD MMM')

            if(new Date(weekStartDateTime)>new Date()){
                weekStartDateTime = moment(weekStartDateTime).subtract(1,"week")
                weekEndDateTime = moment(weekEndDateTime).subtract(1,"week")
                startOfWeek = moment(weekStartDateTime).format('DD MMM')
                endOfWeek = moment(weekEndDateTime).format('DD MMM')
            }

            $scope.currentSelection = {
                weekStartDateTime:new Date(weekStartDateTime),
                weekEndDateTime:new Date(weekEndDateTime)
            }

            $scope.dataForRange = startOfWeek+"-"+endOfWeek;
        } else {
            setTimeOutCallback(1000,function () {
                setDateRange()
            })
        }
    }

    setDateRange();
    initializeMonthlyCommitScopesAndMethods($scope,$rootScope,$http,share)
    initializeQuarterlyCommitScopesAndMethods($scope,$rootScope,$http,share)

    share.resetCommitData = function () {
        $scope.monthlyCommits = {
            oppValue:0,
            selfCommitValue:0,
            opps:[],
            selfCommitValueFormat:0,
            response:null,
            graph:{}
        }

        $scope.weeklyCommits = {
            oppValue:0,
            selfCommitValue:0,
            opps:[],
            selfCommitValueFormat:0,
            response:null,
            graph:{}
        }

        $scope.quarterlyCommits = {
            oppValue:0,
            selfCommitValue:0,
            opps:[],
            selfCommitValueFormat:0,
            response:null,
            graph:{}
        }

        $scope.commitsByUsers = [];
    }

    $scope.viewCommitFor = function (mode,user) {
        $scope.viewMode = mode;
        closeAllOppTables($scope)

        if(mode == 'week'){
            setDateRange();
            $scope.viewModeFull = "Weekly"
            share.reviewUser(user)
            $scope.currentMonthSelection = null
            $scope.currentQtrSelection = null
        }

        if(mode == 'month'){
            $scope.viewModeFull = "Monthly"
            $scope.currentSelection = null
            $scope.currentQtrSelection = null
            fetchCommitForMonth($scope,$http,share,$rootScope,mode,$scope.currentMonthSelection,user)
        }

        if(mode == 'qtr'){
            $scope.viewModeFull = "Quarterly"
            $scope.currentMonthSelection = null
            $scope.currentSelection = null
            fetchCommitForQuarter($scope,$http,share,$rootScope,mode,$scope.currentQtrSelection,user)
        }
    }

    $scope.navigateCommits = function (type,currentSelection,member,mode) {

        closeAllOppTables($scope)
        share.clearAllTeamCommits()

        if(mode == 'week'){

            var weekStartDateTime = moment(currentSelection.weekStartDateTime).subtract(1,"week")
            var weekEndDateTime = moment(currentSelection.weekEndDateTime).subtract(1,"week")

            if(!currentSelection){
                weekStartDateTime = moment().startOf("isoWeek");
                weekEndDateTime = moment().endOf("isoWeek");
            }

            if(type == 'next' && currentSelection){
                weekStartDateTime = moment(currentSelection.weekStartDateTime).add(1,"week")
                weekEndDateTime = moment(currentSelection.weekEndDateTime).add(1,"week")
            }

            var startOfWeek = moment(weekStartDateTime).format('DD MMM')
            var endOfWeek = moment(weekEndDateTime).format('DD MMM')

            $scope.currentSelection = {
                weekStartDateTime:weekStartDateTime,
                weekEndDateTime:weekEndDateTime
            }

            $scope.nextButtonDisabled = false;

            var maxAllowedDate = getCommitDayTimeEnd(share.companyDetails);

            if(new Date(maxAllowedDate)>=new Date(weekStartDateTime)){
                $scope.nextButtonDisabled = true;
            } else {
                $scope.nextButtonDisabled = false;
            }

            if(new Date(weekStartDateTime)<= new Date(maxAllowedDate) && new Date(weekEndDateTime)<=new Date(maxAllowedDate)){
                $scope.nextButtonDisabled = false;
            }

            $scope.dataForRange = startOfWeek+"-"+endOfWeek;

            if($scope.viewingTeamData){
                share.fetchTeamWeekCommit(weekStartDateTime,weekEndDateTime,member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[])
            } else {
                fetchCommitForWeek($scope,$http,share,$rootScope,type,weekStartDateTime,weekEndDateTime,member)
            }
        }

        if(mode == "month"){
            navigateMonthlyCommits($scope,$http,share,$rootScope,$scope.currentMonthSelection,type,member,$scope.viewingTeamData)
        }

        if(mode == "qtr"){
            navigateQuarterlyCommits($scope,$http,share,$rootScope,$scope.currentQtrSelection,type,member,$scope.viewingTeamData)
        }
    }

    share.setCssForWeeklyReview = function () {
        $rootScope.forWeeklyReview = "forWeeklyReview"
    }
    share.removeCssForWeeklyReview = function () {
        $rootScope.forWeeklyReview = ""
    }

    closeAllDropDownsAndModals($scope,".list");
    closeAllDropDownsAndModals($scope,".date-selection");

    getOppStages();
    share.clearAllData = function () {
        closeAllOppTables($scope);
        share.clearAllTeamCommits()
        $scope.viewingTeamData = false;
        $(".commit-switch button").removeClass("selected");
        $(".commit-switch button:first-child").addClass("selected");
        $(".pipeline-comparison").empty();
        $(".switch").show(100);
    }

    share.clearSelectedMember = function () {
        $scope.member = {};
        $scope.commitsByUsers = []
    }

    function getOppStages(){

        if(share.opportunityStages){
            $scope.stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }
    
    $scope.updateStoreValue = function (value) {
        value.selfCommitValue = parseFloat(value.selfCommitValueFormat)
    }

    $scope.formatPlaceHolder = function (value,numbersOnly) {

        if(numbersOnly){
            value.selfCommitValueFormat = value.selfCommitValue
        } else {
            value.selfCommitValueFormat = getAmountInThousands(value.selfCommitValue,2,share.primaryCurrency == "INR")
        }
    }

    $scope.sortType = 'closeDateSort';
    $scope.sortReverse = false;

    $scope.closeTable = function () {
        closeAllOppTables($scope)
    }
    
    $scope.showPrevCommits = function (stage) {
        showPrevCommits($scope,$rootScope,$http,share,stage)
    }

    $scope.showOppsWeekly = function (stage) {
        showOppsWeekly($scope,$rootScope,$http,share,stage)
    }

    $scope.showOpps = function (stage) {
        showOpps($scope,$rootScope,$http,share,stage)
    }

    $scope.showOppsForStage = function (stage,prev) {
        closeAllOppTables($scope);
        $scope.weekByWeek = false;
        $scope.prevAndCurrentWeek = true;

        $scope.weekSelection = $scope.dataForRange;
        $scope.stageSelection = stage.relatasStage

        if($rootScope.fySelection){
            $scope.weekSelection = $rootScope.fySelection
        } else {
            $scope.weekSelection = $scope.dataForRange;
        }

        var oppsFiltered = [],
            oppsFlattened = []

        $scope.dontShowRisk = false

        if(stage.relatasStage != "Close Won" && stage.relatasStage != "Close Lost"){
            $scope.dontShowRisk = true
        }

        if(prev){
            _.each($scope.commitsPrev.opportunities, function (oppObj) {
                _.each(oppObj.opps, function (op) {
                    oppsFlattened.push(op)
                })
            })

            oppsFiltered = oppsFlattened.filter(function (op) {
                return op.stageName == stage.relatasStage
            })
        } else {
            _.each($scope.commitsCurrent, function (op) {
                oppsFlattened.push(op)
            })

            oppsFiltered = oppsFlattened.filter(function (op) {
                return op.relatasStage == stage.relatasStage
            })
        }

        paintOppsTable($scope,$rootScope,$http,share,oppsFiltered,true)
    }

    $scope.hoverIn = function (element) {
        element.displayToolTip = true
    }

    $scope.hoverOut = function (element) {
        element.displayToolTip = false
    }

    var userId = null;

    share.reviewUser = function (user,startDate,endDate,onlyPipelineReview) {
        // $scope.viewMode = "week"
        $(".commit-switch button:nth-child(2)").addClass("selected");
        setTimeOutCallback(2000,function () {
            share.clearAllData();
            $(".commit-switch button:nth-child(2)").addClass("selected");
        });
        loadTeamMembers()

        if(user){
            user.nameTruncate = getTextLength(user.fullName,12)
            $scope.member = user;
            userId = user.userId;
        }

        closeAllOppTables($scope)
        // userReportsInit($scope,$http,userId);
        checkOppStagesLoaded(userId,startDate,endDate);

        if(!$scope.viewMode){
            $scope.viewMode = "month";
            $scope.viewModeFull = "Monthly"
        }

        if($scope.viewMode == "month"){
            fetchCommitForMonth($scope,$http,share,$rootScope,"month",null,user)
        } else {
            fetchCommitForWeek($scope,$http,share,$rootScope,'week',startDate,endDate,user)
            $scope.loadOppsForWeek = false;
            $scope.weekByWeek = false;
            $scope.opps = [];

            if(share && share.userId){
                $rootScope.editAccess = share.userId == userId
            }

            weeklyPastCommitHistory($scope,$rootScope,$http,share,userId)
        }
    }

    subMenu($scope)

    var allTemplates = ["fm","pr","co","pc","reports","dp"]
    $scope[allTemplates[1]]=true;
    selectItem($scope,$http,share,$rootScope,"Commits")

    $scope.selectItem = function (selection) {

        if(selection.shortHand == "pr"){
            var nextWeekMoment = moment().add(1,'week');
            var nextstartOfWeek = moment(nextWeekMoment).startOf('isoweek').format('DD MMM')
            var nextendOfWeek = moment(nextWeekMoment).endOf('isoweek').format('DD MMM')
            $scope.dataForRange = nextstartOfWeek+"-"+nextendOfWeek
        } else {
            var startOfWeek = moment().startOf('isoweek').format('DD MMM')
            var endOfWeek = moment().endOf('isoweek').format('DD MMM')

            $scope.dataForRange = startOfWeek+"-"+endOfWeek;
        }

        selectItem($scope,$http,share,$rootScope,selection.name);
        loadTemplate(selection,$scope,allTemplates)
    }

    $scope.registerDp = function () {
        $scope.registerDatePickerId("startDate")
        $scope.registerDatePickerId("endDate")
    }

    $scope.getData = function (filter) {
        $scope.opps = [];
        $scope.loadOppsForWeek = false;
        $scope.weekByWeek = false;

        var url = "/review/past/commits"
        if($scope.startDateCache && $scope.endDateCache){
            url = "/review/past/commits?dateMin="+$scope.startDateCache+"&dateMax="+$scope.endDateCache
        } else {
            // toastr.warning("No start and end date found. Showing for default date range.")
        }

        if(userId){
            url = fetchUrlWithParameter(url,"userId",userId)
        }

        if(filter){
            url = fetchUrlWithParameter(url,"filter",filter)
        }

        $http.get(url)
            .success(function (response) {
                setRangeSelection ($scope,response,$scope.startDateCache,$scope.endDateCache)
                pipelineComparison($scope,$rootScope,$http,share,response)
            });
    }

    $scope.registerDatePickerId = function(id){

        $('#'+id).datetimepicker({
            timepicker:false,
            maxDate:new Date(moment().endOf("isoweek")),
            validateOnBlur:false,
            closeOnDateSelect:true,
            onSelectDate: function (dp, $input){
                $scope.$apply(function () {
                    $scope[id+"Cache"] = dp;
                    $scope[id] = moment(dp).format("DD MMM YYYY")
                });
            }
        });
    }

    function checkOppStagesLoaded(userId,startDate,endDate){
        if(share.opportunityStages){
            processPipelineValues($scope,share,$http,userId);
        } else {
            setTimeOutCallback(1000,function () {
                checkOppStagesLoaded(userId,startDate,endDate)
            })
        }
    }

    $scope.closeList = function (member) {

        $scope.viewSelectedUserCommits(member)
        // share.reviewUser(member);
        share.getAllTasks(member);
        $scope.member = member;
        $scope.currentSelection = null;
        $scope.currentQtrSelection = null;
        $scope.currentMonthSelection = null;
        $scope.selectFromList = false;
        share.teamList = member.children.teamMatesEmailId
        share.clearTeamCommits();
        share.resetCommitData();
        // setDateRange()
    }

    $scope.viewTeamCommits = function (member,viewMode) {
        closeAllOppTables($scope)
        $scope.viewingTeamData = true;
        $scope.currentWeek = false;
        $(".switch").hide(500);
        share.teamMemberSelected = member;

        $scope.viewMode = "month";
        share.resetNavigation()
        share.fetchTeamCommitForMonth(member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[])
    }

    share.setViewMode = function(viewMode){
        $scope.viewMode = viewMode;
    }

    $("body").on('click', 'button', function (e) {
        $(".commit-switch button").removeClass("selected");
        $(this).addClass("selected");
    });

    $scope.viewSelectedUserCommits = function (user) {
        $scope.viewingTeamData = false;
        $scope.exceptionalAccess = false;
        $scope.viewMode = "month"

        $scope.currentQtrSelection = null;
        $scope.currentMonthSelection = null;
        $scope.currentSelection = null;

        share.reviewUser(user)

        $(".switch").show(500);
    }

    $scope.viewExceptionalAccess = function (member) {
        closeAllOppTables($scope)
        $scope.exceptionalAccess = true;
        $scope.viewingTeamData = false;
        $scope.currentWeek = false;
        $(".switch").hide(500);
        $scope.stageSelection = share.commitStage;

        $http.get("/opportunities/by/exceptional/access?accessControl=true&userId="+member.userId)
            .success(function (response) {
                if(response && response.SuccessCode && response.Data && response.Data.length>0){
                    paintOppsTable($scope,$rootScope,$http,share,response.Data,null,"ea")
                }
            });
    }

    $scope.saveWeeklyCommits = function (commits) {
        var obj = {
            week:0,
            month:0,
            quarter:0
        }

        var alphanumericFound = false;

        if(commits.selfCommitValue && String(commits.selfCommitValue).match(/[a-z]/i) || commits.selfCommitValue<0) {
            alphanumericFound = true;
        }

        obj.week = commits.selfCommitValue

        if(alphanumericFound){
            toastr.error("Please enter only positive numbers for opportunity amount")
        } else {

            $http.post("/review/meeting/update/commit/value/weekly",obj)
                .success(function (response) {
                    if(response.SuccessCode){
                        toastr.success("Commits successfully updated")
                        if($scope.member){
                            weeklyPastCommitHistory($scope,$rootScope,$http,share,$scope.member.userId)
                        }
                    } else {
                        toastr.error("Commits not updated. Please try again later")
                    }
                });
        }
    }

    function loadTeamMembers(){
        
        if(window.localStorage['teamMembers']){
            try {
                $scope.team = JSON.parse(window.localStorage['teamMembers']);
            } catch(err){
                setTimeOutCallback(1000,function () {
                    loadTeamMembers()
                })
            }
        } else {
            setTimeOutCallback(1000,function () {
                loadTeamMembers()
            })
        }
    }
    
});

function weeklyPastCommitHistory($scope,$rootScope,$http,share,userId){

    $scope.graphLoading = true;

    setTimeOutCallback(2000,function () {

        var url = "/review/past/commits";

        if(userId){
            url = fetchUrlWithParameter(url,"userId",userId)
        }

        $http.get(url)
            .success(function (response) {
                if(response.SuccessCode){
                    $rootScope.fySelection = "FY "+moment(response.Data.fyRange.start).year()+"-"+moment(response.Data.fyRange.end).year()
                }
                pipelineComparison($scope,$rootScope,$http,share,response)
            });
    })
}

relatasApp.controller("teamCommit",function ($scope,$http,share,$rootScope) {

    $scope.toggleOppsClosingInTimeFrame = function (opps) {
        toggleOppsClosingInTimeFrame($scope,opps,$scope.viewMode)
    }

    $scope.selectedUserValues = function (user) {
        if(user.selected){
            if(isNumber(user.oppsValueUnderCommitStageSort)){
                $scope.oppValueSelected = $scope.oppValueSelected+user.oppsValueUnderCommitStageSort;
            }

            if(isNumber(user.valueSort)){
                $scope.commitValueSelected = $scope.commitValueSelected+user.valueSort;
            }

        } else {
            $scope.commitValueSelected = $scope.commitValueSelected-user.valueSort;
            $scope.oppValueSelected = $scope.oppValueSelected-user.oppsValueUnderCommitStageSort;
        }

        $scope.commitValueSelectedForDisplay = getAmountInThousands($scope.commitValueSelected,2,share.primaryCurrency=="INR")
        $scope.oppValueSelectedForDisplay = getAmountInThousands($scope.oppValueSelected,2,share.primaryCurrency=="INR")
    }

    $scope.closeCommitTable = function (user) {
        $scope.commitValueSelected = 0;
        $scope.oppValueSelected = 0;
    }

    $scope.viewCommitFor = function (mode) {
        $scope.viewMode = mode
        share.setViewMode(mode);
        $scope.currentWeek = false;
        $scope.commitsByUsers = [];
        $(".switch").hide()

        var users = share.teamMemberSelected.children && share.teamMemberSelected.children.teamMatesUserId?share.teamMemberSelected.children.teamMatesUserId:[]

        if(mode == 'week'){
            $scope.viewModeFull = "Weekly"
            share.setDateRange()
            share.fetchTeamWeekCommit(null,null,users)
        }

        if(mode == 'month'){
            $scope.viewModeFull = "Monthly"
            fetchTeamCommitForMonth($scope,$http,share,$rootScope,users)
        }

        if(mode == 'qtr'){
            $scope.viewModeFull = "Quarterly"
            share.quarterCommitCutOff = null;
            fetchTeamCommitForQuarter($scope,$http,share,$rootScope,users)
        }
    }

    share.fetchTeamWeekCommit = function (weekStartDateTime,weekEndDateTime,users) {
        fetchCommitForWeekForTeam($scope,$http,share,$rootScope,'week',weekStartDateTime,weekEndDateTime,users)
    }

    initializeMonthlyCommitScopesAndMethodsForTeam($scope,$http,share,$rootScope);
    initializeQuarterlyCommitScopesAndMethodsForTeam($scope,$http,share,$rootScope);

    $scope.closeCommitTable = function () {
        $scope.commitsByUsers = [];
    }

    share.clearTeamCommits = function () {
        $scope.commitsByUsers = [];
    }

    $scope.showOppsWeekly = function (stage) {
        showOppsWeekly($scope,$rootScope,$http,share,stage)
    }

    $scope.sortType = 'valueSort';
    $scope.sortReverse = true;

    $scope.getTeamCommitsWeek = function (data) {

        $scope.selectionType = data.dateRange;
        $scope.commitsByUsers = [];
        $scope.currentWeek = false;
        var userHashMap = share.companyUsersDictionary;
        var oppList = data.response.opps;

        if(new Date()> new Date(data.response.commitWeek)){
            oppList = data.response.commitsCurrentWeek.opportunities
        }

        var startOfWeek = moment(data.response.commitWeek).startOf("isoWeek"),
            endOfWeek = moment(data.response.commitWeek).endOf("isoWeek");

        var opportunitiesObj = {};

        if(oppList && oppList.length>0){
            _.each(oppList,function (op) {
                if(opportunitiesObj[op.userEmailId]){
                    opportunitiesObj[op.userEmailId].push(op)
                } else {
                    opportunitiesObj[op.userEmailId] = [];
                    opportunitiesObj[op.userEmailId].push(op)
                }
            })
        }

        if(data.response.commitsCurrentRaw && data.response.commitsCurrentRaw.length>0){

            var rawData = data.response.commitsCurrentRaw

            _.each(rawData,function (el) {

                el.profile = userHashMap[el.userId];

                el.value = el.week.userCommitAmount

                if(isNumber(parseFloat(el.value))){
                    el.valueSort = parseFloat(el.value)
                }

                el.value = getAmountInThousands(el.value,2)
                el.emailId = el.profile.emailId;
                el.commitDateTime = moment(el.date).format(standardDateFormat());
                el.oppsValueUnderCommitStage = 0;
                el.oppsValueUnderCommitStageSort = 0;

                $scope.commitsByUsers.push(el)
            })
        }

        var nonExistingUsers = _.differenceBy(share.teamList,_.map($scope.commitsByUsers,"emailId"))
        if(nonExistingUsers && nonExistingUsers.length>0){
            _.each(nonExistingUsers,function (el) {
                $scope.commitsByUsers.push({
                    value:0,
                    valueSort:0,
                    profile:share.team[el],
                    commitDateTime:"-",
                    oppsValueUnderCommitStage:0,
                    oppsValueUnderCommitStageSort:0
                })
            })
        }

        $scope.commitValueSelected = 0;
        $scope.oppValueSelected = 0;

        _.each($scope.commitsByUsers,function (el) {

            el.oppsValueUnderCommitStage = 0;
            el.oppsValueUnderCommitStageSort = 0

            var oppsListForUser = [];
            if(opportunitiesObj[el.profile.emailId]){
                oppsListForUser = opportunitiesObj[el.profile.emailId]
            }

            if(oppsListForUser.length>0){
                _.each(oppsListForUser,function (op) {
                    if(new Date(op.closeDate)>= (new Date(moment(startOfWeek).startOf('isoWeek'))) && new Date(op.closeDate) <= (new Date(moment(endOfWeek).endOf('isoWeek')))){
                        if(isNumber(op.amount) && isNumber(el.oppsValueUnderCommitStageSort)){
                            el.oppsValueUnderCommitStageSort = el.oppsValueUnderCommitStageSort+op.amount
                        }
                    }
                })
            }

            el.selected = true;
            $scope.selectedUserValues(el);
            el.oppsValueUnderCommitStage = getAmountInThousands(el.oppsValueUnderCommitStageSort,2,share.primaryCurrency=="INR");
        })
    }

    share.getTeamCommitData = function (member,viewMode) {
        $scope.commitsByUsers = []
        $scope.viewMode = "week";
        loadTeamMembers(member)
    }

    $scope.showPrevCommits = function (stage) {
        $scope.commitsByUsers = [];
        showPrevCommits($scope,$rootScope,$http,share,stage)
    }

    $scope.closeTable = function () {
        closeAllOppTables($scope)
        $scope.commitsByUsers = [];
    }

    share.clearAllTeamCommits= function () {
        closeAllOppTables($scope);
        $scope.commitsByUsers = [];
    }

    function loadTeamMembers(member){

        $scope.selectedMember = member;
        if(window.localStorage['teamMembers']){
            try {
                pipelineReviewForTeam($scope,$rootScope,$http,share,member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[])
                fetchCommitForWeekForTeam($scope,$http,share,$rootScope,'week',null,null,member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[])
            } catch(err){
                setTimeOutCallback(1000,function () {
                    loadTeamMembers()
                })
            }
        } else {
            setTimeOutCallback(1000,function () {
                loadTeamMembers()
            })
        }
    }

    function pipelineReviewForTeam($scope,$rootScope,$http,share,userIds){

        $scope.pipelineReview = [];

        var url = "/review/meeting/team/pipeline"
        if(userIds){
            url = fetchUrlWithParameter(url,"hierarchylist",userIds)
        }

        $scope.chLoading = true;

        $http.get(url)
            .success(function (response) {
                var insightsUrl = "/insights/team/current/month/stats";

                if(userIds){
                    insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
                }

                $http.get(insightsUrl)
                    .success(function (insightsResponse) {
                        processCurrentCommitValues($scope,$rootScope,share,response,insightsResponse);
                    });
            });
    }

});

relatasApp.controller("team_list",function ($scope,$http,share,$rootScope) {

    $scope.seeWeeklyReview = function (member) {
        $scope.isSideBarOpen = true;

        if(member.children && member.children.teamMatesEmailId){
            share.teamList = member.children.teamMatesEmailId
        } else {
            share.teamList = []
        }

        share.getAllTasks(member);
        share.reviewUser(member);
    }

    share.closeCommitView = function () {
        $scope.isSideBarOpen = !$scope.isSideBarOpen
    }

    $scope.sortType = "commitSort"
    $scope.sortReverse = true;

    share.showUsers = function (team,liuData,allCompanyUserIds,forQuarter,filterType,filter,location,vertical,product,showAll,source) {

        _.each(team,function (el) {

            el.targetAndAchievement.won = parseFloat(el.targetAndAchievement.won.r_formatNumber(2))
            el.targetAndAchievement.wonWithCommas = getAmountInThousands(parseFloat(el.targetAndAchievement.won.r_formatNumber(2)),2,share.primaryCurrency=="INR")
        });

        $scope.teamList = team;
        $scope.totalTeamMembers = team.length;
        $scope.totalTarget = getAmountInThousands(_.sumBy(team,"targetSort"),2,share.primaryCurrency=="INR");
        $scope.totalAchievement = getAmountInThousands(_.sumBy(team,"targetAndAchievement.won"),2,share.primaryCurrency=="INR");
        $scope.totalCommit = getAmountInThousands(_.sumBy(team,"commitSort"),2,share.primaryCurrency=="INR");
        $scope.totalDiff = getAmountInThousands(_.sumBy(team,"differenceSort"),2,share.primaryCurrency=="INR");
    }
});

function fetchCommitForWeek($scope,$http,share,$rootScope,type,startDate,endDate,user){

    var url = '/review/commits/week'

    if(user){
        url = url+"?userId="+user.userId
    }

    if(startDate) {
        url = fetchUrlWithParameter(url,"startDate",moment(startDate).toISOString())
    }

    $scope.opps = []
    $scope.currentWeek = false;

    $http.get(url)
        .success(function (response) {

            var oppsToDisplay = response.opps;

            if(new Date()> new Date(response.commitWeek)){
                oppsToDisplay = response.commitsCurrentWeek.opportunities
            }

            var editAccess = $rootScope.editAccess;

            if(response.editAccess && $rootScope.editAccess) {
                editAccess = true;
            } else {
                editAccess = false;
            }

            var oppValue = getOppValInCommitStageForSelectedWeek(oppsToDisplay,response.commitWeek,share.primaryCurrency,share.currenciesObj);
            var selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount?response.commitsCurrentWeek.week.userCommitAmount:0;

            $scope.weeklyCommits = {
                oppValue:getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"),
                editAccess:editAccess,
                selfCommitValue:selfCommitValue,
                opps:oppsToDisplay,
                selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                response:response
            }
        });
}

function getOppValInCommitStageForSelectedWeek(opps,commitWeekDt,primaryCurrency,currenciesObj) {

    var oppVal = 0,
        startOfWeek = moment(commitWeekDt).startOf("isoWeek"),
        endOfWeek = moment(commitWeekDt).endOf("isoWeek");

    _.each(opps,function (op) {
        op.convertedAmt = op.amountNonNGM;
        op.convertedAmtWithNgm = op.amount

        if(op.currency && op.currency !== primaryCurrency){
            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amountNonNGM/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            } else {
                op.convertedAmtWithNgm = op.convertedAmt;
            }

            op.convertedAmt = parseFloat(op.convertedAmt)
        }
        if(new Date(op.closeDate)>=new Date(startOfWeek) && new Date(op.closeDate)<=new Date(endOfWeek)){
            oppVal = oppVal+op.convertedAmtWithNgm;
        }
    })

    return oppVal

}

function fetchCommitForWeekForTeam($scope,$http,share,$rootScope,type,startDate,endDate,users){

    var url = '/review/commits/week/team'

    if(users){
        url = fetchUrlWithParameter(url,"hierarchylist",users)
    }

    if(startDate && endDate) {
        url = fetchUrlWithParameter(url,"startDate",moment(startDate).toISOString())
        url = fetchUrlWithParameter(url,"endDate",moment(endDate).toISOString())
    }

    $scope.opps = []
    $scope.currentWeek = false;

    $http.get(url)
        .success(function (response) {

            var oppsToDisplay = response.opps;

            if(new Date()> new Date(response.commitWeek)){
                oppsToDisplay = response.commitsCurrentWeek.opportunities
            }

            var editAccess = $rootScope.editAccess;

            if(response.editAccess && $rootScope.editAccess) {
                editAccess = true;
            } else {
                editAccess = false;
            }

            var oppValue = getOppValInCommitStageForSelectedWeek(oppsToDisplay,response.commitWeek,share.primaryCurrency,share.currenciesObj);
            var selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount?response.commitsCurrentWeek.week.userCommitAmount:0;

            $scope.weeklyCommits = {
                oppValue:getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"),
                editAccess:editAccess,
                selfCommitValue:selfCommitValue,
                opps:oppsToDisplay,
                selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                response:response
            }

        });
}

function showPrevCommits($scope,$rootScope,$http,share,stage){
    var oppsFlattened = [];
    closeAllOppTables($scope);

    if(stage.opportunities && stage.opportunities.length>0){
        _.each(stage.opportunities,function (data) {

            if(data.opps){
                _.each(data.opps,function (op) {
                    oppsFlattened.push(op)
                })
            } else {
                oppsFlattened.push(data)
            }
        })
    }

    $scope.weekSelection = stage.rangeFormatted;
    $scope.currentWeek = true;

    if(share.commitStage){
        $rootScope.commitStage = share.commitStage;
    }
    $scope.stageSelection = $rootScope.commitStage

    paintOppsTable($scope,$rootScope,$http,share,oppsFlattened)

}

function showOpps($scope,$rootScope,$http,share,stage){
    closeAllOppTables($scope);

    $scope.weekSelection = stage.rangeFormatted;
    $scope.currentWeek = true;
    if(share.commitStage){
        $rootScope.commitStage = share.commitStage;
    } else if(stage.response.commitStage){
        $rootScope.commitStage = stage.response.commitStage;
    }

    $scope.stageSelection = $rootScope.commitStage
    paintOppsTable($scope,$rootScope,$http,share,stage.opps)

}

function showOppsWeekly($scope,$rootScope,$http,share,commit){
    closeAllOppTables($scope);

    $scope.currentWeek = true;
    if(share.commitStage){
        $rootScope.commitStage = share.commitStage;
    } else if(commit.response.commitStage){
        $rootScope.commitStage = commit.response.commitStage;
    }

    $scope.stageSelection = $rootScope.commitStage
    paintOppsTableWeek($scope,$rootScope,$http,share,commit.opps,commit.response.commitWeek)

}

function closeAllOppTables($scope){
    $scope.currentWeek = false;
    $scope.prevAndCurrentWeek = false;
    $scope.exceptionalAccess = false;
    $scope.weekByWeek = false;
    $scope.opps = [];
}

function pipelineReview($scope,$rootScope,$http,share,userId,startDate,endDate){

    $scope.pipelineReview = [];

    var url = "/review/meeting/pipeline"
    if(userId){
        url = url+"?userId="+userId
    }

    if(startDate && endDate) {
        url = fetchUrlWithParameter(url,"startDate",moment(startDate).toISOString())
        url = fetchUrlWithParameter(url,"endDate",moment(endDate).toISOString())
    }

    $scope.chLoading = true;
    $http.get(url)
        .success(function (response) {
            var insightsUrl = "/insights/current/month/stats";

            if(userId){
                insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userId)
            }

            $http.get(insightsUrl)
                .success(function (insightsResponse) {
                    processCurrentCommitValues($scope,$rootScope,share,response,insightsResponse);
                });
        });
}

function setRangeSelection ($scope,response,startOfWeek,endOfWeek,userStartDate,userEndDate){

     if(response && response.Data.dateMin && response.Data.dateMax){
        startOfWeek = moment(response.Data.dateMin).format('DD MMM');
        endOfWeek = moment(response.Data.dateMax).format('DD MMM')

    } else if(!startOfWeek && !endOfWeek){
         startOfWeek = moment().startOf('isoweek').format('DD MMM');
         endOfWeek = moment().endOf('isoweek').format('DD MMM')
     } else {
        startOfWeek = moment(startOfWeek).startOf('isoweek').format('DD MMM');
        endOfWeek = moment(endOfWeek).endOf('isoweek').format('DD MMM')
    }

    $scope.selectDates = false;
    $scope.rangeSelection = startOfWeek+"-"+endOfWeek;
}

function processCurrentCommitValues($scope,$rootScope,share,response,insightsResponse){

    $scope.commitHistory = [];
    $scope.chLoading = false;
    var weekStartDateTime = getCommitDayTimeEnd(share.companyDetails);
    var weekEndDateTime = moment(weekStartDateTime).add(1,'week');
    var startOfWeek = moment(weekStartDateTime).format('DD MMM')
    var endOfWeek = moment(weekEndDateTime).format('DD MMM')

    var currentMonth = moment().format('MMM')

    var startOfMonth = moment().startOf('month')
    var endOfMonth = moment().endOf('month')

    var commitsArray = [1,2,3];
    var opportunitiesClosingThisWeek = [],
        opportunitiesClosingThisMonth = [],
        opportunitiesClosingThisQtr = [];

    $rootScope.commitStage = response.commitStage;
    share.commitStage = response.commitStage;
    share.commitsPrevRaw = response.commitsPrevRaw
    share.commitsCurrentRaw = response.commitsCurrentRaw

    var thisWeekCommitVal = 0
        ,thisMonthCommitVal = 0
        ,thisQtrCommitVal = 0

    _.each(response.opps,function (op) {
        if(op.relatasStage == response.commitStage){

            if(new Date(op.closeDate)>= (new Date(response.qStart)) && new Date(op.closeDate) <= (new Date(response.qEnd))){
                thisQtrCommitVal = thisQtrCommitVal+parseFloat(op.amount);
                opportunitiesClosingThisQtr.push(op)
            }

            if(new Date(op.closeDate)>= (new Date(startOfMonth)) && new Date(op.closeDate) <= (new Date(endOfMonth))){
                thisMonthCommitVal = thisMonthCommitVal+parseFloat(op.amount);
                opportunitiesClosingThisMonth.push(op)
            }

            if(new Date(op.closeDate)>= new Date(weekStartDateTime) && new Date(op.closeDate) <= new Date(weekEndDateTime)){
                thisWeekCommitVal = thisWeekCommitVal+parseFloat(op.amount);
                opportunitiesClosingThisWeek.push(op)
            }
        }
    });

    $scope.opportunitiesClosingThisMonth = opportunitiesClosingThisMonth
    $scope.opportunitiesClosingThisQtr = opportunitiesClosingThisQtr
    $scope.opportunitiesClosingThisWeek = opportunitiesClosingThisWeek

    var rangeFormatted = startOfWeek+"-"+endOfWeek;

    var mon_wonAmount = 0,
        mon_targetAmount = 0,
        qtr_wonAmount = 0,
        qtr_targetAmount = 0

    if(insightsResponse.Data && insightsResponse.Data.month[0] && insightsResponse.Data.month[0].target) {
        mon_targetAmount = insightsResponse.Data.month[0].target
    }

    if(insightsResponse.Data && insightsResponse.Data.month[0] && insightsResponse.Data.month[0].won){
        mon_wonAmount = insightsResponse.Data.month[0].won
    }

    if(insightsResponse.Data && insightsResponse.Data.qtr[0] && insightsResponse.Data.qtr[0].target) {
        qtr_targetAmount = insightsResponse.Data.qtr[0].target
    }

    if(insightsResponse.Data && insightsResponse.Data.qtr[0] && insightsResponse.Data.qtr[0].won){
        qtr_wonAmount = insightsResponse.Data.qtr[0].won
    }

    var mon_relatasCommit = 0,
        mon_selfCommit = 0,
        qtr_relatasCommit = 0,
        qtr_selfCommit = 0;

    if(response && response.commitsCurrent && response.commitsCurrent){
        mon_relatasCommit = response.commitsCurrent.month.relatasCommitAmount;
        mon_selfCommit = response.commitsCurrent.month.userCommitAmount;

        qtr_relatasCommit = response.commitsCurrent.quarter.relatasCommitAmount;
        qtr_selfCommit = response.commitsCurrent.quarter.userCommitAmount;
    }

    var mon_allValues = [mon_wonAmount,mon_targetAmount,thisMonthCommitVal,mon_selfCommit],
        qtr_allValues = [qtr_wonAmount,qtr_targetAmount,thisQtrCommitVal,qtr_selfCommit]

    var mon_max = _.max(mon_allValues),
        qtr_max = _.max(qtr_allValues)

    var mon_targetStyle = {background:"#f4ada7",width:scaleBetween(mon_targetAmount,0,mon_max)+"%"},
        mon_achievementStyle = {background:"#8ECECB",width:scaleBetween(mon_wonAmount,0,mon_max)+"%"},
        mon_relatasCommitStyle = {background:oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true),width:scaleBetween(thisMonthCommitVal,0,mon_max)+"%"},
        mon_selfCommitStyle = {background:"#648ca6",width:scaleBetween(mon_selfCommit,0,mon_max)+"%"}

    var qtr_targetStyle = {background:"#f4ada7",width:scaleBetween(qtr_targetAmount,0,qtr_max)+"%"},
        qtr_achievementStyle = {background:"#8ECECB",width:scaleBetween(qtr_wonAmount,0,qtr_max)+"%"},
        qtr_relatasCommitStyle = {background:oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true),width:scaleBetween(thisQtrCommitVal,0,qtr_max)+"%"},
        qtr_selfCommitStyle = {background:"#648ca6",width:scaleBetween(qtr_selfCommit,0,qtr_max)+"%"}

    var backgroundColor = {background:oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)}
    var opportunities = response.commitsCurrent.opportunities;

    _.each(commitsArray,function (t,index) {
        var header = "Commit"
        var range = "( "+startOfWeek+"-"+endOfWeek+" )"
        var ccv = thisWeekCommitVal,
            scv = 0
        var noGraph = true;
        var target = {},
            commit = {},
            achievement = {},
            self = {}

        var targetVal = 0,
            achievementVal = 0,
            relatasCommitVal = 0,
            selfCommitVal = 0,
            rangeType = "weekly",
            commitFor = "week"

        if(response && response.commitsCurrent && response.commitsCurrent.week && response.commitsCurrent.week.userCommitAmount){
            scv = response.commitsCurrent.week.userCommitAmount
        } else {
            scv = 0
        }

        if(index == 0){
            opportunities = opportunitiesClosingThisWeek;
        }

        if(index == 1){

            opportunities = opportunitiesClosingThisMonth;
            header = "Commit"
            range = "("+currentMonth+")"
            ccv = thisMonthCommitVal
            rangeFormatted = currentMonth

            if(response && response.commitsCurrent && response.commitsCurrent.month && response.commitsCurrent.month.userCommitAmount){
                scv = response.commitsCurrent.month.userCommitAmount
            } else {
                scv = 0
            }

            targetVal = mon_targetAmount
            relatasCommitVal = thisMonthCommitVal
            selfCommitVal = mon_selfCommit
            achievementVal = mon_wonAmount

            target = mon_targetStyle
            commit = mon_relatasCommitStyle
            self = mon_selfCommitStyle
            achievement = mon_achievementStyle

            noGraph = false
            commitFor = "month"
            rangeType = "monthly"
        }

        if(index == 2){
            targetVal = qtr_targetAmount
            relatasCommitVal = thisQtrCommitVal
            selfCommitVal = qtr_selfCommit
            achievementVal = qtr_wonAmount
            opportunities = opportunitiesClosingThisQtr;

            var append = ""
            _.each(response.monthsInQuarters,function (month,index) {

                if(index == 0 ){
                    append = month.substr(0,1)
                } else {
                    append = append +"-"+month.substr(0,1)
                }
            })

            if(response && response.commitsCurrent && response.commitsCurrent.quarter && response.commitsCurrent.quarter.userCommitAmount){
                scv = response.commitsCurrent.quarter.userCommitAmount
            } else {
                scv = 0
            }

            rangeFormatted = append
            header = "Commit"
            range = "("+append+")"
            ccv = thisQtrCommitVal
            noGraph = false
            target = qtr_targetStyle
            commit = qtr_relatasCommitStyle
            self = qtr_selfCommitStyle
            achievement = qtr_achievementStyle
            commitFor = "quarter"
            rangeType = "qtr"
        }

        $scope.commitHistory.push({
            rangeFormatted:rangeFormatted,
            editAccess:$rootScope.editAccess,
            idForHover:"idForHover"+index,
            idForTooltip:"idForTooltip"+index,
            targetVal:getAmountInThousands(targetVal,2),
            relatasCommitVal:getAmountInThousands(relatasCommitVal,2),
            selfCommitVal:getAmountInThousands(selfCommitVal,2),
            commitFor:commitFor,
            header:header,
            range:range,
            noGraph:noGraph,
            noGraphClass:noGraph?"set-width":"",
            currentCommitValue:getAmountInThousands(ccv,2),
            selfCommitValue:scv,
            selfCommitValueFormat:getAmountInThousands(scv,2),
            borderRight:index !=2?"border-right":"",
            target:target,
            self:self,
            commit:commit,
            achievement:achievement,
            rangeType:rangeType,
            achievementVal:getAmountInThousands(achievementVal,2),
            displayToolTip:false,
            opportunities:opportunities,
            backgroundColor:backgroundColor
        })
    })
}

function processPipelineValues($scope,share,$http,userId){
    $scope.pipelineReview = [];
    $http.get("/review/pipeline/status?userId="+userId)
        .success(function (response) {

            $scope.commitsPrev = response.commitsPrev
            $scope.commitsCurrent= response.opps

            var data = getAmountByStage(response.opps)

            var prevWeekOpps = [];

            if(response && response.commitsPrev) {

                _.each(response.commitsPrev.opportunities,function (data) {
                    _.each(data.opps,function (op) {
                        prevWeekOpps.push(op)
                    })
                })

                var prevWeekData = getAmountByStage(prevWeekOpps,true)

            } else {

                prevWeekData = {
                    array:[],
                    map:{}
                }

                _.each(share.opportunityStages,function (st) {
                    prevWeekData.array.push({
                        stage:st,
                        amount:0
                    })

                    prevWeekData.map[st] = 0;
                })
            }

            var widthOfBox = {width: 100/share.opportunityStages.length+"%"};

            _.each(share.opportunityStages,function (st,index) {

                var growthStatus = ""

                if(prevWeekData.map[st.name] && data.map[st.name]){

                    if(data.map[st.name]>prevWeekData.map[st.name]){
                        growthStatus = "fa-caret-up"
                    } else if(data.map[st.name]<prevWeekData.map[st.name]){
                        growthStatus = "fa-caret-down"
                    } else if(data.map[st.name] == prevWeekData.map[st.name]){
                        growthStatus = ""
                    }
                }

                if(prevWeekData.map[st.name] && !data.map[st.name]){
                    growthStatus = "fa-caret-down"
                }

                if(!prevWeekData.map[st.name] && data.map[st.name]){
                    growthStatus = "fa-caret-up"
                }

                $scope.pipelineReview.push({
                    widthOfBox:widthOfBox,
                    growthStatus:growthStatus,
                    relatasStage:st.name,
                    prevAmount:prevWeekData.map[st.name]?getAmountInThousands(prevWeekData.map[st.name],2):0,
                    currentAmount:data.map[st.name]?getAmountInThousands(data.map[st.name],2):0,
                    // prevAmount:pa,
                    // currentAmount:ca,
                    isLast:share.opportunityStages.length == index+1?"last":"",
                    isFirst: index == 0?"first":""
                })
            })
        })
}

function getAmountByStage(opps,forCommits){

    var data = [];
    var byStage = 'relatasStage';

    if(forCommits){
        byStage = 'stageName';
    }

    if(checkRequired(opps)){

       data =  _
            .chain(opps)
            .groupBy(byStage)
            .map(function(values,key) {
                return {
                    stage:key,
                    amount:_.sumBy(values,function (va) {
                        return parseFloat(va.amount)
                    })
                }
            })
            .value();
    }

    var dataMap = {}

    _.each(data,function (el) {
        dataMap[el.stage] = el.amount
    })

    return {
        array:data,
        map:dataMap
    }

}

function userReportsInit($scope,$http,userId){
    
    var url = "/review/all/opps"
    
    if(userId){
        url = url+"?userId="+userId
    }

    $scope.companyOpps = []
    $scope.regionsOpps = [];
    $scope.regionsOpps = [];
    $scope.companyAmount = 0;
    $scope.topCompanies = [];

    var regions = [],
        products = [],
        companies = [],
        verticals = []
    
    $http.get(url)
        .success(function (response) {

            if(response.SuccessCode && response.Data && response.Data.length>0){
                
                _.each(response.Data,function (op) {

                    var companyName = fetchCompanyFromEmail(op.contactEmailId)
                    if(companyName){
                        op.company = companyName;
                    } else {
                        op.company = "Others";
                    }

                    op.amount = parseFloat(op.amount)

                    if(op.company){
                        companies.push({
                            name:op.company,
                            nameTruncate:op.company,
                            amount:op.amount
                        });
                    }

                    products.push({
                        name:op.productType,
                        nameTruncate:op.productType,
                        amount:op.amount
                    });

                    verticals.push({
                        name:op.vertical,
                        nameTruncate:op.vertical,
                        amount:op.amount
                    });

                    if(op.geoLocation){
                        regions.push({
                            name:op.geoLocation.town,
                            nameTruncate:op.geoLocation.town,
                            amount:op.amount
                        });
                    }

                    op.name = op.company;

                    op.nameTruncated = getTextLength(op.opportunityName,20);
                    op.amountWithCommas = getAmountInThousands(op.amount,2);
                    op.closeDateSort = moment(op.closeDate).unix()
                    op.closeDate = moment(op.closeDate).format(standardDateFormat());
                    $scope.companyOpps.push(op)

                });

                var companyData = groupAndChainForTeamSummary(companies)
                var verticalData = groupAndChainForTeamSummary(verticals)
                var regionData = groupAndChainForTeamSummary(regions)
                var productData = groupAndChainForTeamSummary(products)

                $scope.topCompanies = companyData.length>3?companyData.slice(0,3):companyData;
                $scope.topRegions = regionData.length>3?regionData.slice(0,3):regionData;
                $scope.topProducts = productData.length>3?productData.slice(0,3):productData;
                $scope.topVertical = verticalData.length>3?verticalData.slice(0,3):verticalData;
                
                $scope.companyAmount = getAmountInThousands(_.sumBy(companyData,"amount"),2)
                $scope.verticalAmount = getAmountInThousands(_.sumBy(verticalData,"amount"),2)
                $scope.regionAmount = getAmountInThousands(_.sumBy(regionData,"amount"),2)
                $scope.productAmount = getAmountInThousands(_.sumBy(productData,"amount"),2)

                setTimeOutCallback(2000,function () {
                    summaryChart(companyData,".user-company-chart",shadeGenerator(48,101,154,companyData.length,40),100,100);
                    summaryChart(regionData,".user-region-chart",shadeGenerator(71,89,129, regionData.length,20),100,100);
                    summaryChart(productData,".user-product-chart",shadeGenerator(0,115,76,productData.length,15),100,100);
                    summaryChart(verticalData,".user-vertical-chart",shadeGenerator(250,158,70, verticalData.length,20),100,100);
                })

            }

        })
}

function selectItem($scope,$http,share,$rootScope,name){
    $scope.templateHeader = name;
    menuToggleSelection(name,$scope.subMenu)
}

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function subMenu($scope){

    $scope.subMenu = [{
        name:"Commits",
        shortHand:"pr",
        selected:"selected"
    },
    //     {
    //     name:"Pipeline review",
    //     shortHand:"pc",
    //     selected:""
    // },
    //     {
    //     name:"Funnel movement",
    //     shortHand:"fm",
    //     selected:""
    // }
    //     ,
    {
        name:"Discussion points",
        shortHand:"dp",
        selected:""
    }
    //     ,{
    //     name:"Reports",
    //     shortHand:"reports",
    //     selected:""
    // }
    ]
}

function loadTemplate(selection,$scope,allTemplates) {

    if(selection.shortHand != "pr"){
        $(".commit-switch").hide()
    } else {
        $(".commit-switch").show()
    }

    _.each(allTemplates,function (tp) {
        if(selection.shortHand == tp){
            $scope[tp] = true;
        } else {
            $scope[tp] = false;
        }
    })
}

function pipelineComparison($scope,$rootScope,$http,share,response){

    var commitColName = $rootScope.commitStage?$rootScope.commitStage:response.Data.commitStage;
    commitColName = commitColName+" stage ($)"
    var relatasCommitCol = [commitColName],
        selfCommitCol = ['Self commit ($)'],
        oppCreatedCol = ['Opp created (count)'],
        oppWonAmountCol = ['Opp won ($)'],
        oppWonCol = ['Opp won (count)']

    setTimeOutCallback(100,function () {
        $scope.graphLoading = false;
    })

    formatCommitData(relatasCommitCol,selfCommitCol,response,share)
    formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response)

    var relatasCommitColLength = relatasCommitCol.length,
        prefilledDataRelatasCommit = []

    _.each(relatasCommitCol,function (re,index) {
        if(index == relatasCommitColLength-1 && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
            prefilledDataRelatasCommit.push(_.sumBy(response.Data.oppsInStageClosingThisWeek,"amount"))
        } else {
            prefilledDataRelatasCommit.push(re)
        }
    })

    var colors = {
        "Self commit ($)": '#638ca6',
        "Opp created (count)": '#3498db',
        'Opp won (count)': '#8ECECB',
        'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
    }

    colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

    var axes = {
    }

    axes[commitColName] = 'y2'
    axes["Opp won ($)"] = 'y2'
    axes["Self commit ($)"] = 'y2'

    var chart = c3.generate({
        bindto: ".pipeline-comparison",
        data: {
            // columns: [oppWonCol,oppCreatedCol,prefilledDataRelatasCommit,selfCommitCol,oppWonAmountCol],
            columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol],
            axes: axes,
            type: 'bar',
            types: {
                'Opp created (count)': 'spline',
                "Opp won (count)": 'spline'
            },
            colors:colors,
            bar: {
                width: {
                    ratio: 0.5
                }
            },
            onclick: function(e) {
                $scope.$apply(function () {
                    // closeAllOppTables($scope);
                    // $rootScope.commitStage = share.commitStage;
                    // displayOppsWeeklyHistory(e,$scope,$rootScope,$http,share,response)
                });
            }
        },
        axis: {
            y2: {
                label: {
                    text:'Opp amount',
                    position: 'outer-center'
                },
                show: true
            },
            y: {
                label: {
                    text:'No. of Opps',
                    position: 'outer-center'
                },
                show: true
            },
            x : {
                tick: {
                    fit: true
                }
            }
        },
        tooltip: {
            format: {
                title: function (d) {

                    if(share.weekByIndexNumberForCommits[d]){

                        var date = moment(moment(buildDateObj(share.weekByIndexNumberForCommits[d].weekYear))).add(1,"d");
                        // var date = moment(share.weekByIndexNumberForCommits[d].date).add(1,"week");
                        // date = moment(moment(date).day(share.companyDetails.commitDay)).startOf("isoWeek");
                        date = moment(date).startOf("isoWeek");
                        var endDate = moment(date).endOf("isoWeek")
                        return moment(date).format('DD MMM')+"-"+moment(endDate).format('DD MMM');
                    }
                }
            }
        }
    });
}

function displayOppsWeeklyHistory(event,$scope,$rootScope,$http,share,response){
    $scope.loadOppsForWeek = true;
    $scope.weekByWeek = true;
    $scope.loadingOpps = true;

    var nextWeekMoment = getCommitDayTimeEnd(share.companyDetails);
    var currentWeekYear = moment(nextWeekMoment).week()+""+moment(nextWeekMoment).year()

    if(share.weekByIndexNumberForCommits[event.index]){
        var weekYear = share.weekByIndexNumberForCommits[event.index].weekYear
        var date = share.weekByIndexNumberForCommits[event.index].date
        date = moment(date).add(1,"day") //Compensating timezone issues.
        // $scope.weekSelection = moment(date).startOf('isoweek').format('DD MMM')+"-"+moment(date).endOf('isoweek').format('DD MMM');
        var oppsFlatten = share.oppsCommitedByWeek[weekYear];

        if(oppsFlatten && oppsFlatten.length>0) {
            paintOppsTableWeekHistory($scope,$rootScope,$http,share,_.uniqBy(oppsFlatten,"opportunityId"),date)
        } else {

            if(parseFloat(currentWeekYear) == parseFloat(weekYear)){
                if(response.Data && response.Data.oppsInStageClosingThisWeek && response.Data.oppsInStageClosingThisWeek.length>0){
                    oppsFlatten = response.Data.oppsInStageClosingThisWeek
                }
            }

            paintOppsTableWeekHistory($scope,$rootScope,$http,share,_.uniqBy(oppsFlatten,"opportunityId"),date)
        }
    }
}

function paintOppsTable($scope,$rootScope,$http,share,opps,forPrevCurrent,forEa) {

    if(share.commitStage){
        $rootScope.commitStage = share.commitStage;
    }

    $scope.opps = [];
    if(!forEa){
        $scope.currentWeek = true;
    }

    $scope.commitsByUsers = [];
    var ngmReq = share.companyDetails && share.companyDetails.netGrossMargin;

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            if(forEa){
                op.amountNonNGM = op.amount
            }

            op.topLine = getAmountInThousands(op.amountNonNGM,2,share.primaryCurrency=="INR")
            op.ngmReq = ngmReq;
            if((op.netGrossMargin || op.netGrossMargin == 0) && op.amountNonNGM){
                op.amountWithNgm = (op.amountNonNGM*op.netGrossMargin)/100
            } else {
                op.amountWithNgm = op.amountNonNGM?op.amountNonNGM:op.amount;
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment().startOf('isoWeek'))) && new Date(op.closeDate) <= (new Date(moment().endOf('isoWeek')))){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            if($rootScope.commitStage == op.stageName || forPrevCurrent){

                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2,share.primaryCurrency=="INR");

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

function paintOppsTableWeek($scope,$rootScope,$http,share,opps,commitWeekDt) {

    $scope.opps = [];
    $scope.currentWeek = true;
    $scope.commitsByUsers = [];

    var startOfWeek = moment(commitWeekDt).startOf("isoWeek"),
        endOfWeek = moment(commitWeekDt).endOf("isoWeek");

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.topLine = getAmountInThousands(op.amountNonNGM,2,share.primaryCurrency=="INR")
            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment(startOfWeek).startOf('isoWeek'))) && new Date(op.closeDate) <= (new Date(moment(endOfWeek).endOf('isoWeek')))){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            op.amountWithCommas = getAmountInThousands(op.amount,2,share.primaryCurrency=="INR");
            op.age = moment().diff(moment(op.createdDate), 'day');
            op.closeDateSort = moment(op.closeDate).unix()
            op.closeDate = moment(op.closeDate).format(standardDateFormat());
            op.account = fetchCompanyFromEmail(op.contactEmailId)
            op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

            $scope.opps.push(op)
        })
    }
}

function paintOppsTableWeekHistory($scope,$rootScope,$http,share,opps,commitWeekDt) {

    $scope.opps = [];
    $scope.currentWeek = false;
    $scope.weekByWeek = true;
    $scope.commitsByUsers = [];

    var startOfWeek = moment(commitWeekDt).startOf("isoWeek"),
        endOfWeek = moment(commitWeekDt).endOf("isoWeek");

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment(startOfWeek).startOf('isoWeek'))) && new Date(op.closeDate) <= (new Date(moment(endOfWeek).endOf('isoWeek')))){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            op.amountWithCommas = getAmountInThousands(op.amount,2);
            op.age = moment().diff(moment(op.createdDate), 'day');
            op.closeDateSort = moment(op.closeDate).unix()
            op.closeDate = moment(op.closeDate).format(standardDateFormat());
            op.account = fetchCompanyFromEmail(op.contactEmailId)
            op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

            $scope.opps.push(op)
        })
    }
}

function formatCommitData(relatasCommitCol,selfCommitCol,response,share){

    var rangeType = 'week';
    share.oppsCommitedByWeek = {}
    share.weekByIndexNumberForCommits = {}

    if(response.Data && response.Data.commits){
        response.Data.commits.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.commits,function (el,index) {

            share.weekByIndexNumberForCommits[index] = {
                weekYear:el.commitWeekYear,
                noCommits:el.noCommits,
                date:el.date
            };
            share.oppsCommitedByWeek[el.commitWeekYear] = el.opportunities;

            if(el[rangeType] && el[rangeType].relatasCommitAmount){
                relatasCommitCol.push(el[rangeType].relatasCommitAmount)
            } else {
                relatasCommitCol.push(0)
            }

            if(el[rangeType] && el[rangeType].userCommitAmount){
                selfCommitCol.push(el[rangeType].userCommitAmount)
            } else {
                selfCommitCol.push(0)
            }

        })
    }
}

function formatConversionData(oppCreatedCol,oppWonCol,oppWonAmountCol,response){
    if(response.Data && response.Data.oppsConversion){

        response.Data.oppsConversion.created.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        response.Data.oppsConversion.oppsWon.sort(function (o1, o2) {
            return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
        });

        _.each(response.Data.oppsConversion.oppsWon,function (el) {
            oppWonCol.push(el.count)
            oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
        })

        _.each(response.Data.oppsConversion.created,function (el) {
            oppCreatedCol.push(el.count)
        })
    }
}

function buildTeamProfiles(data,listOfMembers) {

    var childrenForTeamMember = {}

    if(listOfMembers && listOfMembers.length>0){
        _.each(listOfMembers,function (el) {
            childrenForTeamMember[el.userEmailId] = el;
        })
    }

    var team = [];
    var liu = {}
    _.each(data,function (el) {
        var children = {};

        if(childrenForTeamMember[el.emailId]){
            children = childrenForTeamMember[el.emailId];
            children.teamMatesEmailId.push(el.emailId)
            children.teamMatesUserId.push(el._id)
        } else {
            children = {
                userEmailId:el.emailId,
                teamMatesEmailId:[el.emailId],
                teamMatesUserId:[el._id]
            }
        }

        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                children:children
            };
        } else {
            team.push({
                fullName:el.firstName+' '+el.lastName,
                image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                children:children
            })
        }

    });

    team.unshift(liu);
    return team;
}

function buildTeamProfilesWithCommits(data,listOfMembers,share) {

    var childrenForTeamMember = {}

    if(listOfMembers && listOfMembers.length>0){
        _.each(listOfMembers,function (el) {
            childrenForTeamMember[el.userEmailId] = el;
        })
    }

    var team = [];
    var liu = {}
    _.each(data,function (el) {
        var children = {};

        if(childrenForTeamMember[el.emailId]){
            children = childrenForTeamMember[el.emailId];
            children.teamMatesEmailId.push(el.emailId)
            children.teamMatesUserId.push(el._id)
        } else {
            children = {
                userEmailId:el.emailId,
                teamMatesEmailId:[el.emailId],
                teamMatesUserId:[el._id]
            }
        }

        var commit = 0,
            difference = 0,
            target = 0;

        if(el.commit && el.commit.month && el.commit.month.userCommitAmount){
            commit = el.commit.month.userCommitAmount
        }

        if(el.targetAndAchievement && el.targetAndAchievement.target){
            target = el.targetAndAchievement.target
            el.targetAndAchievement.target= getAmountInThousands(el.targetAndAchievement.target,2,share.primaryCurrency=="INR");
        }

        difference = target - commit

        if(!el.hierarchyParent){
            liu = {
                fullName:el.firstName+' '+el.lastName,
                // image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                children:children,
                targetAndAchievement:el.targetAndAchievement,
                commit:getAmountInThousands(commit,2,share.primaryCurrency=="INR"),
                commitSort:commit,
                difference:getAmountInThousands(difference,2,share.primaryCurrency=="INR"),
                differenceSort:difference,
                targetSort:target,
                noPicFlag:true,
                nameNoImg:el.firstName?el.firstName.substr(0,2).toUpperCase():el.emailId.substr(0,2).toUpperCase()
            };
        } else {

            team.push({
                fullName:el.firstName+' '+el.lastName,
                // image:'/getImage/'+el._id,
                emailId:el.emailId,
                designation:el.designation,
                userId:el._id,
                children:children,
                targetAndAchievement:el.targetAndAchievement,
                commit:getAmountInThousands(commit,2,share.primaryCurrency=="INR"),
                commitSort:commit,
                difference:getAmountInThousands(difference,2,share.primaryCurrency=="INR"),
                differenceSort:difference,
                targetSort:target,
                noPicFlag:true,
                nameNoImg:el.firstName?el.firstName.substr(0,2).toUpperCase():el.ownerEmailId.substr(0,2).toUpperCase()
            })
        }

    });

    team.unshift(liu);
    return team;
}

function buildTeamProfilesWithLiu_team(data) {
    var team = [];
    var allCompanyUserIds = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            regionOwner:el.regionOwner,
            productTypeOwner:el.productTypeOwner,
            verticalOwner:el.verticalOwner,
            orgHead:el.orgHead,
            hierarchyParent:el.hierarchyParent
        });

        allCompanyUserIds.push(el._id)
    });

    return {team:team,allCompanyUserIds:allCompanyUserIds};
}

function closeAllDropDownsAndModals($scope,id,isModal) {

    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                $scope.showVerticalList = false;
                $scope.showProductList = false;
                $scope.selectFromList = false;
                $scope.showLocationList = false;
                $scope.showDateRangePicker = false;
                $scope.showCompanyList = false;
                $scope.showContactList = false;
                $scope.showQuarterList = false;
                $scope.showAllList = false;
                $scope.showSourceList = false;
                $scope.selectFromList = false;
                $scope.isDisplayFilter = false;
                $scope.selectDates = false;
                $scope.openPriorityList = false;
                $scope.openNewStatusList = false;
                $scope.showHierarchyList = false;
                if(isModal){
                    $scope.closeModal()
                }
            })
        }
    });
}

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else{
                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function getCurrentQuarter(data) {

    var quarters = data.array;
    var now = new Date();
    var currentMonth = now.getMonth();
    var currentQuarter = null;

    _.each(quarters,function (q) {

        var dateStart = new Date(q.start)
        var dateEnd = new Date(q.end)
        var qStartMonth = dateStart.getMonth();
        var qEndMonth = dateEnd.getMonth();
        if(qStartMonth <= currentMonth && qEndMonth >= currentMonth){
            currentQuarter = q
        }
    });

    var quarter = null;
    for (var property in data.obj) {
        if (data.obj.hasOwnProperty(property)) {
            var qtr = data.obj[property];

            if(currentQuarter && qtr && currentQuarter.start == qtr.start){
                quarter = property;
            }
        }
    }

    return quarter;
}

function groupAndChainForTeamSummary(data) {

    var totalAmount = 0;
    var group = _
        .chain(_.flatten(data))
        .groupBy(function (el) {
            if(el && el.name){
                return el.name;
            } else if((el && el.name == null) || (el && !el.name && el.amount)) {
                return "Others"
            }
        })
        .map(function(values, key) {

            if(checkRequired(key)){
                var amount = _.sumBy(values, 'amount');
                totalAmount = amount;

                return {
                    nameTruncated:getTextLength(key,10),
                    name:key,
                    amount:amount,
                    amountWithCommas:getAmountInThousands(amount,3)
                }
            }
        })
        .value();

    var sortProperty = "amount";

    group.sort(function (o1, o2) {
        return o2[sortProperty] > o1[sortProperty] ? 1 : o2[sortProperty] < o1[sortProperty] ? -1 : 0;
    });

    return group;

}

function summaryChart(data,className,pattern,height,width){

    var columns = _.map(data,function (el) {
        if(el){
            return [el.name,el.amount]
        }
    });

    if(!height && !width){
        height = 125;
        width = 125;
    }

    var thickness = 0.12*height;

    var chart = c3.generate({
        bindto: className,
        data: {
            columns: columns,
            type : 'donut'
        },
        // pie: {
        donut: {
            label: {
                format: function (value, ratio, id) {
                    return '';
                }
            },
            width:thickness
        },
        legend: {
            show: false
        },
        color: {
            pattern: pattern

        },
        size: {
            height: height,
            width:width
        },
        tooltip: {
            position: function(data, width, height, thisElement) {
                var top = d3.mouse(thisElement)[1];
                return {top: top, left: parseInt(thisElement.getAttribute('x')) + parseInt(thisElement.getAttribute('width'))}
            }
        }
    });

}

relatasApp.directive('searchedContacts', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="contactList.length>0">' +
        '<div ng-repeat="item in contactList track by $index"> ' +
        '<div class="clearfix cursor" ng-click="addRecipient(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('searchedContacts1', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="assigneeBucket.length>0">' +
        '<div ng-repeat="item in assigneeBucket track by $index"> ' +
        '<div class="clearfix cursor" ng-click="selectAssignee(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag" title="[[item.fullName]], [[item.emailId]]">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

function pickDateTime($scope,minDate,maxDate,id,timezone,scopeSelector,timeRequired,callback){

    var settings = {
        value:"",
        timepicker:timeRequired?true:false,
        validateOnBlur:false,
        onSelectDate: function (dp, $input){
            $(".xdsoft_datetimepicker").hide();

            $scope.$apply(function () {
                var timezone = timezone?timezone:"UTC";
                if(scopeSelector){
                    $scope[scopeSelector] = moment(dp).tz(timezone).format();
                    $scope[scopeSelector+'Formatted'] = moment(dp).tz(timezone).format("DD MMM YYYY");
                }

                if(callback){
                    callback(dp)
                }
            });
        }
    }

    if(minDate){
        settings.minDate = minDate;
    }

    $(id).datetimepicker(settings);
}

relatasApp.service('searchContacts', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);

var tooltip = Chartist.plugins.tooltip();