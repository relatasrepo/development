function fetchCommitForMonth($scope,$http,share,$rootScope,type,currentMonthSelection,user,navigating){
    var startDate = null;

    if(share && share.userId){
        $rootScope.editAccess = share.userId == user.userId
    }

    if(currentMonthSelection){
        startDate = currentMonthSelection.start
    } else {
        setMonthDateRange($scope,share);
    }

    if(navigating && startDate){
        $scope.nextButtonDisabled = new Date()<= new Date(startDate);
    }

    if(!navigating){
        $scope.graphLoading = true
        setTimeOutCallback(2000,function () {
            monthlyCommitHistory($scope,$rootScope,$http,share,user.userId);
        })
    }

    checkCompanySettingsLoaded()

    function checkCompanySettingsLoaded(){

    }

    fetchCurrentMonthCommits($scope,$http,share,$rootScope,user.userId,startDate)
}

function setMonthDateRange($scope,share){

    if(share.companyDetails){
        var start = getMonthCommitCutOff(share.companyDetails),
            end = moment(start).add(1,'month')

        $scope.currentMonthSelection = {
            start:new Date(start),
            end:new Date(end)
        }

        $scope.nextButtonDisabled = new Date()<= new Date(getMonthCommitCutOff(share.companyDetails));

        $scope.dataForRange = moment(start).format('MMM');

    } else {
        setTimeOutCallback(1000,function () {
            setMonthDateRange($scope,share)
        })
    }
}

function fetchCurrentMonthCommits($scope,$http,share,$rootScope,userId,startDate){
    var url = '/review/commits/month';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    if(startDate){
        url = fetchUrlWithParameter(url,"startDate",startDate)
    }

    $http.get(url)
        .success(function (response) {

            $scope.monthlyCommits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){
                $rootScope.commitStage = response.commitStage;

                var editAccess = $rootScope.editAccess;

                if(response.editAccess && $rootScope.editAccess) {
                    editAccess = true;
                } else {
                    editAccess = false;
                }

                var oppsToDisplay = response.opps;

                if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                    oppsToDisplay = response.commitCurrentMonth.opportunities
                }

                var oppValue = getOppValInCommitStageForSelectedMonth(oppsToDisplay,response.commitMonth,share.primaryCurrency,share.currenciesObj)
                var selfCommitValue = response.commitCurrentMonth.month.userCommitAmount?response.commitCurrentMonth.month.userCommitAmount:0;

                $scope.monthlyCommits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                getTargetsAndAchievements($http,userId,startDate,null,function (insights) {
                  if(insights){
                      $scope.monthlyCommits.graph = setGraphValues(insights,response,share,"month");
                  }
                });
            }

        })
}

function getOppValInCommitStageForSelectedMonth(opps,commitDt,primaryCurrency,currenciesObj) {

    var oppVal = 0,
        startDate = moment(commitDt).startOf("month"),
        end = moment(commitDt).endOf("month");

    _.each(opps,function (op) {

        op.convertedAmt = op.amountNonNGM;
        op.convertedAmtWithNgm = op.amount

        if(op.currency && op.currency !== primaryCurrency){
            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                op.convertedAmt = op.amountNonNGM/currenciesObj[op.currency].xr
            }

            if(op.netGrossMargin || op.netGrossMargin == 0){
                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
            } else {
                op.convertedAmtWithNgm = op.convertedAmt;
            }

            op.convertedAmt = parseFloat(op.convertedAmt)
        }

        if(new Date(op.closeDate)>=new Date(startDate) && new Date(op.closeDate)<=new Date(end)){
            oppVal = oppVal+op.convertedAmtWithNgm;
        }
    })

    return oppVal

}

function setGraphValues(insightsData,commitData,share,mode){

    var stages = [];
    var obj = {};

    obj.idForTooltip = 0
    obj.idForHover = 0

    obj.targetVal = 0
    obj.achievementVal = 0
    obj.selfCommitVal = 0
    obj.relatasCommitVal = 0


    function getOppStages(){
        if(share.opportunityStages){
            stages = _.map(share.opportunityStages,"name")
        } else {
            setTimeOutCallback(1000,function () {
                getOppStages()
            })
        }
    }

    getOppStages()

    if(mode == "month"){
        if(insightsData && insightsData.Data && insightsData.Data.month){
            _.each(insightsData.Data.month,function (el) {
                if(el.target){
                    obj.targetVal = obj.targetVal+el.target
                }
                if(el.won){
                    obj.achievementVal = obj.achievementVal+el.won
                }
            })
        }

        if(commitData){
            if(commitData.commitCurrentMonth.month.userCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentMonth.month.userCommitAmount;
            }

            if(commitData.commitCurrentMonth.month.relatasCommitAmount){
                obj.relatasCommitVal = commitData.commitCurrentMonth.month.relatasCommitAmount;
            }
        }
    } else {

        if(insightsData && insightsData.Data && insightsData.Data.qtr){
            _.each(insightsData.Data.qtr,function (el) {
                if(el.target){
                    obj.targetVal = obj.targetVal+el.target
                }

                obj.achievementVal = obj.achievementVal+el.won
            })
        }

        if(commitData){
            if(commitData.commitCurrentQuarter.quarter.userCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentQuarter.quarter.userCommitAmount;
            }

            if(commitData.commitCurrentQuarter.quarter.relatasCommitAmount){
                obj.selfCommitVal = commitData.commitCurrentQuarter.quarter.relatasCommitAmount;
            }
        }
    }

    var allValues = [obj.targetVal,obj.achievementVal,obj.selfCommitVal,obj.relatasCommitVal]
    var max = _.max(allValues)

    var mon_targetStyle = {background:"#f4ada7",width:scaleBetween(obj.targetVal,0,max)+"%"},
        mon_achievementStyle = {background:"#8ECECB",width:scaleBetween(obj.achievementVal,0,max)+"%"},
        mon_relatasCommitStyle = {background:oppStageStyle(commitData.commitStage,stages.indexOf(commitData.commitStage),false,false,true),width:scaleBetween(obj.selfCommitVal,0,max)+"%"},
        mon_selfCommitStyle = {background:"#648ca6",width:scaleBetween(obj.selfCommitVal,0,max)+"%"}

    obj.target = mon_targetStyle
    obj.achievement = mon_achievementStyle
    obj.selfCommit = mon_relatasCommitStyle
    obj.relatasCommit = mon_selfCommitStyle
    obj.displayToolTip = false;

    obj.targetVal = getAmountInThousands(obj.targetVal,2,share.primaryCurrency=="INR")
    obj.achievementVal = getAmountInThousands(obj.achievementVal,2,share.primaryCurrency=="INR")

    return obj;
}

function getTargetsAndAchievements($http,userIds,startDate,viewMode,callback){

    var insightsUrl = "/insights/current/month/stats";

    if(userIds){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"userIds",userIds)
    }

    if(startDate){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"startDate",startDate)
    }

    if(viewMode){
        insightsUrl = fetchUrlWithParameter(insightsUrl,"viewMode",viewMode)
    }

    $http.get(insightsUrl)
        .success(function (insightsResponse) {
            callback(insightsResponse)
        });
}

function navigateMonthlyCommits($scope,$http,share,$rootScope,currentMonthSelection,type,member,viewingTeamData){

    var start = getMonthCommitCutOff(share.companyDetails),
        end = moment(start).add(1,'month')

    if(type == 'prev' && currentMonthSelection){
        start = moment(currentMonthSelection.start).subtract(1,"month")
        end = moment(currentMonthSelection.end).subtract(1,"month")
    }

    if(type == 'next' && currentMonthSelection){
        start = moment(currentMonthSelection.start).add(1,"month")
        end = moment(currentMonthSelection.end).add(1,"month")
    }

    if(new Date(start) >= new Date(getMonthCommitCutOff(share.companyDetails))){
        $scope.nextButtonDisabled = true;
    } else {
        $scope.nextButtonDisabled = false;
    }

    $scope.dataForRange = moment(start).format('MMM');

    $scope.currentMonthSelection = {
        start:new Date(start),
        end:new Date(end)
    }

    if(viewingTeamData){
        share.fetchTeamCommitForMonth(member.children && member.children.teamMatesUserId?member.children.teamMatesUserId:[],new Date(start))
    } else {
        fetchCommitForMonth($scope,$http,share,$rootScope,type,$scope.currentMonthSelection,member,"navigating")
    }
}

function initializeMonthlyCommitScopesAndMethods($scope,$rootScope,$http,share){

    $scope.saveMonthlyCommits = function (commits,userId) {

        if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
            $http.post("/review/meeting/update/commit/value/monthly",{commitValue:commits.selfCommitValue})
                .success(function (response) {
                    if(response.SuccessCode){
                        toastr.success("Commits successfully updated")
                        monthlyCommitHistory($scope,$rootScope,$http,share,userId)
                    } else {
                        toastr.error("Commits not updated. Please try again later")
                    }
                });
        } else {
            toastr.error("Please enter only numbers for opportunity amount")
        }
    }

    $scope.updateMonthlyStoreValue = function (value) {
        value.selfCommitValue = parseFloat(value.selfCommitValueFormat)
    }

    $scope.showOppsMonthly = function (stage) {

        closeAllOppTables($scope);

        $scope.weekSelection = stage.rangeFormatted;
        $scope.currentWeek = true;
        if(share.commitStage){
            $rootScope.commitStage = share.commitStage;
        } else if(stage.response.commitStage){
            $rootScope.commitStage = stage.response.commitStage;
        }

        $scope.stageSelection = $rootScope.commitStage
        paintOppsTableMonth($scope,$rootScope,$http,share,stage.opps,stage.response.commitMonth)
    }

}

function toggleOppsClosingInTimeFrame($scope,opps,viewMode) {
    if(viewMode == "month"){
        if(opps.length == $scope.monthlyCommits.opps.length){
            $scope.opps = getOppsClosingInTf(opps)
        } else {
            $scope.opps = $scope.monthlyCommits.opps;
        }
    }

    if(viewMode == "qtr"){
        if(opps.length == $scope.quarterlyCommits.opps.length){
            $scope.opps = getOppsClosingInTf(opps)
        } else {
            $scope.opps = $scope.quarterlyCommits.opps;
        }
    }

    if(viewMode == "week"){
        if(opps.length == $scope.weeklyCommits.opps.length){
            $scope.opps = getOppsClosingInTf(opps)
        } else {
            $scope.opps = $scope.weeklyCommits.opps;
        }
    }
}

function getOppsClosingInTf(opps) {
    var oppsClosingInTf = [];

    _.each(opps,function (op) {
        if(op.closingThisWeek && op.closingThisWeek.background){
            oppsClosingInTf.push(op)
        }
    });

    return oppsClosingInTf;
}

function initializeMonthlyCommitScopesAndMethodsForTeam($scope,$http,share,$rootScope) {
    share.fetchTeamCommitForMonth = function (users,startDate) {
        $scope.viewMode = "month"
        fetchTeamCommitForMonth($scope,$http,share,$rootScope,users,startDate)
    }

    $scope.showOppsMonth = function (stage) {
        closeAllOppTables($scope);

        $scope.weekSelection = stage.rangeFormatted;
        $scope.currentWeek = true;
        $rootScope.commitStage = share.commitStage;
        $scope.stageSelection = $rootScope.commitStage

        paintOppsTableMonth($scope,$rootScope,$http,share,stage.opps,stage.response.commitMonth)
    }

    $scope.closeOppForCommit = function () {
        $scope.showOppForCommits = false;
    }

    $scope.showOpportunities = function (forUser,index) {
        $scope.showOppForCommits = true;

        var startDate = moment(forUser.commitForDate).startOf("month"),
            end = moment(forUser.commitForDate).endOf("month");

        $scope.selectedUser = forUser;

        $scope.opportunitiesForCommit = [];
        if(share.opportunitiesObj[forUser.emailId]){
            $scope.opportunitiesForCommit = _.filter(share.opportunitiesObj[forUser.emailId],function (op) {
                if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                    op.topLine = parseFloat(op.convertedAmt.toFixed(2));
                    op.bottomLine = parseFloat(op.convertedAmtWithNgm.toFixed(2));
                    op.closeDateFormatted = moment(op.closeDate).format(standardDateFormat());
                    return op;
                }
            });
        }
    }

    $scope.getTeamCommitsMonth = function (data) {

        var startDate = moment(data.response.commitMonth).startOf("month"),
            end = moment(data.response.commitMonth).endOf("month");

        $scope.selectionType = data.dateRange;
        $scope.commitsByUsers = [];
        $scope.currentWeek = false;
        var userHashMap = share.companyUsersDictionary;
        var oppList = data.opps;

        var opportunitiesObj = {};

        if(oppList && oppList.length>0){
            _.each(oppList,function (op) {
                if(opportunitiesObj[op.userEmailId]){
                    opportunitiesObj[op.userEmailId].push(op)
                } else {
                    opportunitiesObj[op.userEmailId] = [];
                    opportunitiesObj[op.userEmailId].push(op)
                }
            })
        }

        share.opportunitiesObj = opportunitiesObj;

        if(data.response.commitsCurrentRaw && data.response.commitsCurrentRaw.length>0){

            _.each(data.response.commitsCurrentRaw,function (el) {

                el.profile = userHashMap[el.userId];
                el.value = el.month.userCommitAmount
                if(isNumber(parseFloat(el.value))){
                    el.valueSort = parseFloat(el.value)
                }
                el.value = getAmountInThousands(el.value,2)
                el.emailId = el.profile.emailId;
                el.commitDateTime = moment(el.date).format(standardDateFormat());
                el.oppsValueUnderCommitStage=0;
                el.oppsValueUnderCommitStageSort=0;

                $scope.commitsByUsers.push(el)
            })
        }

        var nonExistingUsers = _.differenceBy(share.teamList,_.map($scope.commitsByUsers,"emailId"))
        if(nonExistingUsers && nonExistingUsers.length>0){
            _.each(nonExistingUsers,function (el) {
                $scope.commitsByUsers.push({
                    value:0,
                    valueSort:0,
                    profile:share.team[el],
                    commitDateTime:"-",
                    oppsValueUnderCommitStage:0,
                    oppsValueUnderCommitStageSort:0
                })
            })
        }

        $scope.commitValueSelected = 0;
        $scope.oppValueSelected = 0;

        _.each($scope.commitsByUsers,function (el) {

            el.oppsValueUnderCommitStage = 0;
            el.oppsValueUnderCommitStageSort = 0

            var oppsListForUser = [];
            if(opportunitiesObj[el.profile.emailId]){
                oppsListForUser = opportunitiesObj[el.profile.emailId]
            }

            if(oppsListForUser.length>0){
                _.each(oppsListForUser,function (op) {
                    if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                        if(op.amount && isNumber(el.oppsValueUnderCommitStageSort)){
                            el.oppsValueUnderCommitStageSort = el.oppsValueUnderCommitStageSort+op.amount
                        }
                    }
                })
            }

            el.selected = true;
            $scope.selectedUserValues(el);
            el.oppsValueUnderCommitStage = getAmountInThousands(el.oppsValueUnderCommitStageSort,2,share.primaryCurrency=="INR");
        })
    }
}

function fetchTeamCommitForMonth($scope,$http,share,$rootScope,users,startDate) {

    var url = '/review/commits/month/team';

    if(users){
        url = fetchUrlWithParameter(url,"hierarchylist",users)
    }

    if(startDate){
        url = fetchUrlWithParameter(url,"startDate",startDate)
    }

    $http.get(url)
        .success(function (response) {

            $scope.monthlyCommits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                $rootScope.commitStage = response.commitStage
                share.commitStage = response.commitStage

                var selfCommitValue = 0;

                var oppsToDisplay = response.opps;

                if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                    oppsToDisplay = response.commitCurrentMonth.opportunities
                }

                if(response.commitCurrentMonth.month.userCommitAmount){
                    selfCommitValue = response.commitCurrentMonth.month.userCommitAmount;
                }

                var oppValue = getOppValInCommitStageForSelectedMonth(oppsToDisplay,response.commitMonth,share.primaryCurrency,share.currenciesObj)

                if(startDate){
                    share.setDateRange(response.commitMonth,moment(response.commitMonth).add(1,"month"),"month")
                } else {
                    share.setDateRange(new Date(),moment().add(1,"month"),"month")
                }

                $scope.monthlyCommits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:false,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                getTargetsAndAchievements($http,users,startDate,null,function (insights) {
                    if(insights){
                        $scope.monthlyCommits.graph = setGraphValues(insights,response,share,"month");
                    }
                });
            }
        });
}

function monthlyCommitHistory($scope,$rootScope,$http,share,userId){

    var url = '/review/monthly/commits/history';

    if(userId){
        url = fetchUrlWithParameter(url,"userId",userId)
    }

    $http.get(url)
        .success(function (response) {

            var commitColName = response.commitStage;
            commitColName = commitColName+" stage ($)"
            var relatasCommitCol = [commitColName],
                selfCommitCol = ['Self commit'],
                oppCreatedCol = ['Opp created (count)'],
                oppWonAmountCol = ['Opp won ($)'],
                oppWonCol = ['Opp won (count)'],
                commitsMadeObj = {}

            response.commits.sort(function (o1, o2) {
                return new Date(o1.commitForDate) > new Date(o2.commitForDate) ? 1 : new Date(o1.commitForDate) < new Date(o2.commitForDate) ? -1 : 0;
            });

            response.oppsConversion.created.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            response.oppsConversion.oppsWon.sort(function (o1, o2) {
                return new Date(o1.forDate) > new Date(o2.forDate) ? 1 : new Date(o1.forDate) < new Date(o2.forDate) ? -1 : 0;
            });

            _.each(response.commits,function (el,index) {
                commitsMadeObj[index] = el;
                selfCommitCol.push(el.month.userCommitAmount)
                relatasCommitCol.push(el.month.relatasCommitAmount)
            });

            _.each(response.oppsConversion.created,function (el) {
                oppCreatedCol.push(el.count)
            });

            _.each(response.oppsConversion.oppsWon,function (el) {
                oppWonAmountCol.push(el.amount?parseFloat(el.amount.r_formatNumber(2)):0)
                oppWonCol.push(el.count)
            });

            setTimeOutCallback(100,function () {
                $scope.graphLoading = false;
            })

            var colors = {
                "Self commit": '#638ca6',
                "Opp created (count)": '#3498db',
                'Opp won (count)': '#8ECECB',
                'Opp won ($)':oppStageStyle("Close Won",$scope.stages.indexOf($rootScope.commitStage),false,false,true)
            }

            colors[commitColName] = oppStageStyle($rootScope.commitStage,$scope.stages.indexOf($rootScope.commitStage),false,false,true)

            var axes = {
            }

            axes[commitColName] = 'y2'
            axes["Opp won ($)"] = 'y2'
            axes["Self commit"] = 'y2'

            var chart = c3.generate({
                bindto: ".pipeline-comparison",
                data: {
                    columns: [oppWonCol,oppCreatedCol,selfCommitCol,oppWonAmountCol],
                    axes: axes,
                    type: 'bar',
                    types: {
                        'Opp created (count)': 'spline',
                        "Opp won (count)": 'spline'
                    },
                    colors:colors,
                    bar: {
                        width: {
                            ratio: 0.5
                        }
                    },
                    onclick: function(e) {
                        $scope.$apply(function () {
                            closeAllOppTables($scope);
                            $rootScope.commitStage = share.commitStage;
                            displayOppsMonthly(e,$scope,share,response,commitsMadeObj)
                        });
                    }
                },
                axis: {
                    y2: {
                        label: {
                            text:'Opp amount',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    y: {
                        label: {
                            text:'No. of Opps',
                            position: 'outer-center'
                        },
                        show: true
                    },
                    x : {
                        tick: {
                            fit: true
                        }
                    }
                },
                tooltip: {
                    format: {
                        title: function (d) {
                            if(commitsMadeObj[d]){
                                var startDate = commitsMadeObj[d].commitForDate
                                var dateRange = moment(startDate).format('MMM YY')
                                return dateRange;
                            }
                        }
                    }
                }
            });
        })
}

function displayOppsMonthly(event,$scope,share,response,commitsMadeObj){

    $scope.opps = [];
    $scope.currentWeek = false;
    $scope.weekByWeek = true;
    $scope.commitsByUsers = [];
    $scope.loadOppsForWeek = true;
    $scope.weekByWeek = true;
    $scope.loadingOpps = true;

    var opps = commitsMadeObj[event.index].opportunities

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.ngmReq = response.netGrossMarginReq;
            op.amountWithNgm = op.amountNonNGM?op.amountNonNGM:op.amount;

            if(response.netGrossMarginReq){
                op.amountWithNgm = (op.amountWithNgm*op.netGrossMargin)/100
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= (new Date(moment(commitsMadeObj[event.index].commitForDate))) && new Date(op.closeDate) <= (new Date(commitsMadeObj[event.index].commitForDate))){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            if(response.commitStage == op.stageName){
                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2);

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

function paintOppsTableMonth($scope,$rootScope,$http,share,opps,commitDt) {

    if(share.commitStage){
        $rootScope.commitStage = share.commitStage;
    }

    $scope.opps = [];
    $scope.currentWeek = true;

    $scope.commitsByUsers = [];
    var startDate = moment(commitDt).startOf("month"),
        end = moment(commitDt).endOf("month");

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            op.topLine = op.amountNonNGM?getAmountInThousands(op.amountNonNGM,2,share.primaryCurrency=="INR"):getAmountInThousands(op.amount,2,share.primaryCurrency=="INR")
            op.amountWithNgm = op.amount

            if(!op.amountNonNGM && (op.netGrossMargin || op.netGrossMargin == 0)){
                op.amountWithNgm = parseFloat(((op.amount*op.netGrossMargin)/100).toFixed(2))
            }

            op.closingThisWeek = {};

            if(new Date(op.closeDate)>= new Date(startDate) && new Date(op.closeDate) <= new Date(end)){
                op.closingThisWeek = {
                    background:"rgb(202, 227, 221)"
                }
            }

            op.currency = op.currency && op.currency.length>0?op.currency:share.primaryCurrency;

            if($rootScope.commitStage == op.stageName){

                op.amountWithCommas = getAmountInThousands(op.amountWithNgm,2,share.primaryCurrency=="INR");

                var age = moment(op.closeDate).diff(moment(op.createdDate), 'day');

                if(op.relatasStage != "Close Won" && op.relatasStage != "Close Lost"){
                    age = moment().diff(moment(op.createdDate), 'day');
                }

                op.age = age;
                op.closeDateSort = moment(op.closeDate).unix()
                op.closeDate = moment(op.closeDate).format(standardDateFormat());

                op.account = fetchCompanyFromEmail(op.contactEmailId)

                op.opportunityNameTruncated = getTextLength(op.opportunityName,20)

                $scope.opps.push(op)
            }
        })
    }
}

$("body").on('click', '.onhover', function (e) {
    var parentOffset = $(this).parent().offset();

    var relX = e.pageX - parentOffset.left;
    var relY = e.pageY - parentOffset.top;
    var table = $('.opportunities-dialog-box');

    table.css({
        top:90
    })

    table.css({
        top:relY+110
    })

});