// icon tabs
( function ($)
{
	// Extend "$.fn" object. Your code will call "$('#css-selector').myJQPlugin({opt1: 'value'})" to instantiate the plugin.
	$.fn.iconTabify = function(options)
	{
		// options can be null here
		
		// merge plugin defaults and provided options into an empty object and assign the reference to a local variable
	    var opts = $.extend( {}, $.fn.iconTabify.defaults, options );

	    // Our plugin implementation code goes here
	    
	    // returning to make it chainable
	    // do "return this.each(function() { plugin implementation code... })" if the jquery object is a collection
	    
	    var root = $(this);
	    var total = $(".icon-tab-item", this).length;
	    var $el;

	    var currentTab = null;
	    var currentTabContent = null;
	    
	    for(var i=0; i < total; i++)
	    {
	    	$el = $($(".icon-tab-item", this)[i]);

	    	$(".icon-tab-item-href", $el).click(function () {
	    		$(".icon-tab-item-href", root).removeClass("active");
	    		$(this).addClass("active");

	    		if(currentTabContent != null)
	    		{
	    			$(currentTabContent).hide();
	    		}

	    		currentTabContent = $(this).attr("data-content");

	    		$(currentTabContent).show();

	    		return false;
	    	});
	    }

	    $(".active", root).click();

	    return this.html(opts.foo);
	}

	// Plugin defaults – added as a property on our plugin function.
	$.fn.iconTabify.defaults = {

	};

}) (jQuery);