
var reletasApp = angular.module('reletasApp', ['ngRoute','datatables','ngSanitize','angular-loading-bar','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    };

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');

        var str = searchContent;

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

var timezone ;

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}

var contextEmailId = getParams(window.location.href).context;
var contextMobileNumber = getParams(window.location.href).mobileNumber;
var showContext = true;
if(!checkRequired(contextEmailId)){
    showContext = false;
}

reletasApp.service('share', function () {
    return {}
});

reletasApp.controller("logedinUser", function ($scope, $http,share,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            $(".seeMoreIcon").addClass("disabled-see-more")
            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.currentUser = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
                share.lUseEmailId = response.Data.emailId
            }
            else{

            }
        }).error(function (data) {

        })
});

reletasApp.controller("left_contacts_bar", function ($scope, $http, $rootScope, share) {

    $scope.contactsList = [];
    $scope.currentLength = 0;
    $scope.lastFetched = 0;
    $scope.filterBy = 'all';
    $scope.show_contacts_back = false;
    $scope.show_filter = false;
    $scope.usersInHierarchy = []
    var isFilter = false;

    $scope.searchContacts = function(searchContent){
        if(checkRequired(searchContent)){
            $scope.filterBy = isFilter ? $scope.filterBy : 'search';
            $scope.contactsList = [];
            $scope.currentLength = 0;
            var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)
            $scope.getContacts(url,0,25,$scope.filterBy,searchContent,null,function (account) {
                $scope.accountSelected(account)
            });
        }
    };

    $scope.refreshContactsList = function(){
        $scope.filterBy = isFilter ? $scope.filterBy : 'all';
        $scope.searchContent = "";
        $(".searchContentClass").val("");
        $scope.contactsList = [];
        $scope.currentLength = 0;
        var url = fetchUrlWithParameter("/company/middle/bar/accounts", "hierarchylist", $scope.usersInHierarchy)

        $scope.getContacts(url,0,25,$scope.filterBy,null,null,function (account) {
            $scope.accountSelected(account)
        });

    };

    $scope.getContacts = function(url,skip,limit,filterBy,searchContent,isBack,callback){
        if(url.indexOf("?") == -1)
            url = url + "?"
        else
            url = url + "&"
        if(checkRequired(searchContent)){
            url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy+'&searchContent='+searchContent
        }
        else url = url+'skip='+skip+'&limit='+limit+'&filterBy='+filterBy;

        $http.get(url)
            .success(function(response){
                $scope.contactsList = [];
                $scope.isConnectionOpen = false;
                if(response.SuccessCode){

                    $scope.grandTotal = response.Data.total;

                    if(response.Data.accounts.length > 0){

                        if(!isBack || $scope.currentLength <= 0){
                            $scope.currentLength += response.Data.returned;
                            $scope.returned = response.Data.returned
                        }

                        if($scope.currentLength > 25){
                            $scope.show_contacts_back = true;
                        }else $scope.show_contacts_back = false;

                        for(var i=0; i<response.Data.accounts.length; i++){

                            var name = response.Data.accounts[i].name;
                            var hasAccess = false;
                            var accessIcon = "fa fa-lock";
                            var ownerEmailId = response.Data.accounts[i].ownerEmailId
                            var ownerId = response.Data.accounts[i].ownerId

                            var usersWithAccess = _.map(response.Data.accounts[i].access,"emailId")

                            if(_.includes(usersWithAccess,response.liuEmailId) || $rootScope.currentUser.corporateAdmin){
                                hasAccess = true;
                                accessIcon = ""
                            }

                            var costOfSalesWithComma = 0,
                                costOfDeliveryWithComma = 0;

                            if(response.Data.accounts[i].costOfSales){
                                costOfSalesWithComma = getAmountInThousands(response.Data.accounts[i].costOfSales,2)
                            }

                            if(response.Data.accounts[i].costOfDelivery){
                                costOfDeliveryWithComma = getAmountInThousands(response.Data.accounts[i].costOfDelivery,2)
                            }

                            var obj;
                            obj = {
                                isOwner:ownerEmailId == response.liuEmailId,
                                userId:name,
                                fullName:name,
                                designation:"",
                                fullDesignation:"",
                                name:getTextLength(name,25),
                                emailId:name,
                                noPicFlag:true,
                                image:"https://logo.clearbit.com/"+ getTextLength(name,25) +".com",
                                noPPic:(name.substr(0,2)).capitalizeFirstLetter(),
                                cursor:'cursor:default',
                                url:null,
                                idName:'com_con_item_'+name,
                                hasAccess:hasAccess,
                                target:response.Data.accounts[i].target,
                                partner:response.Data.accounts[i].partner,
                                important:response.Data.accounts[i].important,
                                costOfSales:response.Data.accounts[i].costOfSales,
                                costOfDelivery:response.Data.accounts[i].costOfDelivery,
                                costOfSalesWithComma:costOfSalesWithComma,
                                costOfDeliveryWithComma:costOfDeliveryWithComma,
                                accessIcon:accessIcon,
                                usersWithAccess:usersWithAccess,
                                ownerId:ownerId,
                                ownerEmailId:ownerEmailId,
                                accountAccessRequested:response.Data.accounts[i].accountAccessRequested
                            };

                            $scope.contactsList.push(obj);
                            $http.get("/companylogos/" + getTextLength(name,25) +".com")
                        }
                    }
                    else {
                        $scope.isAllConnectionsLoaded = true;
                    }
                }
                else{
                    $scope.isAllConnectionsLoaded = true;
                }

                if(callback){
                    callback($scope.contactsList[0])
                }

            })
    };

    $scope.validNum = function(num){
        return num < 10 ? '0'+num : num;
    }

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }


    function checkLiuLoaded(){

        if($rootScope.currentUser){
            $scope.getContacts('/company/middle/bar/accounts',0,25,$scope.filterBy,null,null,function (account) {
                $scope.accountSelected(account)
            });
        } else {
            setTimeOutCallback(1000,function () {
                checkLiuLoaded();
            })
        }
    }

    checkLiuLoaded();


    $scope.loadMoreContacts = function(skip,limit,searchContent,isBack){
        if(isBack){
            $scope.currentLength = $scope.currentLength-25;
            skip = skip-25
        }
        if(skip < 25){
            skip = 0
            $scope.currentLength = 0;
        }

        $scope.getContacts('/company/middle/bar/accounts',skip,limit,$scope.filterBy,searchContent,isBack,function (account) {
            $scope.accountSelected(account)
        });

    };

    $scope.calculateFromTo = function(current){
        return current < 0 ? 0 : current
    };

    $scope.accountSelected = function(account){

        if(account){
            checkTeamLoaded();
        }

        function checkTeamLoaded(){
            if(share.teamDictionary && share.teamArray){
                var userIds = _.map(share.teamArray,"userId");
                share.getAccessControl(account,userIds)
            } else {
                setTimeOutCallback(1000,function () {
                    checkTeamLoaded()
                })
            }
        }
    };

});

reletasApp.controller("contact_details",function($scope, $http, share){
    share.getContactInfo = function(emailId,companyName,designation,name,url,userId,mobileNumber){

        var url;
        var fetchWith = emailId
        url = '/contacts/details/'+fetchWith+'/relationship?fetchProfile=yes'
        if(!emailId) {
            fetchWith = mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
            url = '/contacts/details/'+fetchWith+'/relationship?fetchProfile=yes'
        }

        if(isNumber(mobileNumber)){
            url = url+'&mobileNumber='+mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
        }

        $http.get(url)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.cUserId = null;
                    if(checkRequired(response.Data.relation)){
                        if(checkRequired(response.Data.relation.personId)){
                            $scope.p_pic = '/getImage/'+response.Data.relation.personId
                            $scope.no_pic = false;
                            $scope.cUserId = response.Data.relation.personId;
                        }
                        else if(checkRequired(response.Data.relation.personName)){
                            $scope.no_pic = true;
                            $scope.p_pic = response.Data.relation.personName.substr(0,2)
                        }
                        $scope.companyName = response.Data.relation.companyName;
                        $scope.designation = response.Data.relation.designation;
                        $scope.p_position = $scope.getPosition(response.Data.relation.companyName,response.Data.relation.designation);
                        if(!checkRequired($scope.p_position)){
                            $scope.getPosition(companyName,designation);
                        }
                        $scope.p_class_invisible = checkRequired($scope.p_position) ? '' : 'invisible'
                        if(!checkRequired($scope.p_position)){
                            $scope.p_position = 'No Designation'
                        }
                        $scope.p_name = response.Data.relation.personName || '';
                        if(!checkRequired($scope.p_name)){
                            $scope.p_name = name;
                        }
                        share.updateTaskContents($scope.p_name || name,emailId,$scope.cUserId || userId);
                        $scope.p_location = response.Data.relation.location || '';
                        $scope.p_mobileNumber = response.Data.relation.mobileNumber || '';
                        $scope.p_emailId = response.Data.relation.personEmailId || '';
                        $scope.p_favorite = response.Data.relation.favorite || false;
                        $scope.p_favoriteColor = response.Data.relation.favorite == true ? 'color: #f86b4f;' : 'color: #777777!important';
                        $scope.p_favoriteColor_a = response.Data.relation.favorite == true ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                        $scope.p_id = response.Data.relation._id;
                        $scope.tag = response.Data.relation.hashtag;
                        var existsUrl = '';
                        if(checkRequired(response.Data.profile) && checkRequired(response.Data.profile.publicProfileUrl)){
                            if(response.Data.profile.publicProfileUrl.charAt(0) != 'h'){
                                existsUrl = '/'+response.Data.profile.publicProfileUrl
                            }
                        }
                        else if(!checkRequired(url) && checkRequired(response.Data.relation.publicProfileUrl)){
                            if(response.Data.relation.publicProfileUrl.charAt(0) != 'h'){
                                existsUrl = '/'+response.Data.relation.publicProfileUrl
                            }
                        }
                        if(!checkRequired(existsUrl)){
                            share.contactAction(url || '#')
                        }
                        else share.contactAction(existsUrl)
                    }
                    share.updateSendEmailContent("","",true,true,true,$scope.p_name || name,emailId,$scope.cUserId || userId,$scope.cUserId || userId,$scope.companyName || companyName,$scope.designation || designation);
                    share.updateSendEmailContent_tableCtl("","",true,true,true,$scope.p_name || name,emailId,$scope.cUserId || userId,$scope.cUserId || userId,$scope.companyName || companyName,$scope.designation || designation);
                    $scope.getInteractionTypeObj = function(obj,total){
                        switch (obj._id){
                            case 'google-meeting':
                            case 'meeting':return {
                                priority:0,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'call':return {
                                priority:1,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'sms':return {
                                priority:2,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'email':return {
                                priority:3,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'facebook':return {
                                priority:4,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'twitter':return {
                                priority:5,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            case 'linkedin':return {
                                priority:6,
                                value:'height:'+$scope.calculatePercentage(obj.count,total)+'%',
                                type:obj._id,
                                count:obj.count,
                                title:obj.count+' interactions'
                            };
                                break;

                            default : return null;
                        }
                    };

                    $scope.calculatePercentage = function(count,total){
                        return Math.round((count*100)/total);
                    };
                    $scope.totalInteractionsCount = 0;
                    $scope.interactionsByType = [];

                    if(response.Data.interactionTypes && response.Data.interactionTypes.length > 0){
                        $scope.totalInteractionsCount = response.Data.interactionTypes[0].totalCount;

                        var existingTypes = [];
                        var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                        for(var t=0; t<response.Data.interactionTypes[0].typeCounts.length; t++){
                            var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionTypes[0].typeCounts[t],response.Data.interactionTypes[0].maxCount);
                            if(interactionTypeObj != null){
                                existingTypes.push(interactionTypeObj.type);
                                $scope.interactionsByType.push(interactionTypeObj);
                            }
                        }
                        var mobileCountStatus = 0;
                        var mobileCountStatusLists = ['call','sms','email'];
                        for(var u=0; u<allTypes.length; u++){
                            if(existingTypes.indexOf(allTypes[u]) == -1){
                                var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionTypes[0].totalCount)
                                if(newObj != null){
                                    if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                        mobileCountStatus ++;
                                    }
                                    $scope.interactionsByType.push(newObj);
                                }
                            }
                        }

                        if(mobileCountStatus > 0){
                            $scope.showDownloadMobileApp = true;
                        }else $scope.showDownloadMobileApp = false;
                        $scope.showSocialSetup = response.Data.showSocialSetup;

                        $scope.interactionsByType.sort(function (o1, o2) {
                            return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                        });
                        $scope.interactionsByType = {
                            a:$scope.interactionsByType[0],
                            b:$scope.interactionsByType[1],
                            c:$scope.interactionsByType[2],
                            d:$scope.interactionsByType[3],
                            e:$scope.interactionsByType[4],
                            f:$scope.interactionsByType[5],
                            g:$scope.interactionsByType[6]
                        };
                    }
                }
            });

    };

    $scope.updateFavorite = function(p_id,favorite){
        var reqObj = {contactId:p_id,favorite:!favorite};
        $http.post('/contacts/update/favorite/web',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.p_favorite = reqObj.favorite;
                    $scope.p_favoriteColor = reqObj.favorite == true ? 'color: #f86b4f;' : 'color: #777777!important';
                    $scope.p_favoriteColor_a = reqObj.favorite == true ? 'color: #f86b4f;background: transparent !important;border: none;' : 'color: #777777!important';
                }
                else{
                    toastr.error(response.Message);
                }
            })
    };

    $scope.getPosition = function(companyName,designation){
        var position = '';
        if(checkRequired(companyName) && checkRequired(designation)){
            position = designation+', '+companyName
        }
        else if(checkRequired(companyName) && !checkRequired(designation)){
            position = companyName
        }
        else if(!checkRequired(companyName) && checkRequired(designation)){
            position = designation
        }

        return position;
    };

    /*Function to delete a hashtag*/

    $scope.deleteHashtag = function(p_id,hashtag){

        var hashtagIndex = $scope.tag.indexOf(hashtag);

        $http.get("/api/hashtag/delete?contactId="+p_id.toString()+"&hashtag="+hashtag.toString())
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success("Hashtag Deleted");
                }
                else {
                    toastr.error("Hashtag Delete Failed. Please try again later");
                }
            });
        $scope.tag.splice(hashtagIndex, 1);
    };

    /*Function to add a Hashtag*/

    $scope.addHashtag = function (p_id,hashtag,p_emailId) {

        var str = hashtag.replace(/[^\w\s]/gi, '');

        var obj = { contactId:p_id, hashtag: str};
        $http.post("/api/hashtag/new",obj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.hashtag = '';
                    toastr.success("Hashtag - "+hashtag.italics()+" Added!");
                    //window.location = '/contact/selected?context='+p_emailId;
                    //share.showRelation();

                }
                else {
                    toastr.error("Error! Hashtag not Added.");
                }
            });
        share.getContactInfo(p_emailId);
    };

    /*View all Hashtagged contacts*/

    $scope.showHashtags = function(hashtag){
        window.location = '/contact/connect?searchContent='+hashtag+'&yourNetwork=true&extendedNetwork=true&forCompanies=false';
    };

});

reletasApp.controller("action_on_contact",function($scope, $http, share){
    share.contactAction = function(url){
        $scope.scheduleMeeting = url;
    };

    share.updateTaskContents = function(name,emailId,userId){
        $scope.name = name
        $scope.emailId = emailId
        $scope.userId = userId
    };

    $scope.assignTask = function(){
        share.assignTask($scope.name,$scope.emailId,$scope.userId)
    }

    /*Function to add a Hashtag*/

    $scope.addHashtag = function (p_id,hashtag) {

        var str = hashtag.replace(/[^\w\s]/gi, '');

        var obj = { contactId:p_id, hashtag: str};
        $http.post("/api/hashtag/new",obj)
            .success(function(response){
                if(response.SuccessCode){
                    $scope.hashtag = '';
                    toastr.success("Hashtag Added!");
                    //window.location = '/contact/selected?context='+p_emailId;
                    //share.showRelation();

                }
                else {
                    toastr.error("Error! Hashtag not Added.");
                }
            });

    };
});

/* Create an array with the values of all the select options in a column */
$.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $(td).attr("dateSort");
    } );
}

reletasApp.controller("contact_interaction_table",function($scope, $http, share, DTOptionsBuilder, DTColumnDefBuilder,searchService,$sce){

    $scope.searchContacts = function(keywords){

        if(keywords && keywords.length > 2){

            searchService.search(keywords).success(function(response){
                if(response.SuccessCode){
                    $scope.showResults = true;
                    processSearchResults($scope,$http,response.Data);
                } else {
                    $scope.showResults = true;
                    $scope.contacts = [];
                    var obj = {
                        fullName: '',
                        name: '',
                        image: '',
                        emailId:keywords
                    }
                    $scope.contacts.push(obj)
                }
            }).error(function(){
                console.log('error');
            });
        } else {
            $scope.contacts = null;
        }
    };

    $scope.searchedContacts = [];

    $scope.addRecipient = function (contact) {
        var emailId = contact.emailId
        if(validateEmail(emailId)){
            $scope.searchedContacts.push(emailId)
            $scope.contacts = [];
            $scope.showResults = false;
            $scope.email_cc = ''
        } else {
            toastr.error("Please enter a valid email ID")
        }
    }

    $scope.removeRecipient = function(emailId){
        var rmIndex = $scope.searchedContacts.indexOf(emailId);
        $scope.searchedContacts.splice(rmIndex, 1);
    }

    $scope.interactions = [];
    $scope.textload = "Loading interactions. Please wait..";
    $scope.textProcess = function(){return $scope.textload}
    var vm = this;
    vm.persons = [];
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(10).withLanguage({
        "oPaginate": {
            "sNext": "»",
            "sPrevious": "«"
        }
    })
        .withOption('columns', [{ "orderDataType": "dom-value" },null,null,{ "orderDataType": "dom-value" },null])
        .withOption('order', [[ 0, "desc" ]])
        .withOption("bDestroy",true)
        .withOption('oLanguage', {
            "sEmptyTable": $scope.textProcess,
            "sLengthMenu": "Show _MENU_ for page",
            //"sProcessing": "DataTables is currently busy",
            //"bProcessing":false,
            "processing":true
        });

    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1).notSortable(),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4).notSortable()
    ];
    vm.dtInstance = {

    };
    vm.dtInstanceCallback = dtInstanceCallback;
    function dtInstanceCallback(dtInstance) {
        vm.dtInstance = dtInstance;
    }
    $scope.show_loadMore = false;
    $scope.getInteractions = function(emailId,companyName,designation,name,url,userId, skip, limit){
        $scope.interactions = [];
        //vm.persons = $scope.interactions;
        share.getInteractions(emailId,companyName,designation,name,url,userId, skip, limit, "Loading all interactions. Please wait..")
    };

    share.getInteractions = function(emailId,companyName,designation,name,url,userId,mobileNumber, skip, limit, message){

        $scope.show_loadMore = false;
        $scope.cEmailId = emailId
        $scope.companyName = companyName
        $scope.designation = designation
        $scope.p_name = name
        $scope.url = url
        $scope.cuserId = userId;

        $scope.interactions = [];
        skip = skip || 0;
        limit = limit || 100;

        //vm.persons = $scope.interactions;
        if(message){
            $scope.textload = message;
            vm.dtInstance.DataTable.clear().draw();
        }
        else{
            $scope.textload = "Loading interactions. Please wait..";
            $scope.interactions = [];
            if(vm.dtInstance && vm.dtInstance.DataTable){
                vm.dtInstance.DataTable.clear().draw();
            }
        }

        var url;
        var fetchWith = emailId
        url = "/interactions/get/days/summary/emailId/"+fetchWith+"?skip="+skip+"&limit="+limit
            if(isNumber(emailId)) {
            fetchWith = emailId ? emailId.replace(/[^a-zA-Z0-9]/g,''):name.replace(/[^a-zA-Z0-9]/g,'')
            url = "/interactions/get/days/summary/mobileNumber/"+fetchWith+"?skip="+skip+"&limit="+limit
        } else if(!emailId && isNumber(mobileNumber)){
                fetchWith = mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
                url = "/interactions/get/days/summary/mobileNumber/"+fetchWith+"?skip="+skip+"&limit="+limit
            }

        if(isNumber(mobileNumber)){
            url = url+'&mobileNumber='+mobileNumber.replace(/[^a-zA-Z0-9]/g,'')
        }

        $http.get(url)
            .success(function(response){

                if(response.SuccessCode){
                    $scope.totalRecords = response.Data.total;
                    $scope.page_records = response.Data.returned;
                    $scope.show_loadMore = $scope.page_records < $scope.totalRecords
                    for(var i=0; i<response.Data.Data.length; i++){
                        var actionTakenOn = '';
                        var actionTakenDate = null;
                        var title = '';
                        var remaindDays = response.remaindDays;
                        if(checkRequired(response.Data.Data[i].trackInfo) && checkRequired(response.Data.Data[i].trackInfo.lastOpenedOn)){
                            actionTakenDate = moment(response.Data.Data[i].trackInfo.lastOpenedOn).format()
                            actionTakenOn = moment(response.Data.Data[i].trackInfo.lastOpenedOn).tz(response.timezone).format("DD MMM YYYY")
                        }
                        if(checkRequired(response.Data.Data[i].title)){
                            title = response.Data.Data[i].title;
                        }
                        else if(checkRequired(response.Data.Data[i].description)){
                            title = response.Data.Data[i].description;
                        }
                        var dataObj = $scope.getInteractionTypeImage(response.Data.Data[i],name,emailId,remaindDays,response.timezone);
                        if(checkRequired(dataObj.title)){
                            title = dataObj.title;
                        }
                        var colorClass = "";
                        if(dataObj.colorRed){
                            colorClass = 'color:#F86A52'
                        }
                        else if(dataObj.colorBlue){
                            colorClass = 'color:#269ED8'
                        }

                        if(colorClass == 'color:#269ED8'){
                            var viewedOn = moment(response.Data.Data[i].trackInfo.lastOpenedOn).format("DD MMM YYYY, h:mm a");
                            var isEmailRead = true;
                            colorClass = 'color:#2d3e48';
                            var localtimezone = '';

                            //$scope.$watch('$viewContentLoaded', function()
                            //{
                            //    localtimezone = (jstz.determine().name());
                            //
                            //});


                        } else {
                            var viewedOn = '';
                            var isEmailRead = false;
                        }

                        if(response.Data.Data[i].interactionType == 'meeting'){
                            if(checkRequired(response.Data.Data[i].trackInfo) && response.Data.Data[i].trackInfo.action == "confirmed"){
                                dataObj.meetingActionTextShow = true;
                                dataObj.meetingActionText = "Confirmed";
                            }
                            else if(response.Data.Data[i].endDate){
                                dataObj.meetingActionTextShow = true;
                                dataObj.meetingActionText = "Confirmed";
                            }
                        }
                        dataObj.updateMailRead = true;
                        var obj = {
                            dateSort:moment(response.Data.Data[i].interactionDate).format(),
                            actionTakenDate:actionTakenDate,
                            dateInteracted:moment(response.Data.Data[i].interactionDate).tz(response.timezone).format("DD MMM YYYY"),
                            title:getTextLength(title,50),
                            fullTitle:title,
                            actionTakenOn:actionTakenOn,
                            actionTakenClass:colorClass,
                            viewedOn: viewedOn,
                            emailOpens:response.Data.Data[i].trackInfo && response.Data.Data[i].trackInfo.emailOpens ? response.Data.Data[i].trackInfo.emailOpens : 0,
                            emailRead: isEmailRead,
                            //localtimezone: moment().tz(localtimezone).zoneAbbr(),
                            refId:response.Data.Data[i].refId,
                            dataObj:dataObj
                        };
                        $scope.interactions.push(obj);

                    }

                    vm.persons = $scope.interactions;

                }
                else{
                    $scope.textload = "There are no interactions";
                    $scope.interactions = [];
                    if(vm.dtInstance && vm.dtInstance.DataTable){
                        vm.dtInstance.DataTable.clear().draw();
                    }
                }
            })
    };

    $scope.getInteractionTypeImage = function(interaction,name,emailId,remaindDays,timezone){

        switch(interaction.interactionType){
            case 'google-meeting':
            case 'meeting':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    iconClass:"fa-calendar-check-o green-color",
                    iconTitle:"Meeting",
                    followUpBut:false,
                    actionType:actionType,
                    ignoreBut:false,
                    tweetReplyBut:false,
                    reTweetBut:false,
                    tweetFavorite:false,
                    commentBut:false
                };
                break;
            case 'document-share':
                return {
                    iconClass:"fa-file-pdf-o",
                    iconTitle:"Document",
                    docActionView:interaction.action == 'sender',
                    docActionAnalytics:interaction.action != 'sender',
                    docActionButText:interaction.action == 'sender' ? 'View':'View Stats',
                    action:interaction.action == 'sender' ? 'viewDoc':'viewStats',
                    url:'/readDocument/'+interaction.refId,
                    docId:interaction.refId
                };
                break;
            case 'meeting-comment':
            case 'message':
            case 'email':
                var emailFollowUpBut = false;
                var emailIgnoreBut = false;
                var colorRed = false;
                var colorBlue = false;
                var showResponseDate = false;
                var title = "";
                var updateOpened = false;
                var updateReplied = false;
                var replyBut = interaction.action != 'receiver';
                if(interaction.action == 'sender'){
                    if(checkRequired(interaction.trackInfo)){
                        if(interaction.trackInfo.trackOpen){
                            updateOpened = true;
                        }
                        if(interaction.trackInfo.trackResponse){
                            updateReplied = true;
                        }
                        if(interaction.trackInfo.gotResponse){
                            replyBut = false;
                        }
                    }
                }
                if(interaction.action == 'receiver' && !interaction.ignore && interaction.interactionType == 'email' && interaction.source == 'relatas'){

                    if(checkRequired(interaction.trackInfo)){
                        if(interaction.trackInfo.trackOpen){
                            // track info enabled
                            if(interaction.trackInfo.isRed){
                                // opened or no action
                                emailFollowUpBut = true;
                                emailIgnoreBut = false;
                                colorBlue = true;
                                showResponseDate = true;
                            }
                            else{
                                // not opened or no action
                                emailFollowUpBut = true;
                                emailIgnoreBut = true;
                                colorRed = true;
                            }
                        }

                        if(interaction.trackInfo.trackResponse){
                            // track response enabled
                            var interactionDate = moment(interaction.interactionDate).tz(timezone);
                            interactionDate.date(interactionDate.date() + remaindDays);
                            var now = moment().tz(timezone);

                            if(now.isAfter(interactionDate) || now.format("DD-MM-YYYY") == interactionDate.format("DD-MM-YYYY")){

                                if(interaction.trackInfo.gotResponse){
                                    // got response
                                    emailFollowUpBut = false;
                                    //emailFollowUpBut = true;
                                    emailIgnoreBut = false;
                                    colorBlue = false;
                                    colorRed = false;
                                    showResponseDate = true;
                                }
                                else{
                                    // not response
                                    if(checkRequired(interaction.title)){
                                        title = interaction.title;
                                    }
                                    else if(checkRequired(interaction.description)){
                                        title = interaction.description;
                                    }
                                    title = "Notification remainder, Subject: "+title;
                                    emailFollowUpBut = true;
                                    emailIgnoreBut = true;

                                    if(!colorBlue){
                                        colorRed = true;
                                        colorBlue = false;
                                    }
                                }
                            }
                            else{
                                if(interaction.trackInfo.gotResponse){
                                    emailFollowUpBut = true;
                                }

                                if(interaction.trackInfo.gotResponse){
                                    // got response
                                    emailFollowUpBut = false;
                                    //emailFollowUpBut = true;
                                    emailIgnoreBut = false;
                                    colorBlue = false;
                                    colorRed = false;
                                    showResponseDate = true;
                                }
                            }
                        }
                    }
                }
                var subject = "Re: "+interaction.title;
                var body = "";
                if(checkRequired(interaction.description)){
                    body = "\n\n"+interaction.description
                }

                if(interaction.title && interaction.title.substring(0,2).toLowerCase() == 're'){
                    subject = interaction.title
                }

                if(checkRequired(interaction.trackInfo)){
                    if(interaction.trackInfo.gotResponse){
                        replyBut = false;
                    }
                }

                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }

                return {
                    iconClass:"fa-envelope green-color",
                    iconTitle:"Email",
                    followUpBut:emailFollowUpBut,
                    ignoreBut:emailIgnoreBut,
                    tweetReplyBut:false,
                    reTweetBut:false,
                    tweetFavorite:false,
                    commentBut:false,
                    _id:interaction._id,
                    title:title,
                    subjectNormal:interaction.title,
                    compose_email_subject:subject,
                    compose_email_body:"",
                    compose_email_track_viewed:true,
                    compose_email_remaind:true,
                    compose_email_doc_tracking:true,
                    body:body,
                    isCalPassReq:false,
                    colorBlue:colorBlue,
                    colorRed:colorRed,
                    showResponseDate:showResponseDate,
                    replyBut:replyBut,
                    replyBut:interaction.ignore ? false : !emailFollowUpBut,
                    interaction:interaction,
                    emailPointer:"cursor:pointer",
                    updateOpened:updateOpened,
                    emailId:null,
                    userId:interaction.userId,
                    trackId:interaction.trackId,
                    updateReplied:updateReplied,
                    actionType:actionType
                };
                break;
            case 'sms':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    iconClass:"fa-reorder green-color",
                    iconTitle:"SMS",
                    followUpBut:false,
                    ignoreBut:false,
                    tweetReplyBut:false,
                    reTweetBut:false,
                    tweetFavorite:false,
                    commentBut:false,
                    actionType:actionType
                };
                break;
            case 'call':

                var contact;
                $scope.isMobileInteraction = true;

                if(interaction.fullName) {
                    contact = interaction.fullName
                } else if(interaction.emailId){
                    contact = interaction.emailId
                } else {
                    contact = interaction.mobileNumber
                }

                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                    var callDescription = "You called " +contact+"."
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                    var callDescription = contact + " called you."
                }

                return {
                    iconClass:"fa-phone green-color",
                    actionType:actionType,
                    iconTitle:"Call",
                    followUpBut:false,
                    ignoreBut:false,
                    tweetReplyBut:false,
                    reTweetBut:false,
                    tweetFavorite:false,
                    callDescription:callDescription,
                    commentBut:false
                };
                break;
            case 'task':
                var status = checkRequired(interaction.trackInfo) && interaction.trackInfo.status;
                return {
                    iconClass:"fa-tasks",
                    iconTitle:"Tasks",
                    taskCheckBut:status || interaction.action == 'sender',
                    status:status,
                    updateTaskStatus:interaction.action == 'sender',
                    taskId:interaction.refId,
                    _id:interaction._id
                };
                break;
            case 'twitter':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                return {
                    iconClass:"fa-twitter-square twitter-color",
                    iconTitle:"Twitter",
                    tweetReplyBut:true,
                    reTweetBut:interaction.action == 'sender',
                    tweetFavorite:true,
                    postId:interaction.refId,
                    replyAction:'reply',
                    reTweet:'re-tweet',
                    favorite:'favorite',
                    _id:interaction._id,
                    actionType:actionType
                };
                break;
            case 'facebook':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                var shareLinkFlag = false;
                var likeLinkFlag = false;
                var commentLinkFlag = false;
                if(interaction.interactionType == 'facebook' && interaction.subType == "share"){
                    shareLinkFlag = true;
                    likeLinkFlag = true;
                    commentLinkFlag = true;
                }
                if(interaction.interactionType == 'facebook' && interaction.subType == "status"){
                    likeLinkFlag = true;
                    commentLinkFlag = true;
                }
                if(interaction.interactionType == 'facebook' && interaction.subType == "comment"){
                    likeLinkFlag = true;
                    commentLinkFlag = true;
                }
                if(interaction.interactionType == 'facebook' && interaction.subType == "like"){
                    likeLinkFlag = false;
                }
                return {
                    iconClass:"fa-facebook-official fb-color",
                    iconTitle:"Facebook",
                    commentBut:false,
                    likeLink: likeLinkFlag,
                    shareLink: shareLinkFlag,
                    commentLink: commentLinkFlag,
                    postId:interaction.refId,
                    actionType:actionType
                };
                break;
            case 'linkedin':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                if(interaction.interactionType == 'linkedin' && interaction.subType == "share"){
                    var lFlag = true;
                } else{
                    var lFlag = false;
                }
                return {
                    iconClass:"fa-linkedin-square linkedin-color",
                    iconTitle:"Linkedin",
                    shareBtn:lFlag,
                    postUrl: interaction.postURL,
                    actionType:actionType
                };
                break;
            case 'calendar-password':
                if(interaction.action == 'receiver') {
                    var actionType = "fa fa-arrow-right orange-color"
                } else {
                    var actionType = "fa fa-arrow-left orange-off-color"
                }
                var calendarPasswordApprove = false;
                var calPassIgnoreBut = false;
                if(interaction.action == 'sender' && !interaction.ignore){
                    calendarPasswordApprove = true;
                    calPassIgnoreBut = true;
                }

                return {
                    iconClass:"fa-calendar-check-o green-color",
                    iconTitle:"Calendar Password Request",
                    followUpBut:false,
                    calendarPasswordApprove:calendarPasswordApprove,
                    ignoreBut:calPassIgnoreBut,
                    tweetReplyBut:false,
                    reTweetBut:false,
                    tweetFavorite:false,
                    commentBut:false,
                    isCalPassReq:true,
                    _id:interaction._id,
                    reqId:interaction.refId,
                    name:name,
                    emailId:emailId,
                    actionType:actionType
                };
                break;
            default :

                return {iconClass:''};
                break;
        }
    };

    share.ignoreEmail = function(id,isCalPassReq,reqId){
        $scope.ignoreEmail(id,isCalPassReq,reqId);
    };

    $scope.ignoreEmail = function(id,isCalPassReq,reqId){
        if(checkRequired(id)){
            $http.post('/interactions/update/interaction/ignore',{interactionId:id,isCalPassReq:isCalPassReq,requestId:reqId})
                .success(function(response){
                    if(response.SuccessCode){
                        //share.getInteractions($scope.cEmailId,$scope.companyName,$scope.designation,$scope.p_name,$scope.url,$scope.cuserId);
                        for(var i=0; i<$scope.interactions.length; i++){
                            if($scope.interactions[i].dataObj._id == id){
                                $scope.interactions[i].dataObj.followUpBut = false;
                                $scope.interactions[i].dataObj.ignoreBut = false;
                                //$scope.interactions[i].actionTakenOn = moment().format("DD MMM YYYY")
                                //$scope.interactions[i].actionTakenDate = moment().format()
                                $scope.interactions[i].actionTakenClass = ""
                            }
                        }
                    }
                    else toastr.error(response.Message)
                })
        }
    };

    $scope.showEmailBody = function(dataObj,emailAction,doIgnore){
        if(dataObj.iconTitle == "Email"){
            //if(!dataObj.composeMailBox && !dataObj.emailBodyBox){

            if(emailAction == 'reply'){
                dataObj.isView = false;
                if(!dataObj.composeMailBox){
                    dataObj.doIgnore = doIgnore;
                    dataObj.composeMailBox = true;
                    dataObj.emailBodyBox = true;
                }
                else{
                    dataObj.doIgnore = doIgnore;
                    dataObj.composeMailBox = false;
                    dataObj.emailBodyBox = false;
                }
            }
            else{
                if(!dataObj.isView){
                    dataObj.doIgnore = false;
                    dataObj.isView = true;
                    dataObj.composeMailBox = false;
                    dataObj.emailBodyBox = true;
                    dataObj.loading = true;
                }
                else{
                    dataObj.composeMailBox = false;
                    dataObj.emailBodyBox = false;
                    dataObj.isView = false;
                }
            }
            if(checkRequired(dataObj.bodyContent)){
                dataObj.interaction.description = dataObj.bodyContent.replace(/\n/g, "<br />")
                dataObj.interaction.source = 'relatas'
            }
            if(checkRequired(dataObj.interaction.emailContentId) && dataObj.interaction.source != 'relatas'){
                $http.get('/message/get/email/single/web?emailContentId='+dataObj.interaction.emailContentId+'&googleAccountEmailId='+dataObj.interaction.googleAccountEmailId)
                    .success(function(response){
                        dataObj.loading = false;
                        if(response.SuccessCode){
                            dataObj.bodyContent = $sce.trustAsHtml(response.Data.data);

                            setTimeOutCallback(10,function () {
                                var myDomElement = document.getElementsByClassName("timeline-view");
                                var selection = $( myDomElement ).find( "style" );
                                if(selection.length){
                                    selection.remove();
                                }
                            });

                            if(dataObj.updateOpened && dataObj.updateMailRead){
                                $scope.updateEmailOpen(dataObj.emailId,dataObj.trackId,dataObj.userId);
                            }
                        }
                        else dataObj.bodyContent = response.Message || '';
                    })
            }
            else{
                dataObj.loading = false;
                dataObj.bodyContent = dataObj.interaction.description.replace(/\n/g, "<br />");
                if(dataObj.updateOpened && dataObj.updateMailRead){
                    $scope.updateEmailOpen(dataObj.emailId,dataObj.trackId,dataObj.userId);
                }
            }
            dataObj.updateMailRead = dataObj.updateMailRead ? false : true;

        }
    };

    $scope.updateEmailOpen = function(emailId,trackId,userId){

        if(!emailId){
            emailId = share.lUseEmailId;
        }

        if(checkRequired(emailId) && checkRequired(trackId) && checkRequired(userId)){
            $http.get('/track/email/open/'+emailId+'/track/id/'+trackId+'/user/'+userId)
                .success(function(response){

                });
        }
    };

    share.updateSendEmailContent_tableCtl = function(compose_email_subject,compose_email_body,compose_email_doc_tracking,compose_email_track_viewed,compose_email_remaind,p_name,cEmailId,cuserId,url,companyName,designation,interactionId){
        $scope.compose_email_subject = compose_email_subject;
        $scope.compose_email_body = compose_email_body;
        $scope.compose_email_doc_tracking = compose_email_doc_tracking;
        $scope.compose_email_track_viewed = compose_email_track_viewed;
        $scope.compose_email_remaind = compose_email_remaind;
        $scope.p_name = p_name;
        $scope.cEmailId = cEmailId;
        $scope.cuserId = cuserId;
        $scope.url = url;
        $scope.companyName = companyName;
        $scope.designation = designation;
        $scope.interactionId = interactionId;
    };

    $scope.approveCalPass = function(interactionId,requestId,requestedByName,requestedByEmailId){
        if(checkRequired(interactionId) && checkRequired(requestId) && checkRequired(requestedByEmailId) && checkRequired(requestedByName)){
            var reqObj = {
                requestedByName:requestedByName,
                requestedByEmailId:requestedByEmailId,
                requestId:requestId,
                accepted:true
            };
            $http.post('/schedule/action/calendarPasswordResponse',reqObj)
                .success(function(response){
                    if(response.SuccessCode){
                        $scope.ignoreEmail(interactionId,false,null)
                        //share.getInteractions($scope.cEmailId,$scope.companyName,$scope.designation,$scope.p_name,$scope.url,$scope.cuserId);
                    }
                    else toastr.error(response.Message)
                })
        }
    };

    $scope.updateTaskStatus = function(taskId,status,interactionId){
        var reqObj = {
            taskId:taskId,
            status:!status ? 'notStarted':'complete',
            lUseEmailId:share.lUseEmailId,
            cEmailId:$scope.cEmailId
        };
        $http.post('/task/update/status/web',reqObj)
            .success(function(response){
                if(response.SuccessCode){
                    toastr.success(response.Message);
                    $scope.interactions.forEach(function(item){
                        if(item.dataObj.taskId == taskId){
                            item.actionTakenDate = moment().format("DD MMM YYYY")
                            item.actionTakenDate = moment().format()
                        }
                    });
                    // share.getInteractions_out()
                }
                else toastr.error(response.Message);
            })
    };

    share.getInteractions_out = function(refId){
        $scope.interactions.forEach(function(item){
            if(item.refId == refId){
                item.actionTakenDate = moment().format("DD MMM YYYY")
                item.actionTakenDate = moment().format()
            }
        });
        // share.getInteractions($scope.cEmailId,$scope.companyName,$scope.designation,$scope.p_name,$scope.url,$scope.cuserId);
    };

    $scope.docActionView = function(docId) {
        window.location = '/readDocument/'+docId;
    };

    $scope.docActionAnalytics = function(docId) {
        share.showDocAnalytics($scope.cEmailId,docId)
    };

    $scope.twitterAction = function(postId,action){
        share.commentSocialFeed(postId,action,$scope.cEmailId);
    };

    $scope.facebookLike = function(refId){
        //console.log(refId);
        $http.put('/social/feed/facebook/like?objectId='+refId)
            .success(function(response){
                if(response.SuccessCode == 0){
                    toastr.error("Error! The access tokens have expired. Please ADD your Facebook account to Relatas in the profile <a href=/settings> Settings </a>section.");
                } else {
                    toastr.success("Post Successfully Liked");
                }
            });
    };

    $scope.linkedinShare = function(postUrl){
        //console.log(postUrl);
        $http.put('/social/feed/linkedin/share?url='+postUrl)
            .success(function(response){
                if(response.SuccessCode == 0){
                    toastr.error("Error! The access tokens have expired. Please ADD your LinkedIn account to Relatas in the profile <a href=/settings> Settings </a>section.");
                } else {
                    toastr.success("Post Successfully Shared on LinkedIn");
                }
            });
    };

    $scope.facebookShare = function(refId){
        //console.log(refId);
        $http.put('/social/feed/facebook/share?objectId='+refId)
            .success(function(response){
                if(response.SuccessCode == 0){
                    toastr.error("Error! The access tokens have expired. Please ADD your Facebook account to Relatas in the profile <a href=/settings> Settings </a>section.");
                } else {
                    toastr.success("Post Successfully Shared on Facebook");
                }
            });
    };

    $scope.facebookComment = function(refId, comment){
        //console.log(refId);
        $http.put('/social/feed/facebook/comment?objectId='+refId+'&msg='+comment)
            .success(function(response){
                if(response.SuccessCode == 0){
                    toastr.error("Error! The access tokens have expired. Please ADD your Facebook account to Relatas in the profile <a href=/settings> Settings </a>section.");
                } else {
                    toastr.success("Commented Successfully on Facebook");
                }
            });
    };

    $scope.resetFields = function(){
        $scope.compose_email_subject = "";
        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
    };

    $scope.sendEmail = function(subject,body,docTrack,trackViewed,remind,rName,rEmailId,receiverId,dataObj){
        
        if(!checkRequired(subject)){
            toastr.error("Please enter an email subject.")
        }
        else if(!checkRequired(body)){
            toastr.error("Please enter an email body.")
        }
        else{

            if(checkRequired(dataObj.bodyContent)){
                body = body+'\n\n\n'+dataObj.bodyContent;
            }

            var obj = {
                email_cc:$scope.searchedContacts.length>0?$scope.searchedContacts:null,
                receiverEmailId:rEmailId,
                receiverName:rName,
                message:body,
                subject:subject,
                receiverId:receiverId,
                docTrack:docTrack,
                trackViewed:trackViewed,
                remind:remind
            };
            if(dataObj.updateReplied){
                obj.updateReplied = true;
                obj.refId = dataObj.interaction.refId
            }

            //Used for Outlook
            if(dataObj && dataObj.interaction && dataObj.interaction.refId){
                obj.id = dataObj.interaction.refId
            }

            $("#send-email-but").addClass("disabled")
            $(".send-email-but").addClass("disabled")
            $http.post("/messages/send/email/single/web",obj)
                .success(function(response){
                    if(response.SuccessCode){
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();
                        dataObj.composeMailBox = false;
                        dataObj.emailBodyBox = false;
                        dataObj.compose_email_body = "";
                        dataObj.compose_email_doc_tracking = true;
                        dataObj.compose_email_track_viewed = true;
                        dataObj.compose_email_remaind = true;
                        if(checkRequired(dataObj.doIgnore)){
                            dataObj.doIgnore = false;
                            share.ignoreEmail(dataObj._id,false,null);
                        }

                        toastr.success(response.Message);
                        setTimeout(function(){
                            share.getInteractions($scope.cEmailId,$scope.companyName,$scope.designation,$scope.p_name,$scope.url,$scope.cuserId);
                        },2000)
                    }
                    else{
                        $("#send-email-but").removeClass("disabled")
                        toastr.error(response.Message);
                    }
                    //$scope.interactionId = null;
                })
        }
    };
});

function removeDuplicates(arr) {
    var end = arr.length;

    for (var i = 0; i < end; i++) {
        for (var j = i + 1; j < end; j++) {
            if (arr[i].pageNumber == arr[j].pageNumber) {
                var shiftLeft = j;
                for (var k = j + 1; k < end; k++, shiftLeft++) {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for (var i = 0; i < end; i++) {
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

reletasApp.controller("docAnalytics",function($scope, $http, share){
    share.showDocAnalytics = function(emailId,refId){
        $http.get('/analytics/'+refId+'/byEmail/'+emailId)
            .success(function(response){
                if(response && response.accessInfo && response.accessInfo.pageReadTimings){

                    var prt = response.accessInfo.pageReadTimings;
                    var abc = prt.slice();
                    var prtNoDups = removeDuplicates(abc);

                    var separate = [];
                    for (var i in prtNoDups) {

                        var totalTime = 0;
                        var count = 0;
                        var pageNo = 0;
                        for (var j in prt) {
                            if (prt[j].pageNumber == prtNoDups[i].pageNumber) {

                                totalTime = totalTime + parseFloat(prt[j].Time);
                                count += 1;
                                pageNo = prtNoDups[i].pageNumber;
                            }
                        }
                        //  if(totalTime > 0){
                        var before = totalTime;

                        // totalTime = totalTime/count
                        totalTime = Math.round(totalTime / 60) // im minutes
                        if (totalTime == 0) {
                            if (before > 0) {
                                totalTime = 0.5
                            }
                        }
                        // }

                        var tootlTip = 'Page:' + pageNo + '\nMinutes:' + totalTime;
                        separate.push([
                            pageNo,
                            totalTime,
                            tootlTip,
                            'color:#03A2EA'
                        ])
                        var timeMax = 0;
                        jQuery.map(separate, function (obj) {
                            if (obj[1] > 0)
                                timeMax = obj[1];
                        });
                    }
                    $scope.firstAccessed = "First Accessed: " + $scope.getDateFormat(response.firstAccessed) + ' ' + $scope.getTimeFormat(response.firstAccessed);
                    $scope.lastAccessed = "Last Accessed : " + $scope.getDateFormat(response.lastAccessed) + ' ' + $scope.getTimeFormat(response.lastAccessed)
                    $scope.numerOfTimesAccessed = "Number of times accessed: " + response.accessCount || 0

                    $("#analytics-box").toggle(100);
                    var position = $("#"+refId).offset();
                    $("#analytics-box").css({top:position.top});

                    if (checkRequired(separate[0])) {
                        $scope.drawAnalytics(separate, prtNoDups.length, timeMax);
                    }
                    else {
                        toastr.info("This document not accessed yet.");
                    }
                }
                else toastr.info("Document not accessed yet.");
            })
    };

    $scope.drawAnalytics = function(a, maxPages, timeMax) {

        var data = new google.visualization.DataTable();

        data.addColumn('number', 'Pages');
        data.addColumn('number', 'Minutes');
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role: 'style'});
        data.addRows(a);
        var options = {
            width: 250,
            hAxis: {title: 'Page', minValue: 0, maxValue: maxPages + 2},
            vAxis: {title: 'Total Page Open Time', minValue: 0, maxValue: timeMax + 2}
        };


        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById("doc_chart"));
        chart.draw(data, options);

    };

    $scope.getDateFormat = function(date) {
        var dateObj = moment(date).tz(timezone)
        return dateObj.format("DD MMM, YYYY")
        //return dateObj.getDate() + ' ' + monthNameSmall[dateObj.getMonth()] + ',' + dateObj.getFullYear()
    };

    $scope.getTimeFormat = function(date) {
        var dateObj = moment(date).tz(timezone);
        return dateObj.format("hh:mm a")
    };
});

reletasApp.controller("send_email_controller",function($scope, $http, share,searchService){

    $scope.searchContacts = function(keywords){

        if(keywords && keywords.length > 2){

            searchService.search(keywords).success(function(response){
                if(response.SuccessCode){
                    $scope.showResults = true;
                    processSearchResults($scope,$http,response.Data);
                } else {
                    $scope.showResults = true;
                    $scope.contacts = [];
                    var obj = {
                        fullName: '',
                        name: '',
                        image: '',
                        emailId:keywords
                    }
                    $scope.contacts.push(obj)
                }
            }).error(function(){
                console.log('error');
            });
        } else {
            $scope.contacts = null;
        }
    };

    $scope.searchedContacts = [];

    $scope.addRecipient = function (contact) {
        var emailId = contact.emailId
        if(validateEmail(emailId)){
            $scope.searchedContacts.push(emailId)
            $scope.contacts = [];
            $scope.showResults = false;
            $scope.email_cc = ''
        } else {
            toastr.error("Please enter a valid email ID")
        }
    }

    $scope.removeRecipient = function(emailId){
        var rmIndex = $scope.searchedContacts.indexOf(emailId);
        $scope.searchedContacts.splice(rmIndex, 1);
    }

    $scope.resetFields = function(){
        $scope.compose_email_subject = "";
        $scope.compose_email_body = "";
        $scope.compose_email_doc_tracking = true;
        $scope.compose_email_track_viewed = true;
        $scope.compose_email_remaind = true;
        $scope.url = "";
        $scope.companyName = "";
        $scope.designation = "";
    };

    share.ignoreAfterFollowup = function(id){
        // $scope.interactionId = id;
    };

    share.updateSendEmailContent = function(compose_email_subject,compose_email_body,compose_email_doc_tracking,compose_email_track_viewed,compose_email_remaind,p_name,cEmailId,cuserId,url,companyName,designation){
        $scope.compose_email_subject = compose_email_subject;
        $scope.compose_email_body = compose_email_body;
        $scope.compose_email_doc_tracking = compose_email_doc_tracking;
        $scope.compose_email_track_viewed = compose_email_track_viewed;
        $scope.compose_email_remaind = compose_email_remaind;
        $scope.p_name = p_name;
        $scope.cEmailId = cEmailId;
        $scope.cuserId = cuserId;
        $scope.url = url;
        $scope.companyName = companyName;
        $scope.designation = designation;
    };

    $scope.sendEmail = function(subject,body,docTrack,trackViewed,remind,rName,rEmailId,receiverId,interactionId,newMessage){

        if(!checkRequired(subject)){
            toastr.error("Please enter an email subject.")
        }
        else if(!checkRequired(body)){
            toastr.error("Please enter an email body.")
        }
        else{
            var obj = {
                email_cc:$scope.searchedContacts.length>0?$scope.searchedContacts:null,
                receiverEmailId:rEmailId,
                receiverName:rName,
                message:body,
                subject:subject,
                receiverId:receiverId,
                docTrack:docTrack,
                trackViewed:trackViewed,
                remind:remind,
                newMessage:newMessage?newMessage:false
            };
            $("#send-email-but").addClass("disabled")
            $(".send-email-but").addClass("disabled")
            $http.post("/messages/send/email/single/web",obj)
                .success(function(response){
                    if(response.SuccessCode){
                        $("#send-email-but").removeClass("disabled");
                        $(".send-email-but").removeClass("disabled");
                        $scope.resetFields();

                        if(checkRequired(interactionId)){
                            share.ignoreEmail(interactionId,false,null)
                        }
                        else  $(".compose-email-inline").slideToggle(200);
                        toastr.success(response.Message);
                        setTimeout(function(){
                            share.getInteractions($scope.cEmailId,$scope.companyName,$scope.designation,$scope.p_name,$scope.url,$scope.cuserId);
                        },2000)
                    }
                    else{
                        $("#send-email-but").removeClass("disabled")
                        toastr.error(response.Message);
                    }
                    //$scope.interactionId = null;
                })
        }
    };
});

reletasApp.controller("comment-social",function($scope, $http, share){
    share.commentSocialFeed = function(postId,action,cEmailId){
        if(checkRequired(postId) && checkRequired(action) && checkRequired(cEmailId)){

            $scope.postId = postId
            $scope.action = action
            $scope.cEmailId = cEmailId
            if(action == 'reply'){
                $("#comment-section").toggle(100);
                var position = $("#"+postId).offset();
                $("#comment-section").css({top:position.top});
            }
            else if(action == 're-tweet'){
                $http.get('/social/feed/twitter/tweet/update/web?action='+action+'&postId='+postId)
                    .success(function(response){
                        if(response.SuccessCode){
                            toastr.success(response.Message)
                            share.getInteractions_out(postId)
                        }
                        else toastr.error(response.Message);
                    })
            }
            else if(action == 'favorite'){
                $http.get('/social/feed/twitter/tweet/update/web?action='+action+'&postId='+postId)
                    .success(function(response){
                        if(response.SuccessCode){
                            toastr.success(response.Message)
                            share.getInteractions_out(postId)
                        }
                        else toastr.error(response.Message);
                    })
            }
        }
    };

    $scope.submitCommentSocial = function(comment){

        if(checkRequired(comment) && checkRequired(comment.trim())){
            $http.get('/social/feed/twitter/tweet/update/web?action='+$scope.action+'&postId='+$scope.postId+'&emailId='+$scope.cEmailId+'&status='+comment)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success(response.Message);
                        $("#commentSocialFeedId").val("");
                        $scope.commentSocialFeed = ""
                        $("#comment-section").toggle(100);
                        share.getInteractions_out($scope.postId)
                    }
                    else toastr.error(response.Message);
                })
        }
        else toastr.error("Please provide Comment");
    }
});

reletasApp.controller("task-assign",function($scope, $http, share){
    share.assignTask = function(name,emailId,userId){

        $scope.dueDate = null;
        $scope.name = name;
        $scope.emailId = emailId;
        $scope.userId = userId;

        $("#add-task-box").toggle(100);
        var position = $(".assignTasks").offset();
        $("#add-task-box").css({top:position.top+10});
        $('#dueDateTask').datetimepicker({
            dayOfWeekStart: 1,
            format:'d-m-Y h:i a',
            lang: 'en',
            minDate: new Date(),
            onClose: function (dp, $input){
                var selected = moment(dp);
                $scope.dueDate = selected.format();
            }
        });
    };

    $scope.assignTaskFinal = function(title,description){

        if(!checkRequired($scope.dueDate) || moment().isAfter(moment($scope.dueDate)) || moment().isSame(moment($scope.dueDate))){
            toastr.error("Please select valid date and time")
        }
        else if(!(checkRequired(title) && checkRequired(title.trim()))){
            toastr.error("Please provide Task title");
        }
        else if(!(checkRequired(description) && checkRequired(description.trim()))){
            toastr.error("Please provide Task description");
        }
        else if(!checkRequired($scope.dueDate)){
            toastr.error("Please select valid date")
        }
        else{
            var reqObj = {
                "taskName":title,
                "assignedTo":$scope.userId,
                "assignedToEmailId":$scope.emailId,
                "dueDate":$scope.dueDate,
                "taskFor":"other",
                "description":description
            };
            $http.post('/task/create/new/web',reqObj)
                .success(function(response){
                    if(response.SuccessCode){
                        toastr.success(response.Message);
                        $scope.closePopup()
                    }
                    else toastr.error(response.Message);
                })
        }
    };

    $scope.closePopup = function(){
        $("#taskTitle").val("")
        $("#taskDescription").val("")
        $scope.taskTitle = ""
        $scope.taskDescription = ""
        $("#add-task-box").toggle(100);
    }


});

reletasApp.controller("notifications_page_data_controller", function($scope, $http){

    var totalNotificationsCount;
    $scope.counts={losingTouchCount:0,emailResponsesPending:0,emailAwaitingResponses:0};

    $http.get('/dashboard/status/counts/top/web')
        .success(function (response){
            if(response.SuccessCode){
                $scope.counts.losingTouchCount = $scope.getValidStringNumber(response.Data.losingTouchCount || 0);
                $scope.counts.emailResponsesPending = $scope.getValidStringNumber(response.Data.emailResponsesPending || 0);
                $scope.counts.emailAwaitingResponses = $scope.getValidStringNumber(response.Data.emailAwaitingResponses || 0);
                $scope.sumOf1 = parseFloat($scope.counts.losingTouchCount) + parseFloat($scope.counts.emailResponsesPending) + parseFloat($scope.counts.emailAwaitingResponses);
                $http.get('/today/left/section')
                    .success(function (response){
                        if(response.SuccessCode){
                            var checkText = response.Data.pendingTasks;
                            var noTasks = "Click on Add to assign tasks";
                            //$scope.sumOf = parseFloat($scope.openTasksCount)|| 0 + parseFloat($scope.sumOf1)|| 0;
                            if (angular.equals(checkText, noTasks)) {
                                $scope.openTasksCount = 0;
                            }
                            else {
                                $scope.openTasksCount = response.Data.pendingTasks.length;
                            }
                            $scope.sumOf1=$scope.sumOf1+$scope.openTasksCount;

                        }
                    });
            }
            else {
                $scope.counts.losingTouchCount = $scope.getValidStringNumber(0);
                $scope.counts.emailResponsesPending = $scope.getValidStringNumber(0);
                $scope.counts.emailAwaitingResponses = $scope.getValidStringNumber(0);
            }
        }).error(function (data){

        });

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };




    var dateNow = new Date().getTime()

    $http.get('/api/v2/calendar/'+dateNow)

        .success(function (response){

        });

});

Date.parseDate = function( input, format ){
    return moment(input,format).toDate();
};
Date.prototype.dateFormat = function( format ){
    return moment(this).format(format);
};

function changeTextById(id,text){
    $("#"+id).html(text);
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function getInteractionTypeImage (type){
    switch(type){
        case 'google-meeting':
        case 'meeting':
            return "fa-calendar-check-o green-color";
            break;
        case 'document-share':
            return "fa-file-pdf-o";
            break;
        case 'message':
        case 'meeting-comment':
        case 'email':
            return "fa-envelope green-color";
            break;
        case 'sms':
            return "fa-reorder green-color";
            break;
        case 'call':
            return "fa-phone green-color";
            break;
        case 'task':
            return "fa-check-square-o";
            break;
        case 'twitter':
            return "fa-twitter-square twitter-color";
            break;
        case 'facebook':
            return "fa-facebook-official fb-color";
            break;
        case 'linkedin':
            return "fa-linkedin-square linkedin-color";
            break;
        case 'calendar-password':
            return "fa-calendar-check-o green-color";
            break;
        default :return '';
    }
};

function removeRelatasTrackImg(data, index){
    var temp = data;
    var f = temp.lastIndexOf('<img style=', index);
    var e = index + temp.substring(index).indexOf('\">');
    e = e != -1 ? e + 2 : e;
    var slice = temp.slice(f, e)

    return temp = temp.replace(slice, "")
    //return temp = temp;
}

function processSearchResults($scope,$http,response) {

    var contactsArray = response;
    $scope.contacts = [];

    if(contactsArray.length>0){
        for(var i=0;i<contactsArray.length;i++){

            var obj = {};

            if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                var name = getTextLength(contactsArray[i].personName,20);
                var image = '/getImage/'+contactsArray[i].personId._id;

                obj = {
                    fullName:contactsArray[i].personName,
                    name:name,
                    image:image
                };

                obj.emailId = contactsArray[i].personEmailId;
                obj.twitterUserName = contactsArray[i].twitterUserName;

            }
            else {
                var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                obj = {
                    fullName: contactsArray[i].personName,
                    name: getTextLength(contactsArray[i].personName, 20),
                    image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                    emailId:contactsArray[i].personEmailId,
                    twitterUserName: contactsArray[i].twitterUserName
                    // noPicFlag:true
                };
            }

            if(obj.twitterUserName){
                obj.tweetAccExists = true;
            }

            obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
            obj._id = contactsArray[i]._id;


            if(!userExists(obj.name) && validateEmail(obj.emailId)){
                $scope.contacts.push(obj)
            }

            function userExists(username) {
                return $scope.contacts.some(function(el) {
                    return el.name === username;
                });
            }
        }
    }
}

reletasApp.service('searchService', ['$http', function($http){
    return {
        search: function(keywords){
            return $http.post('/search/user/contacts', { "contactName" : keywords});
        }
    }
}]);
