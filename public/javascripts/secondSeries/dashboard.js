
var reletasApp = angular.module('reletasApp', ['ngRoute','angular-loading-bar']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

reletasApp.controller("header_controller", function($scope){
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false
    $scope.getMiddleBarTemplate = function(){
        return "/contacts/left/bar/template"
    }

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        //var str = searchContent.replace(/[^\w\s]/gi, '');

        var str = encodeURIComponent(searchContent);

        if(typeof str == 'string' && str.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+str+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

var timezone ;
var styles = [
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    },
    {
        height: 53,
        url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
        width: 53
    }
]

reletasApp.service('todayService', function () {
    return {}
});

reletasApp.controller("logedinUser", function ($scope, $http,$rootScope) {

    $http.get('/profile/get/current/web')
        .success(function (response) {
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },2000)

            if(response.SuccessCode){
                $scope.l_usr = response.Data;
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
            }
            else{

            }
        }).error(function (data) {

        })
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
reletasApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

reletasApp.controller("dashboard_top_counts", function ($scope, $http, todayService) {
    $scope.emailAwaitingResponses_show = false;
    $http.get('/dashboard/status/counts/top/web')
        .success(function (response) {
            if(response.SuccessCode){
                $scope.losingTouchCount = $scope.getValidStringNumber(response.Data.losingTouchCount || 0);
                $scope.unpreparedMeetingsCount = $scope.getValidStringNumber(response.Data.unpreparedMeetingsCount || 0);
                $scope.upcomingMeetingsCount = $scope.getValidStringNumber(response.Data.upcomingMeetings || 0);
                $scope.emailResponsesPending = $scope.getValidStringNumber(response.Data.emailResponsesPending || 0);
                $scope.emailAwaitingResponses = $scope.getValidStringNumber(response.Data.emailAwaitingResponses || 0);
            }
            else{
                $scope.losingTouchCount = $scope.getValidStringNumber(0);
                $scope.unpreparedMeetingsCount = $scope.getValidStringNumber(0);
                $scope.upcomingMeetingsCount = $scope.getValidStringNumber(0);
                $scope.emailResponsesPending = $scope.getValidStringNumber(0);
                $scope.emailAwaitingResponses = $scope.getValidStringNumber(0);
            }
        }).error(function (data) {

        });

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
});

reletasApp.controller("dashboard_top_five_contacts", function ($scope, $http, todayService) {
    $scope.topFiveInteracted = [];
    $scope.no_top_interacted_show = false;
    $http.get('/interactions/topfive/web')
        .success(function(response){

            if(response.SuccessCode){

                if(response.Data && response.Data.length > 0){
                    for(var i=0; i<response.Data.length; i++){
                        var li = '';
                        var obj = {name:''};
                        if(checkRequired(response.Data[i].firstName) && response.Data[i].firstName.length > 0){
                            response.Data[i].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name = name
                                }
                            })
                        }
                        var done = false;
                        if(checkRequired(response.Data[i].lastName) && response.Data[i].lastName.length > 0){
                            response.Data[i].lastName.forEach(function(name){
                                if(checkRequired(name) && !done){
                                    obj.name += ' '+name
                                    done = true;
                                }
                            })
                        }
                        if(!checkRequired(obj.name) && response.Data[i]._id.emailId){
                            obj.name = response.Data[i]._id.emailId;
                        }
                        else if(!checkRequired(obj.name) && response.Data[i]._id.mobileNumber){
                            obj.name = response.Data[i]._id.mobileNumber
                        }

                        if(checkRequired(obj.name)){
                            obj.fullName = obj.name
                            obj.name = getTextLength(obj.name,12);
                        }
                        if(checkRequired(response.Data[i].userId) && response.Data[i].userId.length > 0){
                            response.Data[i].userId.forEach(function(id){
                                if(checkRequired(id)){
                                    obj.image = '/getImage/'+id;
                                }
                            })
                        }
                        if(!checkRequired(obj.image)){
                            obj.cursor = 'cursor:default';
                            obj.image = null;
                            obj.noPicFlag = true;
                        }else obj.cursor = 'cursor:pointer';

                        if(checkRequired(response.Data[i].publicProfileUrl) && response.Data[i].publicProfileUrl.length > 0){
                            response.Data[i].publicProfileUrl.forEach(function(id){
                                if(checkRequired(id)){
                                    if(id.charAt(0) != 'h'){
                                        obj.url = '/'+id;
                                    }
                                }
                            })
                        }
                        if(!checkRequired(obj.url)){
                            obj.url = null;
                        }
                        obj.interactionsCount = response.Data[i].count || 0;
                        obj.emailId = response.Data[i]._id.emailId;

                        obj.className = i==0? 'col-sm-offset-2' : '';

                        obj.nameNoImg = (obj.name.substr(0,2)).toUpperCase();
                        $scope.topFiveInteracted.push(obj);

                    }
                }
                else {

                }
            }
            else{
                $scope.no_top_interacted_show = true;
                $scope.no_top_interacted_message = response.Message;
            }
        });

    $scope.interactionsContext = function(emailId){

        var obj = {'emailId':emailId};
        $http.post('/addContact/notExists',obj)
            .success(function(response){
                if(response.SuccessCode){
                }
            });

        window.location.replace('/contact/selected?context='+emailId)
    }

    $scope.gotoSchedulePage = function(url){
        if(checkRequired(url)){
            window.location = url;
        }
    }
});

reletasApp.controller("dashboard_past_7_day_interactions_by_contact_type", function ($scope, $http, todayService) {
    $http.get('/dashboard/past/days/interacted/info')
        .success(function (response) {
            if(response.SuccessCode){
                $scope.companies = $scope.getValidStringNumber(response.Data.companies || 0);
                $scope.favorite = $scope.getValidStringNumber(response.Data.favorite || 0);
                if(response.Data.types && response.Data.types.length > 0){
                    $scope.customer = $scope.getValidStringNumber(response.Data.types[0].customer || 0);
                    $scope.prospect = $scope.getValidStringNumber(response.Data.types[0].prospect || 0);
                    $scope.influencer = $scope.getValidStringNumber(response.Data.types[0].influencer || 0);
                    $scope.decision_maker = $scope.getValidStringNumber(response.Data.types[0].decision_maker || 0);
                }
                else {
                    $scope.customer = $scope.getValidStringNumber(0);
                    $scope.prospect = $scope.getValidStringNumber(0);
                    $scope.influencer = $scope.getValidStringNumber(0);
                    $scope.decision_maker = $scope.getValidStringNumber(0);
                }
            }
            else{
                $scope.companies = $scope.getValidStringNumber(0);
                $scope.favorite = $scope.getValidStringNumber(0);
                $scope.customer = $scope.getValidStringNumber(0);
                $scope.prospect = $scope.getValidStringNumber(0);
                $scope.influencer = $scope.getValidStringNumber(0);
                $scope.decision_maker = $scope.getValidStringNumber(0);
            }
        }).error(function (data) {

        });



    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
});

reletasApp.controller("top_companies_locations_interaction_by_type", function ($scope, $http, todayService) {
    $scope.interactionsByCompany = [];
    $scope.interactionsByLocation = [];
    $scope.interactionsByType = [];
    $scope.totalInteractionsCount = 0;
    $http.get('/dashboard/past/days/interacted/info/type')
        .success(function (response) {
            if(response.SuccessCode){

                var sortedArrTopFive = sortObject(response.Data.interactionsByCompany[0].typeCounts)

                if(sortedArrTopFive.length>5){
                    sortedArrTopFive = sortedArrTopFive.slice(0,5)
                }

                if(response.Data && response.Data.interactionsByCompany && response.Data.interactionsByCompany.length > 0 && response.Data.interactionsByCompany[0] && response.Data.interactionsByCompany[0].typeCounts && response.Data.interactionsByCompany[0].typeCounts.length > 0){
                    sortedArrTopFive.forEach(function(item){
                        var obj = {
                            companyName:item.companyName,
                            count:item.count,
                            percentage:'width:'+$scope.calculatePercentage(item.count,response.Data.interactionsByCompany[0].totalCount,100)+'%'
                        };
                        $scope.interactionsByCompany.push(obj)
                    });
                }
                if(response.Data && response.Data.interactionsByLocation && response.Data.interactionsByLocation.length > 0 && response.Data.interactionsByLocation[0] && response.Data.interactionsByLocation[0].typeCounts && response.Data.interactionsByLocation[0].typeCounts.length > 0){
                    $scope.interactionsByLocation = {
                        "name": "",
                        "children": []
                    };
                    response.Data.interactionsByLocation[0].typeCounts.forEach(function(item){
                        var obj = {
                            name:item._id,
                            count:item.count,
                            mcap:$scope.calculatePercentage(item.count,response.Data.interactionsByLocation[0].totalCount,100)
                        };
                        $scope.interactionsByLocation.children.push(obj);
                    });

                    fillLocations($scope.interactionsByLocation)
                }
                if(response.Data && response.Data.interactionsByType && response.Data.interactionsByType.length > 0 && response.Data.interactionsByType[0] && response.Data.interactionsByType[0].typeCounts && response.Data.interactionsByType[0].typeCounts.length > 0){
                    $scope.totalInteractionsCount = response.Data.interactionsByType[0].totalCount || 0;
                    var existingTypes = [];
                    var allTypes = ['meeting','call','sms','email','facebook','twitter','linkedin'];
                    for(var t=0; t<response.Data.interactionsByType[0].typeCounts.length; t++){
                        var interactionTypeObj = $scope.getInteractionTypeObj(response.Data.interactionsByType[0].typeCounts[t],response.Data.interactionsByType[0].maxCount,100);
                        if(interactionTypeObj != null){
                            existingTypes.push(interactionTypeObj.type);
                            $scope.interactionsByType.push(interactionTypeObj);
                        }
                    }
                    var mobileCountStatus = 0;
                    var mobileCountStatusLists = ['call','sms','email'];
                    for(var u=0; u<allTypes.length; u++){
                        if(existingTypes.indexOf(allTypes[u]) == -1){
                            var newObj = $scope.getInteractionTypeObj({_id:allTypes[u],count:0},response.Data.interactionsByType[0].maxCount,100)
                            if(newObj != null){
                                if(mobileCountStatusLists.indexOf(allTypes[u]) != -1){
                                    mobileCountStatus ++;
                                }
                                $scope.interactionsByType.push(newObj);
                            }
                        }
                    }

                    if(mobileCountStatus > 0){
                        $scope.showDownloadMobileApp = true;
                    }else $scope.showDownloadMobileApp = false;
                    $scope.showSocialSetup = response.Data.showSocialSetup;

                    $scope.interactionsByType.sort(function (o1, o2) {
                        return o1.priority < o2.priority ? -1 : o1.priority > o2.priority ? 1 : 0;
                    });

                    $scope.interactionsByType = {
                        a:$scope.interactionsByType[0],
                        b:$scope.interactionsByType[1],
                        c:$scope.interactionsByType[2],
                        d:$scope.interactionsByType[3],
                        e:$scope.interactionsByType[4],
                        f:$scope.interactionsByType[5],
                        g:$scope.interactionsByType[6]
                    };
                }
            }
            else{

            }
        }).error(function (data) {

        });

    $scope.getInteractionTypeObj = function(obj,total,percentageFor){
        switch (obj._id){
            case 'google-meeting':
            case 'meeting':return {
                priority:0,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'call':return {
                priority:1,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'sms':return {
                priority:2,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'email':return {
                priority:3,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'facebook':return {
                priority:4,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'twitter':return {
                priority:5,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            case 'linkedin':return {
                priority:6,
                value:'height:'+$scope.calculatePercentage(obj.count,total,percentageFor)+'%',
                type:obj._id,
                count:obj.count,
                title:obj.count+' interactions'
            };
                break;

            default : return null;
        }
    };

    $scope.getValidStringNumber = function(number){
        if(typeof number == 'number'){
            return number < 10 ? '0'+number : number
        }
        else return '00';
    };
    $scope.calculatePercentage = function(count,total,percentageFor){
        return Math.round((count*percentageFor)/total);
    };
});

reletasApp.controller("today_contacts_on_social_media", function($scope, $http, todayService){
    $scope.no_top_media_show = false;
    $scope.calculatePercentage = function(count,total,percentageFor){
        return Math.round((count*percentageFor)/total);
    };
    $scope.socialItems = [];
    $http.get('/dashboard/top/social/media/today')
        .success(function(response){
            if(response.SuccessCode){

                var f = response.Data.facebookMaxValue
                var l = response.Data.linkedinMaxValue
                var t = response.Data.twitterMaxValue
                var b = 0;
                if(f > l && f > t){
                    b = f;
                }
                else if(l > f && l > t){
                    b = l;
                }
                else b = t;

                if(response.Data.values.length > 0){
                    for (var i = 0; i < response.Data.values.length; i++) {
                        var li = '';
                        var obj = {name: ''};
                        if (checkRequired(response.Data.values[i].firstName) && response.Data.values[i].firstName.length > 0) {
                            response.Data.values[i].firstName.forEach(function (name) {
                                if (checkRequired(name)) {
                                    obj.name = name
                                }
                            })
                        }
                        if (checkRequired(response.Data.values[i].lastName) && response.Data.values[i].lastName.length > 0) {
                            response.Data.values[i].lastName.forEach(function (name) {
                                if (checkRequired(name)) {
                                    obj.name += ' ' + name
                                }
                            })
                        }
                        if (!checkRequired(obj.name) && response.Data.values[i]._id.firstName) {
                            obj.name = response.Data.values[i]._id.firstName;
                        }
                        if (!checkRequired(obj.name) && response.Data.values[i]._id.emailId) {
                            obj.name = response.Data.values[i]._id.emailId;
                        }
                        else if (!checkRequired(obj.name) && response.Data.values[i]._id.mobileNumber) {
                            obj.name = response.Data.values[i]._id.mobileNumber;
                        }

                        if (checkRequired(obj.name)) {
                            obj.fullName = obj.name;
                            obj.name = getTextLength(obj.name, 8);
                        }
                        if (checkRequired(response.Data.values[i].userId) && response.Data.values[i].userId.length > 0) {
                            response.Data.values[i].userId.forEach(function (id) {
                                if (checkRequired(id)) {
                                    obj.image = '/getImage/' + id;
                                }
                            })
                        }
                        if (!checkRequired(obj.image)) {
                            obj.cursor = 'cursor:default';
                            obj.image = '/images/default.png';
                            obj.noPicFlag = true;
                        } else obj.cursor = 'cursor:pointer';

                        if (checkRequired(response.Data.values[i].publicProfileUrl) && response.Data.values[i].publicProfileUrl.length > 0) {
                            response.Data.values[i].publicProfileUrl.forEach(function (id) {
                                if (checkRequired(id)) {
                                    if (id.charAt(0) != 'h') {
                                        obj.url = '/' + id;
                                    }
                                }
                            })
                        }
                        if (!checkRequired(obj.url)) {
                            obj.url = null;
                        }

                        obj.emailId = response.Data.values[i]._id.emailId;
                        obj.nameNoImg = (obj.name.substr(0,2)).toUpperCase();
                        obj.facebook = response.Data.values[i].facebook;
                        obj.linkedin = response.Data.values[i].linkedin;
                        obj.twitter = response.Data.values[i].twitter;
                        obj.twitterP = 'height:'+$scope.calculatePercentage(response.Data.values[i].twitter,b,100)+'%;';
                        obj.linkedinP = 'height:'+$scope.calculatePercentage(response.Data.values[i].linkedin,b,100)+'%;';
                        obj.facebookP = 'height:'+$scope.calculatePercentage(response.Data.values[i].facebook,b,100)+'%;';
                        obj.linkedinCount = response.Data.values[i].linkedin <= 0 ? '' : response.Data.values[i].linkedin
                        obj.facebookCount = response.Data.values[i].facebook <= 0 ? '' : response.Data.values[i].facebook
                        obj.twitterCount = response.Data.values[i].twitter <= 0 ? '' : response.Data.values[i].twitter
                        $scope.socialItems.push(obj);
                    }
                }
            }
            else{
                $scope.no_top_media_message = response.Message;
                $scope.no_top_media_button_title = 'Setup Favorites'
                $scope.no_top_media_show = true;
            }
        })


    $scope.interactionsContext = function(emailId){
        window.location.replace('/contact/selected?context='+emailId)
    }
});

/*-------------------------------------------------SCROLL-------------------------------------------------------*/

var start = 1;
var itemsScroll = 6;
var itemsToShow = 6;

if(window.innerWidth < 480){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 1
    itemsScroll = 1
}else if(window.innerWidth < 900){
    $(".als-next").css({right:'44px'})
    $(".als-prev").css({left:'43px'})
    itemsToShow = 3
    itemsScroll = 3
}
var end = itemsToShow;

reletasApp.controller("losing_touch_scroll", function ($scope, $http, todayService) {
    $scope.losingTouchConnections = [];
    $scope.losingTouchConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;

   // commonConnectionsMessage(true)
    getCommonConnections($scope, $http, '/contacts/filter/losingtouch/web?skip=0&limit=12', true);

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.losingTouchConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;

            for(var i=0; i<$scope.losingTouchConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    $scope.losingTouchConnections[i].className = ''
                    //addHide($scope.losingTouchConnectionsIds[i],false);
                }
                else{
                    $scope.losingTouchConnections[i].className = 'hide'
                    //addHide($scope.losingTouchConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            var url = '/contacts/filter/losingtouch/web?skip='+$scope.currentLength+'&limit=12';
            getCommonConnections($scope, $http, url, false);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.losingTouchConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.losingTouchConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.losingTouchConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    $scope.losingTouchConnections[i].className = ''
                    //addHide($scope.losingTouchConnectionsIds[i],false);
                }
                else{
                    $scope.losingTouchConnections[i].className = 'hide'
                    //addHide($scope.losingTouchConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev_l",true)
            }
            else addHide("als-prev_l",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next_l",true)
            }
            else addHide("als-next_l",false)
        }
        else{
            addHide("als-prev_l",true)
            addHide("als-next_l",true)
        }
    };

    $scope.interactionsContext = function(emailId){
        window.location.replace('/contact/selected?context='+emailId)
    }

    $scope.connectionClick = function(url){
        if(checkRequired(url)){
            trackMixpanel("clicked on common connection",url,function(){
                window.location.replace(url);
            })
        }
    }
});

function hidePrevNext(idNameP,idNameN,hide){
    if(hide){
        $("#"+idNameP).addClass('hide')
        $("#"+idNameN).addClass('hide')
    }
    else{
        $("#"+idNameP).removeClass('hide')
        $("#"+idNameN).removeClass('hide')
    }
}

function addHide(elementId,addHide){
    addHide ? $("#"+elementId).addClass('hide') : $("#"+elementId).removeClass('hide')
}

function getCommonConnections($scope, $http, url, isHides){
    if(!$scope.grandTotal){
        $scope.grandTotal = 0;
    }

    $scope.no_people_near_you_show = false;
    $http.get(url)
        .success(function (response) {
            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                $scope.timezone = response.Data.timezone;
                if(response.Data.contacts && response.Data.contacts.length > 0){
                    var html = '';
                    hidePrevNext('als-prev_l','als-next_l',true)
                    $scope.currentLength += response.Data.returned;

                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj = {name:''};
                        if(checkRequired(response.Data.contacts[i].firstName) && response.Data.contacts[i].firstName.length > 0){
                            response.Data.contacts[i].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name = name
                                }
                            })
                        }
                        if(checkRequired(response.Data.contacts[i].lastName) && response.Data.contacts[i].lastName.length > 0){
                            response.Data.contacts[i].lastName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name += ' '+name
                                }
                            })
                        }
                        if(!checkRequired(obj.name) && response.Data.contacts[i]._id.emailId){
                            obj.name = response.Data.contacts[i]._id.emailId;
                        }
                        else if(!checkRequired(obj.name) && response.Data.contacts[i]._id.mobileNumber){
                            obj.name = response.Data.contacts[i]._id.mobileNumber;
                        }

                        if(checkRequired(obj.name)){
                            obj.fullName = obj.name;
                            obj.name = getTextLength(obj.name,13);
                        }
                        if(checkRequired(response.Data.contacts[i].userId) && response.Data.contacts[i].userId.length > 0){
                            response.Data.contacts[i].userId.forEach(function(id){
                                if(checkRequired(id)){
                                    obj.image = '/getImage/'+id;
                                }
                            })
                        }
                        if(!checkRequired(obj.image)){
                            obj.cursor = 'cursor:default';
                            obj.image = null;
                            obj.noPicFlag = true;
                        }else obj.cursor = 'cursor:pointer';

                        if(checkRequired(response.Data.contacts[i].publicProfileUrl) && response.Data.contacts[i].publicProfileUrl.length > 0){
                            response.Data.contacts[i].publicProfileUrl.forEach(function(id){
                                if(checkRequired(id)){
                                    if(id.charAt(0) != 'h'){
                                        obj.url = '/'+id;
                                    }
                                }
                            })
                        }
                        if(!checkRequired(obj.url)){
                            obj.url = null;
                        }
                        obj.interactionsCount = response.Data.contacts[i].count || 0;
                        obj.idName = 'losing_touch_item_'+response.Data.contacts[i].lastInteracted;
                        obj.className = 'hide';
                        obj.emailId = response.Data.contacts[i]._id.emailId;

                        obj.nameNoImg = (obj.name.substr(0,2)).toUpperCase();
                        if(obj.interactionsCount != 0){
                            var iDate = moment(response.Data.contacts[i].lastInteracted).tz($scope.timezone);
                            var now = moment().tz($scope.timezone);
                            var diff = now.diff(iDate);
                            diff = moment.duration(diff).asDays();
                            obj.duration = moment.duration(diff,"days").humanize()+' ago';
                        }
                        else{
                            obj.duration = "no interactions"
                        }


                        if(isHides && i<itemsToShow){
                            obj.className = ''
                        }
                        $scope.losingTouchConnections.push(obj);

                        $scope.losingTouchConnectionsIds.push('losing_touch_item_'+response.Data.contacts[i].lastInteracted);

                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext('als-prev_l','als-next_l',true)
                    $scope.no_people_near_you_show = true;
                    $scope.no_people_near_you_message = response.Message;
                }else{
                    $scope.no_people_near_you_show = true;
                    $scope.no_people_near_you_message = response.Message;
                    $scope.isAllConnectionsLoaded = true;
                }
            }
            else if(isHides){
                hidePrevNext('als-prev_l','als-next_l',true)
                $scope.no_people_near_you_show = true;
                $scope.no_people_near_you_message = response.Message;
            }
            else{
                $scope.no_people_near_you_show = true;
                $scope.no_people_near_you_message = response.Message;
                $scope.isAllConnectionsLoaded = true;
            }
        })
}

reletasApp.controller("location_based_connections_scroll", function ($scope, $http, todayService) {
    $scope.locationConnections = [];
    $scope.locationConnectionsIds = [];
    $scope.currentLength = 0;
    $scope.isFirstTime = true;
    $scope.isConnectionOpen = false;
    $scope.start = start;
    $scope.end = end;
    $scope.isAllConnectionsLoaded = false;

    // commonConnectionsMessage(true)
    getCommonConnections_location($scope, $http, '/contacts/filter/user/location?skip=0&limit=12', true);

    $scope.NextConn = function(){
        var statusNext =  $scope.statusNext;
        var statusPrev =  $scope.statusPrev;
        var total = $scope.locationConnectionsIds.length;

        if($scope.end < total){
            var itemsToStayPrev = $scope.start+itemsScroll;
            var itemsToStayNext = $scope.end+itemsScroll;

            for(var i=0; i<$scope.locationConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    $scope.locationConnections[i].className = ''
                    //addHide($scope.losingTouchConnectionsIds[i],false);
                }
                else{
                    $scope.locationConnections[i].className = 'hide'
                    //addHide($scope.losingTouchConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end+itemsScroll;
            $scope.start = $scope.start+itemsScroll;

            $scope.statusNext = statusNext+itemsScroll;
            $scope.statusPrev = statusNext;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
        }
        $scope.showHideArrows();
        if($scope.fetchCommonConnectionsNext() && $scope.isAllConnectionsLoaded == false && $scope.isConnectionOpen == false){
            $scope.isConnectionOpen = true;
            var url = '/contacts/filter/user/location?skip='+$scope.currentLength+'&limit=12';
            getCommonConnections_location($scope, $http, url, false);
        }
    };

    $scope.fetchCommonConnectionsNext = function(){
        if($scope.end >= $scope.locationConnectionsIds.length - 12){
            return true;
        }else return false;
    };

    $scope.PrevConn = function(){

        var statusPrev = $scope.statusPrev;
        var total = $scope.locationConnectionsIds.length;

        if($scope.start >1){
            var itemsToStayPrev = $scope.start-itemsScroll;
            var itemsToStayNext = $scope.end-itemsScroll;

            for(var i=0; i<$scope.locationConnectionsIds.length; i++){

                if(i+1 >= itemsToStayPrev && i+1 <= itemsToStayNext){
                    $scope.locationConnections[i].className = ''
                    //addHide($scope.losingTouchConnectionsIds[i],false);
                }
                else{
                    $scope.locationConnections[i].className = 'hide'
                    //addHide($scope.losingTouchConnectionsIds[i],true);
                }
            }

            $scope.end = $scope.end-itemsScroll;
            $scope.start = $scope.start-itemsScroll;

            $scope.statusNext = statusPrev;
            $scope.statusPrev = statusPrev-itemsScroll+1;

            if($scope.statusNext > total){
                $scope.statusNext = total;
            }
            if($scope.statusNext <= itemsScroll){
                $scope.statusNext = itemsScroll;
            }

            if($scope.statusPrev <= 0){
                $scope.statusPrev = 1;
            }
            $scope.showHideArrows()
        }
    };

    $scope.showHideArrows = function(){
        if($scope.grandTotal >0){
            if($scope.start <=1 ){
                addHide("als-prev_location",true)
            }
            else addHide("als-prev_location",false)

            if($scope.end >= $scope.grandTotal){
                addHide("als-next_location",true)
            }
            else addHide("als-next_location",false)
        }
        else{
            addHide("als-prev_location",true)
            addHide("als-next_location",true)
        }
    };

    $scope.connectionClick = function(url){
        if(checkRequired(url)){
            trackMixpanel("clicked on common connection",url,function(){
                window.location.replace(url);
            })
        }
    }

    $scope.interactionsContext = function(emailId){
        window.location.replace('/contact/selected?context='+emailId)
    }
});

function getCommonConnections_location($scope, $http, url, isHides){
    $scope.no_people_near_you_show = false;
    $scope.grandTotal = 0;
    $http.get(url)
        .success(function (response) {

            $scope.isConnectionOpen = false;
            if(response.SuccessCode){
                $scope.grandTotal = response.Data.total;
                $scope.timezone = response.Data.timezone;
                $scope.location = response.Data.location
                if(response.Data.contacts && response.Data.contacts.length > 0){
                    var html = '';
                    hidePrevNext('als-prev_location','als-next_location',true)
                    $scope.currentLength += response.Data.returned;

                    for(var i=0; i<response.Data.contacts.length; i++){
                        var li = '';
                        var obj = {name:''};
                        if(checkRequired(response.Data.contacts[i].firstName) && response.Data.contacts[i].firstName.length > 0){
                            response.Data.contacts[i].firstName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name = name
                                }
                            })
                        }
                        if(checkRequired(response.Data.contacts[i].lastName) && response.Data.contacts[i].lastName.length > 0){
                            response.Data.contacts[i].lastName.forEach(function(name){
                                if(checkRequired(name)){
                                    obj.name += ' '+name
                                }
                            })
                        }
                        if(!checkRequired(obj.name) && response.Data.contacts[i]._id.emailId){
                            obj.name = response.Data.contacts[i]._id.emailId;
                        }
                        else if(!checkRequired(obj.name) && response.Data.contacts[i]._id.mobileNumber){
                            obj.name = response.Data.contacts[i]._id.mobileNumber;
                        }

                        if(checkRequired(obj.name)){
                            obj.fullName = obj.name;
                            obj.name = getTextLength(obj.name,13);
                        }
                        if(checkRequired(response.Data.contacts[i].userId) && response.Data.contacts[i].userId.length > 0){
                            response.Data.contacts[i].userId.forEach(function(id){
                                if(checkRequired(id)){
                                    obj.image = '/getImage/'+id;

                                }
                            })
                        }
                        if(!checkRequired(obj.image)){
                            obj.cursor = 'cursor:default';
                            obj.image = null;
                            obj.noPicFlag = true;
                        }else obj.cursor = 'cursor:pointer';

                        if(checkRequired(response.Data.contacts[i].publicProfileUrl) && response.Data.contacts[i].publicProfileUrl.length > 0){
                            response.Data.contacts[i].publicProfileUrl.forEach(function(id){
                                if(checkRequired(id)){
                                    if(id.charAt(0) != 'h'){
                                        obj.url = '/'+id;
                                    }
                                }
                            })
                        }
                        if(!checkRequired(obj.url)){
                            obj.url = null;
                        }
                        obj.interactionsCount = response.Data.contacts[i].count || 0;
                        obj.idName = 'losing_touch_item_'+response.Data.contacts[i].lastInteracted;
                        obj.className = 'hide';
                        obj.emailId = response.Data.contacts[i]._id.emailId;
                        obj.nameNoImg = (obj.name.substr(0,2)).toUpperCase();
                        if(obj.interactionsCount != 0){
                            var iDate = moment(response.Data.contacts[i].lastInteracted).tz($scope.timezone);
                            var now = moment().tz($scope.timezone);
                            var diff = now.diff(iDate);
                            diff = moment.duration(diff).asDays();
                            obj.duration = moment.duration(diff,"days").humanize()+' ago';
                        }
                        else obj.duration = "no interactions"

                        if(isHides && i<itemsToShow){
                            obj.className = ''
                        }

                        $scope.locationConnections.push(obj);
                        $scope.locationConnectionsIds.push('location_base_item_'+response.Data.contacts[i].lastInteracted);

                    }
                    if(isHides){
                        $scope.statusPrev = 1;
                        $scope.statusNext = itemsScroll;
                    }
                    $scope.showHideArrows()
                }
                else if(isHides){
                    hidePrevNext('als-prev_location','als-next_location',true)
                    $scope.no_people_near_you_show = true;
                    $scope.no_people_near_you_message = response.Message;
                    $scope.location = response.Data.location
                }
                else{
                    $scope.no_people_near_you_show = true;
                    $scope.no_people_near_you_message = response.Message;
                    $scope.isAllConnectionsLoaded = true;
                    $scope.location = response.Data.location
                }
            }
            else if(isHides){
                hidePrevNext('als-prev_location','als-next_location',true)
                $scope.no_people_near_you_show = true;
                $scope.no_people_near_you_message = response.Message;
                $scope.location = response.Data.location

            }else{
                $scope.no_people_near_you_show = true;
                $scope.no_people_near_you_message = response.Message;
                $scope.isAllConnectionsLoaded = true;
                $scope.location = response.Data.location
            }
        })
}

/*--------------------------------------------------------------------------------------------------------*/

reletasApp.controller("popup_content_cntlr",function($scope, $http, todayService){
    $scope.intItems = [];
    $scope.close_interaction_popup = function(){
        $("#map_cluster_click_popup").hide();
    };
    todayService.updatePopup = function(peopleMet,totalInteractions,interactionItems){
        $("#peopleMet").text(peopleMet);
        $("#totalInteractions").text(totalInteractions);
        $("#interactionItems").html(interactionItems);
        $("#map_cluster_click_popup").show();
    }
});

reletasApp.controller("global_map_all_interactions", function ($scope, $http, todayService) {
    $scope.time = 0;
    $scope.map_cluster_click_popup = false;
    $("#map_cluster_click_popup").hide();
    $scope.peopleMet = 0
    $scope.totalInteractions = 0
    $scope.interactionItems = []

    $scope.showUserLocation = function() {
        $scope.geocoder = new google.maps.Geocoder();
        $scope.myLatlng = new google.maps.LatLng(13.5333, 2.0833);

        $scope.mapOptions = {
            center: $scope.myLatlng,
            zoom: 2
        };

        $scope.map = new google.maps.Map(document.getElementById("map"), $scope.mapOptions);

        $scope.markerCluster = new MarkerClusterer($scope.map, [], {zoomOnClick: false, minimumClusterSize: 1, styles: styles});

        $scope.interactionsCount();
        google.maps.event.addListener($scope.markerCluster, 'clusterclick', function (cluster) {
             $scope.c = cluster.getMarkers();
            if (checkRequired($scope.c[0])) {
                $scope.showGlobalInteractionInfo($scope.c);
            }
        });
        google.maps.event.addListener($scope.map, 'zoom_changed', function () {
            if ($scope.map.getZoom() < 2) $scope.map.setZoom(2);
        });
    };

    $scope.interactionsCount = function() {
        $http.get('/dashboard/interactions/globalmap')
            .success(function(response){
                if(response.SuccessCode){
                    if (response.Data && response.Data.length > 0) {
                        for (var interaction = 0; interaction < response.Data.length; interaction++) {
                            if (checkRequired(response.Data[interaction]._id)) {
                                var result = response.Data[interaction]._id;
                                result.count = response.Data[interaction].count;
                                $scope.showLocations(result);
                            }
                        }
                    }
                }
            })
    };

    $scope.showLocations = function(result) {
        if (checkRequired(result) && result.count !=0) {
            if (checkRequired(result.locationLatLang)) {
                if (checkRequired(result.locationLatLang.latitude) && checkRequired(result.locationLatLang.longitude)) {
                    $scope.createMarkerLocationLatLang(result, result.locationLatLang.latitude, result.locationLatLang.longitude);
                } else if (checkRequired(result.location)) {
                    $scope.createMarkerGeocode(result, 0);
                } else  $scope.createMarkerCurrentLocation(result)
            } else if (checkRequired(result.location)) {
                $scope.createMarkerGeocode(result, 0);
            } else  $scope.createMarkerCurrentLocation(result)
        }
    };

    $scope.createMarkerLocationLatLang = function(result, lat, lang) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lang)),
            map: $scope.map,
            title: "Meeting location: " + result.location || '',
            user: result
        });
        $scope.markerCluster.addMarker(marker);
    };

    $scope.createMarkerCurrentLocation = function(result) {
        if (checkRequired(result.currentLocation)) {
            if (checkRequired(result.currentLocation.latitude)) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(result.currentLocation.latitude, result.currentLocation.longitude),
                    map: $scope.map,
                    title: "Meeting location: " + result.currentLocation.city || '',
                    user: result
                });
                $scope.markerCluster.addMarker(marker)
            }
        }
    };

    $scope.createMarkerGeocode = function(result, limit) {
        $scope.geocoder.geocode({ 'address': result.location}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var newAddress = results[0].geometry.location;
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng())),
                    map: $scope.map,
                    title: "Meeting location: " + result.location || '',
                    user: result
                });
                var latLangObj = {
                    userId: result._id,
                    latitude: newAddress.lat(),
                    longitude: newAddress.lng()
                };
                $scope.markerCluster.addMarker(marker);
                $scope.updateLocationLatLang(latLangObj);

            } else if (status == 'OVER_QUERY_LIMIT') {
                $scope.time += 500;
                limit++;
                if (limit < 3) {
                    setTimeout(function () {
                        $scope.createMarkerGeocode(result, limit);
                    },  $scope.time);
                }
            }
        });
    };

    $scope.updateLocationLatLang = function(latLangObj) {
        $http.post('/updateLocationLatLang',latLangObj)
            .success(function(response){

            });
    };

    $scope.showGlobalInteractionInfo = function(cluster) {

        var infoContent = cluster;
        $scope.interactionItems = [];
        $scope.totalInteractions = 0;
        $scope.peopleMet = cluster.length;
        infoContent.sort(function(a,b)
        {
            if (a.user.count < b.user.count) return 1;
            if (a.user.count > b.user.count) return -1;
            return 0;
        });
            var mmmmm = '';
            for (var m = 0; m < infoContent.length; m++) {
                $scope.totalInteractions += parseInt(infoContent[m].user.count || 0);
                var name = infoContent[m].user.firstName + ' ' + infoContent[m].user.lastName;
                var totalm = infoContent[m].user.count < 10 ? '0' + infoContent[m].user.count : infoContent[m].user.count;
                var userUrl = '/' + $scope.getValidUniqueUrl(infoContent[m].user.publicProfileUrl)
                var summaryUrl = '/interactions/summary/' + infoContent[m].user._id;
                mmmmm += '<div class="map-row clearfix"><span class="pull-left" title=' + name.replace(/\s/g, '&nbsp;') + '><a style="color:#333333" href=' + userUrl + '>' + getTextLength(name, 14) + '</a></span><span class="pull-right fs-14"><a style="color:#333333" href=' + summaryUrl + '>' + totalm + '</a></span></div>'
            }

        todayService.updatePopup($scope.peopleMet,$scope.totalInteractions,mmmmm);
       // $scope.map_cluster_click_popup = true;
    };

    $scope.getValidUniqueUrl = function(name){
        if(name.charAt(0) == 'h'){
            name = name.split('/');
            name = name[name.length - 1];
            return name;
        }
        else return name;
    };

    $scope.showUserLocation();

});

reletasApp.service('profileService', function () {
    return {

    };
});

reletasApp.controller("setup_social_account", function($scope, $http, profileService){

    $http.get('/profile/get/edit')
        .success(function (response) {

            if(response.Data.facebook.id){
                $scope.facebookTrue = true;
            } else {
                $scope.facebookTrue = false;
            }

            if(response.Data.twitter.id){
                $scope.twitterTrue = true;
            } else {
                $scope.twitterTrue = false;
            }

            if(response.Data.linkedin.id){

                $scope.linkedinTrue = true;
            } else {

                $scope.linkedinTrue = false;
            }

            $scope.socialAction = function(accountType){
                    window.location = '/profile/update/add/'+accountType+'/dashboard';
                }

        });

});

function getTextLength(text,maxLength){
    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}

function changeTextById(id,text){
    $("#"+id).html(text);
}

function sortObject(arr) {
    arr.sort(function(a, b) {
        return b.count - a.count ;
    });
    return arr; // returns array
}