/**
 * Created by naveen on 11/7/15.
 */

$(document).ready(function() {


    mixpanelEventTracker(".mInfluencer", "Influencer Button");
    mixpanelEventTracker("#mLogout", "Log Out");

    //Forms Tracker
    // mixpanelFormTracker(".mYourRatingForm", "Your Rating - Forms");

    function mixpanelFormTracker(eventForm, eventName){
        $("body").on("click",eventForm,function(){
            mixpanel.track_forms(eventName);
        });
    }
});

function mixpanelEventTracker(eventBtn, eventName){
    $("body").on("click",eventBtn,function(){
        mixpanel.track(eventName);
    });
}

function mixpanelTracker(eventName){
    mixpanel.track(eventName);
}

function identifyMixPanelUser(profile){
    mixpanel.identify(profile.emailId);
    mixpanel.people.set({
        "$email": profile.emailId,    // only special properties need the $
        "$first_name": profile.firstName,
        "$last_name": profile.lastName,
        "$created": new Date(),
        "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
        "page":window.location.href
    });
}


//mixpanel.init('Project Token', {'loaded':function() {
//    var distinct_id = mixpanel.get_distinct_id()}
//});

function trackMixpanel(m_event,url,cb){
    mixpanel.track(m_event || 'Link_clicked',{link:url,page:window.location.href},cb);
    // mixpanel.people.increment($(this).attr('m_event') || 'Link_clicked',cb);
    setTimeout(cb, 500);
}


//This is old code. These are not tracked.

//$(window).load(function(){
//    $("a").click(function(event) {
//        if($(this).attr('href') != undefined){
//            var cb = generate_callback($(this).attr('href'));
//            event.preventDefault();
//            mixpanel.track($(this).attr('m_event') || 'Link_clicked',{link:$(this).attr('href'),page:window.location.href});
//            mixpanel.people.increment($(this).attr('m_event') || 'Link_clicked',cb);
//
//            setTimeout(cb, 500);
//        }
//    });
//
//    function generate_callback(link) {
//        console.log('clicked link')
//        return function() {
//            window.location.replace(link);
//        }
//    }
//});


//Track user's time on page. We are not using this anywhere.

//function getHiddenProp(){
//    var prefixes = ['webkit','moz','ms','o'];
//
//    // if 'hidden' is natively supported just return it
//    if ('hidden' in document) return 'hidden';
//
//    // otherwise loop over all the known prefixes until we find one
//    for (var i = 0; i < prefixes.length; i++){
//        if ((prefixes[i] + 'Hidden') in document)
//            return prefixes[i] + 'Hidden';
//    }
//
//    // otherwise it's not supported
//    return null;
//}
//
//function isHidden() {
//    var prop = getHiddenProp();
//    if (!prop) return false;
//
//    return document[prop];
//}
//
//// use the property name to generate the prefixed event name
//var visProp = getHiddenProp();
//if (visProp) {
//    var evtname = visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
//    document.addEventListener(evtname, visChange);
//}
//
//function visChange() {
//    var txtFld = document.getElementById('visChangeText');
//
//    mixpanel.time_event("Logs In");
//    mixpanel.time_event("Logs Out");
//    mixpanel.track("Logs Out", {"Gender": "Male", "Age": 21});
//    if (txtFld) {
//        if (isHidden())
//            //txtFld.value += "Tab Hidden!\n";
//        console.log("Hidden");
//        else
//            //txtFld.value += "Tab Visible!\n";
//            console.log("Visible");
//    }
//}