/**
 * Created by naveen on 12/2/15.
 */

//Mixpanel if host or the domain is local/showcase/Staging
var domain = window.location.hostname;

var localDomain = 'localhost';
var showcaseDomain = 'showcase';
var stagingDomain = 'staging';
var exampledevDomain = 'exampledev';
var aporvDomain = 'aporv';

if((domain.indexOf(localDomain) > -1) || (domain.indexOf(showcaseDomain) > -1) || (domain.indexOf(stagingDomain) > -1) || (domain.indexOf(exampledevDomain) > -1) || (domain.indexOf(aporvDomain) > -1)){
    console.log("This is Local");
//Google analytics. ajitmoily@gmail.com is the Admin

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-72504005-1', 'auto');
    ga('send', 'pageview');


}

else {

//Google Analytics

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-51230566-1', 'auto');
    ga('send', 'pageview');

}