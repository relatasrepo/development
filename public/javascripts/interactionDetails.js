
$(document).ready(function() {

    $.ajaxSetup({ cache: false });

    var userProfile;
    var interactionsTable;
    var loggedinUserProfile;
    var publicProfile;
    var windowUrl = ""+window.location;

    var user2Id = windowUrl.split('/')[5];
    applyDataTable();

    getAllInteractionDetails();
    loadLoggedinUserProfile();
    getSenderProfile(user2Id);

    function getAllInteractionDetails(){
        $.ajax({
            url: '/getInteractions/all/summary/'+user2Id,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (interactions) {

                addInteractionsToTable(interactions)
            }
        })
    }

    function addInteractionsToTable(interactions){
        if(interactions.length > 0){
            var rows = []
            for(var i=0;i<interactions.length; i++){
                var details = interactions[i].description || interactions[i].type;
                var row = [
                    getInteractionTypeImage(interactions[i].type,interactions[i].interactionDate,interactions[i].source,interactions[i]),
                    getInteractionType(interactions[i]),
                    interactionTitle(interactions[i])
                ];
                rows.push(row);
            }
            interactionsTable.rows.add( rows ).draw();
        }
    }

    function getInteractionType(interaction){
        var type = '';
        switch(interaction.type){
            case 'meeting': type = 'Meeting'
                break;
            case 'document-share': type = 'Document';
                break;
            case 'email': type = 'Email';
                break;
            case 'google-meeting':type = 'Meeting';
                break;
            default : type = 'Message';
                break;
        }
        return type;
    }


    function getInteractionTypeImage(type,date,source,interaction){
        var interactionDate = new Date(date);
        var img = '';

        switch(type){
            case 'meeting':
                if(source != 'relatas'){
                    img += '&nbsp;&nbsp;<img style="margin-top: -2%;width: 25px" src="/images/interaction/icon_google.png" />';
                }
                else{
                    img += '&nbsp;&nbsp;<img style="height: 25px" src="/images/interaction/icon_rel_cal.png"/>';
                }
                break;
            case 'document-share': img += '&nbsp;&nbsp;<img style="margin-top: -2%" src="/images/interaction/icon_documents.png" />';
                break;
            case 'email': img += '&nbsp;&nbsp;<img style="margin-top: -2%;width: 25px" src="/images/interaction/icon_gmail.png" />';
                break;
            case 'google-meeting': img += '&nbsp;&nbsp;<img style="margin-top: -2%;width: 25px" src="/images/interaction/icon_google.png" />';
                break;
            case 'message': img += '&nbsp;&nbsp;<img style="height: 25px" src="/images/interaction/icon_rel_mssg.png"/>';
                break;
            default :img += '&nbsp;&nbsp;<img style="height: 25px" src="/images/interaction/icon_rel_mssg.png"/>';
        }

        return img+'<span id="sortDate" value='+date+' id="meetingTime">'+getDateStringToShow(interactionDate)+'</span>';
    }

    function interactionTitle(interaction){
        var title = '';
        switch(interaction.type){
            case 'meeting': var meetingUrl = '/meeting/'+interaction.refId;
                var titleIn = validateRequired(interaction.title) ? interaction.title : interaction.type;
                if(interaction.source != 'relatas'){
                    title += getTextLength(titleIn,45);
                }else
                     title += '<a href='+meetingUrl+'>'+getTextLength(titleIn,45)+'</a>';
                break;
            case 'document-share':  var docUrl = '/readDocument/'+interaction.refId;
                title += '<a href='+docUrl+'>'+getTextLength(interaction.title,45)+'</a>';
                break;
            case 'google': title += getTextLength(interaction.title,45);
                break;
            case 'email': title += getTextLength(interaction.title,45);
                break;

            default :var msgUrl = '/messages/all/'+interaction.userId+'?id='+interaction.refId;
                //title += getTextLength(interaction.title,45);
                title += '<a href='+msgUrl+'>'+getTextLength(interaction.title,45)+'</a>';
                break;
        }
        return title;
    }

    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-msg").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-msg").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'9%'})

        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        //setTimeout(function(){
        $("#message").text(message)
        //},1000);
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-msg").popover('destroy');
        if(mFlag){
            window.location.reload();
        }
    });

    /****************************************************************************/

    var title,location;

    var mFlag = false;
    var monthNameSmall = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var day = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];



// Function to load loggedin user profile
    function loadLoggedinUserProfile(){
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (userData) {
                if (userData == null) {}else{
                    loggedinUserProfile = userData;
                    // $('#title').html(userData.firstName);
                    $('.user-msg-name').text(userData.firstName);
                    $('#loggedin-user-name').text(userData.firstName+" "+userData.lastName);
                    $('#loggedin-user-designation').text(userData.designation);
                    $('#loggedin-user-companyName').text(userData.companyName);

                    if( userData.profilePicUrl.charAt(0) == '/' || userData.profilePicUrl.charAt(0) == 'h'){

                        $('#profilePic').attr("src",userData.profilePicUrl);
                    }else $('#profilePic').attr("src","/"+userData.profilePicUrl);
                    imagesLoaded("#profilePic",function(instance,img) {
                        if(instance.hasAnyBroken){
                            $('#profilePic').attr("src","/images/default.png");
                        }
                    });


                }
            },
            error: function (event, request, settings) {
                showMessagePopUp("Error while requesting loggedin user data",'error')
                //alert('Error while requesting loggedin user data')
            },
            timeout: 20000
        });
    }

    // Function to get sender profile information
    /* Here i'm using ajax POST to send sender id (Sender is not logged in) */
    function getSenderProfile(senderId){
        var user = {
            id:senderId
        }
        if(senderId == undefined || senderId == null || senderId == '') {
            showMessagePopUp("Invitation sender is not registered user with Relatas",'error')
            $('#sender-right-side-profilePic').attr('src','/images/default.png');
            $('#relatas-tab').addClass('invisible');
            //alert("Invitation sender is not registered user with Relatas " )
        }
        else{
            $.ajax({
                url:'/getSenderProfile',
                type:'POST',
                datatype:'JSON',
                data:user,
                success:function(profile){
                    if (!profile) {
                        showMessagePopUp("Error in getting invitation sender profile",'error');

                        // alert('Error in getting invitation sender profile');
                    }else{
                        publicProfile = profile;
                        getCommonConnections(publicProfile._id);
                        getLinkedinPofile(publicProfile._id);
                        getTwitterInfo(publicProfile._id);
                        getFacebookInfo(publicProfile._id);
                        $('#relatas-tab').removeClass('invisible');
                        $("#uniqueUrl").attr('href',profile.publicProfileUrl)
                        $("#userMessage").text("Your interactions Summary with "+publicProfile.firstName);

                        if( profile.profilePicUrl.charAt(0) == '/' || profile.profilePicUrl.charAt(0) == 'h'){

                            $('#sender-right-side-profilePic').attr("src",profile.profilePicUrl);
                        } else  $('#sender-right-side-profilePic').attr("src","/"+profile.profilePicUrl);
                        imagesLoaded("#sender-right-side-profilePic",function(instance,img) {
                            if(instance.hasAnyBroken){
                                $('#sender-right-side-profilePic').attr("src","/images/default.png");
                            }
                        });

                        $('#sender-right-side-name').text(profile.firstName+" "+profile.lastName);
                        $('#sender-right-side-designation').text(profile.designation);

                        $('#sender-right-side-companyName').text(profile.companyName);

                        $('#sender-right-side-location').text(getTextLength(profile.location ,25));
                        $('#sender-right-side-location').attr("title",profile.location)
                        $('#sender-right-side-emailId').text(getTextLength(profile.emailId,25));
                        $('#sender-right-side-emailId').attr("title",profile.emailId)
                        $('#sender-right-side-phoneNumber').text(profile.mobileNumber || '');
                        $('#sender-right-side-skypeId').text(getTextLength(profile.skypeId,25));
                        $('#sender-right-side-skypeId').attr("title",profile.skypeId)

                        if(validateRequired(profile.skypeId)){
                            $('#sender-right-side-skypeId').attr("href","skype:"+profile.skypeId+"?call")
                            $('#sender-right-side-skypeId').attr("target","_blank")
                        }
                    }
                },
                error: function (event, request, settings) {
                    showMessagePopUp("Error in getting invitation sender profile",'error');
                },
                timeout: 40000
            })
        }
    }

    function getTextLength(text,maxLength){
        if(!validateRequired(text)){
            return "";
        }
        var textLength = text.length;

        if(textLength != 0){
            if(textLength >maxLength){
                var formattedText = text.slice(0,maxLength)

                return formattedText+'..';
            }
            else return text;
        } else return '';

    }

    var comConnectionsLength;
    var currentStart = 0;
    var currentEnd = 0;
    // Logic to get common connections
    function getCommonConnections(publicUserId){
        var user = {
            id:publicUserId,
            requestFrom:true
        }

        $.ajax({
            url:'/getCommonConnections',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){

                if (result == false) {

                    $('.small-login-container').css({display:'block'});
                    $('.linkedin-social-details').css({display:'none'});
                }
                else if (result.message) {

                    $('.small-login-container').css({display:'none'});
                    $('.social-connections-list').html('<span style="color: white"> &nbsp;&nbsp;'+result.message+'</span>')
                }
                else{

                    $('.small-login-container').css({display:'none'});
                    $('.linkedin-social-details').css({display:'block'});
                    if(validateRequired(result.relationToViewer) && validateRequired(result.relationToViewer.relatedConnections) && validateRequired(result.relationToViewer.relatedConnections.values) && result.relationToViewer.relatedConnections.values.length > 0){

                        var connections = result.relationToViewer.relatedConnections.values;
                        comConnectionsLength = connections.length >= 9 ? 9 : connections.length ;
                        if(connections.length > 2){
                            $("#connection-next").show()
                            //$("#connection-prev").show()
                        }

                        for(var i=0; i<connections.length; i++){
                            var linkedinUrl = connections[i].siteStandardProfileRequest.url;
                            var firstName = connections[i].firstName || '';
                            var lastName = connections[i].lastName || '';
                            var title = firstName+' '+lastName;
                            var iUrl = connections[i].pictureUrl || '/images/default.png';
                            var commonConnetionHtml ='';
                            var id = 'connection-'+i;
                            if(i > 2)
                                commonConnetionHtml = ' <td class="social-connection-item" style="display: none" id='+id+'>';
                            else
                                commonConnetionHtml = ' <td class="social-connection-item" id='+id+'>';

                            commonConnetionHtml += '<a target="_blank" href='+linkedinUrl+'><div class="profile-picture-small social-connection-profile-picture">';
                            //commonConnetionHtml += '<img class="profile-picture-border" src="/images/white_circular_border_small.png" title='+title.replace(/\s/g, '&nbsp;')+'>';
                            commonConnetionHtml += '<img class="profile-pic-img-small" style="border: 2px solid white;" id="lImage1" src='+iUrl+' title='+title.replace(/\s/g, '&nbsp;')+'><br></div></a>';
                            commonConnetionHtml += '</td>';
                            if(i < 9){
                                // $("#connection-next").attr('next',i+1);
                                $('.social-connections-list').append(commonConnetionHtml);
                            }
                            if(i  <= 2)
                                currentEnd = i;
                        }
                    }else{
                        $("#connection-next").hide()
                        $("#connection-prev").hide()
                        $('.small-login-container').css({display:'none'});
                        $('.social-connections-list').html('<span style="color: white"> &nbsp;&nbsp;No common connections found</span>')
                    }

                    if(validateRequired(result.currentShare)){
                        var statusContent = '';
                        if(validateRequired(result.currentShare.content)){
                            statusContent = result.currentShare.content.description || result.currentShare.comment || '';

                            if(!validateRequired(result.currentShare.content.description)){
                                if(validateRequired(result.currentShare.attribution) && validateRequired(result.currentShare.attribution.share)){
                                    statusContent = result.currentShare.attribution.share.comment || '';

                                }else statusContent = result.currentShare.comment;
                            }
                        }else if(validateRequired(result.currentShare.attribution) && validateRequired(result.currentShare.attribution.share)){
                            statusContent = result.currentShare.attribution.share.comment || '';

                        }else{
                            statusContent = result.currentShare.comment || '';
                        }
                        if(validateRequired(statusContent)){
                            var linkedinStatusUpdaeHtml = '<div class="social-post-title">';

                            linkedinStatusUpdaeHtml += '<span>'+statusContent+'</span></div>';
                            $('#latestPost').html(linkedinStatusUpdaeHtml);
                        }
                    }

                    if(validateRequired(result.positions) && validateRequired(result.positions.values) && result.positions.values.length > 0){
                        var positions = result.positions.values;
                        var linkedinCompaniesHtml = '';
                        linkedinCompaniesHtml +='';
                        for(var p=0;p<positions.length;p++){
                            if(p<=2){
                                linkedinCompaniesHtml +='<span style="color: white">'+positions[p].company.name+'</span></br>';
                            }
                        }
                        $('.social-latest-positions').html(linkedinCompaniesHtml);
                    }
                }
            },
            error: function (event, request, settings) {

            }
        })
    };

    $("#connection-next").on('click',function(){
        if(currentEnd < comConnectionsLength-1){
            var nowStart = currentEnd+1;
            var nowEnd = (nowStart+2) >= comConnectionsLength ? comConnectionsLength : nowStart+2;
            displayPrevNext(nowStart,nowEnd)
        }
    });
    $("#connection-prev").on('click',function(){
        if(currentStart > 0){
            var nowStart = currentStart-3 <= 0 ? 0 : currentStart-3;
            var nowEnd = (nowStart+2) >= comConnectionsLength ? comConnectionsLength : nowStart+2;
            displayPrevNext(nowStart,nowEnd)
        }
    });
    function displayPrevNext(nowStart,nowEnd){
        for(var c=0; c<comConnectionsLength; c++){
            if(c < nowStart){
                $("#connection-"+c).hide();
            }else if(c >= nowStart && c<=nowEnd){
                $("#connection-"+c).show();
            }else $("#connection-"+c).hide();
        }

        if(nowStart == 0)
            $("#connection-prev").hide()
        else  $("#connection-prev").show()

        if(nowEnd == comConnectionsLength ||  nowEnd == comConnectionsLength-1)
            $("#connection-next").hide()
        else  $("#connection-next").show()

        currentStart = nowStart;
        currentEnd = nowEnd;
    }
// Logic to get twitter info
    function getTwitterInfo(publicUserId){
        var user = {
            id:publicUserId,
            requestFrom:true
        }
        $.ajax({
            url:'/getTwitterInfo',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){
                if (result == false) {
                    $('.small-login-container').css({display:'block'});

                }
                else if (result.message) {
                    $('#twitter-msg').html('<span style="color: white"> &nbsp;&nbsp;No tweets found</span>');


                }
                else{
                    for(var i=0;i<result.length;i++){
                        if (i<5) {
                            $('#twitter-msg').append('<span style="color: white">'+result[i].text+'</span></br><hr></br>');
                        }
                    }
                }
            },
            error: function (event, request, settings) {

            }

        })
    };

// Logic to get facebook info
    function getFacebookInfo(publicUserId){
        var user = {
            id:publicUserId,
            requestFrom:true
        }
        $.ajax({
            url:'/getFacebookInfo',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){

                if (result == false) {

                    $('.small-login-container').css({display:'block'});
                }
                else if (result.message) {

                    $('.small-login-container').css({display:'none'});
                    $('#facebook-msg').html('<span style="color: white"> &nbsp;&nbsp;No common facebook friends found.</span>');
                }
                else{

                    for(var i=0;i<result.length;i++){

                        $('#facebook-msg').append('<span style="color: white">'+result[i].name+'</span></br><hr></br>');

                    }
                    getFacebookStatus(user);
                }
            },
            error: function (event, request, settings) {

            }

        })
    };

    // Function to get linkedin profile of public user
    function getLinkedinPofile(publicUserId){
        var user = {
            id:publicUserId
        }
        $.ajax({
            url:'/linkedinPro',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){

                if (result == false) {

                }
                else if (result.message) {

                }
                else{
                    var linkedinSchoolHtml ='';
                    var schools = result.educations.values;
                    if(validateRequired(schools)){
                        if(validateRequired(schools[0])){
                            schools.forEach(function(school){
                                linkedinSchoolHtml += '<span style="color: white">'+school.schoolName+'</span></br>';
                            })
                        }
                        $('.social-latest-school').html(linkedinSchoolHtml);
                    }
                }
            },
            error: function (event, request, settings) {

            }
        })
    };

    // function to get facebook status
    function getFacebookStatus(user){
        $.ajax({
            url:'/facebookStatus',
            type:'POST',
            datatype:'JSON',
            data:user,
            traditional:true,
            success:function(result){
                if (!result) {
                    // alert('loginRequired')
                }
                else if (result.message) {
                    //  $('.facebook-social-details').html('<span>Unable to fetch facebook status</span>');
                    //alert(message.message)
                }
                else {

                    // handling facebook status

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    };


    function getDayString(day){
        if (day == 1 || day == 21 || day == 31) {
            return 'st';
        }
        else if (day == 2 || day == 22 ) {
            return 'nd';
        }
        else if (day == 3 || day == 23) {
            return 'rd';
        }
        else return 'th';
    }

    function getDateStringToShow(date){
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hours = date.getHours();
        var min = date.getMinutes();

        var dayString = getDayString(d);
        var data = monthNameSmall[date.getMonth()]+' '+date.getDate()+' '+date.getFullYear();
        return data;
    }

// Function to validate one variable is empty or not

    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }


    function sidebarHeight2(){
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height':sideHeight})
    }

    sidebarHeight2()
    sidebarHeight()
    function sidebarHeight(){
        var sideHeight = $(document).height()
        $("#fb-tab-content").css({'min-height':sideHeight-270})
        $("#twitter-tab-content").css({'min-height':sideHeight-270})
        $("#linkedin-tab-content").css({'min-height':sideHeight-270})
        $("#relatas-tab-content").css({'min-height':sideHeight-270})

    }

    function applyDataTable(){
        interactionsTable = $('#interactionsTable').DataTable({

            "dom": '<"top"iflp<"clear">>',
            "order": [[ 0, "desc" ]],
            "columns": [
                { "orderDataType": "dom-value" },
                null,
                null
            ],
            "oLanguage": {
                "sEmptyTable": "No Interactions"
            }

        });
    }
    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {

            return $('#sortDate', td).attr("value");
        } );
    }
});