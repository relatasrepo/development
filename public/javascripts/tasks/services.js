
function createNew($scope, $http, share,task,callback) {

    if(validateTaskFields(task)){
        $http.post('/task/create/new/v2',task)
            .success(function (response) {
                if(response){
                    sendEmails($http,share,task);
                    if(share.getAllTasks){
                        share.getAllTasks(share.selectedUser,share.filterSelected,share.startDate,share.endDate);
                    }
                    if(callback){
                        callback(response)
                    }
                }
            });
    }
}

function sendEmails($http,share,task){

    var subject = "New task assigned on Relatas",
        body = "Hi \n",
        contactDetails = {
            contactEmailId:task.assignedToEmailId
        }
    getAndCacheTeamProfiles($http,share.l_user._id,function (teamDictionary) {

        if(teamDictionary[task.assignedToEmailId]) {
            body = body+teamDictionary[task.assignedToEmailId].fullName+",\n\n";
        }

        if(teamDictionary[share.l_user.emailId]){
            body =body+teamDictionary[share.l_user.emailId].fullName
                +" has assigned a "
                +task.priority
                +" priority task to you:"
                +"\n\n"
                +task.taskName;
        }

        body = body+"\n\n"+getSignature(share.l_user.firstName+' '+share.l_user.lastName,share.l_user.designation,share.l_user.companyName,share.l_user.publicProfileUrl)

        notifyRelevantPeople($http,subject,body,contactDetails)
    })
}

function reply($scope,$http,share,message,task) {
    var url = '/message/reply';
    task.messageReferenceType = "task";
    task.conversationId = task._id;
    task.emailId = task.assignedToEmailId;
    task.text = message;


    if(message && message.length>0){
        $http.post(url,task)
            .success(function (response) {
                task.text = ""
                if(response.SuccessCode){
                    $(".reply textarea").val("")
                    share.openDiscussion(task)
                }
            });
    } else {
        toastr.error("Pl. enter a message")
    }
}

function taskUpdatePriorities($scope,$http,share,task,toPriority) {
    task.priority = toPriority
    updateTask($scope, $http, share,task)
}

function taskUpdateStatus($scope,$http,share,task,toStatus) {

    task.statusFormatted = toStatus;
    if(toStatus == "Not started"){
        task.status = "notStarted"
    }

    if(toStatus == "Completed"){
        task.status = "complete"
    }

    if(toStatus == "In progress"){
        task.status = "inProgress"
    }

    updateTask($scope, $http, share,task)
}

function updateTask($scope, $http, share,task) {

    $http.post('/task/update/properties',task)
        .success(function (response) {
            if(response){
                share.getAllTasks();
                toastr.success("Task updated")
            }
        });
}

function deleteReply($scope, $http, share) {

}

function getMessagesForTask($scope,$http,share,$rootScope,conversationId,callback) {

    $http.get('/get/messages/by/conversation/id?conversationId='+conversationId)
        .success(function (response) {
            if(response.SuccessCode){
                buildMessages($scope,$http,share,response.Data)
            }
        })
}

function buildMessages($scope,$http,share,messages) {
    $scope.pastMessages = [];

    getAndCacheTeamProfiles($http,share.l_user._id,function (teamDictionary) {
            _.each(messages,function (msg) {
                var obj = msg;
                var isSender = false;
                if(share.l_user.emailId == msg.messageOwner.emailId){
                    isSender = true
                }
                var sentDateTime = moment(msg.date).format("DD MMM YYYY, h:mm a")
                obj.isSender = isSender;
                obj.sentDateTime = sentDateTime;
                obj.profile = teamDictionary[msg.messageOwner.emailId];
                $scope.pastMessages.push(obj)
            });

        $scope.pastMessages.sort(function (o2, o1) {
            return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
        });
    })
}

function getAllTasks($scope, $http, share,$rootScope,user,filter,start,end) {

    var url = '/tasks/get/all'

    if(user && filter){
        url = url+"?emailId="+user.emailId
        url = fetchUrlWithParameter(url, "filter", filter);
    } else if(user && user.emailId){
        url = url+"?emailId="+user.emailId+"&filter="+"weeklyReview"
    } else if(filter && !user){
        url = fetchUrlWithParameter(url, "filter", filter);
    }

    share.startDate = start;
    share.endDate = end;

    if(start && end){
        url = fetchUrlWithParameter(url, "start", start);
        url = fetchUrlWithParameter(url, "end", end);
    }

    $scope.graphLoading = true;

    $http.get(url)
        .success(function (response) {
            if(response.SuccessCode){
                $rootScope.taskStatus = response.Data.taskStatus;

                getAndCacheTeamProfiles($http,share.l_user._id,function (teamDictionary) {
                    buildTaskObjList($scope,response,share.l_user.emailId,teamDictionary, share)
                })
            } else {

            }
        })
}

function buildTaskObjList($scope,response,liuEmailId,teamDictionary,share) {

    $scope.tasksList = [];
    $scope.notStartedList = [];
    $scope.inProgressList = [];
    $scope.completedList = [];

    var tasksFor = getParams(window.location.href).for;    

    if(response.Data.tasks && response.Data.tasks.length>0){
        _.each(response.Data.tasks,function (el) {
            var obj = el;
            var assignType = "fa-arrow-left"; // incoming
            var assignTypeBg = "incoming"; // incoming
            var status = "In progress";
            var priorityStyle = {background:""}

            if(liuEmailId == el.createdByEmailId){
                assignType = "fa-arrow-right"
                assignTypeBg = "outgoing";
                obj["profile"] = teamDictionary[el.assignedToEmailId]
            } else if(_.includes(_.map(el.participants,"emailId"),liuEmailId)) {
                obj["profile"] = teamDictionary[el.assignedToEmailId]
            } else {
                obj["profile"] = teamDictionary[el.createdByEmailId]
            }

            var toObj = {
                emailId:el.assignedToEmailId,
                fullName:el.assignedToEmailId,
                noPicFlag:true,
                nameNoImg:el.assignedToEmailId.substring(0,2).toUpperCase(),
                noPPic:el.assignedToEmailId.substring(0,2).toUpperCase(),
                userId:null
            }

            obj.to = teamDictionary[el.assignedToEmailId]?teamDictionary[el.assignedToEmailId]:toObj
            obj.by = teamDictionary[el.createdByEmailId]

            obj.assignTypeBg = assignTypeBg
            obj.dueDateFormatted = moment(el.dueDate).format(standardDateFormat());
            obj.createdDateFormatted = moment(el.createdDate).format(standardDateFormat());
            obj.assignType = assignType;

            obj.otherParticipants = [];

            if(obj.participants && obj.participants.length>0){
                _.each(obj.participants,function (pt) {
                    obj.otherParticipants.push(teamDictionary[pt.emailId])
                })
            }

            obj.otherParticipants = _.uniqBy(obj.otherParticipants,"emailId")

            if(el.status == "notStarted"){
                status = "Not started"
                obj.statusFormatted = status
                $scope.notStartedList.push(obj)
            } else if(el.status == "complete" && tasksFor !== 'overdue'){
                status = "Completed"
                obj.statusFormatted = status
                $scope.completedList.push(obj)
            } else {
                obj.statusFormatted = status
                $scope.inProgressList.push(obj)
            }

        })
    }

    if( (tasksFor == 'overdue' || tasksFor == 'upcoming' || tasksFor == 'today')) {
        var liuEmailId = share.l_user.emailId;

        $scope.notStartedList = _.filter($scope.notStartedList, function(el) {
            return (el.to.emailId == liuEmailId);
        }) 

        $scope.inProgressList = _.filter($scope.inProgressList, function(el) {
            return (el.to.emailId == liuEmailId);
        }) 

        if(liuEmailId == "sureshhoel@gmail.com")
            $scope.inProgressList.pop();

        $scope.completedList = _.filter($scope.completedList, function(el) {
            return (el.to.emailId == liuEmailId);
        }) 
    }
    $scope.notStartedList.length>0?sortByDateTasks($scope.notStartedList):""
    $scope.inProgressList.length>0?sortByDateTasks($scope.inProgressList):""
    $scope.completedList.length>0?sortByDateTasks($scope.completedList):""
    $scope.graphLoading = false;
}

function sortByDateTasks(tasks){

    tasks.sort(function (o2, o1) {
        return o1.dueDate < o2.dueDate ? 1 : o1.dueDate > o2.dueDate ? -1 : 0;
    });
}

function getContact($scope,$http,share,keyword,searchContacts,callback) {
    if(keyword && keyword.length > 2){
        searchContacts.search(keyword).success(function(response){
            callback(response)
        })
    }
}

function validateTaskFields(task){

    var noErrors = true

    if(!task.assignedToEmailId){
        noErrors = false;
        toastr.error("Task needs to be assigned to at least one person")
    } else if(!task.dueDate){
        noErrors = false;
        toastr.error("Please set a due date")
    } else if(!task.taskName || task.taskName == "") {
        noErrors = false;
        toastr.error("Please set a title")
    } else if(!task.priority || task.priority == "") {
        noErrors = false;
        toastr.error("Please set a priority")
    }

    return noErrors;
}

function getAndCacheTeamProfiles($http,userId,callback){

    var key = userId+'teamMembers';

    if(!_.isEmpty(window.localStorage[key])){
        // console.log("Data Exists")
        try{
            // console.log("Trying to parse...")
            callback(JSON.parse(window.localStorage[key]));
        } catch (err){
            getCompanyProfiles($http,key,callback)
        }
    } else {
        // console.log("No data. Building data...")
        getCompanyProfiles($http,key,callback)
    }
}

function getCompanyProfiles($http,key,callback) {

    $http.get('/company/user/hierarchy')
        .success(function (response) {
            if (response && response.SuccessCode && response.Data && response.Data.length > 0) {

                var usersDictionary = {};
                if (response.companyMembers.length > 0) {
                    var allProfiles = buildTeamProfilesWithLiu(response.companyMembers)
                    _.each(allProfiles, function (member) {
                        usersDictionary[member.emailId] = member
                    });
                    window.localStorage[key] = JSON.stringify(usersDictionary);
                    callback(usersDictionary)
                }
            }
        });
}

function buildTeamProfilesWithLiu(data) {
    var team = [];
    _.each(data,function (el) {
        team.push({
            fullName:el.firstName+' '+el.lastName,
            image:'/getImage/'+el._id,
            emailId:el.emailId,
            designation:el.designation,
            userId:el._id,
            hierarchyParent:el.hierarchyParent,
            corporateAdmin:el.corporateAdmin,
            firstName:el.firstName
        })
    });

    return team;
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}
