relatasApp.controller("taskMenu", function ($scope, $http, share, $rootScope) {

    closeAllDropDownsAndModals($scope,".task-filter")
    closeAllDropDownsAndModals($scope,".drop-down-list")
    // closeAllDropDownsAndModals($scope,".drop-down-list-2")

    $scope.filters = [{
        name:"All tasks",
        value:"allTasks"
    },{
        name:"Weekly sales call tasks",
        value:"weeklyReview"
    },{
        name:"Mobile tasks",
        value:"mobile"
    }]

    $scope.defaultMenu = "All tasks"
    if(/commit/.test(window.location.pathname)){
        $scope.defaultMenu = "Weekly sales call tasks"
    }

    $scope.createNew = function () {
        mixpanelTracker("Tasks>create new ");
        share.createNew()
        $rootScope.createFormIsOpen = true
    };

    share.unhideCreateTaskBtn = function () {
        $scope.createFormIsOpen = false;
    }

    $scope.displayFilter = function () {
        $scope.isDisplayFilter = !$scope.isDisplayFilter;
    }

    $scope.selectFilter = function (selection) {
        share.filterSelected = selection;
        $scope.defaultMenu = selection.name
        share.getAllTasks(share.selectedUser,selection.value,share.startDate,share.endDate)
    }

    $scope.tasksByDateRange = function(start,end) {
        $scope.displayDatePicker = false;
        if(start && end && start != "" && end != ""){
            $scope.dateRange = $scope.startFormatted +" - "+$scope.endFormatted
        }
        share.getAllTasks(share.selectedUser,share.filterSelected && share.filterSelected.value?share.filterSelected.value:"dueDate",start,end)
    }

    share.getTasksByDateFilter = function(start, end) {
        $scope.startFormatted = moment(start).format("DD MMM YYYY");
        $scope.endFormatted = moment(end).format("DD MMM YYYY");

        if(start && end && start != "" && end != ""){
            $scope.dateRange = $scope.startFormatted +" - "+$scope.endFormatted
        }
        share.getAllTasks(share.selectedUser,"dueDate",start,end)
    }

    $scope.dateRange = "Filter by due date";

    $scope.selectDateRange = function () {
        $scope.displayDatePicker = true;
    }

    $scope.pickDueDate = function () {

        pickDateTime($scope,null,null,"#start",false,"start",false,function (timeSelected) {

        })
        pickDateTime($scope,null,null,"#end",false,"end",false,function (timeSelected) {
        })
    }

    $scope.clearDateRange = function () {
        $scope.dateRange = "Filter by due date";
        $scope.start = null;
        $scope.end = null;
        $scope.endFormatted = "";
        $scope.startFormatted = "";
        $scope.displayDatePicker = false;
    }
})

relatasApp.controller("tasksList", function ($scope, $http, share, $rootScope) {
    $scope.headers = ["Assigned to","Task","Assigned on","Due date","Status","Category"]
    share.getAllTasks = function (user,filter,start,end) {
        share.selectedUser = user
        share.assignListPrePopulate(user);
        getAllTasks($scope, $http, share,$rootScope,user,filter,start,end)
    }

    $scope.openDiscussion = function (task) {
        share.openDiscussion(task)

        mixpanelTracker("Tasks>open task ");
        if((/commit/.test(window.location.pathname))){
            share.setCssForWeeklyReview()
        }
    }
})

relatasApp.controller("taskDiscussion", function ($scope, $http, share, $rootScope) {

    closeAllDropDownsAndModals($scope,".drop-down-list")
    closeAllDropDownsAndModals($scope,".close-div")

    $scope.moreSettingsOpen = function () {
        mixpanelTracker("Tasks>more settings ");
        $scope.displayMoreOptions = true;
    }

    $scope.closeMoreOptions = function () {
        $scope.displayMoreOptions = false;
    }

    $scope.pickDueDate = function () {
        pickDateTime($scope,new Date(),null,"#changeDueDate",false,"dueDate",false,function (timeSelected) {
            $scope.task.dueDate = new Date(timeSelected)
            $scope.task.dueDateFormatted = moment(timeSelected).format(standardDateFormat())
            updateTask($scope, $http, share,$scope.task)
        })
    }

    share.openDiscussion = function (task) {
        $scope.taskName = "";
        $("div[id^='taTextElement']").empty();
        $scope.task = task;
        $scope.ifTaskDiscussion = true;
        $scope.taskName = $scope.task.taskName
        $("div[id^='taTextElement']").append($scope.taskName)
        getMessagesForTask($scope,$http,share,$rootScope,task._id)
    }

    $scope.selectNewPriority = function (task,toPriority) {
        $scope.openPriorityList = !$scope.openPriorityList;
        taskUpdatePriorities($scope,$http,share,task,toPriority)
    }

    $scope.selectNewStatus = function (task,toStatus) {
        $scope.openNewStatusList = false;
        taskUpdateStatus($scope,$http,share,task,toStatus)
    }

    $scope.changePriority = function () {
        $scope.openPriorityList = true;
    }

    $scope.changeStatus = function () {
        mixpanelTracker("Tasks>change status ");
        $scope.openNewStatusList = true;
    }

    $scope.taskPrioritiesList = ["low","medium","high"];

    $scope.closeDiscussion = function () {
        $scope.ifTaskDiscussion = false;

        if((/commit/.test(window.location.pathname))){
            share.removeCssForWeeklyReview()
        }
    }

    $scope.closeTaskName = function () {
        $scope.showTaskNameEditor = false;
    }

    $scope.sendMessage = function (message,task) {
        mixpanelTracker("Tasks>send message for task ");
        reply($scope,$http,share,message,task)
    }

    $scope.editTaskName = function () {
        mixpanelTracker("Tasks>edit task title");
        $scope.showTaskNameEditor = true;
    }

    $scope.saveTaskName = function (taskName) {
        $scope.showTaskNameEditor = false;
        $scope.task.taskName = taskName;
        updateTask($scope, $http, share,$scope.task)
    }

})

relatasApp.controller("taskCreateNew", function ($scope, $http, share, $rootScope,searchContacts) {

    share.taskFor = "others";

    if((/commit/.test(window.location.pathname))){
        share.taskFor = "weeklyReview"
    }
    
    $rootScope.toolBarList = [
        ['h1','h2','h3'],
        ['bold','italics'],
        ['ul','ol'],
        ['indent','outdent'],
        // ['justifyLeft','justifyCenter','justifyRight','justifyFull']
    ]
    $scope.toList = []; // Only if multiple
    $scope.addRecipient = function (contact) {
        $scope.keyword = "";
        $scope.contactList = [];
        $scope.assigneeBucket = [];
        $scope.toList.push(contact);
        $(".addPeople").val("")
    };

    $scope.selectAssignee = function (contact) {
        $scope.assignList = [];
        $scope.keyword = "";
        $scope.contactList = [];
        $scope.assigneeBucket = [];
        $scope.assignList.push(contact);
        $(".addPeople").val("")
    };

    $scope.startDateFormatted = moment().format("DD MMM YYYY")
    $scope.dueDateFormatted = "Select"

    $scope.removeFromToList = function (contact) {
        $scope.toList = $scope.toList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }
    
    $scope.closeTaskCreator = function () {
        $scope.createNewTask = false
        $rootScope.createFormIsOpen = false
    }

    $scope.removeFromAssigneeList = function (contact) {
        $scope.assignList = $scope.assignList.filter(function (item) {
            return item.emailId != contact.emailId;
        })
    }

    closeAllDropDownsAndModals($scope,".search-box-wrapper")

    $scope.pickStartDate = function () {
        pickDateTime($scope,new Date(),null,"#startDate",false,"startDate",false)
    }

    $scope.pickDueDate = function () {
        pickDateTime($scope,new Date(),null,"#dueDate",false,"dueDate",false)
    }

    $scope.searchContacts = function (keyword,onlyOne) {
        getContact($scope,$http,share,keyword,searchContacts,function (response) {
            var bucket = "contactList";
            if(onlyOne){
                bucket = "assigneeBucket"
            }
            
            populateList($scope,response,bucket);
        })
    }

    $scope.task = {
        taskName:"",
        description:"",
        priority:null,
        startDate:$scope.startDate,
        dueDate:$scope.dueDate,
        toList:$scope.assignList,
        participants:$scope.toList,
        taskFor:"other"
    }

    $scope.saveTask = function () {
        mixpanelTracker("Tasks>save task ");
        $scope.task.startDate = $scope.startDate
        $scope.task.dueDate = $scope.dueDate
        $scope.task.assignedToEmailId = $scope.assignList && $scope.assignList[0]?$scope.assignList[0].emailId:null;
        $scope.task.participants = $scope.toList;
        $scope.task.taskFor = (/commit/.test(window.location.pathname))?"weeklyReview":"other";

        if((/opportunities/.test(window.location.pathname))){
            $scope.task.taskFor = "opportunities"
        }

        share.unhideCreateTaskBtn()

        createNew($scope,$http,share,$scope.task,function () {
            $scope.createNewTask = false;
            $scope.task = {
                taskName:"",
                description:"",
                priority:null,
                startDate:"",
                dueDate:"",
                toList:[],
                participants:[],
                taskFor:(/commit/.test(window.location.pathname))?"weeklyReview":"other"
            }
        })
    }

    share.assignListPrePopulate = function(user){
        $scope.assignList = [];
        if(user){
            $scope.assignList.push(user)
        }

    }

    share.createNew = function () {
        $("div[id^='taTextElement']").empty();
        $scope.createNewTask = true;
    }
    
});

function populateList($scope,response,bucket) {
    if(response.SuccessCode && response.Data.length>0){

        var contactsArray = response.Data.filter(function (res) {
            return res.personEmailId != null
        });

        $scope[bucket] = [];

        if(contactsArray.length>0){
            for(var i=0;i<contactsArray.length;i++){

                var obj = {};

                if(checkRequired(contactsArray[i].personId) && checkRequired(contactsArray[i].personName)){

                    var name = getTextLength(contactsArray[i].personName,20);
                    var image = '/getImage/'+contactsArray[i].personId._id;

                    obj = {
                        fullName:contactsArray[i].personName,
                        name:name,
                        image:image
                    };

                    obj.emailId = contactsArray[i].personEmailId;
                    obj.twitterUserName = contactsArray[i].twitterUserName;
                    obj.mobileNumber = contactsArray[i].mobileNumber;

                }
                else {
                    var contactImageLink = contactsArray[i].contactImageLink ? encodeURIComponent(contactsArray[i].contactImageLink) : null
                    obj = {
                        fullName: contactsArray[i].personName,
                        name: getTextLength(contactsArray[i].personName, 20),
                        image: '/getContactImage/' + contactsArray[i].personEmailId + '/' + contactImageLink,
                        emailId:contactsArray[i].personEmailId,
                        twitterUserName: contactsArray[i].twitterUserName,
                        mobileNumber: contactsArray[i].mobileNumber
                        // noPicFlag:true
                    };
                }

                if(obj.twitterUserName){
                    obj.tweetAccExists = true;
                }

                obj.watchThisContact = contactsArray[i].watchThisContact?contactsArray[i].watchThisContact:false;
                obj._id = contactsArray[i]._id;
                obj.personId = contactsArray[i].personId && contactsArray[i].personId._id?contactsArray[i].personId._id:null;

                $scope[bucket].push(obj)

            }
        }
    }
}