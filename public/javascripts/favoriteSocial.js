$(document).ready(function(){
    var userIdArr = []
    getFavoriteContacts(0,7,true);
    function getFavoriteContacts(skip,limit,getFeeds){
        $.ajax({
            url:'/contacts/filter/favorite/web?limit='+limit+'&skip='+skip,
            type:'GET',
            datatype:'JSON',
            success:function(response){

               if(response.Data.total > 0){
                   $("#total-contacts").text("Your favorite list("+response.Data.total+" People)")
                   $("#nav-prev").attr("data-total",response.Data.total)
                   $("#nav-prev").attr("data-returned",response.Data.returned)
                   $("#nav-prev").attr("data-skipped",response.Data.skipped)

                   $("#nav-next").attr("data-total",response.Data.total)
                   $("#nav-next").attr("data-returned",response.Data.returned)
                   $("#nav-next").attr("data-skipped",response.Data.skipped)
                   userIdArr = response.Data.userIdArr;
                   displayF_Contacts(response.Data.contacts)
                   if(getFeeds){
                      //userIdArr = userIdArr.splice(0,3);
                      getUserSocialFeeds()
                   }
               }
            }
        })
    }

    function displayF_Contacts(contacts){
        $('.fv-contact').html('')
        for(var c=0; c<contacts.length;c++){

            var html = '<div class="fv-contact-inner">';
            html += '<div class="fv-contact-inner-top">';
            if(contacts[c].personId == '' || contacts[c].personId == null){
                html += '<img src="/images/default.png">'
            }
            else{
                var img = '/getImage/'+contacts[c].personId._id
                html += '<img src='+img+'>';
            }
            html += '</div>';
            html +='<div class="fv-contact-inner-bottom">';
            html += '<span>'+contacts[c].personName+'</span>';
            html += '</div>'
            html += '</div>'

            $('.fv-contact').append(html)
        }
    }

    $("#nav-prev").on('click',function(){
        var skip = $(this).attr("data-skipped")
        skip = parseInt(skip)
        if(skip > 0){
            getFavoriteContacts(skip-7,7);
        }
    });

    $("#nav-next").on('click',function(){
        var skip = $(this).attr("data-skipped")
        var total = $(this).attr("data-total")
        skip = parseInt(skip)
        total = parseInt(total)
        if(skip <= total){
            getFavoriteContacts(skip+7,7);
        }
    })

    function getUserSocialFeeds(){

        $.ajax({
            url:'/contacts/favorite/social/feed/web',
            type:'POST',
            datatype:'JSON',
            data:{userIdArr:JSON.stringify(userIdArr)},
            success:function(response){

                $("#social-feed-users").html('');
                if(checkRequired(response) && response.length > 0){
                    for(var i=0; i<response.length; i++){

                        if(checkRequired(response[i].facebook) && response[i].facebook.length > 0){
                            displayFBFeed(response[i].facebook,response[i].user)
                        }

                        if(checkRequired(response[i].twitter) && response[i].twitter.length > 0){
                            displayTwitterFeed(response[i].twitter,response[i].user)
                        }

                        if(checkRequired(response[i].linkedin) && checkRequired(response[i].linkedin.values) && response[i].linkedin.values.length > 0){

                            displayLinkedinFeed(response[i].linkedin.values.length,response[i].linkedin.values,response[i].user)
                        }
                    }
                }
                sidebarHeight()
            }
        })
    }

    function displayLinkedinFeed(total,feeds,user){
        var shareContent = '<div id='+user._id+'>';
        shareContent += '<table>';
        shareContent += '<tr>';
        shareContent += '<td>';
        //var img = '/getImage/'+user._id;
        shareContent += '<img style="  width: 40px;max-width: none;" src='+feeds[0].updateContent.person.pictureUrl+'>';
        shareContent += '</td>';
        shareContent += '<td>';
        shareContent += '<span>'+user.firstName+' '+user.lastName+'</span>';
        shareContent += '</td>';
        shareContent += '<tr>';
        shareContent += '<td>&nbsp;';
        shareContent += '</td>';
        shareContent += '<td>';

        for(var i=0; i<feeds.length; i++){
            linkedinPost(feeds[i]);
            shareContent += '<div id="linkedin-feed-click" updateKey='+feeds[i].updateKey+' style="cursor: pointer;" userId='+user._id+'>';
            switch (feeds[i].updateType){
                case "SHAR": if(checkRequired(feeds[i].updateContent.person.currentShare.content.submittedImageUrl)){
                    shareContent += '<img src='+feeds[i].updateContent.person.currentShare.content.submittedImageUrl+'>'
                }
                    shareContent += '<span>'+feeds[i].updateContent.person.currentShare.content.description+'</span><br><hr><br>';
            }
            shareContent += '</div>'
        }

        shareContent += '</td>';
        shareContent += '</tr>';
        shareContent += '</table>';
        shareContent += '</div>';
        var dom = $(shareContent);
        dom.attr("feed",JSON.stringify(feeds))
        $("#social-feed-users").append(dom);

        function linkedinPost(feed){
            if(checkRequired(feed) && checkRequired(feed.updateContent) && checkRequired(feed.updateContent.person.currentShare) && checkRequired(feed.updateContent.person.currentShare.content) && checkRequired(feed.updateContent.person.currentShare.content.description)){
                var post = '<tr>';
                post += '<td>';
                post += '<img style="width:30px" src='+feeds[0].updateContent.person.pictureUrl+'>';
                post += '</td>';
                post += '<td>';
                post += '<img src="/images/linkedin_20x20.png">';
                post += '</td>';
                post += '<td>';
                post += feed.updateContent.person.currentShare.content.description;
                post += '<img style="width:30px" id="action-feed-comment" class="cursor-pointer" src="/images/comment_icon.png">';

                if(feed.isLiked){
                    post += '<img style="width:30px" id="action-feed-like" class="cursor-pointer" title="liked" src="/images/like_icon.png">';
                }
                else{
                    post += '<img style="width:30px" id="action-feed-like" class="cursor-pointer" title="like" src="/images/like_icon.png">';
                }

                post += '</td>';
                post += '</tr>';
                post = $(post);
                post.find('#action-feed-like').attr('updateKey',feed.updateKey);
                post.find('#action-feed-comment').attr('updateKey',feed.updateKey);

                post.find('#action-feed-like').attr('feed-source','linkedin');
                post.find('#action-feed-comment').attr('feed-source','linkedin');

                $("#feed-list-right").append(post)
            }
        }
    }

    function displayFBFeed(feeds,user){
        for(var i=0; i<feeds.length; i++){
            var post = '<tr>';
            post += '<td>';
            var img = '/getImage/'+user._id;
            post += '<img style="width:30px" src='+img+'>';
            post += '</td>';
            post += '<td>';
            post += '<img src="/images/facebook_icon_24px.png">';
            post += '</td>';
            post += '<td>';
            post += feeds[i].story +', '+ feeds[i].name;
            post += '<img style="width:30px" id="action-feed-comment" class="cursor-pointer" title="like" src="/images/comment_icon.png">';
            post += '<img style="width:30px" id="action-feed-like" class="cursor-pointer" title="like" src="/images/like_icon.png">';
            post += '</td>';

            post += '</tr>';

            post = $(post);
            post.find('#action-feed-like').attr('facebook-post-id',feeds[i].id.split('_')[1]);
            post.find('#action-feed-comment').attr('facebook-post-id',feeds[i].id.split('_')[1]);

            post.find('#action-feed-like').attr('feed-source','facebook');
            post.find('#action-feed-comment').attr('feed-source','facebook');

            $("#feed-list-right").append(post)
        }
    }

    function displayTwitterFeed(feeds,user){
        for(var i=0; i<feeds.length; i++){
            var post = '<tr>';
            post += '<td>';
            post += '<img style="width:30px" src='+feeds[i].user.profile_image_url+'>';
            post += '</td>';
            post += '<td>';
            post += '<img src="/images/icon-twitter-20px.png">';
            post += '</td>';
            post += '<td>';
            post += feeds[i].text;
            post += '&nbsp;&nbsp;<a id="action-feed-favorite" style="text-decoration: underline;cursor: pointer;color: rgb(42, 158, 217);">Favorite</a>&nbsp;';
            post += '<a id="action-feed-re-tweet" style="text-decoration: underline;cursor: pointer;color: rgb(42, 158, 217);">Re-Tweet</a>';
            post += '</td>';
            post += '</tr>';

            post = $(post);
            post.find('#action-feed-favorite').attr('twitter-post-id',feeds[i].id_str);
            post.find('#action-feed-favorite').attr('twitter-post-favorite',feeds[i].favorited);
            post.find('#action-feed-re-tweet').attr('twitter-post-id',feeds[i].id_str);
            post.find('#action-feed-re-tweet').attr('twitter-post-re-tweeted',feeds[i].retweeted);

            post.find('#action-feed-favorite').attr('feed-source','twitter');
            post.find('#action-feed-re-tweet').attr('feed-source','twitter');

            $("#feed-list-right").append(post)
        }
    }

    $("body").on("click", "#linkedin-feed-click", function () {

        var updateKey = $(this).attr("updateKey")
        var userId = $(this).attr("userId")
        var feeds = $("#"+userId).attr('feed')
        feeds = JSON.parse(feeds)
        for(var i=0; i<feeds.length; i++){
            if(feeds[i].updateKey == updateKey){
                displayRightSectionSelectedFeed(feeds[i])
                break;
            }
        }

        $("#default-page-info").hide();
        $("#onclick-page-info").show();
    });

    function displayRightSectionSelectedFeed(feed){
        $("#onclick-page-info").html('');
        var shareContent = '<table>';
        shareContent += '<tr>';
        shareContent += '<td>';
        shareContent += '<img src='+feed.updateContent.person.pictureUrl+'>';
        shareContent += '</td>';
        shareContent += '<td>';
        shareContent += '<span>'+feed.updateContent.person.firstName+' '+feed.updateContent.person.lastName+'</span>';
        shareContent += '</td>';
        shareContent += '</tr>';
        shareContent += '<tr>';
        shareContent += '<td>&nbsp;';
        shareContent += '</td>';
        shareContent += '<td>';

        shareContent += '<span>'+feed.updateContent.person.currentShare.source.serviceProvider.name+'</span><br>';
        if(checkRequired(feed.updateContent.person.currentShare.content.title)){
            shareContent += '<span>'+feed.updateContent.person.currentShare.content.title+'</span><br>';
        }
        if(checkRequired(feed.updateContent.person.currentShare.content.submittedImageUrl)){
            shareContent += '<img style="width: 50px;" src='+feed.updateContent.person.currentShare.content.submittedImageUrl+'><br>';
        }
        if(checkRequired(feed.updateContent.person.currentShare.content.description)){
            shareContent += '<span>'+feed.updateContent.person.currentShare.content.description+'</span><br>';
        }

        shareContent += '</td>';
        shareContent += '</table>';
        $("#onclick-page-info").html(shareContent);
    }

    $("body").on("click", '#action-feed-like',function(){
        if($(this).attr('feed-source') == 'linkedin'){
            var updateKey = $(this).attr('updateKey');
            if(checkRequired(updateKey)){
                $.ajax({
                    url:'/social/feed/linkedin/feed/update/web?action=like&updateKey='+updateKey,
                    type:'GET',
                    datatype:'JSON',
                    success:function(response){
                        if(response){
                            alert('Action successfully completed')
                        }
                        else alert("Your action not completed. Please try again")
                    }
                })
            }
        }
        else if($(this).attr('feed-source') == 'facebook'){
            var postId = $(this).attr('facebook-post-id');
            if(checkRequired(postId)){
                $.ajax({
                    url:'/social/feed/facebook/feed/update/web?action=like&postId='+postId,
                    type:'GET',
                    datatype:'JSON',
                    success:function(response){
                        if(response){
                            alert('Action successfully completed')
                        }
                        else alert("Your action not completed. Please try again")
                    }
                })
            }
        }
    });

    $("body").on("click", '#action-feed-comment',function(){
        if($(this).attr('feed-source') == 'linkedin'){
            var updateKey = $(this).attr('updateKey');
            if(checkRequired(updateKey)){
                $.ajax({
                    url:'/social/feed/linkedin/feed/update/web?action=comment&updateKey='+updateKey,
                    type:'GET',
                    datatype:'JSON',
                    success:function(response){
                        if(response){
                            alert('Action successfully completed')
                        }
                        else alert("Your action not completed. Please try again")
                    }
                })
            }
        }
        else if($(this).attr('feed-source') == 'facebook'){
            var postId = $(this).attr('facebook-post-id');
            var comment = 'I like this, make ground water!'
            if(checkRequired(postId)){
                $.ajax({
                    url:'/social/feed/facebook/feed/update/web?action=comment&postId='+postId+'&comment='+comment,
                    type:'GET',
                    datatype:'JSON',
                    success:function(response){
                        if(response){
                            alert('Action successfully completed')
                        }
                        else alert("Your action not completed. Please try again")
                    }
                })
            }
        }
    });

    $("body").on("click", '#action-feed-favorite',function(){
        var id = $(this).attr('twitter-post-id');
        var favorite = $(this).attr('twitter-post-favorite');
        if(favorite != true && favorite != 'true'){
            if(checkRequired(id)){
                $.ajax({
                    url:'/social/feed/twitter/tweet/update/web?action=favorite&postId='+id,
                    type:'GET',
                    datatype:'JSON',
                    success:function(response){
                        if(response){
                            alert('Action successfully completed')
                        }
                        else alert("Your action not completed. Please try again")
                    }
                })
            }
        }
        else alert('This tweet is already your favorite')
    });

    $("body").on("click", '#action-feed-re-tweet',function(){
        var id = $(this).attr('twitter-post-id');
        var reTweet = $(this).attr('twitter-post-re-tweeted');
        if(reTweet != true && reTweet != 'true'){
            if(checkRequired(id)){
                $.ajax({
                    url:'/social/feed/twitter/tweet/update/web?action=re-tweet&postId='+id,
                    type:'GET',
                    datatype:'JSON',
                    success:function(response){
                        if(response){
                            alert('Action successfully completed')
                        }
                        else alert("Your action not completed. Please try again")
                    }
                })
            }
        }
        else alert('This tweet is already you re-tweeted')
    });

    /*setTimeout(function(){
        showMessagePopUp('hiiiiiiiiiiiiiiiiii','success')
    },4000)*/

    function showMessagePopUp(message, msgClass) {

        var html = _.template($("#message-popup").html())();
        $("#user-msg").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-msg").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top': '9%'});

        if (msgClass == 'error') {
            $(".popover").addClass('popupError');
            $("#popupTitle").text('ERROR')
        } else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess');
            $("#popupTitle").text('SUCCESS')
        } else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip');
            $("#popupTitle").text('TIP')
        }
        $("#message").text(message)
    }

    function sidebarHeight() {
        var sideHeight = $(document).height()
        $(".left-sidebar").css({'min-height': sideHeight})
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});