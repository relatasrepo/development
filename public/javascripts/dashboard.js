$(document).ready(function () {

    $.ajaxSetup({ cache: false });

    var shareFlag = false;
    var plusFlag = true;
    var docFlag = true;
    var defaultDashboardPopupFlag = true;

    var userProfile,userTimezone,myLatlng,mapOptions,map,docName,docUrl,popoverSource,s3bucket,AWScredentials,geocoder,domainName,interactedArrStr,dStatusCount;

    var newRequestArr = [];
    var markerCluster;
    var senderPicArr = [];
    var invitations = [];
    var participants = [];
    var meetingTitleArr = [];
    var participantsArr = [];
    var newInteractionArray = [];
    var monthNameSmall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var styles = [
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        },
        {
            height: 53,
            url: "http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/images/m1.png",
            width: 53
        }
    ]
    var dashboardStatus = JSON.parse($("#dashboardStatusInfo").attr("class"));

    if(checkRequired(dashboardStatus)){

        dStatusCount = 30;

        if(dashboardStatus.completeProfile)
            dStatusCount += 10;

        if(dashboardStatus.addLinkedinProfile)
            dStatusCount += 10;

        if(dashboardStatus.syncContacts)
            dStatusCount += 25;

        if(dashboardStatus.identityShare)
            dStatusCount += 15;

        if(dashboardStatus.sendEmailToYourContacts)
            dStatusCount += 10;

        if(dStatusCount < 100){
            $("#dashboardShowHide").show();
            $("#profileProgressbar").val(dStatusCount);
            $("#progress").text(dStatusCount + '%');
            if(dashboardStatus.sendEmailToYourContacts){
                $("#sendMails").attr("src", '/images/green/email.png');
            }
            if(dashboardStatus.syncContacts){
                $("#uploadContacts").attr("src", '/images/green/upload_contacts.png');
            }
            if(dashboardStatus.identityShare){
                $("#shareImageId").attr("src", '/images/green/share.png');
            }
            if(dashboardStatus.completeProfile){
                $("#updateProfile").attr("src", '/images/green/profile.png');
            }
            if(dashboardStatus.addLinkedinProfile){
                $("#changeProfilePic").attr("src", '/images/green/profile_pic.png');
            }

        }else{
            $("#dashboardShowHide").hide();
        }
        if(dashboardStatus.dashboardPopup){
            defaultDashboardPopupFlag = false;
            $("#dashboardPopupLink").show();
        }else{
            defaultDashboardPopupFlag = true;
            //getMeetingContactAnalytics([],true);
            $("#dashboardPopupLink").hide();
        }
    }

    getUserProfile();
    getTotalContactsCount();
    getMeetingsStatus();
    getTotalDocsCount();
    getNonReadDocs();
    getRedDocs();
    getUnReadMsgCount();
    sidebarHeight();
    getAWSCredentials();

    var isPageLoaded;
    Pace.on('start',function(){
        isPageLoaded = false;
    });

    Pace.on('done',function(){
        isPageLoaded = true;
    });

    $.ajax({
        url:'/getDomainName',
        type:'GET',
        success:function(domain){
            domainName = domain;

        }
    });
    function getAWSCredentials() {
        $.ajax({
            url: '/getAwsCredentials',
            type: 'GET',
            datatye: 'JSON',
            success: function (credentials) {
                AWScredentials = credentials;
                AWS.config.update(credentials);
                AWS.config.region = credentials.region;
                s3bucket = new AWS.S3({ params: {Bucket: credentials.bucket} });
                docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
            }
        })
    }


    function getNonReadDocs() {
        $.ajax({
            url: '/documents/sharedWithMeNonRead/count',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if(checkRequired(result)){
                    if(result){
                        updateNotificationCount(result)
                    }
                }else{

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function getRedDocs(){
        $.ajax({
            url: '/documents/sharedDocRed/count',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if(checkRequired(result)){
                    if(result){
                        updateNotificationCount(result)
                    }
                }else{

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function getUnReadMsgCount(){
        $.ajax({
            url: '/messages/unRead/count',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if(result){
                    updateNotificationCount(result)
                }

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    $("body").on("click","#nonInteracted",function(){
        selectNonInteractedContacts();
    });

    $("body").on("click","#nonInteracted-text",function(){
        selectNonInteractedContacts();
    });

    $("body").on("click", "#go-to-Contacts", function(){
        selectNonInteractedContacts();
    });

    function selectNonInteractedContacts(){
        $("#overlay").remove();
        $.ajax({
            url:"/contacts/nonInteracted/emails",
            type:"POST",
            datatype:"JSON",
            data:{arrStr:interactedArrStr},
            success:function(response){
                if(response)
                    window.location.assign('/contacts');
            }
        });
    }

    $("body").on("click", "#ok-close", function () {
        $("#overlay").remove();
        $("#user-name").popover('destroy');
    });

    function getPendingToDo(timezone, eventSource) {
        $.ajax({
            url: '/meetings/todo/pending?filter=currentDay&timezone=' + timezone,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (events) {

                if (events && checkRequired(events) && events.length > 0) {

                    var now = new Date();
                    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0).toString();
                    for(var event=0; event<events.length; event++){
                        var localStart = new Date(events[event].toDo.dueDate);

                        var date = new Date(localStart)
                        var meetingDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0).toString();
                        if (today == meetingDate) {

                            var eventObj = {
                                id: events[event].toDo._id,
                                title: events[event].toDo.actionItem || 'No title',
                                start: new Date(localStart),
                                end: new Date(),
                                description: '',
                                location: '',
                                invitationId: events[event].invitationId,
                                isEvent:false,
                                isTodo:true,
                                isMeeting: false
                            };

                            eventSource.push(eventObj);
                        }
                    }
                }
                if (eventSource.length > 0) {
                    eventSource.sort(function (o1, o2) {
                        return o1.start < o2.start ? -1 : o1.start > o2.start ? 1 : 0;
                    });
                }

                if (checkRequired(eventSource[0])) {
                    constructAndDisplayTodayMeetingsHtml(eventSource)
                }
                else {
                    var todayMeetingHtml = '<table class="meetings-table">No meetings scheduled for today</table>';
                    $('#meetings-table-content').html(todayMeetingHtml);
                }
            },
            error: function (event, request, settings) {
                if (eventSource.length > 0) {
                    eventSource.sort(function (o1, o2) {
                        return o1.start < o2.start ? -1 : o1.start > o2.start ? 1 : 0;
                    });
                }

                if (checkRequired(eventSource[0])) {
                    constructAndDisplayTodayMeetingsHtml(eventSource)
                }
                else {
                    var todayMeetingHtml2 = '<table class="meetings-table">No meetings scheduled for today</table>';
                    $('#meetings-table-content').html(todayMeetingHtml2);
                }
            },
            timeout: 20000
        })
    }

    function getAllTodayEvents(eventSource){
        $.ajax({
            url:'/events/dashboard/today',
            type:'GET',
            datatype:'JSON',
            success:function(events){
                if(checkRequired(events) && events.length > 0){
                    //var eventSource = [];
                    var now = new Date();
                    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0).toString();
                    for(var event=0; event<events.length; event++){
                        var localStart = new Date(events[event].startDateTime);
                        var localEnd = new Date(events[event].endDateTime);

                        var date = new Date(localStart)
                        var meetingDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0).toString();
                        if (today == meetingDate) {

                            var eventObj = {
                                id: events[event]._id,
                                title: events[event].eventName || 'No title',
                                start: new Date(localStart),
                                end: new Date(localEnd),
                                description: events[event].eventDescription,
                                location: events[event].eventLocation,
                                invitationId: events[event]._id,
                                isEvent:true,
                                isMeeting: false
                            };
                            eventSource.push(eventObj);
                        }
                    }
                }
                getPendingToDo(userTimezone,eventSource);
            }
        })
    }

    //getGoogleCalendarEvents()
    function getGoogleCalendarEvents() {
        $.ajax({
            url: '/interactions/dashboard/today',
            type: 'GET',
            datatype: "json",
            traditional: true,
            success: function (events) {

                var eventSource = [];
                var now = new Date();
                var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0).toString();
                if (!checkRequired(events) && events.length == 0) {

                    eventSource = eventSource.concat(newRequestArr);
                    getAllTodayEvents(eventSource);
                }
                else {

                    for (var i in events) {

                        var localStart = new Date(events[i].interactionDate);
                        var localEnd = new Date(events[i].endDate);

                        var date = new Date(localStart)
                        var meetingDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0).toString();
                        if (today == meetingDate) {

                            var event = {
                                id: events[i]._id,
                                title: events[i].title || 'No title',
                                start: new Date(localStart),
                                end: new Date(localEnd),
                                description: events[i].description,
                                location: events[i].location,
                                invitationId: events[i].refId,
                                isEvent:false,
                                //isMeeting: events[i].source == 'relatas'
                                isMeeting: true
                            }

                            eventSource.push(event);
                        }
                    }

                    eventSource = eventSource.concat(newRequestArr);
                    getAllTodayEvents(eventSource);


                }
            },
            error: function (event, request, settings) {
                getAllTodayEvents(newRequestArr);
            }
        });
    }

    // Function to get user meeting info
    function getMeetingsStatus() {
        getMeetingsStatusUpcoming();
        getMeetingsStatusPastMeeting();
    }

    // Function to get user meeting info
    function getMeetingsStatusUpcoming() {
        $.ajax({
            url: '/dashboard/upcomingMeetings/sevenDays',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if(checkRequired(result) && result.length > 0){
                    $('#upcomingMeetingCount').text(result[0].count || 0);
                }
                else{
                    $('#upcomingMeetingCount').text(0);
                }
            },
            error: function (event, request, settings) {
            },
            timeout: 20000
        })
    }

    function getMeetingsStatusPastMeeting() {
        $.ajax({
            url: '/dashboard/pastMeetings/sevenDays',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                if(checkRequired(result) && result.length > 0){
                    $('#pastMeetingCount').text(result[0].count || 0);
                }
                else{
                    $('#pastMeetingCount').text(0);
                }
            },
            error: function (event, request, settings) {
            },
            timeout: 20000
        })
    }

    var invFlag = false;
    var calPassLength = 0;

    function getReceivedCalendarPasswordRequests() {
        $.ajax({
            url: '/getReceivedCalendarPasswordRequests',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (requests) {
                if (requests == false) {
                    invFlag = false;
                    getNewInvitationsCount()
                }
                else {
                    invFlag = true;
                    if (checkRequired(requests[0])) {
                        updateNotificationCount(requests.length)
                        calPassLength = requests.length;

                        for (var i = 0; i < calPassLength; i++) {
                            displayCalendarPasswordRequests(requests[i])
                        }
                    }
                    setTimeout(function () {
                        getNewInvitationsCount()
                    }, 10)
                }
            },
            error: function (event, request, settings) {
                getNewInvitationsCount()

            },
            timeout: 20000
        })
    }

    function displayCalendarPasswordRequests(request) {
        if (checkRequired(request)) {
            $.ajax({
                url: '/getUserBasicInfo/' + request.requestFrom.userId,
                type: 'GET',
                datatype: 'JSON',
                traditional: true,
                success: function (profile) {
                    if (checkRequired(profile)) {
                        var newRequestsHtml = '';
                        var senderName = profile.firstName + ' ' + profile.lastName;
                        var name = profile.firstName + '-' + profile.lastName;
                        senderName = getTextLength(senderName, 16);
                        var senderPicUrl = profile.profilePicUrl || '/images/default.png';
                        var title1 = "Calendar password request";
                        var title = getTextLength(title1, 16)
                        var start = '';
                        newRequestsHtml += '<td id="calPassReq" requestId=' + request._id + ' requestByEmailId=' + profile.emailId + ' requestByName=' + name + ' requestByUserId=' + profile._id + ' style="padding-right: 10px;cursor:pointer;"><a><table style="width: initial;border: 1px solid #dfdddd;"><tr style="background-color: #00a3e6"><td style="padding: 10px 0px 10px 10px;"><img style="width:40px; height:40px;border: 2px solid rgb(249, 243, 243);border-radius: 20px;" src=' + senderPicUrl + '></td>'
                        newRequestsHtml += '<td style="padding-top: 4%;"><p style="color:#ffffff;margin-bottom: -2%;">Sent by:</p><p style="color:#ffffff;font-weight: bold;">' + senderName + '</p></td></tr>'
                        newRequestsHtml += '<tr><td colspan="2" style="background-color: #f6f4f4;text-align: center;padding: 15px;"><span style="color:#4c4c4c;font-weight: bold;font-size: 19px;">' + title + '</span></td></tr></table></a></td>'
                        $('#requests').append(newRequestsHtml)
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 20000
            })
        }
    }

    $("body").on("click", "#calPassReq", function () {
        var requestedByUserId = $(this).attr("requestByUserId");
        var requestedByName = $(this).attr("requestByName");
        var requestedByEmailId = $(this).attr("requestByEmailId");
        var requestId = $(this).attr("requestId");
        if (popoverSource != null) {
            popoverSource.popover('destroy');
            popoverSource = null;
            return;
        }
        var html = _.template($("#calPass-popup").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'right',
            id: "myPopover"
        });

        $(this).popover('show');
        popoverSource = $(this)
        $("#calPass-submit").attr("requestedByUserId", requestedByUserId);
        $("#calPass-submit").attr("requestedByName", requestedByName);
        $("#calPass-submit").attr("requestedByEmailId", requestedByEmailId);
        $("#calPass-submit").attr("requestId", requestId);
    })

    $("body").on("click", "#closePopup-calPass", function () {
        popoverSource.popover('destroy');
    })

    $("body").on("change", "#notApproved", function () {
        if ($(this).prop("checked")) {
            var userName = userProfile.firstName;
            var message = 'Sorry, ' + userName + ' has declined to share his calendar password with you, at this point in time.'
            $("#messageCalPass").val(message)
            messageFlag = true;
        }
        else {
            $("#messageCalPass").val('')
        }
    })

    $("body").on("change", "#approve", function () {
        if ($(this).prop("checked")) {
            $("#messageCalPass").val('')
            messageFlag = false;
        }
    })

    var messageFlag = false;
    var reloadFlag = false;
    $("body").on("click", "#calPass-submit", function () {
        var requestedByUserId = $(this).attr("requestedByUserId");
        var requestId = $(this).attr("requestId");
        var requestedByName = $(this).attr("requestedByName") || '';
        var requestedByEmailId = $(this).attr("requestedByEmailId");
        requestedByName = requestedByName.replace(/-/g, ' ');
        var calMsg = $("#messageCalPass").val();
        if (messageFlag) {
            calMsg = $.nl2br(calMsg);
        } else if (checkRequired(calMsg)) {
            calMsg = "Message from " + userProfile.firstName + ' ' + userProfile.lastName + " below:\n" + calMsg;
            calMsg = $.nl2br(calMsg);
        }
        var accepted = false;
        if ($("#approve").prop("checked")) {
            accepted = true;
        }
        popoverSource.popover('destroy');
        popoverSource = null;
        if (checkRequired(userProfile.profilePrivatePassword)) {
            var resObj = {
                requestedByUserId: requestedByUserId,
                requestedByName: requestedByName,
                requestedByEmailId: requestedByEmailId,
                publicProfileUrl: userProfile.publicProfileUrl,
                profilePrivatePassword: userProfile.profilePrivatePassword,
                emailId: userProfile.emailId,
                name: userProfile.firstName + ' ' + userProfile.lastName,
                requestId: requestId,
                accepted: accepted,
                msg: calMsg || ''
            };

            if (checkRequired(resObj)) {
                $.ajax({
                    url: '/calendarPasswordResponse',
                    type: 'POST',
                    datatype: 'JSON',
                    data: resObj,
                    success: function (response) {
                        if (response == 'noMsg') {
                            showMessagePopUp("Please provide message.", 'error');
                        } else if (response) {
                            showMessagePopUp("Your response for this request successfully sent.", 'success');
                            reloadFlag = true;
                        }
                        else {
                            showMessagePopUp("Error occurred please try again.", 'error');
                        }
                    }
                })
            }
            else showMessagePopUp("Error occurred please try again.", 'error');
        }
        else {
            showMessagePopUp("You didn't set password for your calendar.", 'error');
        }
    })

    // Functions to get new invitations count
    function getNewInvitationsCount() {
        $.ajax({
            url: '/newInvitationsCount',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {  // result contains invitations count and new invitations array
                getGoogleCalendarEvents();
                if (result.invitations[0]) {

                    var invis = removeDuplicates_id(result.invitations)

                    displayNewInvitations(invis);
                }
                else {
                    if (!invFlag) {
                        $('.meeting-invites-list').text('No New Requests Found');
                    }
                }
            },
            error: function (event, request, settings) {
                getGoogleCalendarEvents();
            },
            timeout: 20000
        })
    }

    // Functions to get total docs count
    function getTotalDocsCount() {
        $.ajax({
            url: '/totalDocsCount',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                $('#documentsSharedCount').text(result.docsCount)

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    // Function to get user contacts info
    function getPendingToDoCount(timezone) {
        $.ajax({
            url: '/meetings/todo/pending?filter=pending&timezone='+timezone+'&returnData=count',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {

                if(checkRequired(result))
                    updateNotificationCount(result)
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    var cCount = 0;
    // Function to get user contacts info
    function getTotalContactsCount() {
        $.ajax({
            url: '/totalContacts',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                $('#contactsCount').text(result.contactCount);
                if(parseInt(result.contactCount) <= 0){
                    $("#data").hide();
                    $("#noData").show();
                }else{
                    $("#noData").hide();
                    $("#data").show();
                }
                if (cCount < 2) {
                    cCount++;
                    setTimeout(function () {
                        getTotalContactsCount();
                    }, 500)
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function addProgress(number) {

        var num = $("#profileProgressbar").attr("value");
        var total = parseInt(num) + number;
        total = total >= 100 ? 100 : total
        $("#profileProgressbar").val(total);
        $("#progress").text(total + '%');
    }

    function getValidUniqueUrl(uniqueName) {
        var url = window.location + ''.split('/');
        var patt = new RegExp(url[2]);
        if (patt.test(uniqueName)) {
            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i + 1];
                }
            }
            return uniqueName;
        } else {
            return uniqueName;
        }
    }

    // Function to get user contacts info
    function getUserProfile() {
        $.ajax({
            url: '/userProfile',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (result) {
                userProfile = result;
                if(result){
                    identifyMixPanelUser(result);
                }

                if(checkRequired(userProfile) && checkRequired(userProfile.timezone) && checkRequired(userProfile.timezone.name)){
                    userTimezone = userProfile.timezone.name;
                }
                else{
                    var tz = jstz.determine();
                    userTimezone = tz.name();
                }
                getPendingToDoCount(userTimezone);
                showUserLocation();
                getReceivedCalendarPasswordRequests();

                $('#profilePic').attr("src", result.profilePicUrl);
                if (result.serviceLogin == 'linkedin') {
                    $("#uploadContactsUrl").attr('href', '/addGoogleContacts');
                    $("#uploadContactsUrl2").attr('href', '/addGoogleContacts');
                }
                imagesLoaded("#profilePic", function (instance, img) {
                    if (instance.hasAnyBroken) {
                        $('#profilePic').attr("src", "/images/default.png");
                    }
                });
            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        })
    }

    function identifyMixPanelUser(profile){

        mixpanel.identify(profile.emailId);
        mixpanel.people.set({
            "$email": profile.emailId,    // only special properties need the $
            "$first_name": profile.firstName,
            "$last_name": profile.lastName,
            "$created": new Date(),
            "$last_login":new Date(profile.lastLoginDate || new Date()),         // properties can be dates...
            "page":window.location.href
        });
    }

    function showUserLocation() {
        geocoder = new google.maps.Geocoder();
        myLatlng = new google.maps.LatLng(13.5333, 2.0833);

        mapOptions = {
            center: myLatlng,
            zoom: 2
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        markerCluster = new MarkerClusterer(map, [], {zoomOnClick: false, minimumClusterSize: 1, styles: styles});

        interactionsCount(userProfile._id);
        google.maps.event.addListener(markerCluster, 'clusterclick', function (cluster) {
            var c = cluster.getMarkers()
            if (checkRequired(c[0])) {
                showGlobalInteractionInfo(c);
            }
        });
        google.maps.event.addListener(map, 'zoom_changed', function () {
            if (map.getZoom() < 2) map.setZoom(2);
        });
    }

    var time = 0;

    function interactionsCount(userId) {

        $.ajax({
            url: '/interactionsCount/individual/' + userId,
            type: 'GET',
            success: function (individualCounts) {
                if (checkRequired(individualCounts) && individualCounts.length > 0) {
                    for (var interaction = 0; interaction < individualCounts.length; interaction++) {
                        if (checkRequired(individualCounts[interaction]._id)) {
                            var result = individualCounts[interaction]._id;
                            result.count = individualCounts[interaction].count;
                            showLocations(result);
                        }
                    }
                }
            }
        })
    }


    $("body").on("click", "#closePopup-googleMeetingInfo", function () {
        $("#mapPopup").popover('destroy');
    });

    var b = 0;
    function showLocations(result) {
        if (checkRequired(result) && result.count !=0) {
            if (checkRequired(result.locationLatLang)) {
                if (checkRequired(result.locationLatLang.latitude) && checkRequired(result.locationLatLang.longitude)) {
                    createMarkerLocationLatLang(result, result.locationLatLang.latitude, result.locationLatLang.longitude);
                } else if (checkRequired(result.location)) {
                    createMarkerGeocode(result, 0);
                } else  createMarkerCurrentLocation(result)
            } else if (checkRequired(result.location)) {
                createMarkerGeocode(result, 0);
            } else  createMarkerCurrentLocation(result)
        }
    }

    function updateLocationLatLang(latLangObj) {
        $.ajax({
            url: '/updateLocationLatLang',
            type: 'POST',
            datatype: 'JSON',
            data: latLangObj,
            success: function (response) {

            }
        })
    }

    function createMarkerCurrentLocation(result) {
        if (checkRequired(result.currentLocation)) {
            if (checkRequired(result.currentLocation.latitude)) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(result.currentLocation.latitude, result.currentLocation.longitude),
                    map: map,
                    title: "Meeting location: " + result.currentLocation.city || '',
                    user: result
                });
                markerCluster.addMarker(marker)
            }
        }
    }

    function createMarkerGeocode(result, limit) {
        geocoder.geocode({ 'address': result.location}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var newAddress = results[0].geometry.location;
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(newAddress.lat()), parseFloat(newAddress.lng())),
                    map: map,
                    title: "Meeting location: " + result.location || '',
                    user: result
                });
                var latLangObj = {
                    userId: result._id,
                    latitude: newAddress.lat(),
                    longitude: newAddress.lng()
                };
                markerCluster.addMarker(marker);
                updateLocationLatLang(latLangObj);

            } else if (status == 'OVER_QUERY_LIMIT') {
                time += 500;
                limit++;
                if (limit < 3) {
                    setTimeout(function () {
                        createMarkerGeocode(result, limit);
                    }, time);
                }
            }
        });
    }


    function createMarkerLocationLatLang(result, lat, lang) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(parseFloat(lat), parseFloat(lang)),
            map: map,
            title: "Meeting location: " + result.location || '',
            user: result
        });
        markerCluster.addMarker(marker);
    }

    function getSenderProfile(senderInfoArr) {
        for (var j in senderInfoArr) {
            getSenderProfileAjax(senderInfoArr[j]);
        }
    }

    function getSenderProfileAjax(senderInfo) {

        var user = {
            id: senderInfo.senderId
        }
        $.ajax({
            url: '/getSenderProfile',
            type: 'POST',
            datatype: 'JSON',
            data: user,
            success: function (profile) {
                if (!profile) {
                    $("#senderPic" + senderInfo.i).attr("src", '/images/default.png');
                } else {
                    $("#senderPic" + senderInfo.i).attr("src", profile.profilePicUrl);
                    $("#senderPic" + senderInfo.i).attr("title", profile.firstName);
                }
                imagesLoaded('#senderPic' + senderInfo.i, function (instance, img) {
                    if (instance.hasAnyBroken) {
                        $('#' + instance.elements[0].id).attr("src", "/images/default.png");
                    }
                });
            }
        })
    }

    function getParticipantImageAjax(participantEmail) {

        $.ajax({
            url: '/getParticipantProfile/' + participantEmail.emailId,
            type: 'GET',
            traditional: true,
            success: function (result) {
                if (checkRequired(result)) {
                    var id = result.emailId.replace(/\@/g, '').replace(/\./g, '') + '' + participantEmail.invitationId;

                    $('.profile' + id).attr("href", '/' + getValidUniqueUrl(result.publicProfileUrl));
                    $('.participant' + id).attr("title", result.firstName + ' ' + result.lastName);
                    imagesLoaded('#participant' + id, function (instance, img) {
                        if (instance.hasAnyBroken) {
                            $('.participant' + id).attr("src", "/images/default.png");
                        }
                    });
                }
                else {

                }
            },
            error: function (event, request, settings) {

            }
        })
    }

    function getParticipantImage(participantEmail) {

        for (var i in participantEmail) {
            if (checkRequired(participantEmail[i].emailId)) {
                getParticipantImageAjax(participantEmail[i])
            }
        }
    }

    function validateSuggestEvent(invitation) {
        if (invitation.suggested && invitation.suggestedBy) {
            if (invitation.suggestedBy.userId == userProfile._id) {
                return false;
            } else return true;
        } else return true;
    }

    function isSender(invitation){
        if(invitation.senderId == userProfile._id){
            return true;
        }else return false;
    }

    function displayNewInvitations(invitationsList) {
        invitations = [];
        senderPicArr = []
        for (var i in invitationsList) {
            if (invitationsList[i].deleted || invitationsList[i].status == 'reScheduled') {
            }
            else{
                if(invitationsList[i].suggested) {
                    if (validateSuggestEvent(invitationsList[i])) {
                        invitationsList[i].isRequest = true;
                        invitationsList[i].isSender = false;
                        validateTimeSlots(invitationsList[i])
                    }else {
                        invitationsList[i].isSender = true;
                        validateTimeSlots(invitationsList[i])
                    }
                }else {
                    invitationsList[i].isSender = isSender(invitationsList[i]);
                    validateTimeSlots(invitationsList[i])
                }
            }
        }
        newRequestHtml(invitations);
    }

    function validateTimeSlots(invitationsList) {
        var today = new Date();
        today.setSeconds(0, 0);
        var slotFlag = true;
        var todayStr = today.getFullYear()+'-'+today.getMonth()+'-'+today.getDate();
        for(var i=0; i<invitationsList.scheduleTimeSlots.length; i++){
            var mDate = new Date(invitationsList.scheduleTimeSlots[i].start.date);
            var mStr = mDate.getFullYear()+'-'+mDate.getMonth()+'-'+mDate.getDate();
            if(todayStr == mStr && slotFlag){
                var event = {
                    id: invitationsList.invitationId,
                    title:invitationsList.scheduleTimeSlots[i].title ,
                    start: mDate,
                    end: new Date(invitationsList.scheduleTimeSlots[i].end.date),
                    description: invitationsList.scheduleTimeSlots[i].description,
                    location: invitationsList.scheduleTimeSlots[i].location,
                    invitationId: invitationsList.invitationId,
                    isEvent:false,
                    isMeeting: true,
                    isSender:invitationsList.isSender,
                    isRequest:true,
                    meetingObj:invitationsList,
                    slotNumber:i
                }
                slotFlag = false;
                newRequestArr.push(event)
            }
        }

        var maxSlot = invitationsList.scheduleTimeSlots.reduce(function (a, b) {
            return new Date(a.start.date) > new Date(b.start.date) ? a : b;
        });
        var dateNew = new Date(maxSlot.start.date);
        dateNew.setSeconds(0, 0);

        if (dateNew >= today && invitationsList.isSender == false) {
            updateNotificationCount(1);
            invitationsList.maxSlot = maxSlot;
            invitations.push(invitationsList);
        }
    }

    function newRequestHtml(invitations) {

        if (invitations[0]) {
            for (var j in invitations) {
                var newRequestsHtml = '';
                var senderName = invitations[j].senderName;
                senderName = getTextLength(senderName, 16);
                var senderPicUrl = '/getImage/' + invitations[j].senderId;
                var meetingUrl = '/meeting/' + invitations[j].invitationId;

                if (invitations[j].suggested && invitations[j].suggestedBy) {
                    senderPicUrl = '/getImage/' + invitations[j].suggestedBy.userId;
                    if (invitations[j].selfCalendar) {
                        if (invitations[j].senderId == userProfile._id) {
                            senderName = invitations[j].toList[0].receiverFirstName;
                        }
                    }
                    else{
                        if (invitations[j].senderId == userProfile._id) {
                            senderName = invitations[j].to.receiverName;
                        }
                    }
                }

                senderPicArr.push({
                    senderId: invitations[j].senderId,
                    i: j
                });
                var title = invitations[j].maxSlot.title;
                title = getTextLength(title, 16)
                var start = new Date(invitations[j].maxSlot.start.date);
                var end = new Date(invitations[j].maxSlot.end.date);
                var zone = '(' + getTimeZone() + ')'
                var meetingTime = getTimeFormat(start) + ' - ' + getTimeFormat(end) + ' ' + zone;
                var meetingDate = start.getDate() + '' + getDayString(start.getDate()) + ' ' + monthNameSmall[start.getMonth()] + ' ' + start.getFullYear();

                newRequestsHtml += '<td style="padding-right: 10px"><a href=' + meetingUrl + '><table style="width: initial;border: 1px solid #dfdddd;"><tr style="background-color: #00a3e6"><td style="padding: 10px 0px 10px 10px;"><img style="width:40px; height:40px;border: 2px solid rgb(249, 243, 243);border-radius: 20px;" id=' + 'senderPic' + j + ' src=' + senderPicUrl + '></td>'
                newRequestsHtml += '<td style="padding-top: 4%;"><p style="color:#ffffff;margin-bottom: -2%;">Sent by:</p><p style="color:#ffffff;font-weight: bold;">' + senderName + '</p></td></tr>'
                newRequestsHtml += '<tr><td colspan="2" style="background-color: #f6f4f4;text-align: center;padding: 15px;"><span style="color:#4c4c4c;font-weight: bold;font-size: 19px;">' + title + '</span><br><span id="newMeetingDate" style="color:#666666">' + meetingDate + '</span><br><span id="newMeetingTime" style="color:#666666;">' + meetingTime + '</span></td></tr></table></a></td>'

                var length = $('#requests').children().length;
                if (length <= 3) {
                    $('#requests').append(newRequestsHtml)
                }
                else {
                    var otherRequests = '<td style="padding-right: 20px;"><a href="/notifications"><span style="font-size: 30px">+</span></a></td>';
                    if (plusFlag)
                        $('#requests').append(otherRequests);
                    plusFlag = false;
                }
            }
        }
        else {

            if (calPassLength == 0)
                $('.meeting-invites-list').text('No New Requests Found');
        }
    }

    function getTextLength(text, maxLength) {
        var textLength = text.length;
        if (textLength > maxLength) {
            var formattedText = text.slice(0, maxLength)
            return formattedText + '..';
        }
        else return text;
    }

    function setMeetingTitles() {
        if (checkRequired(meetingTitleArr[0])) {
            for (var i = 0; i < meetingTitleArr.length; i++) {
                $("#meetingTitle" + meetingTitleArr[i].id).attr("title", meetingTitleArr[i].title);
            }
        }
    }

    function constructAndDisplayTodayMeetingsHtml(meetings) {
        participants = []
        var invitations = [];
        var eventArr = [];
        if (meetings[0]) {
            var todayScheduleHtml = '<table class="meetings-table" style="vertical-align: middle">';
            for (var i in meetings) {
                var start = new Date(meetings[i].start);
                var end = new Date(meetings[i].end);
                var meetingTitleN = meetings[i].title;
                var meetingTitle = getTextLength(meetingTitleN, 25);
                meetingTitleN = meetingTitleN.replace(/\s/g, '-');
                var zone = '(' + getTimeZone() + ')';
                var meetingTime = ''
                if(meetings[i].isTodo){
                    meetingTime = getTimeFormat(start) +' ' + zone;
                }else
                 meetingTime = getTimeFormat(start) + ' - ' + getTimeFormat(end) + ' ' + zone;

                var meetingTimeTitle = meetingTime;
                meetingTime = getTextLength(meetingTime,23);
                var meetingTimeN = getDateFormat(start) + '=' + getTimeFormatDate(start);
                var meetingLocation = meetings[i].location || '';
                var meetingUrl = '#';
                todayScheduleHtml += '<tr><td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 25%;vertical-align: middle"><div class="meeting-time">';

                if(meetings[i].isTodo){
                    todayScheduleHtml += '<img style="height: 25px" src="/images/relatasIcon.png"/>'
                    todayScheduleHtml += '&nbsp;<img src="/images/clock_new.png" />&nbsp;<span id="meetingTime" title='+meetingTimeTitle.replace(/\s/g, '&nbsp;')+'>' + meetingTime + '</span></div></td>';
                    meetingUrl = '/meeting/' + meetings[i].invitationId;

                    todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle"><div class="meeting-title"><span style="font-style: normal;font-size: 16px;font-weight: normal"><img style="margin-top: -1%;width: 25px" src="/images/todo1.jpg"/><a href=' + meetingUrl + ' style="color: #666666" id=' + 'meetingTitle' + meetings[i].id + '>' + meetingTitle + '</a></span></div></td>';
                    todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle" >&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal" title=' + meetingLocation.replace(/\s/g, '&nbsp;') + '>' + getTextLength(meetingLocation, 15) + '</span></td>'
                }else
                if (meetings[i].isEvent) {
                    todayScheduleHtml += '<img style="height: 25px" src="/images/relatasIcon.png"/>'
                    todayScheduleHtml += '&nbsp;<img src="/images/clock_new.png" />&nbsp;<span id="meetingTime" title='+meetingTimeTitle.replace(/\s/g, '&nbsp;')+'>' + meetingTime + '</span></div></td>';
                    meetingUrl = '/event/' + meetings[i].id;
                    eventArr.push(meetings[i].id);
                    todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle"><div class="meeting-title"><span style="font-style: normal;font-size: 16px;font-weight: normal"><img style="margin-top: -1%" src="/images/event_new.png"/><span id=' + 'rsvp' + meetings[i].id+ '></span>&nbsp;&nbsp;<a href=' + meetingUrl + ' style="color: #666666" id=' + 'meetingTitle' + meetings[i].id + '>' + meetingTitle + '</a></span></div></td>';
                    todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle" ><img src="/images/icon_location_small.png"/>&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal" title=' + meetingLocation.replace(/\s/g, '&nbsp;') + '>' + getTextLength(meetingLocation, 15) + '</span></td>'
                }
                else if (meetings[i].isMeeting) {
                    todayScheduleHtml += '<img style="height: 25px" src="/images/relatasIcon.png"/>';
                    meetingUrl = '/meeting/' + meetings[i].invitationId;
                    var acceptUrl = '/meeting/accept/' + meetings[i].invitationId;
                    todayScheduleHtml += '&nbsp;<img src="/images/clock_new.png" />&nbsp;<span id="meetingTime" title='+meetingTimeTitle.replace(/\s/g, '&nbsp;')+'>' + meetingTime + '</span></div></td>';

                    if(meetings[i].isSender == true){
                        todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle"><div class="meeting-title"><span><a href=' + meetingUrl + '><img src="/images/mailer/pending.jpg"></a>&nbsp;</span><a href=' + meetingUrl + ' style="color: #666666"><span id=' + 'meetingTitle' + meetings[i].id + ' style="font-style: normal;font-size: 16px;font-weight: normal">' + meetingTitle + '</span></a></div></td>';
                    }else if(meetings[i].isSender == false){
                        todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle"><div class="meeting-title"><span><a href=' + acceptUrl + '><img src="/images/mailer/confirm.jpg"></a>&nbsp;</span><a href=' + meetingUrl + ' style="color: #666666"><span id=' + 'meetingTitle' + meetings[i].id + ' style="font-style: normal;font-size: 16px;font-weight: normal">' + meetingTitle + '</span></a></div></td>';
                    }else  todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle"><div class="meeting-title"><a href=' + meetingUrl + ' style="color: #666666"><span id=' + 'meetingTitle' + meetings[i].id + ' style="font-style: normal;font-size: 16px;font-weight: normal">' + meetingTitle + '</span></a></div></td>';

                    todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3 ;border-right: none;border-bottom: none;width: 20%;vertical-align: middle"><div class="meeting-location" id=' + 'location' + meetings[i].invitationId + '></div></td>';
                }
                else {
                    todayScheduleHtml += '<img style="height: 25px" src="/images/social_g+.png"/>';
                    todayScheduleHtml += '&nbsp;<img src="/images/clock_new.png" /><span id="meetingTime" title='+meetingTimeTitle.replace(/\s/g, '&nbsp;')+'>' + meetingTime + '</span></div></td>';
                    todayScheduleHtml += '<td style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 35%;vertical-align: middle" class="meeting-details"><div class="meeting-title"><span id=' + 'meetingTitle' + meetings[i].id + ' style="font-style: normal;font-size: 16px;font-weight: normal">' + meetingTitle + '</span></div></td>';
                    if (checkRequired(meetingLocation)) {
                        todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3 ;border-right: none;border-bottom: none;width: 20%;vertical-align: middle"><img src="/images/icon_location_small.png"/>&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal" title=' + meetingLocation.replace(/\s/g, '&nbsp;') + '>' + getTextLength(meetingLocation, 15) + '</span></td>';
                    }
                    else todayScheduleHtml += '<td class="meeting-details" style="border-top: 1px solid #c3c3c3 ;border-right: none;border-bottom: none;width: 20%;vertical-align: middle"></td>';
                }
                meetingTitleArr.push(meetings[i]);
                if(meetings[i].isTodo){
                    todayScheduleHtml += '<td colspan="2" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;vertical-align: middle;background-color: #f9f9f9;" >';
                    todayScheduleHtml += '</td>';
                }else
                if (meetings[i].isEvent) {
                    todayScheduleHtml += '<td colspan="2" style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;vertical-align: middle;background-color: #f9f9f9;" id='+meetings[i].id+'>';
                    todayScheduleHtml += '</td>';
                }
                else if (meetings[i].isMeeting) {
                    invitations.push({
                        invitationId: meetings[i].invitationId,
                        meetingTitle: meetingTitleN,
                        meetingTime: meetingTimeN,
                        meetingObj:meetings[i].meetingObj,
                        slotNumber:meetings[i].slotNumber,
                        isRequest:meetings[i].isRequest ? true:false,
                        progressId: 'docProgress' + i
                    })

                    todayScheduleHtml += '<td style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width: 15%" class="meeting-details">';
                    todayScheduleHtml += '<div style="width: 10% !important" class="meeting-participants-list abc" id=' + 'participants' + meetings[i].invitationId + '>';

                    todayScheduleHtml += '</div></td>';
                    todayScheduleHtml += '<td style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;width:10%" class="meeting-details" id=' + 'relatedDoc' + meetings[i].invitationId + '>';

                }
                else {
                    todayScheduleHtml += '<td style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;vertical-align: middle" class="meeting-details">';
                    todayScheduleHtml += '<div style="width: 15% !important" class="meeting-participants-list abc" id=' + 'participants' + meetings[i].invitationId + '>';

                    todayScheduleHtml += '</div></td>';
                    todayScheduleHtml += '<td style="border-top: 1px solid #c3c3c3;border-right: none;border-bottom: none;vertical-align: middle" class="meeting-details" id=' + 'relatedDoc' + meetings[i].invitationId + '>';
                }
                todayScheduleHtml += '</td></tr>';
            }
            todayScheduleHtml += '</table>';
            $('#meetings-table-content').html(todayScheduleHtml);

            getMeetingInfo(invitations);
            getEventDetails(eventArr);
            sidebarHeight();
            setMeetingTitles();
        }
        else {
            sidebarHeight();
            var todayMeetingHtml = '<table class="meetings-table">No meetings scheduled for today</table>';
            $('#meetings-table-content').html(todayMeetingHtml);
            setMeetingTitles();
        }

    }

    function getMeetingInfo(invitations) {

        if (checkRequired(invitations[0])) {
            for (var i in invitations) {
                if(invitations[i].isRequest){
                    DisplayLocationTypes(invitations[i].meetingObj,invitations[i].meetingObj.scheduleTimeSlots[invitations[i].slotNumber],invitations)
                }else{
                    $.ajax({
                        url: '/invitationById/' + invitations[i].invitationId,
                        type: 'GET',
                        datatype: "json",
                        traditional: true,
                        success: function (invitation) {
                            if (!invitation) {

                            }
                            else {

                                invitation.scheduleTimeSlots.forEach(function (slot) {
                                    if (slot.isAccepted == true) {
                                        DisplayLocationTypes(invitation,slot,invitations)
                                    }
                                })
                            }
                        }
                    })
                }
            }
        }
    }

    function DisplayLocationTypes(invitation,slot,invitations){
        var todayScheduleHtml = '';
        var location = '';

        if(invitation.suggested){
            location = checkRequired(slot.suggestedLocation) ? slot.suggestedLocation : slot.location;
        }else location = slot.location;

        switch (slot.locationType) {
            case 'In-Person':
                var htmlLocation1 = '<img src="/images/icon_location_small.png"/>&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal" title=' + location.replace(/\s/g, '&nbsp;') + '>' + getTextLength(location, 15) + '</span>'
                $("#location" + invitation.invitationId).html(htmlLocation1)
                break;
            case 'Phone':
                var htmlLocatio2n = '<img style="width: 20px;margin-top: -2%" src="/images/icon_phone.png"/>&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal">' + getTextLength(location, 15) + '</span>'
                $("#location" + invitation.invitationId).html(htmlLocatio2n)
                break;
            case 'Skype':
                var l = "skype:" + slot.location + "?call";
                var htmlLocation3 = '<img style="width: 20px;margin-top: -2%" src="/images/icon_skype.png"/>&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal" title=' + location.replace(/\s/g, '&nbsp;') + '><a href=' + l + '>' + getTextLength(location, 15) + '</a></span>'
                $("#location" + invitation.invitationId).html(htmlLocation3)
                break;
            case 'Conference Call':
                var htmlLocation4 = '<img style="width: 20px;margin-top: -2%" src="/images/conference_call.png"/>&nbsp;&nbsp;<span id="meetingLocation" style="font-style: normal;font-size: 16px;font-weight: normal" title=' + location.replace(/\s/g, '&nbsp;') + '>' + getTextLength(location, 15) + '</span>'
                $("#location" + invitation.invitationId).html(htmlLocation4)
                break;
        }

        var result = getByValue(invitations, invitation.invitationId)
        var participantEmail2 = userProfile.emailId == invitation.participants[0].emailId ? invitation.participants[1].emailId : invitation.participants[0].emailId;
        var participantEmail = '';
        for (var p = 0; p < invitation.participants.length; p++) {
            if (invitation.participants[p].emailId != userProfile.emailId) {
                participantEmail += invitation.participants[p].emailId + ",";
            }
        }
        var invitationId = invitation.invitationId;
        var progressId = result.progressId;

        if (checkRequired(invitation.docs[0])) {
            if (checkRequired(invitation.docs[0].documentName)) {
                invitation.docs.forEach(function (doc) {

                    var docUrl = '/readDocument/' + doc.documentId;
                    var docName = doc.documentName.replace(/\s/g, '&nbsp;');
                    todayScheduleHtml += '<div style="width: 15%" class="meeting-attachments-list"><a class="meeting-attachment" href=' + docUrl + '>';
                    todayScheduleHtml += '<img class="profile-picture-border" style="position:inherit;margin-left: -40%" title=' + docName + ' src="/images/document_icon_new.png"></a></div>';

                });

            } else {
                todayScheduleHtml += '<div class="meeting-attachments-list" style="width: 15%">';
                todayScheduleHtml += '<label id="selectDoc" participants=' + participantEmail + ' invitationId=' + invitationId + ' progressId=' + progressId + ' class="add-attachment-btn"><img title="Click to upload Document" style="cursor: pointer;position: inherit;width: 30px;margin-top: 150%" class="" src="images/upload_document_icon_new.png" /><progress class="progress" id=' + progressId + ' value="0" max="100"></progress></label>';
                todayScheduleHtml += '<form action="/uploadDocument" enctype="multipart/form-data" id="uploadDocumentForm" method="post" style="display: none">';
                todayScheduleHtml += '<input type="text" id="participantsInput" name="participants" >';
                todayScheduleHtml += '<input type="text" name="invitationId" id="invitationIdInput">';
                todayScheduleHtml += '<input type="file" name="file" accept="application/pdf" id="selectFile"></form>';
            }
        } else {
            todayScheduleHtml += '<div class="meeting-attachments-list" style="width: 15%">';
            todayScheduleHtml += '<label id="selectDoc" participants=' + participantEmail + ' invitationId=' + invitationId + ' progressId=' + progressId + ' class="add-attachment-btn"><img title="Click to upload Document" style="cursor: pointer;position: inherit;width: 30px;margin-top: 150%" class="" src="images/upload_document_icon_new.png" /><progress class="progress" id=' + progressId + ' value="0" max="100"></progress></label>';
            todayScheduleHtml += '<form action="/uploadDocument" enctype="multipart/form-data" id="uploadDocumentForm" method="post" style="display: none">';
            todayScheduleHtml += '<input type="text" id="participantsInput" name="participants" >';
            todayScheduleHtml += '<input type="text" name="invitationId" id="invitationIdInput">';
            todayScheduleHtml += '<input type="file" name="file" accept="application/pdf" id="selectFile"></form>';
        }

        var pId = participantEmail2.replace(/\@/g, '').replace(/\./g, '');
        pId += invitation.invitationId

        $("#relatedDoc" + invitation.invitationId).html(todayScheduleHtml);

        var participantsHtml = '';
        participantsArr.push({
            emailId: participantEmail2,
            invitationId: invitation.invitationId
        });
        var plusFlag = true;
        if (invitation.selfCalendar) {
            if(invitation.toList.length > 1){
                plusFlag = true;
            }else plusFlag = false;
            var imageFlag = true;
            var imageUrl = '';
            var obj = {};
            if(invitation.senderId == userProfile._id){
                for (var k = 0; k < invitation.toList.length; k++) {
                    if(checkRequired(invitation.toList[k].receiverId)){
                        if(imageFlag){
                            imageUrl = '/getImage/'+invitation.toList[k].receiverId;
                            imageFlag = false;
                            obj = {
                                emailId: invitation.toList[k].receiverEmailId,
                                invitationId: invitation.invitationId,
                                invitation: invitation
                            }
                        }
                    }
                }
                if(!checkRequired(imageUrl)){
                    imageUrl = '/getImage/hfhgdh234324hghjgjhg34';
                    obj = {
                        emailId: invitation.toList[0].receiverEmailId,
                        invitationId: invitation.invitationId,
                        invitation: invitation,
                        noReceiverId:true
                    }

                }

            }
            else{
                imageUrl = '/getImage/'+invitation.senderId;
                obj = {
                    emailId: invitation.senderEmailId,
                    invitationId: invitation.invitationId,
                    invitation: invitation
                }

            }


            var meetingUrl = '/meeting/' + invitation.invitationId;
            var pId2 = obj.emailId.replace(/\@/g, '').replace(/\./g, '');
            pId2 += invitation.invitationId;
            participantsHtml += '<table><tr><td>'
            participantsHtml += '<div style="width: 15%;height:0px" class="profile-picture-red meeting-participant-pic">';
            if(obj.noReceiverId == true){
                participantsHtml += '<a style="cursor: pointer"><img style="width:30px;height:30px;border-radius: 25px;" src='+imageUrl+' title='+obj.emailId+'></a></div>';
            }else participantsHtml += '<a class=' + 'profile' + pId2 + '><img style="width:30px;height:30px;border-radius: 25px;" src='+imageUrl+' class=' + 'participant' + pId2 + '></a></div>';

            participantsHtml += '</td><td>'

            participantsHtml += '<div style="width: 15%;height:0px" class="profile-picture-red meeting-participant-pic">';
            if(plusFlag)
                participantsHtml += '<a href=' + meetingUrl + ' style="font-size: 150%;">+</a>';

            participantsHtml += '</div></td></tr></table>';

            $("#participants" + invitation.invitationId).html(participantsHtml);

            if(obj.noReceiverId != true){

                getParticipantImage([obj])
            }

        } else {
            var imageUrl2;
            if(invitation.senderId == userProfile._id){
                imageUrl2 = '/getImage/'+invitation.to.receiverId
            }else imageUrl2 = '/getImage/'+invitation.senderId

            participantsHtml += '<div style="width: 15%;height:0px" class="profile-picture-red meeting-participant-pic">';
            participantsHtml += '<a class=' + 'profile' + pId + '><img style="width:30px;height:30px;border-radius: 25px;margin-top: -1050%" src='+imageUrl2+' class=' + 'participant' + pId + '></a></div>';
            $("#participants" + invitation.invitationId).html(participantsHtml);
            getParticipantImage([
                {
                    emailId: participantEmail2,
                    invitationId: invitation.invitationId,
                    invitation: invitation
                }
            ])
        }
    }

    function getEventDetails(arr) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i]) {
                getEventDetailsAjax(arr[i]);
                getEventToShare(arr[i])
            }
        }
    }

    function getEventDetailsAjax(eventId) {
        $.ajax({
            url: '/event/getGoogleId/' + eventId,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success:function (response) {
                if (!checkRequired(response)) {
                    addSVPButton(eventId)
                }
                else if (response == false) {
                    addSVPButton(eventId)
                }
                else {
                    if (checkRequired(response.eventsList)) {
                        if (checkRequired(response.eventsList[0])) {
                            if (response.eventsList[0].googleEventId) {
                                if (response.eventsList[0].isDeleted) {
                                    addSVPButton(eventId)
                                }
                                else {
                                    if (response.eventsList[0].confirmed) {

                                    }
                                    else {
                                        addSVPButton(eventId)
                                    }
                                }
                            }
                            else {
                                addSVPButton(eventId)
                            }
                        }
                        else  addSVPButton(eventId);  // show rsvp button
                    }
                    else  addSVPButton(eventId)  // show rsvp button
                }
            }
        })
    }

    function addSVPButton(eventId) {
        var cUrl = '/event/addToCalendar/' + eventId + '/' + userProfile._id;
        $("#rsvp" + eventId).append('<a href=' + cUrl + '><img src="/images/mailer/rsvp.png"></a>');
    }


    function getEventToShare(eventId){
        $.ajax({
            url: '/event/getById/' + eventId,
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success:function (response) {
                if (checkRequired(response) && checkRequired(response.eventName)) {

                    var url = domainName+'/event/'+eventId;

                    var linkedinUrl = "http://www.linkedin.com/shareArticle?mini=true&url="+encodeURIComponent(url)+"&title="+encodeURIComponent(response.eventName)+"&summary="+encodeURIComponent(response.speakerDesignation || response.eventName)+"&source=Relatas"
                    var twitterUrl ="http://twitter.com/share?text="+encodeURIComponent('Checkout this event: '+response.eventName)+":&url="+encodeURIComponent(url)
                    var facebookUrl = "https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(url)

                    var eventHtml = '<table style="margin-left: -50%;width: 150%"><tr>';
                    eventHtml += '<td><span style="color: #666666">Share:</span></td>'
                    eventHtml += '<td><a target="_share" href='+linkedinUrl+'><img src="/images/icon_linkedin.png" style="width: 70%;"></a></td>'
                    eventHtml += '<td><a target="_share" href='+twitterUrl+'><img src="/images/icon_twitter.png" style="width: 70%;"></a></td>'
                    eventHtml += '<td><a target="_share" href='+facebookUrl+'><img src="/images/icon_facebook.png" style="width: 70%;"></a></td>'
                    eventHtml += '</tr></table>';
                    $("#"+eventId).append(eventHtml);
                }
            }
        })
    }

    function getByValue(arr, invitationId) {
        for (var i in arr) {
            if (arr[i].invitationId == invitationId) return arr[i];
        }
    }

    // Add participant button
    $("body").on("click", "#addParticipant", function () {
        if (popoverSource != null) {
            popoverSource.popover('destroy');
            popoverSource = null;
            return;
        }
        var html = _.template($("#addInvitees-template-details").html())();
        $(this).popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $(this).popover('show');
        popoverSource = $(this)
        var invitationId = $(this).attr("invitationId");
        $("#addInvitees-submit").attr("invitationId", invitationId);
    });

    $("body").on("click", "#closePopup-addInvitees", function () {
        popoverSource.popover('destroy');
    });

    $("body").on("click", "#addInvitees-submit", function () {
        var invitationId = $(this).attr("invitationId");
        var invitees = $("#inviteesEmails").val();
        invitees = invitees.toLowerCase();
        if (checkRequired(invitees)) {
            addParticipantAjaxCall(invitationId, invitees);
        }
        else {
            showMessagePopUp("Please provide invitees emails", 'error')
        }
        popoverSource.popover('destroy');
    });

    function addParticipantAjaxCall(invitationId, invitees) {
        $.ajax({
            url: '/addInvitees/' + invitationId + '/' + invitees,
            type: 'GET',
            success: function (inviteesAddInfo) {
                if (!inviteesAddInfo) {
                    showMessagePopUp("Adding invitees failed", 'error')
                }
                else window.location.reload();
            }
        })
    }

    $("body").on("click", "#selectDoc", function () {
        var element = $(this)
        var invitationId = $(this).attr('invitationId');
        var participants = $(this).attr('participants');
        var progressId = $(this).attr('progressId');
        getAllDocuments(element, invitationId, participants, progressId);
    });
    function getAllDocuments(element, invitationId, participants, progressId) {
        $.ajax({
            url: '/myDocuments',
            type: 'GET',
            datatype: 'JSON',
            traditional: true,
            success: function (documents) {
                showUploadedDocsPopup(documents.documents, element, invitationId, participants, progressId)
            }
        })
    }

    function showUploadedDocsPopup(documents, element, invitationId, participants, progressId) {
        var html = _.template($("#uploaded-docs-popup").html())();
        element.popover({
            title: "",
            html: true,
            content: html,
            container: 'body',
            placement: 'left',
            id: "myPopover"
        });
        popoverSource = element
        element.popover('show');
        $("#uploadedDocTable").attr("invitationId", invitationId);
        $("#uploadedDocTable").attr("participants", participants);
        $("#uploadedDocTable").attr("progressId", progressId);
        var docsHtml = '';
        if (checkRequired(documents[0])) {
            for (var i in documents) {
                var docId = documents[i]._id;
                var documentName = documents[i].documentName;
                var documentUrl = documents[i].documentUrl;
                docsHtml += '<tr><td style="cursor:pointer" documentUrl=' + documentUrl + ' documentId=' + docId + ' documentName=' + documentName.replace(/\s/g, '-') + '>' + documentName + '</td></tr>';
            }
        }
        else {
            docsHtml += '<tr><td>No uploaded documents</td></tr>';
        }
        $("#uploadedDocTable").html(docsHtml)
    }

    $("body").on("click", "#closePopup-uploaded-docs", function () {
        popoverSource.popover('destroy');
    });

    $("body").on("click", "#uploadedDocTable td", function () {
        var documentId = $(this).attr("documentId");
        var docName = $(this).attr("documentName");
        var documentUrl = $(this).attr("documentUrl");
        var invitationId = $("#uploadedDocTable").attr('invitationId');
        var participants = $("#uploadedDocTable").attr('participants');
        var progressId = $("#uploadedDocTable").attr('progressId');
        docName = docName.replace(/\-/g, ' ');
        if (checkRequired(documentId) && checkRequired(docName) && checkRequired(documentUrl) && checkRequired(invitationId) && checkRequired(participants) && checkRequired(invitationId)) {
            var data = {
                documentId: documentId,
                invitationId: invitationId,
                participants: participants
            }
            if (checkRequired(popoverSource)) {
                popoverSource.popover('destroy');
            }

            $.ajax({
                url: '/addDocToMeeting',
                type: 'POST',
                datatype: 'JSON',
                data: data,
                success: function (response) {
                    mixpanelTrack("Share Document");
                    mixpanelIncrement("# Doc S");
                    window.location.reload();
                }
            })
        }
    });

    // Add Document button
    $("body").on("click", "#addDocument", function () {

        var invitationId = $("#uploadedDocTable").attr('invitationId');
        var participants = $("#uploadedDocTable").attr('participants');
        var progressId = $("#uploadedDocTable").attr('progressId');

        if (checkRequired(invitationId) && checkRequired(participants) && checkRequired(progressId)) {
            var fileChooser = document.getElementById('selectFile');
            if (docFlag) {
                $('#selectFile').trigger('click');
                docFlag = false;
                $('#selectFile').on('change', function () {
                    $('#participantsInput').val(participants);
                    $('#invitationIdInput').val(invitationId);
                    var file = fileChooser.files[0];
                    if (file) {
                        $("#" + progressId).show();
                        var dateNow = new Date();
                        var d = dateNow.getDate();
                        var m = dateNow.getMonth();
                        var y = dateNow.getFullYear();
                        var hours = dateNow.getHours();
                        var min = dateNow.getMinutes();
                        var sec = dateNow.getSeconds();
                        var timeStamp = y + ':' + m + ':' + d + ':' + hours + ':' + min + ':' + sec;
                        var dIdentity = timeStamp + '_' + file.name;
                        var docNameTimestamp = dIdentity.replace(/\s/g, '');
                        docName = file.name;
                        var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};

                        var request = s3bucket.putObject(params);
                        request.on('httpUploadProgress', function (progress) {

                            $("#" + progressId).val(progress.loaded / progress.total * 100)
                        });
                        request.send(function (err, data) {
                            if (err) {
                                docFlag = true;
                                showMessagePopUp("Doc upload failed", 'error');
                            }
                            else {
                                docFlag = true;
                                docUrl = docUrl + docNameTimestamp;
                                var docD = {
                                    documentName: docName,
                                    documentUrl: docUrl,
                                    awsKey: docNameTimestamp,
                                    participants: participants,
                                    invitationId: invitationId,
                                    sharedDate: new Date()
                                }

                                $.ajax({
                                    url: '/uploadDocument',
                                    type: 'POST',
                                    datatype: 'JSON',
                                    data: docD,
                                    traditional: true,
                                    success: function (result) {
                                        mixpanelTrack("Share Document");
                                        mixpanelIncrement("# Doc S");
                                        window.location.reload();
                                    }
                                })
                            }
                        });
                    }
                    else {
                        showMessagePopUp("Please select file to upload.", 'error');
                    }
                })
            }
            else {

            }
        }
    });

    function showMessagePopUp(message, msgClass) {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        if (msgClass == 'error') {
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }
        else if (msgClass == 'success') {
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }
        else if (msgClass == 'tip') {
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message);
        $("#closePopup-message").focus();

    }

    $("body").on("click", "#closePopup-message", function () {
        $("#user-name").popover('destroy');
        if (reloadFlag) {
            reloadFlag = false;
            window.location.reload();
        }
    });

    function showGlobalInteractionInfo(cluster) {
        $("#mapPopup").popover('destroy');
        var infowindow = new google.maps.InfoWindow({
            content: '<div id="content"></div>'
        });
        var infoContent = cluster;
        var totalInteractionCount = 0;
        var mmmmm = ''
        for (var m = 0; m < infoContent.length; m++) {
            totalInteractionCount += parseInt(infoContent[m].user.count || 0);
            var name = infoContent[m].user.firstName + ' ' + infoContent[m].user.lastName;
            var totalm = infoContent[m].user.count < 10 ? '0' + infoContent[m].user.count : infoContent[m].user.count;
            var userUrl = '/' + getValidUniqueUrl(infoContent[m].user.publicProfileUrl)
            var summaryUrl = '/interactions/summary/' + infoContent[m].user._id;
            mmmmm += '<tr><td style="width: 30%" title=' + name.replace(/\s/g, '&nbsp;') + '><a style="color:#333333" href=' + userUrl + '>' + getTextLength(name, 14) + '</a></td><td style="text-align: center;width: 30%"><a style="color:#333333" href=' + summaryUrl + '>' + totalm + '</a></td></tr>'
        }

        var html = _.template($("#googleMeetingInfo-popup").html())();
        $("#mapPopup").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#mapPopup").popover('show');
        $(".popover").css({'margin-top': '10%'})
        $(".popover").css({'max-width': '20%'})

        $(".arrow").addClass("invisible")
        var totalPeople = infoContent.length;
        $("#totalPeople").text(totalPeople < 10 ? '0' + totalPeople : totalPeople);
        $("#totalMeetings").text(totalInteractionCount < 10 ? '0' + totalInteractionCount : totalInteractionCount);
        $("#gTable tbody").html(mmmmm);
    }

    function getDayString(day) {
        if (day == 1 || day == 21 || day == 31) {
            return 'st';
        }
        else if (day == 2 || day == 22) {
            return 'nd';
        }
        else if (day == 3 || day == 23) {
            return 'rd';
        }
        else return 'th';
    }

    function getTimeFormat(date) {
        var hrs = date.getHours();
        var min = date.getMinutes();
        var meridian;
        if (hrs > 12) {
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if (hrs == 12) {
                meridian = 'PM'
            } else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0" + min;
        }
        var time = hrs + ":" + min + " " + meridian;
        return time;
    }

    function getTimeFormatDate(date) {
        var dateObj = new Date(date)
        var hrs = dateObj.getHours();
        var min = dateObj.getMinutes();
        var meridian;
        if (hrs > 12) {
            hrs -= 12;
            meridian = 'PM';
        }
        else {
            if (hrs == 12) {
                meridian = 'PM'
            } else
                meridian = 'AM';
        }
        if (min < 10) {
            min = "0" + min;
        }
        var time = hrs + ":" + min + "=" + meridian;
        return time;
    }

    function getDateFormat(date) {
        var dateObj = new Date(date)
        return dateObj.getDate() + '-' + monthNameSmall[dateObj.getMonth()] + '-' + dateObj.getFullYear()
    }

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    function getDateFormatSpace(date) {
        var dateObj = new Date(date)
        return dateObj.getDate() + ' ' + monthNameSmall[dateObj.getMonth()] + ' ' + dateObj.getFullYear()
    }

    $("#todayDate").text(' (' + getDateFormatSpace(new Date()) + ')');

    function sidebarHeight() {
        var sideHeight = $(document).height()
        $("#sidebarHeight").css({'min-height': sideHeight})
    }

    function getTimeZone() {
        var start = new Date()
        var zone = start.toTimeString().replace(/[()]/g, "").split(' ');
        if (zone.length > 3) {
            var tz = '';
            for (var i = 2; i < zone.length; i++) {
                tz += zone[i].charAt(0);
            }
            return tz || zone[2]
        }
        else return zone[2]
    }

    jQuery.nl2br = function (varTest) {
        return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
    };

    function removeDuplicates_id(arr,notId) {

        var end = arr.length;

        for (var i = 0; i < end; i++) {
            for (var j = i + 1; j < end; j++) {
                if (!notId ? arr[i]._id == arr[j]._id : arr[i] == arr[j]) {
                    var shiftLeft = j;
                    for (var k = j + 1; k < end; k++, shiftLeft++) {
                        arr[shiftLeft] = arr[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        var whitelist = [];
        for (var l = 0; l < end; l++) {
            whitelist[l] = arr[l];
        }

        return whitelist;
    }

    function updateNotificationCount(num){
        var current = $("#notificationsCount").text();
        num = parseInt(current)+parseInt(num);
        $("#notificationsCount").text(num);
    }

    $('#dashboard').on("click", function () {
        window.location.replace('/dashboard');
    });
    $('#calendar').on("click", function () {
        window.location.replace('/calendar');
    });

    $('#myDocuments').on("click", function () {
        window.location.replace('/docs');
    });
    $('#logOut').on("click", function () {
        window.location.replace('/logout');
    });

    $("#facebookShareImg").on("click", function () {
        $(".img").trigger("click")
    });

    $("#closeDashboardMessage").on("click", function () {
        $("#dashboardShowHide").fadeOut()
    });

    function mixpanelTrack(eventName){
        mixpanel.track(eventName,{page:window.location.href});
    }
    function mixpanelIncrement(eventName){
        mixpanel.people.increment(eventName);
    }
    $("#fbShare").on("click", function () {

        updateShareId();
    });

    $("#twitShare").on("click", function () {

        updateShareId();
    });

    $("#linkShare").on("click", function () {

        updateShareId();
    });

    function updateShareId() {
        if (!shareFlag) {
            $.ajax({
                url: '/updateShareId',
                type: 'GET',
                success: function (response) {
                    addProgress(15)
                    $("#shareImageId").attr("src", '/images/green/share.png');
                }
            })
        }
    }

});
