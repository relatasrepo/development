
$(document).ready(function(){

    getVideos();

    $( ".video-list" ).on("click","li.playThis", function(event) {
        socialShare($(this).attr('data-link'))
        $('.video iframe').attr('src',$(this).attr('data-link'))
        $( ".video-list li" ).removeClass("highlight-list")
        $(this).addClass("highlight-list");
    });

});

function socialShare(link){

    var list = '<li><a target="_blank" href="https://www.facebook.com/share.php?u='+encodeURIComponent(link)+'"><i class="fa fa-facebook"></i></a></li>'
    list = list+'<li><a target="_blank" href="https://twitter.com/intent/tweet?text='+encodeURIComponent(link)+'"><i class="fa fa-twitter"></i></a></li>'
    list = list+'<li><a target="_blank" href="https://www.linkedin.com/cws/share?url='+encodeURIComponent(link)+'"><i class="fa fa-linkedin"></i></a></li>'

    $('.social-sharing').hide().html(list).fadeIn('slow')
}


function getVideos() {

    var url = "/admin/sales-ai/get/urls"
    $.get(url,function (response) {
        var urls = response.data.urls;
        var list = '';
        var items = '';
        var defaultVideo = null;

        urls = urls.sort(function (a, b) {  return a.order - b.order;  });

        for(var i=0;i<urls.length;i++){

            if(urls[i].isDefault){
                defaultVideo = urls[i].url
                socialShare(urls[i].originalUrl)
            }

            items = '<div style="font-weight: 600;">'+(i+1) +'. '+ urls[i].title+'</div>'
            items = items+'<div>'+urls[i].description+'</div>'
            // items = items+'<div class="link">'+urls[i].url+'</div>'

            list = list+'<li class="playThis" data-link="'+urls[i].url+'">'+items+'</li>'
        }

        if(defaultVideo){
            $('.video iframe').attr('src',defaultVideo)
        }

        $('.video-list').hide().html(list).fadeIn('slow')

    });

}


function jsonToQueryString(json) {
    return '?' +
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}