$(document).ready(function(){
    var companyObject = JSON.parse($('#company-object-hidden').text());
    var AWScredentials,s3bucketVideo,s3bucketLogo,s3bucketDoc,videoUrl,logoUrl,docUrl;
    var videoName,logoName,docName;
    getCompanyInfo();
    getAWSCredentials();

    function getAWSCredentials() {
        $.ajax({
            url: '/getAwsCredentials',
            type: 'GET',
            datatye: 'JSON',
            success: function (credentials) {
                AWScredentials = credentials;

                AWS.config.update(credentials);
                AWS.config.region = credentials.region;
                s3bucketVideo = new AWS.S3({ params: {Bucket: credentials.companyVideosBucket} });
                s3bucketLogo = new AWS.S3({ params: {Bucket: credentials.companyLogosBucket} });
                s3bucketDoc = new AWS.S3({ params: {Bucket: credentials.bucket} });
                videoUrl = 'https://' + AWScredentials.companyVideosBucket + '.s3.amazonaws.com/';
                logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
            }
        })
    }

    $('#commitWeekHour').datetimepicker({
        datepicker:false,
        format:'H',
        onSelectDate: function (dp, $input){
            console.log(dp)
            console.log($input)
        }
    });

    function getCompanyInfo(){
        $.ajax({
            url:'/corporate/company/'+companyObject._id,
            type:'GET',
            datatype:'JSON',
            success:function(companyProfile){
                if(companyProfile){
                    companyObject = companyProfile;

                    $("#regionRequired").prop('checked', companyObject.opportunitySettings.regionRequired);
                    $("#productRequired").prop('checked', companyObject.opportunitySettings.productRequired);
                    $("#verticalRequired").prop('checked', companyObject.opportunitySettings.verticalRequired);
                    $("#sourceRequired").prop('checked', companyObject.opportunitySettings.sourceRequired);
                    $("#netGrossMargin").prop('checked', companyObject.netGrossMargin);
                    $("#rmNotif").prop('checked', companyObject.notificationForReportingManager);
                    $("#orgHeadNotif").prop('checked', companyObject.notificationForOrgHead);

                    if(companyObject && companyObject.logo && companyObject.logo.url){
                        $("#header-logo-edit").show()
                        $("#header-logo").hide();
                        $("#header-logo-edit").attr("src",companyObject.logo.url);
                    } else {
                        $("#header-logo-edit").hide()
                        $("#header-logo").show();
                    }
                    
                    if(companyProfile.geoLocations && companyProfile.geoLocations.length>0){
                        getRegions(companyProfile.geoLocations)
                    }

                    if(companyProfile.productList && companyProfile.productList.length>0){
                        getProductList(companyProfile.productList)
                    }

                    if(companyProfile.verticalList && companyProfile.verticalList.length>0){
                        getVerticalList(companyProfile.verticalList)
                    }

                    if(companyProfile.sourceList && companyProfile.sourceList.length>0){
                        getSourceList(companyProfile.sourceList)
                    }

                    if(companyProfile.closeReasons && companyProfile.closeReasons.length>0){
                        getListForDisplay(companyProfile.closeReasons,'#closeReasonsList')
                    }

                    getCurrency(companyProfile.currency)

                    $("#sfClientId").val(companyProfile.salesForceSettings.clientId || '');
                    $("#sfClientSecret").val(companyProfile.salesForceSettings.clientSecret || '');

                    $("#companyName").val(companyProfile.companyName || '');
                    $("#emailDomains").val(companyProfile.emailDomains.join());
                    $("#description").val(companyProfile.description || '');
                    $("#presentationTitle").val(companyProfile.presentationTitle || '');
                    $("#presentationType").val(companyProfile.presentationType || 'none');

                    if(checkRequired(companyProfile.socialInfo)){
                       $("#facebookPage").val(companyProfile.socialInfo.facebookPage || '')
                       $("#linkedinPage").val(companyProfile.socialInfo.linkedinPage || '')
                       $("#twitterPage").val(companyProfile.socialInfo.twitterPage || '')
                    }

                    if(checkRequired(companyProfile.quickStats)){
                        $("#website").val(companyProfile.quickStats.website || '');
                        $("#founded-in").val(companyProfile.quickStats.foundedIn || '');
                        $("#head-quarters").val(companyProfile.quickStats.headQuarters || '');
                        $("#industry").val(companyProfile.quickStats.industry || '');
                        $("#company-size").val(companyProfile.quickStats.companySize || '')
                        $("#usp").val(companyProfile.quickStats.usp || '');
                    }
                    /* VIDEOS */
                    if(checkRequired(companyProfile.videos) && companyProfile.videos.length){
                        $("#exist-video").show();
                        $("#add-video").hide();
                        $("#video-change").show();
                        var videoName = checkRequired(companyProfile.videos[0].videoName) ? companyProfile.videos[0].videoName : companyProfile.videos[0].url
                        $("#video-name").html(videoName+'&nbsp;&nbsp;&nbsp;')
                    }
                    else{
                        $("#exist-video").hide();
                        $("#video-change").hide();
                        $("#add-video").show();
                    }

                    /*FY*/

                    if(companyProfile.fyMonth){
                        $("#selectedMonth").text(companyProfile.fyMonth)
                        $("#month").val(companyProfile.fyMonth);
                    }

                    if(companyProfile.commitDay){
                        $("#commitWeekday").val(companyProfile.commitDay);
                    }

                    if(companyProfile.monthCommitCutOff){
                        $("#commitMonthly").val(companyProfile.monthCommitCutOff);
                    }

                    if(companyProfile.qtrCommitCutOff){
                        $("#commitQtrly").val(companyProfile.qtrCommitCutOff);
                    }

                    if(companyProfile.commitHour){
                        $("#commitWeekHour").val(companyProfile.commitHour);
                    }

                    if(companyProfile.documentNumber){
                        $("#documentNumber").val(companyProfile.documentNumber);
                    }

                    /* DOCUMENTS */
                    if(checkRequired(companyProfile.documents) && companyProfile.documents.length){
                        $("#exist-doc").show();
                        $("#add-doc").hide();
                        $("#doc-change").show();
                        var dName = checkRequired(companyProfile.documents[0].documentName) ? companyProfile.documents[0].documentName : companyProfile.documents[0].url;
                        $("#doc-name").html(dName+'&nbsp;&nbsp;&nbsp;')
                    }
                    else{
                        $("#exist-doc").hide();
                        $("#doc-change").hide();
                        $("#add-doc").show();
                    }

                    /* SLIDE */
                    if(checkRequired(companyProfile.slides) && companyProfile.slides.length){
                        $("#exist-slide").show();
                        $("#add-slide").hide();
                        $("#slide-change").show();
                        var slideName = checkRequired(companyProfile.slides[0].slideName) ? companyProfile.slides[0].slideName : companyProfile.slides[0].url;
                        $("#slide-name").html(slideName+'&nbsp;&nbsp;&nbsp;')
                    }
                    else{
                        $("#exist-slide").hide();
                        $("#slide-change").hide();
                        $("#add-slide").show();
                    }

                    if(checkRequired(companyProfile.logo) && checkRequired(companyProfile.logo.imageName)){
                        $("#header-logo-name").show()
                        $("#header-logo-name").html('&nbsp;&nbsp;&nbsp;'+companyProfile.logo.imageName+'&nbsp;&nbsp;&nbsp;&nbsp;');
                        // $("#header-logo").hide()
                        // $("#header-logo-edit").show()
                    }
                    else{
                        // $("#header-logo-name").hide();
                        // $("#header-logo-edit").hide()
                      //  $("#header-logo").val('Upload')
                    }

                    if(checkRequired(companyProfile.latestNews) && companyProfile.latestNews.length > 0){
                        var newsList = $("#latestNews input");
                        for(var news=0; news<companyProfile.latestNews.length; news++){
                            var n = $(newsList[news]).val(companyProfile.latestNews[news].news);
                        }
                    }
                }
            }
        })
    }

    $("#saveDetails").on('click',function(){
        var companyName = $("#companyName").val();
        var emailDomains = $("#emailDomains").val();
        var description = $("#description").val();
        var presentationTitle = $("#presentationTitle").val();
        var website = $("#website").val();
        var foundedIn = $("#founded-in").val();
        var headQuarters = $("#head-quarters").val();
        var industry = $("#industry").val();
        var companySize = $("#company-size").val();
        var presentationType = $("#presentationType").val();
        var usp = $("#usp").val();
        var newsList = $("#latestNews input");

        var newsArr = [];
        for(var news=0; news<newsList.length; news++){

            var n = $(newsList[news]).val();
            if(checkRequired(n)){
                newsArr.push({news:n});
            }
        }
        emailDomains = emailDomains.replace(/\s/g,'');
        var emailDomainsArr = [];
        if(checkRequired(emailDomains)){
            if(contains(emailDomains,',')){
                emailDomains = emailDomains.split(',');
                for(var d=0; d<emailDomains.length; d++) {
                    if (checkRequired(emailDomains[d])) {
                        emailDomainsArr.push(emailDomains[d])
                    }
                }
            }
            else{
                emailDomainsArr.push(emailDomains);
            }
        }

        if(emailDomainsArr.length > 0){
            if(checkRequired(companyName)){
                var obj = {
                    companyName:companyName,
                    companyId:companyObject._id,
                    emailDomains:emailDomainsArr,
                    description:description,
                    presentationTitle:presentationTitle,
                    presentationType:presentationType,
                    latestNews:newsArr,
                    socialInfo:{
                        facebookPage:$("#facebookPage").val(),
                        linkedinPage:$("#linkedinPage").val(),
                        twitterPage:$("#twitterPage").val()
                    },
                    quickStats:{
                        website:website,
                        foundedIn:foundedIn,
                        headQuarters:headQuarters,
                        industry:industry,
                        companySize:companySize,
                        usp:usp
                    }
                };
               validatePresentationType(obj)

            }
            else{
                alert("Please provide company name.");
            }
        }
        else{
            alert("Please provide email domains.");
        }
    });

    function validatePresentationType(obj){
        updateCompanyInfo(JSON.stringify(obj))
        // if(obj.presentationType != 'none'){
        //     switch (obj.presentationType){
        //         case 'video' : if(checkRequired(companyObject.videos) && companyObject.videos.length > 0){
        //                             updateCompanyInfo(JSON.stringify(obj))
        //                        }else alert('Please provide presentation Video');
        //             break;
        //         case 'document' : if(checkRequired(companyObject.documents) && companyObject.documents.length > 0){
        //                                 updateCompanyInfo(JSON.stringify(obj))
        //                           }else alert('Please provide presentation Document');
        //             break;
        //         case 'slide' : if(checkRequired(companyObject.slides) && companyObject.slides.length > 0){
        //                                 updateCompanyInfo(JSON.stringify(obj))
        //                        }else alert('Please provide presentation Slide');
        //             break;
        //     }
        // }else{
        //     updateCompanyInfo(JSON.stringify(obj));
        // }
    }

    function updateCompanyInfo(updateInfo){
        $.ajax({
            url:'/corporate/company/update',
            type:'POST',
            datatype:'JSON',
            data:{data:updateInfo},
            success:function(success){
                if(success){
                    alert('Company details successfully updated.');
                }
                else{
                    alert('An error occurred while updating company details.');
                }
            }
        })
    }

    function contains(str, subStr) {
        return str.indexOf(subStr) != -1;
    }

    $("#showcompanyBasic").addClass('selected');
    $("#showcompanyBasic").on('click',function(event){
        $("#showcompanyBasic").addClass('selected');
        $("#showcommitSettings").removeClass('selected');

        $("#commitSettings").hide()
        $("#companyBasic").show()
    })

    $("#showcommitSettings").on('click',function(event){
        $("#showcommitSettings").addClass('selected');
        $("#showcompanyBasic").removeClass('selected');

        $("#commitSettings").show()
        $("#companyBasic").hide()
    })

    /* Adding contacts */
    $("#add-contact").on('click',function(){
        var uniqueName_emailId = $("#unique-name").val();
        var contactFor = $("#contact-for").val();
        if(checkRequired(uniqueName_emailId) && checkRequired(contactFor)){
             var obj = {
                 companyId:companyObject._id,
                 uniqueName_emailId:uniqueName_emailId,
                 contactFor:contactFor
             };
            $.ajax({
                url:'/corporate/company/add/contact',
                type:'POST',
                datatype:'JSON',
                data:obj,
                traditional:true,
                success:function(result){
                    if(result){
                        alert("Contact successfully added.")
                    }
                    else{
                        alert("An error occurred. Please try again.");
                    }
                }
            })
        }
        else{
            alert("Either you missed Unique name or email id or contact for.")
        }
    });

    /* Adding Salesforce */
    $("#saveSfSettings").on('click',function(){
        var sfClientId = $("#sfClientId").val();
        var sfClientSecret = $("#sfClientSecret").val();
        if(checkRequired(sfClientSecret) && checkRequired(sfClientId)){
             var obj = {
                 companyId:companyObject._id,
                 clientId:sfClientId.replace(/\s/g,''),
                 clientSecret:sfClientSecret.replace(/\s/g,'')
             };
            $.ajax({
                url:'/corporate/company/add/salesforce',
                type:'POST',
                datatype:'JSON',
                data:obj,
                traditional:true,
                success:function(result){
                    if(result){
                        alert("Salesforce details successfully added.")
                    }
                    else{
                        alert("An error occurred. Please try again.");
                    }
                }
            })
        }
        else{
            alert("Either you missed Unique name or email id or contact for.")
        }
    });
    /* Adding contacts */

    /* Upload video */
    $("#video-upload-button").on('click',function(){
        $("#video-upload").trigger('click');
    });

    $("#video-change").on('click',function(){
        $("#exist-video").hide();
        $("#video-change").hide();
        $("#add-video").show();
    });

    $("#save-video-link").on('click',function(){
        var url = $("#video-link-box").val();
        //var name = $("#video-name-box").val();

        if(checkRequired(url)){
            if(contains(url,'https://www.youtube.com/watch?v')){
                var regex = /[?&]([^=#]+)=([^&#]*)/g,
                    params = {},
                    match;
                while(match = regex.exec(url)) {
                    params[match[1]] = match[2];
                }
                if(checkRequired(params.v) || checkRequired(params.V)){
                    url = 'https://www.youtube.com/embed/'+params.v || params.V
                }
            }

            var video = {
                companyId:companyObject._id,
                videoName:'',
                sourceType:'link',
                url:url,
                awsKey:null
            }
            $.ajax({
                url:'/corporate/company/add/video',
                type:'POST',
                datatype:'JSON',
                data:video,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#exist-video").show();
                        $("#add-video").hide();
                        $("#video-name").html(url+'&nbsp;&nbsp;&nbsp;&nbsp;');
                        $("#video-change").show()
                        getCompanyInfo()
                        alert('Video successfully updated');
                    }
                    else{
                        getCompanyInfo()
                        alert("An error occurred. Please try again.")
                        $("#exist-video").hide();
                        $("#add-video").show();
                    }
                }
            })
        }
        else alert("You missed either url or name of video");
    });

   var uploadFlag = false;
    $("#video-upload").on('change',function(){
        var file = $(this)[0].files[0];
        if(checkRequired(file)){
            if (file) {

                var dateNow = new Date();
                var d = dateNow.getDate();
                var m = dateNow.getMonth();
                var y = dateNow.getFullYear();
                var hours = dateNow.getHours();
                var min = dateNow.getMinutes();
                var sec = dateNow.getSeconds();
                var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
                var dIdentity = timeStamp+'_'+file.name;
                var docNameTimestamp = dIdentity.replace(/\s/g,'');
                videoName = file.name;
                $("#exist-video").show();
                $("#add-video").hide();
                uploadFlag = false;
                var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
                var request = s3bucketVideo.putObject(params);
                $("#video-change").hide()
                request.on('httpUploadProgress', function (progress) {
                    var pr = Math.round(progress.loaded / progress.total *100);
                    $("#video-name").html('('+pr+'%)'+file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    if(pr == 100){
                        $("#video-name").html(file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    }
                });
                request.send(function (err, data) {
                    if(err){
                        uploadFlag = true;
                        $("#exist-video").hide();
                        $("#add-video").show();
                        alert("Video upload failed")
                    }
                    else{
                        uploadFlag = true;
                        videoUrl = videoUrl+docNameTimestamp;
                        var video = {
                            companyId:companyObject._id,
                            videoName:videoName,
                            sourceType:'video',
                            url:videoUrl,
                            awsKey:docNameTimestamp
                        }

                        $.ajax({
                            url:'/corporate/company/add/video',
                            type:'POST',
                            datatype:'JSON',
                            data:video,
                            traditional:true,
                            success:function(result){
                                if(result){
                                    alert('Video successfully uploaded');
                                    videoUrl = 'https://' + AWScredentials.companyVideosBucket + '.s3.amazonaws.com/';
                                }
                                else{
                                    alert("Video upload failed")
                                    $("#exist-video").hide();
                                    $("#add-video").show();
                                    videoUrl = 'https://' + AWScredentials.companyVideosBucket + '.s3.amazonaws.com/';
                                }
                            }
                        })
                    }
                });

            }
            else {
               alert("Please select file to upload.");
            }
        }
    });
    /* Upload video End*/

    /* Upload file */
    $("#file-input-upload").on('click',function(){
        $("#file-upload").trigger('click');
    });

    $("#file-input-change").on('click',function(){
        $("#file-upload").trigger('click');
    });

    $("#setFYMonth").on('click',function(){
        var month = $("#month").val()

        $.ajax({
            url:'/corporate/company/set/fym',
            type:'POST',
            datatype:'JSON',
            data:{month:month},
            traditional:true,
            success:function(result){
                getCompanyInfo()
            }
        })
    });

    $("#setCommitWeekday").on('click',function(){
        var commitWeekday = $("#commitWeekday").val()
        var commitWeekHour = $("#commitWeekHour").val()

        $.ajax({
            url:'/corporate/company/set/commitweek',
            type:'POST',
            datatype:'JSON',
            data:{commitWeekday:commitWeekday,commitWeekHour:commitWeekHour},
            traditional:true,
            success:function(result){
                alert("Commit day updated successfully.");
                getCompanyInfo()
            }
        })
    });

    $("#setCommitMonthly").on('click',function(){
        var commitMonthly = $("#commitMonthly").val()

        $.ajax({
            url:'/corporate/company/set/commit/month',
            type:'POST',
            datatype:'JSON',
            data:{commitMonthly:parseInt(commitMonthly)},
            traditional:true,
            success:function(result){
                alert("Month Commit cutoff day updated successfully.");
                getCompanyInfo()
            }
        })
    });

    $("#setCommitQtrly").on('click',function(){
        var commitQtrly = $("#commitQtrly").val()

        $.ajax({
            url:'/corporate/company/set/commit/qtr',
            type:'POST',
            datatype:'JSON',
            data:{commitQtrly:parseFloat(commitQtrly)},
            traditional:true,
            success:function(result){
                alert("Quarter Commit cutoff day updated successfully.");
                getCompanyInfo()
            }
        })
    });

    $("#file-upload").on('change',function(){
        var file = $(this)[0].files[0];

    });
    /* Upload file */

    /* Upload logo */

    $("#header-logo-edit").on('click',function(){
        $(this).hide()
        $("#header-logo-name").hide();
        $("#header-logo").show();
    });

    $("#header-logo").on('click',function(){
        $("#header-logo-upload").trigger('click');
    });

    $("#header-logo-upload").on('change',function(){
        var file = $(this)[0].files[0];

        if (file) {
            var url = URL.createObjectURL(this.files[0]);
            var img = new Image;
            img.onload = function() {
                uploadEventImage(file);
            };
            img.src = url;
        }
        else {
            alert("Please select file to upload.");
        }
    });

    function uploadEventImage(file){
        var dateNow = new Date();
        var d = dateNow.getDate();
        var m = dateNow.getMonth();
        var y = dateNow.getFullYear();
        var hours = dateNow.getHours();
        var min = dateNow.getMinutes();
        var sec = dateNow.getSeconds();
        var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
        var dIdentity = timeStamp+'_'+file.name;
        var docNameTimestamp = dIdentity.replace(/\s/g,'');
        docName = file.name;
        $("#header-logo-name").show()
        var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
        var request = s3bucketLogo.putObject(params);
        request.on('httpUploadProgress', function (progress) {
            var pr = Math.round(progress.loaded / progress.total *100);
            $("#header-logo-name").html('('+pr+'%)'+file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
            if(pr == 100){
                $("#header-logo-name").html(file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
            }
        });
        request.send(function (err, data) {
            if(err){
                uploadFlag = true;
                logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                $("#header-logo-name").hide()
                alert("An error occurred. Please try again.")

            }
            else{
                logoUrl = logoUrl+docNameTimestamp;
                var logo = {
                    companyId:companyObject._id,
                    imageName:file.name,
                    sourceType:'image',
                    url:logoUrl,
                    awsKey:docNameTimestamp
                }

                setTimeout(function () {
                    getCompanyInfo();
                },100)

                $.ajax({
                    url:'/corporate/company/add/logo',
                    type:'POST',
                    datatype:'JSON',
                    data:logo,
                    traditional:true,
                    success:function(result){
                        if(result){
                            $("#header-logo").val('Change');
                            alert('Logo successfully uploaded');
                            logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                        }
                        else{
                            $("#header-logo-name").hide();
                            $("#header-logo").val('Upload');
                            // alert("Logo upload failed");

                            logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                        }
                    }
                })
            }
        });
    }

    /* Upload logo */

    /* Upload Document */
    $("#doc-upload-button").on('click',function(){
        $("#doc-upload").trigger('click');
    });

    $("#doc-change").on('click',function(){
        $("#exist-doc").hide();
        $("#doc-change").hide();
        $("#add-doc").show();
    });

    var uploadFlagDoc = false;
    $("#doc-upload").on('change',function(){
        var file = $(this)[0].files[0];

        if(checkRequired(file)){
            if (file && file.type == "application/pdf") {

                var dateNow = new Date();
                var d = dateNow.getDate();
                var m = dateNow.getMonth();
                var y = dateNow.getFullYear();
                var hours = dateNow.getHours();
                var min = dateNow.getMinutes();
                var sec = dateNow.getSeconds();
                var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
                var dIdentity = timeStamp+'_'+file.name;
                var docNameTimestamp = dIdentity.replace(/\s/g,'');
                docName = file.name;
                $("#exist-doc").show();
                $("#add-doc").hide();
                uploadFlagDoc = false;
                var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
                var request = s3bucketDoc.putObject(params);
                $("#doc-change").hide()
                request.on('httpUploadProgress', function (progress) {
                    var pr = Math.round(progress.loaded / progress.total *100);
                    $("#doc-name").html('('+pr+'%)'+file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    if(pr == 100){
                        $("#doc-name").html(file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    }
                });
                request.send(function (err, data) {
                    if(err){
                        uploadFlag = true;
                        $("#exist-doc").hide();
                        $("#add-doc").show();
                        alert("Document upload failed")
                    }
                    else{
                        uploadFlag = true;
                        docUrl = docUrl+docNameTimestamp;
                        var doc = {
                            companyId:companyObject._id,
                            documentName:docName,
                            url:docUrl,
                            awsKey:docNameTimestamp
                        };

                        $.ajax({
                            url:'/corporate/company/add/document',
                            type:'POST',
                            datatype:'JSON',
                            data:doc,
                            traditional:true,
                            success:function(result){
                                if(result){
                                    alert('Document successfully uploaded');
                                    docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
                                }
                                else{
                                    alert("Document upload failed")
                                    $("#exist-doc").hide();
                                    $("#add-doc").show();
                                    docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
                                }
                            }
                        })
                    }
                });
            }
            else {
                alert("Please select (.pdf) file to upload.");
            }
        }
    });
    /* Upload Document End*/

    /* Add Slide */
    $("#slide-change").on('click',function(){
        $("#exist-slide").hide();
        $("#slide-change").hide();
        $("#add-slide").show();
    });

    $("#addRegion").on('click',function(){
        addRegion($("#region").val(),companyObject._id)
    });

    $("#addCloseReasons").on('click',function(){
        addReason($("#reason").val(),companyObject._id)
    });

    $("#addVertical").on('click',function(){
        addVertical($("#vertical").val(),companyObject._id)
    });

    $("#addProduct").on('click',function(){
        addProduct($("#product").val(),companyObject._id)
    });

    $("#addSource").on('click',function(){
        addSource($("#source").val(),companyObject._id)
    });

    $("#addCurrency").on('click',function(){
        addCurrency($("#currency").val(),companyObject._id)
    });

    $("body").on('click', '#regionsList li', function (e) {
        if (confirm("Click OK to delete "+$(this).text())) {
            deleteRegion($(this).text())
        }
        return false;
    });

    $("body").on('click', '#verticalList li', function (e) {
        if (confirm("Click OK to delete "+$(this).text())) {
            deleteVertical($(this).text())
        }
        return false;
    });

    $("body").on('click', '#productList li', function (e) {
        if (confirm("Click OK to delete "+$(this).text())) {
            deleteProduct($(this).text())
        }
        return false;
    });

    $("body").on('click', '#sourceList li', function (e) {
        if (confirm("Click OK to delete "+$(this).text())) {
            deleteSource($(this).text())
        }
        return false;
    });

    $("body").on('click', '#closeReasonsList li', function (e) {
        if (confirm("Click OK to delete "+$(this).text())) {
            deleteReason($(this).text())
        }
        return false;
    });

    $("body").on('click', '#regionRequired', function (e) {
        var isChecked = $('#regionRequired').is(":checked")

        $.ajax({
            url:'/corporate/region/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#productRequired', function (e) {
        var isChecked = $('#productRequired').is(":checked")

        $.ajax({
            url:'/corporate/product/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#sourceRequired', function (e) {
        var isChecked = $('#sourceRequired').is(":checked")

        $.ajax({
            url:'/corporate/source/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#verticalRequired', function (e) {
        var isChecked = $('#verticalRequired').is(":checked")

        $.ajax({
            url:'/corporate/vertical/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#netGrossMargin', function (e) {
        var isChecked = $('#netGrossMargin').is(":checked")

        $.ajax({
            url:'/corporate/netgrossmargin/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    alert("Successfully settings saved")
                }
            }
        });
    });

    $("body").on('click', '#orgHeadNotif', function (e) {
        var isChecked2 = $('#rmNotif').is(":checked")
        var isChecked = $('#orgHeadNotif').is(":checked")
        updateDetails({notificationForReportingManager:isChecked2,notificationForOrgHead:isChecked,companyId:companyObject._id});
    });
    
    $("body").on('click', '#rmNotif', function (e) {
        var isChecked2 = $('#rmNotif').is(":checked")
        var isChecked = $('#orgHeadNotif').is(":checked")
        updateDetails('/corporate/opp/email/notif',{notificationForReportingManager:isChecked2,notificationForOrgHead:isChecked,companyId:companyObject._id});
    });

    function updateDetails(url,updateObj){

        $.ajax({
            url:url,
            type:'POST',
            datatype:'JSON',
            data:updateObj,
            traditional:true,
            success:function(result){
                if(result){
                    alert("Successfully settings saved")
                }
            }
        });
    }

    function getRegions(geoLocations) {
        var li ='';
        for(var i=0;i<geoLocations.length;i++){
            li = li+"<li class='delete'>"+geoLocations[i].region+"</li>"
        }
        $("#regionsList").html(li)
    }

    function getProductList(products) {
        var li ='';
        for(var i=0;i<products.length;i++){
            li = li+"<li class='delete'>"+products[i].name+"</li>"
        }
        $("#productList").html(li)
    }

    function getSourceList(source) {
        var li ='';
        for(var i=0;i<source.length;i++){
            li = li+"<li class='delete'>"+source[i].name+"</li>"
        }
        $("#sourceList").html(li)
    }

    function getVerticalList(verticals) {
        var li ='';
        for(var i=0;i<verticals.length;i++){
            li = li+"<li class='delete'>"+verticals[i].name+"</li>"
        }
        $("#verticalList").html(li)
    }

    function getListForDisplay(item,id) {
        var li ='';
        for(var i=0;i<item.length;i++){
            li = li+"<li class='delete'>"+item[i].name+"</li>"
        }
        $(id).html(li)
    }

    $("#save-slide-link").on('click',function(){
        var url = $("#slide-link-box").val();
        //var name = $("#video-name-box").val();

        if(checkRequired(url) && contains(url,'slideshare')){

            var slide = {
                companyId:companyObject._id,
                slideName:'',
                sourceType:'link',
                url:url,
                awsKey:null
            };

            $.ajax({
                url:'/corporate/company/add/slide',
                type:'POST',
                datatype:'JSON',
                data:slide,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#exist-slide").show();
                        $("#add-slide").hide();
                        $("#slide-name").html(url+'&nbsp;&nbsp;&nbsp;&nbsp;');
                        $("#slide-change").show();

                        alert('Slide successfully updated');
                    }
                    else{

                        alert("An error occurred. Please try again.")
                        $("#exist-slide").hide();
                        $("#add-slide").show();
                    }
                }
            })
        }
        else alert("You missed either url or name of Slide");
    });

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }
    
    function addRegion(region,companyId) {
        var geoLocations = {};
        geoLocations.region = region;
        geoLocations.companyId = companyId;

        if(region){
            $.ajax({
                url:'/corporate/geolocation/add',
                type:'POST',
                datatype:'JSON',
                data:geoLocations,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#region").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        }
    }

    function addReason(reason,companyId) {
        var geoLocations = {};
        geoLocations.reason = reason;
        geoLocations.companyId = companyId;

        if(reason){
            $.ajax({
                url:'/corporate/close/reason/add',
                type:'POST',
                datatype:'JSON',
                data:geoLocations,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#reason").val("")
                        getCompanyInfo()
                    }
                }
            });
        }
    }
    
    function addProduct(product,companyId) {
        var products = {};
        products.product = product;
        products.companyId = companyId;

        if(product){
            $.ajax({
                url:'/corporate/product/add',
                type:'POST',
                datatype:'JSON',
                data:products,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#product").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        }
    }

    function addSource(product,companyId) {
        var products = {};
        products.source = product;
        products.companyId = companyId;

        if(product){
            $.ajax({
                url:'/corporate/source/add',
                type:'POST',
                datatype:'JSON',
                data:products,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#source").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        }
    }

    function addVertical(vertical,companyId) {
        var verticals = {};
        verticals.vertical = vertical;
        verticals.companyId = companyId;

        if(product){
            $.ajax({
                url:'/corporate/vertical/add',
                type:'POST',
                datatype:'JSON',
                data:verticals,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#vertical").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        }
    }

    function addCurrency(currency,companyId) {
        var currency = {
            currency:currency,
            companyId:companyId
        };

        if(currency){
            $.ajax({
                url:'/corporate/currency/update',
                type:'POST',
                datatype:'JSON',
                data:currency,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#currency").val("")
                        fetchCompanyInfoAgain("currency")
                    }
                }
            });
        }
    }

    function deleteRegion(region) {

        $.ajax({
            url:'/corporate/geolocation/remove',
            type:'POST',
            datatype:'JSON',
            data:{region:region,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    fetchCompanyInfoAgain()
                }
            }
        });
    }
    
    function deleteSource(source) {

        $.ajax({
            url:'/corporate/source/remove',
            type:'POST',
            datatype:'JSON',
            data:{source:source,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    fetchCompanyInfoAgain()
                }
            }
        });
    }

    function deleteReason(reason) {

        $.ajax({
            url:'/corporate/close/reason/remove',
            type:'POST',
            datatype:'JSON',
            data:{reason:reason,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    getCompanyInfo()
                }
            }
        });
    }

    function deleteVertical(vertical) {

        $.ajax({
            url:'/corporate/vertical/remove',
            type:'POST',
            datatype:'JSON',
            data:{vertical:vertical,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    fetchCompanyInfoAgain()
                }
            }
        });
    }

    function deleteProduct(product) {

        $.ajax({
            url:'/corporate/product/remove',
            type:'POST',
            datatype:'JSON',
            data:{product:product,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    fetchCompanyInfoAgain()
                }
            }
        });
    }

    function fetchCompanyInfoAgain(functionType) {
        $.ajax({
            url: '/corporate/company/' + companyObject._id,
            type: 'GET',
            datatype: 'JSON',
            success: function (companyProfile) {
                if (companyProfile) {
                    companyObject = companyProfile;
                    if(!functionType){
                        if (companyProfile.geoLocations && companyProfile.geoLocations.length > 0) {
                            getRegions(companyProfile.geoLocations)
                            getProductList(companyProfile.productList)
                            getSourceList(companyProfile.sourceList)
                            getVerticalList(companyProfile.verticalList)
                        }
                    } else if(functionType === "currency") {
                        getCurrency(companyProfile.currency)
                    }
                }
            }
        });
    }

    function getCurrency(currency){
        if(currency){
            $("#currencyList").html(currency)
        } else {
            $("#currencyList").html("USD")
        }
    }

});
