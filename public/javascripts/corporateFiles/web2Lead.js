$(document).ready(function(){

    var companyObject = JSON.parse($('#company-object-hidden').text());
    var companyId = companyObject._id;

    var widgetHtml = '<div id="relatas-weblead-container"></div>'
    var widgetJS = '<script async type="text/javascript" data-relatas_company_id="'+companyId+'" src="https://relatas.com/rWebLead/load.js?"></script>';

    $('#relatas-js-include').val(widgetJS);
    $('#relatas-html-include').val(widgetHtml);

    getSupportTeam(companyId);
    getWidgetItems(companyId)

    var typingTimer;
    var doneTypingInterval = 1000;
    var imgs = '';
    $('.supportTeam').keyup(function(){
        clearTimeout(typingTimer);
        var searchFor = $(".supportTeam").val();

        if (searchFor.length>3) {
            typingTimer = setTimeout(function(){
                searchTeamMates(searchFor,imgs)
            }, doneTypingInterval);
        } else {
            $('#team-support').hide();
        }
    });

    $("body").on('click', '#team-support ul li', function (e) {

        var obj = $(this).text().split(", ")
        var emailId = null;
        for(var i=0;i<obj.length;i++) {
            if(validateEmail(obj[i])){
                emailId = obj[i]
            }
        }

        if(emailId){
            $.post("/corporate/admin/web2Lead/add/to/team",{companyId:companyId,emailId:emailId},function (response) {
                if(response){
                    getSupportTeam(companyId);
                }
            });
        }
    });

    $("body").on('click', '#saveConfiguration', function (e) {

        var updateObj = {
            "meetingTitle": $('#meetingTitle').val(),
            "failureReply": $('#failureReply').val(),
            "successReply": $('#successReply').val(),
            "mailSubject": $('#mailSubject').val(),
            "welcomeTitle": $('#welcomeTitle').val(),
            "majorColor":$('#majorColor').val(),
            "textColor":$('#textColor').val()
        }

        var saveObj = {
            updateObj:updateObj,
            companyId:companyId
        }

        $.post("/corporate/rweblead/messages",saveObj,function (response) {
            if(response){
                $("#flash-messages").show().html("Saved!").delay(3000).fadeOut()
            }
        });
    })

    $("body").on('click', '.current-support-team ul li', function (e) {

        var obj = $(this).text().split(", ")
        var emailId = null;
        for(var i=0;i<obj.length;i++) {
            if(validateEmail(obj[i])){
                emailId = obj[i]
            }
        }

        if (confirm("Remove from support team list? ")) {

            if(emailId){
                $.post("/corporate/admin/web2Lead/remove/from/team",{companyId:companyId,emailId:emailId},function (response) {
                    if(response){
                        getSupportTeam(companyId);
                    }
                });
            }
        }
        return false;
    });

    function searchTeamMates(searchFor,imgs) {
        $.get( "/rweblead/search/team/mates?searchFor="+searchFor+"&companyId="+companyId, function( data ) {
            
            for(var i=0;i<data.length;i++) {
                var obj = {
                    image: "/getImage/" + data[i]._id,
                    name: data[i].firstName+' '+data[i].lastName,
                    // name: data[i].firstName,
                    designation: data[i].designation,
                    mobileNumber: data[i].mobileNumber,
                    emailId: data[i].emailId
                }

                var li = '<li id="emailId">'+
                    '<img src=" '+obj.image+'">'+
                    '<span>'+obj.name+', <span>'+obj.designation+'</span>' +'</span>'+
                        '<span style="display: none">, '+obj.emailId+'</span>'
                    '</li>';
                imgs = imgs+li
            }

            var template = '<ul>'+imgs+'</ul>'
            $('#team-support').hide().html(template).fadeIn('fast');

        });
    }

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    function getWidgetItems(companyId) {
        $.get( "/corporate/company/"+companyId, function( data ) {
            if(data){
                $('#meetingTitle').val(data.meetingTitle)
                $('#failureReply').val(data.failureReply)
                $('#successReply').val(data.successReply)
                $('#mailSubject').val(data.mailSubject)
                $('#welcomeTitle').val(data.welcomeTitle);
                $('#majorColor').val(data.majorColor);
                $('#textColor').val(data.textColor);
            }

        })
    }

    function getSupportTeam(companyId) {
        $.get( "/rweblead/fetch/support/team?companyId="+companyId, function( data ) {
            console.log(data)

            if(data) {

                var imgs = '';
                for(var i=0;i<data.length;i++){
                    var obj = {
                        image:"/getImage/"+data[i]._id,
                        name: data[i].firstName+' '+data[i].lastName,
                        designation:data[i].designation,
                        mobileNumber:data[i].mobileNumber,
                        emailId:data[i].emailId
                    }

                    var li = '<li>'+
                        '<img src=" '+obj.image+'">'+
                        '<p>'+obj.name+'<br>'+'<span>'+obj.designation+'</span>'+'</p>'+
                        '<span style="display: none">, '+obj.emailId+'</span>'+
                        '</li>';
                    imgs = imgs+li
                }

                var template = '<ul>'+imgs+'</ul>'

                $('.current-support-team').hide().html(template).fadeIn('slow');
            } else {
                $('.current-support-team').html('<span style="margin-bottom: 10px;display: block;">No Support team setup.</span>')
            }
        });
    }

});

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}