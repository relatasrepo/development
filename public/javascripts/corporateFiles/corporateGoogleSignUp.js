$(document).ready(function(){
    var company,companyProfile;
    var facebookUserData,twitterUserData,linkedinUserData;
    $.ajax({
        url:'/corporate/getCompanyName',
        type:'GET',
        success:function(response){
            if(validateRequired(response)){
                companyProfile = response;
                company = response.companyName;
            }
        }
    });

    var emailId;
    $("#changeProfileImageBut").on("click",function(){
        storeToSession()
        $("#changeProfileImage").trigger('click');
    });

    $("#changeProfileImage").on("change",function(){
        $("#changeProfileImgForm").submit()

    });
    getSession();
    var googleData;
    var googleUserData;
    bindUserInformationToUi(false);

    var imageUrl;
    function getProfileImageUrl(){
        $.ajax({
            url:'/getProfileImageUrl',
            type:'GET',
            success:function(response){
                if(validateRequired(response)){
                    imageUrl = response;

                    $("#profilePic").attr('src',response)
                }
            }
        })
    }

    function storeToSession(){
        var data = formData();

        $.ajax({
            url:'/corporate/signUp/storeSession',
            type:'POST',
            datatpe:'JSON',
            data:data,
            success:function(response){

            }
        })
    }

    function getSession(){

        $.ajax({
            url:'/corporate/signUp/getSession',
            type:'GET',
            datatpe:'JSON',
            success:function(response){

                if(validateRequired(response)){

                    if(validateRequired(response.firstName))
                        $('#firstName').val(response.firstName || '')

                    if(validateRequired(response.lastName))
                        $('#lastName').val(response.lastName || '')

                    if(validateRequired(response.publicProfileUrl))
                        $('#publicProfileUrl').val(response.publicProfileUrl || '')

                    if(validateRequired(response.emailId))
                        $('#emailId').val(response.emailId || '')

                    if(validateRequired(response.googleId))
                        $('#googleId').val(response.googleId || '')

                    $("#designation").val(response.designation || '');
                    $("#companyName").val(response.companyName || companyProfile.companyName);
                    $("#location").val(response.location || '');
                    $("#serviceLogin").val(response.serviceLogin || '');
                    $("#key").val(response.key || '');

                    $('#profilePic').attr('src', response.profilePicUrl || '/images/default.png');
                    imageUrl = response.profilePicUrl;
                    getProfileImageUrl();
                    /* session to ui */


                        $('#mobile').val(response.mobile || ''),
                        $("#timezone").val(response.timezone || 'Asia/Kolkata'),
                        $('#skypeId').val(response.skypeId || ''),
                        $('#day').val(response.day || 'Day'),
                        $('#month').val(response.month || 'Month'),
                        $('#year').val(response.year || 'Year'),
                        $('#profilePrivatePassword').val(response.profilePrivatePassword);
                    $('#profileDescription').val(response.profileDescription);
                    if(response.public == 'true' || response.public == true){
                        $('#public').prop('checked',true);
                    }
                    else{
                        $('#public').prop('checked',false);
                        $('#selectPublic').prop('checked',true);
                    }
                    $("#workHoursStart").val(response.workHoursStart || '00:00');
                    $("#workHoursEnd").val(response.workHoursEnd || '00:00');
                    $('#startTime1').val(response.startTime1);
                    $('#endTime1').val(response.endTime1);
                    $('#startTime2').val(response.startTime2);
                    $('#endTime2').val(response.endTime2);

                    if(response.profilePrivatePassword == ''){
                        $("#profilePrivate").prop('checked',false);
                    }
                    else{
                        $("#profilePrivate").prop('checked',true);

                    }

                    if(response.sendDailyAgenda == false){
                        $('#sendDailyAgenda').prop('checked',false);
                    }
                    else{
                        $('#sendDailyAgenda').prop('checked',true);
                    }

                    /* session to ui */

                }else{
                    bindUserInformationToUi(true);
                }
            }
        })
    }


// Function to bind user linkedin data to ui
    function bindUserInformationToUi(bind){

        $.ajax({
            url:'/corporate/googleLogin/getSession',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(data){
                googleData = data;

                if(bind){
                    $('#profilePic').attr('src', data._json.picture || '/images/default.png');
                    imageUrl = data._json.picture;
                    getProfileImageUrl();
                    $("#firstName").val(googleData._json.given_name || googleData._json.name || '');
                    $("#lastName").val(googleData._json.family_name || googleData._json.given_name || '');
                    $("#googleId").val(googleData._json.id);
                    $('#publicProfileUrl').val(googleData.displayName.replace(/\s/g,''));
                    checkIdentityAvailability( $('#publicProfileUrl').val())
                    var email = googleData.emails[0].value || '';
                    $("#emailId").val(email.toLowerCase());
                    $("#timezone").val('Asia/Kolkata')
                    $("#designation").val('');
                    $("#companyName").val(companyProfile.companyName);
                    $("#location").val('');
                    $("#serviceLogin").val('google');

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }
    var flag = false;
    function checkIdentityAvailability(identity){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity)
        }
        $.ajax({
            url:'/checkUrl',
            type:'POST',
            datatype:'JSON',
            data:details,
            traditional:true,
            success:function(result){

                if(result == true){
                    $("#publicProfileUrl").val(getValidRelatasIdentity(identity))
                    i = 1
                    if(flag){
                        flag = false;
                        showMessagePopUp("Your selected name is not available. Next best name suggested is:  "+identity+"",'tip')
                    }
                }
                else{
                    a(identity)
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }
    var i = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+i
        i++;
        flag = true;
        checkIdentityAvailability(identity)
    }


    $("#publicProfileUrl").focusout(function(){
        var text = $("#publicProfileUrl").val()
        checkIdentityAvailability(text)
    });

// Function to generate form data
    function formData(){
        var mail = $('#emailId').val();
        var data={
            'firstName':$('#firstName').val(),
            'lastName':$('#lastName').val(),
            'publicProfileUrl':$('#publicProfileUrl').val(),
            'googleId':$("#googleId").val(),
            'emailId':mail.toLowerCase(),
            'designation':$("#designation").val(),
            'companyName':$("#companyName").val(),
            'location':$("#location").val(),
            'timezone':$("#timezone").val(),
            'key':$("#key").val(),
            'serviceLogin':$("#serviceLogin").val(),
            'profilePicUrl':imageUrl || googleData._json.pictureUrl || '/profileImages/default.png',
            'mobile':$('#mobile').val(),
            'profilePrivatePassword':$('#profilePrivatePassword').val(),
            'public':$('#public').prop('checked'),
            'skypeId':$('#skypeId').val(),
            'day':$('#day').val() == 'Day' ? 0 : $('#day').val(),
            'month':$('#month').val() == 'Month' ? 0 : $('#month').val(),
            'year':$('#year').val() == 'Year' ? 0 : $('#year').val(),
            'profileDescription':$('#profileDescription').val(),
            'workHoursStart': $('#workHoursStart').val(),
            'workHoursEnd': $('#workHoursEnd').val(),
            'startTime1':$("#startTime1").val(),
            'endTime1':$("#endTime1").val(),
            'startTime2':$("#startTime2").val(),
            'endTime2':$("#endTime2").val(),
            'sendDailyAgenda':true
        };

        var date = moment().tz(data.timezone);
        data.tzName = data.timezone;
        data.tzZone = date.format("Z");

        return data;
    }

    /* Social account info */

    loadGoogleUserData();
    loadFacebookUserData();
    loadTwitterUserData();
    loadLinkedinUserData();

    function authenticateLinkedin(){
        if(validateRequired(linkedinUserData)){

        }else{
            storeToSession()
            window.location.replace('/linkedinGoogleLogin');
        }
    }

    function authenticateFacebook(){
        if(validateRequired(facebookUserData)){

        }else{
              storeToSession()
            window.location.replace('/FacebookGoogleLogin');
        }
    }

    function authenticateTwitter(){
        if(validateRequired(twitterUserData)){

        }else{
             storeToSession()
            window.location.replace('/twitterGoogleLogin');
        }
    }

    $("#addLinkedinAccountH3").on("click",function(){
        authenticateLinkedin()
    });

    $("#addFacebookAccountH3").on("click",function(){
        authenticateFacebook()
    });

    $("#addTwitterAccountH3").on("click",function(){
        authenticateTwitter()
    })

    $('#addLinkedinAccount').change(function(){
        if($('#addLinkedinAccount').prop('checked')) {
            authenticateLinkedin()
        }
        else{
            linkedinUserData = '';
            $.ajax({
                url:'/removeLinkedinProfile',
                type:'GET',
                success:function(isSuccess){
                    loadLinkedinUserData();
                }
            });
        }
    });

    $('#addFacebookAccount').change(function(){
        if($('#addFacebookAccount').prop('checked')) {
            authenticateFacebook()
        }
        else{
            facebookUserData = '';
            $.ajax({
                url:'/removeFacebookUser',
                type:'GET',
                success:function(isSuccess){
                    loadFacebookUserData();
                }
            });
        }
    });

    $('#addTwitterAccount').change(function(){
        if($('#addTwitterAccount').prop('checked')) {
            authenticateTwitter()
        }
        else{
            twitterUserData = '';
            $.ajax({
                url:'/removeTwitterUser',
                type:'GET',
                success:function(isSuccess){
                    loadTwitterUserData();
                }
            });
        }
    });

    function authenticateGoogle0(){
        if(validateRequired(googleUserData)){

        }else window.location.replace('/googleGoogleLogin');

    }
    function authenticateGoogle1(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[1])){

            } else  window.location.replace('/addAnotherGoogleAccount2Google');

        }else authenticateGoogle0()

    }
    function authenticateGoogle2(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[2])){

            }else  window.location.replace('/addAnotherGoogleAccount3Google');

        }else  authenticateGoogle0()


    }
    function authenticateGoogle3(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[3])){

            }else window.location.replace('/addAnotherGoogleAccount4Google');

        }else  authenticateGoogle0()
    }

    $("#primaryGmailAccount").on("click",function(){
        storeToSession()
        authenticateGoogle0()
    });

    $("#addAnotherGoogleAccount1Text").on("click",function(){
        storeToSession()
        authenticateGoogle1()

    });

    $("#addAnotherGoogleAccount2Text").on("click",function(){
        storeToSession()
        authenticateGoogle2()

    });

    $("#addAnotherGoogleAccount3Text").on("click",function(){
        storeToSession()
        authenticateGoogle3()

    });

    $('#addGooleAccount').change(function(){
        storeToSession()
        if($('#addGooleAccount').prop('checked')) {
            authenticateGoogle0()
        }
        else{
            googleUserData[0] = null;
            $.ajax({
                url:'/removeGoogleUser/0',
                type:'GET',
                success:function(isSuccess){
                    loadGoogleUserData();
                }
            });
        }
    });

    $('#addAnotherGoogleAccount1').change(function(){
        storeToSession()
        if($('#addAnotherGoogleAccount1').prop('checked')) {
            authenticateGoogle1()
        }
        else{
            googleUserData[1] = null;
            $.ajax({
                url:'/removeGoogleUser/1',
                type:'GET',
                success:function(isSuccess){
                    loadGoogleUserData();
                }
            });
        }
    });


    $('#addAnotherGoogleAccount2').change(function(){
        storeToSession()
        if($('#addAnotherGoogleAccount2').prop('checked')) {
            authenticateGoogle2()
        }
        else{
            googleUserData[2] = null;
            $.ajax({
                url:'/removeGoogleUser/2',
                type:'GET',
                success:function(isSuccess){
                    loadGoogleUserData();
                }
            });
        }
    });

    $('#addAnotherGoogleAccount3').change(function(){
        storeToSession()
        if($('#addAnotherGoogleAccount3').prop('checked')) {
            authenticateGoogle3()
        }
        else{
            googleUserData[3] = null;
            $.ajax({
                url:'/removeGoogleUser/3',
                type:'GET',
                success:function(isSuccess){
                    loadGoogleUserData();
                }
            });
        }
    });


    function removeNullValues(array){
        var dataArray = [];
        for(var i=0;i<array.length;i++){
            if(array[i]){
                dataArray.push(array[i]);
            }
        }
        return dataArray;
    }

    // *************************** social login onfo
    function loadGoogleUserData(){
        $.ajax({
            url:'/googleUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(data){

                if(data == null || data == undefined  || data == '' || data.error){
                    $('#addGooleAccountText').text('abc@abc.com');
                    $('#addAnotherGoogleAccount1Text').text('Add another Gmail Account');
                    $('#addAnotherGoogleAccount2Text').text('Add another Gmail Account');
                    $('#addAnotherGoogleAccount3Text').text('Add another Gmail Account');
                }
                else{

                    validateGoogleAccount(data,function(data){

                        googleUserData=removeNullValues(data);
                        if (googleUserData[0]) {
                            $("#addGooleAccount").attr('checked','checked');
                            $('#addGooleAccountText').text(googleUserData[0].emails[0].value);
                        }
                        else{
                            $('#addGooleAccountText').text('abc@abc.com');
                            $("#addGooleAccount").attr('checked',false);
                        }

                        if (googleUserData[1]) {
                            $("#addAnotherGoogleAccount1").attr('checked','checked')
                            $('#addAnotherGoogleAccount1Text').text(googleUserData[1].emails[0].value);
                        }
                        else{
                            $("#addAnotherGoogleAccount1").attr('checked',false)
                            $('#addAnotherGoogleAccount1Text').text('Add another Gmail Account');
                        }

                        if (googleUserData[2]) {
                            $("#addAnotherGoogleAccount2").attr('checked','checked')
                            $('#addAnotherGoogleAccount2Text').text(googleUserData[2].emails[0].value);
                        }
                        else{
                            $("#addAnotherGoogleAccount2").attr('checked',false)
                            $('#addAnotherGoogleAccount2Text').text('Add another Gmail Account');
                        }

                        if (googleUserData[3]) {
                            $("#addAnotherGoogleAccount3").attr('checked','checked')
                            $('#addAnotherGoogleAccount3Text').text(googleUserData[3].emails[0].value);
                        }
                        else{
                            $("#addAnotherGoogleAccount3").attr('checked',false)
                            $('#addAnotherGoogleAccount3Text').text('Add another Gmail Account');
                        }
                    })

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }


    function loadFacebookUserData(){

        $.ajax({
            url:'/facebookUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(data){
                if(data == null || data == undefined  || data == '' || data.error){
                    $('#addFacebookAccountH3').text('Add your Facebook Account');
                }
                else{
                    facebookUserData=data;
                    $('#addFacebookAccountH3').text(data.displayName);
                    $('#addFacebookAccount').attr('checked','checked');
                    $( "<style> #activeFacebook { display:block; }</style>" ).appendTo('head');

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }

    function loadTwitterUserData(){

        $.ajax({
            url:'/twitterUser',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(data){
                if(data == null || data == undefined  || data == '' || data.error){
                    $('#addTwitterAccountH3').text('Add your Twitter Account');
                }
                else{
                    twitterUserData=data;
                    $('#addTwitterAccountH3').text(data.displayName);
                    $('#addTwitterAccount').attr('checked','checked');
                    $( "<style> #activeTwitter { display:block; }</style>" ).appendTo('head');

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }

    function loadLinkedinUserData(){

        $.ajax({
            url:'/linkedinProfile',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(data){

                if(data == null || data == undefined  || data == '' || data.error){
                    $('#addLinkedinAccountH3').text('Add your Linkedin Account')
                }
                else{
                    linkedinUserData=data;
                    $('#addLinkedinAccountH3').text(data._json.firstName+" "+data._json.lastName);
                    $('#addLinkedinAccount').attr('checked','checked');
                    $( "<style> #activeLinkedin { display:block; }</style>" ).appendTo('head');

                }

            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })

    }
    // ***************************
//Function to validate if user accets same google account
    function validateGoogleAccount(data,callback){
        var end = data.length;

        for(var i = 0; i < end; i++)
        {
            for(var j = i + 1; j < end; j++)
            {
                if (data[i] == null || data[j] == null) {}else{
                    if(data[i].emails[0].value == data[j].emails[0].value)
                    {   showMessagePopUp("You already accepted "+data[i].emails[0].value+" account",'error')
                        // alert('You already accepted '+data[i].emails[0].value+' account');
                        var shiftLeft = j;
                        for(var k = j+1; k < end; k++, shiftLeft++)
                        {
                            data[shiftLeft] = data[k];
                        }
                        end--;
                        j--;
                    }
                }
            }
        }

        var whitelist = [];
        for(var i = 0; i < end; i++){
            whitelist[i] = data[i];
        }
        callback(whitelist);
    }


    /* Social account info */

    //On click event on saveDetailsButton
    $('#saveDetailsButton').on("click",function(){
        var data=formData();

        var startTime1 = $('#startTime1').val();
        var endTime1 = $('#endTime1').val();
        var startTime2 = $('#startTime2').val();
        var endTime2 = $('#endTime2').val();
        if($('#selectPublic').is(':checked')){
            if(validateRequired(startTime1) && validateRequired(endTime1) || validateRequired(startTime2) && validateRequired(endTime2)){
                var start1Arr;
                var end1Arr;
                var start2Arr;
                var end2Arr;
                var startDate1;
                var endDate1;
                var startDate2;
                var endDate2;
                var date = new Date();
                var flag1 = true;
                var flag2 = true;
                if(validateRequired(startTime1) && validateRequired(endTime1)){
                    data.startTime1 = startTime1;
                    data.endTime1   = endTime1;
                    start1Arr = startTime1.split(':');
                    end1Arr = endTime1.split(':');
                    startDate1 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(start1Arr[0]),parseInt(start1Arr[0]),0,0);
                    endDate1 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(end1Arr[0]),parseInt(end1Arr[0]),0,0);
                    if(endTime1 == '00:00' && startTime1 == '00:00'){

                    }else
                    if( startDate1 >= endDate1){

                        flag1 = false;
                    }
                }else{
                    data.startTime1 = '';
                    data.endTime1   = '';
                }
                if(validateRequired(startTime2) && validateRequired(endTime2)){
                    data.startTime2 = startTime2;
                    data.endTime2   = endTime2;
                    start2Arr = startTime2.split(':');
                    end2Arr = endTime2.split(':');
                    startDate2 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(start2Arr[0]),parseInt(start2Arr[0]),0,0);
                    endDate2 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(end2Arr[0]),parseInt(end2Arr[0]),0,0);
                    if(startTime2 == '00:00' && endTime2 == '00:00'){

                    }else
                    if(startDate2 >= endDate2){

                        flag2 = false;

                    }else if(startTime2 != '00:00' && endTime2 == '00:00'){
                        flag2 = false;
                    }
                }else{
                    data.startTime2 = '';
                    data.endTime2   = '';
                }

                if(flag1 == false || flag2 == false){

                    showMessagePopUp("Please select valid times",'error','30%')
                    $('#selectPublic').focus()
                    //alert('Please select valid times');
                }else afterChangeTime(data);
            }else{
                showMessagePopUp("Please select From and TO time for Time1 & Time2",'error','30%')
                $('#selectPublic').focus()
                // alert('Please select From and TO time for Time1 & Time2');
            }
        }else{
            validateWorkHours(data)
        }
    });

    function validateWorkHours(data){

        if(data.workHoursStart == '00:00' && data.workHoursEnd == '00:00'){
            afterChangeTime(data);
        }
        else{
            var workHoursStartArr = data.workHoursStart.split(':');
            var workHoursEndArr = data.workHoursEnd.split(':');
            var date = new Date();
            var start = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(workHoursStartArr[0]),parseInt(workHoursStartArr[1]),0,0);
            var end = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(workHoursEndArr[0]),parseInt(workHoursEndArr[1]),0,0);
            if(start >= end){
                showMessagePopUp("Please select valid work hours. Please have To(hrs) greater than From(hrs).",'error');
            }
            else{
                afterChangeTime(data);
            }
        }
    }

    function afterChangeTime(data){

        if($('#profilePrivate').prop('checked')){

            if(validateRequired(data.profilePrivatePassword)){
                onSaveFormData(data);
            }
            else{
                showMessagePopUp("You selected 'Make Profile Private', Please set access password",'error')
            }

        }else{
            data.profilePrivatePassword = '';
            onSaveFormData(data);
        }
    }

    // Function to create total user info as json
    function createNewUser(data){
        var user = {
            'firstName':data.firstName,
            'lastName'  :data.lastName,
            'publicProfileUrl':data.publicProfileUrl,
            'screenName':data.publicProfileUrl,
            'googleId':data.googleId,
            'designation':data.designation,
            'companyName':data.companyName,
            'location':data.location,
            'emailId':data.emailId.toLowerCase(),
            'registeredUser':true,
            'corporateUser':true,
            'companyId':companyProfile._id,
            'public':data.public,
            'key':data.key,
            'profilePicUrl':data.profilePicUrl,
            'serviceLogin':data.serviceLogin,
            'workHoursStart':data.workHoursStart,
            'workHoursEnd':data.workHoursEnd,
            'timezone':data.timezone,
            'tzName':data.tzName,
            'tzZone':data.tzZone,
            'profilePrivatePassword':data.profilePrivatePassword,
            'mobileNumber':data.mobile,
            'skypeId':data.skypeId,
            'profileDescription':data.profileDescription,
            'day':data.day,
            'month':data.month,
            'year':data.year
        };
        if(!validateRequired(user.companyName)){
            user.companyName = companyProfile.companyName;
        }

        if(validateRequired(data.startTime1) && validateRequired(data.endTime1) && validateRequired(data.startTime2) && validateRequired(data.endTime2)){
            user.startTime1 = data.startTime1;
            user.endTime1 = data.endTime1;
            user.startTime2 = data.startTime2;
            user.endTime2 = data.endTime2;
        }

        user.google=googleString()
        user.linkedin=linkedinString()
        user.facebook=facebookString()
        user.twitter=twitterString()

        return user;
    }

    function onSaveFormData(data){
        var result = validate(data);
        if(result == true){
            data.publicProfileUrl = getValidRelatasIdentity(data.publicProfileUrl)
            var details = {
                publicProfileUrl:data.publicProfileUrl
            }
            $.ajax({
                url:'/checkUrl',
                type:'POST',
                datatype:'JSON',
                data:details,
                traditional:true,
                success:function(result){
                    if(result == true){
                        checkEmailAddress(data)
                    }
                    else{
                        showMessagePopUp("Public profile url already exist in the Relatas, Please change it",'error');
                    }
                },
                error: function (event, request, settings) {

                },
                timeout: 30000
            })

        }else{
            result.focus();

        }
    }

    function checkEmailAddress(data){

        $.ajax({
            url:'/checkEmail/'+data.emailId,
            type:'GET',
            traditional:true,
            success:function(result){
                if(result == true){
                    var profileData = createNewUser(data);
                    emailId = profileData.emailId;

                    if(validateRequired(googleUserData)){
                        if(googleUserData[0]){
                            if ($('#acceptTerms').prop('checked')) {
                                saveFormData(profileData);
                            }
                            else{
                                $('#acceptTerms').focus();
                                showMessagePopUp("Please accept terms and conditions",'error')
                            }

                        }
                        else showMessagePopUp("Please add primary google account",'error')
                    }
                    else   showMessagePopUp("Please add primary google account",'error')
                }
                else{
                    showMessagePopUp("Email address already exist in the Relatas, Please try login with other account",'error')

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }
    var successFlag =false;
// Function to store form data in session
    function saveFormData(data){

        $.ajax({
            url: '/corporate/registration',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: data,
            success: function (msg) {
                if (msg == true) {
                    //identifyMixPanelUser(data)
                    successFlag = true;
                    updateInteractions();
                    getGoogleContacts();
                    showMessagePopUp("Congratulations your account has been created. Please add details to profile page and Connect your social networks to make your profile smart and productive.",'success')
                    //mixpanelTrack("Signup LinkedIn");
                }
                else if(msg == 'invalidKey'){
                    showMessagePopUp("Please provide a valid key.",'error');
                }else{
                    showMessagePopUp("Error occurred while saving, Please try again",'error');
                }

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }
    function updateInteractions(){
        $.ajax({
            url:'/interactions/email',
            type:'POST',
            datatype:'JSON',
            data:{accessToken:googleUserData[0].accessToken},
            success:function(response){

            }
        });
    }
    function getGoogleContacts(){
        $.ajax({
            url:'/addContacts/corporateUser/googleContacts',
            type:'POST',
            datatype:'JSON',
            data:{accessToken:googleUserData[0].accessToken},
            success:function(response){

            }
        });
    }

    //Functions to validate required feilds
    function validate(data){
        if(!validateRequired(data.firstName.trim())){
            showMessagePopUp("Please provide First name",'error');
            return $('#firstName');
        }else if(!validateRequired(data.lastName.trim())){
            showMessagePopUp("Please provide Last name",'error');
            return $('#lastName');
        }else  if(!validateRelatasIdentity(data.publicProfileUrl.trim())){
            showMessagePopUp("Please provide Unique Relatas identity",'error');
            return $('#publicProfileUrl');
        }else  if(!validateRequired(data.emailId.trim())){
            showMessagePopUp("An error occurred while saving data",'error');
            return $('#emailId');
        }else if (!validateEmailField(data.emailId)) {
            showMessagePopUp("Please provide valid email id",'error');
            return $('#emailId');
        }else if (!validateCorporateEmailId(data.emailId)) {
            showMessagePopUp("Please provide valid "+company+" ID",'error');
            return $('#emailId');
        }
        else if(!validateRequired(data.key)){
            showMessagePopUp("Please provide valid key.",'error');
            return $("#key");
        }
        else if(data.googleId == ''){
            showMessagePopUp("An error occurred while saving data",'error');
            return ("#emailId")
        }
        else if(!validateRequired(data.designation.trim())){
            showMessagePopUp("Please provide your designation",'error')

            return $('#designation');
        }
        else if(!validateRequired(data.companyName.trim())) {
            showMessagePopUp("Please provide Company name", 'error')

            return $('#companyName');
        }
        else if (!phone(data.mobile)) {
            showMessagePopUp("Please provide valid mobile number (+countrycode + 10 digit number)",'error','20%');
            return $('#mobile');
        }
        else if(!validateRequired(data.location)){
            showMessagePopUp("Please provide location",'error','20%');
            return $('#location');
        }
        else if(!validateRequired(data.timezone)){
            showMessagePopUp("Please provide Time Zone",'error','20%');
            return $('#timezone');
        }
        else return true;
    }
    $(".PhoneNo").bind("keypress", function (event) {
        if (event.charCode != 0) {
            var regex = new RegExp("^[0-9+-]*$");

            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });

    function phone(no){
        var targ=no.replace(/[^\d]/g,''); // remove all non-digits
        if(targ.length >=10 && targ.length <=15){
            return true;
        }
        else return false;
    }

    $('.PhoneNo').bind('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
        //alert('cut,copy & paste options are disabled !!');
    });

    $('#profileDescription').bind("keypress",function(event){
        var text = $('#profileDescription').val()
        var words = text.split(/[\s]+/);

        if(words.length >= 400){
            event.preventDefault();
            showMessagePopUp("You exceeded max limit of 400 words.",'error','45%')
            return false;
        }
    });

    function validateRelatasIdentity(rIdentity){

        var regex = new RegExp("^[a-zA-Z0-9._ ]*$");
        if(regex.test(rIdentity) && rIdentity != ''){
            return true;
        }
        else{
            return false;
        }
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g,'');
        return rIdentity;

    }

    function validateCorporateEmailId(emailId){

        var emailDomains = companyProfile.emailDomains;

        var eArr = emailId.split('@');
        var isValid = false;

        for(var domain=0; domain < emailDomains.length; domain++){
            if(eArr[1] == emailDomains[domain]){
                isValid =true;
            }
        }
        return isValid;
    }

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    // Function to validate required feild
    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':'25%'})
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
        //setTimeout(function(){
        $("#message").text(message)
        //},1000);
        $(".popover").focus();

    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');
        if(successFlag){
            successFlag = false;
            window.location.replace('/corporate/auth/'+emailId+'/'+companyProfile._id+'/updatePartial');
        }
    });
    // Function to generate valid json string for goole account
    function googleString(){

        var google ='';
        if (googleUserData == null || googleUserData == '' || googleUserData == undefined) {
            google +='{ "id":"",';
            google +='"token":"",';
            google +='"refreshToken":"",';
            google +='"name":""}';
        }
        else{
            for (var i = 0; i < googleUserData.length; i++) {
                if(googleUserData[i] == null || googleUserData[i]._json == undefined){}
                else{
                    var token=JSON.stringify(googleUserData[i].accessToken || '');
                    var id=JSON.stringify(googleUserData[i]._json.id || '');
                    var refreshToken=JSON.stringify(googleUserData[i].refreshToken || '');
                    var emailId=JSON.stringify(googleUserData[i].emails[0].value || '');
                    var name=JSON.stringify(googleUserData[i]._json.name || '');

                    google +='{ "id":'+id+',';
                    google +=' "token":'+token+',';
                    google +='"refreshToken":'+refreshToken+',';
                    google +='"emailId":'+emailId+',';
                    google +='"name":'+name+'';

                    if(i == googleUserData.length-1){
                        google +='}';
                    }
                    else{
                        google +='},';
                    }

                }
            }
        }
        var googleNew ="["+google+"]";
        return googleNew;
    }


    // Function to generate valid json string for linkedin account
    function linkedinString(){
        var linkedin='{';
        if(linkedinUserData == null || linkedinUserData == '' || linkedinUserData == undefined){
            linkedin +='"id":"",';
            linkedin +='"token":"",';
            linkedin +='"emailId":"",';
            linkedin +='"name":""';
        }else{
            var id=JSON.stringify(linkedinUserData._json.id || '');
            var token=JSON.stringify(linkedinUserData.accessToken || '');
            var emailId=JSON.stringify(linkedinUserData._json.emailAddress || '');
            var name=JSON.stringify(linkedinUserData._json.formattedName || '');

            linkedin +='"id":'+id+',';
            linkedin +='"token":'+token+',';
            linkedin +='"emailId":'+emailId+',';
            linkedin +='"name":'+name+'';
        }
        linkedin +='}';
        return linkedin;
    }

// Function to generate valid json string for facebook account
    function facebookString(){
        var facebook='{';
        if(facebookUserData == null || facebookUserData == '' || facebookUserData == undefined){
            facebook +='"id":"",';
            facebook +='"token":"",';
            facebook +='"emailId":"",';
            facebook +='"name":""';
        }else{
            var id=JSON.stringify(facebookUserData._json.id || '');
            var token=JSON.stringify(facebookUserData.accessToken || '');
            var emailId=JSON.stringify(facebookUserData._json.email || '');
            var name=JSON.stringify(facebookUserData._json.name || '');

            facebook +='"id":'+id+',';
            facebook +='"token":'+token+',';
            facebook +='"emailId":'+emailId+',';
            facebook +='"name":'+name+'';
        }
        facebook +='}';
        return facebook;
    }

    // Function to generate valid json string for twitter account
    function twitterString(){
        var twitter='{';
        if(twitterUserData == null || twitterUserData == '' || twitterUserData == undefined){
            twitter +='"id":"",';
            twitter +='"token":"",';
            twitter +='"refreshToken":"",';
            twitter +='"displayName":"",';
            twitter +='"userName":""';
        }else{
            var id=JSON.stringify(twitterUserData.id || '');
            var token=JSON.stringify(twitterUserData.accessToken || '');
            var displayName=JSON.stringify(twitterUserData.displayName || '');
            var userName=JSON.stringify(twitterUserData.username || '');
            var refreshToken=JSON.stringify(twitterUserData.refreshToken || '');

            twitter +='"id":'+id+',';
            twitter +='"token":'+token+',';
            twitter +='"refreshToken":'+refreshToken+',';
            twitter +='"displayName":'+displayName+',';
            twitter +='"userName":'+userName+'';
        }
        twitter +='}';
        return twitter;
    }
    function returnCompanyName(headline){
        if (!validateRequired(headline)) {

        }
        else{
            var details = headline.split(" at ");
            return details[1];
        }
    }

    function returnDesignation(headline){
        if (!validateRequired(headline)) {

        }
        else{
            var details = headline.split(" at ");
            return details[0];
        }
    }
});
