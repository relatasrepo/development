$(document).ready(function(){
    var companyObject = JSON.parse($('#company-object-hidden').text());
    var table,adminId;
    applyDataTable();
    getCompanyInfo();

    function getCompanyInfo(){
        $.ajax({
            url:'/corporate/company/'+companyObject._id,
            type:'GET',
            datatype:'JSON',
            success:function(companyProfile){
                if(companyProfile){

                    companyObject = companyProfile;
                    if(checkRequired(companyProfile) && checkRequired(companyProfile.admin) && companyProfile.admin.length > 0){
                        adminId = companyProfile.admin[0].adminId
                    }
                    getCurrentCompanyMembers();
                }
            }
        })
    }

    function getCurrentCompanyMembers(){
        $.ajax({
            url:'/corporate/admin/getMembers/company/'+companyObject._id,
            type:'GET',
            datatype:'JSON',
            success:function(response){
                createTableData(response)
            }
        })
    }

    function createTableData(members){

        if(members.length > 0){
            if(checkRequired(table)){
                table.clear().draw();
            }
            for(var i=0; i<members.length; i++){

                var name = members[i].firstName+' '+members[i].lastName;
                var parent = members[i].hierarchyParent;
                var treeLevel = 0;
                if(checkRequired(members[i].hierarchyPath)){
                    var path = members[i].hierarchyPath;
                    path = path.replace(/,/g," ");
                    path = path.trim().split(" ");
                    treeLevel = path.length;
                }
                var oldParent = checkRequired(parent) ? parent : null;
                var optionsHtml = '<option value="null">none</option>';
                var prentHtml ='';
                adminId = null;
                if(adminId != members[i]._id){
                    for(var j=0; j<members.length; j++){
                        if(members[i]._id != members[j]._id && members[j].hierarchyParent != members[i]._id.toString()){
                            var names = members[j].firstName+' '+members[j].lastName;
                            var val = members[i]._id+'_'+members[j]._id; // userId_parentId
                            if(parent && members[j]._id == parent){
                                prentHtml += '<option value='+val+'>'+names+'</option>';
                            }else{
                                optionsHtml += '<option value='+val+'>'+names+'</option>';
                            }
                        }
                    }
                }

                optionsHtml = prentHtml+optionsHtml;
                var arr = [
                     name,
                    members[i].emailId,
                    treeLevel,
                    '<select class="assign-to" oldParent='+oldParent+'>'+optionsHtml+'</select>',
                    members[i].userType == 'registered' ? 'YES' : 'NO'
                ];
                addRowsToTable(arr);
            }
        }
    }

    function validateAdmin(adminList,userId){

        if(checkRequired(adminList) && adminList.length > 0){
            var isAdmin = false;
            for(var i=0; i<adminList.length; i++){

                if(adminList[i].adminId == userId){
                    isAdmin = true;
                }
            }
            return isAdmin;
        }else return false;
    }

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    $("#clear-hierarchy").on('click',function(){
         $.ajax({
            url:'/corporate/admin/clearHierarchy',
            type:'POST',
            data:{companyId:companyObject._id},
            datatype:'JSON',
            success:function(response){
                if(response){
                    getCompanyInfo();
                    alert('Clearing hierarchy success.')
                }
                else alert('Clearing hierarchy failed.')
            }
        })
    });

    $("body").on("change",".assign-to",function(){


        var user_parent = $(this).val();

        user_parent = user_parent.split('_');

        var obj = {
             userId:user_parent[0],
             parentId:user_parent[1],
             oldParent:$(this).attr('oldparent'),
             action:'hierarchy-update',
             companyId:companyObject._id
        };

        updateHierarchy(obj)
    });

    function updateHierarchy(obj){
        $.ajax({
            url:'/corporate/admin/updateHierarchy',
            type:'POST',
            datatype:'JSON',
            data:obj,
            success:function(response){

                if(response){
                    getCompanyInfo();
                    alert('Hierarchy updated successfully.');
                }else{
                    alert('Updating hierarchy failed.');
                }
            }
        })
    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }


    function applyDataTable(){
        table = $('#people').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 1, "asc" ]],

            "oLanguage": {
                "sEmptyTable": "No Members Found"
            }
        });
    }

});