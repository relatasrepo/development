var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

function Communicator(){
    return {}
}

relatasApp.controller("documentDetails", function ($scope,$http,$rootScope){
    paintMenu($scope);
    getCompanyDetails($scope,$http);

    Communicator.prototype.refreshData = function () {
        getCompanyDetails($scope,$http);
    }

    $rootScope.isCorporateAdmin = true;

    $scope.selectMenu = function (item) {
        $scope.currentSelection = item.name;
        item.selected = "selected";
        resetOtherSelection($scope,item);

        if($scope.currentSelection == 'Delete Document Templates') {
            fetchDocumentTemplates($scope, $http);
        }
    }


    $scope.sortBy = function(tableHeaderName) {
        $scope.reverse = ($scope.tableHeaderName === tableHeaderName) ? !$scope.reverse : false;
        $scope.tableHeaderName = tableHeaderName;
    }

    $scope.toggleDocCategoryActivation = function (item, status) {
        item.companyId = $scope.companyObject._id;
        item.active = status;

        $http.post("/corporate/document/category/modify", item)
            .success(function (response) {
                _.each($scope.documentCategoryList, function(list) {
                    if(list.name == item.name)
                        list.active = status
                });
            });
    }

    $scope.addDocumentCategory = function (category) {

        if(category && !checkDuplicates($scope.documentCategoryList, category) ){

            $http.post("/corporate/document/category/add",{name:category, companyId:$scope.companyObject._id})
                .success(function (response) {
                    $scope.documentCategoryList.push({
                        name:category,
                        active: true
                    });
        
                    $scope.docCategory = " ";
                });
        }
        else {
            alert("Please enter a valid and unique value")
        }
    }

    $scope.deleteDocuments = function () {
        var documents = _.filter($scope.documentList,"select")

        if (window.confirm('This operation will permanently delete the documents. Are you sure you want to delete the selected documents?')) {
            $http.post("/corporate/admin/delete/documents",_.map(documents,"_id"))
                .success(function (response) {
                    alert("The selected documents were successfully deleted.")
                    $scope.fetchDocumentForUser($scope.selectedUser);
                })
        }
    }

    $scope.deleteDocumentTemplates = function () {
        var documentTemplates = _.filter($scope.documentTemplateList,"select")

        if (window.confirm('This operation will permanently delete the document templates. Are you sure you want to delete the selected document templates?')) {
            $http.post("/corporate/admin/delete/document/templates",_.map(documentTemplates,"_id"))
                .success(function (response) {
                    alert("The selected document templates were successfully deleted.")
                    fetchDocumentTemplates($scope, $http);
                })
        }
    }

    $scope.fetchDocumentForUser = function (userId) {
        $scope.selectedUser = userId;
        $scope.selectedUserFrom = $scope.teamObj[userId];

        $scope.selectAll = false;
        $scope.forOppRefresh = userId
        fetchDocuments($scope,$http,$scope.teamObj[userId]);
    }

    $scope.toggleSelectionDocuments = function () {
        $scope.documentList.forEach(function (el) {
            el.select = !el.select
        })
    }

    $scope.toggleSelectionDocumentTemplates = function () {
        $scope.documentTemplateList.forEach(function (el) {
            el.select = !el.select
        })
    }

    getTeamMembers($scope,$http);

});

function resetOtherSelection($scope,currentSelection) {
    $scope.menu.forEach(function (el) {
        if(el.name !== currentSelection.name){
            el.selected = ""
        }
    })
}

function getTeamMembers($scope,$http){

    $scope.data = {
        model: null
    };

    $http.get("/corporate/admin/all/team/members")
        .success(function (response) {
            $scope.data.teamMembers = response;

            $scope.teamObj = {};

            $scope.data.teamMembers.forEach(function (el) {
                el.name = el.firstName +" "+el.lastName;
                $scope.teamObj[el._id] = el;
            });

            $scope.data.teamMembers.sort(function (a,b) {
                if(a.firstName < b.firstName) return -1;
                if(a.firstName > b.firstName) return 1;
                return 0;
            })
        })
}

function paintMenu($scope) {

    $scope.menu = [{
        name:"Category",
        selected:"selected"
    }, {
        name:"Delete Document Templates",
        selected:""
    },{
        name:"Delete Documents",
        selected:""
    },]

    $scope.menu.sort(function (a,b) {
        if(a.name < b.name) return -1;
        if(a.name > b.name) return 1;
        return 0;
    })
}

function fetchDocuments($scope,$http,user) {
    $scope.loadingDocuments = true;
    $http.get("/corporate/admin/documents/get/all/data?userId="+user._id)
        .success(function (response) {
            $scope.documentList = response;
            console.log("Document Response:",response);
            $scope.loadingDocuments = false;
        })
}

function fetchDocumentTemplates($scope,$http) {
    $scope.loadingDocumentTemplates = true;
    $http.get("/documentTemplates/get/all/data")
        .success(function (response) {
            console.log("Document Templates:", response);

            $scope.documentTemplateList = response.Data;
            $scope.loadingDocumentTemplates = false;
        })
}

function getCompanyDetails($scope,$http){
    $scope.companyObject = JSON.parse($('#company-object-hidden').text());
    $http.get('/corporate/company/'+$scope.companyObject._id)
        .success(function (companyProfile) {
            if(!$scope.currentSelection){
                $scope.currentSelection = $scope.menu[0].name;
            }
            $scope.documentCategoryList =  companyProfile.documentCategoryList;
        })
}

function checkDuplicates(list,val){

    var contains = false;
    _.each(list,function (el) {
        if(el.name.toLowerCase() == val.toLowerCase()){
            contains = true;
        }
    })

    return contains;
}