var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.controller("assign_ownership", function ($scope, $http,$rootScope){

    var companyObject = JSON.parse($('#company-object-hidden').text());
    var companyId = companyObject._id;
    $rootScope.isCorporateAdmin = true;
    $scope.toggleSideBoard = function () {
        $scope.toggleDisplay = !$scope.toggleDisplay
    }

    // $scope.displayTeamMembersList = true;
    $scope.openTeamMembersList = function () {
        $scope.displayTeamMembersList = !$scope.displayTeamMembersList;
    }

    $scope.reportees = [];

    $scope.removeReportee = function (user) {
        $scope.reportees = $scope.reportees.filter(function (re) {
            return re.emailId != user.emailId
        })
    }

    $scope.addReportees = function (user) {
        if(!$scope.reportees){
            $scope.reportees = [];
        }

        if(user.selected){
            $scope.reportees.push(user)
        }

        _.uniqBy($scope.reportees,"emailId");
    }

    $scope.headers = ["Regions", "Products", "Verticals", "Business Units"];

    function getTeam(companyId,callback) {

        $http.get('/profile/get/current/web')
            .success(function (profile) {

                $scope.accessibleProperties = [];
                $rootScope.orgHead = null;
                if(profile.SuccessCode){

                    $scope.companyDetails = profile.companyDetails;

                    var url = "/get/all/team?companyId="+companyId;
                    $scope.teamList = [];

                    $http.get(url)
                        .success(function (response) {

                            //These arrays will be initialized and cached.
                            $scope.region = [];
                            $scope.vertical = [];
                            $scope.product = [];

                            $scope.regionDisabled = false;
                            $scope.prodDisabled = false;
                            $scope.verticalDisabled = false;

                            _.each($scope.companyDetails.geoLocations,function (el) {
                                $scope.region.push({
                                    name:el.region,
                                    selected:false,
                                    disabled:true,
                                    type:"regionOwner"
                                })
                            });

                            _.each($scope.companyDetails.productList,function (el) {
                                $scope.product.push({
                                    name:el.name,
                                    selected:false,
                                    disabled:true,
                                    type:"productTypeOwner"
                                })
                            })

                            _.each($scope.companyDetails.verticalList,function (el) {
                                $scope.vertical.push({
                                    name:el.name,
                                    selected:false,
                                    disabled:true,
                                    type:"verticalOwner"
                                })
                            });
                            $scope.businessUnits = [];

                            _.each($scope.companyDetails.businessUnits,function (el) {

                                $scope.businessUnits.push({
                                    name:el.name,
                                    // selected:_.includes(obj.businessUnits,el.name),
                                    selected:false,
                                    disabled:true,
                                    type:"buOwner"
                                })
                            });

                            $scope.userDictionary = {}

                            _.each(response,function (el) {

                                if(el.orgHead){
                                    $rootScope.orgHead = el._id
                                }

                                $scope.userDictionary[el._id] = el;

                                var obj = el;
                                obj.image = '/getImage/'+el._id
                                obj.fullName = el.firstName +" "+ el.lastName
                                obj.name = el.firstName +" "+ el.lastName

                                obj.regionList = []
                                obj.productList = []
                                obj.verticalList = []
                                obj.businessUnitsList = []

                                var lastRegion = el.regionOwner && el.regionOwner.length>3?el.regionOwner[2]+", ":""
                                var lastProd = el.productTypeOwner && el.productTypeOwner.length>3?el.productTypeOwner[2]+", ":""
                                var lastVertical = el.verticalOwner && el.verticalOwner.length>3?el.verticalOwner[2]+", ":""
                                var lastBu = el.businessUnits && el.businessUnits.length>3?el.businessUnits[2]+", ":""
                                _.each(el.regionOwner,function (item,index) {
                                    if(index<2){
                                        obj.regionList.push(item+", ")
                                    }
                                })

                                obj.regionList.push(lastRegion)

                                _.each(el.productTypeOwner,function (item,index) {
                                    if(index<2){
                                        obj.productList.push(item+", ")
                                    }
                                })

                                obj.productList.push(lastProd)

                                _.each(el.businessUnits,function (item,index) {
                                    if(index<2){
                                        obj.businessUnitsList.push(item+", ")
                                    }
                                })

                                obj.businessUnitsList.push(lastBu)

                                _.each(el.verticalOwner,function (item,index) {
                                    if(index<2){
                                        obj.verticalList.push(item+", ")
                                    }
                                })

                                obj.verticalList.push(lastVertical);

                                $scope.teamList.push(obj);
                            });

                            $scope.accessibleProperties.push({
                                name:"Regions",
                                items: $scope.region
                            },{
                                name:"Products",
                                items: $scope.product
                            },{
                                name:"Verticals",
                                items: $scope.vertical
                            },{
                                name:"Business Units",
                                items: $scope.businessUnits
                            });

                            $scope.teamMembers = response;
                            $scope.reportees = _.take($scope.teamMembers, 4);

                            if(callback){
                                callback(true)
                            }
                        });
                }
            });
    }

    getTeam(companyId);

    $scope.setCompanyTarget = function (user) {
        $scope.companyTargetAccess = user.companyTargetAccess;
    }

    $scope.showResults = false;
    $scope.background = ""

    $scope.closeModal = function () {
        $scope.background = ""
        $scope.showModal = false;
        getTeam(companyId,function () {
        });
    }

    $scope.allRegion = true;
    $scope.allProduct = true;
    $scope.allVertical = true;

    $scope.selectAll = function (type) {

        $scope.accessibleProperties.forEach(function (ac) {
            if(ac.name == type){
                ac.items.forEach(function (el) {
                    el.selected = ac.all;
                })
            }
        })

        if(type == 'region'){
            _.each($scope.region,function (el) {

                if(!el.disabled){
                    el.selected = $scope.allRegion
                }
            })
        }

        if(type == 'product'){
            _.each($scope.product,function (el) {
                if(!el.disabled){
                    el.selected = $scope.allProduct
                }
            })
        }

        if(type == 'vertical'){
            _.each($scope.vertical,function (el) {
                if(!el.disabled){
                    el.selected = $scope.allVertical
                }
            })
        }

    }

    $scope.saveSettings = function () {

        var settingsBulkUpdate = {
            productTypeOwner:[],
            regionOwner: [],
            verticalOwner: [],
            businessUnits: [],
            userId:$scope.userSelected._id
        }

        _.each($scope.region,function (el) {
            if(el.selected){
                settingsBulkUpdate.regionOwner.push(el.name)
            }
        })

        _.each($scope.product,function (el) {
            if(el.selected){
                settingsBulkUpdate.productTypeOwner.push(el.name)
            }
        })

        _.each($scope.vertical,function (el) {
            if(el.selected){
                settingsBulkUpdate.verticalOwner.push(el.name)
            }
        });

        _.each($scope.businessUnits,function (el) {
            if(el.selected){
                settingsBulkUpdate.businessUnits.push(el.name)
            }
        })

        settingsBulkUpdate.companyTargetAccess = $scope.companyTargetAccess;

        if(confirm("Are you sure you want to Grant these access?")){
            $http.post('/corporate/admin/add/opp/owner',settingsBulkUpdate)
                .success(function (response) {
                    getTeam(companyId,function () {
                        $scope.setAccess($scope.userDictionary[$scope.userSelected._id])
                    });
                });
        }

    }

    $scope.setAccessControl = function (value,settings,user) {

        if(value){

            // $http.post('/corporate/admin/add/opp/owner',settings)
            //     .success(function (response) {
            //         getTeam(companyId,function () {
            //             $scope.setAccess($scope.userDictionary[settings.userId])
            //         });
            //     });

        } else {

            var childrenReset = [];
            _.each($scope.teamMembers,function (el) {
                if(_.includes(el.hierarchyPath,settings.userId)){
                    childrenReset.push(el._id)
                }
            });

            settings.childrenReset = childrenReset;

            $http.post('/corporate/admin/remove/opp/owner',settings)
                .success(function (response) {
                });
        }
    }

    $scope.setAccess = function (user) {
        $scope.background = "blur";
        $scope.showModal = true;
        $scope.userSelected = user;

        var isOrgHead = false;

        if(user._id == $rootScope.orgHead){
            isOrgHead = true;
        }

        var parent = $scope.userDictionary[user.hierarchyParent];
        $scope.rm = parent;

        if(!parent){
            $scope.rm = {
                name:"None",
                emailId:"-"
            }
        }

        _.each($scope.region,function (obj) {
            obj.userId = user._id

            if(user.regionOwner && user.regionOwner.indexOf(obj.name) > -1){
                obj.selected = true;
            } else {
                obj.selected = false;
            }

            if(!isOrgHead && parent && parent.regionOwner && parent.regionOwner.indexOf(obj.name) > -1){
                obj.disabled = false
            } else {

                $scope.regionDisabled = true;
                obj.disabled = true;
            }

            if(isOrgHead){
                obj.disabled = false
                $scope.regionDisabled = false;
                // obj.selected = true;
            }

            if(!obj.selected){
                $scope.allRegion = false;
            }

        });

        _.each($scope.product,function (obj) {
            obj.userId = user._id

            if(user.productTypeOwner && user.productTypeOwner.indexOf(obj.name) > -1){
                obj.selected = true;
            } else {
                obj.selected = false;
            }

            if(!isOrgHead && parent && parent.productTypeOwner && parent.productTypeOwner.indexOf(obj.name) > -1){
                obj.disabled = false
            } else {
                obj.disabled = true;
                $scope.prodDisabled = true;
            }

            if(isOrgHead){
                obj.disabled = false
                $scope.prodDisabled = false;
                // obj.selected = true;
            }

            if(!obj.selected){
                $scope.allProduct = false;
            }

        });

        _.each($scope.vertical,function (obj) {
            obj.userId = user._id

            if(user.verticalOwner && user.verticalOwner.indexOf(obj.name) > -1){
                obj.selected = true;
            } else {
                obj.selected = false;
            }

            if(!isOrgHead && parent && parent.verticalOwner && parent.verticalOwner.indexOf(obj.name) > -1){
                obj.disabled = false
            } else {
                obj.disabled = true;
                $scope.verticalDisabled = true;
            }

            if(isOrgHead){
                obj.disabled = false
                $scope.verticalDisabled = false;
                // obj.selected = true;
            }

            if(!obj.selected){
                $scope.allVertical = false;
            }
        });

        $scope.accessibleProperties.forEach(function (ac) {
            var allAccess = true;
            _.each(ac.items,function (it) {
                if(ac.name == "Regions"){
                    it.selected = _.includes(user.regionOwner,it.name)
                }
                if(ac.name == "Products"){
                    it.selected = _.includes(user.productTypeOwner,it.name)
                }
                if(ac.name == "Verticals"){
                    it.selected = _.includes(user.verticalOwner,it.name)
                }
                if(ac.name == "Business Units"){
                    it.selected = _.includes(user.businessUnits,it.name)
                }

                if(!it.selected){
                    allAccess = false;
                }
            });

            ac.all = allAccess;
        });
    }

});

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResults">' +
            '<div ng-repeat="item in searchList track by $index"> ' +
            '<div class="clearfix cursor" title="[[item.fullName]], [[item.emailId]]" ng-click="addUser(item)">' +
            '<div class="pull-left relative">' +
            '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
            '<span ng-if="item.noPicFlag">' +
            '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
            // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
            '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
            '</div></div></div>'
    };
});

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else if(obj.name){
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                } else if(obj.fullName){

                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});