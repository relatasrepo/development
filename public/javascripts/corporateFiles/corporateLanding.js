$(document).ready(function(){
    var table,domainName;

    $.ajax({
        url:'/getDomainName',
        type:'GET',
        success:function(dName){
            domainName = dName;
        }
    });

    getCompanyProfile();
    applyDataTable();
    function getCompanyProfile(){

            $.ajax({
                url:'/corporate/company/profile/byUrl',
                type:'GET',
                datatype:'JSON',
                success:function(companyProfile){

                    if(companyProfile){
                        getCompanyContacts(companyProfile._id)
                        $("#description").text(companyProfile.description || '');
                        $("#presentationTitle").text(companyProfile.presentationTitle || '');

                        if(checkRequired(companyProfile.socialInfo)){
                            if(checkRequired(companyProfile.socialInfo.facebookPage)){
                                $("#facebook-share-content").attr('data-href',companyProfile.socialInfo.facebookPage || '');
                            }else $("#facebook-box").hide()

                            if(checkRequired(companyProfile.socialInfo.linkedinPage)){
                                $("#linkedin-share-content").attr('data-id',companyProfile.socialInfo.linkedinPage || '')
                            }else $("#linkedin-box").hide()

                            if(checkRequired(companyProfile.socialInfo.twitterPage)){
                                $("#twitter-share-content").attr('href',companyProfile.socialInfo.twitterPage || '')
                            }else $("#twitter-box").hide()

                        }
                        else{
                            $("#twitter-box").hide()
                            $("#linkedin-box").hide()
                            $("#facebook-box").hide()
                        }

                        if(checkRequired(companyProfile.quickStats)){
                            $("#website").text(companyProfile.quickStats.website || '');
                            $("#website").attr('href',getValidUrl(companyProfile.quickStats.website));
                            $("#founded").text("Founded in: "+companyProfile.quickStats.foundedIn || '');
                            $("#headquarters").text("Headquarters: "+companyProfile.quickStats.headQuarters || '');
                            $("#company-size").text("Company Size: "+companyProfile.quickStats.companySize || '')
                            $("#industry").text("Industry: "+companyProfile.quickStats.industry || '');
                            $("#usp").text(companyProfile.quickStats.usp || '');
                        }

                        if(companyProfile.presentationType){
                            switch (companyProfile.presentationType){
                                case 'video': $("#video-frame").attr('src',companyProfile.videos[0].url);
                                    break;
                                case 'slide':$("#video-frame").attr('src',companyProfile.slides[0].url);
                                    break
                                case 'document':$("#video-frame").attr('src',companyProfile.documents[0].url);
                                    break;
                                default :$(".presentation").hide();
                                    break;
                            }
                        }else $(".presentation").hide();

                        if(checkRequired(companyProfile.latestNews) && companyProfile.latestNews.length > 0){
                            for(var news=0; news<companyProfile.latestNews.length; news++){
                                var html = '';
                                if(news < 1)
                                    html = '<strong>'+companyProfile.latestNews[news].news+'</strong><br>';
                                else html = '<hr><strong>'+companyProfile.latestNews[news].news+'</strong><br>';

                                $("#latestNews").append(html);
                            }
                        }
                    }
                }
            })
    }

    function getCompanyContacts(companyId){
        $.ajax({
            url:'/corporate/company/'+companyId+'/contacts',
            type:'GET',
            success:function(contacts){

                if(checkRequired(contacts) && contacts.length){
                    displayCompanyContacts(contacts);
                }
            }
        })
    }

    function getValidUrl(url){
        if(checkRequired(url)){
            if(contains(url,'http')){
                return url;
            }
            else return 'http://'+url;
        }else return '#'

    }

        function contains(str, subStr) {
            return str.indexOf(subStr) != -1;
        }


        function getValidUniqueUrl(uniqueName) {
        var url = window.location + ''.split('/');
        var patt = new RegExp(url[2]);
        if (patt.test(uniqueName)) {
            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i + 1];
                }
            }
            return uniqueName;
        } else {
            return uniqueName;
        }
    }

    function displayCompanyContacts(contacts){
       for(var contact=0; contact<contacts.length; contact++){
           var imageUrl = '/getImage/'+contacts[contact].userId._id;
           var uniqueName = getValidUniqueUrl(contacts[contact].uniqueName);
           var names = contacts[contact].userId.firstName+' '+contacts[contact].userId.lastName;
           uniqueName = contacts[contact].userId.corporateUser ? '/'+uniqueName : domainName+'/'+uniqueName;
           var td1 = '<img class="contact-img" src='+imageUrl+'>';
           var td2 = '<span title='+names.replace(/\s/g, '&nbsp;')+'>'+getTextLength(names,11)+'</span>';
           var td3 = contacts[contact].userId.designation;
           var td4 = contacts[contact].contactFor;
          // var td5 ='<td style="border-top: 1px solid #DDD;">&nbsp;</td>';
           var td6 = '<a href='+uniqueName+'><button class="btn btn-sm blue-btn">Setup Meeting</button></a>'

           addRowsToTable( [
               td1,td2,td3,td4,td6
           ])
       }
    }

    function getTextLength(text,maxLength){
        if(!checkRequired(text)){
            return ''
        }
        var textLength = text.length;
        if(textLength >maxLength){
            var formattedText = text.slice(0,maxLength)
            return formattedText+'..';
        }
        else return text;
    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }

    function applyDataTable(){
        table = $('#company-contact-list').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 1, "asc" ]],
            "fnDrawCallback": function(oSettings) {

                if (oSettings.fnRecordsTotal() < 11) {
                    $('#company-contact-list_wrapper').hide();
                }
                else  $('#company-contact-list_wrapper').show();
            },

            "oLanguage": {
                "sEmptyTable": "No Members Found"
            }
        });
    }

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }
});