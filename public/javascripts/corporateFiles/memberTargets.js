var relatasApp = angular.module('relatasApp', ['ngRoute','ngLodash']).config(['$interpolateProvider', '$httpProvider', function($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);

relatasApp.service('share', function() {
    return {
    }
});

relatasApp.controller("hierarchyController", function($scope, $http, $location, $rootScope,share){
    $scope.isCorporateUser = false;
    $rootScope.isCorporateAdmin = true;

    $rootScope.$on("fetchedUserProfile", function(event, isCorporateUser){
        $scope.isCorporateUser = isCorporateUser
    })

    share.toggleSideBoard = function () {
        $scope.toggleDisplay = !$scope.toggleDisplay
    }

    $(document).ready(function(){
        function convertArrayToTree(data){

            var root = data.find(function(item) {
                return item.hierarchyParent === null;
            });

            var tree = {
                _id: root._id,
                firstName: root.firstName,
                lastName: root.lastName,
                designation: root.designation,
                emailId: root.emailId
            };

            var parents = [tree];
            while (parents.length > 0) {
                var newParents = [];
                parents.forEach(function(parent) {
                    data.filter(function(item) {
                        return item.hierarchyParent === parent._id
                    }).forEach(function(child) {
                        var c = { _id: child._id, firstName: child.firstName, lastName: child.lastName, designation: child.designation, emailId: child.emailId};
                        parent.children = parent.children || [];
                        parent.children.push(c);
                        newParents.push(c);
                    });
                });
                parents = newParents;
            }

            return tree
        }

        $.getJSON("/company/user/hierarchy", function(data){
            var data2 = {};
            data2 = convertArrayToTree(data.Data);

            var data3 = {};
            data3 = {children:[data2]};

            function addItem(parentUL, branch) {

                for (var key in branch.children) {

                    var item = branch.children[key];
                    var $item = $('<li>', {

                    });

                    $item.append($('<label>', {
                        'ng-click': "setTargetsForUser(" + item._id+")",
                        text: item.firstName +' '+item.lastName
                    }));

                    $item.append($('<p>', {
                        text: item.designation
                    }));

                    parentUL.append($item);
                    if (item.children) {
                        var $ul = $('<ul>', {
                        }).appendTo($item);
                        $item.append();
                        addItem($ul, item);
                    }
                }
            }

            $(function () {
                addItem($('#root'), data3);
            });

        });

    });
});

relatasApp.controller("assign_ownership", function ($scope, $http,$rootScope,share){

    var companyObject = JSON.parse($('#company-object-hidden').text());
    var companyId = companyObject._id;

    $scope.toggleSideBoard = function () {
        share.toggleSideBoard();
    }

    $scope.saveTargets = function () {
        alert("Targets saved successfully.")
        $http.post('/opportunities/set/target',$scope.teamList,function (response) {
        })
    }

    $scope.saveCompanySettings = function () {
        $scope.companyList[0].userId = companyId
        alert("Targets saved successfully.")

        $http.post('/opportunities/set/company/target',$scope.companyList,function (response) {

        })
    }

    function getTargets(companyId,callback) {
        $http.get("/opportunities/get/targets?companyId="+companyId)
            .success(function (targets) {
                callback(targets)
            });
    }

    getTargets(companyId,function (targets) {

        $scope.fiscalYear = [];
        var fyDictionary = {};

        targets.fiscalYear.sort(function (o1, o2) {
            return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
        });

        _.each(targets.fiscalYear,function (fy) {
            fyDictionary[fy.monthYear] = true;
            $scope.fiscalYear.push(fy.monthYear);
        });

        $scope.fiscalYear.push("Total")

        $scope.teamList = [];
        $scope.userDictionary = {};
        $scope.companyTarget = {
            targets: [],
            iscompanyTarget:true,
            fullName:"Company Target"
        };

        var companyTargetExists = false;

        _.each(targets.team,function (el) {


            $scope.userDictionary[el.user._id] = el.user;
            var obj = {};
            obj.image = '/getImage/' + el.user._id
            obj.fullName = el.user.firstName + " " + el.user.lastName
            obj.name = el.user.firstName + " " + el.user.lastName
            obj.emailId = el.user.emailId
            obj.emailIdXs = getTextLength(el.user.emailId,12)
            obj.userId = el.user._id
            obj.designation = el.user.designation
            obj.designationXs = getTextLength(el.user.designation,12)

            var targetsList = [];
            _.each(el.targets.salesTarget,function (st) {
                var dt = new Date(st.date);
                var monthYear = monthNames[dt.getUTCMonth()].substring(0,3)+" "+dt.getUTCFullYear()
                if(fyDictionary[monthYear]){

                    st.monthYear = monthYear
                    targetsList.push(st)
                }
            });

            //Find values that are in fiscalYear but not in targetsList
            var notExist = targets.fiscalYear.filter(function(obj) {

                return !targetsList.some(function(obj2) {
                    return obj.monthYear == obj2.monthYear;
                });
            });

            _.each(notExist,function (el) {
                targetsList.push({
                    "target": el.target,
                    "date": el.date,
                    "monthYear": el.monthYear
                })
            });

            targetsList.sort(function (o1, o2) {
                return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
            });

            obj.targets = targetsList;

            if(el.isCompanyTarget){
                companyTargetExists = true;
                obj.fullName = "Company Target"
                $scope.companyTarget = obj
            } else {
                obj.totalTarget = _.sumBy(obj.targets,function (tr) {
                    return parseFloat(tr.target);
                })

                obj.totalTarget = parseFloat(obj.totalTarget.toFixed(2))
                $scope.teamList.push(obj);
            }

            $scope.companyTarget = _.cloneDeep(obj);

        });

        if(!companyTargetExists){
            $scope.companyTarget.userId = companyId
            $scope.companyTarget.fullName = "Company Target"
            _.each($scope.companyTarget.targets,function (tr) {
                tr.target = 0;
            })
        }

        $scope.companyList = [$scope.companyTarget]

    });

});

relatasApp.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResults">' +
        '<div ng-repeat="item in searchList track by $index"> ' +
        '<div class="clearfix cursor" title="[[item.fullName]], [[item.emailId]]" ng-click="addUser(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="[[item.image]]" title="[[item.fullName]], [[item.emailId]]" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag">' +
        '<span class="contact-no-image">[[item.nameNoImg]]</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">[[item.name]]</p><p class="margin0">[[item.emailId]]</p></div>' +
        '<div class="pull-left"><p class="margin0">[[item.name]]</p></div>' +
        '</div></div></div>'
    };
});

relatasApp.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.imageNotLoaded = function(obj){

                if(!obj && scope.p_name){
                    scope.p_pic = scope.p_name.substr(0,2).toUpperCase();
                    scope.no_pic = true;
                }
                else if(obj.name){
                    obj.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    obj.noPPic = obj.name.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.name.substr(0,2).toUpperCase();
                    scope.noPPic = obj.name.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                } else if(obj.fullName){

                    obj.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    obj.noPicFlag = true;
                    scope.nameNoImg = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPPic = obj.fullName.substr(0,2).toUpperCase();
                    scope.noPicFlag = true;
                }
            };
            element.bind('error', function() {
                //call the function that was passed
                scope.$apply(attrs.imageonload);
            });
        }
    };
});

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];

function getTextLength(text,maxLength){
    if(!text)
        return "";

    var textLength = text.length;
    if(textLength >maxLength){
        var formattedText = text.slice(0,maxLength)
        return formattedText+'..';
    }
    else return text;
}

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}