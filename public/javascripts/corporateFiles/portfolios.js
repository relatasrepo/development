var relatasApp = angular.module('relatasApp', ['ngRoute','ngSanitize','ngLodash']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {

    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

relatasApp.controller("companyDetails", function ($scope,$http,$rootScope){

    $http.get('/profile/get/current/web')
        .success(function (response) {
            $scope.liuData = response.Data;
            getPortfolios($scope,$http,function (portfolios) {
                paintMenu($scope,portfolios);

                getTeamMembers($scope,$http,function () {
                    getCompanyDetails($scope,$http);
                });
            });
        })


    $scope.getActivityLogs = function(currentSelection){

        $scope.activityLogs = []
        $http.get("/corporate/admin/portfolio/logs?currentSelection="+currentSelection)
            .success(function (response) {

                $scope.activityLogs = [];
                if(response && response.length>0){
                    _.each(response,function(re){
                        if(re.date){
                            $scope.activityLogs.push(re)
                        }
                    })
                };
                _.each($scope.activityLogs,function (log) {
                    log.date = new Date(log.date);
                    log.formattedDate = moment(log.date).format("MMMM Do YYYY, h:mm:ss a");
                    log.by = $scope.teamObjEmailId[log.byEmailId]?$scope.teamObjEmailId[log.byEmailId].name:log.byEmailId
                });


                $scope.activityLogs.sort(function (o1, o2) {
                    return o1.date < o2.date ? 1 : o1.date > o2.date ? -1 : 0;
                    // return new Date(o1.date) > new Date(o2.date) ? 1 : new Date(o1.date) < new Date(o2.date) ? -1 : 0;
                });
            })
    }

    $scope.showAllUsers = function(item){
        _.each($scope.portfolioList,function (po) {
            if(po.name !== item.name){
                // item.allUsersList = false;
            }
        });
        item.allUsersList = !item.allUsersList;
    }

    $scope.openPortfolioManager = function(portfolio){
        $scope.showPortfolioManager = true;
        $scope.selectedPortfolio = portfolio;
        $scope.toDeleteList = [];

        $rootScope.allPortfolios = [
            {
                name:"Business Units",
                list:$scope.companyResponse.businessUnits
            },
            {
                name:"Regions",
                list:$scope.companyResponse.geoLocations
            },
            {
                name:"Products",
                list:$scope.companyResponse.productList
            },
            {
                name:"Verticals",
                list:$scope.companyResponse.verticalList
            }
        ]

        $rootScope.allPortfolios.forEach(function (al) {
            if(al.name == "Regions"){
                al.list.forEach(function(re){
                   if(!re.name){
                       re.name = re.region
                   }
                });
            }
            al.list.sort(function (a,b) {
                if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                return 0;
            })
        })

        $http.post("/corporate/admin/portfolio/get/by/type",{
            name:portfolio.name,
            type:$scope.currentSelectionObj.name
        })
            .success(function(response){
                $scope.subportfolios = response;

                $scope.teamMembersForSelection = [];
                var existingUsers = {}

                $scope.subportfolios.forEach(function (portfolio) {

                    var userName = portfolio.ownerEmailId;
                    if($scope.teamObjEmailId && $scope.teamObjEmailId[portfolio.ownerEmailId]){
                        userName = $scope.teamObjEmailId[portfolio.ownerEmailId].name
                    }

                    portfolio.userName = userName;

                    existingUsers[portfolio.ownerEmailId] = true;
                    var totalAmt = 0;
                    _.each(portfolio.targets[0].values,function (tr) {
                        if(tr.name !== "Total"){
                            totalAmt = totalAmt+tr.amount;
                        }
                    });

                    portfolio.targets[0].values.push({
                        monthYear:"Total",
                        valueFormatted: getAmountInThousands(totalAmt,2)
                    })
                });

                _.each($scope.data.teamMembers,function(el){
                    if(!existingUsers[el.emailId]){
                        $scope.teamMembersForSelection.push(el)
                    }
                });

                $scope.teamMembersForSelection.sort(function (a,b) {
                    if(a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                    if(a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                    return 0;
                })
            })
    }

    $scope.setHead = function(user,subportfolios){

        var headExists = false;

        _.each(subportfolios,function(el){
            if(el.isHead){
                headExists = true;
            }
        });

        if(!user.isHead && subportfolios.length == 1){
            user.isHead = true;
        }

        var name = user.ownerEmailId;
        if($scope.teamObjEmailId && $scope.teamObjEmailId[user.ownerEmailId]){
            name = $scope.teamObjEmailId[user.ownerEmailId].name
        }

        _.each(subportfolios,function(el){
            if(el.ownerEmailId != user.ownerEmailId){
                el.isHead = false;
            }
        });

        if(!headExists){
            user.isHead = true
            _.each(subportfolios,function(el){
                if(el.ownerEmailId == user.ownerEmailId){
                    el.isHead = true;
                }
            });
        }

        if(user.isHead){
            _.each(user.accessLevel,function (ac) {

                if(ac.type == "Business Units") {
                    ac.values = _.map($scope.companyResponse.businessUnits,"name");
                }

                if(ac.type == "Regions") {
                    ac.values = _.map($scope.companyResponse.geoLocations,"name");
                }

                if(ac.type == "Products") {
                    ac.values = _.map($scope.companyResponse.productList,"name");
                }

                if(ac.type == "Verticals") {
                    ac.values = _.map($scope.companyResponse.verticalList,"name");
                }
            });
            toastr.warning("Portfolio Heads by default get access to all other portfolios. "+name +" has been " +
                "granted access to all other portfolios so that they get company wide access to "+$scope.selectedPortfolio.name)
        }

        var what = "Set portfolio head - "+ name+" for "+$scope.selectedPortfolio.name;
        if(!user.isHead){
            what = "Remove as portfolio head - "+ name+" for "+$scope.selectedPortfolio.name;
        }

        $scope.activityLogsCache.push({
            what:what,
            action:"Portfolio head "+name,
            byEmailId: $scope.liuData.emailId
        });
    }

    $scope.selectAll = function(list,all,user,type){

        var values = [];
        _.each(list,function(el){
            el.selected = all;
            if(el.selected){
                values.push(el.name)
            }
        });

        _.each(user.accessLevel,function(ac){
            if(ac.type == type){
                ac.values = values
            }
        });

        if(all){
            $scope.activityLogsCache.push({
                what:"Given access to all portfolios -"  +" in "+ type+" for "+user.userName,
                action:"Given access to all portfolios -"  +" in "+ type+" for "+user.userName,
                byEmailId: $scope.liuData.emailId
            });
        } else {
            $scope.activityLogsCache.push({
                what:"Removed access to all portfolios -"  +" in "+ type+" for "+user.userName,
                action:"Removed access to all portfolios -"  +" in "+ type+" for "+user.userName,
                byEmailId: $scope.liuData.emailId
            });
        }

        $scope.activityLogsCache =  _.uniqBy($scope.activityLogsCache,"what");


        $scope.editsMade = true;
    }

    $scope.selectUser_sub = function(portfolio,user){

        $scope.editsMade = true;

        var userDoesntExist = false;

        _.each($scope.subportfolios,function(po){
            if(po.ownerEmailId == user.emailId){
                userDoesntExist = true;
            }
        });

        var isHead = false;
        if(!$scope.subportfolios || $scope.subportfolios.length == 0){
            isHead = true;
        }

        if(!userDoesntExist){

            if(user.orgHead || user.hierarchyParent){

                $scope.showUsers = false;
                $scope.selectedUser = user;

                if(!$scope.subportfolios || ($scope.subportfolios && $scope.subportfolios.length == 0)){
                    $scope.subportfolios = []
                }

                var fyStart = moment(moment().month($scope.companyResponse.fyMonth)).startOf("month");
                var fyEnd = moment(moment(fyStart).add(11,"months")).endOf("month");

                var newItemTargets = {
                    fy:{
                        start: new Date(),
                        end: new Date()
                    },
                    values:[]
                };

                newItemTargets.fy.start = new Date(fyStart);
                newItemTargets.fy.end = new Date(fyEnd);

                var fyMonths = getMonthsBetweenDates(fyStart,fyEnd);

                _.each(fyMonths,function (va) {
                    newItemTargets.values.push({
                        date: new Date(va),
                        amount: 0
                    })
                });

                newItemTargets.values.push({
                    monthYear: "Total",
                    amount: 0,
                    valueFormatted:"0"
                });

                var accessLevel = [];
                var accessTypes = ""

                _.each($rootScope.allPortfolios,function(el){
                    if(el.name != $scope.currentSelectionObj.name){
                        if(accessTypes == ""){
                            accessTypes = "all "+accessTypes+el.name;
                        } else {
                            accessTypes = accessTypes+", all "+el.name;
                        }

                        if(el.name == "Regions"){
                            accessLevel.push({
                                type:el.name,
                                values:_.map(el.list,"region")
                            })
                        } else {
                            accessLevel.push({
                                type:el.name,
                                values:_.map(el.list,"name")
                            })
                        }
                    }
                });

                $scope.activityLogsCache.push({
                    what:"Added user - "+ user.name+" for "+$scope.selectedPortfolio.name,
                    action:"Set portfolio head "+user.name,
                    byEmailId: $scope.liuData.emailId
                });

                toastr.warning(user.name+" has access to "+$scope.selectedPortfolio.name+" and " +accessTypes+". Please edit the access for any corrections.")

                $scope.subportfolios.push({
                    type:$scope.currentSelectionObj.name,
                    name: $scope.selectedPortfolio.name,
                    isHead:isHead,
                    ownerEmailId:user.emailId,
                    userName:user.name,
                    targets: [newItemTargets],
                    accessLevel:accessLevel
                });

                _.each($scope.subportfolios,function(po){
                    if(po.ownerEmailId == user.emailId){
                        $scope.openAccessLevel(po);
                    }
                });

                if (window.confirm('Do you wish to sync '+user.name+"'s targets from employee database?")) {
                    $scope.syncTargets(user);
                }

            } else {
                toastr.error("Please add "+user.name+" to the Org. hierarchy before assigning them access to a portfolio")
            }
        } else {
            toastr.error(user.name+" is already part of "+$scope.selectedPortfolio.name)
        }

        $scope.selectedUser = {}
    }

    $scope.closePortmanager = function(){
        if($scope.editsMade){
            if (window.confirm('There are unsaved changes. Do you wish to discard the changes and close this window?')) {
                $scope.editsMade = false;
                $scope.showPortfolioManager = false;
                $scope.subportfolios = [];
                $scope.activityLogsCache = [];
            }
        } else {
            $scope.showPortfolioManager = false;
        }

        $scope.getTargetsForPortfiolio($scope.currentSelectionObj)
    }

    closeAllDropDownsAndModals($scope,".dismiss-ths");

    $scope.showAllUsersForSelection = function(){
        $scope.showUsers = true;
    }

    $scope.selectUser = function(item,user){
        item.head = user.name;
        item.headEmailId = user.emailId;
        item.allUsersList = false;

        $scope.activityLogsCache.push({
            what:"Set portfolio head - "+ user.name+" for "+item.name,
            action:"Set portfolio head "+user.name,
            byEmailId: $scope.liuData.emailId
        });
    }

    $scope.savePortfolio = function(portfolio){
        $scope.showPortfolioForm = false;

        if(portfolio){

            $http.post("/corporate/admin/portfolio/create",{data:portfolio}).success(function (response) {

                $http.post("/corporate/admin/portfolio/logs",[{
                    what:"New portfolio created - "+ portfolio,
                    action:"Added",
                    byEmailId: $scope.liuData.emailId,
                    currentSelection:portfolio,
                    date: new Date()
                }]).success(function (response) {
                })

                getPortfolios($scope,$http,function (portfolios) {
                    paintMenu($scope,portfolios);
                });
            });
        } else {
         console.log("Wrong")
        }
    }

    $scope.closeForm = function(){
        $scope.showPortfolioForm = false;
    }

    $scope.editName = function(){

    }

    $scope.openPortfolioCreationForm = function(){
        $scope.showPortfolioForm = true;
        $(".modal").css("display", "block");
    }

    $scope.deleteThisItem = function(item){

        if (window.confirm('Do you wish to permanently delete '+ item.name+ '? Any team members and targets assigned to this portfolio will not be accessible.')) {
            $scope.activityLogsCache.push({
                what:"Deleted value - "+ item.name,
                action:"delete",
                byEmailId: $scope.liuData.emailId
            });

            if(!$scope.deleteUsersWithPortfolio){
                $scope.deleteUsersWithPortfolio = []
            };

            $scope.deleteUsersWithPortfolio.push({
                name:item.name,
                type:$scope.currentSelection,
            });

            $scope.portfolioList = $scope.portfolioList.filter(function (po) {
                return po.name != item.name
            });

            $scope.saveData();
        }
    }

    $scope.deleteThisItem_sub = function(item,index){

        if(item.isHead){
            toastr.error("Please assign someone else as the Portfolio Head before you delete the current Portfolio Head")
        } else {

            $scope.toDeleteList.push(item);

            if (window.confirm("Do you wish to delete "+ item.userName +"? All associated data between "+ item.userName +" and this portfolio will be deleted.")) {

                $http.post("/corporate/admin/portfolio/sub/delete",item)
                    .success(function (response) {
                        $scope.subportfolios.splice(index, 1);
                        $http.post("/corporate/admin/portfolio/logs",[{
                            what:"Deleted value - "+ item.name +" for user "+item.userName,
                            action:"delete",
                            type: item.type,
                            currentSelection: $scope.currentSelection,
                            byEmailId: $scope.liuData.emailId
                        }])
                            .success(function (response) {
                                // $scope.saveData_sub();
                                $scope.activityLogsCache = []
                                $scope.getActivityLogs($scope.currentSelection);
                                toastr.success("The data entered is Saved. Click on Activity Log to see details")
                            })
                    })
            }
        }

    }

    $scope.deletePortfolio = function(){

        var fromPortfolios = false;

        if($scope.currentSelectionObj && $scope.currentSelectionObj.fromPortfolios){
            fromPortfolios = true;
        }

        $http.post("/corporate/admin/portfolio/delete",{
            name:$scope.currentSelection,
            fromPortfolios:fromPortfolios
        }).success(function (response) {
            $scope.menu = $scope.menu.filter(function (me) {
                return me.name != $scope.currentSelection;
            });

            $http.post("/corporate/admin/portfolio/logs",[{
                what:"Deleted portfolio - "+ $scope.currentSelection,
                action:"Deleted",
                byEmailId: $scope.liuData.emailId,
                currentSelection:$scope.currentSelection,
                date: new Date()
            }]).success(function (response) {
            })

            $scope.selectMenu($scope.menu[0]);
        });
    }

    $scope.addNewItem = function(currentSelection,item){

        if(item && item != "" && item != " "){

            var newItemTargets = {
                fy:{
                    start: new Date(),
                    end: new Date()
                },
                values:[]
            };

            if($scope.portfolioList[0] && $scope.portfolioList[0].targets[0]){
                newItemTargets.fy.start = $scope.portfolioList[0].targets[0].fy.start;
                newItemTargets.fy.end = $scope.portfolioList[0].targets[0].fy.end;

                $scope.portfolioList[0].targets[0].values.forEach(function (va) {
                    if(va.date){
                        newItemTargets.values.push({
                            date: new Date(va.date),
                            amount: 0
                        })
                    } else if(va.monthYear == "Total"){
                        newItemTargets.values.push({
                            monthYear: "Total",
                            amount: 0,
                            valueFormatted:""
                        })
                    }
                })
            } else {

                var fyStart = moment(moment().month($scope.companyResponse.fyMonth)).startOf("month");
                var fyEnd = moment(moment(fyStart).add(11,"months")).endOf("month");

                newItemTargets.fy.start = fyStart;
                newItemTargets.fy.end = fyEnd;

                var fyMonths = getMonthsBetweenDates(fyStart,fyEnd);

                _.each(fyMonths,function (va) {
                    newItemTargets.values.push({
                        date: new Date(va),
                        amount: 0
                    })
                });

                newItemTargets.values.push({
                    monthYear: "Total",
                    amount: 0,
                    valueFormatted:""
                })
            }

            $scope.activityLogsCache.push({
                what:"Added new value - "+ item +" to "+ $scope.currentSelection,
                action:"add",
                byEmailId: $scope.liuData.emailId
            });

            if(currentSelection == "Regions"){

                $scope.portfolioList.push({
                    name: item,
                    region: item,
                    head: "Assign",
                    headEmailId: null,
                    targets: [newItemTargets]
                })
            } else {
                $scope.portfolioList.push({
                    name: item,
                    head: "Assign",
                    headEmailId: null,
                    targets: [newItemTargets]
                })
            }

            $scope.saveData();

        } else {
            alert("Please enter a name")
        }

    }

    $scope.buildLog = function(portfolio){

        if($scope.activityLogsCache && $scope.activityLogsCache.length>0){
            var editedExisting = false;
            _.each($scope.activityLogsCache,function (log) {
                if(log.action == "Edited name "+portfolio.name){
                    editedExisting = true;
                    log.what = "Edited portfolio name to "+ portfolio.name
                }
            })

            if(!editedExisting){
                $scope.activityLogsCache.push({
                    what:"Edited portfolio name to "+ portfolio.name,
                    action:"Edited name "+portfolio.name,
                    byEmailId: $scope.liuData.emailId
                });
            }

        } else {

            $scope.activityLogsCache.push({
                what:"Edited portfolio name to "+ portfolio.name,
                action:"Edited name "+portfolio.name,
                byEmailId: $scope.liuData.emailId
            });
        }
    }

    closeAllDropDownsAndModals($scope,".p-user-ls");

    $scope.reCalculateValues = function(item,portfolio){

        if(!portfolio.headEmailId){
            item.amount = 0;
            toastr.error("Please set portfolio head before assigning targets")
        } else {
            item.amount = parseFloat(item.amount);

            if($scope.activityLogsCache && $scope.activityLogsCache.length>0){
                var editedExisting = false;
                _.each($scope.activityLogsCache,function (log) {
                    if(log.action == "Edited target "+portfolio.name+" for "+ moment(item.date).format("MMM YYYY")){
                        editedExisting = true;
                        log.what = "Set target "+ item.amount+" for "+ moment(item.date).format("MMM YYYY") +" in "+portfolio.name
                    }
                })

                if(!editedExisting){

                    $scope.activityLogsCache.push({
                        what:"Set target "+ item.amount+" for "+ moment(item.date).format("MMM YYYY") +" in "+portfolio.name,
                        action:"Edited target "+portfolio.name+" for "+ moment(item.date).format("MMM YYYY"),
                        byEmailId: $scope.liuData.emailId
                    });
                }
            } else {

                $scope.activityLogsCache.push({
                    what:"Set target "+ item.amount+" for "+ moment(item.date).format("MMM YYYY") +" in "+portfolio.name,
                    action:"Edited target "+portfolio.name+" for "+ moment(item.date).format("MMM YYYY"),
                    byEmailId: $scope.liuData.emailId
                });
            }

            $scope.portfolioList.forEach(function (port) {
                portfolio.targets.forEach(function (po) {
                    var sumVal =  0;
                    po.values.forEach(function (tr) {
                        if(tr.monthYear == "Total"){
                            tr.value = sumVal
                            tr.valueFormatted = getAmountInThousands(sumVal,2)
                        } else {
                            sumVal = sumVal+tr.amount;
                        }
                    })
                })
            })
        }

    }

    $scope.reCalculateValues_sub = function(item,portfolio){

        var headIdentified = false;
        _.each($scope.subportfolios,function(po){
            if(po.isHead){
                headIdentified = true;
            }
        });

        var name = portfolio.ownerEmailId;
        if($scope.teamObjEmailId && $scope.teamObjEmailId[portfolio.ownerEmailId]){
            name = $scope.teamObjEmailId[portfolio.ownerEmailId].name
        }

        if(!headIdentified){
            item.amount = 0;
            toastr.error("Please set a portfolio head before assigning targets")
        } else {
            item.amount = parseFloat(item.amount);

            if($scope.activityLogsCache && $scope.activityLogsCache.length>0){
                var editedExisting = false;
                _.each($scope.activityLogsCache,function (log) {
                    if(log.action == "Edited target "+portfolio.name+" for "+ moment(item.date).format("MMM YYYY")+" for "+name){
                        editedExisting = true;
                        log.what = "Set target "+ item.amount+" for "+ moment(item.date).format("MMM YYYY") +" in "+portfolio.name+" for "+name
                    }
                })

                if(!editedExisting){

                    $scope.activityLogsCache.push({
                        what:"Set target "+ item.amount+" for "+ moment(item.date).format("MMM YYYY") +" in "+portfolio.name+" for "+name,
                        action:"Edited target "+portfolio.name+" for "+ moment(item.date).format("MMM YYYY")+" for "+name,
                        byEmailId: $scope.liuData.emailId
                    });
                }
            } else {

                $scope.activityLogsCache.push({
                    what:"Set target "+ item.amount+" for "+ moment(item.date).format("MMM YYYY") +" in "+portfolio.name+" for "+name,
                    action:"Edited target "+portfolio.name+" for "+ moment(item.date).format("MMM YYYY")+" for "+name,
                    byEmailId: $scope.liuData.emailId
                });
            }

            portfolio.targets.forEach(function (po) {
                var sumVal =  0;
                po.values.forEach(function (tr) {
                    if(tr.monthYear == "Total"){
                        tr.value = sumVal
                        tr.valueFormatted = getAmountInThousands(sumVal,2)
                    } else {
                        sumVal = sumVal+tr.amount;
                    }
                })
            });

            $scope.editsMade = true;
        }

    }

    $scope.activityLogsCache = [];
    $scope.displayActivityLog = false;

    $scope.saveData = function(){

        $scope.newItem = "";
        var fromPortfolios = false;

        if($scope.currentSelectionObj && $scope.currentSelectionObj.fromPortfolios){
            fromPortfolios = true;
        }

        var portfolioList= [];

        _.each($scope.portfolioList,function (po) {
            portfolioList.push(_.cloneDeep(po));
        });

        _.each(portfolioList,function (po) {
            _.each(po.targets,function (tr) {
                tr.values = tr.values.filter(function (va) {
                    return va.monthYear != "Total"
                })
            })
        });

        _.each($scope.activityLogsCache,function (log) {
            log.type = "portfolios";
            log.date = new Date();
            if(!log.currentSelection || (log.currentSelection && (log.currentSelection == "" || log.currentSelection == " "))){
                log.currentSelection = $scope.currentSelection
            }
        });

        var namesToChange = [];
        _.each(portfolioList,function(po){
            _.each($scope.localPortfolioListCache,function(lo){
                if(po._id == lo._id && po.name != lo.name){
                    namesToChange.push({
                        type:$scope.currentSelection,
                        old:lo.name,
                        "new":po.name,
                    })
                }
            });
        });

        $http.post("/corporate/admin/portfolio/save",{
            name:$scope.currentSelection,
            content:portfolioList,
            fromPortfolios:fromPortfolios,
            deleteUsersWithPortfolio:$scope.deleteUsersWithPortfolio,
            namesToChange:namesToChange
        }).success(function (response) {

            $scope.deleteUsersWithPortfolio = [];
            getCompanyDetails($scope,$http,null,true);
            $scope.activityLogsCache =  _.uniqBy($scope.activityLogsCache,"what");

            $http.post("/corporate/admin/portfolio/logs",$scope.activityLogsCache)
                .success(function (response) {
                    $scope.activityLogsCache = []
                    $scope.getActivityLogs($scope.currentSelection);
                    toastr.success("The data entered is Saved. Click on Activity Log to see details")
                })
        });
    }

    $scope.updateAccessLevels = function(item,name,isSelected,type){
        if(item.accessLevel){

            _.each(item.accessLevel,function(ac){
                if(ac.type == type){
                    if(!isSelected){
                        ac.values = ac.values.filter(function(va){
                            return va != name
                        })
                    } else {
                        ac.values.push(name)
                    }
                }
            });

            if(!$scope.activityLogsCache){
                $scope.activityLogsCache = [];
            }

            if(isSelected){
                $scope.activityLogsCache.push({
                    what:"Given access to - "+ name +" in "+ type+" for "+item.userName,
                    action:"Given access to - "+ name +" in "+ type+" for "+item.userName,
                    byEmailId: $scope.liuData.emailId
                });
            } else {
                $scope.activityLogsCache.push({
                    what:"Removed access to - "+ name +" in "+ type+" for "+item.userName,
                    action:"Removed access to - "+ name +" in "+ type+" for "+item.userName,
                    byEmailId: $scope.liuData.emailId
                });
            }

            $scope.activityLogsCache =  _.uniqBy($scope.activityLogsCache,"what");

            $scope.editsMade = true;
        }
    }

    $scope.showMessage = function(el){
        el.showSomeMessage = !el.showSomeMessage
    }

    $scope.syncTargets = function(user){

        $http.get("/corporate/admin/get/user/targets?userId="+user._id)
            .success(function (response) {

                if(response && response[0] && response[0].targets && response[0].targets.length>0){
                    var targets= {};
                    _.each(response[0].targets,function(tr){
                        targets[tr.monthYear] = tr.target
                    })

                    var totalAmt = 0;

                    if(!$scope.activityLogsCache){
                        $scope.activityLogsCache = [];
                    }

                    _.each($scope.subportfolios,function(po){
                        if(po.ownerEmailId == user.emailId){

                            var name = po.ownerEmailId;
                            if($scope.teamObjEmailId && $scope.teamObjEmailId[po.ownerEmailId]){
                                name = $scope.teamObjEmailId[po.ownerEmailId].name
                            }
                            _.each(po.targets,function(tr){
                                _.each(tr.values,function(va){
                                    va.amount = targets[moment(va.date).format("MMM YYYY")]?targets[moment(va.date).format("MMM YYYY")]:0;
                                    if(va.monthYear !== "Total"){
                                        totalAmt = totalAmt+va.amount;

                                        if(va.amount){
                                            $scope.activityLogsCache.push({
                                                what:"Set target "+ va.amount+" for "+ moment(va.date).format("MMM YYYY") +" in "+po.name+" for "+name,
                                                action:"Edited target "+po.name+" for "+ moment(va.date).format("MMM YYYY")+" for "+name,
                                                byEmailId: $scope.liuData.emailId,
                                                type: "Sync Targets",
                                                date : new Date(moment().add(1,"minute"))
                                            });
                                        }

                                    } else if(va.monthYear == "Total"){
                                        va.valueFormatted = getAmountInThousands(totalAmt,2)
                                    }
                                })
                            });
                        }
                    });
                }
            });
    }

    $scope.openAccessLevel = function(user){

        var accessLevel = {}
        _.each(user.accessLevel,function(ac){
            accessLevel[ac.type] = ac.values;
        })

        _.each($rootScope.allPortfolios,function(al){
            var allSelected = true;
            _.each(al.list,function(li){
                li.selected = accessLevel[al.name]?accessLevel[al.name].indexOf(li.name)>-1:false;
                if(!li.selected){
                    allSelected = false;
                }
            });

            al.allSelected = allSelected;
        })

        user.showAccess = true;
    }

    $scope.saveData_sub = function(){

        var subportfolios= [];

        _.each($scope.subportfolios,function (po) {
            subportfolios.push(_.cloneDeep(po));
        });

        _.each(subportfolios,function (po) {
            _.each(po.targets,function (tr) {
                tr.values = tr.values.filter(function (va) {
                    va.amount = parseFloat(va.amount);
                    if(va.date){
                        return va;
                    }
                })
            })
        });

        _.each($scope.activityLogsCache,function (log) {
            log.type = $scope.currentSelection;
            log.date = new Date();
            if(!log.currentSelection || (log.currentSelection && (log.currentSelection == "" || log.currentSelection == " "))){
                log.currentSelection = $scope.currentSelection
            }
        });

        $scope.activityLogsCache =  _.uniqBy($scope.activityLogsCache,"what");

        $http.post("/corporate/admin/portfolio/sub/save",subportfolios).success(function (response) {

            $http.post("/corporate/admin/portfolio/logs",$scope.activityLogsCache)
                .success(function (response) {
                    $scope.activityLogsCache = [];
                    $scope.editsMade = false;
                    $scope.getActivityLogs($scope.currentSelection);
                    toastr.success("The data entered is Saved. Click on Activity Log to see details")
                })
        });
    }

    $rootScope.isCorporateAdmin = true;

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    $scope.getTargetsForPortfiolio = function (forHead) {

        $scope.portfolioTarget = {}
        var url = "/corporate/admin/portfolio/targets?type="+$scope.currentSelection;
        if(forHead == "head"){
            url = "/corporate/admin/portfolio/targets?type="+$scope.currentSelection+"&forHead="+true
        }
        $scope.dataLoaded = false;
        $http.get(url)
            .success(function (response) {

                var fy = response.fy;

                _.each($scope.portfolioList,function (po) {
                    if($scope.teamObjEmailId && $scope.teamObjEmailId[po.headEmailId]){
                        po.head = $scope.teamObjEmailId[po.headEmailId].name;
                    } else {
                        po.head = "Assign"
                    }

                    po.headTarget = 0;
                    po.allTarget = 0;

                    if(response.targets[po.name]){
                        po.headTarget = response.targets[po.name].head;
                        po.allTarget = response.targets[po.name].all;
                    }

                    po.headTarget = getAmountInThousands(po.headTarget,2)
                    po.allTarget = getAmountInThousands(po.allTarget,2)

                    var totalAmt = 0;
                    _.each(po.targets[0].values,function (tr) {
                        if(tr.name !== "Total"){
                            totalAmt = totalAmt+tr.amount;
                        }
                    });

                    if(fy){
                        $scope.fy = moment(fy.start).format("MMM YYYY")+"-"+moment(moment(fy.end).subtract(1,"day")).format("MMM YYYY")
                    }

                    if(po.targets[0].values.length == 12){
                        po.targets[0].values.push({
                            amount:totalAmt,
                            monthYear: "Total",
                            valueFormatted:getAmountInThousands(totalAmt,2)
                        })
                    }

                    _.each($scope.FyMonths,function (mo,index) {
                        if(!mo.monthlyTotal){
                            mo.monthlyTotal = 0
                        }

                        if(mo.name !== "Total"){
                            mo.monthlyTotal = mo.monthlyTotal+po.targets[0].values[index].amount;
                            mo.monthFormatted = getAmountInThousands(mo.monthlyTotal,2);
                        }

                        if(mo.name === "Total"){
                            mo.monthlyTotal = mo.monthlyTotal+totalAmt;
                            mo.monthFormatted = getAmountInThousands(mo.monthlyTotal,2);
                        }
                    });
                });

                $scope.dataLoaded = true;
            })
    }

    $scope.selectMenu = function (item,dontCallCloseWindow) {

        if(item){
            $scope.dataLoaded = false;
            $scope.deleteUsersWithPortfolio = [];

            if(!dontCallCloseWindow){
                $scope.closePortmanager();
            };

            $scope.displayActivityLog = false;
            $scope.viewingActivityLogs = "";
            $scope.newItem = "";
            $scope.myVar = "head";
            $scope.currentSelection = item.name;
            $scope.currentSelectionObj = item;
            item.selected = "selected";
            $scope.activityLogsCache = [];
            $scope.portfolioList = [];
            $scope.localPortfolioListCache = [];
            resetOtherSelection($scope,item);

            $scope.getActivityLogs($scope.currentSelection);

            var fromPortfolios = false;

            $("#required").prop('checked', $scope.companyResponse.opportunitySettings.regionRequired)

            if($scope.currentSelectionObj && $scope.currentSelectionObj.fromPortfolios){
                fromPortfolios = true;
                $("#required").prop('checked', $scope.currentSelectionObj.required)
            }

            if($scope.currentSelection === "Business Units") {
                $("#required").prop('checked', $scope.companyResponse.opportunitySettings.businessUnitRequired)
            }

            if($scope.currentSelection === "Products") {
                $("#required").prop('checked', $scope.companyResponse.opportunitySettings.productRequired)
            }

            if($scope.currentSelection === "Verticals") {
                $("#required").prop('checked', $scope.companyResponse.opportunitySettings.verticalRequired)
            }

            if(fromPortfolios){
                $scope.actionType = "Delete";
            } else {
                $scope.actionType = "Clear";
            }

            $scope.portfolioList = [];

            $scope.FyMonths = [
                {name:"Apr",monthlyTotal:0}
                ,{name:"May",monthlyTotal:0}
                ,{name:"Jun",monthlyTotal:0}
                ,{name:"Jul",monthlyTotal:0}
                ,{name:"Aug",monthlyTotal:0}
                ,{name:"Sep",monthlyTotal:0}
                ,{name:"Oct",monthlyTotal:0}
                ,{name:"Nov",monthlyTotal:0}
                ,{name:"Dec",monthlyTotal:0}
                ,{name:"Jan",monthlyTotal:0}
                ,{name:"Feb",monthlyTotal:0}
                ,{name:"Mar",monthlyTotal:0}
                ,{name:"Total",monthlyTotal:0}]

            if($scope.companyResponse.fyMonth === "January"){
                $scope.FyMonths = [
                    {name:"Jan",monthlyTotal:0}
                    ,{name:"Feb",monthlyTotal:0}
                    ,{name:"Mar",monthlyTotal:0}
                    ,{name:"Apr",monthlyTotal:0}
                    ,{name:"May",monthlyTotal:0}
                    ,{name:"Jun",monthlyTotal:0}
                    ,{name:"Jul",monthlyTotal:0}
                    ,{name:"Aug",monthlyTotal:0}
                    ,{name:"Sep",monthlyTotal:0}
                    ,{name:"Oct",monthlyTotal:0}
                    ,{name:"Nov",monthlyTotal:0}
                    ,{name:"Dec",monthlyTotal:0}
                    ,{name:"Total",monthlyTotal:0}]
            }

            $scope.companyResponse.geoLocations.forEach(function(el){
                el.name = el.region;
            })

            if(item.fromPortfolios){
                $scope.portfolioList = item.list;
            } else {

                if(item.name == "Business Units") {
                    $scope.portfolioList = $scope.companyResponse.businessUnits;
                    $scope.localPortfolioListCache = $scope.companyResponse.businessUnits;
                }

                if(item.name == "Regions") {
                    $scope.portfolioList = $scope.companyResponse.geoLocations;
                    $scope.localPortfolioListCache = $scope.companyResponse.geoLocations;

                    $scope.portfolioList.forEach(function (po) {
                        po.name = po.region;
                    })
                }

                if(item.name == "Products") {
                    $scope.portfolioList = $scope.companyResponse.productList;
                    $scope.localPortfolioListCache = $scope.companyResponse.productList;
                }

                if(item.name == "Verticals") {
                    $scope.portfolioList = $scope.companyResponse.verticalList;
                    $scope.localPortfolioListCache = $scope.companyResponse.verticalList;
                }
            }

            $scope.localPortfolioListCache = _.cloneDeep($scope.localPortfolioListCache)

            $http.get("/corporate/admin/portfolio/targets?type="+$scope.currentSelection+"&forHead="+true)
                .success(function (response) {
                    $scope.portfolioTarget = {}
                    $scope.portfolioTarget = response.targets;
                    var fy = response.fy;

                    _.each($scope.portfolioList,function (po) {
                        if($scope.teamObjEmailId && $scope.teamObjEmailId[po.headEmailId]){
                            po.head = $scope.teamObjEmailId[po.headEmailId].name;
                        } else {
                            po.head = "Assign"
                        }

                        po.headTarget = 0;
                        po.allTarget = 0;

                        if(response.targets[po.name]){
                            po.headTarget = response.targets[po.name].head;
                            po.allTarget = response.targets[po.name].all;
                        }

                        po.headTarget = getAmountInThousands(po.headTarget,2)
                        po.allTarget = getAmountInThousands(po.allTarget,2)

                        var totalAmt = 0;
                        _.each(po.targets[0].values,function (tr) {
                            if(tr.name !== "Total"){
                                totalAmt = totalAmt+tr.amount;
                            }
                        });

                        if(fy){
                            $scope.fy = moment(fy.start).format("MMM YYYY")+"-"+moment(moment(fy.end).subtract(1,"day")).format("MMM YYYY")
                        }

                        if(po.targets[0].values.length == 12){
                            po.targets[0].values.push({
                                amount:totalAmt,
                                monthYear: "Total",
                                valueFormatted:getAmountInThousands(totalAmt,2)
                            })
                        }

                        _.each($scope.FyMonths,function (mo,index) {
                            if(!mo.monthlyTotal){
                                mo.monthlyTotal = 0
                            }

                            if(mo.name !== "Total"){
                                mo.monthlyTotal = mo.monthlyTotal+po.targets[0].values[index].amount;
                                mo.monthFormatted = getAmountInThousands(mo.monthlyTotal,2);
                            }

                            if(mo.name === "Total"){
                                mo.monthlyTotal = mo.monthlyTotal+totalAmt;
                                mo.monthFormatted = getAmountInThousands(mo.monthlyTotal,2);
                            }
                        });
                    });

                    $scope.dataLoaded = true;
                })
        }
    }

    $("#required").on('click',function() {

        var fromPortfolios = false;

        if($scope.currentSelectionObj && $scope.currentSelectionObj.fromPortfolios){
            fromPortfolios = true;
        }

        var url = "/corporate/region/required";

        if($scope.currentSelection === "Business Units") {
            url = "/corporate/bu/required";
        }

        if($scope.currentSelection === "Products") {
            url = "/corporate/product/required";
        }

        if($scope.currentSelection === "Verticals") {
            url = "/corporate/vertical/required";
        }

        if(fromPortfolios){
            url = "/corporate/portfolio/required";
        }
        
        $scope.activityLogsCache.push({
            what:"Mandatory selection settings set to - "+ $("#required").prop('checked') +" for "+ $scope.currentSelection,
            action:"Set",
            byEmailId: $scope.liuData.emailId
        });

        $http.post(url,{required:$("#required").prop('checked'),name:$scope.currentSelection,companyId:$scope.companyResponse._id})
            .success(function (response) {
            })
    });

    $scope.tableHeaderName = 'opportunityName';
    $scope.reverse = true;

    $scope.sortBy = function(tableHeaderName) {
        $scope.reverse = ($scope.tableHeaderName === tableHeaderName) ? !$scope.reverse : false;
        $scope.tableHeaderName = tableHeaderName;
    }

    $scope.makeContactMandatory = function(type){
        var url = '/corporate/admin/update/customer/setting';
        $http.post(url,$scope.contactSettings)
            .success(function (response) {
            });

    }
    $scope.editXr = function (currency) {
        currency.editThis = true;
    }

    $scope.transferOpps = function () {
        var opps = _.filter($scope.oppList,"select")
        startOppTransfer($scope,$http,opps)
    }

    $scope.deleteOpps = function () {
        var opps = _.filter($scope.oppList,"select")

        if (window.confirm('This operation will permanently delete the opportunties. Are you sure you want to delete the selected opportunties?')) {
            $http.post("/corporate/admin/delete/opps",_.map(opps,"opportunityId"))
                .success(function (response) {
                    alert("The selected opportunities were successfully deleted.")
                    $scope.fetchForUser($scope.selectedUser);
                })
        }
    }

    $scope.openPopup = function (){
        $scope.showPopup = !$scope.showPopup
    }

    $scope.searchForUser = function (searchText){
        $scope.teamMembers = [];
        _.each($scope.data.teamMembers,function (el) {
            if(_.includes(el.firstName.toLowerCase(),searchText.toLowerCase()) || _.includes(el.lastName.toLowerCase(),searchText.toLowerCase()) || _.includes(el.emailId.toLowerCase(),searchText.toLowerCase())){
                $scope.teamMembers.push(el)
            }
        })
    }

    $scope.fetchForUser = function (userId) {
        $scope.selectedUser = userId;
        $scope.selectedUserFrom = $scope.teamObj[userId];
        $scope.forOppRefresh = userId
        fetchOpps($scope,$http,$scope.teamObj[userId]);
    }

    $scope.toggleSelectionOpps = function () {
        $scope.oppList.forEach(function (el) {
            el.select = !el.select
        })
    }

    $scope.saveXr = function (currency) {

        $http.post("/corporate/admin/update/xr/currency",currency)
            .success(function (response) {
                currency.editThis = false;
            });
    }

    $scope.staticCurrencyList = getStaticCurrencyList()

    $scope.selectThisCurrency = function(currency){
        $scope.selectedCurrency = currency
        $scope.currencyInput = currency.name
        $scope.currencyLists = []
    }

    $scope.searchForCurrency = function(currencyInput){
        $scope.currencyLists = [];
        _.each($scope.staticCurrencyList,function (el) {
            if(_.includes(el.name.toLowerCase(),currencyInput.toLowerCase()) || _.includes(el.symbol.toLowerCase(),currencyInput.toLowerCase())){
                $scope.currencyLists.push(el)
            }
        });

        if(currencyInput.length == 0){
            $scope.currencyLists = [];
        }
    }

    $scope.updatePrimaryCurrency = function (currency) {
        if($scope.currencyList.length == 1){
            alert("At least one primary currency is required");
            $scope.currencyList.forEach(function (el) {
                el.isPrimary = true;
            })
        } else {

            $scope.currencyList.forEach(function (el) {
                if(el.name !== currency.name){
                    el.isPrimary = false;
                }
            })

            $http.post("/corporate/admin/update/primary/currency",$scope.currencyList)
                .success(function (response) {
                    getCompanyDetails($scope,$http);
                });
        }
    }


    $scope.updateDefaultRenewalType = function (type) {

        $scope.typeList.forEach(function (el) {
            if(el.name !== type.name){
                el.isDefaultRenewalType = false;
            }
        })

        $http.post("/corporate/admin/update/default/type",$scope.typeList)
            .success(function (response) {
                getCompanyDetails($scope,$http);
            });
    }

    $scope.updateRenewalType = function (type) {

        $http.post("/corporate/admin/update/renewal/type",[type])
            .success(function (response) {
                getCompanyDetails($scope,$http);
            });
    }

    $scope.solutionRequiredStatus = function () {
        $http.post("/corporate/admin/update/opp/settings/required",{solutionRequired:$scope.solutionRequired})
            .success(function (response) {
            });
    }

    $scope.typeRequiredStatus = function () {
        $http.post("/corporate/admin/update/opp/settings/required",{typeRequired:$scope.typeRequired})
            .success(function (response) {
            });
    }

    $scope.buRequiredStatus = function () {
        $http.post("/corporate/admin/update/opp/settings/required",{businessUnitRequired:$scope.businessUnitRequired})
            .success(function (response) {
            });
    }

    $scope.updateCurrencyStatus = function (currency) {

        if(!currency.isPrimary){
            $http.post("/corporate/admin/update/primary/currency",$scope.currencyList)
                .success(function (response) {
                });
        } else {
            alert("Can not deactivate a primary currency.")
            currency.isDeactivated = false;
        }
    }

    $scope.deleteThisCurrency = function (currency) {

        if(currency.isPrimary || $scope.currencyList.length == 1){
            alert("Can not delete primary currency. Please set another currency as primary to delete this currency.")
        } else if($scope.currencyList.length == 1){
            alert("Can not delete the only currency.")
        } else {

            $http.post("/corporate/admin/delete/currency",currency)
                .success(function (response) {
                    $scope.currencyList = $scope.currencyList.filter(function (el) {
                        return el.name !== currency.name
                    })
                });
        }
    }

    $scope.deleteThisReason = function (item) {
        item.companyId = $scope.companyObject._id;
        $http.post("/corporate/close/reason/remove",item)
            .success(function (response) {
                $scope.closeReasons = $scope.closeReasons.filter(function (el) {
                    return el.name !== item.name
                })
            });
    }

    $scope.deleteThisVertical = function (item) {
        item.companyId = $scope.companyObject._id;
        item.vertical = item.name;
        $http.post("/corporate/vertical/remove",item)
            .success(function (response) {
                $scope.verticalList = $scope.verticalList.filter(function (el) {
                    return el.name !== item.name
                })
            });
    }

    $scope.deleteThisSource = function (item) {
        item.companyId = $scope.companyObject._id;
        item.source = item.name;
        $http.post("/corporate/source/remove",item)
            .success(function (response) {
                $scope.sourceList = $scope.sourceList.filter(function (el) {
                    return el.name !== item.name
                })
            });
    }

    $scope.deleteThisProduct = function (item) {
        item.companyId = $scope.companyObject._id;
        item.product = item.name;
        $http.post("/corporate/product/remove",item)
            .success(function (response) {
                $scope.productList = $scope.productList.filter(function (el) {
                    return el.name !== item.name
                })
            });
    }

    $scope.deleteThisRegion = function (item) {
        item.companyId = $scope.companyObject._id;

        if (window.confirm('Do you wish to delete the region?. There might be opportunities linked to this region')) {
            $http.post("/corporate/geolocation/remove",item)
                .success(function (response) {
                    Communicator.prototype.refreshData();
                });
        }
    }

    $scope.deleteThisSolution = function (item) {
        item.companyId = $scope.companyObject._id;
        if (window.confirm('Do you wish to delete the solution?. There might be opportunities linked to this solution')) {
            $http.post("/corporate/solution/remove",item)
                .success(function (response) {
                    $scope.solutionList = $scope.solutionList.filter(function (el) {
                        return el.name !== item.name
                    })
                });
        }
    }

    $scope.deleteThisType = function (item) {
        item.companyId = $scope.companyObject._id;

        if($scope.typeList.length == 1) {
            $scope.typeList.forEach(function (el) {
                el.isDefaultRenewalType = true
                el.isTypeRenewal = true
            })
            alert("There should be at least one default type")
        } else {

            if (window.confirm('Do you wish to delete the type?. There might be opportunities linked to this type')) {
                $http.post("/corporate/type/remove", item)
                    .success(function (response) {
                        $scope.typeList = $scope.typeList.filter(function (el) {
                            return el.name !== item.name
                        })
                    });

            }
        }

    }

    $scope.deleteThisBu = function (item) {
        item.companyId = $scope.companyObject._id;
        if (window.confirm('Do you wish to delete the business unit?. There might be opportunities linked to this business unit')) {
            $http.post("/corporate/bu/remove", item)
                .success(function (response) {
                    $scope.businessUnits = $scope.businessUnits.filter(function (el) {
                        return el.name !== item.name
                    })
                });
        }
    }

    $scope.addCurrency = function () {

        if($scope.selectedCurrency && !$scope.selectedCurrency.name){
            alert("Please select valid currency")
        } else {

            var currency = {name:$scope.selectedCurrency.name,symbol:$scope.selectedCurrency.symbol,xr:$("#xr").val()};

            if($scope.currencyList.length == 0){
                currency.xr = 1
                currency.isPrimary = true
            }

            if(!currency.name){
                alert("Please select a currency")
            } else if(!currency.xr){
                alert("Please enter exchange rate for the currency")
            } else {

                $http.post("/corporate/currency/update",currency)
                    .success(function (response) {
                        $("#currency").val("");
                        $("#xr").val("")
                        $scope.selectedCurrency = {};
                        Communicator.prototype.refreshData();
                    });
            }
        }
    }

    $scope.addSolution = function () {

        if($("#solution").val() && $("#solution").val().length>0 && !checkDuplicates($scope.solutionList,$("#solution").val())){
            $http.post("/corporate/solution/add",{name:$("#solution").val(),companyId:$scope.companyObject._id})
                .success(function (response) {
                    $scope.solutionList.push({
                        name:$("#solution").val()
                    })
                    $("#solution").val("");
                });
        } else {
            alert("Please enter a valid value")
        }
    }

    $scope.addType = function () {

        if($("#type").val() && $("#type").val().length>0 && !checkDuplicates($scope.typeList,$("#type").val())){

            $http.post("/corporate/type/add",{name:$("#type").val(),companyId:$scope.companyObject._id})
                .success(function (response) {
                    $scope.typeList.push({
                        name:$("#type").val()
                    })
                    $("#type").val("");
                });
        } else {
            alert("Please enter a valid and unique value")
        }

    }

    $scope.addBu = function () {

        if($("#bu").val() && $("#bu").val().length>0 && !checkDuplicates($scope.businessUnits,$("#bu").val())){

            $http.post("/corporate/bu/add",{name:$("#bu").val(),companyId:$scope.companyObject._id})
                .success(function (response) {
                    $scope.businessUnits.push({
                        name:$("#bu").val()
                    })
                    $("#bu").val("");
                });
        } else {
            alert("Please enter a valid and unique value")
        }
    }

    $scope.selectUserForOppTransfer = function (user) {
        $scope.selectedUser = user;
        $scope.searchText = user.firstName +" "+ user.lastName +"("+ user.emailId + ")";

        $scope.teamMembers = [];
    }

});

function getVerticalItems(callback) {
    callback([{
        name:"BFSI"
    },{
        name:"IT Services"
    },{
        name:"Govt"
    }])
}

function getTeamMembers($scope,$http,cb){

    $scope.data = {
        model: null
    };

    $http.get("/corporate/admin/all/team/members")
        .success(function (response) {
            $scope.data.teamMembers = response;

            $scope.teamObj = {};
            $scope.teamObjEmailId = {};

            $scope.data.teamMembers.forEach(function (el) {
                el.name = el.firstName +" "+el.lastName;
                $scope.teamObj[el._id] = el;
                $scope.teamObjEmailId[el.emailId] = el;
            });

            $scope.data.teamMembers.sort(function (a,b) {
                if(a.firstName < b.firstName) return -1;
                if(a.firstName > b.firstName) return 1;
                return 0;
            })

            cb()
        })
}

function resetOtherSelection($scope,currentSelection) {
    $scope.menu.forEach(function (el) {
        if(el.name !== currentSelection.name){
            el.selected = ""
        }
    })
}

$(document).ready(function(){
    var companyObject = JSON.parse($('#company-object-hidden').text());
    var AWScredentials,s3bucketVideo,s3bucketLogo,s3bucketDoc,videoUrl,logoUrl,docUrl;
    var videoName,logoName,docName;
    getCompanyInfo();
    getAWSCredentials();

    function getAWSCredentials() {
        $.ajax({
            url: '/getAwsCredentials',
            type: 'GET',
            datatye: 'JSON',
            success: function (credentials) {
                AWScredentials = credentials;
                AWS.config.update(credentials);
                AWS.config.region = credentials.region;
                s3bucketVideo = new AWS.S3({ params: {Bucket: credentials.companyVideosBucket} });
                s3bucketLogo = new AWS.S3({ params: {Bucket: credentials.companyLogosBucket} });
                s3bucketDoc = new AWS.S3({ params: {Bucket: credentials.bucket} });
                videoUrl = 'https://' + AWScredentials.companyVideosBucket + '.s3.amazonaws.com/';
                logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
            }
        })
    }

    $('#commitWeekHour').datetimepicker({
        datepicker:false,
        format:'H',
        onSelectDate: function (dp, $input){
        }
    });

    function getCompanyInfo(){

        $.ajax({
            url:'/corporate/company/'+companyObject._id,
            type:'GET',
            datatype:'JSON',
            success:function(companyProfile){
                if(companyProfile){
                    companyObject = companyProfile;

                    $("#regionRequired").prop('checked', companyObject.opportunitySettings.regionRequired);
                    $("#productRequired").prop('checked', companyObject.opportunitySettings.productRequired);
                    $("#verticalRequired").prop('checked', companyObject.opportunitySettings.verticalRequired);
                    $("#sourceRequired").prop('checked', companyObject.opportunitySettings.sourceRequired);
                    $("#netGrossMargin").prop('checked', companyObject.netGrossMargin);
                    $("#rmNotif").prop('checked', companyObject.notificationForReportingManager);
                    $("#orgHeadNotif").prop('checked', companyObject.notificationForOrgHead);
                    $("#renewalStatus").prop('checked', companyObject.opportunitySettings.renewalStatusRequired);

                    $("#sfClientId").val(companyProfile.salesForceSettings.clientId || '');
                    $("#sfClientSecret").val(companyProfile.salesForceSettings.clientSecret || '');

                    $("#companyName").val(companyProfile.companyName || '');
                    $("#emailDomains").val(companyProfile.emailDomains.join());
                    $("#description").val(companyProfile.description || '');
                    $("#presentationTitle").val(companyProfile.presentationTitle || '');
                    $("#presentationType").val(companyProfile.presentationType || 'none');

                    if(checkRequired(companyProfile.socialInfo)){
                        $("#facebookPage").val(companyProfile.socialInfo.facebookPage || '')
                        $("#linkedinPage").val(companyProfile.socialInfo.linkedinPage || '')
                        $("#twitterPage").val(companyProfile.socialInfo.twitterPage || '')
                    }

                    if(checkRequired(companyProfile.quickStats)){
                        $("#website").val(companyProfile.quickStats.website || '');
                        $("#founded-in").val(companyProfile.quickStats.foundedIn || '');
                        $("#head-quarters").val(companyProfile.quickStats.headQuarters || '');
                        $("#industry").val(companyProfile.quickStats.industry || '');
                        $("#company-size").val(companyProfile.quickStats.companySize || '')
                        $("#usp").val(companyProfile.quickStats.usp || '');
                    }
                    /* VIDEOS */
                    if(checkRequired(companyProfile.videos) && companyProfile.videos.length){
                        $("#exist-video").show();
                        $("#add-video").hide();
                        $("#video-change").show();
                        var videoName = checkRequired(companyProfile.videos[0].videoName) ? companyProfile.videos[0].videoName : companyProfile.videos[0].url
                        $("#video-name").html(videoName+'&nbsp;&nbsp;&nbsp;')
                    }
                    else{
                        $("#exist-video").hide();
                        $("#video-change").hide();
                        $("#add-video").show();
                    }

                    /*FY*/

                    if(companyProfile.fyMonth){
                        $("#selectedMonth").text(companyProfile.fyMonth)
                        $("#month").val(companyProfile.fyMonth);
                    }

                    if(companyProfile.commitDay){
                        $("#commitWeekday").val(companyProfile.commitDay);
                    }

                    if(companyProfile.monthCommitCutOff){
                        $("#commitMonthly").val(companyProfile.monthCommitCutOff);
                    }

                    if(companyProfile.qtrCommitCutOff){
                        $("#commitQtrly").val(companyProfile.qtrCommitCutOff);
                    }

                    if(companyProfile.commitHour){
                        $("#commitWeekHour").val(companyProfile.commitHour);
                    }

                    /* DOCUMENTS */
                    if(checkRequired(companyProfile.documents) && companyProfile.documents.length){
                        $("#exist-doc").show();
                        $("#add-doc").hide();
                        $("#doc-change").show();
                        var dName = checkRequired(companyProfile.documents[0].documentName) ? companyProfile.documents[0].documentName : companyProfile.documents[0].url;
                        $("#doc-name").html(dName+'&nbsp;&nbsp;&nbsp;')
                    }
                    else{
                        $("#exist-doc").hide();
                        $("#doc-change").hide();
                        $("#add-doc").show();
                    }

                    /* SLIDE */
                    if(checkRequired(companyProfile.slides) && companyProfile.slides.length){
                        $("#exist-slide").show();
                        $("#add-slide").hide();
                        $("#slide-change").show();
                        var slideName = checkRequired(companyProfile.slides[0].slideName) ? companyProfile.slides[0].slideName : companyProfile.slides[0].url;
                        $("#slide-name").html(slideName+'&nbsp;&nbsp;&nbsp;')
                    }
                    else{
                        $("#exist-slide").hide();
                        $("#slide-change").hide();
                        $("#add-slide").show();
                    }

                    if(checkRequired(companyProfile.logo) && checkRequired(companyProfile.logo.imageName)){
                        $("#header-logo-name").show()
                        $("#header-logo-name").html('&nbsp;&nbsp;&nbsp;'+companyProfile.logo.imageName+'&nbsp;&nbsp;&nbsp;&nbsp;');
                        $("#header-logo").hide()
                        $("#header-logo-edit").show()
                    }
                    else{
                        $("#header-logo-name").hide();
                        $("#header-logo-edit").hide()
                        //  $("#header-logo").val('Upload')
                    }

                    if(checkRequired(companyProfile.latestNews) && companyProfile.latestNews.length > 0){
                        var newsList = $("#latestNews input");
                        for(var news=0; news<companyProfile.latestNews.length; news++){
                            var n = $(newsList[news]).val(companyProfile.latestNews[news].news);
                        }
                    }
                }
            }
        })
    }

    $("#saveDetails").on('click',function(){
        var companyName = $("#companyName").val();
        var emailDomains = $("#emailDomains").val();
        var description = $("#description").val();
        var presentationTitle = $("#presentationTitle").val();
        var website = $("#website").val();
        var foundedIn = $("#founded-in").val();
        var headQuarters = $("#head-quarters").val();
        var industry = $("#industry").val();
        var companySize = $("#company-size").val();
        var presentationType = $("#presentationType").val();
        var usp = $("#usp").val();
        var newsList = $("#latestNews input");

        var newsArr = [];
        for(var news=0; news<newsList.length; news++){

            var n = $(newsList[news]).val();
            if(checkRequired(n)){
                newsArr.push({news:n});
            }
        }
        emailDomains = emailDomains.replace(/\s/g,'');
        var emailDomainsArr = [];
        if(checkRequired(emailDomains)){
            if(contains(emailDomains,',')){
                emailDomains = emailDomains.split(',');
                for(var d=0; d<emailDomains.length; d++) {
                    if (checkRequired(emailDomains[d])) {
                        emailDomainsArr.push(emailDomains[d])
                    }
                }
            }
            else{
                emailDomainsArr.push(emailDomains);
            }
        }

        if(emailDomainsArr.length > 0){
            if(checkRequired(companyName)){
                var obj = {
                    companyName:companyName,
                    companyId:companyObject._id,
                    emailDomains:emailDomainsArr,
                    description:description,
                    presentationTitle:presentationTitle,
                    presentationType:presentationType,
                    latestNews:newsArr,
                    socialInfo:{
                        facebookPage:$("#facebookPage").val(),
                        linkedinPage:$("#linkedinPage").val(),
                        twitterPage:$("#twitterPage").val()
                    },
                    quickStats:{
                        website:website,
                        foundedIn:foundedIn,
                        headQuarters:headQuarters,
                        industry:industry,
                        companySize:companySize,
                        usp:usp
                    }
                };
                validatePresentationType(obj)

            }
            else{
                alert("Please provide company name.");
            }
        }
        else{
            alert("Please provide email domains.");
        }
    });

    function validatePresentationType(obj){
        updateCompanyInfo(JSON.stringify(obj))
    }

    function updateCompanyInfo(updateInfo){
        $.ajax({
            url:'/corporate/company/update',
            type:'POST',
            datatype:'JSON',
            data:{data:updateInfo},
            success:function(success){
                if(success){
                    alert('Company details successfully updated.');
                }
                else{
                    alert('An error occurred while updating company details.');
                }
            }
        })
    }

    function contains(str, subStr) {
        return str.indexOf(subStr) != -1;
    }

    /* Adding contacts */
    $("#add-contact").on('click',function(){
        var uniqueName_emailId = $("#unique-name").val();
        var contactFor = $("#contact-for").val();
        if(checkRequired(uniqueName_emailId) && checkRequired(contactFor)){
            var obj = {
                companyId:companyObject._id,
                uniqueName_emailId:uniqueName_emailId,
                contactFor:contactFor
            };
            $.ajax({
                url:'/corporate/company/add/contact',
                type:'POST',
                datatype:'JSON',
                data:obj,
                traditional:true,
                success:function(result){
                    if(result){
                        alert("Contact successfully added.")
                    }
                    else{
                        alert("An error occurred. Please try again.");
                    }
                }
            })
        }
        else{
            alert("Either you missed Unique name or email id or contact for.")
        }
    });

    /* Adding Salesforce */
    $("#saveSfSettings").on('click',function(){
        var sfClientId = $("#sfClientId").val();
        var sfClientSecret = $("#sfClientSecret").val();
        if(checkRequired(sfClientSecret) && checkRequired(sfClientId)){
            var obj = {
                companyId:companyObject._id,
                clientId:sfClientId.replace(/\s/g,''),
                clientSecret:sfClientSecret.replace(/\s/g,'')
            };
            $.ajax({
                url:'/corporate/company/add/salesforce',
                type:'POST',
                datatype:'JSON',
                data:obj,
                traditional:true,
                success:function(result){
                    if(result){
                        alert("Salesforce details successfully added.")
                    }
                    else{
                        alert("An error occurred. Please try again.");
                    }
                }
            })
        }
        else{
            alert("Either you missed Unique name or email id or contact for.")
        }
    });
    /* Adding contacts */

    /* Upload video */
    $("#video-upload-button").on('click',function(){
        $("#video-upload").trigger('click');
    });

    $("#video-change").on('click',function(){
        $("#exist-video").hide();
        $("#video-change").hide();
        $("#add-video").show();
    });

    $("#save-video-link").on('click',function(){
        var url = $("#video-link-box").val();
        //var name = $("#video-name-box").val();

        if(checkRequired(url)){
            if(contains(url,'https://www.youtube.com/watch?v')){
                var regex = /[?&]([^=#]+)=([^&#]*)/g,
                    params = {},
                    match;
                while(match = regex.exec(url)) {
                    params[match[1]] = match[2];
                }
                if(checkRequired(params.v) || checkRequired(params.V)){
                    url = 'https://www.youtube.com/embed/'+params.v || params.V
                }
            }

            var video = {
                companyId:companyObject._id,
                videoName:'',
                sourceType:'link',
                url:url,
                awsKey:null
            }
            $.ajax({
                url:'/corporate/company/add/video',
                type:'POST',
                datatype:'JSON',
                data:video,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#exist-video").show();
                        $("#add-video").hide();
                        $("#video-name").html(url+'&nbsp;&nbsp;&nbsp;&nbsp;');
                        $("#video-change").show()
                        getCompanyInfo()
                        alert('Video successfully updated');
                    }
                    else{
                        getCompanyInfo()
                        alert("An error occurred. Please try again.")
                        $("#exist-video").hide();
                        $("#add-video").show();
                    }
                }
            })
        }
        else alert("You missed either url or name of video");
    });

    var uploadFlag = false;
    $("#video-upload").on('change',function(){
        var file = $(this)[0].files[0];
        if(checkRequired(file)){
            if (file) {

                var dateNow = new Date();
                var d = dateNow.getDate();
                var m = dateNow.getMonth();
                var y = dateNow.getFullYear();
                var hours = dateNow.getHours();
                var min = dateNow.getMinutes();
                var sec = dateNow.getSeconds();
                var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
                var dIdentity = timeStamp+'_'+file.name;
                var docNameTimestamp = dIdentity.replace(/\s/g,'');
                videoName = file.name;
                $("#exist-video").show();
                $("#add-video").hide();
                uploadFlag = false;
                var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
                var request = s3bucketVideo.putObject(params);
                $("#video-change").hide()
                request.on('httpUploadProgress', function (progress) {
                    var pr = Math.round(progress.loaded / progress.total *100);
                    $("#video-name").html('('+pr+'%)'+file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    if(pr == 100){
                        $("#video-name").html(file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    }
                });
                request.send(function (err, data) {
                    if(err){
                        uploadFlag = true;
                        $("#exist-video").hide();
                        $("#add-video").show();
                        alert("Video upload failed")
                    }
                    else{
                        uploadFlag = true;
                        videoUrl = videoUrl+docNameTimestamp;
                        var video = {
                            companyId:companyObject._id,
                            videoName:videoName,
                            sourceType:'video',
                            url:videoUrl,
                            awsKey:docNameTimestamp
                        }

                        $.ajax({
                            url:'/corporate/company/add/video',
                            type:'POST',
                            datatype:'JSON',
                            data:video,
                            traditional:true,
                            success:function(result){
                                if(result){
                                    alert('Video successfully uploaded');
                                    videoUrl = 'https://' + AWScredentials.companyVideosBucket + '.s3.amazonaws.com/';
                                }
                                else{
                                    alert("Video upload failed")
                                    $("#exist-video").hide();
                                    $("#add-video").show();
                                    videoUrl = 'https://' + AWScredentials.companyVideosBucket + '.s3.amazonaws.com/';
                                }
                            }
                        })
                    }
                });

            }
            else {
                alert("Please select file to upload.");
            }
        }
    });
    /* Upload video End*/

    /* Upload file */
    $("#file-input-upload").on('click',function(){
        $("#file-upload").trigger('click');
    });

    $("#file-input-change").on('click',function(){
        $("#file-upload").trigger('click');
    });

    $("#setFYMonth").on('click',function(){
        var month = $("#month").val()

        $.ajax({
            url:'/corporate/company/set/fym',
            type:'POST',
            datatype:'JSON',
            data:{month:month},
            traditional:true,
            success:function(result){
                getCompanyInfo()
            }
        })
    });

    $("#setCommitWeekday").on('click',function(){
        var commitWeekday = $("#commitWeekday").val()
        var commitWeekHour = $("#commitWeekHour").val()

        $.ajax({
            url:'/corporate/company/set/commitweek',
            type:'POST',
            datatype:'JSON',
            data:{commitWeekday:commitWeekday,commitWeekHour:commitWeekHour},
            traditional:true,
            success:function(result){
                alert("Commit day updated successfully.");
                getCompanyInfo()
            }
        })
    });

    $("#setCommitMonthly").on('click',function(){
        var commitMonthly = $("#commitMonthly").val()

        $.ajax({
            url:'/corporate/company/set/commit/month',
            type:'POST',
            datatype:'JSON',
            data:{commitMonthly:parseInt(commitMonthly)},
            traditional:true,
            success:function(result){
                alert("Month Commit cutoff day updated successfully.");
                getCompanyInfo()
            }
        })
    });

    $("#setCommitQtrly").on('click',function(){
        var commitQtrly = $("#commitQtrly").val()

        $.ajax({
            url:'/corporate/company/set/commit/qtr',
            type:'POST',
            datatype:'JSON',
            data:{commitQtrly:parseFloat(commitQtrly)},
            traditional:true,
            success:function(result){
                alert("Quarter Commit cutoff day updated successfully.");
                getCompanyInfo()
            }
        })
    });

    $("#file-upload").on('change',function(){
        var file = $(this)[0].files[0];

    });
    /* Upload file */

    /* Upload logo */

    $("#header-logo-edit").on('click',function(){
        $(this).hide()
        $("#header-logo-name").hide();
        $("#header-logo").show();
    });

    $("#header-logo").on('click',function(){
        $("#header-logo-upload").trigger('click');
    });

    $("#header-logo-upload").on('change',function(){
        var file = $(this)[0].files[0];

        if (file) {
            var url = URL.createObjectURL(this.files[0]);
            var img = new Image;
            img.onload = function() {
                if(img.width <=190 && img.width >= 180 && img.height >= 40 && img.height <= 50){
                    uploadEventImage(file);
                }
                else {
                    uploadEventImage(file);
                    alert("Your current image size is :"+img.width+'px x '+img.height+'px. Recommended image size is 184px x 48px');
                }
            };
            img.src = url;
        }
        else {
            alert("Please select file to upload.");
        }
    });

    function uploadEventImage(file){
        var dateNow = new Date();
        var d = dateNow.getDate();
        var m = dateNow.getMonth();
        var y = dateNow.getFullYear();
        var hours = dateNow.getHours();
        var min = dateNow.getMinutes();
        var sec = dateNow.getSeconds();
        var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
        var dIdentity = timeStamp+'_'+file.name;
        var docNameTimestamp = dIdentity.replace(/\s/g,'');
        docName = file.name;
        $("#header-logo-name").show()
        var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
        var request = s3bucketLogo.putObject(params);
        request.on('httpUploadProgress', function (progress) {
            var pr = Math.round(progress.loaded / progress.total *100);
            $("#header-logo-name").html('('+pr+'%)'+file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
            if(pr == 100){
                $("#header-logo-name").html(file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
            }
        });
        request.send(function (err, data) {
            if(err){
                uploadFlag = true;
                logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                $("#header-logo-name").hide()
                alert("An error occurred. Please try again.")

            }
            else{
                logoUrl = logoUrl+docNameTimestamp;
                var logo = {
                    companyId:companyObject._id,
                    imageName:file.name,
                    sourceType:'image',
                    url:logoUrl,
                    awsKey:docNameTimestamp
                }

                $.ajax({
                    url:'/corporate/company/add/logo',
                    type:'POST',
                    datatype:'JSON',
                    data:logo,
                    traditional:true,
                    success:function(result){
                        if(result){
                            $("#header-logo").val('Change');
                            alert('Logo successfully uploaded');
                            logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                        }
                        else{
                            $("#header-logo-name").hide();
                            $("#header-logo").val('Upload');
                            alert("Logo upload failed");
                            logoUrl = 'https://' + AWScredentials.companyLogosBucket + '.s3.amazonaws.com/';
                        }
                    }
                })
            }
        });
    }

    /* Upload logo */

    /* Upload Document */
    $("#doc-upload-button").on('click',function(){
        $("#doc-upload").trigger('click');
    });

    $("#doc-change").on('click',function(){
        $("#exist-doc").hide();
        $("#doc-change").hide();
        $("#add-doc").show();
    });

    var uploadFlagDoc = false;
    $("#doc-upload").on('change',function(){
        var file = $(this)[0].files[0];

        if(checkRequired(file)){
            if (file && file.type == "application/pdf") {

                var dateNow = new Date();
                var d = dateNow.getDate();
                var m = dateNow.getMonth();
                var y = dateNow.getFullYear();
                var hours = dateNow.getHours();
                var min = dateNow.getMinutes();
                var sec = dateNow.getSeconds();
                var timeStamp = y+':'+m+':'+d+':'+hours+':'+min+':'+sec;
                var dIdentity = timeStamp+'_'+file.name;
                var docNameTimestamp = dIdentity.replace(/\s/g,'');
                docName = file.name;
                $("#exist-doc").show();
                $("#add-doc").hide();
                uploadFlagDoc = false;
                var params = {Key: docNameTimestamp, ContentType: file.type, Body: file};
                var request = s3bucketDoc.putObject(params);
                $("#doc-change").hide()
                request.on('httpUploadProgress', function (progress) {
                    var pr = Math.round(progress.loaded / progress.total *100);
                    $("#doc-name").html('('+pr+'%)'+file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    if(pr == 100){
                        $("#doc-name").html(file.name+'&nbsp;&nbsp;&nbsp;&nbsp;');
                    }
                });
                request.send(function (err, data) {
                    if(err){
                        uploadFlag = true;
                        $("#exist-doc").hide();
                        $("#add-doc").show();
                        alert("Document upload failed")
                    }
                    else{
                        uploadFlag = true;
                        docUrl = docUrl+docNameTimestamp;
                        var doc = {
                            companyId:companyObject._id,
                            documentName:docName,
                            url:docUrl,
                            awsKey:docNameTimestamp
                        };

                        $.ajax({
                            url:'/corporate/company/add/document',
                            type:'POST',
                            datatype:'JSON',
                            data:doc,
                            traditional:true,
                            success:function(result){
                                if(result){
                                    alert('Document successfully uploaded');
                                    docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
                                }
                                else{
                                    alert("Document upload failed")
                                    $("#exist-doc").hide();
                                    $("#add-doc").show();
                                    docUrl = 'https://' + AWScredentials.bucket + '.s3.amazonaws.com/';
                                }
                            }
                        })
                    }
                });
            }
            else {
                alert("Please select (.pdf) file to upload.");
            }
        }
    });
    /* Upload Document End*/

    /* Add Slide */
    $("#slide-change").on('click',function(){
        $("#exist-slide").hide();
        $("#slide-change").hide();
        $("#add-slide").show();
    });

    $("#addRegion").on('click',function(){
        addRegion($("#region").val(),companyObject._id,companyObject)
    });

    $("#addCloseReasons").on('click',function(){
        addReason($("#reason").val(),companyObject._id,companyObject)
    });

    $("#addVertical").on('click',function(){
        addVertical($("#vertical").val(),companyObject._id,companyObject)
    });

    $("#addProduct").on('click',function(){
        addProduct($("#product").val(),companyObject._id,companyObject)
    });

    $("#addSource").on('click',function(){
        addSource($("#source").val(),companyObject._id,companyObject)
    });

    $("body").on('click', '#regionRequired', function (e) {
        var isChecked = $('#regionRequired').is(":checked")

        $.ajax({
            url:'/corporate/region/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#productRequired', function (e) {
        var isChecked = $('#productRequired').is(":checked")

        $.ajax({
            url:'/corporate/product/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#sourceRequired', function (e) {
        var isChecked = $('#sourceRequired').is(":checked")

        $.ajax({
            url:'/corporate/source/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#verticalRequired', function (e) {
        var isChecked = $('#verticalRequired').is(":checked")

        $.ajax({
            url:'/corporate/vertical/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){

                }
            }
        });
    });

    $("body").on('click', '#netGrossMargin', function (e) {
        var isChecked = $('#netGrossMargin').is(":checked")

        $.ajax({
            url:'/corporate/netgrossmargin/required',
            type:'POST',
            datatype:'JSON',
            data:{required:isChecked,companyId:companyObject._id},
            traditional:true,
            success:function(result){
                if(result){
                    alert("Successfully settings saved")
                }
            }
        });
    });

    $("body").on('click', '#orgHeadNotif', function (e) {
        var isChecked2 = $('#rmNotif').is(":checked")
        var isChecked = $('#orgHeadNotif').is(":checked")
        updateDetails({notificationForReportingManager:isChecked2,notificationForOrgHead:isChecked,companyId:companyObject._id});
    });

    $("body").on('click', '#rmNotif', function (e) {
        var isChecked2 = $('#rmNotif').is(":checked")
        var isChecked = $('#orgHeadNotif').is(":checked")
        updateDetails('/corporate/opp/email/notif',{notificationForReportingManager:isChecked2,notificationForOrgHead:isChecked,companyId:companyObject._id});
    });

    $("body").on('click', '#renewalStatus', function (e) {
        var renewalStatus = $('#renewalStatus').is(":checked")
        updateDetails('/corporate/admin/renewalStatus',{renewalStatus:renewalStatus,companyId:companyObject._id});
    });

    function updateDetails(url,updateObj){

        $.ajax({
            url:url,
            type:'POST',
            datatype:'JSON',
            data:updateObj,
            traditional:true,
            success:function(result){
                if(result){
                    alert("Successfully settings saved")
                }
            }
        });
    }

    $("#save-slide-link").on('click',function(){
        var url = $("#slide-link-box").val();
        //var name = $("#video-name-box").val();

        if(checkRequired(url) && contains(url,'slideshare')){

            var slide = {
                companyId:companyObject._id,
                slideName:'',
                sourceType:'link',
                url:url,
                awsKey:null
            };

            $.ajax({
                url:'/corporate/company/add/slide',
                type:'POST',
                datatype:'JSON',
                data:slide,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#exist-slide").show();
                        $("#add-slide").hide();
                        $("#slide-name").html(url+'&nbsp;&nbsp;&nbsp;&nbsp;');
                        $("#slide-change").show();

                        alert('Slide successfully updated');
                    }
                    else{

                        alert("An error occurred. Please try again.")
                        $("#exist-slide").hide();
                        $("#add-slide").show();
                    }
                }
            })
        }
        else alert("You missed either url or name of Slide");
    });

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    function addRegion(region,companyId,companyDetails) {
        var geoLocations = {};
        geoLocations.region = region;
        geoLocations.companyId = companyId;

        if(region && region != "" && region != " " && !checkDuplicates(companyDetails.geoLocations,region,"region")){
            $.ajax({
                url:'/corporate/geolocation/add',
                type:'POST',
                datatype:'JSON',
                data:geoLocations,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#region").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        } else {
            alert("Please enter a valid and unique value")
        }
    }

    function addReason(reason,companyId,companyDetails) {
        var geoLocations = {};
        geoLocations.reason = reason;
        geoLocations.companyId = companyId;

        if(reason && reason != "" && reason != " " && !checkDuplicates(companyDetails.closeReasons,reason)){
            $.ajax({
                url:'/corporate/close/reason/add',
                type:'POST',
                datatype:'JSON',
                data:geoLocations,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#reason").val("")
                        getCompanyInfo()
                    }
                }
            });
        } else {
            alert("Please enter a valid and unique value")
        }
    }

    function addProduct(product,companyId,companyDetails) {
        var products = {};
        products.product = product;
        products.companyId = companyId;

        if(product && product != "" && product != " " && !checkDuplicates(companyDetails.productList,product)){
            $.ajax({
                url:'/corporate/product/add',
                type:'POST',
                datatype:'JSON',
                data:products,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#product").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        } else {
            alert("Please enter a valid and unique value")
        }
    }

    function addSource(product,companyId,companyDetails) {
        var products = {};
        products.source = product;
        products.companyId = companyId;

        if(product && !checkDuplicates(companyDetails.sourceList,product)){
            $.ajax({
                url:'/corporate/source/add',
                type:'POST',
                datatype:'JSON',
                data:products,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#source").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        } else {
            alert("Please enter a valid and unique value")
        }
    }

    function addVertical(vertical,companyId,companyDetails) {
        var verticals = {};
        verticals.vertical = vertical;
        verticals.companyId = companyId;

        if(vertical && vertical != "" && vertical != " " && !checkDuplicates(companyDetails.verticalList,vertical)){
            $.ajax({
                url:'/corporate/vertical/add',
                type:'POST',
                datatype:'JSON',
                data:verticals,
                traditional:true,
                success:function(result){
                    if(result){
                        $("#vertical").val("")
                        fetchCompanyInfoAgain()
                    }
                }
            });
        } else {
            alert("Please enter a valid and unique value")
        }
    }

    function fetchCompanyInfoAgain(functionType) {
        $.ajax({
            url: '/corporate/company/' + companyObject._id,
            type: 'GET',
            datatype: 'JSON',
            success: function (companyProfile) {
                if (companyProfile) {
                    companyObject = companyProfile;
                    Communicator.prototype.refreshData();
                }
            }
        });
    }

});

function paintMenu($scope,portfolios) {

    $scope.menu = [{
        name:"Products",
        selected:""
    }, {
        name:"Regions",
        selected:""
    },{
        name:"Verticals",
        selected:""
    }
    // ,{
    //     name:"Solutions",
    //     selected:""
    // },{
    //     name:"Sources",
    //     selected:""
    // },{
    //     name:"Type",
    //     selected:""
    // }
    ,{
        name:"Business Units",
        selected:""
    }]

    $scope.menu = $scope.menu.concat(portfolios);

    $scope.menu.sort(function (a,b) {
        if(a.name < b.name) return -1;
        if(a.name > b.name) return 1;
        return 0;
    })
}

function getPortfolios($scope,$http,callabck) {
    $http.get('/corporate/admin/portfolio/all')
        .success(function (response) {
            callabck(response)
        });
}

function getCompanyDetails($scope,$http,portfolioType,dontChangeMenu){
    $scope.companyObject = JSON.parse($('#company-object-hidden').text());
    $http.get('/corporate/company/'+$scope.companyObject._id)
        .success(function (response) {
            $scope.companyResponse = response;

            if(!$scope.currentSelection && !dontChangeMenu){
                $scope.currentSelection = $scope.menu[0].name;
                $scope.menu[0].selected = "selected";

                $scope.selectMenu($scope.menu[0])
            }
        })
}

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
        return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

function getSelectionStart(o) {
    if (o.createTextRange) {
        var r = document.selection.createRange().duplicate()
        r.moveEnd('character', o.value.length)
        if (r.text == '') return o.value.length
        return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
}

function getStaticCurrencyList(){

    var cObj = {
        "AED": "United Arab Emirates Dirham",
        "AFN": "Afghan Afghani",
        "ALL": "Albanian Lek",
        "AMD": "Armenian Dram",
        "ANG": "Netherlands Antillean Guilder",
        "AOA": "Angolan Kwanza",
        "ARS": "Argentine Peso",
        "AUD": "Australian Dollar",
        "AWG": "Aruban Florin",
        "AZN": "Azerbaijani Manat",
        "BAM": "Bosnia-Herzegovina Convertible Mark",
        "BBD": "Barbadian Dollar",
        "BDT": "Bangladeshi Taka",
        "BGN": "Bulgarian Lev",
        "BHD": "Bahraini Dinar",
        "BIF": "Burundian Franc",
        "BMD": "Bermudan Dollar",
        "BND": "Brunei Dollar",
        "BOB": "Bolivian Boliviano",
        "BRL": "Brazilian Real",
        "BSD": "Bahamian Dollar",
        "BTC": "Bitcoin",
        "BTN": "Bhutanese Ngultrum",
        "BWP": "Botswanan Pula",
        "BYN": "Belarusian Ruble",
        "BZD": "Belize Dollar",
        "CAD": "Canadian Dollar",
        "CDF": "Congolese Franc",
        "CHF": "Swiss Franc",
        "CLF": "Chilean Unit of Account (UF)",
        "CLP": "Chilean Peso",
        "CNH": "Chinese Yuan (Offshore)",
        "CNY": "Chinese Yuan",
        "COP": "Colombian Peso",
        "CRC": "Costa Rican ColÃ³n",
        "CUC": "Cuban Convertible Peso",
        "CUP": "Cuban Peso",
        "CVE": "Cape Verdean Escudo",
        "CZK": "Czech Republic Koruna",
        "DJF": "Djiboutian Franc",
        "DKK": "Danish Krone",
        "DOP": "Dominican Peso",
        "DZD": "Algerian Dinar",
        "EGP": "Egyptian Pound",
        "ERN": "Eritrean Nakfa",
        "ETB": "Ethiopian Birr",
        "EUR": "Euro",
        "FJD": "Fijian Dollar",
        "FKP": "Falkland Islands Pound",
        "GBP": "British Pound Sterling",
        "GEL": "Georgian Lari",
        "GGP": "Guernsey Pound",
        "GHS": "Ghanaian Cedi",
        "GIP": "Gibraltar Pound",
        "GMD": "Gambian Dalasi",
        "GNF": "Guinean Franc",
        "GTQ": "Guatemalan Quetzal",
        "GYD": "Guyanaese Dollar",
        "HKD": "Hong Kong Dollar",
        "HNL": "Honduran Lempira",
        "HRK": "Croatian Kuna",
        "HTG": "Haitian Gourde",
        "HUF": "Hungarian Forint",
        "IDR": "Indonesian Rupiah",
        "ILS": "Israeli New Sheqel",
        "IMP": "Manx pound",
        "INR": "Indian Rupee",
        "IQD": "Iraqi Dinar",
        "IRR": "Iranian Rial",
        "ISK": "Icelandic KrÃ³na",
        "JEP": "Jersey Pound",
        "JMD": "Jamaican Dollar",
        "JOD": "Jordanian Dinar",
        "JPY": "Japanese Yen",
        "KES": "Kenyan Shilling",
        "KGS": "Kyrgystani Som",
        "KHR": "Cambodian Riel",
        "KMF": "Comorian Franc",
        "KPW": "North Korean Won",
        "KRW": "South Korean Won",
        "KWD": "Kuwaiti Dinar",
        "KYD": "Cayman Islands Dollar",
        "KZT": "Kazakhstani Tenge",
        "LAK": "Laotian Kip",
        "LBP": "Lebanese Pound",
        "LKR": "Sri Lankan Rupee",
        "LRD": "Liberian Dollar",
        "LSL": "Lesotho Loti",
        "LYD": "Libyan Dinar",
        "MAD": "Moroccan Dirham",
        "MDL": "Moldovan Leu",
        "MGA": "Malagasy Ariary",
        "MKD": "Macedonian Denar",
        "MMK": "Myanma Kyat",
        "MNT": "Mongolian Tugrik",
        "MOP": "Macanese Pataca",
        "MRO": "Mauritanian Ouguiya (pre-2018)",
        "MRU": "Mauritanian Ouguiya",
        "MUR": "Mauritian Rupee",
        "MVR": "Maldivian Rufiyaa",
        "MWK": "Malawian Kwacha",
        "MXN": "Mexican Peso",
        "MYR": "Malaysian Ringgit",
        "MZN": "Mozambican Metical",
        "NAD": "Namibian Dollar",
        "NGN": "Nigerian Naira",
        "NIO": "Nicaraguan CÃ³rdoba",
        "NOK": "Norwegian Krone",
        "NPR": "Nepalese Rupee",
        "NZD": "New Zealand Dollar",
        "OMR": "Omani Rial",
        "PAB": "Panamanian Balboa",
        "PEN": "Peruvian Nuevo Sol",
        "PGK": "Papua New Guinean Kina",
        "PHP": "Philippine Peso",
        "PKR": "Pakistani Rupee",
        "PLN": "Polish Zloty",
        "PYG": "Paraguayan Guarani",
        "QAR": "Qatari Rial",
        "RON": "Romanian Leu",
        "RSD": "Serbian Dinar",
        "RUB": "Russian Ruble",
        "RWF": "Rwandan Franc",
        "SAR": "Saudi Riyal",
        "SBD": "Solomon Islands Dollar",
        "SCR": "Seychellois Rupee",
        "SDG": "Sudanese Pound",
        "SEK": "Swedish Krona",
        "SGD": "Singapore Dollar",
        "SHP": "Saint Helena Pound",
        "SLL": "Sierra Leonean Leone",
        "SOS": "Somali Shilling",
        "SRD": "Surinamese Dollar",
        "SSP": "South Sudanese Pound",
        "STD": "SÃ£o TomÃ© and PrÃ­ncipe Dobra (pre-2018)",
        "STN": "SÃ£o TomÃ© and PrÃ­ncipe Dobra",
        "SVC": "Salvadoran ColÃ³n",
        "SYP": "Syrian Pound",
        "SZL": "Swazi Lilangeni",
        "THB": "Thai Baht",
        "TJS": "Tajikistani Somoni",
        "TMT": "Turkmenistani Manat",
        "TND": "Tunisian Dinar",
        "TOP": "Tongan Pa'anga",
        "TRY": "Turkish Lira",
        "TTD": "Trinidad and Tobago Dollar",
        "TWD": "New Taiwan Dollar",
        "TZS": "Tanzanian Shilling",
        "UAH": "Ukrainian Hryvnia",
        "UGX": "Ugandan Shilling",
        "USD": "United States Dollar",
        "UYU": "Uruguayan Peso",
        "UZS": "Uzbekistan Som",
        "VEF": "Venezuelan BolÃ­var Fuerte",
        "VND": "Vietnamese Dong",
        "VUV": "Vanuatu Vatu",
        "WST": "Samoan Tala",
        "XAF": "CFA Franc BEAC",
        "XAG": "Silver Ounce",
        "XAU": "Gold Ounce",
        "XCD": "East Caribbean Dollar",
        "XDR": "Special Drawing Rights",
        "XOF": "CFA Franc BCEAO",
        "XPD": "Palladium Ounce",
        "XPF": "CFP Franc",
        "XPT": "Platinum Ounce",
        "YER": "Yemeni Rial",
        "ZAR": "South African Rand",
        "ZMW": "Zambian Kwacha",
        "ZWL": "Zimbabwean Dollar"
    }

    var currencyList = [];

    for(var key in cObj){
        currencyList.push({
            symbol:key,
            name:cObj[key]
        })
    }

    return currencyList;

}

function fetchOpps($scope,$http,user) {
    $scope.loadingOpps = true;
    $http.get("/corporate/admin/fetch/opps/for/users?userId="+user._id)
        .success(function (response) {
            $scope.oppList = response;
            if($scope.oppList[0]){
                $scope.oppList.forEach(function (op) {
                    op.zone = op.geoLocation && op.geoLocation.zone?op.geoLocation.zone:""
                    op.town = op.geoLocation && op.geoLocation.town?op.geoLocation.town:""
                });
            }

            $scope.loadingOpps = false;
        })
}

function startOppTransfer($scope,$http,oppRemoveList){

    var contacts = [];
    var toTransfer = [];

    var liuData = {};

    _.each($scope.data.teamMembers,function (el) {
        if(el.corporateAdmin){
            liuData = el;
            $scope.liuData = el;
        }
    })

    _.each(oppRemoveList,function (op) {
        contacts.push(op.contactEmailId)

        if(op.partners && op.partners.length>0){
            contacts = contacts.concat(_.map(op.partners,"emailId"))
        }

        if(op.decisionMakers && op.decisionMakers.length>0){
            contacts = contacts.concat(_.map(op.decisionMakers,"emailId"))
        }

        if(op.influencers && op.influencers.length>0){
            contacts = contacts.concat(_.map(op.influencers,"emailId"))
        }

        toTransfer.push({
            transferredBy:"",
            from:op.userEmailId,
            fromUserId:op.userId,
            to:$scope.selectedUser._id,
            opps:op.opportunityId,
            contacts:_.uniq(contacts),
            emailId:$scope.selectedUser.emailId,
            reportingManagerForUser1:op.userId,
            reportingManagerForUser2:$scope.selectedUser.userId
        })
    });

    var obj = {
        toTransfer:_.uniqBy(toTransfer,"opps")
    };

    transferOpportunities($scope,$http,obj,liuData,oppRemoveList)
}

function transferOpportunities($scope,$http,obj,liuData,oppRemoveList){

    $http.post("/corporate/admin/transfer/opps",obj)
        .success(function (response) {



            var opps = _.map(oppRemoveList,"opportunityName")
            var subject = opps.length + " Opportunities transferred."

            var intro = ""

            $scope.add_cc = [];

            var oppString = "";

            _.each(opps,function (op,index) {
                oppString = oppString+op+",\n"
            })

            $scope.add_cc.push($scope.selectedUserFrom.emailId)
            $scope.add_cc.push($scope.selectedUser.emailId)

            var receiverName = $scope.selectedUser.firstName
            var oppDetails = "The Following opportunities have been transferred from " +$scope.selectedUserFrom.emailId+" to "+$scope.selectedUser.emailId
                +'\n'+ oppString

                +'\n\n\n'+getSignature(liuData.firstName+' '+liuData.lastName,liuData.designation,liuData.companyName,liuData.publicProfileUrl)
                // +'\n\n Powered by Relatas';

            intro = "Hi "+receiverName+", "+intro
            var body = intro+",\n\n"+oppDetails;

            $scope.teamMembers = [];
            $scope.showPopup = false;

            sendEmail($scope,$http,subject,body,null,null,liuData._id,$scope.selectedUser.emailId,null,liuData,null,$scope.selectedUser.emailId)
        });
}

function sendEmail($scope,$http,subject,body,prevMessage,share,userId,emailId,mobileNumber,liu,contactDetails,internalMailRecName){

    var respSentTimeISO = moment().format();

    if(prevMessage && prevMessage.dataObj != null){
        body = body+'\n\n\n'+prevMessage.dataObj.bodyContent;
        $scope.reminder = prevMessage.dataObj.compose_email_remaind
    }

    var obj = {
        email_cc:$scope.add_cc.length>0?$scope.add_cc:null,
        receiverEmailId:internalMailRecName?emailId:$scope.cEmailId,
        receiverName:internalMailRecName,
        message:body,
        subject:subject,
        receiverId:"",
        docTrack:true,
        trackViewed:true,
        remind:$scope.reminder,
        respSentTime:respSentTimeISO,
        isLeadTrack:false,
        newMessage:prevMessage?false:true
    };

    if(internalMailRecName){
        obj.receiverEmailId = emailId
        obj.receiverName = internalMailRecName
        obj.receiverId = ''
    }

    if(prevMessage && prevMessage != null && prevMessage.updateReplied){
        obj.updateReplied = true;
        obj.refId = prevMessage.dataObj.interaction.refId
    }

    //Used for Outlook
    if(prevMessage && prevMessage.dataObj && prevMessage.dataObj.interaction && prevMessage.dataObj.interaction.refId){
        obj.id = prevMessage.dataObj.interaction.refId
    }

    $http.post("/messages/send/email/single/web",obj)
        .success(function(response){

            alert("Selected opps transferred successfully")
            fetchOpps($scope,$http,$scope.teamObj[$scope.forOppRefresh])
            if(response.SuccessCode){
                $scope.selectedUser = {}
            }
            else{
            }
        })

}

function getSignature(fullName,designation,companyName,uName) {
    return "---\n"+fullName+"\n"+designation+", "+companyName+"\n"+"www.relatas.com/"+uName+'\n ---\n Powered by Relatas';
}

function checkDuplicates(list,val,isRegion){

    var contains = false;
    _.each(list,function (el) {

        if(isRegion){

            if(el.region.toLowerCase() == val.toLowerCase()){
                contains = true;
            }
        } else {
            if(el.name.toLowerCase() == val.toLowerCase()){
                contains = true;
            }
        }
    })

    return contains;
}

function getAmountInThousands(num,digits,ins) {
    num = parseFloat(num);

    var si = [
        { value: 1E18, symbol: " E" },
        { value: 1E15, symbol: " P" },
        { value: 1E12, symbol: " T" },
        { value: 1E9,  symbol: " B" },
        { value: 1E6,  symbol: " M" },
        { value: 1E3,  symbol: " K" }
    ]

    // ins = true;

    if(ins){
        // digits = 2;
        si = [
            { value: 1E7,  symbol: " Cr" },
            { value: 1E5,  symbol: " L" },
            { value: 1E3,  symbol: " K" }
        ]
    }

    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;

    for (i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
        }
    }

    if(num){
        return num.toFixed(digits).replace(rx, "$1");
    } else {
        return num;
    }

}

relatasApp.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

function getMonthsBetweenDates(from,to) {
    var startDate = moment(from).add(2, 'days');
    var endDate = moment(to).add(2, 'days');

    var result = [];

    if (endDate.isBefore(startDate)) {
        throw "End date must be greated than start date."
    }

    var currentDate = startDate.clone();

    while (currentDate.isBefore(endDate)) {
        result.push(new Date(currentDate));
        currentDate.add(1, 'month');
    }

    return result;
}

function closeAllDropDownsAndModals($scope,id,isModal) {
    $(document).mouseup(function (e) {
        var container = $(id);

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            $scope.$apply(function (){
                _.each($scope.portfolioList,function (po) {
                    po.allUsersList = false;
                    po.showSomeMessage = false;
                });

                // _.each($scope.subportfolios,function (po) {
                //     po.showAccess = false;
                // });

                $scope.showUsers = false;
            })
        }
    });
}
