$(document).ready(function(){

    var companyObject = JSON.parse($('#company-object-hidden').text());

    checkIfAuthError();
    function checkIfAuthError(){
        $.ajax({
            url:'/corporate/linkedinError/get',
            type:'GET',
            success:function(response){
                if(response == 'invalidEmailId'){
                    showMessagePopUp("Bummer. Looks like your emailId not valid. Can you please try again with your "+companyObject.companyName+" ID.",'error');
                    clearLinkedInError();
                }else if(response == 'linkedinAuthError'){
                    showMessagePopUp("Bummer. Looks like a LinkedIn server issue. Can you please try again. If the problem persists, please email us at hello@relatas.com.",'error');
                    clearLinkedInError();
                }else if(response == 'notCorporateUser'){
                    showMessagePopUp("Bummer. Looks like you are not a corporate user. Can you please try again with your corporate account.",'error');
                }
            }
        })
    }

    function clearLinkedInError(){
        $.ajax({
            url:'/corporate/linkedinError/clear',
            type:'GET',
            success:function(response){

            }
        })
    }
    var popover = null;
    function showMessagePopUp(message,msgClass)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        popover = $("#user-name");
        $(".arrow").addClass("invisible");
        //  $(".popover").addClass('popupError')
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        $("#message").text(message)
        $("#message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        $(".popover").hide();
    });
});