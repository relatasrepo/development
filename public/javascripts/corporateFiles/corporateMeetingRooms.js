
var reletasApp = angular.module('reletasApp', ['ngRoute']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf

}]);
var table;
$(document).ready(function(){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        'positionClass':'toast-top-full-width',
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "8000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    var companyObject = JSON.parse($('#company-object-hidden').text());
    var adminId;
    applyDataTable();
    getCompanyInfo();

    function getCompanyInfo(){
        $.ajax({
            url:'/corporate/company/'+companyObject._id,
            type:'GET',
            datatype:'JSON',
            success:function(companyProfile){
                if(companyProfile){

                    companyObject = companyProfile;

                    getCurrentCompanyMembers();
                }
            }
        })
    }

    function getCurrentCompanyMembers(){
        $.ajax({
            url:'/corporate/admin/meetingrooms/company/'+companyObject._id,
            type:'GET',
            datatype:'JSON',
            success:function(response){
                createTableData(response)
            }
        })
    }

    function createTableData(members){
        if(members && members.length > 0){
            for(var i=0; i<members.length; i++){

                var arr = [
                    members[i].firstName || '',
                    members[i].lastName || '',
                    members[i].publicProfileUrl,
                    members[i].emailId,
                    moment(members[i].createdDate).format("DD MMM YYYY")
                ];
                addRowsToTable(arr);
            }
        }
    }

    function checkRequired(data) {
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else {
            return true;
        }
    }

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }


    function applyDataTable(){
        table = $('#meetingRooms').DataTable({
            "dom": '<"top"iflp<"clear">>',
            "order": [[ 3, "asc" ]],

            "oLanguage": {
                "sEmptyTable": "There are no Meeting Rooms"
            }
        });
    }

});

reletasApp.controller("create_meeting_room",function($scope, $http){

    $scope.createMeetingRoom = function(firstName,lastName,emailId,publicProfileUrl){

        if(!checkRequired(firstName)){
            toastr.error("Please enter First Name")
        }
        else if(!checkRequired(lastName)){
            toastr.error("Please enter Last Name")
        }
        else if(!checkRequired(emailId)){
            toastr.error("Please enter Email ID")
        }
        else if(!$scope.validateEmailId(emailId)){
            toastr.error("Please enter valid Email ID")
        }
        else if(!checkRequired(publicProfileUrl)){
            toastr.error("Please enter Unique Name")
        }
        else{
            var companyObject = JSON.parse($('#company-object-hidden').text());
            var data = {
                firstName:firstName,
                lastName:lastName,
                emailId:emailId,
                publicProfileUrl:publicProfileUrl,
                companyName: companyObject.companyName,
                profilePicUrl:'/images/default.png',
                registeredUser: true,
                corporateUser: true,
                resource:true,
                userType:'registered',
                createdDate:new Date(),
                companyId:companyObject._id
            };
            // check emailId exists or not
            $http.get('/checkEmail/'+data.emailId.trim())
                .success(function(response){
                    if(!response){
                        // already exists
                        toastr.error("Email ID already exists. Please change it")
                    }
                    else{
                        $scope.createMeetingRoom_final(data);
                    }
                });
        }
    };

    $scope.createMeetingRoom_final = function(data){
        checkIdentityAvailability(data.publicProfileUrl,function(isOk){
            if(isOk){

                $http.post('/corporate/admin/create/meetingrooms/new', data)
                    .success(function(response){
                        if(response){
                            toastr.success("Meeting room successfully created")
                                $scope.firstName = ''
                                $scope.lastName = ''
                                $scope.emailId = ''
                                $scope.publicProfileUrl = ''
                            var arr = [
                                response.firstName || '',
                                response.lastName || '',
                                response.publicProfileUrl,
                                response.emailId,
                                moment(response.createdDate).format("DD MMM YYYY")
                            ];
                            addRowsToTable(arr);
                        }
                        else{
                            toastr.error("An error occurred. Please try again")
                        }
                    })
            }
            else{
                toastr.error("Unique name already exists. Please change it");
            }
        })
    };

    $scope.validateEmailId = function(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return pattern.test(emailText);
    };

    function addRowsToTable(rowArr){
        table.row.add( rowArr ).draw();
    }

    $scope.validateUniqueName = function(uName){
        checkIdentityAvailability(uName)
    }
    var flag = false;
    function checkIdentityAvailability(identity,callback){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity),
            userId:$scope.userId
        };
        $http.post('/checkUrl',details)
            .success(function(response){
                if(response == true){
                    $scope.publicProfileUrl = getValidRelatasIdentity(identity)

                    i = 1
                    if(flag){
                        flag = false;
                        toastr.warning("Your selected name is not available. Next best name suggested is:  "+identity+"")
                    }
                    if(callback){callback(true)}
                }
                else{
                    if(callback){
                        callback(false)
                    }
                    else a(identity)
                }
            });
    }
    var i = 1;
    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+i
        i++;
        flag = true;
        checkIdentityAvailability(identity)
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g,'');
        return rIdentity;
    }
});

