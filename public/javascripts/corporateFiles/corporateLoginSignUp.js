$(document).ready(function(){
  var companyObject = JSON.parse($('#company-object-hidden').text());

  var loginFlag = false;

    checkIfAuthError();
    function checkIfAuthError(){
        $.ajax({
            url:'/corporate/linkedinError/get',
            type:'GET',
            success:function(response){

                if(response == 'otherCompanyUser'){
                    getErrorCompanyProfile()
                    //showMessagePopUp("Bummer. Looks like your emailId not valid. Can you please try again with your "+companyObject.companyName+" ID.",'error');
                    clearLinkedInError();
                }else
                if(response == 'invalidEmailId'){
                    showMessagePopUp("Bummer. Looks like your emailId not valid. Can you please try again with your "+companyObject.companyName+" ID.",'error');
                    clearLinkedInError();
                }else if(response == 'linkedinAuthError'){
                    showMessagePopUp("Bummer. Looks like a LinkedIn server issue. Can you please try again. If the problem persists, please email us at hello@relatas.com.",'error');
                    clearLinkedInError();
                }else if(response == 'notCorporateUser'){
                    showMessagePopUp("Bummer. Looks like you are not a corporate user. Can you please try again with your corporate account.",'error');
                }
            }
        })
    }

    function getErrorCompanyProfile(){
        $.ajax({
            url:'/corporate/error/companyProfile',
            type:'GET',
            success:function(response){

                if(response != false && validateRequired(response)){
                    showMessagePopUp("Bummer. Looks like you are trying to log in wrong way. Can you please try log in to "+response.url+" .",'error');
                }
            }
        })
    }

    function clearLinkedInError(){
        $.ajax({
            url:'/corporate/linkedinError/clear',
            type:'GET',
            success:function(response){

            }
        })
    }

    $("#generate").on('click',function(){

        var emailId = $("#corporate-email-id").val();
        var firstName = $("#corporate-first-name").val();
        var lastName = $("#corporate-last-name").val();
        if(validateEmailField(emailId) && validateCorporateEmailId(emailId,companyObject.companyName)){
            if(!validateRequired(firstName)){
                showMessage('Enter first name');
            }
            else if(!validateRequired(lastName)){
                showMessage('Enter last name')
            }else
            sendKey({emailId:emailId,firstName:firstName,lastName:lastName});
        }
    });

    function sendKey(detailsObj){
        var obj = {
             emailId:detailsObj.emailId,
             firstName:detailsObj.firstName,
             lastName:detailsObj.lastName,
             companyId:companyObject._id,
             companyName:companyObject.companyName
        };

            $.ajax({
                url:'/corporate/generateKey',
                type:'POST',
                datatype:'JSON',
                data:obj,
                success:function(response){
                    if(response == 'exist'){
                        showMessage("You are already added to "+companyObject.companyName+". Please login. ");
                    }
                    else if(response){
                        loginFlag = true;
                       showMessage("You are successfully added to "+companyObject.companyName+" <br> An email has been sent to your email ID.<br> Please check your email.",true);
                    }
                    else showMessage("An error occurred. Please try again.",'error');
                }
            });
    }

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {

            return true;
        }
        else {
            showMessage('Enter valid emailId')
            return false;
        }
    }

    function validateCorporateEmailId(emailId,companyName){
        var emailDomains = companyObject.emailDomains;

         var eArr = emailId.split('@');
         var isValid = false;

        for(var domain=0; domain < emailDomains.length; domain++){
            if(eArr[1] == emailDomains[domain]){
                isValid =true;
            }
        }

         if(isValid){
             return true;
         }else{
             showMessage('Enter valid '+companyObject.companyName+' ID');

         }
    }

    function showMessage(msg,html){
        if(html){
            $("#msg").html(msg);
        }else
        $("#msg").text(msg);
        setTimeout(function(){
            $("#msg").text('Register with your corporate email address');
        },2000)
    }

    var popover = null;
    function showMessagePopUp(message,msgClass,isHtml)
    {
        var html = _.template($("#message-popup").html())();
        $("#company-object-hidden").popover({
            title: "Login Failed" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#company-object-hidden").popover('show');
        popover = $("#company-object-hidden");
        $(".arrow").addClass("invisible");
        //  $(".popover").addClass('popupError')
        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }

        isHtml ? $("#message").html(message) : $("#message").text(message);

        $("#message").focus();
    }

    $("body").on("click","#closePopup-message",function(){
        popover.popover('destroy');
        if(loginFlag){
            loginFlag = false;
            window.location.replace('/login');
        }
    });

    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }
});