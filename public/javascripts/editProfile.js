$(document).ready(function() {

    $.ajaxSetup({ cache: false });

    var googleUserData,facebookUserData,twitterUserData,linkedinUserData;
    var pic;
    var userProfile;
    var timezone;

    $('#home').on('click',function(){
        window.location.replace('/');
    });

    $('#calendar').on('click',function(){
        window.location.replace('/home');
    });

    $("#changeProfileImageBut").on("click",function(){
        $("#selectShareFile").trigger('click');
    });

    var docName;
    $('#selectShareFile').on('change',function(){
        $("#changeProfileImgForm").submit()

    });

    getProfile();
    getEditProfileSession();

    function getUserProfile(){
        $.ajax({
            url:'/userProfileEdit',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(profile){
                checkMandatoryFields(profile)
                bindPofileInfo(profile);
            }
        });
    }

    function getValidUniqueUrl(uniqueName){
        var url = window.location+''.split('/');
        var patt = new RegExp(url[2]);
        if(patt.test(uniqueName)){

            url = uniqueName.split('/');
            for (var i = url.length - 1; i >= 0; i--) {
                if (url[i] == 'u') {
                    uniqueName = url[i+1];
                }
            }
            return uniqueName;

        }else {
            return uniqueName;
        }
    }

    var isRegisteredUser = false;

    function getProfile(){
        $.ajax({
            url:'/userProfileEdit',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(profile){
                userProfile = profile;
                if(profile.registeredUser == false){
                    $("#terms").show();

                    $('#publicProfileUrl').attr("readonly",false);
                    checkIdentityAvailability(getValidUniqueUrl(profile.publicProfileUrl).replace(/\s/g,''))
                    $("#goToDashboard").attr('href','#');
                    $("#goToCalendar").attr('href','#');
                    $("#goToContacts").attr('href','#');
                    $("#goToDocuments").attr('href','#');
                    $("#goHome").attr('href','#');
                    isRegisteredUser = true;
                }

                $('#profilePic').attr('src', profile.profilePicUrl || 'images/default.png');
                $('#profilePicEdit').attr('src', profile.profilePicUrl || 'images/default.png');
                imagesLoaded('#profilePic',function(instance,img) {
                    if(instance.hasAnyBroken){
                        $('#profilePic').attr("src",'/images/default.png');
                    }
                });
                imagesLoaded('#profilePicEdit',function(instance,img) {
                    if(instance.hasAnyBroken){
                        $('#profilePicEdit').attr("src",'/images/default.png');
                    }
                });
                $("#user-msg-name").text(userProfile.firstName);
                facebook(profile.facebook);
                linkedin(profile.linkedin);
                twitter(profile.twitter);
                google(profile.google);
            }
        });
    }

    function msgSignIn(){
        if(isRegisteredUser){
            showMessagePopUp("Please fill in the mandatory details on this page before you navigate.",'tip');
        }
    }

    $("#goToDashboard").on('click',function(){
        msgSignIn()
    })
    $("#goToCalendar").on('click',function(){
        msgSignIn()
    })
    $("#goToDocuments").on('click',function(){
        msgSignIn()
    })
    $("#goToContacts").on('click',function(){
        msgSignIn()
    })

    var i = 1;
    var flag = false;
    function checkIdentityAvailability(identity){

        var details = {
            publicProfileUrl:getValidRelatasIdentity(identity)
        }
        $.ajax({
            url:'/checkUrlEdit',
            type:'POST',
            datatype:'JSON',
            data:details,
            traditional:true,
            success:function(result){
                if(result == 'error'){

                }else
                if(result == true){
                    var id = getValidRelatasIdentity(identity)
                    i=1
                    $("#publicProfileUrl").val(id)
                    if(flag){
                        flag = false;
                        showMessagePopUp("Your selected name is not available. Next best name suggested is:  "+id+"",'tip')
                    }
                }
                else{
                    a(identity)
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }

    function a(identity){
        identity = getValidRelatasIdentity(identity)+''+i
        i++;
        flag = true;
        checkIdentityAvailability(identity)
    }

    function validateRelatasIdentity(rIdentity){

        var regex = new RegExp("^[a-zA-Z0-9._ ]*$");
        if(regex.test(rIdentity) && rIdentity != ''){
            return true;
        }
        else{
            return false;
        }
    }

    function getValidRelatasIdentity(rIdentity) {
        rIdentity = rIdentity.toLowerCase();
        rIdentity = rIdentity.replace(/\s/g,'');
        return rIdentity;

    }


    $("#publicProfileUrl").focusout(function(){
        if(userProfile.registeredUser == false){
            var text = $("#publicProfileUrl").val()
            checkIdentityAvailability(text)
        }
    });

    function storeFormDataInSession(){

        $.ajax({
            url: '/storeEditProfileSession',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: formData(),
            success: function (msg) {

            },
            error: function (event, request, settings) {

            },
            timeout: 20000
        });
    }

    function getEditProfileSession(){
        $.ajax({
            url:'/getEditProfileSession',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(session){

                if(!validateRequired(session)){
                    getUserProfile();
                }
                else
                {
                    isLinkedinUpdated(session)
                }
            }
        });
    }


    function isLinkedinUpdated(session){
        $.ajax({
            url:'/isLinedinUpdate',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(response){
                if(response){
                    removeProfileSession()
                    getUserProfile();
                }
                else
                {
                    bindSessionInfo(session);
                }

            }
        });
    }

    function removeProfileSession(){
        $.ajax({
            url:'/removeEditProfileSession',
            type:'GET',
            datatype:'JSON',
            traditional:true,
            success:function(response) {

            }
        })
    }

    function checkMandatoryFields(userProfile){
        var message = 'Thank you for visiting again. Your profile is not complete. Please update the mandatory fields in the profile page before proceeding.<br><br>';
        var mFlag = false;
        if(!validateRequired(userProfile.firstName)){
            message+= 'First Name<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.lastName)){
            message += 'Last Name<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.publicProfileUrl)){
            message += 'Unique Name<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.emailId)){
            message += 'Email Id<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.designation)){
            message += 'Designation<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.companyName)){
            message += 'Company Name<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.mobileNumber)){
            message += 'Mobile Number<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.location)){
            message += 'Location<br>'
            mFlag = true;
        }
        if(!validateRequired(userProfile.timezone)){
            message += 'Select your time zone and click save changes.';
            mFlag = true;

        }else{
            if(!validateRequired(userProfile.timezone.name)){
                message += 'Select your time zone and click save changes.';
                mFlag = true;
            }
        }
        if(mFlag){
            showMessagePopUp(message,'error','20',true);
        }
    }

    function bindPofileInfo(data){

        if (validateRequired(data)) {
            var mail = data.emailId || '';
            var code1 = ''+data.mobileNumber+'';
            data.publicProfileUrl = getValidUniqueUrl(data.publicProfileUrl);
            pic = data.profilePicUrl;
            $('#firstName').val(data.firstName || ''),
                $('#lastName').val(data.lastName || ''),
                $('#publicProfileUrl').val(data.publicProfileUrl || ''),
                $('#designation').val(data.designation || ''),
                $('#companyName').val(data.companyName || ''),
                $('#emailId').val(mail.toLowerCase()),
                $('#mobile').val(data.mobileNumber || ''),
                $('#skypeId').val(data.skypeId || ''),
                $('#location').val(data.location || '')
                if(validateRequired(data.birthday)){
                    $('#day').val(data.birthday.day == 0 ? 'Day' : getDay(data.birthday)),
                        $('#month').val(data.birthday.month == 0 ? 'Month' : getMonth(data.birthday)),
                        $('#year').val(data.birthday.year == 0 ? 'Year' : getYear(data.birthday))
                }else{
                    $('#day').val('Day'),
                    $('#month').val('Month'),
                    $('#year').val('Year')
                }

                if(validateRequired(data.workHours)){
                    $("#workHoursStart").val(data.workHours.start || '00:00');
                    $("#workHoursEnd").val(data.workHours.end || '00:00');
                }

                if(validateRequired(data.timezone)){
                    if(data.timezone.updated){
                        $("#timezone").val(data.timezone.name);
                    }else $("#timezone").val('Asia/Kolkata');
                }else{
                    $("#timezone").val('Asia/Kolkata');
                }

               $('#profilePic').attr('src', data.profilePicUrl || '');
               $('#profileDescription').val(data.profileDescription);

                if(data.calendarAccess.calendarType == "selectPublic"){
                    $('#selectPublic').prop('checked',true);
                }else  $('#public').prop('checked',true);

                $('#startTime1').val(data.calendarAccess.timeOne.start || '00:00');
                $('#endTime1').val(data.calendarAccess.timeOne.end || '00:00');
                $('#startTime2').val(data.calendarAccess.timeTwo.start || '00:00');
                $('#endTime2').val(data.calendarAccess.timeTwo.end || '00:00');

            if(data.sendDailyAgenda == false){
                $('#sendDailyAgenda').prop('checked',false);
            }
            else{
                $('#sendDailyAgenda').prop('checked',true);
            }
            if(data.serviceLogin == 'relatas'){
                $("#editPass").css({display:'block'})
                $("#password").val("@defaultPass@");
                $("#retypePassword").val("@defaultPass@");
            }

            if(!validateRequired(data.profilePrivatePassword)){
                $("#profilePrivate").prop('checked',false);
            }
            else{
                $('#profilePrivatePassword').val('@pass@');
                $("#profilePrivate").prop('checked',true);
            }
        }
    }

function getZone(){
    var date = moment().format();
    var zone1 = 'GMT'+date.substr(19,date.length);
    return getTimeZone()+'('+zone1+')';
}

    function facebook(facebook){
        if (validateRequired(facebook)) {
            if (validateRequired(facebook.id)) {
                facebookUserData = facebook
                $('#addFacebookAccountH3').text(facebook.name);
                $('#addFacebookAccount').attr('checked','checked');
                $( "<style> #activeFacebook { display:block; }</style>" ).appendTo('head');
            }else  $('#addFacebookAccountH3').text('Add your Facebook Account');
        }else  $('#addFacebookAccountH3').text('Add your Facebook Account');
    }

    function linkedin(linkedin){
        if (validateRequired(linkedin)) {
            if (validateRequired(linkedin.id)) {
                linkedinUserData = linkedin
                $('#addLinkedinAccountH3').text(linkedin.name);
                $('#addLinkedinAccount').attr('checked','checked');
                $( "<style> #activeLinkedin { display:block; }</style>" ).appendTo('head');
            } else $('#addLinkedinAccountH3').text('Add your Linkedin Account');
        }else $('#addLinkedinAccountH3').text('Add your Linkedin Account');
    }

    function twitter(twitter){
        if (validateRequired(twitter)) {
            if (validateRequired(twitter.id)) {
                twitterUserData = twitter
                $('#addTwitterAccountH3').text(twitter.displayName);
                $('#addTwitterAccount').attr('checked','checked');
                $( "<style> #activeTwitter { display:block; }</style>" ).appendTo('head');
            } else $('#addTwitterAccountH3').text('Add your Twitter Account');
        }else $('#addTwitterAccountH3').text('Add your Twitter Account');
    }

    function google(google){
        googleUserData = google
        if(google[0]){
            if(google[0].id != ''){
                $('#addGooleAccountText').text(google[0].emailId);
                $("#addGooleAccount").prop('checked',true);
                $("#addGooleAccount").attr('googleId',google[0].id);
            }
            else{
                $("#addGooleAccount").attr('checked',false);
                $('#addGooleAccountText').text('abc@abc.com');
            }
        } else{
            $("#addGooleAccount").attr('checked',false);
            $('#addGooleAccountText').text('abc@abc.com');
        }

        if(google[1]){
            if(google[1].id != ''){
                $('#addAnotherGoogleAccount1Text').text(google[1].emailId);
                $("#addAnotherGoogleAccount1").prop('checked',true);
                $("#addAnotherGoogleAccount1").attr('googleId',google[1].id);
            }
            else{
                $("#addAnotherGoogleAccount1").attr('checked',false);
                $('#addAnotherGoogleAccount1Text').text('Add another Gmail Account');
            }
        } else{
            $("#addAnotherGoogleAccount1").attr('checked',false);
            $('#addAnotherGoogleAccount1Text').text('Add another Gmail Account');
        }

        if(google[2]){
            if(google[2].id != ''){
                $('#addAnotherGoogleAccount2Text').text(google[2].emailId);
                $("#addAnotherGoogleAccount2").prop('checked',true);
                $("#addAnotherGoogleAccount2").attr('googleId',google[2].id);
            }
            else{
                $("#addAnotherGoogleAccount2").attr('checked',false);
                $('#addAnotherGoogleAccount2Text').text('Add another Gmail Account');
            }
        }else{
            $("#addAnotherGoogleAccount2").attr('checked',false);
            $('#addAnotherGoogleAccount2Text').text('Add another Gmail Account');
        }

        if(google[3]){
            if(google[3].id != ''){
                $('#addAnotherGoogleAccount3Text').text(google[3].emailId);
                $("#addAnotherGoogleAccount3").prop('checked',true);
                $("#addAnotherGoogleAccount3").attr('googleId',google[3].id);
            }
            else{
                $("#addAnotherGoogleAccount3").attr('checked',false);
                $('#addAnotherGoogleAccount3Text').text('Add another Gmail Account');
            }
        }else{
            $("#addAnotherGoogleAccount3").attr('checked',false);
            $('#addAnotherGoogleAccount3Text').text('Add another Gmail Account');
        }

    }

    function bindSessionInfo(data){
        var mail = data.emailId || '';
        $('#firstName').val(data.firstName || ''),
            $('#lastName').val(data.lastName || ''),
            $('#publicProfileUrl').val(data.publicProfileUrl || ''),
            $('#designation').val(data.designation || ''),
            $('#companyName').val(data.companyName || ''),
            $('#emailId').val(mail.toLowerCase()),
            $('#mobile').val(data.mobile || ''),
            $("#timezone").val(data.timezone || ''),
            $('#skypeId').val(data.skypeId || ''),
            $('#location').val(data.location || ''),
            $('#day').val(data.day || 'Day'),
            $('#month').val(data.month || 'Month'),
            $('#year').val(data.year || 'Year'),
            $('#profilePrivatePassword').val(data.profilePrivatePassword);
        $('#profileDescription').val(data.profileDescription);
        if(data.public == 'true' || data.public == true){
            $('#public').prop('checked',true);
        }
        else{
            $('#public').prop('checked',false);
            $('#selectPublic').prop('checked',true);
        }
        $("#workHoursStart").val(data.workHoursStart || '00:00');
        $("#workHoursEnd").val(data.workHoursEnd || '00:00');
        $('#startTime1').val(data.startTime1);
        $('#endTime1').val(data.endTime1);
        $('#startTime2').val(data.startTime2);
        $('#endTime2').val(data.endTime2);

        if(data.password){
            $("#editPass").css({display:'block'})
            $("#password").val(data.password);
            $("#retypePassword").val(data.retypePassword);
        }

        if(data.profilePrivatePassword == ''){
            $("#profilePrivate").prop('checked',false);
        }
        else{
            $("#profilePrivate").prop('checked',true);

        }

        if(data.sendDailyAgenda == false){
            $('#sendDailyAgenda').prop('checked',false);
        }
        else{
            $('#sendDailyAgenda').prop('checked',true);
        }
    }

    /*********************************************/

    function authenticateLinkedin(){
        if(validateRequired(linkedinUserData)){

        }else{
            storeFormDataInSession();
            window.location.replace('/linkedinEditProfile');
        }
    }

    function authenticateFacebook(){
        if(validateRequired(facebookUserData)){

        }else{
            storeFormDataInSession();
            window.location.replace('/facebookEditProfile');
        }
    }

    function authenticateTwitter(){
        if(validateRequired(twitterUserData)){

        }else{
            storeFormDataInSession();
            window.location.replace('/twitterEditProfile');
        }
    }

    $('#addLinkedinAccount').change(function(){
        if($('#addLinkedinAccount').prop('checked')) {
            authenticateLinkedin()
        }
        else{

            $.ajax({
                url:'/removeLinkedinAccount',
                type:'GET',
                success:function(isSuccess){
                    linkedinUserData = null;
                    getProfile();

                }
            });
        }
    });

    $("#addLinkedinAccountH3").on("click",function(){
        authenticateLinkedin()
    });

    $("#addFacebookAccountH3").on("click",function(){
        authenticateFacebook()
    });

    $("#addTwitterAccountH3").on("click",function(){
        authenticateTwitter()
    })

    $('#addFacebookAccount').change(function(){
        if($('#addFacebookAccount').prop('checked')) {
            authenticateFacebook()
        }
        else{
            $.ajax({
                url:'/removeFacebookAccount',
                type:'GET',
                success:function(isSuccess){
                    facebookUserData = null;
                    getProfile();
                }
            });
        }
    });

    $('#addTwitterAccount').change(function(){
        if($('#addTwitterAccount').prop('checked')) {
            authenticateTwitter()
        }
        else{
            $.ajax({
                url:'/removeTwitterAccount',
                type:'GET',
                success:function(isSuccess){
                    twitterUserData = null;
                    getProfile();
                }
            });
        }
    });

    $('#addGooleAccount').change(function(){
        if($('#addGooleAccount').prop('checked')) {
            if(validateRequired(googleUserData)){
                if(validateRequired(googleUserData[0])){
                    if(validateRequired(googleUserData[0].id)){

                    }else authenticateGoogle0()
                }

            }else{
                authenticateGoogle0()
            }

        }
        else{
            if(userProfile.google.length > 1){
                removeGoogleAccount($(this).attr('googleId'));
            }
            else{
                showMessagePopUp("Prymary google account is mandatory",'error','60%');
                $('#addGooleAccount').prop('checked',true);
            }
        }
    });
    function removeGoogleAccount(googleId){
        $.ajax({
            url:'/removeGoogleAccount/'+googleId,
            type:'GET',
            success:function(isSuccess){
                getProfile();
            }
        });
    }

    function authenticateGoogle0(){
        storeFormDataInSession();
        window.location.replace('/googleEditProfile');
    }
    function authenticateGoogle1(){
        storeFormDataInSession();
        window.location.replace('/addAnotherGoogleAccount2Edit');
    }
    function authenticateGoogle2(){
        storeFormDataInSession();
        window.location.replace('/addAnotherGoogleAccount3Edit');
    }
    function authenticateGoogle3(){
        storeFormDataInSession();
        window.location.replace('/addAnotherGoogleAccount4Edit');
    }

    $("#primaryGmailAccount").on("click",function(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[0])){

            }else authenticateGoogle0()
        }else authenticateGoogle0()
    });

    $("#addAnotherGoogleAccount1Text").on("click",function(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[1])){

            }else authenticateGoogle1()
        }else authenticateGoogle0()
    });

    $("#addAnotherGoogleAccount2Text").on("click",function(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[2])){

            }else authenticateGoogle2()
        }else authenticateGoogle0()
    });

    $("#addAnotherGoogleAccount3Text").on("click",function(){
        if(validateRequired(googleUserData)){
            if(validateRequired(googleUserData[3])){

            }else authenticateGoogle3()
        }else authenticateGoogle0()
    });

    $('#addAnotherGoogleAccount1').change(function(){
        if($('#addAnotherGoogleAccount1').prop('checked')) {
            if(validateRequired(googleUserData)){
                authenticateGoogle1()

            }else{
                authenticateGoogle0()
            }
        }
        else{

            removeGoogleAccount($(this).attr('googleId'));
        }
    });


    $('#addAnotherGoogleAccount2').change(function(){
        if($('#addAnotherGoogleAccount2').prop('checked')) {
            if(validateRequired(googleUserData)){
                authenticateGoogle2()

            }else{
                authenticateGoogle0()
            }
        }
        else{

            removeGoogleAccount($(this).attr('googleId'));
        }
    });

    $('#addAnotherGoogleAccount3').change(function(){
        if($('#addAnotherGoogleAccount3').prop('checked')) {
            if(validateRequired(googleUserData)){
                authenticateGoogle3()
            }else{
                authenticateGoogle0()
            }
        }
        else{

            removeGoogleAccount($(this).attr('googleId'));
        }
    });



    function createNewUser(data){
        var user = {
            'firstName':data.firstName,
            'lastName'  :data.lastName,
            'publicProfileUrl':data.publicProfileUrl,
            'sendDailyAgenda':data.sendDailyAgenda,
            'designation':data.designation,
            'companyName':data.companyName,
            'emailId':data.emailId,
            'mobile':data.mobile,
            'profilePrivatePassword':data.profilePrivatePassword,
            'public':data.public,
            'profileDescription':data.profileDescription,
            'skypeId':data.skypeId || '',
            'location':data.location || '',
            'day':day(data) ,
            'month':month(data) ,
            'year':year(data),
            'profilePicUrl':userProfile.profilePicUrl,
            'serviceLogin':'linkedin',
            'workHoursStart':data.workHoursStart,
            'workHoursEnd':data.workHoursEnd,
            'timezone':data.timezone,
            'tzName':data.tzName,
            'tzZone':data.tzZone
        };

        if(data.password){
            user.password = data.password;
        }

        if(!data.public && validateRequired(data.startTime1) && validateRequired(data.endTime1) || !data.public && validateRequired(data.startTime2) && validateRequired(data.endTime2)){
            user.startTime1 = data.startTime1;
            user.endTime1 = data.endTime1;
            user.startTime2 = data.startTime2;
            user.endTime2 = data.endTime2;
        }

        return user;
    }

    function day(data){
        if(data.day == 'Day'){
            return 0;
        }else{
            return data.day;
        }
    }

    function month(data){
        if(data.month == 'Month'){
            return 0;
        }else{
            return data.month;
        }
    }

    function year(data){
        if(data.year == 'Year'){
            return 0;
        }else{
            return data.year;
        }
    }

    function formData(){
        var mail = $('#emailId').val();
        var data={
            'firstName':$('#firstName').val(),
            'lastName':$('#lastName').val(),
            'publicProfileUrl':$('#publicProfileUrl').val(),
            'designation':$('#designation').val(),
            'companyName':$('#companyName').val(),
            'emailId':mail.toLowerCase(),
            'timezone':$("#timezone").val(),
            'mobile':$('#mobile').val(),
            'sendDailyAgenda':$('#sendDailyAgenda').prop('checked'),
            'skypeId':$('#skypeId').val(),
            'location':$('#location').val(),
            'day':$('#day').val(),
            'month':$('#month').val(),
            'year':$('#year').val(),
            'profilePrivatePassword':$('#profilePrivatePassword').val(),
            'public':$('#public').is(':checked'),
            'profileDescription':$('#profileDescription').val()

        };
        var date = moment().tz(data.timezone);
        data.tzName = data.timezone;
        data.tzZone = date.format("Z");

        if(userProfile.serviceLogin == 'relatas'){
            data.password = $("#password").val();
            data.retypePassword = $("#retypePassword").val();
        }
        data.startTime1 = $('#startTime1').val();
        data.endTime1   = $('#endTime1').val();
        data.startTime2 =  $('#startTime2').val();
        data.endTime2   = $('#endTime2').val();
        data.workHoursStart   = $('#workHoursStart').val();
        data.workHoursEnd   = $('#workHoursEnd').val();

        return data;
    }

    function getDay(data){
        if (validateRequired(data)) {
            return data.day;
        }else return 'Day';
    }
    function getMonth(data){
        if (validateRequired(data)) {
            return data.month;
        }else return 'Month';
    }
    function getYear(data){
        if (validateRequired(data)) {
            return data.year;
        }else return 'Year';
    }

    function validateRequired(fieldData){
        if (fieldData == '' || fieldData == null || fieldData == undefined) {
            return false;
        }
        else return true;
    }

    function validate(data){
        if(!validateRequired(data.firstName.trim())){
            showMessagePopUp("Please provide First name",'error','20%');
            return $('#firstName');
        }
        else if(!validateRequired(data.lastName.trim())){
            showMessagePopUp("Please provide Last name",'error','20%');
            return $('#lastName');
        }
        else  if(!validateRelatasIdentity(data.publicProfileUrl.trim())){
            showMessagePopUp("Please provide Unique Relatas identity",'error','20%');
            return $('#publicProfileUrl');
        }
        else  if(!validateRequired(data.designation.trim())){
            showMessagePopUp("Please provide your designation",'error','20%');
            return $('#designation');
        }
        else  if(!validateRequired(data.companyName.trim())){
            showMessagePopUp("Please provide Company name",'error','20%');
            return $('#companyName');
        }
        else  if(!validateRequired(data.emailId.trim())){
            showMessagePopUp("Please provide Email id",'error','20%');
            return $('#emailId');
        }
        else if(!validateRequired(data.mobile.trim())){
            showMessagePopUp("Please provide mobile number",'error','20%');
            return $('#mobile');
        }
        else if (!validateEmailField(data.emailId)) {
            showMessagePopUp("Please provide valid email id",'error','20%');
            return $('#emailId');
        }
        else if (!phone(data.mobile)) {
            showMessagePopUp("Please provide valid mobile number (+countrycode + 10 digit number)",'error','20%');
            return $('#mobile');
        }
        else if(!validateRequired(data.location)){
            showMessagePopUp("Please provide location",'error','20%');
            return $('#location');
        }
        else if(!validateRequired(data.timezone)){
            showMessagePopUp("Please provide Time Zone",'error','20%');
            return $('#timezone');
        }
        else{
            if(userProfile.serviceLogin == 'relatas'){
                if(!validateRequired(data.password.trim())){
                    showMessagePopUp("Please provide password",'error','20%');
                    return $('#password');
                }
                else if(!validateRequired(data.retypePassword.trim())) {
                    showMessagePopUp("Please retype password", 'error','20%');
                    return $('#retypePassword');
                }
                else if (data.password == data.retypePassword) {
                    return true;
                }
                else{
                    showMessagePopUp("Passwords mismatch. Please re-enter password.",'error','20%');
                    return $('#retypePassword');
                }
            } else  return true;
        }
    }

    function validateEmailField(email) {
        var emailText = email;
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (pattern.test(emailText)) {
            return true;
        } else {

            return false;
        }
    }

    function showMessagePopUp(message,msgClass,top,html)
    {
        var html = _.template($("#message-popup").html())();
        $("#user-name").popover({
            title: "Request a meeting" + '<button type="button" id="close" class="close">&times;</button>',
            html: true,
            content: html,
            container: 'body',
            placement: 'bottom',
            id: "myPopover"
        });
        $("#user-name").popover('show');
        $(".arrow").addClass("invisible");
        $(".popover").css({'margin-top':top,'margin-left':'10%'})

        if(msgClass == 'error'){
            $(".popover").addClass('popupError')
            $("#popupTitle").text('ERROR')
        }else
        if(msgClass == 'success'){
            $(".popover").addClass('popupSuccess')
            $("#popupTitle").text('SUCCESS')
        }else
        if(msgClass == 'tip'){
            $(".popover").addClass('popupTip')
            $("#popupTitle").text('TIP')
        }
       if(html){
           $("#message").html(message)
       }else
        $("#message").text(message)

        setTimeout(function(){
           $("#closePopup-message").focus();
       },200);
    }

    $("body").on("click","#closePopup-message",function(){
        $("#user-name").popover('destroy');

    });

    $(".PhoneNo").bind("keypress", function (event) {
        if (event.charCode != 0) {
            var regex = new RegExp("^[0-9+-]*$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });

    function phone(no){
        var targ=no.replace(/[^\d]/g,''); // remove all non-digits
        if(targ.length >=10 && targ.length <=15){
            return true;
        }
        else return false;
    }


    $('#submitChangesButton').on("click",function(){
        submitChangesButton();
    });

    function submitChangesButton(){
        var data=formData();
        data.startTime1 = null;
        data.endTime1   = null;
        data.startTime2 = null;
        data.endTime2   = null;
        var startTime1 = $('#startTime1').val();
        var endTime1 = $('#endTime1').val();
        var startTime2 = $('#startTime2').val();
        var endTime2 = $('#endTime2').val();
        if($('#selectPublic').is(':checked')){
            if(validateRequired(startTime1) && validateRequired(endTime1) || validateRequired(startTime2) && validateRequired(endTime2)){
                var start1Arr;
                var end1Arr;
                var start2Arr;
                var end2Arr;
                var startDate1;
                var endDate1;
                var startDate2;
                var endDate2;
                var date = new Date();
                var flag1 = true;
                var flag2 = true;
                if(validateRequired(startTime1) && validateRequired(endTime1)){
                    data.startTime1 = startTime1;
                    data.endTime1   = endTime1;
                    start1Arr = startTime1.split(':');
                    end1Arr = endTime1.split(':');
                    startDate1 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(start1Arr[0]),parseInt(start1Arr[0]),0,0);
                    endDate1 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(end1Arr[0]),parseInt(end1Arr[0]),0,0);
                    if(endTime1 == '00:00' && startTime1 == '00:00'){

                    }else
                    if( startDate1 >= endDate1){

                        flag1 = false;
                    }
                }else{
                    data.startTime1 = '';
                    data.endTime1   = '';
                }
                if(validateRequired(startTime2) && validateRequired(endTime2)){
                    data.startTime2 = startTime2;
                    data.endTime2   = endTime2;
                    start2Arr = startTime2.split(':');
                    end2Arr = endTime2.split(':');
                    startDate2 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(start2Arr[0]),parseInt(start2Arr[0]),0,0);
                    endDate2 = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(end2Arr[0]),parseInt(end2Arr[0]),0,0);
                    if(startTime2 == '00:00' && endTime2 == '00:00'){

                    }else
                    if(startDate2 >= endDate2){

                        flag2 = false;

                    }else if(startTime2 != '00:00' && endTime2 == '00:00'){
                        flag2 = false;
                    }
                }else{
                    data.startTime2 = '';
                    data.endTime2   = '';
                }

                if(flag1 == false || flag2 == false){

                    showMessagePopUp("Please select valid times",'error','30%');
                    $('#selectPublic').focus();
                }else afterChangeTime(data);
            }else{
                showMessagePopUp("Please select From and TO time for Time1 & Time2",'error','30%');
                $('#selectPublic').focus();
            }
        }else{
            validateWorkHours(data)
        }
    }

    function validateWorkHours(data){
         if(data.workHoursStart == '00:00' && data.workHoursEnd == '00:00'){
             afterChangeTime(data);
         }
         else{
             var workHoursStartArr = data.workHoursStart.split(':');
             var workHoursEndArr = data.workHoursEnd.split(':');
             var date = new Date();
             var start = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(workHoursStartArr[0]),parseInt(workHoursStartArr[1]),0,0);
             var end = new Date(date.getFullYear(),date.getMonth(),date.getDate(),parseInt(workHoursEndArr[0]),parseInt(workHoursEndArr[1]),0,0);
              if(start >= end){
                  showMessagePopUp("Please select valid work hours. Please have To(hrs) greater than From(hrs).",'error');
              }
              else{
                  afterChangeTime(data);
              }
             }
    }

    function afterChangeTime(data){
        if($('#profilePrivate').prop('checked')){
            if(validateRequired(data.profilePrivatePassword)){
                onSaveFormData(data);
            }
            else{
                showMessagePopUp("You selected 'Make Profile Private', Please set access password",'error','30%');
                $("#profilePrivatePassword").focus();
            }
        }else onSaveFormData(data);
    }

    function onSaveFormData(data){
        var result = validate(data);
        if(result == true){
            checkRIdentity(data)
        }else{
            result.focus();
        }
    }

    function checkRIdentity(data){
        var details = {
            publicProfileUrl:data.publicProfileUrl
        }
        $.ajax({
            url:'/checkUrlEdit',
            type:'POST',
            datatype:'JSON',
            data:details,
            traditional:true,
            success:function(result){
                if(result == 'error'){
                    showMessagePopUp("An error occurred while updating profile please try again",'error','60%')
                }
                else
                if(result == true){
                    checkEmailAddress(data)
                }
                else{
                    showMessagePopUp("Public profile url already exist in the Relatas, Please change it",'error');
                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }


    function checkEmailAddress(data){

        $.ajax({
            url:'/checkEmailEdit/'+data.emailId,
            type:'GET',

            traditional:true,
            success:function(result){
                if(result == 'error'){
                    showMessagePopUp("An error occurred while updating profile please try again",'error','60%')
                }
                else
                if(result == true){
                    submitData(data);
                }
                else{
                    showMessagePopUp("Email you entered is already exist in relatas (email found in other user profile)",'error','60%')

                }
            },
            error: function (event, request, settings) {

            },
            timeout: 30000
        })
    }

    function checkAcceptTerms(data){
        if ($('#acceptTerms').prop('checked')) {
            ajaxCall(data);
        }
        else{
            $('#acceptTerms').focus();
            showMessagePopUp("Please accept terms and conditions",'error')
        }
    }

    function submitData(data){

        var user= createNewUser(data);
        if(validateRequired(userProfile)){
                if(validateRequired(userProfile.google)){
                        if(validateRequired(userProfile.google[0])){
                             if(validateRequired(userProfile.google[0].id)){
                                 if(!userProfile.registeredUser){
                                     checkAcceptTerms(data)
                                 }else
                                 ajaxCall(user);
                             }
                             else msgNoGoogleAcc();
                        }else msgNoGoogleAcc()
                }else msgNoGoogleAcc()
        }
    }

function msgNoGoogleAcc(){
    showMessagePopUp("Please add primary gmail account.",'error')
}

    function ajaxCall(newUser){
        $.ajax({
            url: '/edit',
            type: 'POST',
            datatype: 'JSON',
            traditional: true,
            data: newUser,
            success: function (msg) {
                if(msg == false) {
                }else {
                    window.location.replace('/editProfile');
                }
            },
            error: function (event, request, settings) {
                var status = event.status;
                switch(status){
                    case 4010:
                        console.error("please specify all fields are required");
                        showMessagePopUp("Fields marked with (*) are mandatory",'error','30%');
                        break;
                    case 4013:
                        console.error("Your password is invalid");
                        showMessagePopUp("Your password is invalid",'error');
                        $('#password').focus();
                        break;
                    case 4011:
                        console.error("Your email address must be in the format of name@domain.com");
                        showMessagePopUp("Your email address must be in the format of name@domain.com",'error','20%');
                        $('#emailId').focus();
                        break;
                    case 4012:
                        console.error("Your phone number format error NaN or length (12) with country code");
                        showMessagePopUp("Your phone number format error NaN or length (12) with country code",'error','20%')

                        $('#mobile').focus();
                        break;
                    default :
                        showMessagePopUp("Un expected error occurred please try again",'error','60%');
                        console.error(" render the 404 err page");
                }
            },
            timeout: 20000
        });
    }


    $('#profileDescription').bind("keypress",function(event){
        var text = $('#profileDescription').val();
        var words = text.split(/[\s]+/);

        if(words.length >= 400){
            event.preventDefault();
            showMessagePopUp("You exceeded max limit of 400 words.",'error','45%')
            return false;
        }
    });

    function getTimeZone(){
        var start = new Date()
        var zone = start.toTimeString().replace(/[()]/g,"").split(' ');
        if(zone.length > 3){
            var tz = '';
            for(var i=2; i<zone.length; i++){

                tz += zone[i].charAt(0);
            }
            return tz || zone[2]
        }
        else return zone[2]
    }

    /* Widget Script */
    //var domain = window.location.origin;
    //getWidgetKey();
    function getWidgetKey(){
        $.ajax({
            url:'/webWidget/relatas/generateKey',
            type:'GET',
            datatype:'JSON',
            success:function(resObj){
                if(validateRequired(resObj) && resObj != false && validateRequired(resObj.key)){
                    var script = '<script>(function(e,t){if(null==t){var n,i;t={},t.init=function(e){t.key=e},t.getKey=function(){return t.key},t.getDomain=function(){return"'+domain+'"},window.rww=t,n=e.createElement("script"),n.type="text/javascript",n.async=!0,n.src=rww.getDomain()+"/javascripts/webWidgetFiles/webWidget.js",i=e.getElementsByTagName("script")[0],i.parentNode.insertBefore(n,i)}})(document,null);rww.init("'+resObj.key+'");</script>'+' \n\n and Place  <div id="widget-container"></div>  where you want Widget';
                    $("#widgetScript").val(script);
                }
            }
        })
    }
    /* Widget Script */
});