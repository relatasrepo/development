
$(document).ready(function() {
    var table;
    var monthNameFull = ['January','February','March','April','May','June','July','August','September','October','November','December']
    var monthNameSmall=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    getUserList()
    function getUserList(){
        $.ajax({
            url:'/getAllProfiles/admin/list',
            type:'GET',
            datatye:'JSON',
            success:function(userList){

                if(checkRequired(userList)){

                    if(userList.length > 0){

                        displayUserList(userList)
                    }
                }
            }
        })
    }

    function displayUserList(list){
        var tableArr = []
        for(var user=0; user<list.length; user++){

            if(list[user].firstName){
                var count = user+1;
                var ipLocation = '';
                if(checkRequired(list[user].currentLocation)){

                        var location = list[user].currentLocation;
                        ipLocation = location.city;
                }
                var firstName = list[user].firstName || '';
                var lastName = list[user].lastName || '';
                var fn = firstName+' '+lastName;
                tableArr.push([
                        '<span>'+count+'</span>',
                        '<span id="sortdDate" value='+list[user].createdDate+'>'+getEventDateFormat(new Date(list[user].createdDate))+'</span>',
                        '<span>'+fn+'</span>',
                        '<span>'+list[user].designation || ''+'</span>',
                        '<span>'+list[user].companyName || ''+'</span>',
                        '<span>'+list[user].emailId || ''+'</span>',
                        '<span>'+list[user].serviceLogin || ''+'</span>',
                        '<span>'+ipLocation+'</span>',
                        '<span>'+list[user].location || ''+'</span>',
                        '<span class="sortNumber" id='+'sentInvitations'+list[user]._id+' userId='+list[user]._id+'></span>',
                        '<span class="sortNumber" id='+'invitationsRec'+list[user]._id+' userId='+list[user]._id+'></span>',
                        '<span class="sortNumber" id='+'docsUploaded'+list[user]._id+' userId='+list[user]._id+'></span>',
                        '<span class="sortNumber" id='+'docsShared'+list[user]._id+' userId='+list[user]._id+'></span>',
                        '<span class="sortNumber" id='+'docsRec'+list[user]._id+' userId='+list[user]._id+'></span>',
                        '<span>'+list[user].contacts.length || ''+'</span>',
                        '<span class="sortNumber" id='+'eventsPosted'+list[user]._id+' userId='+list[user]._id+'></span>',
                        '<span class="sortNumber" id='+'eventsOnCalendar'+list[user]._id+' userId='+list[user]._id+'></span>'
                       ])

               // table.row.add( ).draw();
               /*invitationsSentCount(list[user]._id);
                invitationsRecCount(list[user]._id);
                docsUploadedCount(list[user]._id);
                docsSharedCount(list[user]._id);
                docsRecCount(list[user]._id);
                eventsPostedCount(list[user]._id);
                eventsOnCalendarCount(list[user]._id);*/
            }
            if(user == list.length-1){
                //appendCount(list)

                applyDataTable(tableArr)
            }
        }
    }

    function appendCount(list){
        for(var i=0; i<list.length; i++){
            invitationsSentCount(list[i]._id,list[i].emailId);
            invitationsRecCount(list[i]._id,list[i].emailId);
            docsUploadedCount(list[i]._id,list[i].emailId);
            docsSharedCount(list[i]._id,list[i].emailId);
            docsRecCount(list[i]._id,list[i].emailId);
            eventsPostedCount(list[i]._id,list[i].emailId);
            eventsOnCalendarCount(list[i]._id,list[i].emailId);

        }
    }

    function invitationsSentCount(userId,e){
        $.ajax({
            url:'/invitationsSentCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

               $("#sentInvitations"+userId).text(count || 0);
               $("#sentInvitations"+userId).attr("value",count || 0);

            }
        })
    }

    function invitationsRecCount(userId,e){
        $.ajax({
            url:'/invitationsRecCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

                $("#invitationsRec"+userId).text(count || 0);
                $("#invitationsRec"+userId).attr("value",count || 0);
            }
        })
    }

    function docsUploadedCount(userId,e){
        $.ajax({
            url:'/documentsUploadedCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

                $("#docsUploaded"+userId).text(count || 0);
                $("#docsUploaded"+userId).attr("value",count || 0);
            }
        })
    }

    function docsSharedCount(userId,e){
        $.ajax({
            url:'/documentsSharedWithMeCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

                $("#docsShared"+userId).text(count || 0);
                $("#docsShared"+userId).attr("value",count || 0);
            }
        })
    }

    function docsRecCount(userId,e){
        $.ajax({
            url:'/documentsRecCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

                $("#docsRec"+userId).text(count || 0);
                $("#docsRec"+userId).attr("value",count || 0);
            }
        })
    }

    function eventsPostedCount(userId,e){
        $.ajax({
            url:'/eventsPostedCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

                $("#eventsPosted"+userId).text(count || 0);
                $("#eventsPosted"+userId).attr("value",count || 0);
            },
            error: function (event, request, settings) {

            }
        })
    }

    function eventsOnCalendarCount(userId,e){
        $.ajax({
            url:'/eventsSubscribedCount/'+userId,
            type:'GET',
            datatye:'JSON',
            success:function(count){

                $("#eventsOnCalendar"+userId).text(count || 0);
                $("#eventsOnCalendar"+userId).attr("value",count || 0);
            }
        })
    }

    function getEventDateFormat(date){
        var day = date.getDate()
        var format = getVakidNumber(day)+'-'+monthNameSmall[date.getMonth()]+'-'+date.getFullYear();
        return format;
    }
     function getVakidNumber(num){
         if(num < 10){
             return '0'+num;
         }else return num;
     }

    function applyDataTable(tableArr){
        table = $('#userListTable').DataTable({
            "data":tableArr,
            "dom": '<"top"iflp<"clear">>',

            "fnDrawCallback": function (oSettings) {

             },
            "order": [[ 1, "asc" ]],
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){

                if(checkRequired(table)){
                    var oSettings = table.settings();

                    $("td:first", nRow).html(oSettings[0]._iDisplayStart+iDisplayIndex +1);

                    var span = $($( nRow).children()[9]).children();
                    var userId = span.attr('userId');
                    if(!checkRequired(span.text())) {
                        invitationsSentCount(userId);
                        invitationsRecCount(userId);
                        docsUploadedCount(userId);
                        docsSharedCount(userId);
                        docsRecCount(userId);
                        eventsPostedCount(userId);
                        eventsOnCalendarCount(userId);
                    }
                    return nRow;
                }

            },
            "columns": [
                null,
                { "orderDataType": "dom-value" },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                { "orderDataType": "dom-number" },
                { "orderDataType": "dom-number" },
                { "orderDataType": "dom-number" },
                { "orderDataType": "dom-number" },
                { "orderDataType": "dom-number" },
                null,
                { "orderDataType": "dom-number" },
                { "orderDataType": "dom-number" }
            ]
        });
    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-value'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('#sortdDate', td).attr("value");
        } );
    }

    /* Create an array with the values of all the select options in a column */
    $.fn.dataTable.ext.order['dom-number'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('.sortNumber', td).attr("value");
        } );
    }

    function checkRequired(data){
        if (data == '' || data == null || data == undefined) {
            return false;
        }
        else{
            return true;
        }
    }
});