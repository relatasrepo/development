var notificationCategory = getParams(window.location.href).notifyCategory;
var notificationDate = getParams(window.location.href).notifyDate;

relatasApp.controller("opportunityTargets", function($scope, share, $http, $rootScope) {

    share.loadTargets = function(){
        getTargets($scope, $http, share);
    }

    share.getTargets = function (user,filter,fromDate,endDate,switchQtr) {
        $scope.user = user;
        $scope.filter = filter;
        $scope.fromDate = fromDate;
        $scope.endDate = endDate;
        $scope.switchQtr = switchQtr;
        getTargets();
    }
})

relatasApp.controller("today-primary-insight", function($scope, share, $http, $rootScope) {

    $scope.todayInsightNoDataMsg = {
        meetings: "No meetings scheduled today",
        tasks: "No tasks for today",
        dealsClosingSoon: "No deals owned by you closing in next 30 days",
        losingTouch: "Super networker. You are in touch with all your contacts",
        meetingFollowup: "You are all set. No follow ups pending"
    }

    $scope.selectedCommitRange = 1;
    
    $scope.commitRange = [{
        name: 'WEEKLY',
        selected: '',
        daysLeft:0,
        cuttOffDate:'',
        userCommitAmount: 0,
        inputComment: 'Enter your weekly commit',
        nextCommitMsg: 'Commit for next Week'
    },{
        name: 'MONTHLY',
        selected: 'selected',
        daysLeft:0,
        cuttOffDate:'',
        userCommitAmount: 0,
        inputComment: 'Enter your monthly commit',
        nextCommitMsg: 'Commit for next Month'
    },{
        name: 'QUARTERLY',
        selected: '',
        daysLeft:0,
        cuttOffDate:'',
        userCommitAmount: 0,
        inputComment: 'Enter your quarterly commit',
        nextCommitMsg: 'Commit for next Quarter'
    }]

    share.loadToday = function(){
        console.log("share.loadToday");
        loadPage($scope, $http, share, $rootScope);
    }

    $scope.updateCommit = function() {
        var index = $scope.selectedCommitRange;
        var commitforNext = $scope.commitRange[index].commitforNext;

        $scope.saving = true;
        if($scope.commitRange[index].name){

            if($scope.commitRange[index].name == "WEEKLY"){
                
                saveWeeklyCommits($scope,$rootScope,$http,share,{selfCommitValue:$scope.selfCommitValue, commitforNext:commitforNext},function () {
                    $scope.saving = false;
                });
            } else if ($scope.commitRange[index].name == "QUARTERLY"){

                saveQuarterlyCommits($scope,$rootScope,$http,share,{selfCommitValue:$scope.selfCommitValue, commitforNext:commitforNext},function () {
                    $scope.saving = false;
                });
            } else {
                saveMonthlyCommit($scope,$rootScope,$http,share,{selfCommitValue:$scope.selfCommitValue, commitforNext:commitforNext},function () {
                    $scope.saving = false;
                });
            }
        } else {
            saveMonthlyCommit($scope,$rootScope,$http,share,function () {
                $scope.saving = false;
            });
        }
    }

    $scope.viewAllOpps = function() {
        window.open('team/commit/review', '_blank');
    }

    $scope.viewAllTasks = function(filter, redirectLink) {
        if(redirectLink) {
            var url = "tasks/all"
            url = fetchUrlWithParameter(url, "for", filter);
            window.open(url, '_blank');
        }
    }

    $scope.viewAllDealsClosingSoon = function() {
        var oppViewFor =  {
            name:"Opportunity",
            selected:""
        };

        share.openViewFor(oppViewFor, "today_deals_closing");
    }

    $scope.viewAllLosingTouch = function() {
        var url = "today/insights/all"
        url = fetchUrlWithParameter(url, "for", "losingTouch");
        // window.location = url;
        window.open(url, '_blank');
    }

    $scope.viewAllMeetingFollowUps = function() {
        var url = "today/insights/all";  
        url = fetchUrlWithParameter(url, "for", "meetingFollowUp");
        window.open(url, '_blank');

    }

    $scope.viewAllMeetings = function() {
        var url = "today/insights/all"
        url = fetchUrlWithParameter(url, "for", "todayMeeting");
        window.open(url, '_blank');

    }

    share.viewAllTasks = function(filter, redirectLink) {
        $scope.viewAllTasks(filter, redirectLink);
    }

    $scope.setTaskStatus = function(task) {

        if(task.checked) {
            task.status = "complete"
        } else {
            task.status = "inProgress"
        }
        updateTask($scope, $http, task);
    }

    $scope.commitFor = function(commit, index) {
        var url;
        var mode;

        menuToggleSelection(commit.name, $scope.commitRange);

        $scope.selectedCommitRange = index;
        if($scope.commitRange[index].name) {
            mode =  $scope.commitRange[index].name;
        }
        
        if(mode){
            if(mode == "WEEKLY"){
                url = '/review/commits/week';
                fetchCommits($scope,$http,url,mode)

            } else if (mode == "QUARTERLY"){
                url = '/review/commits/quarter';
                fetchCommits($scope,$http,url,mode)

            } else if(mode == "MONTHLY"){
                url = '/review/commits/month';
                fetchCommits($scope,$http,url,mode)
            }
        }
    }

    $scope.goToOpportunity = function(opp) {
        window.open("/opportunities/all?opportunityId="+opp.opportunityId, '_blank');
    }

    $scope.openLosingTouchContact = function(contact, index) {

        localStorage.setItem("losing-touch", JSON.stringify (contact));
        var url = "today/insights/all"

        url = fetchUrlWithParameter(url, "for", "losingTouch");
        url = fetchUrlWithParameter(url+"&index="+index);

        window.open(url, '_blank');
    }

    $scope.openMeetingDetails = function(meeting, index) {
        
        localStorage.setItem("today-meeting", JSON.stringify(meeting));
        var url = "today/insights/all"

        url = fetchUrlWithParameter(url, "for", "todayMeeting");
        url = url+"&index="+index
        window.open(url, '_blank');
    }

    $scope.openMeetingFollowUpDetails = function(contact, index) {
        localStorage.setItem("meeting-followUp", JSON.stringify(contact));
        var url = "today/insights/all"

        url = fetchUrlWithParameter(url, "for", "meetingFollowUp");
        url = url+"&index="+index
        window.open(url, '_blank');
    }

    $scope.openCommitModel = function() {
        $scope.commitModalOpen = true;
    }

    $scope.closeModal = function() {
        $scope.commitModalOpen = false;
    }

})

relatasApp.controller("today-secondary-insight", function($scope, share, $http, $rootScope) {

    $scope.loadTodayMetaInsights = function() {
        $scope.loadingInsights = true;

        $http.get("/today/insights/meta")
            .success(function (response) {
                $scope.loadingInsights = false;
                if(response.Data){

                    var insight = response.Data;

                    $scope.todayMetaInsight = {
                        "impMailCount": insight.mailInsights.important || 0,
                        "upComingMeetings": insight.upcomingMeetings || 0,
                        "upcomingTasks": insight.upcomingTasks || 0,
                        "staleOpportunityCount": insight.staleOpportunities || 0,
                        "dealsAtRiskCount": insight.dealsClosingSoon.count[0] ? insight.dealsClosingSoon.count[0].count : 0,
                        "tasksOverdue": insight.overdueTasks || 0,
                    }

                }
            })
    }

    if(notificationCategory == "dealsAtRiskForUser") {
        updateNotificationOpenDate($http, {"dayString":notificationDate, "category": notificationCategory}, function(response) {});

        function checkDealsAtRisk() {
            if($scope.showDealsAtRisk) {
                $scope.showDealsAtRisk(true);
            } else {
                setTimeout(() => {
                    checkDealsAtRisk();
                }, 200);
            }
        }
        checkDealsAtRisk();
    }

    share.loadTodayMetaInsights = function() {
        $scope.loadTodayMetaInsights();
    }

    $scope.showStaleOpportunities = function() {
        if($scope.todayMetaInsight.staleOpportunityCount) {
            var oppViewFor =  {
                name:"Opportunity",
                selected:""
            };
    
            share.openViewFor(oppViewFor, "today_overdue");
        }
    }

    $scope.showDealsAtRisk = function(fromNotification) {
        if(fromNotification || $scope.todayMetaInsight.dealsAtRiskCount) {
            share.forDealsAtRisk(share.liuData._id);
        }
    }

    $scope.showMailSection = function() {
        if($scope.todayMetaInsight.impMailCount)
            window.open('insights/mails', "_blank");
    }

    $scope.viewAllUpcomingMeetings = function() {
        if($scope.todayMetaInsight.upComingMeetings) {
            var url = "today/insights/all"
            url = fetchUrlWithParameter(url, "for", "upcomingMeeting");
            window.open(url, '_blank');
        }
    }

    // $scope.viewAllRecommendedToMeet = function() {
    //     var url = "today/insights/all"
    //     url = fetchUrlWithParameter(url, "for", "recommendedToMeet");
    //     window.open(url, '_blank');
    // }

    $scope.showTasksOverdue = function() {
        window.open("tasks/all/for/overdue", '_blank');
    }

    $scope.showTasksUpcoming = function() {
        window.open("tasks/all/for/upcoming", '_blank');
    }

    $scope.viewAllTasks = function(filter) {
        var visibility;

        if(filter == 'upcoming') {
            visibility = $scope.todayMetaInsight.upcomingTasks ? true : false;
        } else if(filter == 'overdue') {
            visibility = $scope.todayMetaInsight.tasksOverdue ? true : false;
        }
        share.viewAllTasks(filter, visibility);
    }

})

function saveQuarterlyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/quarterly",{commitValue:commits.selfCommitValue,
                                                                        committingForNext:commits.commitforNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // quarterlyCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    $scope.saving = false;
                    toastr.error("Commits not updated. Please try again later");
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveWeeklyCommits($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){

        $http.post("/review/meeting/update/commit/value/weekly",{week:commits.selfCommitValue,
                                                                    committingForNext:commits.commitforNext})
            .success(function (response) {

                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // weeklyPastCommitHistory($scope,$rootScope,$http,share,share.liuData._id)
                } else {
                    $scope.saving = false;
                    toastr.error("Commits not updated. Please try again later")
                }
                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only positive numbers")
    }
}

function saveMonthlyCommit($scope,$rootScope,$http,share,commits,callback) {

    if(!checkForAlphanumericChars(commits.selfCommitValue) && commits.selfCommitValue>0){
        $http.post("/review/meeting/update/commit/value/monthly",{commitValue:commits.selfCommitValue,
                                                                    committingForNext:commits.commitforNext})
            .success(function (response) {
                if(response.SuccessCode){
                    toastr.success("Commits successfully updated")
                    // monthlyCommitHistory($scope,$rootScope,$http,share)
                } else {
                    $scope.saving = false;
                    toastr.error("Commits not updated. Please try again later")
                }

                if(callback){
                    callback()
                }
            });
    } else {
        toastr.error("Please enter only numbers for opportunity amount")
    }
}

function getTargets($scope, $http, share){

    $scope.loadingTargets = true;
    var url = fetchUrlWithParameter("/opportunities/by/month/year","userId", $scope.user)

    if($scope.filter && $scope.filter == 'accessControl'){
        url = fetchUrlWithParameter(url+"&accessControl="+true)
    }

    if($scope.fromDate && $scope.endDate){
        url = fetchUrlWithParameter(url+"&fromDate="+$scope.fromDate)
        url = fetchUrlWithParameter(url+"&toDate="+$scope.endDate)
    }

    setTimeOutCallback(10,function () {
        drawTargets($scope,$http,url,share,$scope.switchQtr)
    })
}   

function menuToggleSelection(select,menu) {

    _.each(menu,function (item) {
        if(item.name == select){
            item.selected = "selected"
        } else {
            item.selected = ""
        }
    })
}

function loadPage($scope, $http, share, $rootScope) {

    console.log("loadPage");
    $scope.loadingLosingTouch = true;
    $scope.loadingMeetings = true;
    $scope.loadingTasks = true;
    $scope.loadingLosingTouch = true;
    $scope.loadingDealsClosing = true;
    $scope.loadingMeetingFollowUp = true;
    
    getAllCommitCutOffDates($scope, $http);
    fetchCommits($scope, $http);
    getMeetings($scope, $http, share);
    getLosingTouch($scope, $http, $rootScope);

    setTimeOutCallback(100, function() {
        getAllTasksToday($scope, $http, $rootScope);
        getDealsClosingSoon($scope, $http, $rootScope);
        getPastMeetingFollowUps($scope, $http);
        share.loadTodayMetaInsights();
    });

    // setTimeOutCallback(200, function() {
    //     getOverdueInsights('staleOpportunity', $http, $scope, $rootScope, share);
    //     getOverdueInsights('dealsAtRisk', $http, $scope, $rootScope, share);
    //     getOverdueInsights('taskOverdue', $http, $scope, $rootScope, share);
    //     getOverdueInsights('mailResponsePending', $http, $scope, $rootScope, share);
    // });

}

function updateTask($scope, $http,task) {
    $http.post('/task/update/properties',task)
        .success(function (response) {
            if(response){
            }   
        });
}

function drawTargets($scope,$http,url,share,switchQtr){

    $http.get(url)
        .success(function (response) {
            
            var quarters = [
                {
                    quarter:1,
                    startMonth:0,
                    endMonth:2
                },{
                    quarter:2,
                    startMonth:3,
                    endMonth:5
                },{
                    quarter:3,
                    startMonth:6,
                    endMonth:8
                },{
                    quarter:4,
                    startMonth:9,
                    endMonth:11
                }
            ];

            var targetForFy = 0;
            var total = _.map(response.Data,"total")
            var won = _.map(response.Data,"won")
            var lost = _.map(response.Data,"lost")
            var target = _.map(response.Data,"target")

            var values = _.flatten(_.concat(total,won,lost,target))

            var min = Math.min.apply( null, values );
            var max = Math.max.apply( null, values );

            var pipeline = 0;
            var currentQuarter = {};

            _.each(quarters,function (q) {
                if (q.startMonth <= new Date().getMonth() && q.endMonth >= new Date().getMonth()) {
                    currentQuarter = q;
                }
            });

            if(switchQtr){
                currentQuarter.startMonth = (moment(switchQtr.start).format("M"))-1,
                currentQuarter.endMonth = (moment(switchQtr.end).format("M"))-1
            }

            currentQuarter.values = [];
            currentQuarter.target = 0;
            currentQuarter.pipeline = 0;
            currentQuarter.won = 0;
            currentQuarter.lost = 0;

            var currentMonth = monthNames[new Date().getMonth()]

            $scope.cqHeader = monthNames[currentQuarter.startMonth].substr(0,3)+" - "+monthNames[currentQuarter.endMonth].substr(0,3) +" "+new Date().getFullYear();
            _.each(response.Data,function (value) {
                value.won = value.won?value.won:0;
                value.lost = value.lost?value.lost:0;
                value.total = value.total?value.total:0;
                value.openValue = value.openValue?value.openValue:0;

                targetForFy = targetForFy+value.target;

                var thisMonth = monthNames[new Date(moment(value.sortDate).subtract(1,"d")).getMonth()];

                if(thisMonth == currentMonth && new Date(value.sortDate).getFullYear() == new Date().getFullYear()){
                    value.highLightCurrentMonth = true
                }

                pipeline = pipeline+value.openValue
                
                if (currentQuarter.startMonth <= new Date(value.sortDate).getMonth() && currentQuarter.endMonth >= new Date(value.sortDate).getMonth()){
                    currentQuarter.target = currentQuarter.target+parseFloat(value.target)
                    currentQuarter.won = currentQuarter.won+value.won

                    currentQuarter.pipeline = currentQuarter.pipeline+value.openValue
                    currentQuarter.lost = currentQuarter.lost+value.lost
                }

                value.heightWon = {'height':scaleBetween(value.won,min,max)+'%'}
                value.heightLost = {'height':scaleBetween(value.lost,min,max)+'%'}
                value.heightTotal = {'height':scaleBetween(value.openValue,min,max)+'%'}
                value.heightTarget = {'height':scaleBetween(value.target,min,max)+'%'}

                value.won = numberWithCommas(value.won.r_formatNumber(2),share.primaryCurrency == "INR");
                value.lost = numberWithCommas(value.lost.r_formatNumber(2),share.primaryCurrency == "INR");
                value.openValue = numberWithCommas(value.openValue.r_formatNumber(2),share.primaryCurrency == "INR");
                value.target = numberWithCommas(value.target.r_formatNumber(2),share.primaryCurrency == "INR");
            });

            share.setTargetForFy(targetForFy)

            var allValues = [currentQuarter.pipeline,currentQuarter.lost,currentQuarter.won,currentQuarter.target]

            var cqMin = Math.min.apply( null, allValues );
            var cqMax = Math.max.apply( null, allValues );

            $scope.cqTarget = numberWithCommas(currentQuarter.target.r_formatNumber(2),share.primaryCurrency == "INR")
            // $scope.cqLost = {'width':scaleBetween(currentQuarter.lost,cqMin,cqMax)+'%',background: '#6dc3b8'}
            $scope.cqWonStyle = {'width':scaleBetween(currentQuarter.won,0,currentQuarter.target)+'%',background: '#8ECECB'}
            $scope.cqWon = numberWithCommas(currentQuarter.won.r_formatNumber(2),share.primaryCurrency == "INR")
            $scope.cqPipeline = numberWithCommas(currentQuarter.pipeline.r_formatNumber(2),share.primaryCurrency == "INR")

            if(currentQuarter.won && !currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#8ECECB'}
            }

            if(currentQuarter.target){
                $scope.cqWonPercentage = ((currentQuarter.won/currentQuarter.target)*100).r_formatNumber(2)+'%'
            } else {
                $scope.cqWonPercentage = "-"
            }

            if(currentQuarter.won>currentQuarter.target){
                $scope.cqWonStyle = {'width':100+'%',background: '#8ECECB'}
                $scope.cqWonPercentage = 100+'%'
            }

            var t = _.sum(target);
            var w = _.sum(won);
            var g = t - w;

            var valArr = [];
            valArr.push(t,w,g,pipeline)

            var vmin = Math.min.apply( null, valArr );
            var vmax = Math.max.apply( null, valArr );

            $scope.fyWonStyle = {'width':scaleBetween(w,1,t)+'%',background: '#8ECECB'}

            if(!w || w ==0 ){
                $scope.fyWonStyle = {'width':0+'%',background: '#8ECECB'}
            }

            $scope.target = {'width':scaleBetween(t,vmin,vmax)+'%',background: '#6dc3b8'}
            $scope.pipeline = {'width':scaleBetween(pipeline,vmin,vmax)+'%',background: '#767777'}
            $scope.won = {'width':scaleBetween(w,vmin,vmax)+'%',background: '#8ECECB'}
            $scope.gap = {'width':scaleBetween(g,vmin,vmax)+'%',background: '#e74c3c'}

            if(t){
                $scope.wonPercentage = ((w/t)*100).r_formatNumber(2)+'%'
            } else {
                $scope.wonPercentage = "-"
            }

            if(w>t){
                $scope.fyWonStyle = {'width':100+'%',background: '#8ECECB'}
                $scope.won = {'width':100+'%',background: '#8ECECB'}
                $scope.wonPercentage = 100+'%'
            }

            $scope.targetCount = numberWithCommas(t.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.wonCount = numberWithCommas(w.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.gapCount = numberWithCommas(g.r_formatNumber(2),share.primaryCurrency == "INR");
            $scope.pipelineCount = numberWithCommas(pipeline.r_formatNumber(2),share.primaryCurrency == "INR");

            $scope.targetGraph = response.Data;

            $scope.targetGraph.sort(function (o1, o2) {
                return o1.sortDate > o2.sortDate ? 1 : o1.sortDate < o2.sortDate ? -1 : 0;
            });

            _.map($scope.targetGraph, function(el) {
                return el.month =  el.monthYear.split(' ')[0]
            })

            $scope.loadingTargets = false;
        })
}

function getAllTasksToday($scope, $http, $rootScope) {

    var todayDate = moment(new Date()).format("YYYY-MM-DD");
    var tomorrowDate = new Date();
    tomorrowDate.setDate(tomorrowDate.getDate() + 1);

    tomorrowDate = moment(tomorrowDate).format("YYYY-MM-DD");

    $scope.taskInsights = {
        tasks:[],
    }

        $http.get('/tasks/get/all?filter=dueDate&start='+todayDate+'&end='+tomorrowDate)
        .success(function(response) {
            $scope.loadingTasks = false;
            var filteredTasks = [];
            if(response.Data.tasks) {

                filteredTasks = _.filter(response.Data.tasks, function(task) {
                    return ($rootScope.liuEmailId == task.assignedToEmailId);
                })
                
                filteredTasks.forEach(function(task) {
                    task.status = task.status == 'complete' ? true : false;
                    task.formattedTaskName = stripHtmltags(task.taskName);
                })
                
                $scope.taskInsights["totalCount"] = filteredTasks.length;
                $scope.taskInsights.tasks = filteredTasks.slice(0,5);
                $scope.taskInsights["displayedCount"] = $scope.taskInsights.tasks.length;
                $scope.taskInsights["widthDisplay"] = {'width': ($scope.taskInsights.displayedCount/$scope.taskInsights.totalCount)*100 +'%'}; 
                $scope.taskInsights.showNoDataMsg = $scope.taskInsights.tasks.length == 0 ? true : false;
            } else {
                $scope.taskInsights.showNoDataMsg = true;
            }
        })

}

function getLosingTouch($scope, $http, $rootScope) {
    var losingTouchContacts = [];
    
    $scope.losingTouchInsights = {
        losingTouchContacts: []
    };
    
    $http.get('/insights/losing/touch/info/by/relation')
        .success(function(response) {
            $scope.loadingLosingTouch = false;

            var contactsArray = _.uniqBy(response.Data, 'contactEmailId');

            _.each(contactsArray, function(contact) {
                var obj = formatLosingTouchContactDetails(contact);
                if(obj) {
                    losingTouchContacts.push(obj);
                }
            })

            losingTouchContacts.sort(function (o1, o2) {
                return new Date(o1.lastInteractionDate) < new Date(o2.lastInteractionDate) ? -1 : new Date(o1.lastInteractionDate) > new Date(o2.lastInteractionDate) ? 1 : 0;
            });

            $scope.losingTouchInsights["totalCount"] = losingTouchContacts.length;
            $scope.losingTouchInsights.losingTouchContacts = losingTouchContacts.slice(0,3);
            $scope.losingTouchInsights["displayedCount"] = $scope.losingTouchInsights.losingTouchContacts.length;
            $scope.losingTouchInsights["widthDisplay"] = {'width': ($scope.losingTouchInsights.displayedCount/$scope.losingTouchInsights.totalCount)*100 +'%'}; ;
            $scope.losingTouchInsights.showNoDataMsg = $scope.losingTouchInsights.losingTouchContacts.length == 0 ? true : false;

        })
}

function getDealsClosingSoon($scope, $http, $rootScope) {
    var contactList = [];

    $scope.dealsClosingSoonInsights = {
        dealsClosingSoon: []
    };

    $http.get('/insights/deals/closing/soon')
        .success(function(response) {
            $scope.loadingDealsClosing = false;

            _.each(response.Data, function(contact) {
                var companyName = fetchCompanyFromEmail(contact.contactEmailId);
                var companyLogo = "https://logo.clearbit.com/"+ getTextLength(companyName,25) +".com";

                var obj = {
                    companyName: fetchCompanyFromEmail(contact.contactEmailId),
                    amount: parseFloat(contact.amount.toFixed(2)),
                    closeDate: contact.closeDate,
                    opportunityName: getTextLength(contact.opportunityName,20),
                    opportunityId: contact.opportunityId,
                    noPicFlag: imageExists(companyLogo) ? false : true,
                    noPicText: companyName.slice(0,2),
                    accountImageUrl: companyLogo
                }
                contactList.push(obj);

            });

            contactList.sort(function (o1, o2) {
                return new Date(o1.closeDate) - new Date(o2.closeDate);
            });

            $scope.dealsClosingSoonInsights["totalCount"] = response.Data.length;
            $scope.dealsClosingSoonInsights.dealsClosingSoon = contactList.slice(0,3);
            // $scope.dealsClosingSoonInsights.dealsClosingSoon = contactList;
            $scope.dealsClosingSoonInsights["displayedCount"] = $scope.dealsClosingSoonInsights.dealsClosingSoon.length;
            $scope.dealsClosingSoonInsights["widthDisplay"] = {'width': ($scope.dealsClosingSoonInsights.displayedCount/$scope.dealsClosingSoonInsights.totalCount)*100 +'%'}; 
            $scope.dealsClosingSoonInsights.showNoDataMsg = $scope.dealsClosingSoonInsights.dealsClosingSoon.length == 0 ? true : false;
        })
}

function getMeetings($scope, $http, share) {

    $scope.meetingInsights = {
        todayMeetings:[]
    }

    $http.post('/get/all/meetings', {})
    .success(function(response) {
        $scope.loadingMeetings = false;
        var userId = response.userId;

        if(response.Data) {
            
            response.Data.forEach(function(meeting) {
                if(!meeting.actionItemSlotType) {
                    var meetingDetails = fetchMeetingDetails(meeting, share, userId);
                    meetingDetails['participants'] = meeting.toList;
                    meetingDetails['numberOfMeetingParticipants'] = '';
                    meetingDetails['liu'] = response.Data.userId;

                    if(meeting.toList.length>1) {
                        meetingDetails['numberOfMeetingParticipantsExists'] = true;
                        meetingDetails['numberOfMeetingParticipants'] = meeting.toList.length -1
                        for(var j=0;j<meeting.toList.length;j++){
                            // $rootScope.meetingsWithToday.push(meeting.toList[j].receiverEmailId)
                        }
                    }

                    // $rootScope.meetingsWithToday.push(meetingDetails.personEmailId)

                    // meetingDetails.meetingInitiatorIcon = setMeetingInitiatorIcon(meetingDetails);
                    meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/outgoingOrange.png';
                    if(meetingDetails.isSender && meetingDetails.isAccepted){
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/Meeting_Outgoing_confirmed.png';
                    }

                    if(!meetingDetails.isSender && meetingDetails.isAccepted){
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingGreen.png';
                    }

                    if(!meetingDetails.isSender && !meetingDetails.isAccepted){
                        meetingDetails.meetingInitiatorIcon = '/images/dashboard-icons/IncomingOrange.png';
                    }

                    meetingDetails.notSelfMeeting = true;

                    if(meetingDetails.participants.length === 0){
                        if((meetingDetails.personEmailId === meeting.to.receiverEmailId) || !meeting.to.receiverEmailId){
                            meetingDetails.notSelfMeeting = false;
                        }
                    }

                    if(meetingDetails.personEmailId === 'unknownorganizer@calendar.google.com'){
                        meetingDetails.nameTruncate = "Goal";
                        meetingDetails.picUrl = "/images/dashboard-icons/google-icon.png";
                        meetingDetails.meetingInitiatorIcon = "/images/dashboard-icons/flag.png";
                        meetingDetails.locationTypePic = "";
                        meetingDetails.isGoogleGoal = true;
                        meetingDetails.descriptionTruncate = getTextLength(meetingDetails.description,45);
                        meetingDetails.company = '';
                        meetingDetails.companyTruncate = '';
                    }

                    $scope.meetingInsights.todayMeetings.push(meetingDetails);
                }

            })

            $scope.meetingInsights["totalCount"] = $scope.meetingInsights.todayMeetings.length;
            $scope.meetingInsights.todayMeetings = $scope.meetingInsights.todayMeetings.slice(0,3);
            $scope.meetingInsights["displayedCount"] = $scope.meetingInsights.todayMeetings.length;
            $scope.meetingInsights["widthDisplay"] = {'width': ($scope.meetingInsights.displayedCount/$scope.meetingInsights.totalCount)*100 +'%'}; 
            $scope.meetingInsights.showNoDataMsg = $scope.meetingInsights.todayMeetings.length == 0 ? true : false;
            
        } else {
            $scope.meetingInsights.showNoDataMsg = true;
        }
    })
}

function getPastMeetingFollowUps($scope, $http) {

    $scope.meetingFollowupInsights = {
        meetingFollowup: []
    }
    var meeting = _.filter($scope.meetingInsights.todayMeetings,function (meeting) {
        return meeting.ifRelatasMailEventType === "meetingFollowUp"
    })

    var meetingId = meeting[0]?meeting[0].invitationId:null;

    var url ='/fetch/yesterdays/meetings?invitationId='+meetingId;

    if(meetingId){
        $http.get(url)
          .success(function(response){
              $scope.loadingMeetingFollowUp = false;
              var meetingFollowUp = [];

              _.each(response.Data, function(contact) {
                  var obj = formatMeetingFollowUpContactDetails(contact);
                  meetingFollowUp.push(obj);
              })

              meetingFollowUp.sort(function (o1, o2) {
                  return new Date(o2.sortDate) - new Date(o1.sortDate)
              });

              $scope.meetingFollowupInsights["totalCount"] = meetingFollowUp.length;
              $scope.meetingFollowupInsights.meetingFollowup = meetingFollowUp.slice(0,3);
              $scope.meetingFollowupInsights["displayedCount"] = $scope.meetingFollowupInsights.meetingFollowup.length
              $scope.meetingFollowupInsights["widthDisplay"] = {'width': ($scope.meetingFollowupInsights.displayedCount/$scope.meetingFollowupInsights.totalCount)*100 +'%'};
              $scope.meetingFollowupInsights.showNoDataMsg = $scope.meetingFollowupInsights.meetingFollowup.length == 0 ? true : false;

          })
    } else {
        $scope.loadingMeetingFollowUp = true;
    }
}

function formatMeetingFollowUpContactDetails(contact) {
    var obj = {};
    
    if(checkRequired(contact.personId) && checkRequired(contact.personName)){

        var name = getTextLength(contact.personName,10);
        var image = '/getImage/'+contact.personId;

        obj = {
            fullName:contact.personName,
            name:name,
            image:image
        };

    }
    else {
        var contactImageLink = contact.contactImageLink ? encodeURIComponent(contact.contactImageLink) : null
        obj = {
            fullName: contact.personName,
            name: getTextLength(contact.personName, 10),
            image: '/getContactImage/' + contact.personEmailId + '/' + contactImageLink
            // noPicFlag:true
        };
    }

    obj.emailId = contact.personEmailId;
    obj.interactionDate = contact.interactionDate;
    obj.interactionId = contact.interactionId;
    obj.refId = contact.refId;
    obj.title = contact.title;
    obj.description = contact.description;
    obj.companyName = contact.company;
    obj.designation = contact.designation;
    // obj.filterToFetch = filter;
    obj.actionItemId = contact.actionItemId;
    obj.twitterUserName = contact.twitterUserName
    obj.personId = contact.personId;
    obj.participants = contact.participants;
    obj.meetingTimeFormat = "";
    // obj.meetingDateFormat = moment(contact.meetingDate).tz("IST").format("DD MMM");
    obj.meetingDateFormat = moment(contact.meetingDate).fromNow();
    obj.sortDate = contact.meetingDate;

    return obj;
}

function formatLosingTouchContactDetails(contact) {
    var obj = {};

    if(checkRequired(contact.personId) && checkRequired(contact.contactName)){

        var name = getTextLength(contact.contactName,12);
        var image = '/getImage/'+contact.personId;

        obj = {
            fullName:contact.contactName,
            name:name,
            image:image,
            companyName:contact.company,
            designation:contact.designation
        };

        obj.emailId = contact.contactEmailId;
        obj.recordId = contact.contactId;
        obj.filter = 'losingTouch';

    }
    else {
        var contactImageLink = contact.contactImageLink ? encodeURIComponent(contact.contactImageLink) : null
        obj = {
            fullName: contact.contactName,
            name: getTextLength(contact.contactName, 12),
            image: contactImageLink?'/getContactImage/' + contact.contactEmailId + '/' + contactImageLink:null,
            emailId:contact.contactEmailId,
            // noPicFlag:true
            recordId : contact.contactId,
            filter:'losingTouch',
            companyName:contact.company,
            designation:contact.designation
        };
    }

    obj.personId = contact.personId;
    obj.nameNoImg = contact.personName?contact.personName.substr(0,2).toUpperCase():''
    obj.actionItemId = contact.actionItemId;
    obj.filterToFetch = 'losingTouch';
    obj.twitterUserName = contact.twitterUserName;
    obj.publicProfileUrl = contact.publicProfileUrl;
    obj.lastInteractionDate = contact.lastInteractionDate;
    obj.lastInteractionDateFormatted = moment(contact.lastInteractionDate).format("DD MMM YYYY");
    obj.favoriteClass = contact.favorite?'contact-fav':false;
    obj.isImportant = contact.favorite;

    if(contact.favorite){
        atLeastOneImportantContact = true;
    }

    var contactCompany = fetchCompanyFromEmail(contact.contactEmailId);
    var liuCompanyName = fetchCompanyFromEmail(contact.ownerEmailId);

    if(liuCompanyName == "Others") {
        liuCompanyName = null;
    }

    // if(contact.contactEmailId && liuCompanyName != contactCompany && contact.lastInteractionDays>29){
    if(contact.contactEmailId && contact.lastInteractionDays>29){
        return obj;
    }
}

function fetchMeetingDetails(meeting, share, userId) {
    var counter = 1;

    var locationTruncate = '';
    if(meeting.scheduleTimeSlots[0].location){
        locationTruncate = getTextLength(meeting.scheduleTimeSlots[0].location, 50);
    }

    var titleTruncate = '';
    if(meeting.scheduleTimeSlots[0].title) {
        titleTruncate = getTextLength(meeting.scheduleTimeSlots[0].title, 20)
    }
    
    var date = checkRequired(share.timezone) ? moment(meeting.scheduleTimeSlots[0].start.date).tz(share.timezone) : moment(meeting.scheduleTimeSlots[0].start.date);
    var end = checkRequired(share.timezone) ? moment(meeting.scheduleTimeSlots[0].end.date).tz(share.timezone) : moment(meeting.scheduleTimeSlots[0].end.date);
    var obj = {
        start:new Date(date),
        end:new Date(end),
        sortDate:new Date(date.format()),
        meetingDuration: end.diff(date,'minutes'),
        startTime:date.format("hh:mm A"),
        endTime:end.format("hh:mm A"),
        title:meeting.scheduleTimeSlots[0].title || '',
        description:meeting.scheduleTimeSlots[0].description || '',
        location:meeting.scheduleTimeSlots[0].location || '',
        locationTruncate:locationTruncate,
        titleTruncate:titleTruncate,
        locationType:meeting.scheduleTimeSlots[0].locationType || '',
        date:date.format("DD MMM YYYY"),
        dateUpcoming:date.format("MMM DD YYYY"),
        invitationId:meeting.invitationId,
        url:'/today/details/'+ meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId,
        isPreparedMeeting:isPreparedMeeting(meeting,userId),
        isSender:false,
        isAccepted:meeting.scheduleTimeSlots[0].isAccepted || false,
        locationTypePic: getLocationTypePic(meeting.scheduleTimeSlots[0].locationType),
        ifRelatasMailEvent:meeting.actionItemSlot?meeting.actionItemSlot:false,
        ifRelatasMailEventType:meeting.actionItemSlotType,
        meetingIndex:'re-schedule-meeting'+counter,
        slotId:meeting.scheduleTimeSlots[0]._id,
        hoverClass:meeting.actionItemSlot?'non-meeting-row':'meeting-row',
        updateRelatasEvent:'update-relatas-event'+counter,
        displayPopOver:false
    };

    obj.title = getTextLength(obj.title, 25);

    obj.preparedMeetingUrl = obj.isPreparedMeeting ? null : '/today/details/'+ (meeting.recurrenceId ? meeting.recurrenceId : meeting.invitationId);

    if(meeting.suggested){
        obj.location = checkRequired(meeting.scheduleTimeSlots[0].suggestedLocation) ? meeting.scheduleTimeSlots[0].suggestedLocation : meeting.scheduleTimeSlots[0].location;
    }

    if(meeting.suggested){
        if(meeting.suggestedBy.userId == userId){
            obj.isSender = true;
            if(meeting.senderId == userId){
                var data1 = getDetailsIfSender(meeting);
                obj.picUrl = data1.picUrl;
                obj.name = data1.name;
                obj.userId = data1.userId
                obj.personEmailId = data1.personEmailId
            }
            else{
                var data3 = getDetailsIfReceiver(meeting);
                obj.picUrl = data3.picUrl;
                obj.name = data3.name;
                obj.userId = data3.userId
                obj.personEmailId = data3.personEmailId
            }
        }
        else{
            if(meeting.senderId == meeting.suggestedBy.userId){
                obj.isSender = true;
                var data5 = getDetailsIfReceiver(meeting);
                obj.picUrl = data5.picUrl;
                obj.name = data5.name;
                obj.userId = data5.userId
                obj.personEmailId = data5.personEmailId
            }
            else{
                if(meeting.selfCalendar){
                    for(var j=0; j<meeting.toList.length; j++){
                        if(meeting.suggestedBy.userId == meeting.toList[j].receiverId){
                            obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                            var fName = meeting.toList[j].receiverFirstName || '';
                            var lName = meeting.toList[j].receiverLastName || '';
                            obj.name = fName+' '+lName;
                            obj.personEmailId = meeting.toList[j].receiverEmailId;
                        }
                    }
                }
                else{
                    obj.picUrl = '/getImage/'+meeting.to.receiverId;
                    obj.name = meeting.to.receiverName;
                }
            }
        }
    }
    else{
        if(meeting.senderId == userId){
            obj.isSender = true;
            var data2 = getDetailsIfSender(meeting);

            obj.picUrl = data2.picUrl;
            obj.name = data2.name;
            obj.personEmailId = data2.personEmailId;
            obj.userId = data2.userId;
        }
        else{
            var data4 = getDetailsIfReceiver(meeting);
            obj.picUrl = data4.picUrl;
            obj.name = data4.name;
            obj.personEmailId = data4.personEmailId;
            obj.userId = data4.userId
        }
    }
    if(!checkRequired(obj.picUrl) && !checkRequired(obj.name)){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
    }
    if(!checkRequired(obj.picUrl)){
        obj.picUrl = '/images/default.png';
    }
    obj.prepareButtonText = isMeetingAccepted(meeting,userId) ? "Confirmed" : obj.isSender ? "Not Confirmed" : "Confirm";

    if (obj.prepareButtonText == "Confirmed" || obj.prepareButtonText == "Not Confirmed" && obj.isSender === true) {
        obj.buttonClass = "btn btn-transparent";
    }
    else {
        obj.buttonClass = "btn btn-green";
    }

    obj.nameTruncate = getTextLength(obj.name,10);

    var designation = '';
    var company = '';
    for(var i =0;i<meeting.toList.length;i++){

        if(meeting.toList[i].receiverEmailId == obj.personEmailId){
            company = meeting.toList[i].companyName?meeting.toList[i].companyName:'';
            designation = meeting.toList[i].designation?meeting.toList[i].designation:'';
        } else if(meeting.senderEmailId == obj.personEmailId){
            company = meeting.companyName
            designation = meeting.designation
        }

        var companyTruncate = getTextLength(company,10)
        var designationTruncate = getTextLength(designation,10)
    }

    obj.company = company;
    obj.designation = designation;
    obj.companyTruncate = companyTruncate
    obj.designationTruncate = designationTruncate

    return obj;
}

var isMeetingAccepted = function(meeting,userId){

    var isAccepted = false;
    if(meeting.scheduleTimeSlots && meeting.scheduleTimeSlots.length > 0){
        for(var i=0; i<meeting.scheduleTimeSlots.length; i++){
            if(meeting.scheduleTimeSlots[i].isAccepted){
                isAccepted = true;
            }
        }
    }

    if(isAccepted){
        if(meeting.selfCalendar && meeting.toList && meeting.toList.length > 1){
            if(meeting.senderId != userId){
                var exists = false;
                for(var j=0; j<meeting.toList.length > 0; j++){
                    if(meeting.toList[j].receiverId == userId){
                        exists = true;
                    }
                }
                if(!exists){
                    isAccepted = false;
                }
            }
        }
    }

    return isAccepted;
};

var isPreparedMeeting = function(meeting,userId){

    if(meeting.senderId == userId){
        return meeting.isSenderPrepared;
    }
    else if(meeting.selfCalendar){
        var isExists = false;
        var prepared = true;
        for(var j=0; j<meeting.toList.length; j++){
            if(meeting.toList[j].receiverId == userId){
                isExists = true;
                prepared = meeting.toList[j].isPrepared || false;
            }
        }
        return prepared;
    }
    else{
        if(meeting.to.receiverId == userId){
            return meeting.to.isPrepared || false;
        }else return true;
    }
};

var getLocationTypePic = function(meetingLocationType){

    switch (meetingLocationType) {
        case 'In-Person':
            return "fa fa-map-marker";
            break;
        case 'Phone':
            return "fa fa-mobile-phone";
            break;
        case 'Skype':
            return "fa fa-skype";
            break;
        case 'Conference Call':
            return "fa fa-mobile-phone";
            break;
        default:return "fa fa-map-marker";
            break;
    }
};

var getDetailsIfSender = function(meeting){

    var obj = {};
    if(meeting.selfCalendar == true){
        if(meeting.toList.length > 0){
            var isDetailsExists = false;
            for(var i=0; i<meeting.toList.length; i++){
                if(meeting.toList[i].isPrimary){
                    isDetailsExists = true;
                    obj.picUrl = '/getImage/'+meeting.toList[i].receiverId;
                    var fName = meeting.toList[i].receiverFirstName || '';
                    var lName = meeting.toList[i].receiverLastName || '';
                    obj.name = fName+' '+lName;
                    obj.userId = meeting.toList[i].receiverId;
                    obj.personEmailId = meeting.toList[i].receiverEmailId;
                }
            }
            if(!isDetailsExists){
                for(var j=0; j<meeting.toList.length; j++){
                    if(checkRequired(meeting.toList[j].receiverId)){
                        isDetailsExists = true;
                        obj.picUrl = '/getImage/'+meeting.toList[j].receiverId;
                        var fName2 = meeting.toList[j].receiverFirstName || '';
                        var lName2 = meeting.toList[j].receiverLastName || '';
                        obj.name = fName2+' '+lName2;
                        obj.userId = meeting.toList[j].receiverId
                        obj.personEmailId = meeting.toList[j].receiverEmailId;
                    }
                }
            }
            if(!isDetailsExists){
                obj.picUrl = '/images/default.png';
                obj.name = meeting.toList[0].receiverEmailId;
                obj.personEmailId = meeting.toList[0].receiverEmailId;
            }
        }
    }
    else{
        if(meeting.to){
            if(meeting.to.receiverId){
                obj.picUrl = '/getImage/'+meeting.to.receiverId;
                obj.name = meeting.to.receiverName;
                obj.userId = meeting.to.receiverId
                obj.personEmailId = meeting.to.receiverEmailId;
            }
            else{
                obj.picUrl = '/images/default.png';
                obj.name = meeting.to.receiverEmailId;
                obj.personEmailId = meeting.to.receiverEmailId;
            }
        }
    }
    return obj;
};

var getDetailsIfReceiver = function(meeting){
    var obj = {};
    if(meeting.senderId){
        obj.picUrl = '/getImage/'+meeting.senderId;
        obj.name = meeting.senderName;
        obj.userId = meeting.senderId;
        obj.personEmailId = meeting.senderEmailId;
    }
    else{
        obj.picUrl = '/images/default.png';
        obj.name = meeting.senderEmailId;
        obj.personEmailId = meeting.senderEmailId;
    }
    return obj;
};

function getAllCommitCutOffDates($scope, $http) {
    $scope.commitCutOffLoaded = false;

    console.log("getAllCommitCutOffDates");
    $http.get("/review/get/all/commit/cutoff")
        .success(function (response) {

            if(response){
                $scope.commitCutOffLoaded = true;
                $scope.commitRange[0].cuttOffDate = "Commit Close Date: "+moment(response.week).tz("UTC").format(standardDateFormat()); 
                $scope.commitRange[1].cuttOffDate = "Commit Close Date: "+moment(response.month).tz("UTC").format(standardDateFormat())
                $scope.commitRange[2].cuttOffDate = "Commit Close Date: "+moment(response.quarter.startOfQuarter).tz("UTC").format(standardDateFormat())
                
                $scope.commitRange[0].daysLeft = moment().diff(response.week, 'days')*-1;
                $scope.commitRange[1].daysLeft = moment().diff(response.month, 'days')*-1;
                $scope.commitRange[2].daysLeft = moment().diff(response.quarter.startOfQuarter, 'days')*-1;
            }
        })
}

function fetchCommits($scope,$http,url,mode) {
    $scope.loadingCommits = true; 

    if(!mode){
        mode = "MONTHLY"
        url = '/review/commits/month';
    }

    $http.get(url)
        .success(function (response) {

            $scope.commits = {
            selfCommitValue:0,
            selfCommitValueFormat:0,
            }

            if(response){

                var editAccess = false;
                var selfCommitValue = 0;
                var commitCutOffDate = null;
                var nextCommitCutOffDate = null;

                checkCommitCutOffLoaded();
                function checkCommitCutOffLoaded(){
                    if($scope.commitCutOffLoaded){
                        if(mode == "WEEKLY"){
                            commitCutOffDate = new Date($scope.commitRange[0].cuttOffDate);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"week"));
                            selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount; 

                        } else if(mode == "QUARTERLY"){
                            commitCutOffDate = new Date($scope.commitRange[2].cuttOffDate )
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"quarter"));
                            selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount; 

                        } else {
                            commitCutOffDate = new Date($scope.commitRange[1].cuttOffDate );
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"month"));
                            selfCommitValue = response.commitCurrentMonth.month.userCommitAmount; 

                        }
                    } else {
                        setTimeOutCallback(500,function () {
                            checkCommitCutOffLoaded();
                        })
                    }
                }

                if(commitCutOffDate && moment(commitCutOffDate).endOf('day').toDate() >= new Date()){
                    editAccess = true;
                    $scope.canCommitForNext = false;
                } else {
                    $scope.canCommitForNext = true;
                    $scope.nextCommitCutOffDate = nextCommitCutOffDate;
                }

                $scope.commits = {
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                }

                if(mode == 'WEEKLY')
                    $scope.commitRange[0].commitforNext = $scope.canCommitForNext;
                else if(mode == 'MONTHLY')
                    $scope.commitRange[1].commitforNext = $scope.canCommitForNext;
                else if(mode == 'QUARTERLY')
                    $scope.commitRange[2].commitforNext = $scope.canCommitForNext;

                $scope.selfCommitValue = $scope.commits.selfCommitValue;
                $scope.loadingCommits = false; 

            }
        });
}

function fetchCommitV2($scope,$http,url,mode) {
    if(users){
        if(!$scope.teamCommitsViewing) {
            url = fetchUrlWithParameter(url,"userId",users);
        } else {
            url = fetchUrlWithParameter(url,"hierarchylist",users)
        }
    }

    if(!mode){
        mode = "month"
    }

    $http.get(url)
        .success(function (response) {

            $scope.commits = {
                oppValue:0,
                selfCommitValue:0,
                opps:[],
                selfCommitValueFormat:0,
                response:null,
                graph:{}
            }

            if(response){

                $rootScope.commitStage = response.commitStage
                share.commitStage = response.commitStage

                var editAccess = false;
                var selfCommitValue = 0;
                var commitCutOffDate = null;
                var nextCommitCutOffDate = null;

                checkCommitCutOffLoaded();
                function checkCommitCutOffLoaded(){
                    if(share.commitCutOffObj){

                        if(mode == "week"){
                            commitCutOffDate = new Date(share.commitCutOffObj.week);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"week"));
                        } else if(mode == "quarter"){
                            commitCutOffDate = new Date(share.commitCutOffObj.quarter)
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"quarter"));
                        } else {
                            commitCutOffDate = new Date(share.commitCutOffObj.month);
                            nextCommitCutOffDate = new Date(moment(commitCutOffDate).add(1,"month"));
                        }
                    } else {
                        setTimeOutCallback(500,function () {
                            checkCommitCutOffLoaded();
                        })
                    }
                }

                if(commitCutOffDate && new Date(commitCutOffDate) >= new Date()){
                    editAccess = true;
                    $scope.canCommitForNext = false;
                } else {
                    $scope.canCommitForNext = true;
                    $scope.nextCommitCutOffDate = nextCommitCutOffDate;
                }

                var oppsToDisplay = response.opps;

                if(mode == "week"){
                    selfCommitValue = response.commitsCurrentWeek.week.userCommitAmount?response.commitsCurrentWeek.week.userCommitAmount:0
                    if(new Date()> new Date(response.commitWeek)){
                        oppsToDisplay = response.commitsCurrentWeek.opportunities
                    }

                } else if(mode == "quarter"){
                    if(response.commitCurrentQuarter.quarter.userCommitAmount){
                        selfCommitValue = response.commitCurrentQuarter.quarter.userCommitAmount;
                    }
                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentQuarter.opportunities
                    }

                } else {
                    if(response.commitCurrentMonth.month.userCommitAmount){
                        selfCommitValue = response.commitCurrentMonth.month.userCommitAmount;
                    }

                    if(new Date()> new Date(moment(response.cutOffDate).endOf("day"))){
                        oppsToDisplay = response.commitCurrentMonth.opportunities
                    }
                }

                var oppValue = getOppValInCommitStageForSelectedMonth(oppsToDisplay,response.commitMonth,share.primaryCurrency,share.currenciesObj);

                $scope.commits = {
                    oppValue:oppValue?getAmountInThousands(oppValue,2,share.primaryCurrency=="INR"):0,
                    editAccess:editAccess,
                    selfCommitValue:selfCommitValue,
                    opps:oppsToDisplay,
                    selfCommitValueFormat:getAmountInThousands(selfCommitValue,2,share.primaryCurrency=="INR"),
                    response:response
                }

                if(mode != 'week'){
                    getTargetsAndAchievements($http,users,startDate,null,function (insights) {
                        if(insights){
                            $scope.commits.graph = setGraphValues(insights,response,share,mode,oppValue,$scope);
                        }
                    });
                }

                if($scope.teamCommitsViewing){
                    teamCommitsList($scope,share,response,mode,users,$http)
                }
            }

            setTimeOutCallback(100,function () {
                $scope.actionloadingData = false;
                $scope.loadingData = false;
            })
        });
}

function stripHtmltags(taskName) {

    if ( !taskName || (taskName===''))
        return false;
    else
        taskName = taskName.toString();

    return taskName.replace(/<[^>]*>/g, '');
}

// function for controller today-secondary-insights

function getOverdueInsights(filter, $http, $scope, $rootScope, share) {
    switch(filter) {
        case 'staleOpportunity': getStaleOpportunity($scope, $rootScope, $http);
                                break;
        case 'dealsAtRisk': getDealsAtRisk($scope, $http, share);
                                break;
        case 'taskOverdue': getOverdueTasks($scope, $http);
                                break;
        case 'mailResponsePending': getMailResponsePending($scope, $http);
                            break;
        default: break;

    }
}

function getStaleOpportunity($scope, $rootScope, $http) {
    var url = 'reports/get/opportunities/v2';
    var filterObj = {};
    var filter = [];
    var startDate = moment(new Date("01 Jan 2015"));
    var endDate = moment().subtract(1, "days");

    filter.push({
        includeDateRange: true,
        type: "closeDate",
        start: startDate,
        end: endDate
    })

    filter.push({
        name: $rootScope.liuEmailId,
        type: "userEmailId"
    })

    filterObj.filters = filter;

    $http.post(url,filterObj)
        .success(function(response) {
            $scope.staleOpps = _.filter(response.opps, function(opp) {
                return !(opp.stageName == 'Close Won' || opp.stageName == 'Close Lost')
            });

        })
}

function getParams(url){
    var regex = /[?&]([^=#]+)=([^&#]*)/g,
        params = {},
        match;
    while(match = regex.exec(url)) {
        params[match[1]] = match[2];
    }
    return params;
}
