angular.module('adminUserList', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('userList', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.totalUser = 0;
        $scope.userList = [];
        $scope.category = 'all';
        $scope.listOfCompanies = [];

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
            console.log("List of companies:", $scope.listOfCompanies);
        });


        $scope.getInteractions = function() {
            console.log("get user interactions");
            $http.get('/getListOfAllUser/admin/list', { params: { from: $scope.fromDate, to: $scope.toDate, category: $scope.category } }).then(function(response) {
                var userList = response.data;
                console.log("Response from the server:", response);

                if(userList.length > 0){
                    dateTrimer(userList);
                    $scope.userList = userList;
                    $scope.revers = false;
                    $scope.order("registeredDate");

                    $scope.totalUser = userList.length;
                    
                }
                else{
                    $scope.totalUser = 0;
                    $scope.userList = []; 
                }
            });
        }
        $scope.toExcel = function() {
            $scope.path = '?category='+$scope.category+'&from='+$scope.fromDate+'&to='+$scope.toDate;
        }

        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }
        var dateTrimer = function(array){
            array.forEach(function(ele){
                
                if(ele.createdDate){ 
                    ele.createdDate = ele.createdDate.split('T')[0];
                }

                if(ele.lastDAMSentDate){
                    ele.lastDAMSentDate = ele.lastDAMSentDate.split('T')[0];
                }
                
                if(ele.lastDataSync){
                    ele.lastDataSync = ele.lastDataSync.split('T')[0];;
                }

                if(ele.lastLoginDate)
                    ele.lastLoginDate = ele.lastLoginDate.split('T')[0];
                if(ele.registeredDate)
                    ele.registeredDate = ele.registeredDate.split('T')[0];

                if(ele.lastMobileSyncDate)
                    ele.lastMobileSyncDate = ele.lastMobileSyncDate.split('T')[0];
                
                if(ele.mobileLastLoginDate)
                    ele.mobileLastLoginDate = ele.mobileLastLoginDate.split('T')[0];

                if(ele.mobileOnBoardingDate)
                    ele.mobileOnBoardingDate = ele.mobileOnBoardingDate.split('T')[0];

                if(ele.registeredUser === true && (ele.registeredDate == undefined || ele.registeredDate == null))
                    ele.registeredDate = ele.createdDate;

                if(ele.corporateUser)
                    ele.corporateUser = 'C';
                else
                    ele.corporateUser = 'R';
            });
        }
        
    })
    .filter('byEmailCompanyName', function() {
        return function(names, search) {  
            if(angular.isDefined(search)) {
                var results = [];
                var i;
                var searchVal = search.toLowerCase();
                for(i = 0; i < names.length; i++){
                        var firstName = names[i].firstName.toLowerCase();
                        if(names[i].lastName !== undefined && names[i].lastName !== null)
                            var lastName = names[i].lastName.toLowerCase();
                        else
                            var lastName = '';
                        if(names[i].companyName !== undefined && names[i].companyName !== null)
                            var company = names[i].companyName.toLowerCase();
                        else
                            var company = '';
                        if(names[i].emailId !== undefined && names[i].emailId !== null)
                            var email = names[i].emailId.toLowerCase();
                        else
                            var email = '';
                        if(firstName.indexOf(searchVal) >=0 || lastName.indexOf(searchVal) >=0  || company.indexOf(searchVal) >=0 || email.indexOf(searchVal) >=0){
                        results.push(names[i]);
                        }
                    }
                return results;
            } else {
                return names;
            }
        };
    });
