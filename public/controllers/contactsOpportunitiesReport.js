angular.module('companyReports', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('interaction', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.sort = 'grandTotal';
        $scope.category = 'all';
        $scope.totalUser = 0;
        $scope.totalInteraction = 0;
        $scope.reverse = false;
        $scope.sortType = 'emailId';

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
        });

        $scope.getInteractions = function() {
            var fdate = new Date($scope.fromDate);
            var tdate = new Date($scope.toDate);
            $http.get('/admin/getCompanyUsageReports', { params: { from: fdate, to: tdate, category: $scope.category } }).success(function(response) {
                for(var i=0;i<response.length;i++){
                    response[i].emailId = response[i]._id.emailId
                    response[i].userId = response[i]._id.userId
                }
                $scope.data = response;
            });
        }
        $scope.toExcel = function() {
            $scope.path = $scope.fromDate + '/' + $scope.toDate + '/' + $scope.category;
        }
        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }

        $scope.getInteractionDetails = function (user) {
            getInteractionDetails($scope,$http,user,new Date($scope.fromDate),new Date($scope.toDate))
        }
    });

function getInteractionDetails($scope,$http,user,from,to) {

    var leads = 0;
    var prospects = 0;
    var customers = 0;
    var leadEmails = [],prospectsEmails = [],customersEmails = [];

    for(var i=0;i<user.count;i++){
        if(user.contacts[i] && user.contacts[i].contactRelation){
            if(user.contacts[i].contactRelation.prospect_customer && user.contacts[i].contactRelation.prospect_customer == "lead"){
                leads++
                leadEmails.push(user.contacts[i].contactEmailId)
            }
            if(user.contacts[i].contactRelation.prospect_customer && user.contacts[i].contactRelation.prospect_customer == "prospect"){
                prospects++
                prospectsEmails.push(user.contacts[i].contactEmailId)
            }
            if(user.contacts[i].contactRelation.prospect_customer && user.contacts[i].contactRelation.prospect_customer == "customer"){
                customers++
                customersEmails.push(user.contacts[i].contactEmailId)
            }
        }
    }

    user.contactsInteractions = 0;
    user.contactsOppValue = 0;
    user.contactsNoOfOpp = 0;

    user.leadsInteractions = 0;
    user.prospectsInteractions = 0;
    user.customersInteractions = 0;

    user.leadsOppValue = 0;
    user.prospectsOppValue = 0;
    user.customersOppValue = 0;

    user.leadsNoOfOpp = 0;
    user.prospectsNoOfOpp = 0;
    user.customersNoOfOpp = 0;

    var url = '/admin/user/contacts/opportunities?userEmailId='+user._id.emailId+"&userId="+user.userId
    url = fetchUrlWithParameter(url, 'leads', leadEmails)
    url = fetchUrlWithParameter(url, 'prospects', prospectsEmails)
    url = fetchUrlWithParameter(url, 'customers', customersEmails)

    $http.get(url).success(function(response) {

        if(response){

            if(response.leadsData && response.leadsData.interactions){
                user.leadsInteractions = response.leadsData.interactions.count.r_formatNumber(2)
            }

            if(response.prospectsData && response.prospectsData.interactions){
                user.prospectsInteractions = response.prospectsData.interactions.count.r_formatNumber(2)
            }

            if(response.customersData && response.customersData.interactions){
                user.customersInteractions = response.customersData.interactions.count.r_formatNumber(2)
            }

            if(response.leadsData && response.leadsData.opportunities && response.leadsData.opportunities.length>0){
                var leadsNoOfOpp = response.leadsData.opportunities.length;
                user.leadsOppValue = getOpportunitiesValues(leadsNoOfOpp,response.leadsData.opportunities)
                user.leadsNoOfOpp = leadsNoOfOpp;
            }

            if(response.prospectsData && response.prospectsData.opportunities && response.prospectsData.opportunities.length>0){
                var prospectsNoOfOpp = response.prospectsData.opportunities.length;
                user.prospectsOppValue = getOpportunitiesValues(prospectsNoOfOpp,response.prospectsData.opportunities)
                user.prospectsNoOfOpp = prospectsNoOfOpp;
            }

            if(response.customersData && response.customersData.opportunities && response.customersData.opportunities.length>0){
                var customersNoOfOpp = response.customersData.opportunities.length;
                user.customersOppValue = getOpportunitiesValues(customersNoOfOpp,response.customersData.opportunities)
                user.customersNoOfOpp = customersNoOfOpp;
            }

            if(response.all){
                if(response.all.past30DaysInteractions && response.all.past30DaysInteractions.past30days){
                    user.contactsInteractions = response.all.past30DaysInteractions.past30days;
                }

                if(response.all.opportunities && response.all.opportunities[0]){
                    user.contactsOppValue = response.all.opportunities[0].opportunitiesValue.r_formatNumber(2);
                    user.contactsNoOfOpp = response.all.opportunities[0].opportunitiesCount;
                }
            }

            user.leadsCount = leads;
            user.prospectsCount = prospects;
            user.customersCount = customers;

        }
    });
}

function getOpportunitiesValues(length,values) {

    var amount = 0
    for(var i =0;i<length;i++){
        amount = amount+values[i].amount
    }

    return amount.r_formatNumber(2);
}

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue) {

    if (parameterValue instanceof Array)
        if (parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if (parameterValue != undefined && parameterValue != null) {
        if (baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl += parameterName + "=" + parameterValue
    }

    return baseUrl
}