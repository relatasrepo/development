angular.module('adminUserList', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('emailClassification', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.list = [];

        $scope.getClassificationList = function() {
            if($scope.emailId) {
                $http.get('/admin/email/classification/list', {
                    params: {
                        from: $scope.fromDate,
                        to: $scope.toDate,
                        emailId: $scope.emailId.toLowerCase()
                    }
                }).then(function (response) {
                    var list = response.data.Data;
                    if (list.length > 0) {
                        // $scope.order("registeredDate");
                        $scope.list = list;
                    }
                    else {
                        $scope.list = [];
                    }
                });
            }   
        }
        $scope.toExcel = function() {
            $scope.path = '?category='+$scope.category+'&from='+$scope.fromDate+'&to='+$scope.toDate;
        }

        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }

    })
    .filter('byEmailCompanyName', function() {
        return function(names, search) {
            if(angular.isDefined(search)) {
                var results = [];
                var i;
                var searchVal = search.toLowerCase();
                for(i = 0; i < names.length; i++){
                    var firstName = names[i].firstName.toLowerCase();
                    if(names[i].lastName !== undefined && names[i].lastName !== null)
                        var lastName = names[i].lastName.toLowerCase();
                    else
                        var lastName = '';
                    if(names[i].companyName !== undefined && names[i].companyName !== null)
                        var company = names[i].companyName.toLowerCase();
                    else
                        var company = '';
                    if(names[i].emailId !== undefined && names[i].emailId !== null)
                        var email = names[i].emailId.toLowerCase();
                    else
                        var email = '';
                    if(firstName.indexOf(searchVal) >=0 || lastName.indexOf(searchVal) >=0  || company.indexOf(searchVal) >=0 || email.indexOf(searchVal) >=0){
                        results.push(names[i]);
                    }
                }
                return results;
            } else {
                return names;
            }
        };
    });

