angular.module('companyReports', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('interaction', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.sort = 'grandTotal';
        $scope.category = 'all';
        $scope.totalUser = 0;
        $scope.totalInteraction = 0;
        $scope.reverse = false;
        $scope.sortType = 'emailId';

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
        });

        $scope.getInteractions = function() {
            var fdate = new Date($scope.fromDate);
            var tdate = new Date($scope.toDate);
            $http.get('/admin/getCompanyUsageReports', { params: { from: fdate, to: tdate, category: $scope.category } }).success(function(response) {
                for(var i=0;i<response.length;i++){
                    response[i].emailId = response[i]._id.emailId
                }
                $scope.data = response;
            });

        }
        $scope.toExcel = function() {
            $scope.path = $scope.fromDate + '/' + $scope.toDate + '/' + $scope.category;
        }
        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }

        $scope.getInteractionDetails = function (user) {
            getInteractionDetails($scope,$http,user,new Date($scope.fromDate),new Date($scope.toDate))
        }
    });

function getInteractionDetails($scope,$http,user,from,to) {

    var leads = 0;
    var prospects = 0;
    var customers = 0;

    for(var i=0;i<user.count;i++){
        if(user.contacts[i] && user.contacts[i].contactRelation){
            if(user.contacts[i].contactRelation.prospect_customer && user.contacts[i].contactRelation.prospect_customer == "lead"){
                leads++
            }
            if(user.contacts[i].contactRelation.prospect_customer && user.contacts[i].contactRelation.prospect_customer == "prospect"){
                prospects++
            }
            if(user.contacts[i].contactRelation.prospect_customer && user.contacts[i].contactRelation.prospect_customer == "customer"){
                customers++
            }
        }
    }

    user.numberOfloginsPerDay = 0;
    user.logins30days = 0;
    user.past30days = 0;
    user.totalOppCount = 0;
    user.interactionsOnRelatas = 0;
    user.afterInteractions = 0;
    user.beforeInteractions = 0;
    user.relataspast30days = 0;

    $http.get('/admin/user/interactions/v2', { params: { from: from, to: to,userId:user._id.userId,userEmailId:user._id.emailId,joiningDate:user._id.createdDate} }).success(function(response) {

        if(response){
            if(response.userReports && response.userReports.length>0){
                var numberOfloginsPerDay = 0;
                for(var i=0;i<response.userReports.length;i++){
                    numberOfloginsPerDay = numberOfloginsPerDay+response.userReports[i].count;
                }
                user.numberOfloginsPerDay = Math.ceil(numberOfloginsPerDay/response.userFromDays);
            }

            if(response["past30Days"] && response["past30Days"].past30days){
                user.past30days = Math.ceil(response["past30Days"].past30days);
            }
            if(response.oppCount && response.oppCount.opportunitiesCount){
                user.totalOppCount = Math.ceil(response.oppCount.opportunitiesCount);
            }
            if(response.interactionsOnRelatas && response.interactionsOnRelatas.relatasSource){
                user.interactionsOnRelatas = Math.ceil(response.interactionsOnRelatas.relatasSource/response.userFromDays);
            }
            if(response.afterInteractions && response.afterInteractions.afterCount){
                user.afterInteractions = Math.ceil(response.afterInteractions.afterCount/response.userFromDays);
            }
            if(response.beforeInteractions && response.beforeInteractions.beforeCount){
                user.beforeInteractions = Math.ceil(response.beforeInteractions.beforeCount/90);
            }
            if(response.interactionsOnRelatasPast30Days && response.interactionsOnRelatasPast30Days.relatasSource){
                user.relataspast30days = Math.ceil(response.interactionsOnRelatasPast30Days.relatasSource);
            }
            if(response.userReportsPast30Days && response.userReportsPast30Days.count){
                user.logins30days = Math.ceil(response.userReportsPast30Days.count);
            }

            user.leads = leads;
            user.prospects = prospects;
            user.customers = customers;
        }
    });
}
