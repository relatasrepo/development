(function() {
    'use strict';
    angular.module('oppStages', ['ngDragToReorder','ngLodash']);

})();

(function () {
    'use strict';

    angular.module('oppStages')
        .component('simpleList', {
            templateUrl: "/corporate/admin/stage/directive",
            controller: listController
        });

    /* @ngInject */
    function listController(ngDragToReorder,$scope,$http) {
        this.isSupported = ngDragToReorder.isSupported();
        this.draggable = true;
        this.character = '';
        $scope.showModal = {
            display:false
        };

        var that = this;
        var companyObject = JSON.parse($('#company-object-hidden').text());
        var url = '/corporate/admin/opportunity/stages/';

        if(companyObject && companyObject._id) {
            url = '/corporate/admin/opportunity/stages/'+companyObject._id
            getStageValues($http,url,function (response) {
                that.stages = response.companyInfo.opportunityStages;
            })
        }

        $scope.$on('dragToReorder.cardDropped', (e, data) => {
            this.stages = data.list.map((stage, i) => {
                stage.order = i + 1;
                return stage;
            });

            saveStageValues($http,this.stages,function (response) {
            })

        });

        this.saveSettings = () => {

            var hasBlankForm = false;

            _.each(this.stages,function (st) {
                if(st.name.length == 0 || !st.weight) {
                    st.hasBlankForm = true;
                    hasBlankForm = true;
                }
            });

            if(!hasBlankForm){
                var existingStages = _.map(this.stages,"name");

                if(hasDuplicates(existingStages)){
                    toastr.error("Opportunity stages have to be unique");
                } else {

                    $scope.editData=false;

                    _.each(this.stages,function (st) {
                        st.EditThis = false;
                    });

                    saveStageValues($http,this.stages,function (response) {
                        // toastr.success("Opportunity stages saved successfully");
                        getStageValues($http,url,function (response) {
                            that.stages = response.companyInfo.opportunityStages;
                        })
                    })
                }
            } else {
                toastr.error("Opportunity stage and weight can not be empty");
            }
        }

        this.editSettings = () => {
            _.each(this.stages,function (st) {
                if(st.name != "Close Won" && st.name != "Close Lost"){
                    st.EditThis = true;
                    $scope.editData = true;
                }
            });
        }

        this.handleRadioClick = (stage) => {
            $scope.newStage = stage
        }
        
        this.transferOpps = () => {

            var obj = {
                companyId:companyObject._id,
                stage:$scope.newStage.name,
                fromStage:$scope.fromOpp.name
            }

            if($scope.newStage){
                updateStageValuesForOpps($http,obj,function (response) {
                    if(response){
                        // toastr.success($scope.fromOpp.oppsCount+" opportunities transferred and stage deleted.");

                        that.stages = that.stages.filter((st) => {
                            return st.name!= $scope.fromOpp.name;
                        });

                        that.stages = that.stages.map((stage, i) => {
                            stage.order = i + 1;
                            return stage;
                        });

                        saveStageValues($http,that.stages,function (response) {
                            getStageValues($http,url,function (response) {
                                that.stages = response.companyInfo.opportunityStages;
                            })
                        })
                    } else {
                        // toastr.error("Opportunity stage update failed. Please try again later");
                    }
                })
            }
            
            this.cancel()
        }

        this.cancel = () => {
            $scope.showModal = {
                display:false
            };
            $scope.newStage = null
            $scope.editData = false;

            _.each(this.stages,function (st) {
                st.EditThis = false;
            });

            getStageValues($http,url,function (response) {
                that.stages = response.companyInfo.opportunityStages;
            })
        }

        this.commitThisStage = (stage) => {

            _.each(this.stages,function (st) {
                if(st.name != stage.name){
                    st.commitStage = false;
                }
            });

            var defaultCommitStageSet = false;

            _.each(this.stages,function (st) {
                if(st.commitStage){
                    defaultCommitStageSet = true;
                }
            });

            if(!defaultCommitStageSet){
                _.each(this.stages,function (st) {
                    if(st.name == stage.name){
                        st.commitStage = true;
                    }
                });

                toastr.error("Commit Stage can not be empty.")

            } else {

                saveStageValues($http,this.stages,function (response) {
                    toastr.success(stage.name + " set to commit stage.");
                })
            }
        }

        this.limitMaxValue = (stage) => {

            if(stage.weight.length>0){
                var weight = parseFloat(stage.weight);

                $("#checkForInvalid").val(weight)

                if(isNaN(weight)){
                    stage.weight = 0
                    if (window.confirm('Weight has to be a number.')) {
                        stage.weight = 0
                    }
                } else {

                    if(weight>100){
                        if (window.confirm('Weight can not be more than 100')) {
                            stage.weight = 100
                            $("#checkForInvalid").val(weight)
                        }
                    }
                }
            }
        }

        this.deleteStage = (stage) => {

            $scope.showModal = {
                display:false
            };

            if(stage.oppsCount>0){
                $scope.showModal = {
                    display:true
                };
                $scope.fromOpp = stage;
                $scope.oppsCount = stage.oppsCount;
            } else {

                this.stages = this.stages.filter((st) => {
                        return st.name!= stage.name;
                });

                this.stages = this.stages.map((stage, i) => {
                    stage.order = i + 1;
                    return stage;
                });

                saveStageValues($http,this.stages,function (response) {
                    // toastr.success(stage.name + " stage deleted.");
                })
            }
        }

        this.keyup = e => {
            if (e.keyCode === 13) this.add();
        };

        this.add = () => {
            if (this.character) {
                let newStage = {
                    order: this.stages.length + 1,
                    name: this.character
                };
                this.stages.push(newStage);
                this.character = '';
                this.editSettings()
            }

            let blankStage = {
                order: this.stages.length + 1,
                name: ""
            };
            this.stages.push(blankStage);
            
            this.editSettings()
        }
    }
})();

function getStageValues($http,url,callback) {
    $http({
        method: 'GET',
        url: url
    }).then(function (success){
        if(success && success.data){
            callback(success.data)
        } else {
            callback(null)
        }
    },function (error){
        callback(null)
    });
}

function saveStageValues($http,body,callback) {

    body = _.uniqBy(body,"name");

    $http({
        method: 'POST',
        url: "/corporate/admin/update/stage",
        datatype:'JSON',
        data:{payload:body}
    }).then(function (success){
        if(success && success.data){
            callback(success.data)
        } else {
            callback(null)
        }
    },function (error){
        callback(null)
    });
}

function updateStageValuesForOpps($http,body,callback) {
    $http({
        method: 'POST',
        url: '/corporate/admin/update/opps/stage',
        datatype:'JSON',
        data:body
    }).then(function (success){
        if(success && success.data){
            callback(success.data)
        } else {
            callback(null)
        }
    },function (error){
        callback(null)
    });
}

function hasDuplicates(array) {
    return _.some(array, function(elt, index) {
        return array.indexOf(elt) !== index;
    });
}

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    if( el.value.length>3){
        return false;
    }
    return true;
}