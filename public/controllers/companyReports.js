angular.module('companyReports', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('interaction', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.sort = 'grandTotal';
        $scope.category = 'all';
        $scope.totalUser = 0;
        $scope.totalInteraction = 0;
        $scope.revers = true;

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
        });

        $scope.getInteractions = function() {
            var fdate = new Date($scope.fromDate);
            var tdate = new Date($scope.toDate);
            $http.get('/admin/getCompanyReports', { params: { from: fdate, to: tdate, category: $scope.category } }).success(function(response) {
                $scope.data = response.data;
            });

        }
        $scope.toExcel = function() {
            $scope.path = $scope.fromDate + '/' + $scope.toDate + '/' + $scope.category;
        }
        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }
    });
