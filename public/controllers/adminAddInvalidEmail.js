angular.module('relatas', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('invalidEmail', function($scope, $resource, $http) {

        $scope.sortType = 'occurence';
        $scope.sortReverse  = true;  // set the default sort order

        $scope.invalidString = '';
        $scope.exceptionString = '';
        $scope.list = [];

        $scope.invalid = true;
        $scope.exception = false;
        $scope.showPopUp = false;

        $scope.currentLength = 0;
        $scope.show_contacts_back = false;
        var limit = 25;
        $scope.limit = limit;
        $scope.currentPage = 1;
        $scope.corporate = true;
        var isCorporate = true;

        getList($scope,$http,'/admin/invalid/contacts/list?type=invalid&skip='+0+'&limit='+limit,false,'invalid');

        $scope.loadMoreContacts = function(skip,isBack){

            isCorporate = $scope.corporate?true:false;
            if(isBack){
                $scope.currentPage--;
                $scope.currentLength = $scope.currentLength-25;
                skip = skip-25
            } else {
                $scope.currentPage++;
            }

            if(skip < 25){
                skip = 0
                $scope.currentLength = 0;
            }

            $scope.cacheSkip = skip;

            if($scope.currentType == 'exception'){
                getList($scope,$http,'/admin/invalid/contacts/list?type=exception&skip='+skip+'&limit='+limit+'&corporate='+isCorporate,false,'exception')
            } else {
                getList($scope,$http,'/admin/invalid/contacts/list?type=invalid&skip='+skip+'&limit='+limit+'&corporate='+isCorporate,isBack,'invalid')
            }
        };

        $scope.triggerChange = function(){
            isCorporate = $scope.corporate?true:false;
            if($("#optionsRadios1").is(":checked")){
                $scope.currentPage = 1
                $scope.currentLength = 0;
                getList($scope,$http,'/admin/invalid/contacts/list?type=invalid&skip='+0+'&limit='+limit+'&corporate='+isCorporate,false,'invalid')
                // $http.get('/admin/invalid/contacts/list',{params:{type:'invalid'}}).then(function(response) {
                //     $scope.list = addClass(response.data);
                    $scope.currentType = 'invalid';
                    $scope.invalid = true;
                    $scope.exception = false;
                // });
            }
            else if($("#optionsRadios2").is(":checked")){
                $scope.currentPage = 1;
                $scope.currentLength = 0;
                getList($scope,$http,'/admin/invalid/contacts/list?type=exception&skip='+0+'&limit='+limit+'&corporate='+isCorporate,false,'exception')
                // $http.get('/admin/invalid/contacts/list',{params:{type:'exception'}}).then(function(response) {
                //     $scope.list = addClass(response.data);
                    $scope.currentType = 'exception';
                    $scope.invalid = false;
                    $scope.exception = true;
                // });
            }
        };
        
        $scope.getUsers = function(){

            if($("#optionsRadios5").is(":checked")){
                $scope.currentPage = 1
                $scope.currentLength = 0;
                $scope.nonCorporate = false;
                getList($scope,$http,'/admin/invalid/contacts/list?type='+$scope.currentType+'&skip='+0+'&limit='+limit+'&corporate='+true,false,$scope.currentType)
                // $http.get('/admin/invalid/contacts/list',{params:{type:'invalid'}}).then(function(response) {
                //     $scope.list = addClass(response.data);
                //     $scope.currentType = 'invalid';
                //     $scope.invalid = true;
                //     $scope.exception = false;
                // });
            }
            else if($("#optionsRadios6").is(":checked")){
                $scope.corporate = false;
                $scope.currentPage = 1;
                $scope.currentLength = 0;
                getList($scope,$http,'/admin/invalid/contacts/list?type='+$scope.currentType+'&skip='+0+'&limit='+limit+'&corporate='+false,false,$scope.currentType)
                // $http.get('/admin/invalid/contacts/list',{params:{type:'exception'}}).then(function(response) {
                //     $scope.list = addClass(response.data);
                //     $scope.currentType = 'exception';
                //     $scope.invalid = false;
                //     $scope.exception = true;
                // });
            }
        };
        
        $scope.addToList = function(type){
            var listToAdd = [];
            if(type == 'invalid'){
                listToAdd = $scope.invalidString.split(',');
            }
            else if(type == 'exception'){
                listToAdd = $scope.exceptionString.split(',');
            }
            var newList = [];
            listToAdd.forEach(function(a){
                if(a.indexOf('.') != -1){
                    var esc = [a.slice(0, a.indexOf('.')), '\\', a.slice(a.indexOf('.'))].join('');
                    newList.push(esc);
                }
                else{
                    newList.push(a)
                }
            })
            listToAdd = newList;

            if(listToAdd.length == 0 || (listToAdd.length == 1 && listToAdd[0] == "")){
                toastr.error("Please enter content")
            }else{
                $http.post('/admin/invalid/contacts/add', {list:listToAdd, type:type})
                    .success(function (data, status, headers, config) {
                        if(data) {
                            if(data.nModified == 1) {
                                toastr.success("Added successfully.");
                            }
                            else if(data.nModified == 0 && data.ok ==1) {
                                toastr.success("This string already exists in invalid list.");
                            }

                            if(type == 'invalid'){
                                $scope.invalid = true;
                                $scope.exception = false;
                            }
                            else{
                                $scope.invalid = false;
                                $scope.exception = true;
                            }

                            $http.get('/admin/invalid/contacts/list',{params:{type:type}}).then(function(response) {
                                $scope.list = addClass(response.data);
                                $scope.currentType = type;
                            });
                        }
                        else {
                            toastr.error("Something is wrong, try again later.")
                        }
                    })
            }
        }

        $scope.deleteFromList = function(type){
            var listToAdd = [];
            if(type == 'invalid'){
                listToAdd = $scope.invalidString.split(',');
            }
            else if(type == 'exception'){
                listToAdd = $scope.exceptionString.split(',');
            }
            if(listToAdd.length == 0 || (listToAdd.length == 1 && listToAdd[0] == "")){
                toastr.error("Please enter content")
            }else{
                $http.post('/admin/invalid/contacts/delete', {list:listToAdd, type:type})
                    .success(function (data, status, headers, config) {
                        if(data) {
                            if(data.nModified == 1) {
                                toastr.success("Deleted successfully.");
                            }
                            else if(data.nModified == 0 && data.ok ==1) {
                                toastr.success("This string doesn't exists in invalid list.");
                            }
                            if(type == 'invalid'){
                                $scope.invalid = true;
                                $scope.exception = false;
                            }
                            else{
                                $scope.invalid = false;
                                $scope.exception = true;
                            }
                            $http.get('/admin/invalid/contacts/list',{params:{type:type}}).then(function(response) {
                                $scope.list = addClass(response.data);
                                $scope.currentType = type;
                            });
                        }
                        else {
                            toastr.error("Something is wrong, try again later.")
                        }
                    })
            }
        }

        $scope.closePopUp = function(){
            $scope.listByPattern = []
            $scope.showPopUp = false;
            $scope.currentPattern = '';
        }
        
        $scope.deleteContactsByPattern = function(item){

            isCorporate = $scope.corporate?true:false;
            console.log(item)
            if(confirm("Are you sure, you want to delete "+ item.occurence +" contacts.")) {
                var url = '/admin/invalid/contacts/delete/or/edit?action=delete'+'&corporate='+isCorporate;
                // url = fetchUrlWithParameter(url,'contacts',item.usersList)

                console.log(url)
                $http.post(url,{contacts:item.usersList})
                // $http.post('/admin/invalid/contacts/delete/or/edit', {pattern: item.pattern, action: 'delete'})
                    .success(function (data) {
                        if (data) {
                            toastr.success("Deleted " + data.nModified + " invalid contacts successfully.");
                            var skip = $scope.cacheSkip?$scope.cacheSkip:0
                            var url = '/admin/invalid/contacts/list?type='+$scope.currentType+'&skip='+skip+'&limit='+limit
                            getList($scope,$http,url,false,$scope.currentType);
                            // $http.get('/admin/invalid/contacts/list', {params: {type: $scope.currentType}}).then(function (response) {
                            //     $scope.list = addClass(response.data);
                            // });
                        }
                        else {
                            toastr.error("Something is wrong, try again later.")
                        }
                    });
            }
        }

        $scope.inactiveContactsByPattern = function(item){
            isCorporate = $scope.corporate?true:false;
            if(confirm("Are you sure, you want to inactivate "+ item.occurence +" contacts.")) {
                $http.post('/admin/invalid/contacts/delete/or/edit', {
                        pattern: item.pattern,
                        action: 'updateAsInactive'
                    })
                    .success(function (data) {
                        if (data) {
                            toastr.success("inactivated " + data.nModified + " invalid contacts successfully.");
                            getList($scope,$http,'/admin/invalid/contacts/list?type='+$scope.currentType+'&skip='+$scope.cacheSkip?$scope.cacheSkip:0+'&limit='+limit+'&corporate='+isCorporate,false,$scope.currentType);
                            // $http.get('/admin/invalid/contacts/list', {params: {type: $scope.currentType}}).then(function (response) {
                            //     $scope.list = addClass(response.data);
                            // });
                        }
                        else {
                            toastr.error("Something is wrong, try again later.")
                        }
                    });
            }
        }
        
        $scope.showListByPattern = function(item){
            console.log(item)
            $scope.listByPattern = []
            $scope.showPopUp = true;
            $scope.currentPattern = item.pattern;

            $scope.listByPattern = addClass(item.usersList);
            // $http.get('/admin/invalid/contacts/list/by/pattern',{params:{type:$scope.currentType,pattern:item.pattern}}).then(function(response) {
            //     console.log(response.data)
            //     $scope.listByPattern = addClass(response.data);
            // });
        };
    });


angular.element(document).ready(function(){
//var pageloadCount = 0;
//function loadModal($http) {

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        'positionClass': 'toast-top-full-width',
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "3000",
        "hideDuration": "1000",
        "timeOut": "8000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
});

function addClass(list){
    list.forEach(function(a){
       if(a.occurence > 0){
           a.class = 'hilight'
       } 
       else{
        a.class = 'normal'    
       }
    });
    return list;
}

function getList($scope,$http,url,isBack,currentType) {

    $http.get(url).then(function(response) {

        $scope.grandTotal = response.data.grandTotal;

        if($scope.grandTotal > 0) {
            if (!isBack || $scope.currentLength <= 0) {
                $scope.currentLength += 25;
            }

            if ($scope.currentLength > 25) {
                $scope.show_contacts_back = true;
            } else $scope.show_contacts_back = false;
        }

        $scope.list = addClass(response.data.contacts);

        $scope.numberOfPages = Math.ceil(response.data.grandTotal/$scope.limit);
        
        $scope.currentType = currentType?currentType:'invalid';

    });
}

function fetchUrlWithParameter(baseUrl, parameterName, parameterValue) {

    if (parameterValue instanceof Array)
        if (parameterValue.length > 0)
            parameterValue = parameterValue.join(",")
        else
            parameterValue = null

    if (parameterValue != undefined && parameterValue != null) {
        if (baseUrl.indexOf("?") == -1)
            baseUrl += "?"
        else
            baseUrl += "&"
        baseUrl += parameterName + "=" + parameterValue
    }

    return baseUrl
}