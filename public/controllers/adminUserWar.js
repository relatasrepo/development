angular.module('adminWarReport', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('interaction', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.sort = 'grandTotal';
        $scope.category = 'all';
        $scope.totalUser = 0;
        $scope.totalInteraction = 0;
        $scope.revers = true;

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
        });

        $scope.getWarData = function() {
            var fdate = new Date($scope.fromDate);
            var tdate = new Date($scope.toDate);
            $scope.notificationData = [];

            $http.post('/get/war/data', { fromDate: fdate, toDate: tdate, companyId: $scope.category})
            .then(function(response) {
                console.log(response);
            });

        }

        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }

        $scope.getCompanyName = function(companyId) {
            var companyObj;

            companyObj = _.find($scope.listOfCompanies, function(company) {
                                if(company._id == companyId) {
                                    return company.companyName;
                                }
                            });
            return companyObj.companyName;
        }
    });

