angular.module('relatasAdminCompanyHierarchy', ['angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('relatasAdminHierarchy', function($scope, $http) {
        $scope.name = 'sunil';
        $scope.completeArray = [];
        $scope.companyName = null;
        $scope.dataArray = [];
        google.charts.load('current', {packages:["orgchart"]});

        $http({
          method: 'GET',
          url: '/admin/corporate/hierarchyDraw'
        }).then(function successCallback(response) {
            response.data.array.forEach(function(ele){
                ele.noImage = 0;
                $scope.completeArray.push(ele);
            });
            $scope.companyName = response.data.companyName;
            $scope.completeArray.forEach(function(ele){
                var modifiedImageUrl;
                var img = new Image();
                img.src = ele.imageUrl;

                img.onload = function() {
                    modifiedImageUrl = '<img class="img-circle" style="float:left;margin:10px 0px 10px 10px;vertical-align: middle;" width="46px" height="46px" src="'+ele.imageUrl+'">';
                    $scope.dataArray.push([
                        {   
                            v:ele.nameF+' '+ele.nameL,
                            f: modifiedImageUrl + '<p class="textP"><div style="font-whight:bold">'+ele.nameF+' '+ele.nameL+'</div><div style="">'+ ele.designation +'</div><div>'+ele.email+'</div></p>'
                        }
                        ,ele.parentName
                        ,ele.email
                    ]);
                };
                img.onerror = function() {
                    modifiedImageUrl = '<span class="image-placeholder-small-text textImage" style="float:left;margin:10px 0px 10px 10px;">'+ele.nameF.substring(0,2)+'</span>';
                    $scope.dataArray.push([
                        {   
                            v:ele.nameF+' '+ele.nameL,
                            f: modifiedImageUrl + '<p class="textP"><div style="font-whight:bold">'+ele.nameF+' '+ele.nameL+'</div><div style="">'+ ele.designation +'</div><div>'+ele.email+'</div></p>'
                        }
                        ,ele.parentName
                        ,ele.email
                    ]);
                };
                $scope.dataArray.sort(function(a, b) {
                    var textA = a.nameF.toUpperCase();
                    var textB = b.nameF.toUpperCase();
                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                });
                google.charts.setOnLoadCallback(drawChart);
                setTimeout(function(){
                    drawChart();
                },2500);
            });
        });

        function drawChart() {  
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'email');

            data.addRows($scope.dataArray);
            var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
            chart.draw(data, {allowHtml:true,size:'medium',nodeClass:'node',selectedNodeClass:'nodeSelected'});
        };

    });