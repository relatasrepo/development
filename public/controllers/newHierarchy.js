angular.module('newHierarchy', ['angular-loading-bar','ngLodash'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('hierarchy', function($scope, $http,$rootScope) {
        $scope.deleteButton = false;
        $scope.add = false;
        $scope.showDelete = true;
        $scope.showAdd = true;

        $scope.dataArray = [];
        $scope.completeArray = [];
        $scope.nodeToAdd = null;
        $scope.currentNode = null;
        $scope.addFlag = 0;
        $scope.confirmExit = 0;
        var chart;
        $scope.noImage = 0;

        $scope.model = {
            selected: null,
            unassignedArray: []
        };

        $scope.searchList = [];

        $scope.addUser = function (user) {

            $http.post('/corporate/admin/set/org/head',user)
                .success(function (response) {
                    $scope.orgHead = user
                    $rootScope.isOrgHeadSet = true;
                    $scope.showResults = false;
                });
        }

        $rootScope.isOrgHeadSet = false;

        $scope.blur =''
        $scope.toggleHelp = function () {
            $scope.showHelp = !$scope.showHelp;

            if($scope.blur=='blur'){
                $scope.blur=''
            } else {
                $scope.blur=''
            }
        }

        $scope.searchForUser = function (query) {

            if(query && query.length>3){
                query = query.toLowerCase();

                var results = $scope.teamMembers.map(function (el) {
                    if(_.includes(el.email,query) || _.includes(el.nameF,query) || _.includes(el.nameL,query)){

                        var image = '/getImage/'+el.id;

                        return {
                            fullName:el.nameF +" "+ el.nameL,
                            name:el.nameF +" "+ el.nameL,
                            image:image,
                            emailId:el.email,
                            userId:el.id,
                            companyId:el.companyId
                        };
                    }
                });

                $scope.searchList = _.compact(results);

                $scope.showResults = $scope.searchList.length>0;
            } else {
                $scope.showResults = false
            }
        };

        google.charts.load('current', { packages: ["orgchart"] });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "3000",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "3000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        getTeamData();

        function getTeamData() {

            $http({
                method: 'GET',
                url: '/corporate/admin/getHierarchy'
            }).then(function successCallback(response) {

                $scope.teamMembers = response.data
                response.data.forEach(function(ele) {
                    ele.noImage = 0;
                    $scope.completeArray.push(ele);
                });

                $scope.completeArray.forEach(function(ele) {

                    var modifiedImageUrl;
                    var img = new Image();
                    img.src = ele.imageUrl;

                    if(ele.orgHead){
                        $rootScope.isOrgHeadSet = true;

                        var image = '/getImage/'+ele.id;
                        $scope.orgHead = {
                            fullName:ele.nameF +" "+ ele.nameL,
                            name:ele.nameF +" "+ ele.nameL,
                            image:image,
                            emailId:ele.email,
                            userId:ele.id
                        }
                    }

                    img.onload = function() {
                        modifiedImageUrl = '<div style="float:left;width: 62px;" ng-click="test()"><img class="img-circle" style="margin:10px 0 10px 10px;vertical-align: middle;" width="46px" height="46px" src="' + ele.imageUrl + '"></div>';
                        $scope.dataArray.push([{
                            v: ele.nameF + ' ' + ele.nameL,
                            f: modifiedImageUrl + '<div class="textP"><div style="font-weight:bold">' + ele.nameF + ' ' + ele.nameL + '</div><div style="">' + ele.designation + '</div><div>' + ele.email + '</div></div>'
                        }, ele.parentName, ele.email]);
                    };
                    img.onerror = function() {
                        modifiedImageUrl = '<div class="image-placeholder-small-text textImage" style="margin:2px 5px 2px 10px">' + ele.nameF.substring(0, 2) + '</div>';
                        $scope.dataArray.push([{
                            v: ele.nameF + ' ' + ele.nameL,
                            f: modifiedImageUrl + '<div class="textP"><div style="font-weight:bold">' + ele.nameF + ' ' + ele.nameL + '</div><div style="">' + ele.designation + '</div><div>' + ele.email + '</div></div>'
                        }, ele.parentName, ele.email]);
                    };

                    if (ele.parentName === null || ele.parentName === undefined) {
                        ele.noImage = 0;
                        $scope.model.unassignedArray.push(ele);
                    }

                    // $scope.dataArray.sort();
                    $scope.dataArray.sort(function(a, b) {
                        var textA = a.nameF.toUpperCase();
                        var textB = b.nameF.toUpperCase();
                        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                    });
                    google.charts.setOnLoadCallback(drawChart);
                    setTimeout(function(){
                        drawChart();
                    },2500);
                });

            });
        }

        $scope.save = function() {
            if($scope.confirmExit !== 0){
                toastr.clear();
                toastr.options.positionClass = "toast-top-full-width";
                $http.post('/corporate/admin/updateHierarchyNew', { hierarchy: $scope.completeArray }).success(function(response) {
                    console.log("success:" + response.status);
                    toastr.options.timeOut = 2000;
                    toastr.success("Successfully Saved");
                }).error(function(err) {
                    console.log("failure");
                    toastr.options.timeOut = 5000;
                    toastr.error("Not saved, Try again");
                });
                $scope.confirmExit = 0;
            }  
        };

        $scope.deleteNode = function() {
            toastr.clear();
            if ($scope.currentNode !== null) {
                var index = $scope.dataArray.findIndex(function(x){
                    return (x[2] === $scope.dataArray[$scope.currentNode][2]);
                }); //search by email
                var index = $scope.dataArray.findIndex(function(x){
                    return (x[2] === $scope.dataArray[$scope.currentNode][2]);
                }); 
                $scope.dataArray[index][1] = null;

                drawChart();

                var index = $scope.completeArray.findIndex(function(x){
                    return (x.email === $scope.dataArray[$scope.currentNode][2]);
                });
                $scope.completeArray[index].parentName = null;
                $scope.completeArray[index].parentId = null;
                $scope.completeArray[index].path = null;
                $scope.completeArray[index].noImage = 0;
                $scope.model.unassignedArray.push($scope.completeArray[index]);
                
                $scope.model.selected = $scope.completeArray[index];
                $scope.nodeToAdd = $scope.completeArray[index];
                $scope.addFlag = 1;
                
                $scope.currentNode = null;
                $scope.showDelete = false;
                $scope.confirmExit = 1;
            } else {
                $scope.currentNode = null;
                drawChart();
                toastr.options.timeOut = 7000;
                toastr.options.positionClass = "toast-top-full-width";
                //toastr.info("To unassign a Person<br/>1.Select a non-manager from chart<br/>2.Click unassign.")
            }
        };

        $scope.add = function(res) {
            $scope.model.selected = res;
            $scope.nodeToAdd = res;
            $scope.addFlag = 1;
        };

        $scope.addNode = function() {
            toastr.clear();
            if ($scope.nodeToAdd !== null && $scope.currentNode !== null) {
                var i = $scope.model.unassignedArray.findIndex(function(x){
                    return (x.email === $scope.nodeToAdd.email);
                });
                $scope.model.unassignedArray.splice(i, 1);
                var i = $scope.dataArray.findIndex(function(x){
                    return x[2] === $scope.nodeToAdd.email;
                })
                $scope.dataArray[i][1] = $scope.dataArray[$scope.currentNode][0].v;

                var index1 = $scope.completeArray.findIndex(function(x){
                    return (x.email === $scope.dataArray[$scope.currentNode][2]);
                });
                var index2 = $scope.completeArray.findIndex(function(x){
                  return (x.email === $scope.nodeToAdd.email);  
                });

                $scope.completeArray[index2].parentName = $scope.dataArray[$scope.currentNode][0].v;
                $scope.completeArray[index2].parentId = $scope.completeArray[index1].id;
                var pathValidate = $scope.completeArray[index1].path ? $scope.completeArray[index1].path : ',';
                $scope.completeArray[index2].path = pathValidate + $scope.completeArray[index1].id + ',';

                drawChart();

                var row = $scope.dataArray.findIndex(function(x){
                    return x[2] === $scope.completeArray[index2].email;
                });
                updatePath(row);

                $scope.nodeToAdd = null;
                $scope.currentNode = null;
                $scope.showAdd = false;
                $scope.confirmExit = 1;
            } else {
                $scope.nodeToAdd = null;
                $scope.currentNode = null;
                drawChart();
                toastr.options.timeOut = 7000;
                toastr.options.positionClass = "toast-top-full-width";
                //toastr.info("To assign a Manager<br/>1.Select the person from the left nav<br/>2.Select the manager from chart<br/>3.Click assign.")
            }

            $scope.addFlag = 0;

        };

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'email');

            data.addRows($scope.dataArray);
            chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
            chart.draw(data, { allowHtml: true, size: 'medium', nodeClass: 'node', selectedNodeClass: 'nodeSelected' });
            google.visualization.events.addListener(chart, 'select', selectHandler);

            function selectHandler(e) {
                $scope.currentNode = chart.getSelection()[0].row;

                if (chart.getChildrenIndexes(chart.getSelection()[0].row).length === 0 && data.getValue(chart.getSelection()[0].row, 1) !== null && $scope.addFlag === 0) {
                    $scope.showDelete = true;
                    $scope.showAdd = false;
                } else {
                    $scope.showDelete = false;
                }
                if ($scope.addFlag === 1 && data.getValue(chart.getSelection()[0].row, 2) !== $scope.nodeToAdd.email) {
                    var path = 0;
                    var index1 = $scope.completeArray.findIndex(function(x){
                      return (x.email === data.getValue(chart.getSelection()[0].row, 2));  
                    });
                    if ($scope.completeArray[index1].path)
                        path = $scope.completeArray[index1].path.split(',');
                    $scope.showAdd = true;
                    for (var i = 0; i < path.length; i++) {
                        if (String($scope.nodeToAdd.id) === String(path[i]))
                            $scope.showAdd = false;
                    }

                } else if ($scope.addFlag === 0 || data.getValue(chart.getSelection()[0].row, 2) === $scope.nodeToAdd.email)
                    $scope.showAdd = false;

                $scope.$apply();
            }
        };

        function updatePath(row) {
            var children = chart.getChildrenIndexes(row);
            if (children.length >= 1) {
                for (var i = 0; i < children.length; i++) {
                    var parentEmailId = $scope.dataArray[row][2];
                    var childEmailId = $scope.dataArray[children[i]][2];
                    var parentIndex = $scope.completeArray.findIndex(function(x){
                        return x.email === parentEmailId;
                    });
                    var childIndex = $scope.completeArray.findIndex(function(x){
                      return x.email === childEmailId;  
                    });

                    $scope.completeArray[childIndex].path = $scope.completeArray[parentIndex].path + $scope.completeArray[parentIndex].id + ',';
                    updatePath(children[i])
                }
            } else
                return;
        };

        window.onbeforeunload = function(event) {
            if ($scope.confirmExit !== 0)
                return "Exit without saving?";
        };
    })

.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            console.log(element)
            scope.imageNotLoaded = function(obj) {
                obj.noImage = 1;
            };
            element.bind('load', function() {
                // console.log('image is loaded');
            });
            element.bind('error', function() {
                scope.$apply(attrs.imageonload);
            });
        }
    };
})
.directive('searchResults', function() {
    return {
        template: '<div class="search-box-wrapper clearfix" ng-show="showResults">' +
        '<div ng-repeat="item in searchList track by $index"> ' +
        '<div class="clearfix cursor" title="{{item.fullName}}, {{item.emailId}}" ng-click="addUser(item)">' +
        '<div class="pull-left relative">' +
        '<img ng-if="!item.noPicFlag" src="{{item.image}}" title="{{item.fullName}}, {{item.emailId}}" class="contact-image" imageonload="imageNotLoaded(item)">' +
        '<span ng-if="item.noPicFlag">' +
        '<span class="contact-no-image">{{item.nameNoImg}}</span></span></div>' +
        // '<div class="pull-left"><p class="margin0">{{item.name}}</p><p class="margin0">{{item.emailId}}</p></div>' +
        '<div class="pull-left"><p class="margin0">{{item.name}}</p></div>' +
        '</div></div></div>'
    };
});

