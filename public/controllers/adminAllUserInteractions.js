angular.module('allUserInteraction', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('interaction', function($scope, $resource, $http) {

        $scope.fromDate = moment().format("YYYY-MM-DD");
        $scope.toDate = moment().format("YYYY-MM-DD");
        $scope.sort = 'grandTotal';
        $scope.category = 'all';
        $scope.totalUser = 0;
        $scope.totalInteraction = 0;
        $scope.revers = true;

        $http.get('/admin/getListOfCompanies').then(function(response) {
            $scope.listOfCompanies = response.data;
            console.log(response);
        });

        $scope.getInteractions = function() {
            var fdate = new Date($scope.fromDate);
            var tdate = new Date($scope.toDate);
            $http.get('/admin/getAllUserInteractions', { params: { from: fdate, to: tdate, category: $scope.category } }).then(function(response) {
                
                $scope.interaction = response.data.array;
                $scope.totalUser = 0;
                $scope.totalInteraction = 0;
                $scope.totalUser = response.data.users;
                $scope.interaction.forEach(function(ele) {
                    
                    $scope.totalInteraction += ele.grandTotal;
                    if(ele.serviceLogin == 'outlook'){
                        ele.googleOrOutlook = 'Outlook'
                    } else {
                        ele.googleOrOutlook = 'Google';
                    }
                });
            });

        }
        $scope.toExcel = function() {
            $scope.path = $scope.fromDate + '/' + $scope.toDate + '/' + $scope.category;
        }
        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }
    });
