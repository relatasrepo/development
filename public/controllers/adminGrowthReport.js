angular.module('admin', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('growthReport', function($scope, $resource, $http) {

        //$scope.fromDate = moment().format("YYYY-MM-DD");
        //$scope.toDate = moment().format("MMM YYYY");
        $scope.growthList = [];
        $scope.category = 'business';
        $scope.partialDate = moment().format("YYYY-MM");
        $scope.toDate = moment().format("MMMM YYYY");
        var month;
        var year
        $('.date-picker').val(moment(new Date()).format("MMMM YYYY"));
        $('.date-picker').datepicker( {
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',
            setDate:new Date(),
            prevText: "Earlier",
            yearRange: '2010:' + new Date().getFullYear(),
            beforeShow: function(el, dp) {
                $('#ui-datepicker-div').addClass('hide-calendar');
                //$("#ui-datepicker-div .ui-datepicker-month").val(8);
                //console.log($("#ui-datepicker-div selector").val())
                //$("#ui-datepicker-div .ui-datepicker-year").val(year);
            },

            onClose: function(dateText, inst) {
                month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
                month++;
                $scope.toDate = moment(year+'-'+month).format("MMMM YYYY");
                //$scope.toDate = moment(year+'-'+month).format("MMM YYYY");
                month = month < 10 ? '0'+month : month;
                $scope.partialDate = year+'-'+month
                //$scope.toFullDate = new Date(moment($scope.toDate).endOf('month'));
            }
        });

        $scope.getGrowthData = function() {
            //$scope.toFullDate = new Date(moment($scope.toDate).endOf('month'));
            $http.get('/admin/growth/report/info', { params: { endDate: $scope.partialDate, filter: $scope.category } }).then(function(response) {
                if(response.data.SuccessCode){
                    //$scope.growthList = [];
                    var data = response.data.Data;
                    data.category = $scope.category == 'individuals' ? 'retail' : $scope.category;
                    data.date = $scope.toDate;
                    $scope.growthList.push(response.data.Data)
                }
            });
        }
        $scope.toExcel = function() {
            $scope.path = '?category='+$scope.category+'&from='+$scope.fromDate+'&to='+$scope.toDate;
        }

        $scope.order = function(sort){
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }
    })
    .filter('byEmailCompanyName', function() {
        return function(names, search) {
            if(angular.isDefined(search)) {
                var results = [];
                var i;
                var searchVal = search.toLowerCase();
                for(i = 0; i < names.length; i++){
                    var firstName = names[i].firstName.toLowerCase();
                    if(names[i].lastName !== undefined && names[i].lastName !== null)
                        var lastName = names[i].lastName.toLowerCase();
                    else
                        var lastName = '';
                    if(names[i].companyName !== undefined && names[i].companyName !== null)
                        var company = names[i].companyName.toLowerCase();
                    else
                        var company = '';
                    if(names[i].emailId !== undefined && names[i].emailId !== null)
                        var email = names[i].emailId.toLowerCase();
                    else
                        var email = '';
                    if(firstName.indexOf(searchVal) >=0 || lastName.indexOf(searchVal) >=0  || company.indexOf(searchVal) >=0 || email.indexOf(searchVal) >=0){
                        results.push(names[i]);
                    }
                }
                return results;
            } else {
                return names;
            }
        };
    });
