angular.module('redundantContacts', ['ngResource', 'angular-loading-bar'])
    .controller('contacts', function($scope, $resource,$http) {
        // var data = $resource('/admin/getRedundantContacts');
        // $scope.array = data.query();
        var deleteArray = [];
        var temp = [];
        $scope.count = 0;

        $http.get('/admin/getRedundantContacts', { params: {  } }).then(function(response) {
                $scope.array = response.data.contacts;
                $scope.count = response.data.count;
            });
        
        $scope.deleteRow = function(){
            deleteArray = [];
            temp = [];
            for(var i=0;i < $scope.selection.length ;i++){
                var index = $scope.array.findIndex(x => x.id === $scope.selection[i])
                temp.push($scope.array[index]);
            }

         
            for(var i=0;i<temp.length;i++){
                var indexOfId = deleteArray.findIndex(x => x.email === temp[i].userEmail)
                if(indexOfId > -1){
                    deleteArray[indexOfId].id.push(temp[i].id)
                }
                else{
                    deleteArray.push({email:temp[i].userEmail,id:[temp[i].id]});
                }
            }

            $http.post('/admin/user/deleteRedundantContacts', deleteArray)
            .success(function (data, status, headers, config) {
                if(data.status === 'success') {
                    // for(var i=0;i < $scope.selection.length ;i++){
                    //     $scope.array.splice(index,1);
                    // }
                    // $scope.count -= $scope.selection.length;
                    // $scope.selection = [];
                    // $scope.all = -1;

                    $http.get('/admin/getRedundantContacts', { params: {  } }).then(function(response) {
                        $scope.array = response.data.contacts;
                        $scope.count = response.data.count;
                    });
                }
            })

            
            // for(var i=0;i<deleteArray.length;i++)
            //     console.log(deleteArray[i]);
        }

        $scope.all = -1;
        $scope.selection = [];
        $scope.toggleSelection = function toggleSelection(id) {
            var idx = $scope.selection.indexOf(id);
            if (idx > -1) {
              $scope.selection.splice(idx, 1);
              $scope.all = -1;
            }
            else {
              $scope.selection.push(id);
            }
        };

        $scope.toggleSelectionAll = function(){
            if($scope.all == -1){
                $scope.all = 1;
                for(var i = 0;i < $scope.array.length - 1 ;i++){
                    $scope.selection.push($scope.array[i].id);
                }
            }
            else{
                $scope.all = -1;
                $scope.selection=[];
            }
        };
    });