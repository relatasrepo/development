angular.module('redundantContacts', ['ngResource', 'angular-loading-bar'])
    .controller('contacts', function($scope, $resource,$http) {

        getVideos($scope,$http)
        
        $scope.save = function () {

            if($scope.title && $scope.description && $scope.link){

                var obj = {
                    title:$scope.title,
                    description:$scope.description,
                    url:'https://youtube.com/embed/'+$scope.link+'?autoplay=0&controls=0&showinfo=0&autohide=1',
                    originalUrl:$scope.link
                }
                
                $http.post("/admin/sales-ai/add/urls",obj)
                    .success(function (response) {
                        if(response){
                            getVideos($scope,$http)
                        } else {
                         alert("Something went wrong")
                        }

                    });
            } else {
                alert("All the fields are required")
            }

        }
        
        $scope.openOptions = function (video) {
            for(var i=0;i<$scope.videos.length;i++){
                $scope.videos[i].open = false;
            }
            video.open = true
        }

        $scope.makeDefault = function (link) {
            $http.post("/admin/sales-ai/set/default",{link:link})
                .success(function (response) {
                    if(response){
                        getVideos($scope,$http)
                    } else {
                        alert("Something went wrong")
                    }
                })
        }

        $scope.setOrder = function (link,order) {
            
            $http.post("/admin/sales-ai/set/setOrder",{link:link,order:order})
                .success(function (response) {
                    if(response){
                        getVideos($scope,$http)
                    } else {
                        alert("Something went wrong")
                    }
                })
        }
        //
        $scope.delete = function (link) {
            $http.post("/admin/sales-ai/delete/video",{link:link})
                .success(function (response) {
                    if(response){
                        getVideos($scope,$http)
                    } else {
                        alert("Something went wrong")
                    }
                })
        }
    });


function getVideos($scope,$http) {

    $http.get('/admin/sales-ai/get/urls').success(function(response) {

        $scope.videos = response.data.urls;
        $scope.videos = $scope.videos.sort(function (a, b) {  return a.order - b.order;  });
        // var list = '';
        // var items = '';
        // var defaultVideo = null;
        // for(var i=0;i<$scope.videos.length;i++) {
        //     $scope.videos[i].order = i
        // }

    });
}