/**
 * Created by elavarasan on 23/10/15.
 */
var reletasApprouter = angular.module('reletasApp', ['ngRoute','ui.bootstrap']).config(['$interpolateProvider','$httpProvider',function ($interpolateProvider, $httpProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    //- See more at: http://www.oodlestechnologies.com/blogs/AngularJS-caching-issue-for-Internet-Explorer#sthash.N5ABbZcz.dpuf
}]);

reletasApprouter.controller("header_controller", function($scope){
    $scope.getMiddleBarTemplate = function(){
        return ""
    }
    $scope.yourNetworkIn = true
    $scope.extendedNetworkIn = true
    $scope.forCompaniesIn = false

    $scope.searchFromHeader = function(searchContent,yourNetwork,extendedNetwork,forCompanies){

        searchContent = searchContent.replace(/[^\w\s]/gi, '');

        if(typeof searchContent == 'string' && searchContent.length > 0){
            yourNetwork = yourNetwork ? true : false;
            extendedNetwork = extendedNetwork ? true : false;
            forCompanies = forCompanies ? true : false;
            window.location = '/contact/connect?searchContent='+searchContent+'&yourNetwork='+yourNetwork+'&extendedNetwork='+extendedNetwork+'&forCompanies='+forCompanies;
        }
        else toastr.error("Please enter search content")
    };
});

reletasApprouter.directive('ngEnter', function () {
    return function (scope, element, attrs) {

        element.bind("keydown keypress", function (event) {

            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

var timezone;
reletasApprouter.controller("logedinUser", function ($scope, $http,$rootScope) {
    var uName = window.location.pathname;
    uName = uName.replace("/", "");
    var url = '/'+uName+'/profile/web';

    $http.get(url)
        .success(function (response) {
            //response.Data.isCalendarLocked = false;
            setTimeout(function(){
                $(".seeMoreIcon").addClass("disabled-see-more")
            },1000)

            calendar_locked(response.Data.isCalendarLocked);

            if(response.Data.self == true){
                hideElements(false,false,true);
                $scope.l_usr = response.Data.logged_in_user;
            }
            else{
                if(response.Data.logged_in_user.Error){
                    hideElements('login',false,false)
                    $(".login_header").hide();
                }
                else{
                    hideElements('login',true,false);
                    $(".login_header").show();
                    identifyMixPanelUser(response.Data.logged_in_user);
                    $scope.l_usr = response.Data.logged_in_user;
                }
            }
        }).error(function (data) {

        });
    $http.get('/profile/get/current/web')
        .success(function (response) {

            if(response.SuccessCode){
                $rootScope.isCorporateAdmin = response.Data.corporateAdmin;
                $scope.l_usr = response.Data;
                identifyMixPanelUser(response.Data);
                if(checkRequired(response.Data.timezone) && checkRequired(response.Data.timezone.name)){
                    timezone = response.Data.timezone.name;
                }
            }
            else{

            }
        }).error(function (data) {

        })
});
reletasApprouter.controller('calendarController',function($scope,$http,$filter,$timeout){
    var getTime=function(str,format){
        var now = new Date();
        if (format == "h:m") {
            now.setHours(str.substr(0,str.indexOf(":")));
            now.setMinutes(str.substr(str.indexOf(":")+1));
            now.setSeconds(0);
            return now;
        }else
            return "Invalid Format";
    };
    $http.get('/today/weather/forecast/web')
        .success(function (response) {
            function getDate(dt){
                if(dt){
                    $scope.date_today_top = checkRequired(timezone) ? moment.unix(dt).tz(timezone).format('DD MMM YYYY') : moment.unix(dt).format('DD MMM YYYY')
                }
                else $scope.date_today_top = checkRequired(timezone) ? moment.unix().tz(timezone).format('DD MMM YYYY') : moment.unix().format('DD MMM YYYY')
            }
            if(response.SuccessCode){
                if(checkRequired(response.Data.city)){
                    $scope.city_country_top = response.Data.city.name+', '+response.Data.city.country;
                }
                if(response.Data.list != undefined && response.Data.list.length > 0){
                    getDate(response.Data.list[0].dt);

                    $scope.today_temp_min = response.Data.list[0].temp.min+'C';
                    $scope.today_temp_max = response.Data.list[0].temp.max+'C';
                }
            }
        }).error(function (data) {

        });

    var funcGetData=function(){
        $http.get('/api/v2/workingHours').success(function(workingHours){
            $scope.workHoursWeekDay=workingHours.workHoursWeek.weekdayHours;
            $scope.workHoursWeekEnd=workingHours.workHoursWeek.weekendHours;
            if($scope.workHoursWeekDay.start===null){
                $scope.workHoursWeekDay.start='00';
            }
            if($scope.workHoursWeekEnd.start===null){
                $scope.workHoursWeekEnd.start='00';
            }
            if($scope.workHoursWeekDay.end===null){
                $scope.workHoursWeekDay.end='23';
            }
            if($scope.workHoursWeekEnd.end===null){
                $scope.workHoursWeekEnd.end='23';
            }
            var d=getTime('00:00','h:m');
            $scope.slots=[];
            $scope.calendarMatrix=[];
            for(var j=0;j<49;j++){
                $scope.slots.push({'timeText':$filter('date')(d,'hh:mm a'),'value':$filter('date')(d,'HH:mm')});
                $scope.calendarMatrix.push({'value':$filter('date')(d,'HH:mm'),'dates':[]});
                d= new Date(d.getTime()+30*60000);
            }
            $http.get('/api/v2/calendar/'+$scope.datesSE.sd.getTime()).success(function(data){
                $scope.dates=[];
                $scope.dataList=[];
                var people=[];
                $scope.calendarList=data.lists;
                $scope.backgroundColor={tasks:'#cb3c19'};
                for(var q=0;q<$scope.calendarList.length;q++){
                    $scope.calendarList[q].showCalendar=true;
                    $scope.backgroundColor[$scope.calendarList[q]._id]=$scope.calendarList[q].color;
                }
                $scope.calendarDetails={people:0,meetings:0,tasks:0,workHours:0};
                for(var i=0; i<7; i++) {
                    var ds=$scope.datesSE.sd.getDate(),ms=$scope.datesSE.sd.getMonth()+ 1,ys=$scope.datesSE.sd.getFullYear();
                    var tmp=new Date(ys+'-'+ms+'-'+ds);
                    tmp.setDate($scope.datesSE.sd.getDate() + i);
                    $scope.dates.push(tmp);
                    for(var j=0;j<$scope.slots.length;j++){
                        $scope.calendarMatrix[j].dates.push({'value':tmp.getTime(),'showSlotPopUp':false,'showEventPopUp':false,'showPopUp':true});
                    }
                }
                for(var k=0;k<$scope.dates.length;k++){
                    for(var n=0;n<data.events.length;n++){
                        var d=$scope.dates[k].getDate();
                        var m=$scope.dates[k].getMonth();
                        var y=$scope.dates[k].getFullYear();
                        var dc=new Date(data.events[n].scheduleTimeSlots[0].start.date).getDate();
                        var mc=new Date(data.events[n].scheduleTimeSlots[0].start.date).getMonth();
                        var yc=new Date(data.events[n].scheduleTimeSlots[0].start.date).getFullYear();
                        if(d==dc && m==mc  && y==yc){
                            var eDate=new Date(data.events[n].scheduleTimeSlots[0].end.date),
                                sDate=new Date(data.events[n].scheduleTimeSlots[0].start.date);
                            var diffMin=((eDate-sDate)/1000)/60;
                            $scope.calendarDetails.workHours+=parseFloat(diffMin/60);
                            $scope.calendarDetails.meetings+=1;
                            if(data.events[n].participants.length>1){
                                for(var a=0;a<data.events[n].participants.length;a++){
                                    if(people.indexOf(data.events[n].participants[a].emailId)==-1){
                                        people.push(data.events[n].participants[a].emailId);
                                    }
                                }
                            }
                            $scope.dataList.push({
                                'list':data.events[n]._list,
                                'popoverOpen':false,
                                'showCalendar':true,
                                'events':data.events[n]
                            })
                        }
                    }
                }
                $scope.eventPopover={
                    templateUrl:'eventPopoverTemplate.html',
                    templateUrlSlot:'slotPopoverTemplate.html',
                    close:function(d){
                        for(var a=0;a< d.length;a++){
                            for(var b=0;b<d[a].dates.length;b++){
                                d[a].dates[b].showEventPopUp=false;
                                d[a].dates[b].showSlotPopUp=false;
                            }
                        }
                    },
                    closeSlot:function(matrix,a,b){
                        $scope.eventPopover.close(matrix);
                        matrix[a].dates[b].showPopUp=false;
                    },
                    sendData:function(data){
                        $http.put('/api/v2/calendar/events/'+data.events._id,data.events).then(function(){
                            console.log('success');
                        },function(){
                            console.log('failure');
                        })
                    },
                    popoverPlacement:function(index){
                        if(index<3){
                            return 'right';
                        }else{return 'left'}
                    }
                };
                $scope.findWorkingCell=function(time,date){
                    var result=false,
                        weekDayObj=$scope.workHoursWeekDay,
                        weekEndObj=$scope.workHoursWeekEnd;
                    var d=date.getDay();
                    var t=time.split(':');
                    var h=parseInt(t[0]),
                        hswd=parseInt(weekDayObj.start),hewd=parseInt(weekDayObj.end),
                        hswe=parseInt(weekEndObj.start),hewe=parseInt(weekEndObj.end);
                    if(weekDayObj.days.indexOf(d) != -1){
                        if(h>=hswd && h<hewd){
                            result=true;
                        }
                    }else if(weekEndObj.days.indexOf(d) != -1){
                        if(h>=hswe && h<hewe){
                            result=true;
                        }
                    }else{
                        result=true;
                    }
                    return result;
                };
                $scope.event={title:'',locationType:'',location:'',description:'',popoverOpen:false};
                $scope.calendarDetails.people=people.length;
                $scope.findCorrectCell=function(time,date,event){
                    var ret='';
                    if(event===null || event === undefined){
                        ret=false;
                    }else{
                        var d=date.getDate(),y=date.getFullYear(),m=date.getMonth()+1;
                        var x1=new Date(event.scheduleTimeSlots[0].start.date),
                            x2=new Date(event.scheduleTimeSlots[0].end.date);
                        var xd= x1.getDate(),xy= x1.getFullYear(),xm= x1.getMonth()+ 1,xh= x1.getHours(),xmin= x1.getMinutes();
                        var x2d= x2.getDate(),x2y= x2.getFullYear(),x2m= x2.getMonth()+ 1,x2h= x2.getHours(),x2min= x2.getMinutes();
                        var d1=new Date(y+'-'+m+'-'+d+' '+time).getTime();
                        var d2=new Date(xy+'-'+xm+'-'+xd+' '+xh+':'+xmin).getTime();
                        var d3=new Date(x2y+'-'+x2m+'-'+x2d+' '+x2h+':'+x2min).getTime();
                        /*check start time*/
                        ret=eval(d1==d2 || d1>d2&&d1<d3);
                    }
                    return ret;
                };
            });
        });
    };

    $scope.chkTasks=false;
    $scope.toggleTasks=function(){
        $scope.chkTasks=!$scope.chkTasks;
        if($scope.chkTasks){
            $scope.backgroundColor.tasks='#cb3c19';
        }else{
            $scope.backgroundColor.tasks='white';
        }
    };
    $scope.decideWhichPopover=function(a,b){
        for(var i=0;i<$scope.calendarMatrix.length;i++){
            for(var j=0;j<$scope.calendarMatrix[i].dates.length;j++){
                $scope.calendarMatrix[i].dates[j].showSlotPopUp=false;
                $scope.calendarMatrix[i].dates[j].showEventPopUp=false;
            }
        }
        $scope.calendarMatrix[a].dates[b].showEventPopUp=true;
    };
    $scope.toggleCalendar=function(list){
        list.showCalendar=!list.showCalendar;
        for(var a=0;a<$scope.dataList.length;a++){
            if($scope.dataList[a].list==list._id){
                $scope.dataList[a].showCalendar=!$scope.dataList[a].showCalendar;
            }
        }
    };
    $scope.changeCalendarColor=function(list,color){
        $scope.backgroundColor[list._id]=color;
        var data={color:color};
        $http.put('/api/v2/calendar/'+list._id,data).then(
            function(data){

            })
    };
    $scope.datesSE={sd:new Date(),ed:new Date()};
    $scope.counter=1;
    $scope.datesSE.ed.setDate($scope.datesSE.ed.getDate()+7);
    $scope.incWeek=function(start,end){
        $scope.datesSE.sd.setDate(start.getDate()+7);
        $scope.datesSE.ed.setDate(end.getDate()+7);
        funcGetData();
    };
    $scope.decWeek=function(start,end){
        $scope.datesSE.sd.setDate(start.getDate()-7);
        $scope.datesSE.ed.setDate(end.getDate()-7);
        funcGetData();
    };
    $scope.incDay=function(counter,start){
        if(counter==7){
            $scope.datesSE.sd.setDate(start.getDate()+1);
            counter=1;
            funcGetData();
        }else{
            ++counter;
            $scope.datesSE.sd.setDate(start.getDate()+1);
        }
    };
    $scope.decDay=function(counter,start){
        if(counter==7){
            $scope.datesSE.sd.setDate(start.getDate()-1);
            counter=1;
            funcGetData();
        }else{
            ++counter;
            $scope.datesSE.sd.setDate(start.getDate()-1);
        }
    };
    $scope.locationTypes=[{name:'Phone'},{name:'Skype'},{name:'Conference Call'},{name:'In-Person'}];
    funcGetData();
});
reletasApprouter.directive('numbersOnly', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {
            modelCtrl.$parsers.push(function (inputValue) {
                // this next if is necessary for when using ng-required on your input.
                // In such cases, when a letter is typed first, this parser will be called
                // again, and the 2nd time, the value will be undefined
                if (inputValue == undefined) return ''
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }
                return transformedInput;
            });
        }
    };
});

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}
