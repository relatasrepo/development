angular.module('adminUserList', ['ngResource', 'angular-loading-bar'])
    .controller('header_controller', function($scope, $http) {
        $scope.obj = { noImage: 0 };
        $http({
            method: 'GET',
            url: '/profile/get/edit'
        }).then(function successCallback(response) {
            $scope.profileName = response.data.Data.firstName;
            $scope.profileImage = response.data.Data.profilePicUrl;
        });
    })
    .controller('userList', function($scope, $resource, $http) {

        $scope.fromDate = new Date();
        $scope.toDate = new Date();
        $scope.userList = [];
        $scope.total = 0;

        $scope.getInteractions = function() {
            $http.get('/admin/user/insights/action/log/info', { params: { from: $scope.fromDate, to: $scope.toDate, infoFor: 'table' } })
                .then(function(response) {
                    if (response.data.SuccessCode) {
                        $scope.userList = response.data.Data;
                        $scope.total = response.data.Data.length;
                    } else{
                        $scope.total = 0;
                        $scope.userList = [];
                    }
                });
        }
        $scope.toExcel = function() {
            $scope.path = '?from=' + $scope.fromDate + '&to=' + $scope.toDate + '&infoFor=csv';
        }

        $scope.order = function(sort) {
            $scope.revers = ($scope.sort === sort) ? !$scope.revers : false;
            $scope.sort = sort;
        }
        var dateTrimer = function(array) {
            array.forEach(function(ele) {
                if (ele.createdDate !== undefined && ele.createdDate !== null) {
                    ele.createdDate = ele.createdDate.split('T')[0];;
                }
                if (ele.lastLoginDate !== undefined && ele.lastLoginDate !== null)
                    ele.lastLoginDate = ele.lastLoginDate.split('T')[0];
                if (ele.registeredDate !== undefined && ele.registeredDate !== null)
                    ele.registeredDate = ele.registeredDate.split('T')[0];

                if (ele.registeredUser === true && (ele.registeredDate !== undefined || ele.registeredDate !== null))
                    ele.registeredDate = ele.createdDate;

                if (ele.corporateUser !== false && ele.corporateUser !== undefined && ele.corporateUser !== null)
                    ele.corporateUser = 'C';
                else
                    ele.corporateUser = 'R';
            });
        }

    })
    .filter('byEmailCompanyName', function() {
        return function(names, search) {
            if (angular.isDefined(search)) {
                var results = [];
                var i;
                var searchVal = search.toLowerCase();
                for (i = 0; i < names.length; i++) {
                    var firstName = names[i].firstName.toLowerCase();
                    if (names[i].lastName !== undefined && names[i].lastName !== null)
                        var lastName = names[i].lastName.toLowerCase();
                    else
                        var lastName = '';
                    if (names[i].contactEmail !== undefined && names[i].contactEmail !== null)
                        var contactEmail = names[i].contactEmail.toLowerCase();
                    else
                        var contactEmail = '';
                    if (names[i].userEmailId !== undefined && names[i].userEmailId !== null)
                        var email = names[i].userEmailId.toLowerCase();
                    else
                        var email = '';
                    if (names[i].contactName !== undefined && names[i].contactName !== null)
                        var contactName = names[i].contactName.toLowerCase();
                    else
                        var contactName = '';
                    if (firstName.indexOf(searchVal) >= 0 || lastName.indexOf(searchVal) >= 0 || contactEmail.indexOf(searchVal) >= 0 || email.indexOf(searchVal) >= 0 || contactName.indexOf(searchVal) >= 0) {
                        results.push(names[i]);
                    }
                }
                return results;
            } else {
                return names;
            }
        };
    });
