var mongoClient = require('mongodb').MongoClient;
var json2csv = require('json2csv');
var fs = require('fs');
customObjectId = require('mongodb').ObjectID;

var script = function() {

    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.21.21:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var user = db.collection("user");
            var interactions = db.collection("interactionsV2");

            var runForSingleUser = function (skip) {

                // user.find({corporateUser:true},{_id:1,emailId:1}).toArray(function (err,users) {
                    user.find({corporateUser:true,emailId:/22by7/},{_id:1,emailId:1}).toArray(function (err,users) {
                    // user.find({emailId:"sudip@relatas.com"},{contacts:1,emailId:1}).toArray(function (err,users) {

                    var query = { "$or": users.map(function(el) {
                            var obj = {};

                            obj["ownerId"] = el._id;
                            obj["emailId"] = /abb/i;
                            // obj["emailId"] = /gmail/i;
                            // obj["userId"] = {$ne:el._id};
                            obj["interactionType"] = "email";
                            return obj;
                        })};

                    var query2 = { "$or": users.map(function(el) {
                            var obj = {};

                            obj["userId"] = el._id.toString();
                            obj["ownerId"] = el._id;
                            return obj;
                        })};

                    interactions.aggregate([
                        {
                            $match:query
                        },
                        {
                            $group:{
                                _id:{
                                    owner:"$ownerEmailId",
                                    refId:"$refId"
                                },
                                count:{$sum:1},
                                emailIds:{
                                    $push:"$emailId"
                                }
                            }
                        },
                        {
                            $project:{
                                _id:"$_id.owner",
                                ownerEmailId:"$_id.owner",
                                refId:"$_id.refId",
                                count:"$count",
                                emailIds:1
                            }
                        },
                        {
                            $match:{
                                count:{
                                    "$gt":1
                                }
                            }
                        },
                        // {
                        //   $group:{
                        //     _id:"$_id",
                        //     count:{$sum:"$count"}
                        //
                        //   }
                        // }
                    ],{allowDiskUse: true}).toArray(function (err,interactionCounts) {

                        console.log(err);

                        var fields = ["ownerEmailId","refId","emailIds","count"]

                        var result = json2csv({ data: interactionCounts, fields: fields });

                        fs.writeFile('file.csv', result, function(err) {
                            if (err) throw err;
                            console.log('file saved');
                        });

                        // _.each(users,function (user) {
                        //   _.each(interactionCounts,function (interactions) {
                        //     if(user.emailId == interactions[0]._id){
                        //       console.log(interactionCounts.length)
                        //     }
                        //   })
                        // })
                    })
                })
            };

            runForSingleUser(0);
        }
    });
}

script();
