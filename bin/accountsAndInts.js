var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async')

var interactionsCollection;
var opportunitiesCollection;
var accInteractionCollection;
var usersCollection;
var shCollection;
var companyCollection;
var dealsAtRiskMetaCollection;
var OppMetaDataCollection;
var dashboardInsightsCollection;
var opportunitiesTargetCollection;
var accountInsightsCollection;
var newCompaniesInteraction;
var dashboardInsightsVerbose;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Dashboard Insights is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://10.150.0.11/Relatas-2019-06-26-227?poolSize=100", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    //  mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
   // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {

            var db = client.db('Relatas');
            // var db = client.db('Relatas-2019-06-26-227');
            // var db = client.db('Relatas_Prod_01Aug');
            // console.log("Connected to db");
            usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            interactionsCollection = db.collection('interactionsV2');
            accInteractionCollection = db.collection('accInteraction');
            newCompaniesInteraction = db.collection('newCompaniesInteraction');
            shCollection = db.collection('secondaryHierarchy');
            companyCollection = db.collection('company');
            dealsAtRiskMetaCollection = db.collection('dealsAtRiskMeta');
            OppMetaDataCollection = db.collection('oppMetaData');
            opportunitiesTargetCollection = db.collection('opportunitiesTarget');
            dashboardInsightsCollection = db.collection('dashboardInsight');
            accountInsightsCollection = db.collection('accountInsight');
            dashboardInsightsVerbose = db.collection('dashboardInsightsVerbose');

            var runForSingleCompany = function (skip) {
                companyCollection.find({url:/22by7/}).skip(skip).limit(1).toArray(function (err, company) {

                    if(!err && company && company.length>0){
                        console.log(company[0].url)
                        var companyId = company[0]._id;
                        getUserProfiles(companyId,function (err1,users) {
                            if(users[0]){
                                userFiscalYear(company[0],users[0],function (fyMonth,fyRange,allQuarters) {

                                    //From here del
                                    fyRange = {
                                        start:new Date(moment({'year': 2018, 'month': 3}).startOf("month")),
                                        end:new Date(moment({'year': 2019, 'month': 2}).endOf("month"))
                                    }

                                    // allQuarters.currentQuarter.start = fyRange.start;
                                    // allQuarters.currentQuarter.end = fyRange.end;
                                    allQuarters["currentQuarter"] = "quarter2"; //TODO Remove this after qtr1 data
                                    allQuarters[allQuarters.currentQuarter].start = new Date(moment({'year': 2018, 'month': 6}).startOf("month"))
                                    allQuarters[allQuarters.currentQuarter].end = new Date(moment({'year': 2018, 'month': 8}).endOf("month"));

                                    //Till here del
                                    console.log(users.length)

                                    var dayString = moment().format("DDMMYYYY");

                                    if(allQuarters.currentQuarter === "quarter1") {
                                        dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
                                    }

                                    accountInsightsCollection.remove({companyId:companyId,dayString:dayString},function () {
                                        if(!err1 && users && users.length){
                                            async.eachSeries(users, function (user, next){
                                                insightsForUser(user,company[0],fyMonth,fyRange,allQuarters,function(){
                                                    console.log("Done ",user.emailId);
                                                    next();
                                                });

                                            }, function(err) {
                                                console.log('All user done for company',company[0].url);
                                                updateTeamInsightsAllUsers(users,company[0],fyMonth,fyRange,allQuarters,function () {
                                                    runForSingleCompany(skip+1);
                                                    console.log('All team done for company',company[0].url)
                                                })
                                            });
                                        } else {
                                            runForSingleCompany(skip+1);
                                        }
                                    });
                                })
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---");
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

startScript();

function getTargets(userId,qStart,qEnd,fyStart,fyEnd,callback){

    var aggregation = [
        {
            $match:{
                userId:{$in:userId},
                "fy.start":{$gte:new Date(fyStart)},
                "fy.end":{$lte:new Date(fyEnd)}
            }
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$lte:new Date(qEnd),$gte:new Date(qStart)}
            }
        },
        {
            $project: {
                month: { "$month": "$salesTarget.date" },
                year: { "$year": "$salesTarget.date" },
                target: "$salesTarget.target",
                date:"$salesTarget.date"
            }
        },
        {
            $group:{
                _id:{month:"$month",year:"$year"},
                date:{ $max: "$date" },
                month:{ $max: "$month" },
                year:{ $max: "$year" },
                target:{$sum:"$target"}

            }
        }
    ];

    opportunitiesTargetCollection.aggregate(aggregation).toArray(function (err,targets) {

        console.log(fyStart,fyEnd);
        console.log(qEnd,qStart);
        console.log(targets);

    // opportunitiesTargetCollection.aggregate(aggregation,function (err,targets) {

        if(err){
            callback(err,[])
        }
        else{
            callback(err,targets);
        }
    })
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

function insightsForUser(user,company,fyMonth,fyRange,allQuarters,callback) {

    var primaryCurrency = "USD",
        currenciesObj = {}

        company.currency.forEach(function (el) {
        currenciesObj[el.symbol] = el;
        if(el.isPrimary){
            primaryCurrency = el.symbol;
        }
    });

    console.log(JSON.stringify(allQuarters,null,1));

    var qStart = allQuarters[allQuarters.currentQuarter].start;
    var qEnd = allQuarters[allQuarters.currentQuarter].end;

    async.parallel([
        function (callback) {
            interactionsByAccount(user.companyId,[user.emailId],qStart,qEnd,allQuarters,callback)
        },
        function (callback) {
            getOppsForUser([user.emailId],user.companyId,primaryCurrency,currenciesObj,qStart,qEnd,true,allQuarters,fyRange.end,callback)
        },
        function (callback) {
            dealsAtRisk([user.emailId],primaryCurrency,currenciesObj,qStart,qEnd,callback)
        },
        function (callback) {
            getTargets([user._id],qStart,qEnd,fyRange.start,fyRange.end,callback)
        }
    ],function (err,data) {
        if(err){
            console.log("---insightsForUser---");
            console.log(err);
        }

        insightsBuildAndUpdate(data,company,primaryCurrency,currenciesObj,fyRange,user,allQuarters,false,[user.emailId],function () {
            callback();
        });
    })
}

function insightsBuildAndUpdate(data,company,primaryCurrency,currenciesObj,fyRange,user,allQuarters,forTeam,allEmailIds,callback) {

    var qStart = allQuarters[allQuarters.currentQuarter].start;
    var qEnd = allQuarters[allQuarters.currentQuarter].end;

    var thisFyOpps = [],
        thisQuarterOpps = [],
        thisQuarterOpenOpps = {
            opportunities:[],
            interactions:[]
        },
        thisQuarterOppsObj = {};

    var oppsInteractions = {
            opportunities:[],
            interactions:[]
        },
        wonAmt = 0,
        wonCount = 0,
        lostCount = 0,
        pipelineCount = 0,
        renewalCount = 0,
        staleCount = 0,
        staleAmt = 0,
        lostAmt = 0,
        pipelineAmt = 0,
        renewalAmt = 0,
        wonReasons = [],
        accounts = [],
        products = [],
        productsAll = [],
        locations = [],
        lostReasons = [],
        sourceTypes = [],
        oppTypes = [],
        renewalTypes = []

    var dataCreated_cr = []
        ,dataClosed_cr = [];

    _.each(company.typeList,function (tl) {
        if(tl.isTypeRenewal){
            renewalTypes.push(tl.name)
        }
    });

    var raw_opps = [];

    interactionsForOpps(data && data[1] && data[1].raw_opps?data[1].raw_opps:[],forTeam,function (intGroupByContactObj) {

        console.log("Opps length", data[1].raw_opps.length);

        if(data && data[1] && data[1].raw_opps && data[1].raw_opps.length>0){
            raw_opps = data[1].raw_opps;
            var oppIdIntsObj = {};
            _.each(data[1].raw_opps,function (op) {

                op.createdDate = op.createdDate?op.createdDate:op._id.getTimestamp();

                if(!forTeam){

                    var totalInts = 0;
                    var allContactsInOpp = [op.contactEmailId];

                    if(op.decisionMakers && op.decisionMakers.length>0){
                        allContactsInOpp = allContactsInOpp.concat(op.decisionMakers,"emailId")
                    }

                    if(op.influencers && op.influencers.length>0){
                        allContactsInOpp = allContactsInOpp.concat(op.influencers,"emailId")
                    }

                    if(op.partners && op.partners.length>0){
                        allContactsInOpp = allContactsInOpp.concat(op.partners,"emailId")
                    }

                    allContactsInOpp = _.uniq(allContactsInOpp);

                    _.each(allContactsInOpp,function (co) {
                        if(intGroupByContactObj){
                            if(intGroupByContactObj[co] && intGroupByContactObj[co].count){
                                _.each(intGroupByContactObj[co].interactions,function (el) {
                                    if((new Date(el.interactionDate)>= new Date(op.createdDate)) && new Date(el.interactionDate)<= new Date(op.closeDate)){
                                        totalInts++
                                    }
                                })
                            }
                        }
                    });

                    oppsInteractions.opportunities.push({
                        axis: op.opportunityName,
                        value: op.convertedAmtWithNgm,
                        amountWithNgm: op.convertedAmtWithNgm,
                        name:op.opportunityName,
                        stageName:op.stageName,
                        amount: op.convertedAmt,
                        contactEmailId: op.contactEmailId,
                        opportunityId: op.opportunityId
                    });

                    oppIdIntsObj[op.opportunityId] = totalInts;

                    oppsInteractions.interactions.push({
                        axis: op.opportunityName,
                        value: totalInts,
                        name:op.opportunityName,
                        stageName:op.stageName,
                        interactionsCount:totalInts,
                        amount: op.convertedAmt,
                        amountWithNgm: op.convertedAmtWithNgm,
                        contactEmailId: op.contactEmailId,
                        opportunityId: op.opportunityId
                    });
                }

                if(op.relatasStage !== "Close Lost" && op.relatasStage !== "Close Won"){
                    if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){
                        thisQuarterOpenOpps.opportunities.push({
                            axis: op.opportunityName,
                            value: op.convertedAmtWithNgm,
                            amountWithNgm: op.convertedAmtWithNgm,
                            name:op.opportunityName,
                            stageName:op.stageName,
                            amount: op.convertedAmt,
                            contactEmailId: op.contactEmailId,
                            opportunityId: op.opportunityId
                        });

                        thisQuarterOpenOpps.interactions.push({
                            axis: op.opportunityName,
                            value: totalInts,
                            name:op.opportunityName,
                            stageName:op.stageName,
                            interactionsCount:totalInts,
                            amount: op.convertedAmt,
                            amountWithNgm: op.convertedAmtWithNgm,
                            contactEmailId: op.contactEmailId,
                            opportunityId: op.opportunityId
                        });
                    }
                }

                dataCreated_cr.push({
                    monthYear:monthNames[moment(new Date(op.createdDate)).month()].substring(0,3) +" "+moment(new Date(op.createdDate)).year(),
                    emailId:op.userEmailId,
                    sortDate:op.createdDate
                });

                if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){
                    thisQuarterOpps.push(op)

                    op.monthYear = moment(op.closeDate).format("MMM YYYY");
                    if(!thisQuarterOppsObj[op.monthYear]){
                        thisQuarterOppsObj[op.monthYear] = [];
                    }
                    thisQuarterOppsObj[op.monthYear].push(op);
                }

                if(_.includes(["Close Won"], op.stageName)){

                    //For Qtr insights
                    if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){

                        wonAmt = wonAmt+op.convertedAmtWithNgm
                        wonCount++;

                        if(op.closeReasons && op.closeReasons.length>0){
                            _.each(op.closeReasons,function (cr) {
                                var wonObj = {
                                    name:cr?cr:"Others",
                                    amount:op.convertedAmtWithNgm
                                }
                                wonReasons.push(wonObj)
                            })
                        }

                        if(op.geoLocation && op.geoLocation.town){
                            locations.push({
                                amount:op.convertedAmtWithNgm,
                                name:op.geoLocation.town,
                                zone:op.geoLocation.zone?op.geoLocation.zone:""
                            })
                        } else {
                            locations.push({
                                amount:op.convertedAmtWithNgm,
                                name:"no_region_set",
                                zone:op.geoLocation.zone?op.geoLocation.zone:"no_zone_set"
                            })
                        }

                        var obj = {}
                        obj[op.productType?op.productType:"Others"] = op.convertedAmtWithNgm;
                        var accObj = {}
                        accObj[op.accountName] = op.convertedAmtWithNgm
                        accounts.push(accObj)
                        products.push(obj);

                        oppTypes.push({
                            name:op.type,
                            amount:op.convertedAmtWithNgm
                        })

                        sourceTypes.push({
                            name:op.sourceType,
                            amount:op.convertedAmtWithNgm
                        });
                    }

                    dataClosed_cr.push({
                        emailId:op.userEmailId,
                        monthYear:monthNames[moment(new Date(op.closeDate)).month()].substring(0,3) +" "+moment(new Date(op.closeDate)).year(),
                        sortDate:op.closeDate
                    })

                } else if(_.includes(["Close Lost"], op.stageName)){

                    //For Qtr insights
                    if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){

                        lostAmt = lostAmt+op.convertedAmtWithNgm
                        lostCount++

                        if(op.closeReasons && op.closeReasons.length>0){
                            _.each(op.closeReasons,function (cr) {
                                var lostObj = {
                                    name:cr?cr:"Others",
                                    amount:op.convertedAmtWithNgm
                                }
                                lostReasons.push(lostObj)
                            })
                        }
                    }
                } else {
                    if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){
                        pipelineAmt = pipelineAmt+op.convertedAmtWithNgm
                        pipelineCount++
                    }
                }

                if(op.relatasStage !== "Close Lost" && op.relatasStage !== "Close Won"){
                    if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){
                        if(new Date(op.closeDate)< new Date(moment().startOf("day"))){
                            staleCount++;
                            staleAmt = staleAmt+op.convertedAmtWithNgm
                        }
                    }
                }

                if((new Date(op.closeDate)>= new Date(qStart)) && new Date(op.closeDate)<= new Date(qEnd)){
                    if(_.includes(renewalTypes,op.type)){
                        renewalAmt = renewalAmt+op.convertedAmtWithNgm
                        renewalCount++;
                    }
                }

                if(!op.currency){
                    op.currency = primaryCurrency
                }

                productsAll.push({
                    name:op.productType,
                    displayName:op.productType
                });

                if((new Date(op.closeDate)>= new Date(fyRange.start)) && new Date(op.closeDate)<= new Date(fyRange.end)){
                    thisFyOpps.push(op)
                }

            })
        }

        var dayString = moment().format("DDMMYYYY");
        var date = new Date();

        if(allQuarters.currentQuarter === "quarter1"){
            dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
            date = new Date(allQuarters[allQuarters.currentQuarter].end);
        }

        getOppsInteractionsForTeam(allEmailIds,company._id,dayString,forTeam,function (oppsInteractions_team,topOppByOppThisQtr_team) {

            if(forTeam){
                oppIdIntsObj = {};
                _.each(oppsInteractions_team.interactions,function (op) {
                    oppIdIntsObj[op.opportunityId] = op.interactionsCount
                })
            }

            dashboardInsightsCollection.update({companyId:company._id,
                    dayString:dayString,
                    ownerId: user._id,
                    ownerEmailId: user.emailId,
                    "forTeam":forTeam
                },
                {
                    $set:{
                        companyId:company._id,
                        dayString:dayString,
                        ownerId: user._id,
                        date: new Date(date),
                        ownerEmailId: user.emailId,
                        "oppWon":{
                            amount:wonAmt,
                            count:wonCount
                        },
                        "oppLost":{
                            amount:lostAmt,
                            count:lostCount
                        },
                        "renewalOpen":{
                            amount:renewalAmt,
                            count:renewalCount
                        },
                        "stale":{
                            amount:staleAmt,
                            count:staleCount
                        },
                        "accountsWon":accounts,
                        "reasonsWon":wonReasons,
                        "reasonsLost":lostReasons,
                        "regionsWon":locations,
                        "sourcesWon":sourceTypes,
                        "typesWon":oppTypes,
                        "productsWon":products,
                        oppsInteractions: forTeam?oppsInteractions_team:oppsInteractions,
                        topOppByOppThisQtr:forTeam?topOppByOppThisQtr_team:thisQuarterOpenOpps,
                        "dealsAtRisk":data[2],
                        "forTeam":forTeam,
                        currentInsights:currentInsights(raw_opps,qStart,qEnd,oppIdIntsObj)
                    }
                },{upsert:true},function (err,results) {

                    pipelineFlow(company,user,raw_opps,allQuarters,primaryCurrency,currenciesObj,allEmailIds,function(err,pfData){

                        var newCompaniesInteractedData = data[0] && data[0].newCompaniesInteracted?newCompaniesInteracted(data[0].newCompaniesInteracted):[]

                        if(forTeam){
                            newCompaniesInteractedData = data[4] && data[4].newCompaniesInteracted?data[4].newCompaniesInteracted:[]
                        }

                        dashboardInsightsCollection.update({
                                companyId:company._id,
                                dayString:dayString,
                                ownerId: user._id,
                                ownerEmailId: user.emailId,
                                "forTeam":forTeam
                            },
                            {
                                $set: {
                                    // companyId: company._id,
                                    dayString: dayString,
                                    date: new Date(date),
                                    ownerId: user._id,
                                    ownerEmailId: user.emailId,
                                    pipelineFlow: pfData,
                                    newCompaniesInteracted: newCompaniesInteractedData,
                                    conversionRate: conversionRate(dataCreated_cr,dataClosed_cr),
                                    pipelineFunnel: pipelineFunnel(thisFyOpps),
                                    pipelineVelocity: pipelineVelocity(thisQuarterOpps,data[3],thisQuarterOppsObj,primaryCurrency),
                                    targets: data[3],
                                    forTeam:forTeam
                                }
                            },{upsert:true},function(err2,results2){
                                callback()
                            });

                    });
                })
        })
    });
}

function getOppsInteractionsForTeam(userEmailIds,companyId,dayString,forTeam,callback){

    if(forTeam){
        dashboardInsightsCollection.find({companyId:companyId,
            dayString:dayString,
            ownerEmailId: {$in:userEmailIds},
            forTeam:false
        },{oppsInteractions:1,topOppByOppThisQtr:1}).toArray(function (err,data) {
            if(err){
                console.log("---getOppsInteractionsForTeam---")
                console.log(err)
            }

            var oppsInteractions = {
                opportunities:[],
                interactions: []
            }

            var topOppByOppThisQtr = {
                opportunities:[],
                interactions: []
            }

            if(!err && data && data.length>0){
                _.each(data,function (el) {
                    if(el.topOppByOppThisQtr){
                        topOppByOppThisQtr.opportunities = topOppByOppThisQtr.opportunities.concat(el.topOppByOppThisQtr.opportunities)
                        topOppByOppThisQtr.interactions = topOppByOppThisQtr.interactions.concat(el.topOppByOppThisQtr.interactions)
                    }

                    if(el.oppsInteractions){
                        oppsInteractions.opportunities = oppsInteractions.opportunities.concat(el.oppsInteractions.opportunities)
                        oppsInteractions.interactions = oppsInteractions.interactions.concat(el.oppsInteractions.interactions)
                    }
                })
            }
            callback(oppsInteractions,topOppByOppThisQtr)
        });
    } else {
        callback()
    }

}

function interactionsForOpps(raw_opps,forTeam,callback){

    var contacts = [],
    userEmailIds = [];
    var minInteractionDate = new Date();

    if(raw_opps && raw_opps.length>0 && !forTeam){
        _.each(raw_opps,function (op) {

            op.createdDate = op.createdDate ? op.createdDate : op._id.getTimestamp();

            if(new Date(op.createdDate)<= minInteractionDate){
                minInteractionDate = new Date(op.createdDate);
            }

            var allContactsInOpp = [op.contactEmailId];

            if (op.decisionMakers && op.decisionMakers.length > 0) {
                allContactsInOpp = allContactsInOpp.concat(_.pluck(op.decisionMakers, "emailId"))
            }

            if (op.influencers && op.influencers.length > 0) {
                allContactsInOpp = allContactsInOpp.concat(_.pluck(op.influencers, "emailId"))
            }

            if (op.partners && op.partners.length > 0) {
                allContactsInOpp = allContactsInOpp.concat(_.pluck(op.partners, "emailId"))
            }

            userEmailIds.push(op.userEmailId);
            contacts = contacts.concat(allContactsInOpp);
        });

        userEmailIds = _.uniq(userEmailIds);

        interactionsCollection.find({ownerEmailId: {$in: userEmailIds},interactionDate:{$gte:minInteractionDate},emailId: {$in: _.compact(_.uniq(contacts))}}
            ,{ownerEmailId:1,emailId:1,interactionDate:1}).toArray(function (err,interactions) {
            if(callback && !err){

                // if(interactions && interactions.length>0){
                //     interactions = _.uniq(interactions,'refId');
                // }

                var intGroupByContactObj = {};
                var intGroupByContact =  _
                    .chain(interactions)
                    .groupBy('emailId')
                    .map(function(values, key) {
                        intGroupByContactObj[key] = {
                            count:values.length,
                            interactions:values
                        }

                        return null;
                    })
                    .value();
                callback(intGroupByContactObj)
            } else {
                callback({})
            }
        })
    } else {
        callback({})
    }
}

function pipelineFunnel(opps) {

    var group = _
        .chain(opps)
        .groupBy('relatasStage')
        .map(function(values, key) {
            return {
                _id:key,
                totalAmount:sumBy(values,"convertedAmtWithNgm"),
                count:values.length,
                opps:values
            };
        })
        .value();

    var stages =[
        'Prospecting',
        'Evaluation',
        'Proposal',
        'Close Won',
        'Close Lost'
    ]

    var existingStages = _.pluck(group,"_id");

    var uniqueResult = stages.filter(function(obj) {
        return !existingStages.some(function(obj2) {
            return obj == obj2;
        });
    });

    var nonExisting = [];
    if(uniqueResult.length>0){
        _.each(uniqueResult,function (el) {
            nonExisting.push({
                _id:el,
                totalAmount:0,
                count:0
            })
        })
    }

    var data = group.concat(nonExisting);

    return data;
}

var userFiscalYear = function (company,user,callback) {

    if(company && user){

        var fyMonth = "April"; //Default
        if (!company || !company.fyMonth) {
        } else {
            fyMonth = company.fyMonth
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if(currentMonth<monthNames.indexOf(fyMonth)){
            fromDate.setFullYear(currentYr-1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month');

        var timezone = user.timezone && user.timezone.name?user.timezone.name:"UTC";

        callback(company.fyMonth,
            {start:moment(fromDate).tz(timezone).format(),end: moment(toDate).tz(timezone).format()},
            setQuarter(fyMonth,timezone,fromDate,toDate),
            timezone)
    } else {
        callback(null,null,null,null)
    }
}

function interactionsByAccount(companyId,userEmailId,qStart,qEnd,allQuarters,callback){

    var selfAccount = fetchCompanyFromEmail(userEmailId[0]);
    selfAccount = selfAccount?selfAccount:"Others"
    selfAccount = selfAccount.toLowerCase();
    var notMatch = {'$regex' : '^((?!'+selfAccount+').)*$'}

    interactionsCollection.find({ownerEmailId:{$in:userEmailId},emailId:notMatch,interactionDate:{$lte: new Date(),$gte: new Date(moment().subtract(3,"months"))}},{refId:1,ownerEmailId:1,emailId:1,interactionDate:1}).toArray(function (err,interactions) {

        if(!err && interactions && interactions.length>0){

            if(interactions && interactions.length>0){
                interactions = _.uniq(interactions,'refId');
            }

            for(var i=0; i<interactions.length; i++){

                var accName = fetchCompanyFromEmail(interactions[i].emailId);
                accName = accName?accName:"Others"

                if(selfAccount !== accName){
                    interactions[i].accountName = accName
                } else {

                }

                interactions[i].monthYear = monthNames[moment(new Date(interactions[i].interactionDate)).month()] +" "+moment(new Date(interactions[i].interactionDate)).year()
            }
        } else {
            interactions = [];
        }

        groupIntsByAccName(interactions,qStart,qEnd,userEmailId,companyId,allQuarters,function (newCompaniesInteracted) {
            if(callback){
                callback(null,newCompaniesInteracted)
            }
        })
    })
}

function getOppsForUser(userEmailId,companyId,primaryCurrency,currenciesObj,qStart,qEnd,forTeam,allQuarters,fyEnd,callback){

    var query = {
        // $or:[{companyId:companyId},{companyId:companyId.toString()}],
        // userEmailId:{$in:userEmailId},
        closeDate:{$lte: new Date(fyEnd)},
        $or:[{
            userEmailId:{$in:userEmailId}
        },{"usersWithAccess.emailId":{$in:userEmailId}}]
    }

    opportunitiesCollection.find(query).toArray(function (err,opps) {

        if(!err && opps && opps.length>0){
            _.each(opps,function (op) {
                var accName = fetchCompanyFromEmail(op.contactEmailId);
                accName = accName?accName:"Others"

                op.accountName = accName;
                op.amount = parseFloat(op.amount);
                op.amountWithNgm = op.amount;

                if(op.netGrossMargin || op.netGrossMargin == 0){
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                }

                op.convertedAmt = op.amount;

                op.convertedAmtWithNgm = op.amountWithNgm

                if(op.currency && op.currency !== primaryCurrency){

                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                    }

                    if(op.netGrossMargin || op.netGrossMargin == 0){
                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                    }

                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))

                }

            });

            callback(err,groupOppsByAccName(opps,qStart,qEnd,companyId,forTeam,userEmailId,allQuarters))
        } else {
            callback(err,{
                raw_opps:[],
                array:[]
            })
        }
    });
}

function updateTeamInsightsAllUsers(users,company,fyMonth,fyRange,allQuarters,callback) {
    async.eachSeries(users, function (user, next){
        insightsForTeam(user,company,fyMonth,fyRange,allQuarters,function(){
            next();
        });
    }, function(err) {
        callback()
    });
}

function insightsForTeam(user,company,fyMonth,fyRange,allQuarters,callback){

    getAllReportees(user,function (allReportees,allEmailIds,allUserIds) {

        console.log("Team for::",user.emailId,"--Reportees>>",allEmailIds.length)
        var primaryCurrency = "USD",
            currenciesObj = {}

        company.currency.forEach(function (el) {
            currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                primaryCurrency = el.symbol;
            }
        });

        var qStart = allQuarters[allQuarters.currentQuarter].start;
        var qEnd = allQuarters[allQuarters.currentQuarter].end;

        async.parallel([
            function (callback) {
                callback(null,[]) //Don't get interactions at team level.
            },
            function (callback) {
                getOppsForUser(allEmailIds,user.companyId,primaryCurrency,currenciesObj,qStart,qEnd,false,allQuarters,fyRange.end,callback)
            },
            function (callback) {
                dealsAtRiskTeam(user.companyId,allEmailIds,allQuarters,callback)
            },
            function (callback) {
                getTargets(allUserIds,qStart,qEnd,fyRange.start,fyRange.end,callback)
            },
            function (callback) {
                getCompaniesInteracted(user.companyId,allEmailIds,callback)
            }
        ],function (err,data) {
            if(err){
                console.log("-----insightsForTeam-----")
                console.log(err)
            }

            insightsBuildAndUpdate(data,company,primaryCurrency,currenciesObj,fyRange,user,allQuarters,true,allEmailIds,function () {
                callback();
            });
        })
    });
}

function getAllReportees(user,callback){

    async.parallel([
        function (callback) {
            getOrgHierarchy(user._id,user.companyId,callback)
        },
        function (callback) {
            getSecondaryHierarchies(user._id,user.companyId,callback)
        }
    ],function (err,data) {
        var allUsers = [{
            emailId:user.emailId,
            _id:user._id
        }];

        var allEmailIds = [user.emailId],
            allUserIds = [user._id];

        if(!err) {
            if(data[0]){
                _.each(data[0],function(el){
                    allEmailIds.push(el.emailId)
                    allUserIds.push(el._id)
                    allUsers.push({
                        emailId:el.emailId,
                        _id:el._id
                    })
                })
            }
            if(data[1]){
                _.each(data[1],function(el){
                    allEmailIds.push(el.ownerEmailId)
                    allUserIds.push(el.userId)
                    allUsers.push({
                        emailId:el.ownerEmailId,
                        _id:el.userId
                    })
                })
            }

            callback(uniqBy(allUsers,"emailId"),uniqBy(allEmailIds),allUserIds)
        } else {
            callback([],[],[])
        }
    })
}

var getOrgHierarchy = function(userId,companyId,callBack) {
    usersCollection.find({ corporateUser: true,companyId:companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' },
            resource: { $ne: true } },
        { _id: 1, emailId: 1, designation: 1, hierarchyParent: 1, hierarchyPath: 1, firstName: 1, lastName: 1,companyId:1,regionOwner: 1,productTypeOwner: 1,verticalOwner: 1 }).toArray(function(err, users) {
        callBack(null, users)
    })
};

var getSecondaryHierarchies= function (userId,companyId,callback){
    shCollection.find({ companyId:companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' }},{monthlyTargets:0}).toArray(function (err,result) {
        callback(err,result)
    });
}

function getCompaniesInteracted(companyId,userEmailId,callback){
    newCompaniesInteraction.find({ownerEmailId:{$in:userEmailId}}).toArray(function (err,interactions) {
        if (!err && interactions && interactions.length > 0) {

            var uniqInts = [];
            var intGroup = _
                .chain(interactions)
                .groupBy('accountName')
                .map(function(values, key) {

                    values.sort(function (o1, o2) {
                        return o1.interactionDate > o2.interactionDate ? 1 : o1.interactionDate < o2.interactionDate ? -1 : 0;
                    });

                    uniqInts.push(values[0]);
                    return null;
                }).value()

            callback(err, {newCompaniesInteracted: newCompaniesInteracted(uniqInts)});
        } else {
            callback(null, {newCompaniesInteracted: []})
        }
    })

}

function newCompaniesInteracted(interactionAccounts){

    var filler = [];
    var monthsPrev = 6;

    for(var i = 0;i<monthsPrev;i++){
        var date = new Date(moment().subtract(i, "month").toDate())
        var obj = {
            monthYear:monthNames[date.getUTCMonth()]+" "+date.getUTCFullYear(),
            sortDate:date,
            accountDetails:[]
        };

        filler.push(obj)
    };

    var finalData = _
        .chain(interactionAccounts)
        .groupBy('monthYear')
        .map(function(value, key) {

            return {
                monthYear: key,
                sortDate:value[0].interactionDate,
                accountDetails: value
            }
        })
        .value();

    // Find values that are in filler but not in arrayValues
    var fillerResults = filler.filter(function(obj) {
        return !finalData.some(function(obj2) {
            return obj.monthYear == obj2.monthYear;
        });
    });

    var data = finalData.concat(fillerResults)

    data.sort(function (o1, o2) {
        return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
    });

    var sixMonthsAgo = moment(moment().subtract(6, "month")).startOf("month");

    data = data.filter(function (el) {
        if(new Date(el.sortDate)>=new Date(sixMonthsAgo)){
            return el;
        }
    });

    return data;
}

function pipelineFlow(company,user,opps,allQuarters,primaryCurrency,currenciesObj,allEmailIds,callback){

    var qStart = allQuarters[allQuarters.currentQuarter].start;
    var qEnd = allQuarters[allQuarters.currentQuarter].end;

    var startMonthYear = {
        month:moment(qStart).month(),
        year:moment(qStart).year()
    }

    var endMonthYear = {
        month:moment(qEnd).month()+1,
        year:moment(qEnd).year()
    }

    var group = _
        .chain(opps)
        .groupBy('relatasStage')
        .map(function(values, key) {
            return {
                _id:key,
                opportunities:values
            };
        })
        .value();

    var oppStages = [];
    var commitStage = "Proposal"; // default.

    _.each(company.opportunityStages,function (st) {
        oppStages.push(st.name);
        if(st.commitStage) {
            commitStage = st.name;
        }
    });

    var asOfToday = buildOppMetaDataObj(group, user.emailId, company._id, endMonthYear.month, endMonthYear.year,primaryCurrency,currenciesObj,qStart,qEnd);

    getMetaDataForMonth(company._id,user.emailId,startMonthYear,endMonthYear,qStart,qEnd,primaryCurrency,currenciesObj,asOfToday,allEmailIds,function (err,pipelineFlowData) {

        callback(err,{
            metaData:pipelineFlowData,
            oppStages:oppStages,
            qStart:new Date(qStart),
            qEnd:new Date(qEnd),
            commitStage:commitStage
        })
    });
}

function pipelineVelocity(thisQuarterOpps,targetGraph,thisQuarterOppsObj,primaryCurrency){

    var currentMonthYear = moment().format("MMM YYYY")

    if(targetGraph && targetGraph.length>0){

        _.each(targetGraph,function (tr) {
            tr.monthYear = moment(tr.date).format("MMM YYYY")
            var thisMonthOpps = thisQuarterOppsObj[tr.monthYear];

            var won = 0,
                lost = 0,
                pipeline = 0,
                min = 0,
                max = 0;

            _.each(thisMonthOpps,function (op) {
                if(op.relatasStage == "Close Won"){
                    won = won+op.convertedAmtWithNgm
                } else if(op.relatasStage == "Close Lost"){
                    lost = lost+op.convertedAmtWithNgm
                } else {
                    pipeline = pipeline+op.convertedAmtWithNgm
                }
            })

            max = won+lost+pipeline+tr.target;

            tr.won = won;
            tr.lost = lost;
            tr.pipeline = pipeline;

            if(tr.monthYear == currentMonthYear){
                tr.highLightCurrentMonth = true
            }

            tr.heightWon = {'height':scaleBetween(tr.won,min,max)+'%'}
            tr.heightLost = {'height':scaleBetween(tr.lost,min,max)+'%'}
            tr.heightTotal = {'height':scaleBetween(tr.pipeline,min,max)+'%'}
            tr.heightTarget = {'height':scaleBetween(tr.target,min,max)+'%'}

            tr.won = numberWithCommas(formatNumber(tr.won),primaryCurrency == "INR");
            tr.lost = numberWithCommas(formatNumber(tr.lost),primaryCurrency == "INR")
            tr.openValue = numberWithCommas(formatNumber(tr.pipeline),primaryCurrency == "INR")
            tr.target = numberWithCommas(tr.target?formatNumber(tr.target):0,primaryCurrency == "INR")

            tr.month = moment(tr.date).format("MMM");

        })

        targetGraph.sort(function (o1,o2) {
            return o1.date > o2.date ? 1 : o1.date < o2.date ? -1 : 0;
        });
    }

    return targetGraph;
}

function getMetaDataForMonth(companyId,userEmailId,startMonthYear,endMonthYear,qStart,qEnd,primaryCurrency,currenciesObj,asOfToday,allEmailIds,callback){

    var findQuery = {
        companyId:companyId,
        userEmailId:{$in:allEmailIds},
        // $or:[{month:startMonthYear.month,year:startMonthYear.year}]
        $or:[{month:3,year:2019}] //For Prev Months Data Pipeline numbers
    }

    OppMetaDataCollection.find(findQuery).toArray(function (err,asOfMonthStart) {

        if(asOfToday){
            callback(err,[flattenOppMeta(asOfMonthStart),asOfToday]);
        } else {
            callback(err,[{},{}]);
        }
    });
}

function buildOppMetaDataObj(opps,userEmailId,companyId,month,year,primaryCurrency,currenciesObj,startSelection,endSelection){
    var obj = {
        data:[]
    };

    var createdThisSelection = [];
    var dataForMonthYear = month+""+year

    _.each(opps,function (el) {
        obj.userEmailId = userEmailId;
        obj.companyId = companyId;
        obj.month = month;
        obj.year = year;

        var amount = 0
            ,amountWithNgm = 0
            ,oppIds = []
            ,regions = []
            ,products = []
            ,verticals = []
            ,reasons = []
            ,types = []
            ,solutions = []
            ,businessUnits = [];

        _.each(el.opportunities,function (op) {

            amountWithNgm = amountWithNgm+op.convertedAmtWithNgm
            amount = amount+op.convertedAmt;
            if(op.geoLocation && (op.geoLocation.zone || op.geoLocation.town)){
                regions.push(op.geoLocation);
            }

            products.push(op.productType);
            reasons.push(op.closeReasons);
            types.push(op.type);
            solutions.push(op.solution);
            verticals.push(op.vertical);
            businessUnits.push(op.businessUnit);

            var monthYearCreated = moment(op.createdDate).month()+""+moment(op.createdDate).year();

            if(new Date(op.createdDate)>= new Date(startSelection) && new Date(op.createdDate)<= new Date(endSelection)){
                createdThisSelection.push({
                    closeDate:op.closeDate,
                    createdDate:op.createdDate,
                    opportunityId:op.opportunityId,
                    amount:op.amountWithNgm,
                    amount_original:op.amount,
                    convertedAmt:op.convertedAmt,
                    convertedAmtWithNgm:op.convertedAmtWithNgm,
                    fromSnapShot:false,
                    userId:op.userId,
                    userEmailId:op.userEmailId,
                    contactEmailId:op.contactEmailId,
                    relatasStage:op.relatasStage,
                    stageName:op.stageName,
                    opportunityName:op.opportunityName,
                    geoLocation:op.geoLocation,
                    productType:op.productType,
                    createdByEmailId:op.createdByEmailId,
                    netGrossMargin: op.netGrossMargin
                })
            }

            oppIds.push({
                closeDate:op.closeDate,
                createdDate:op.createdDate,
                opportunityId:op.opportunityId,
                amount:op.amountWithNgm,
                amount_original:op.amount,
                convertedAmt:op.convertedAmt,
                convertedAmtWithNgm:op.convertedAmtWithNgm,
                fromSnapShot:monthYearCreated == dataForMonthYear?false:true,
                userId:op.userId,
                userEmailId:op.userEmailId,
                contactEmailId:op.contactEmailId,
                relatasStage:op.relatasStage,
                stageName:op.stageName,
                opportunityName:op.opportunityName,
                geoLocation:op.geoLocation,
                productType:op.productType,
                createdByEmailId:op.createdByEmailId,
                netGrossMargin: op.netGrossMargin
            });

        });

        obj.data.push({
            stageName:el._id,
            oppIds:oppIds,
            amount:amount,
            amountWithNgm:amountWithNgm,
            numberOfOpps:el.opportunities.length,
            regions:_.compact(_.flatten(regions)),
            reasons:_.compact(_.flatten(reasons)),
            products:_.compact(_.flatten(products)),
            solutions:_.compact(_.flatten(solutions)),
            verticals:_.compact(_.flatten(verticals))
        })
    })

    return {
        createdThisSelection:createdThisSelection,
        oppMetaDataFormat:obj
    };
}

function flattenOppMeta(asOfMonthStart){

    var flattenedData = {};

    if(asOfMonthStart && asOfMonthStart[0]){

        if(asOfMonthStart && asOfMonthStart.length === 1){
            flattenedData = asOfMonthStart[0];
        } else {
            flattenedData = {
                companyId:asOfMonthStart[0].companyId,
                month:asOfMonthStart[0].month,
                year:asOfMonthStart[0].year,
                userEmailId:"allUsers",
                data:[]
            }

            var obj = {};
            _.each(asOfMonthStart,function (el) {

                _.each(el.data,function (da) {

                    if(da.stageName){
                        if(!obj[da.stageName]){
                            obj[da.stageName] = {
                                oppIds:[],
                                regions:[],
                                reasons:[],
                                products:[],
                                solutions:[],
                                verticals:[],
                                amount:0,
                                amountWithNgm:0,
                                numberOfOpps:0,
                                stageName:da.stageName
                            };
                        }
                        obj[da.stageName].oppIds = obj[da.stageName].oppIds.concat(da.oppIds)
                        obj[da.stageName].regions = obj[da.stageName].regions.concat(da.regions)
                        obj[da.stageName].reasons = obj[da.stageName].reasons.concat(da.reasons)
                        obj[da.stageName].products = obj[da.stageName].products.concat(da.products)
                        obj[da.stageName].solutions = obj[da.stageName].solutions.concat(da.solutions)
                        obj[da.stageName].verticals = obj[da.stageName].verticals.concat(da.verticals)
                        obj[da.stageName].amount = obj[da.stageName].amount+da.amount
                        obj[da.stageName].amountWithNgm = obj[da.stageName].amountWithNgm+da.amountWithNgm
                        obj[da.stageName].numberOfOpps = obj[da.stageName].numberOfOpps+da.numberOfOpps
                    }
                });
            });

            for(var key in obj){
                flattenedData.data.push(obj[key])
            }
        }
    }

    return flattenedData;
}

function conversionRate(dataCreated,dataClosed) {
    var filler = [];
    var monthsPrev = 6;

    for(var i = 0;i<monthsPrev;i++){
        var prevDate = new Date(moment().subtract(i, "month").toDate())

        var obj = {
            monthYear:monthNames[prevDate.getUTCMonth()].substring(0,3)+" "+prevDate.getUTCFullYear(),
            sortDate:prevDate,
            count:0
        };

        filler.push(obj)
    }

    var created = _
        .chain(dataCreated)
        .groupBy('monthYear')
        .map(function(value, key) {

            return {
                monthYear: key,
                sortDate:value[0].sortDate,
                count: value.length
            }
        })
        .value();

    var closed = _
        .chain(dataClosed)
        .groupBy('monthYear')
        .map(function(value, key) {

            return {
                monthYear: key,
                sortDate:value[0].sortDate,
                count: value.length
            }
        })
        .value();
    //Find values that are in filler but not in arrayValues
    var fillerResults = filler.filter(function(obj) {
        return !created.some(function(obj2) {
            return obj.monthYear == obj2.monthYear;
        });
    });

    var data = created.concat(fillerResults)

    //Find values that are in filler but not in arrayValues
    var fillerResultsClose = filler.filter(function(obj) {
        return !closed.some(function(obj2) {
            return obj.monthYear == obj2.monthYear;
        });
    });

    var data2 = closed.concat(fillerResultsClose)

    var nonExistingMonthsClose = data.filter(function(obj) {
        return !data2.some(function(obj2) {
            return obj.monthYear == obj2.monthYear;
        });
    });

    _.each(nonExistingMonthsClose,function (el) {
        data2.push({
            monthYear:el.monthYear,
            sortDate:el.sortDate,
            count:0
        })
    });

    var nonExistingMonthsOpen = data2.filter(function(obj) {
        return !data.some(function(obj2) {
            return obj.monthYear == obj2.monthYear;
        });
    });

    _.each(nonExistingMonthsOpen,function (el) {
        data.push({
            monthYear:el.monthYear,
            sortDate:el.sortDate,
            count:0
        })
    });

    var prevSixMonthDate = moment().subtract(7,"month");

    data = data.filter(function (fl) {
        if(new Date(fl.sortDate)>= new Date(prevSixMonthDate)){
            return fl;
        }
    })

    data2 = data2.filter(function (fl) {
        if(new Date(fl.sortDate)>= new Date(prevSixMonthDate)){
            return fl;
        }
    })

    data.sort(function (o1, o2) {
        return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
    });

    data2.sort(function (o1, o2) {
        return new Date(o1.sortDate) > new Date(o2.sortDate) ? 1 : new Date(o1.sortDate) < new Date(o2.sortDate) ? -1 : 0;
    });

    var result = {
        created:data.slice(Math.max(data.length - 7, 1)),
        closed:data2.slice(Math.max(data2.length - 7, 1))
    }

    return result;
}

function updateAccountInsightsWithInteractions(data,companyId,qStart,qEnd,allQuarters,callback) {

    var dayString = moment().format("DDMMYYYY");
    var date = new Date();

    if(allQuarters.currentQuarter === "quarter1"){
        dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
        date = new Date(allQuarters[allQuarters.currentQuarter].end)
    }

    accountInsightsCollection.find({companyId:companyId,dayString:dayString}).toArray(function (err,dataExists) {

        if(!err && dataExists[0]){

            if(data && data.array && data.array.length>0){

                if(dataExists[0].accountsInteractions && dataExists[0].accountsInteractions.interactions && dataExists[0].accountsInteractions.interactions.length>0){

                    var obj = dataExists[0].accountsInteractions.interactions.concat(data.array),
                        holder = {};

                    obj.forEach(function (d) {
                        if(holder.hasOwnProperty(d.axis)) {

                            var lastInteractedDate = null;

                            if(holder[d.axis].lastInteractedDate && d.lastInteractedDate){
                                lastInteractedDate = new Date(holder[d.axis].lastInteractedDate)> new Date(d.lastInteractedDate)?new Date(holder[d.axis].lastInteractedDate):new Date(d.lastInteractedDate)
                            }

                            holder[d.axis] = {
                                lastInteractedDate:lastInteractedDate,
                                interactionsCount: holder[d.axis].interactionsCount + d.interactionsCount,
                                usersWithAccess: _.uniq(holder[d.axis].usersWithAccess.concat(d.usersWithAccess))
                            };
                        } else {
                            holder[d.axis] = {
                                lastInteractedDate:d.lastInteractedDate,
                                interactionsCount: d.interactionsCount,
                                usersWithAccess: d.usersWithAccess
                            };
                        }
                    });

                    var obj2 = [];

                    for(var prop in holder) {
                        obj2.push({
                            axis: prop,
                            name: prop,
                            value: holder[prop].interactionsCount,
                            lastInteractedDate:holder[prop].lastInteractedDate,
                            interactionsCount: holder[prop].interactionsCount,
                            usersWithAccess: holder[prop].usersWithAccess
                        });
                    }

                    accountInsightsCollection.update({companyId:companyId,
                        dayString:dayString}, {
                        $set: {
                            companyId: companyId,
                            date: date,
                            dayString: dayString,
                            "accountsInteractions.interactions": obj2
                        }
                    },function (err1,results) {

                        if(callback){
                            callback()
                        }
                    })
                } else {
                    accountInsightsCollection.update({companyId:companyId,
                        dayString:dayString}, {
                        $set: {
                            companyId: companyId,
                            date: date,
                            dayString: dayString,
                            "accountsInteractions.interactions": data.array
                        }
                    },{upsert:true},function (err1,results) {

                        if(callback){
                            callback()
                        }
                    })
                }
            }

        } else {
            accountInsightsCollection.update({companyId:companyId,
                dayString:dayString}, {
                $set: {
                    companyId: companyId,
                    date: date,
                    dayString: dayString,
                    "accountsInteractions.interactions": data.array
                }
            },{upsert:true},function (err1,results) {

                if(callback){
                    callback()
                }
            })
        }
    })
}

function updateAccountInsightsWithOpps(data,companyId,allQuarters,callback) {
    var dayString = moment().format("DDMMYYYY");

    if(allQuarters.currentQuarter === "quarter1"){
        dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
    }

    accountInsightsCollection.find({companyId:companyId,dayString:dayString}).toArray(function (err,dataExists) {
        forAccsInts(err,dataExists,data,companyId,allQuarters,function () {
            forAccsIntsThisQtr(err,dataExists,data,companyId,allQuarters,callback);
        });
    })
}

function forAccsInts(err,dataExists,data,companyId,allQuarters,callback) {

    var dayString = moment().format("DDMMYYYY");
    var date = new Date()
    if(allQuarters.currentQuarter === "quarter1"){
        dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
        date = new Date(allQuarters[allQuarters.currentQuarter].end)
    }

    if(!err && dataExists[0]){

        if(data && data.array && data.array.length>0){

            if(dataExists[0].accountsInteractions && dataExists[0].accountsInteractions.opportunities && dataExists[0].accountsInteractions.opportunities.length>0){

                var obj2 = sumUpOppsAll(dataExists,data);

                accountInsightsCollection.update({companyId:companyId,
                    dayString:dayString}, {
                    $set: {
                        companyId: companyId,
                        date: date,
                        dayString: dayString,
                        "accountsInteractions.opportunities": obj2
                    }
                },function (err1,results) {

                    if(callback){
                        callback()
                    }
                })
            } else {

                accountInsightsCollection.update({companyId:companyId,
                    dayString:dayString}, {
                    $set: {
                        companyId: companyId,
                        dayString: dayString,
                        date: date,
                        "accountsInteractions.opportunities": data.array
                    }
                },{upsert:true},function (err1,results) {

                    if(callback){
                        callback()
                    }
                })
            }
        } else {

            if(callback){
                callback()
            }
        }

    } else {

        accountInsightsCollection.update({companyId:companyId,
            dayString:dayString}, {
            $set: {
                companyId: companyId,
                date: date,
                dayString: dayString,
                "accountsInteractions.opportunities": data.array
            }
        },{upsert:true},function (err1,results) {

            if(callback){
                callback()
            }
        })
    }
}

function forAccsIntsThisQtr(err,dataExists,data,companyId,allQuarters,callback) {

    var dayString = moment().format("DDMMYYYY");
    var date = new Date()
    if(allQuarters.currentQuarter === "quarter1"){
        dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
        date = new Date(allQuarters[allQuarters.currentQuarter].end)
    }

    if(!err && dataExists[0]){

        if(data && data.thisQtr && data.thisQtr.length>0){

            if(dataExists[0].topAccByOppThisQtr && dataExists[0].topAccByOppThisQtr.opportunities && dataExists[0].topAccByOppThisQtr.opportunities.length>0){

                var obj2 = sumUpOppsThisQtr(dataExists,data);

                accountInsightsCollection.update({companyId:companyId,
                    dayString:dayString}, {
                    $set: {
                        companyId: companyId,
                        date: date,
                        dayString: dayString,
                        "topAccByOppThisQtr.opportunities": obj2
                    }
                },function (err1,results) {

                    if(callback){
                        callback()
                    }
                })
            } else {

                accountInsightsCollection.update({companyId:companyId,
                    dayString:dayString}, {
                    $set: {
                        companyId: companyId,
                        dayString: dayString,
                        date: date,
                        "topAccByOppThisQtr.opportunities": data.thisQtr
                    }
                },{upsert:true},function (err1,results) {

                    if(callback){
                        callback()
                    }
                })
            }
        } else {
            if(callback){
                callback()
            }
        }

    } else {

        accountInsightsCollection.update({companyId:companyId,
            dayString:dayString}, {
            $set: {
                companyId: companyId,
                date: date,
                dayString: dayString,
                "topAccByOppThisQtr.opportunities": data.thisQtr
            }
        },{upsert:true},function (err1,results) {

            if(callback){
                callback()
            }
        })
    }
}

function sumUpOppsAll(dataExists,data) {

    var obj = dataExists[0].accountsInteractions.opportunities.concat(data.array),
        holder = {};

    obj.forEach(function (d) {

        if(holder.hasOwnProperty(d.axis)) {
            if(!holder[d.axis].opps){
                holder[d.axis].opps = []
            }

            holder[d.axis] = {
                oppsAmount: holder[d.axis].oppsAmount + d.oppsAmount,
                oppsCount: holder[d.axis].oppsCount+d.oppsCount,
                usersWithAccess: _.uniq(holder[d.axis].usersWithAccess.concat(d.usersWithAccess)),
                opps: holder[d.axis].opps.concat(d.opps)
            };
        } else {
            holder[d.axis] = {
                oppsAmount:d.oppsAmount,
                oppsCount:d.oppsCount,
                usersWithAccess:d.usersWithAccess,
                opps: d.opps
            };
        }
    });

    var obj2 = [];

    for(var prop in holder) {
        obj2.push({
            axis: prop,
            name: prop,
            value: holder[prop].oppsAmount,
            oppsAmount: holder[prop].oppsAmount,
            oppsCount: holder[prop].oppsCount,
            usersWithAccess: holder[prop].usersWithAccess,
            opps: holder[prop].opps
        });
    }

    return obj2;
}

function sumUpOppsThisQtr(dataExists,data) {

    var obj = dataExists[0].topAccByOppThisQtr.opportunities.concat(data.thisQtr),
        holder = {};

    obj.forEach(function (d) {

        if(holder.hasOwnProperty(d.axis)) {
            if(!holder[d.axis].opps){
                holder[d.axis].opps = []
            }

            holder[d.axis] = {
                oppsAmount: holder[d.axis].oppsAmount + d.oppsAmount,
                oppsCount: holder[d.axis].oppsCount+d.oppsCount,
                usersWithAccess: _.uniq(holder[d.axis].usersWithAccess.concat(d.usersWithAccess)),
                opps: holder[d.axis].opps.concat(d.opps)
            };
        } else {
            holder[d.axis] = {
                oppsAmount:d.oppsAmount,
                oppsCount:d.oppsCount,
                usersWithAccess:d.usersWithAccess,
                opps: d.opps
            };
        }
    });

    var obj2 = [];

    for(var prop in holder) {
        obj2.push({
            axis: prop,
            name: prop,
            value: holder[prop].oppsAmount,
            oppsAmount: holder[prop].oppsAmount,
            oppsCount: holder[prop].oppsCount,
            usersWithAccess: holder[prop].usersWithAccess,
            opps: holder[prop].opps
        });
    }

    return obj2;
}

function dealsAtRiskTeam(companyId,emailId,allQuarters,callback) {

    var dayString = moment().format("DDMMYYYY");

    if(allQuarters.currentQuarter === "quarter1") {
        dayString = moment(allQuarters[allQuarters.currentQuarter].end).format("DDMMYYYY");
    }

    var aggregation = [
        {
            $match: {
                companyId:companyId,ownerEmailId:{$in:emailId},dayString:dayString,forTeam:false
            }
        },
        {
            $project:{
                dealsAtRisk:1
            }
        },
        {
            $group:{
                _id:null,
                date:{ $max: "$dealsAtRisk.dealsRiskAsOfDate" },
                amount:{ $sum: "$dealsAtRisk.amount" },
                count:{ $sum: "$dealsAtRisk.count" }
            }
        }
    ]

    dashboardInsightsCollection.aggregate(aggregation).toArray(function (err,deals) {
    // dashboardInsightsCollection.aggregate(aggregation,function (err,deals) {
        if(err){
            console.log("---dashboardInsightsCollection---")
            console.log(err)
        }

        if(!err && deals && deals.length>0){

            if(deals[0].amount){
                deals[0].amount = parseFloat(deals[0].amount.toFixed(2))
            }

            callback(err,deals[0])
        } else {
            callback(err,{
                "_id": null,
                "date":  new Date(),
                "amount": 0,
                "count": 0
            })
        }
    })
}

function dealsAtRisk(emailId,primaryCurrency,currenciesObj,qStart,qEnd,callback) {

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMetaCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}).sort({date:-1}).limit(1).toArray(function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null;

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        el.convertedAmt = parseFloat(parseFloat(el.convertedAmt).toFixed(2))

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        }

        callback(err,{
            count:sumBy(deals,"count"),
            amount:totalDealValueAtRisk,
            dealsRiskAsOfDate:dealsRiskAsOfDate
        })
    })
}

function groupIntsByAccName(interactions,qStart,qEnd,userEmailIds,companyId,allQuarters,callback) {

    var newCompaniesInteracted = [];
    var accountNames = [];

    if(interactions.length>0){
        var intGroup = _
            .chain(interactions)
            .groupBy('accountName')
            .map(function(values, key) {

                values.sort(function (o1, o2) {
                    return o1.interactionDate > o2.interactionDate ? 1 : o1.interactionDate < o2.interactionDate ? -1 : 0;
                });

                accountNames.push(key);
                newCompaniesInteracted.push({
                    companyId: values[0].companyId,
                    ownerId: values[0].ownerId,
                    accountName:values[0].accountName,
                    monthYear:values[0].monthYear,
                    interactionDate:values[0].interactionDate,
                    ownerEmailId:values[0].ownerEmailId,
                    emailId:values[0].emailId
                });

                return {
                    usersWithAccess:userEmailIds,
                    axis:key,
                    value:values.length,
                    accountName: key,
                    interactionsCount: values.length,
                    lastInteractedDate: values[values.length-1].interactionDate
                }
            })
            .value();

        updateAccountInsightsWithInteractions({
            array:intGroup
        },companyId,qStart,qEnd,allQuarters,function () {

            newCompaniesInteraction.remove({ownerEmailId:{$in:userEmailIds},accountName:{$in:accountNames}},function () {
                newCompaniesInteraction.insert(newCompaniesInteracted,function (err,result) {
                   if(err){
                       console.log("-----newCompaniesInteraction err---")
                       console.log(err)
                   }

                   if(callback){
                       callback({newCompaniesInteracted:newCompaniesInteracted});
                   }
                });
            })
        })
    } else {

        if(callback){
            callback({newCompaniesInteracted:newCompaniesInteracted});
        }
    }
}

function groupOppsByAccName(opps,qStart,qEnd,companyId,forTeam,userEmailIds,allQuarters) {

    var oppClosingThisQtrAccs = [];

    var group = _
        .chain(opps)
        .groupBy('accountName')
        .map(function(values, key) {

            var oppAmt = 0,
                oppAmtQtr = 0,
                oppsThisQtr = [],
                oppsByAccAll = [];

            _.each(values,function (va) {
                oppAmt = oppAmt+va.convertedAmtWithNgm;
                va.createdDate = va.createdDate?va.createdDate:va._id.getTimestamp()
                oppsByAccAll.push({
                    stageName: va.stageName,
                    closeDate: va.closeDate,
                    createdDate: va.createdDate,
                    amount:va.convertedAmtWithNgm
                });

                if(va.stageName !== "Close Won" && va.stageName !== "Close Lost"){
                    if((new Date(va.closeDate)>= new Date(qStart)) && new Date(va.closeDate)<= new Date(qEnd)){
                        oppAmtQtr = oppAmtQtr+va.convertedAmtWithNgm;
                        oppsThisQtr.push({
                            stageName: va.stageName,
                            closeDate: va.closeDate,
                            createdDate: va.createdDate,
                            amount:va.convertedAmtWithNgm
                        })
                    }
                }
            });

            if(oppsThisQtr.length>0){
                oppClosingThisQtrAccs.push({
                    axis:key,
                    value:oppAmtQtr,
                    accountName: key,
                    oppsCount: oppsThisQtr.length,
                    oppsAmount: oppAmtQtr,
                    opps:oppsThisQtr,
                    usersWithAccess: userEmailIds
                })
            }

            var obj = {
                axis:key,
                value:oppAmt,
                accountName: key,
                oppsCount: values.length,
                oppsAmount: oppAmt,
                opps:oppsByAccAll,
                usersWithAccess: userEmailIds
            }

            return obj;
        })
        .value();

    if(forTeam){
        updateAccountInsightsWithOpps({array:group,thisQtr:oppClosingThisQtrAccs},companyId,allQuarters);
    }

    return {
        raw_opps:opps,
        array:group
    };
}

function currentInsights(opps,qStart,qEnd,oppIdIntsObj){

    var interactionsForWonOpp = 0,
        maxInteractionsForWonOpp = 0,
        daysToCloseOpp = 0,
        wonOpps = 0,
        oppsClosingThisQtr = 0,
        renewalOppsNext90Days = 0,
        renewalOppsAmountNext90Days = 0,
        staleOpps = 0,
        newOppsAdded = 0,
        wonAmount = 0,
        allDealSizes = [],
        intPerThousandAmountWon = [],
        allDealCloseDays = [];

    var threeMonthsEndDate = moment().add(90,"days");

    if(opps && opps.length>0){
        _.each(opps,function (op) {

            var oppCreatedDate = op.createdDate?op.createdDate:op._id.getTimestamp();

            if(new Date(oppCreatedDate)>= new Date(qStart) && new Date(oppCreatedDate) <= new Date(qEnd)) {
                newOppsAdded++
            }

            if(new Date(op.closeDate)>= new Date(qStart) && new Date(op.closeDate) <= new Date(qEnd)) {
                oppsClosingThisQtr++
            }

            if(new Date(op.closeDate)>= new Date() && new Date(op.closeDate)<= new Date(threeMonthsEndDate) && op.type && op.type.toLowerCase() == "renewal"){
                renewalOppsAmountNext90Days = renewalOppsAmountNext90Days+parseFloat(op.convertedAmtWithNgm);
                renewalOppsNext90Days++
            }

            if(op.stageName != "Close Won" && op.stageName != "Close Lost" && new Date(op.closeDate) < new Date(qStart)){
                staleOpps++
            }

            if(op.relatasStage === "Close Won" && new Date(op.closeDate)>= new Date(qStart) && new Date(op.closeDate) <= new Date(qEnd)){

                var allInteractions = 0;
                wonOpps++;

                wonAmount = wonAmount+op.convertedAmtWithNgm;

                var createCloseDiff = moment(op.closeDate).diff(moment(oppCreatedDate), 'days');

                if(createCloseDiff == 0){
                    createCloseDiff = 1;
                }

                allDealCloseDays.push(createCloseDiff)

                daysToCloseOpp = daysToCloseOpp+createCloseDiff;

                if(oppIdIntsObj[op.opportunityId]){
                    interactionsForWonOpp = oppIdIntsObj[op.opportunityId]
                    allInteractions = oppIdIntsObj[op.opportunityId]
                }

                if(allInteractions>maxInteractionsForWonOpp){
                    maxInteractionsForWonOpp = allInteractions
                }

                if(allInteractions && op.convertedAmtWithNgm){
                    var amount = op.convertedAmtWithNgm;
                    intPerThousandAmountWon.push(allInteractions/amount);
                }

                allDealSizes.push(op.convertedAmtWithNgm)
            }
        })
    }

    var avgDaysToCloseOpp = 0,
        avgDealSize = 0,
        avgInteractionsPerAmountWon = 0;

    if(daysToCloseOpp && wonOpps){
        avgDaysToCloseOpp = parseFloat((daysToCloseOpp/wonOpps).toFixed(2));
    }

    if(interactionsForWonOpp && wonAmount){
        avgInteractionsPerAmountWon = parseFloat(((interactionsForWonOpp/(wonAmount/1000))).toFixed(2));
    }

    if(wonAmount && wonOpps){
        avgDealSize = parseFloat((wonAmount/wonOpps).toFixed(2));
    }

    var minDaysToCloseOpp = _.min(allDealCloseDays)
    var maxDaysToCloseOpp = _.max(allDealCloseDays)

    return [{
            avgDealSize:avgDealSize,
            maxDealSize:_.max(allDealSizes),
            minDealSize:_.min(allDealSizes),
            staleOpps:staleOpps,
            wonAmount:wonAmount,
            minIntPerThousandAmountWon:_.min(intPerThousandAmountWon),
            maxIntPerThousandAmountWon:_.max(intPerThousandAmountWon),
            maxInteractionsForWonOpp:maxInteractionsForWonOpp,
            avgInteractionsPerAmountWon:avgInteractionsPerAmountWon,
            renewalOppsNext90Days:renewalOppsNext90Days,
            renewalOppsAmountNext90Days:renewalOppsAmountNext90Days,
            avgDaysToCloseOpp:avgDaysToCloseOpp,
            maxDaysToCloseOpp:maxDaysToCloseOpp?maxDaysToCloseOpp:1,
            minDaysToCloseOpp:minDaysToCloseOpp?minDaysToCloseOpp:1,
            wonOpps:wonOpps,
            interactionsForWonOpp:interactionsForWonOpp,
            daysToCloseOpp:daysToCloseOpp
        }]
}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

function setQuarter(startMonth,timezone,fyStartDate,fyEndDate) {

    var qtrObj = {};

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var months = [];
    months.push(fyStartDate)

    _.each(monthNames,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(moment(months[0]).startOf('month')).tz(timezone).format(),end:moment(moment(months[2]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter2 = {start:moment(moment(months[3]).startOf('month')).tz(timezone).format(),end:moment(moment(months[5]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter3 = {start:moment(moment(months[6]).startOf('month')).tz(timezone).format(),end:moment(moment(months[8]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter4 = {start:moment(moment(months[9]).startOf('month')).tz(timezone).format(),end:moment(moment(months[11]).endOf('month')).tz(timezone).format()}

    var currentMonth = moment().month()
    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    return qtrObj
}

function scaleBetween(unscaledNum, min, max,minAllowed,maxAllowed) {

    if(!minAllowed){
        minAllowed = 0;
    }

    if(!maxAllowed) {
        maxAllowed = 100;
    }

    if ((min == 0 && max == 0) || (min == max)) {
        return minAllowed
    } else {
        var val = (maxAllowed - minAllowed) * (unscaledNum - min) / (max - min) + minAllowed;

        if(!isNumber(val)){
            val = 0;
        }

        return val;
    }
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function numberWithCommas(x,ins) {

    if(ins){
        return numberWithCommas_ins(x);
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

function formatNumber(num) {

    if(num && num.toString().length>2){

        if(num % 1 != 0){
            return num.toFixed(2)
        } else {
            return num
        }
    } else {
        return num;
    }
}

function numberWithCommas_ins(x) {
    x=String(x).toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function checkRequired(data){
    if (data == '' || data == null || data == undefined || data == "undefined") {
        return false;
    }
    else{
        return true;
    }
}

