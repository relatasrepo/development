var gmailApi = require('node-gmail-api');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

var emailId = process.argv[2];
var dbUrl = process.argv[3];
var days = process.argv[4];

console.log("User Email ID is : " + emailId);
console.log("MongoDb URL is : " + dbUrl);
console.log("Fetching Subject for : " + days + " Days");

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connection Successful")
    if (err) {
        console.log(err)
    }

    db.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {

        var filter = '';
        var after = new Date();
        var before = new Date();
        var now = new Date();

        after.setDate(after.getDate() - days);
        before.setDate(before.getDate() + 2);
        var monthAfter = after.getMonth()+1;
        var monthBefore = before.getMonth()+1;
        filter = 'after:'+after.getFullYear()+'/'+monthAfter+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore+'/'+before.getDate();

        var collection = db.collection("user");
        collection.find({"emailId":emailId}).toArray(function(err, googleRefreshToken){
            getNewGoogleToken(googleRefreshToken[0].google[0].token,googleRefreshToken[0].google[0].refreshToken,function(token){
                console.log("==>Old Token is:")
                console.log(googleRefreshToken[0].google[0].token)
                console.log("==>New Token is:")
                console.log(token)
                var gmail = new gmailApi(token);
                var s = gmail.messages(filter);

                s.on('data', function (d) {
                    //console.log(JSON.stringify(d.payload.headers,null,4))
                    var email = {};
                    for(var i=0; i<d.payload.headers.length; i++){
                        if(d.payload.headers[i].name == 'From' || d.payload.headers[i].name == 'from'){
                            email.from = extractEmails(d.payload.headers[i].value);
                        }
                        if(d.payload.headers[i].name == 'To' || d.payload.headers[i].name == 'to'){
                            email.To = extractEmails(d.payload.headers[i].value);
                        }
                        if(d.payload.headers[i].name == 'Subject' || d.payload.headers[i].name == 'subject'){
                            email.subject = d.payload.headers[i].value;
                        }
                        if(d.payload.headers[i].name == 'Cc' || d.payload.headers[i].name == 'cc'){
                            email.Cc = extractEmails(d.payload.headers[i].value);
                        }
                        if(d.payload.headers[i].name == 'Bcc' || d.payload.headers[i].name == 'bcc'){
                            email.Bcc = extractEmails(d.payload.headers[i].value);
                        }
                        if(d.payload.headers[i].name == 'Date' || d.payload.headers[i].name == 'date'){
                            email.date = new Date(d.payload.headers[i].value);
                        }
                        if(d.payload.headers[i].name == 'Message-Id' || d.payload.headers[i].name == 'Message-ID'){
                            email.id = d.payload.headers[i].value;
                        }
                    }
                    console.log('\x1b[36m%s\x1b[0m',"------------END------------")

                    var isValid = true;
                    if(email.to && email.to.length > 0){
                        for(var to in email.to){
                            isValid = !matchInArray(email.to[to].split('@')[0])
                        }
                    }
                    if(email.from && email.from.length > 0){
                        for(var from in email.from){
                            isValid = !matchInArray(email.from[from].split('@')[0])
                        }
                    }
                    if(email.Cc && email.Cc.length > 0){
                        for(var Cc in email.Cc){
                            isValid = !matchInArray(email.Cc[Cc].split('@')[0])
                        }
                    }
                    if(email.Bcc && email.Bcc.length > 0){
                        for(var Bcc in email.Bcc){
                            isValid = !matchInArray(email.Bcc[Bcc].split('@')[0])
                        }
                    }
                    console.log(isValid ? email : null) ;
                })
            });
        })
    })
})


function matchInArray(stringSearch){
    var position = String(getInvalidEmailList()).search(stringSearch);
    return (position > -1);
}

function extractEmails (text)
{
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function getNewGoogleToken(token,refreshToken,callback){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
}

getInvalidEmailList = function(){
    return invalidEmails;
}

var invalidEmails = [
    /calendar-notification/i,
    /feedproxy/i,
    /techgig/i,
    /team/i,
    /blog/i,
    /info/i,
    /support/i,
    /admin/i,
    /hello/i,
    /no-reply/i,
    /noreply/i,
    /reply/i,
    /help/i,
    /mailer-daemon/i,
    /googlemail.com/i,
    /mail-noreply/i,
    /alert/i,
    /calendar-notification/i,
    /eBay/i,
    /flipkartletters/i,
    /pinterest/i,
    /dobambam.com/i,
    /notify/i,
    /offers/i,
    /iicicibank/i,
    /indiatimes/i,
    /info@relatas.in/i,
    /facebookmail/i,
    /message/i,
    /facebookmail.com/i,
    /notification/i,
    /youcanreply/i,
    /jobs/i,
    /news/i,
    /linkedin/i,
    /list/i,
    /updates/i,
    /verify/i
];