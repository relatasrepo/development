var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async')

var userColl;

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {
        console.log("client")
        if (error)
            throw error;
        else {
            // var db = client.db('newDb');
            var db = client.db('Relatas');
            // var db = client.db('exampledev');
            userColl = db.collection('user');

            var runForSingleCompany = function (skip) {
                userColl.find({emailId:'bharadwaj.nagendra@ivalue.co.in'},{contacts:1}).toArray(function (err, users) {
                    var bulk = userColl.initializeUnorderedBulkOp();

                    users[0].contacts.forEach(function(contact){
                        if (validateEmail(contact.personName)) {
                            if(contact.personEmailId && contact.personEmailId.toLowerCase() !== contact.personName.toLowerCase()) {
                                var updateObject = {"contacts.$.personName":contact.personEmailId};
                                bulk.find({_id:users[0]._id,"contacts.personEmailId": contact.personEmailId}).update({ $set: updateObject});
                            }
                        }
                    })

                    bulk.execute(function(err, result) {
                        if (err) {
                            console.log(err)
                        } else {
                            result = result.toJSON();
                        }
                        console.log(err)
                        console.log(result)
                    });
                });
            };

            runForSingleCompany(0);
        }
    });
}

startScript();

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
