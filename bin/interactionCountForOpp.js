var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var date = require('date-and-time');
var timediff = require('timediff');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;

var start = function() {

    mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
        // mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        // mongoClient.connect("mongodb://localhost/June28", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                console.log('authenticated')
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var opportunitiesCollection = db.collection('opportunities');

                var runForSingleUser = function (skip) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}).skip(0).limit(1).toArray(function (err, user) {
                    usersCollection.find({corporateUser: true}).skip(skip).limit(1).toArray(function (err, user) {

                        if(!err && user && user.length>0){
                            var userProfile = user[0];

                            updateStart(userProfile,interactionCollection,opportunitiesCollection,function (err,result) {

                                console.log("Completed >",userProfile.emailId,skip)
                                runForSingleUser(skip+1)
                            })

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

function updateStart(userProfile,interactionCollection,opportunitiesCollection,callback) {

    getOppById([userProfile._id],opportunitiesCollection,function (oErr,opps) {

        if(!oErr && opps && opps.length>0){

            var contactsAndOpp = [],oppGroup = [],allContacts = [],oppCreatedDates = [];

            _.each(opps,function (op) {

                var contacts = _.flatten(op.contacts)

                oppCreatedDates.push(op._id._id.getTimestamp());

                var obj = {
                    oppId:op._id._id,
                    contacts:contacts,
                    createdDate:op._id.createdDate,
                    closeDate:op._id.closeDate
                };

                var obj2 = {
                    oppId:op._id._id,
                    createdDate:op._id.createdDate,
                    closeDate:op._id.closeDate,
                    contacts:_.map(contacts,function (co) {
                        return {
                            emailId:co
                        }
                    })
                };

                allContacts.push(contacts);
                contactsAndOpp.push(obj);
                oppGroup.push(obj2);
            });

            var minDate = _.min(oppCreatedDates);

            getInteractionCount([userProfile._id],_.flatten(allContacts),minDate,new Date(),interactionCollection,function (err,interactions) {

                var data = _
                    .chain(interactions)
                    .groupBy('_id.emailId')
                    .map(function(value, key) {

                        var values = [];
                        _.each(value,function (va) {
                            values.push({
                                count:va.count,
                                date:va._id.day+'-'+va._id.month+'-'+va._id.year,
                                interactionDate:new Date(va._id.year, va._id.month-1, va._id.day)
                            })
                        });

                        return {
                            emailId:key,
                            data:values
                        }
                    })
                    .value();

                var intWithContactsDictionary = {};

                if(data.length>0){
                    _.each(data,function (el) {
                        intWithContactsDictionary[el.emailId] = el.data;
                    })
                }

                _.each(oppGroup,function (op) {

                    var oppDateCreatedDate = op.oppId.getTimestamp();
                    oppDateCreatedDate = new Date(oppDateCreatedDate);
                    var oppCloseDate = new Date(op.closeDate)

                    if(op.createdDate){
                        oppDateCreatedDate = new Date(op.createdDate)
                    }

                    _.each(op.contacts,function (co) {
                        var counter = 0;
                        var values = intWithContactsDictionary[co.emailId];


                        if(values && values.length>0){
                            _.each(values,function (va) {

                                if(new Date(va.interactionDate)>=oppDateCreatedDate && new Date(va.interactionDate)<=oppCloseDate){
                                    counter = counter+va.count
                                }
                            });
                        }

                        co.counter = counter
                    });
                });

                // callback()

                updateOppWithInteractions(oppGroup,opportunitiesCollection,function (updateErr,result) {

                    if(callback){
                        callback(updateErr,result);
                    }
                })
            });
        } else {
            callback()
        }
    })
}

function getOppById (userIds,opportunitiesCollection,callback) {

    var match = {
        userId:{$in:userIds},
        relatasStage:{$in:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id: {
                    _id:"$_id",
                    createdDate: "$createdDate",
                    closeDate: "$closeDate"
                },
                contacts:{
                    $addToSet:{
                        partners:"$partners.emailId",
                        decisionMakers:"$decisionMakers.emailId",
                        influencers:"$influencers.emailId",
                        contactEmailId:"$contactEmailId"
                    }
                }
            }
        },
        {
            $project:{
                _id:"$_id",
                contacts:{ "$setUnion": [ "$contacts.partners","$contacts.decisionMakers", "$contacts.influencers", "$contacts.contactEmailId" ] }
            }

        }
    ],function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

function getInteractionCount(userIds,contacts,from,to,InteractionsCollection,callback){

    var query = { "$or": userIds.map(function(el) {
        var obj = {};

        obj["ownerId"] = el
        obj["userId"] = {$ne:el}
        obj["emailId"] = {$in:contacts}
        obj["action"] = "receiver"

        obj["interactionDate"] = {
            $gte: new Date(from),
            $lte: new Date(to)
        }
        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match: query
        },
        {
            $project:{
                emailId:"$emailId",
                day:{$dayOfMonth:"$interactionDate"},
                month:{$month:"$interactionDate"},
                year:{$year:"$interactionDate"}
            }
        },
        {
            $group: {
                _id: {
                    emailId:"$emailId",
                    day:"$day",
                    month:"$month",
                    year:"$year"
                },
                count: {$sum: 1}
            }
        }
    ],function(err, data){
        callback(err,data)
    });
}

function updateOppWithInteractions(userGroup,opportunitiesCollection,callback) {

    if(userGroup.length>0){
        var bulk = opportunitiesCollection.initializeUnorderedBulkOp();

        _.each(userGroup,function (el) {

            _.each(el.contacts,function (co) {

                bulk.find({ _id: el.oppId,"contactEmailId": co.emailId}).update({ $set: {interactionCount:co.counter} });
                bulk.find({ _id: el.oppId,"partners.emailId": co.emailId}).update({ $set: {"partners.$.interactionCount":co.counter} });
                bulk.find({ _id: el.oppId,"decisionMakers.emailId": co.emailId}).update({ $set: {"decisionMakers.$.interactionCount":co.counter} });
                bulk.find({ _id: el.oppId,"influencers.emailId": co.emailId}).update({ $set: {"influencers.$.interactionCount":co.counter} });
            })
        });

        bulk.execute(function(err, result) {
            if (err) {
                console.log(err)
            } else {
                result = result.toJSON();
            }

            if (callback) { callback(err, result) }
        });
    }
}

start();