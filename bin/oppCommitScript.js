var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var redis = require('redis');
var oppCommitCollection;
var opportunitiesCollection;
var redisClient = redis.createClient();

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 6;
rule.minute = 59;
rule.second = 59;

var start = function() {

    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.21.21:27017/Relatas", function(error, client) { // This is LIVE
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
   // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var usersCollection = db.collection('user');
            var companyCollection = db.collection('company');
            oppCommitCollection = db.collection('oppCommits');
            opportunitiesCollection = db.collection('opportunities');

            var runForSingleUser = function (skip) {

                usersCollection.find({corporateUser: true},{contacts:0}).skip(skip).limit(1).toArray(function (err, user) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"},{contacts:0}).skip(skip).limit(1).toArray(function (err, user) {
                    if(!err && user && user[0]){
                        var userId = user[0]._id,
                            emailId = user[0].emailId,
                            timezone = user[0].timezone.name,
                            companyId = user[0].companyId;

                        getCompanyDetails(companyCollection,companyId,timezone,function (companyDetails) {
                            getFiscalYear(companyDetails, timezone, function (fyMonth, fyRange, allQuarters, timezone) {

                                var commitStage = getCommitStage(companyDetails.opportunityStages),
                                    weekCutOff = getWeeklyCutOffDate(companyDetails),
                                    monthCutOff = getMonthlyCutOffDate(companyDetails,timezone),
                                    qtrCutOff = getQuarterlyCutOffDate(companyDetails,allQuarters,timezone),
                                    todayString = new Date(moment().add(1,"day")).toDateString(); //compensating tz issues.

                                if(new Date(weekCutOff).toDateString() == todayString){
                                    startWeekly(user[0],companyDetails,timezone,commitStage,fyMonth, fyRange, allQuarters,function () {
                                        console.log("Completed!")
                                    })
                                } else {
                                    console.log("Weekly Already Done!")
                                }

                                if(new Date(monthCutOff).toDateString() == todayString){
                                    startMonthly(user[0],companyDetails,timezone,commitStage,fyMonth, fyRange, allQuarters,function () {
                                        console.log("Completed!")
                                    })
                                } else {
                                    console.log("Monthly Already Done!")
                                }

                                if(new Date(qtrCutOff).toDateString() == todayString){
                                    startQuarterly(user[0],companyDetails,timezone,commitStage,fyMonth, fyRange, allQuarters,function () {
                                        console.log("Completed!")
                                    })
                                } else {
                                    console.log("Qtrly Already Done!")
                                }

                                console.log(todayString)
                                console.log(new Date(qtrCutOff).toDateString())
                                console.log(new Date(monthCutOff).toDateString())

                                runForSingleUser(skip+1)

                            })
                        })

                    } else {
                        console.log("Done!")
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

// start();

initScheduler()

function initScheduler() {

    var sendDailyAgendaRuleZone4 = new schedule.RecurrenceRule(); //5:00 PM IST
    sendDailyAgendaRuleZone4.dayOfWeek = [2,5]
    sendDailyAgendaRuleZone4.hour = 12;  // 12:30 UTC
    sendDailyAgendaRuleZone4.minute = 30;

    var sendDailyAgendaRuleObjectZone4 = schedule.scheduleJob(sendDailyAgendaRuleZone4, function(){
        logger.info('Agenda mail Kicked off for zone FOUR');
        start();
    });

}

function getCompanyDetails(companyCollection,companyId,timezone,callback) {

    var redisKey = "companyDetails" + String(companyId);

    getFromCache(redisKey, function (companyDetails){
        if(companyDetails){
            callback(companyDetails)
        } else {
            companyCollection.find({_id:companyId}).toArray(function (err, companyDetails) {
                if(!err && companyDetails && companyDetails[0]){
                    setCache(redisKey,companyDetails[0])
                }
                callback(companyDetails)
            })
        }
    });

}

function getCommitStage(oppStages) {
    var commitStage = "Proposal"; // default.

    if (oppStages) {
        _.each(oppStages, function (st) {
            if (st.commitStage) {
                commitStage = st.name;
            }
        });
    }

    return commitStage
}

function startWeekly(userProfile,companyDetails,timezone,commitStage,fyMonth, fyRange, allQuarters,callback) {
    var weekStartDateTime = getCommitDayTimeEnd(companyDetails);
    var nextWeekMoment = weekStartDateTime;
    var commitWeekYear = moment(nextWeekMoment).week()+""+moment(nextWeekMoment).year()

    updateCommitWeekly(userProfile._id,commitWeekYear,commitStage,fyRange.start,fyRange.end,timezone,nextWeekMoment,callback)
}

var updateCommitWeekly = function (userId,weekYear,commitStage,qStart,qEnd,timezone,commitDate,callback) {

    getOppByStageSnapShot(userId,qStart,qEnd,commitStage,function (err,opps) {

        _.each(opps,function (op) {

            if(op.opps && op.opps.length>0){
                _.each(op.opps,function (el) {
                    if(!el.createdDate){
                        el.createdDate = el._id.getTimestamp()
                    }
                })
            }
        });

        var data = {
            opportunities: opps,
            week: {
                userCommitAmount: 0,
                relatasCommitAmount: 0
            },
            month: {
                userCommitAmount: 0,
                relatasCommitAmount: 0
            },
            quarter: {
                userCommitAmount: 0,
                relatasCommitAmount: 0
            },
            commitForDate:commitDate,
            commitFor:"week"
        }

        var findQuery = {
            userId:userId,
            commitWeekYear:weekYear
        };

        oppCommitCollection.findOne(findQuery,{_id:1,userId:1},function (err,existingCommit) {

            if(!err && existingCommit){
                var updateOpps = {
                    opportunities: opps
                }

                oppCommitCollection.update(findQuery,{$set:updateOpps},function (err,result) {
                    callback(err,opps)
                })
            } else {
                insertCommit(userId,weekYear,data,callback)
            }
        })
    })
}

function startMonthly(userProfile,companyDetails,timezone,commitStage,fyMonth, fyRange, allQuarters,callback) {

    var monthCommitCutOff = 10

    if(companyDetails && companyDetails.monthCommitCutOff){
        monthCommitCutOff = companyDetails.monthCommitCutOff
    }

    var startOfMonth = moment(moment().startOf('month')).add(monthCommitCutOff,"day");

    if(new Date()> new Date(startOfMonth)){ // Cutofftime
        startOfMonth = moment(startOfMonth).add(1,'month')
    }

    var endOfMonth = moment(startOfMonth).add(1,'month'),
        monthYear = moment(startOfMonth).month()+""+moment(startOfMonth).year();

    updateCommitMonth(userProfile._id,userProfile.emailId,monthYear,commitStage,startOfMonth,endOfMonth,timezone,startOfMonth,companyDetails.netGrossMargin,callback)
}

function startQuarterly(userProfile,companyDetails,timezone,commitStage,fyMonth, fyRange, allQuarters,callback) {
    var qtrCommitCutOff = 15

    if(companyDetails && companyDetails.qtrCommitCutOff){
        qtrCommitCutOff = companyDetails.qtrCommitCutOff
    }

    var startOfQuarter = allQuarters[allQuarters.currentQuarter].start,
        endOfQuarter = allQuarters[allQuarters.currentQuarter].end;

    startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")
    endOfQuarter = moment(endOfQuarter).tz(timezone).add(15,"day")

    if(qtrCommitCutOff){
        startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")
        endOfQuarter = moment(allQuarters[allQuarters.currentQuarter].end).tz(timezone).add(qtrCommitCutOff,"day")
    }

    if(new Date()> new Date(startOfQuarter)){ // Cutofftime
        startOfQuarter = moment(startOfQuarter).add(3,'month')
        endOfQuarter = moment(endOfQuarter).add(3,'month')
    }

    var quarterYear =  getQuarterYear(allQuarters,startOfQuarter,fyMonth,timezone,fyRange.start);
    updateCommitQuarter(userProfile._id,userProfile.emailId,commitStage,startOfQuarter,endOfQuarter,timezone,startOfQuarter,companyDetails.netGrossMargin,quarterYear,callback)
}

var updateCommitMonth = function (userId,userEmailId,monthYear,commitStage,startOfMonth,endOfMonth,timezone,commitDate,netGrossMarginReq,callback) {

    getOppByInCommitStageByDateRange(userId,commitStage,null,null,netGrossMarginReq,function (err,opps) {

        var data = {
            opportunities: opps,
            date: new Date(),
            commitWeekYear: null,
            monthYear: monthYear,
            commitForDate: new Date(commitDate),
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userId,
            commitFor:'month',
            userEmailId:userEmailId
        }

        var findQuery = {
            userId:userId,
            monthYear:monthYear
        };

        oppCommitCollection.findOne(findQuery,{_id:1,userId:1},function (err,commitMade) {

            if(!err && commitMade){
                var updateOpps = {
                    opportunities: opps
                }
                oppCommitCollection.update(findQuery,{$set:updateOpps},function (err,result) {
                    callback()
                })
            } else {
                oppCommitCollection.insert([data],function (err,resultInsert) {
                    callback()
                })
            }
        })
    })
}

var updateCommitQuarter = function (userId,userEmailId,commitStage,dateMin,dateMax,timezone,commitDate,netGrossMarginReq,quarterYear,callback) {

    getOppInCommitStage(userId,commitStage,dateMin,dateMax,netGrossMarginReq,function (err,opps) {

        var data = {
            opportunities: opps,
            date: new Date(),
            commitWeekYear: null,
            monthYear: null,
            quarterYear:quarterYear,
            commitForDate: new Date(dateMin),
            week:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            month:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            quarter:{
                userCommitAmount:0,
                relatasCommitAmount:0
            },
            userId: userId,
            commitFor:'quarter',
            userEmailId:userEmailId
        }

        var findQuery = {
            userId:userId,
            quarterYear:quarterYear
        };

        oppCommitCollection.findOne(findQuery,{_id:1,userId:1},function (err,commitMade) {
            var updateOpps = {
                opportunities: opps
            }

            if(!err && commitMade){
                oppCommitCollection.update(findQuery,{$set:updateOpps},function (err,result) {
                    callback()
                })
            } else {
                oppCommitCollection.insert([data],function (err,resultInsert) {
                    callback()
                })
            }
        })
    })
}

function insertCommit(userId,weekYear,updateObj,callback) {

    updateObj.userId = userId;
    updateObj.commitWeekYear = weekYear;
    updateObj.date = new Date();

    oppCommitCollection.insert(updateObj,function (err,resultInsert) {
        callback()
    })
}

function getOppByStageSnapShot(userId,qtrStart,qtrEnd,commitStage,callback){

    var match = {
        userId:userId,
        $or:[{relatasStage:commitStage},{stageName:commitStage}]
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:"$relatasStage",
                opps:{
                    $push:{
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "relatasStage": "$relatasStage",
                        "amount": "$amount",
                        "opportunityId": "$opportunityId",
                        "closeDate": "$closeDate",
                        "geoLocation": "$geoLocation",
                        "productType": "$productType",
                        "vertical": "$vertical",
                        "netGrossMargin": "$netGrossMargin",
                        "userEmailId": "$userEmailId",
                        "createdDate": "$createdDate"
                    }
                },
                totalAmount:{$sum:"$amount"}
            }
        }
    ]).toArray(function (err,opps) {
        callback(err,opps)
    })
}

function getOppByInCommitStageByDateRange(userId,commitStage,dateMin,dateMax,netGrossMarginReq,callback){

    var match = {
        userId:userId,
        $or:[{relatasStage:commitStage},{stageName:commitStage}]
    }

    opportunitiesCollection.aggregate([{
        $match:match
    }]).toArray(function (err,opps) {
        var opportunities = []
        if(!err && opps){
            if(netGrossMarginReq){
                opportunities  = calculateNGM(opps)
            } else {
                opportunities = opps;
            }
        }
        callback(err,opportunities)
    })
}

function getOppInCommitStage(userId,commitStage,dateMin,dateMax,netGrossMarginReq,callback){

    var match = {
        userId:userId,
        $or:[{relatasStage:commitStage},{stageName:commitStage}]
    }

    opportunitiesCollection.aggregate([{
        $match:match
    }]).toArray(function (err,opps) {
        callback(err,opps)
    })
}

function getFiscalYear(company,timezone,callback){

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        if (!company || !company.fyMonth) {
            var fyMonth = "April"; //Default
        } else {
            fyMonth = company.fyMonth
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if (currentMonth < monthNames.indexOf(fyMonth)) {
            fromDate.setFullYear(currentYr - 1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month');

        if(!timezone){
            timezone = "Asia/Kolkata"
        }

        var obj = {
            fyMonth: fyMonth,
            startEnd: {start: moment(fromDate).tz(timezone).format(), end: moment(toDate).tz(timezone).format()},
            quarters: setQuarter(fyMonth, timezone,fromDate),
            timezone: timezone
        }

        callback(
            company.fyMonth,
            {start: moment(fromDate).tz(timezone).format(), end: moment(toDate).tz(timezone).format()},
            setQuarter(fyMonth, timezone,fromDate),
            timezone)
}

function setQuarter(startMonth,timezone,fyStartDate,fyEndDate) {

    var qtrObj = {};

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November"];

    var months = [];
    months.push(fyStartDate)

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(moment(months[0]).startOf('month')).tz(timezone).format(),end:moment(moment(months[2]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter2 = {start:moment(moment(months[3]).startOf('month')).tz(timezone).format(),end:moment(moment(months[5]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter3 = {start:moment(moment(months[6]).startOf('month')).tz(timezone).format(),end:moment(moment(months[8]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter4 = {start:moment(moment(months[9]).startOf('month')).tz(timezone).format(),end:moment(moment(months[11]).endOf('month')).tz(timezone).format()}

    var currentMonth = moment().month()
    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    return qtrObj
}

function setCache(redisKey,data){
    redisClient.setex(redisKey, 1800, JSON.stringify(data));
}

function getFromCache(redisKey,callback) {
    redisClient.get(redisKey, function (error, cacheResult) {
        try {
            var data = JSON.parse(cacheResult);
            callback(data)
            // callback(null)
        } catch (err) {
            callback(null)
        }
    });
}

var calculateNGM = function(opps,ping) {

    var opportunitiesList = [];

    _.each(opps,function (op) {

        op = JSON.parse(JSON.stringify(op))

        op.amountNonNGM = op.amount;

        if(op.netGrossMargin){
            var amountWithNGM = op.netGrossMargin*op.amount

            if(amountWithNGM){
                amountWithNGM = amountWithNGM/100
            }

            op.amount = amountWithNGM;
        }

        opportunitiesList.push(op)
    })

    opps = opportunitiesList;
    return opps;
}

var getQuarterYear = function (allQuarters,startOfQuarter,fyMonth,timezone,fyStartDate) {
    var qtrString = getQtrString(allQuarters,startOfQuarter,timezone,fyStartDate);
    return qtrString+""+moment(startOfQuarter).year();
}

function getQtrString(allQuarters,startOfQuarter,timezone,fyStartDt){

    var prevQtrs = buildQuarters(moment(fyStartDt).subtract(1,"year"),timezone);
    var nextQtrs = buildQuarters(moment(fyStartDt).add(1,"year"),timezone);

    var qtrString = null;
    for( var key in allQuarters){
        if(new Date(startOfQuarter)>=new Date(allQuarters[key].start) && new Date(startOfQuarter)<=new Date(allQuarters[key].end)){
            qtrString = key
        }
    }

    //If not found in the current quarter, find in the next qtr
    if(!qtrString){
        for( var key in nextQtrs){
            if(new Date(startOfQuarter)>=new Date(nextQtrs[key].start) && new Date(startOfQuarter)<=new Date(nextQtrs[key].end)){
                qtrString = key
            }
        }
    }

    //If not found in the current and next quarter, find in the prev qtr
    if(!qtrString){
        for( var key in prevQtrs){
            if(new Date(startOfQuarter)>=new Date(prevQtrs[key].start) && new Date(startOfQuarter)<=new Date(prevQtrs[key].end)){
                qtrString = key
            }
        }
    }

    return qtrString;
}

function buildQuarters(fyStartDate,timezone) {

    var quarters = [],
        twelve_months_for_loop = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November"],
        months = [],
        qtrYears = [],
        qtrObj = {};

    months.push(fyStartDate);

    _.each(twelve_months_for_loop,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    quarters.push({start:moment(months[0]).tz(timezone).startOf('month').format(),end:moment(months[2]).tz(timezone).endOf('month').format(),qtr:"quarter1"})
    quarters.push({start:moment(months[3]).tz(timezone).startOf('month').format(),end:moment(months[5]).tz(timezone).endOf('month').format(),qtr:"quarter2"})
    quarters.push({start:moment(months[6]).tz(timezone).startOf('month').format(),end:moment(months[8]).tz(timezone).endOf('month').format(),qtr:"quarter3"})
    quarters.push({start:moment(months[9]).tz(timezone).startOf('month').format(),end:moment(months[11]).tz(timezone).endOf('month').format(),qtr:"quarter4"})

    _.each(quarters,function (el) {
        qtrObj[el.qtr] = el
    });

    return qtrObj;
}

function getCommitDayTimeEnd(companyDetails,timezone) {

    var dateTime = moment().day("Monday");
    if(companyDetails && companyDetails.commitDay){
        dateTime = moment().day(companyDetails.commitDay)
    }

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    dateTime = moment(dateTime).tz(timezone)

    if(companyDetails && companyDetails.commitHour){
        dateTime = dateTime.hour(parseInt(companyDetails.commitHour))
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    } else {
        dateTime = dateTime.hour(18)
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    }

    if(new Date(dateTime)< new Date()){
        dateTime = moment(dateTime).add(1,'week') //well past commit time
    }

    return new Date(dateTime);
}

function getQuarterlyCutOffDate(companyDetails,allQuarters,timezone){
    var qtrCommitCutOff = 15

    if(companyDetails && companyDetails.qtrCommitCutOff){
        qtrCommitCutOff = companyDetails.qtrCommitCutOff
    }

    var startOfQuarter = allQuarters[allQuarters.currentQuarter].start;
    startOfQuarter = moment(startOfQuarter).tz(timezone).add(15,"day")

    if(qtrCommitCutOff){
        startOfQuarter = moment(allQuarters[allQuarters.currentQuarter].start).tz(timezone).add(qtrCommitCutOff,"day")
    }

    return new Date(startOfQuarter)

}

function getMonthlyCutOffDate(companyDetails,timezone){
    var monthCommitCutOff = 10

    if(companyDetails && companyDetails.monthCommitCutOff){
        monthCommitCutOff = companyDetails.monthCommitCutOff
    }

    return new Date(moment(moment().startOf('month')).add(monthCommitCutOff,"day").tz(timezone));

}

function getWeeklyCutOffDate(companyDetails){
    return getCommitDayTimeEnd(companyDetails)

}
