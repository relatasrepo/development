var mongoose = require('mongoose');
var schedule = require('node-schedule');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;

var userCollection = require('../databaseSchema/userManagementSchema').User;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;
var taskCollection = require('../databaseSchema/taskSchema').tasks;
var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions
var scheduleInvitation = require('../databaseSchema/userManagementSchema').scheduleInvitation;
var todayInsightsCollection = require('../databaseSchema/userManagementSchema').todayInsights;
var dealsAtRiskMetaCollection =  require('../databaseSchema/userManagementSchema').dealsAtRiskMeta;

var googleCalendarAPI = require('../common/googleCalendar');
var ContactManagement = require('../dataAccess/contactManagement');
var outlookSync =  require('../routes/office365_bip.js');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var _ = require("lodash")
var moment = require("moment")

var ImitateLoginProcessObj = new googleCalendarAPI();
var emailSenders = new emailSender();
var outlookSyncObj = new outlookSync();
var contactManagementObj = new ContactManagement();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();

var bipExceptionEmailList = [
    "kemparaju@relatas.com"
]

var database;

if (appCredential.getServerEnvironment() == 'LOCAL' || appCredential.getServerEnvironment() == 'DEV' || appCredential.getServerEnvironment() == 'LIVE') {

    bipExceptionEmailList = [
        "kemparaju@relatas.com"
    ]

    var dbPasswords = appCredential.getDBPasswords();
    mongoose.connect(dbPasswords.relatasDB.dbConnectionUrl, {user: dbPasswords.relatasDB.user, pass: dbPasswords.relatasDB.password, auth: { authdb: 'Relatas' }},function(error){

        if(error)
            emailSenders.sendUncaughtException(error, JSON.stringify(error), "error", appCredential.getServerEnvironment() + ": BIP",bipExceptionEmailList);
    });

    // initScheduler();
}
else {
    mongoose.connect("mongodb://localhost/Relatas?poolSize=100", function(error, response) {

        if(!error) {
            console.log("Connecting to database");

            database = client.db('Relatas');
            initScheduler();
        }
    });
    // mongoose.connect("mongodb://172.31.21.21:27017/Relatas?poolSize=100",{useMongoClient:true}); //LIVE
}

process.on('uncaughtException', function (err) {
    console.log('uncaughtException: message: ' + err.message + ' stack: ' + JSON.stringify(err));
    emailSenders.sendUncaughtException(err,err.message,err.stack,appCredential.getServerEnvironment()+": BIP",bipExceptionEmailList);
});

runForSingleUser(0);

function runForSingleUser(skip) {

    var project = { contacts:0};
    var skip_limit = {skip:skip,limit:1};
    var notMatch = {'$regex' : '^((?!'+'22by7'+').)*$'}

    // userCollection.findOne({},project,skip_limit).lean().exec(function(err, userProfile) {
    // userCollection.findOne({emailId:'naveenpaul.markunda@gmail.com'},project,skip_limit).lean().exec(function(err, userProfile) {
    userCollection.findOne({emailId:'kemparajutest@outlook.com'},project,skip_limit).lean().exec(function(err, userProfile) {
    // userCollection.findOne({corporateUser:true,zoneNumber:zoneNumber},project,skip_limit).lean().exec(function(err, userProfile) {
    // userCollection.findOne({corporateUser:true,emailId:notMatch},project,skip_limit).lean().exec(function(err, userProfile) {

        // var userObj = JSON.parse(JSON.stringify(userProfile))
        var userObj = userProfile;
        var userId = castToObjectId(userObj._id);
        var userEmailId = [];
        userEmailId.push(userObj.emailId);

        // getStaleOpportunities(userId, function(error, result) {
        //     console.log("Stale opportunity:", result);

            // var todayInsight = new todayInsightsCollection({
            //     userId: userId,
            //     staleOppsCount:Number(result[0].count),
            //     recommendedToMeetCount:Number(result[0].count),
            //     UpcomingMeetinsCount:Number(result[0].count),
            //     upcomingTasksCount:Number(result[0].count),
            //     overdueTasksCount:Number(result[0].count),
            //     dealsAtRiskCount:Number(result[0].count),
            //     impMailsResponseCount:Number(result[0].count),
            //     date: new Date()
            // }); 

            // todayInsight.save(function(error, result) {
            //     console.log("Saved successfully!!");
            // });
        // });

        // getTasksOverdue(userEmailId, function(error, tasks) {
        //     console.log("Task result:", tasks.length);
        // })

        getTasksUpcoming(userEmailId, function(error, tasks) {
            console.log("Upcoming tasks:", tasks.length);
        })

        // getImpMailPendingResponse(userId, function(response) {
        //     console.log("Response from imp mails:", response);
        // })

        // getUpcomingMeetings(userObj._id, userProfile.timezone, userProfile.serviceLogin, function(meetings) {
        //     console.log("Meetings:", meetings);
        // });
    });
};

function getStaleOpportunities(userId, callback) {
    var match = {
        userId:userId,
        closeDate:{$lte:new Date()},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:null,
                count:{$sum:1}
            }
        }
    ],function(error, result){
        if(error){
            console.log("Error in stale opps:", error);
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
}

function getTasksOverdue(emailId, callback) {
    var query = {
        assignedToEmailId:{$in:emailId}
    };

    query.dueDate = {
        $lt: new Date()
    }

    query.complete = {$nin:['complete']}

    taskCollection.find(query,null,function(err,tasks){
        if(err){
            console.log("Error in getting task");
            callback(err, null);
        }
        callback(err,tasks)
    })
}

function getTasksUpcoming(emailId, callback) {
    var query = {
        assignedToEmailId:{$in:emailId},
        dueDate: {$gt: new Date()},
    }

    taskCollection.find(query, null, function(err, tasks) {
        if(err) {
            console.log("error in getting upcoming tasks");
            callback(err, null);
        }
        callback(err, tasks);
    })
}

function getImpMailPendingResponse(userId, callback) {
    var from  = moment().subtract(14, "days");

    getImportantMails(userId, from, function(err, mails) {
        if(!err && mails && mails.length>0){
            var filteredMails = filterByOnlyRecentInteractionThread(mails)

            var threadIds = _.pluck(filteredMails,"emailThreadId");

            var threadsHaveLatestReplies = [],tempMails = [];

            checkLiuRespondedToLastEmail(userId,from,threadIds,function (err,prevEmails) {

                var important = 0,onlyToMe = 0,followUp = 0,positive = 0,negative = 0;
                var emailContentIds = [];
                var response = {};

                _.each(prevEmails,function (el) {
                    if(el.emailId != el.ownerEmailId && el.action == "receiver" ){
                        threadsHaveLatestReplies.push(el.emailThreadId)
                    }
                })

                _.each(filteredMails,function (el) {
                    if(!_.includes(threadsHaveLatestReplies,el.emailThreadId)){
                        tempMails.push(el)
                    }
                })

                _.each(tempMails,function (el) {

                    if((el.eAction && el.eAction == 'important') || el.importance>0){
                        important++;
                        emailContentIds.push(el.emailContentId);
                    }

                    response.important = important;
                    response.contentIds = emailContentIds

                    callback(response);
                })

            });
        }
    })
}

function getImportantMails(userId,from, callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId:userId,
                userId:{$ne: userId},
                interactionDate:{$gte:new Date(from),$lte:new Date()},
                $or:[
                    {toCcBcc: 'me'},
                    {eAction: 'important'},
                    {importance: {'$gt': 0}},
                    // {"trackInfo.trackResponse":true}
                    {$and:[{"trackInfo.trackResponse":true},{createdDate:{$lte:new Date(moment().subtract(7, "days").toDate())}}]}
                ],
                notImportant:{$ne:true},
                hasUnsubscribe:{$ne:true}
            }
        },
        {
            $project:{
                importance:1,
                toCcBcc:1,
                sentiment:1,
                trackInfo:1,
                interactionDate:1,
                emailThreadId:1,
                title:1,
                action:1
            }
        }
    ]).exec(function(error, interactions){
        callback(error, interactions);
    })
}

function filterByOnlyRecentInteractionThread(interactions){
    var filteredInteractions = _
        .chain(interactions)
        .groupBy('emailThreadId')
        .map(function(value, key) {
            return _.max(value,"interactionDate")
        })
        .value();

    return filteredInteractions;
}

function checkLiuRespondedToLastEmail(userId,from,emailThreadIds,callback) {

    InteractionsCollection.aggregate(
        [{
            $match: {
                ownerId:userId,
                userId:{$ne:userId},
                emailThreadId:{$in:emailThreadIds},
                interactionDate:{$gte:new Date(from),$lte:new Date()},
                interactionType:"email"
            }
        },
        {
            $group:{
                _id:"$emailThreadId",
                interactions:{
                    $push:{
                        ownerEmailId:"$ownerEmailId",
                        emailId: "$emailId",
                        interactionDate: "$interactionDate",
                        emailThreadId: "$emailThreadId",
                        action: "$action",
                        title: "$title",
                        cc:"$cc",
                        to:"$to"
                    }
                }
            }
        }
    ]).exec(function(error, interactions){
        if(!error && interactions){
            getLatestMailInThread(interactions,callback)
        }
    })
}

function getLatestMailInThread(interactions,callback) {

    var latestReplies = [];
    _.each(interactions,function (intThread) {
        intThread.interactions.sort(function (o2, o1) {
            return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
        });

        if(intThread.interactions[0] && (intThread.interactions[0].to && intThread.interactions[0].to.length == 1 || intThread.interactions[0].cc && intThread.interactions[0].cc.length == 1)){
            latestReplies = latestReplies.concat(intThread.interactions[0])
        } else {
        }
    })

    callback(null,latestReplies)
}

function getUpcomingMeetings(userId, zone, findOutlookOrGoogle, callback) {
    var timezone;
        if(checkRequired(zone) && checkRequired(zone.name)){
            timezone = zone.name;
        }else timezone = 'UTC';

        var date = moment().add(1, "days");

        if(checkRequired(date)){
            date = new Date(date).toISOString();
        }
        //emailSenderObj.sendUncaughtException('',date,JSON.stringify(req.body),'TODAY_MEETINGS 2 '+user.emailId)
        var dateMin = checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
        var dateMax = checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

        dateMin.date(dateMin.date())
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        dateMax.date(dateMax.date())
        dateMax.hour(23)
        dateMax.minute(59)
        dateMax.second(59)
        dateMax.millisecond(59)

        dateMin = dateMin.format();
        dateMax = dateMax.format();

        getPendingMeetingsByDate(userId,dateMin,dateMax,findOutlookOrGoogle,function(todayMeetings){
            callback(todayMeetings);
        });

}

function getPendingMeetingsByDate(rId,dateMin,dateMax,findOutlookOrGoogle,callback){
    var projection = {senderId:1,senderName:1,senderEmailId:1}
    dateStart = new Date(dateMin);
    dateEnd = new Date(dateMax);

    if(findOutlookOrGoogle == 'outlook'){
        userCollection.findOne({_id:castToObjectId(rId)},{emailId:1}).exec(function(err, user){
            if(err){
                logger.info('Error in  userMeetingsByDate(): ',err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                deleted: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                senderId: rId,
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            }
                        ]
                    },
                    projection,
                    function(error,invitations){
                        if(error){
                            logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        })

        } else {

            scheduleInvitation.find({
                    isGoogleMeeting:true,
                    $or: [
                        {
                            "to.receiverId":rId,
                            "to.canceled":{$ne: true},
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            deleted: {$ne: true},
                            toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            senderId: rId,
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}},
                        }
                    ]
                },
                projection,
                function(error,invitations){
                    if(error){
                        logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                    }
                    callback(invitations);
                });
    }

    // userMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,findOutlookOrGoogle,function(meetings){
    //     if(common.checkRequired(meetings) && meetings.length > 0){
    //         callback(meetings)
    //     }else callback([])
    // })
}

function userMeetingsByDate(rId,dateStart,dateEnd,projection,findOutlookOrGoogle,callback) {

    if(findOutlookOrGoogle == 'outlook'){
        userCollection.findOne({_id:castToObjectId(rId)},{emailId:1}).exec(function(err, user){
            if(err){
                logger.info('Error in  userMeetingsByDate(): ',err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                deleted: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                senderId: rId,
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            }
                        ]
                    },
                    projection,
                    function(error,invitations){
                        if(error){
                            logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        })

        } else {

            scheduleInvitation.find({
                    isGoogleMeeting:true,
                    $or: [
                        {
                            "to.receiverId":rId,
                            "to.canceled":{$ne: true},
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            deleted: {$ne: true},
                            toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            senderId: rId,
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}},
                        }
                    ]
                },
                projection,
                function(error,invitations){
                    if(error){
                        logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                    }
                    callback(invitations);
                });
    }
};

function dealsAtRisk(emailId,primaryCurrency,currenciesObj,callback) {

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMetaCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}).sort({date:1}).toArray(function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null;

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        }

        callback(err,{
            count:sumBy(deals,"count"),
            amount:totalDealValueAtRisk,
            dealsRiskAsOfDate:dealsRiskAsOfDate
        })
    })
}

function initScheduler() {

    var sendDailyAgendaRuleZone1 = new schedule.RecurrenceRule(); //00:45 AM IST

    sendDailyAgendaRuleZone1.dayOfWeek = [0,new schedule.Range(1, 6)];
    sendDailyAgendaRuleZone1.hour = 23;  //19:15 UTC
    sendDailyAgendaRuleZone1.minute = 55;

    var sendDailyAgendaRuleObjectZone1 = schedule.scheduleJob(sendDailyAgendaRuleZone1, function(){
        console.log('Agenda mail Kicked off for zone ONE');
        runForSingleUser(0,1);
    });

}

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id);
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined || data == "undefined") {
        return false;
    }
    else{
        return true;
    }
}
