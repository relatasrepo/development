var mongoClient = require('mongodb').MongoClient;
var _ = require('lodash');
var async = require('async');

var companyCollection;
var opportunitiesCollection;
var bulkOppCollection;

function updateLocation() {

    connectToDb(function(db) {
        companyCollection = db.collection('company');
        opportunitiesCollection = db.collection('opportunities');
        bulkOppCollection = opportunitiesCollection.initializeUnorderedBulkOp();

        companyCollection.findOne({companyName:"Example Development"}, {_id:1}, function(error1, company) {
            if(!error1 && company) {
                opportunitiesCollection.find({companyId:company._id}, {_id:1, opportunityName:1, geoLocation:1}).toArray(function(error2, opportunities) {
                    
                    if(!error2 && opportunities.length > 0) {
                        _.each(opportunities, function(opp) {
                            var  town = opp.geoLocation ? opp.geoLocation.town : null;
                            var locArr = [];
    
                            if(town) {
                                locArr = town.replace(',', ' ').split(' ');
                                town = _.includes(['new', 'greater', 'mount', 'hong', 'old', 'san', 'madhya'], locArr[0].toLowerCase()) ? getUniqCityName(locArr[0] + ' ' + locArr[1]) : getUniqCityName(locArr[0]);
                                
                                console.log("Updating location for opp:", opp.opportunityName);
                                bulkOppCollection.find({_id: opp._id, companyId:company._id}).update({$set:{"geoLocation.town":town}});
                            }
                        });

                        bulkOppCollection.execute(function(err, result) {
                            console.log("**************** Done ********************");                                                    
                        })
                    } else {
                        console.log("**************** Done ********************");                       
                    }
                });
            } else {
                console.log("Error in finding the company")
            }
        })
    })

}

updateLocation();

function connectToDb(callback) {
    // mongoClient.connect("mongodb://reladmin:Hennur123@mongodb://10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //mongoClient.connect("mongodb://localhost", function(error, client) {
        if(error) {
            throw error
        } else {
            var db = client.db('Relatas');
            callback(db);
        }
    })
} 

function getUniqCityName(cityName) {
    var lCityName = cityName.toLowerCase();

    var cities = {
        "bangalore": "Bengaluru",
        "bengaluru": "Bengaluru",
        "banagalore": "Bengaluru",
        "bangalor": "Bengaluru",
        "mysore": "Mysuru",
        "new delhi": "Delhi",
        "greater noida": "Noida",
        "noida": "Noida",
        "cochin": "Kochi",
        "mangalore": "Mangaluru",
        "gurugram": "Gurgaon",
        "madras": "Chennai"
    };

    return cities[lCityName] ? cities[lCityName] : cityName;
}