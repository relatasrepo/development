var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;

var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

var emailId = process.argv[2];
var dbUrl = process.argv[3];

console.log("User Email ID is : " + emailId);
console.log("MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connect")
    if(err){
        console.log(err)
    }

    db.authenticate(dbPasswords.relatasDB.user,dbPasswords.relatasDB.password, function (err, res) {

        var collection = db.collection("user");
        collection.find({"emailId":emailId},{google:1}).toArray(function(err, googleRefreshToken){
            getNewGoogleToken(googleRefreshToken[0].google[0].token,googleRefreshToken[0].google[0].refreshToken,function(token){
                console.log("==>New Token is:")
                console.log(googleRefreshToken[0].google[0].token)
                console.log("==>New Token is:")
                console.log(token)
            });
        })
    })
})

function getNewGoogleToken(token,refreshToken,callback){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
}