var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async')

var insightsAdminReportsCollection;
var usersCollection;
var userLogsCollection;
var companyCollection;
var losingTouchCollection;
var dealsAtRiskMetaCollection;
var opportunitiesCollection;
var opportunitiesTargetCollection;
var oppLogCollection;
var interactionsCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('insightsAdminReports is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@mongodb://10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            // var db = client.db('newDb');
            var db = client.db('Relatas');
            // var db = client.db('Relatas_Prod_01Aug');
            // console.log("Connected to db");
            usersCollection = db.collection('user');
            userLogsCollection = db.collection('userlog');
            companyCollection = db.collection('company');
            dealsAtRiskMetaCollection = db.collection('dealsAtRiskMeta');
            insightsAdminReportsCollection = db.collection('insightsAdminReports');
            losingTouchCollection = db.collection('losingTouch');
            opportunitiesCollection = db.collection('opportunities');
            opportunitiesTargetCollection = db.collection('opportunitiesTarget');
            oppLogCollection = db.collection('opportunityLog');
            interactionsCollection = db.collection('interactionsV2');

            var runForSingleCompany = function (skip) {

                companyCollection.find({}).skip(skip).limit(1).toArray(function (err, company) {
                    if(!err && company && company.length>0){
                        var companyId = company[0]._id;

                        getUserProfiles(companyId,function (err1,users) {
                            if(users && users[0]){
                                var allUserEmailIds = _.pluck(users,'emailId');

                                userFiscalYear(company[0],users[0],function (fyMonth,fyRange,allQuarters) {
                                    if(!err1 && users && users.length) {
                                        async.eachSeries(users, function (user, next) {
                                            reportForSingleUser(user,company[0],fyMonth,fyRange,allQuarters,allUserEmailIds,function(){
                                                console.log("Done ",user.emailId);
                                                next();
                                            });
                                        }, function(err) {
                                            saveInsightsAdminReportsForTeam(companyId,function () {
                                                runForSingleCompany(skip+1);
                                            })
                                        });
                                    } else {
                                        runForSingleCompany(skip+1);
                                    }
                                })
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---");
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

startScript();

function reportForSingleUser(user,company,fyMonth,fyRange,allQuarters,allUserEmailIds,callback){

    var primaryCurrency = "USD",
        currenciesObj = {};

    if(company.currency){
        company.currency.forEach(function (el) {
            currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                primaryCurrency = el.symbol;
            }
        });
    }

    var netGrossMargin = company.netGrossMargin?company.netGrossMargin:false;

    var qStart = allQuarters[allQuarters.currentQuarter].start;
    var qEnd = allQuarters[allQuarters.currentQuarter].end;

    async.parallel([
        function (callback) {
            losingTouch(user._id,allUserEmailIds,callback)
        },
        function (callback) {
            dealsAtRisk([user.emailId],primaryCurrency,currenciesObj,qStart,qEnd,callback)
        },
        function (callback) {
            oppREConversion(user._id,callback)
        },
        function (callback) {
            pipelineVelocity(user._id,company._id,fyMonth,fyRange,netGrossMargin,callback)
        },
        function (callback) {
            interactions(user._id,company._id,allUserEmailIds,callback)
        }
    ],function (err,data) {
        if(err){
            console.log("---reportForSingleUser---");
            console.log(err);
        } else {
            saveInsightsAdminReports(user,data,false,function () {
                callback()
            })
        }
    })
}

function interactions(userId,companyId,allUserEmailIds,callback) {

    allInteractions(userId,companyId,allUserEmailIds,function (all_err,allCount) {
        oppInteractions(userId,companyId,function (opp_err,opp_int) {
           callback(null,{allInteractions:allCount,oppInteractions:opp_int});
        });
    });
}

function allInteractions(userId,companyId,allUserEmailIds,callback) {

    interactionsCollection.aggregate([
        {
            $match:{
                ownerId:userId,
                emailId:{$nin:allUserEmailIds},
                interactionDate:{
                    $gte: new Date(moment().subtract(1,"week")),
                    $lte: new Date()
                }
            }
        },
        {
            $project: {
                _id:1
            }
        },
        {
            $group: {
                _id: null,
                count: { $sum: 1 }
            }
        }
    ]).toArray(function(err, data) {
        callback(err,data && data[0] && data[0].count?data[0].count:0)
    });
}

function oppInteractions(userId,companyId,callback) {

    opportunitiesCollection.find({userId:userId},{contactEmailId:1}).toArray(function (err,data) {

        if(!err && data && data.length>0){
            var contacts = _.uniq(_.pluck(data,"contactEmailId"));

            interactionsCollection.aggregate([
                {
                    $match:{
                        ownerId:userId,
                        interactionDate:{
                            $gte: new Date(moment().subtract(1,"week")),
                            $lte: new Date()
                        },
                        emailId:{$in:contacts}
                    }
                },
                {
                    $project: {
                        _id:1
                    }
                },
                {
                    $group: {
                        _id: null,
                        count: { $sum: 1 }
                    }
                }
            ]).toArray(function(err, data) {
                callback(err,data && data[0] && data[0].count?data[0].count:0)
            });

        } else {
            callback(err,0)
        }
    });
}

function saveInsightsAdminReportsForTeam(companyId,callback) {
    var date = moment(),
        dayString = moment(date).format("DDMMYYYY");

    insightsAdminReportsCollection.find({
        companyId:companyId,
        dayString:dayString,
        forTeam:false}).toArray(function (err,data) {

            var losingTouch = 0,
                losingTouchImpact= 0,
                allInteractions= 0,
                oppInteractions= 0,
                dealsAtRisk= 0,
                oppREConversion= 0,
                pipelineVelocity= {
                    expectedPipeline:0,
                    shortfall:0,
                    totalCount:0,
                    totalAmount:0,
                    wonCount:0,
                    wonAmount:0,
                    currentOopPipeline:[],
                    currentQuarter: "",
                    currentTargets:[],
                    oppNextQ:[],
                    staleOpps:[]
                },
                dealsAtRiskImpact= 0,
                dealsAtRiskAction = [];

            if(!err && data){
                _.each(data,function (el) {

                    if(el.pipelineVelocity){
                        pipelineVelocity.expectedPipeline = pipelineVelocity.expectedPipeline+el.pipelineVelocity.expectedPipeline
                        pipelineVelocity.shortfall = pipelineVelocity.shortfall+el.pipelineVelocity.shortfall
                        pipelineVelocity.totalAmount = pipelineVelocity.totalAmount+el.pipelineVelocity.totalAmount
                        pipelineVelocity.totalCount = pipelineVelocity.totalCount+el.pipelineVelocity.totalCount
                        pipelineVelocity.wonCount = pipelineVelocity.wonCount+el.pipelineVelocity.wonCount
                        pipelineVelocity.wonAmount = pipelineVelocity.wonAmount+el.pipelineVelocity.wonAmount
                        pipelineVelocity.currentOopPipeline = pipelineVelocity.currentOopPipeline.concat(el.pipelineVelocity.currentOopPipeline)
                        pipelineVelocity.currentTargets = pipelineVelocity.currentTargets.concat(el.pipelineVelocity.currentTargets)
                        pipelineVelocity.oppNextQ = pipelineVelocity.oppNextQ.concat(el.pipelineVelocity.oppNextQ)
                        pipelineVelocity.staleOpps = pipelineVelocity.staleOpps.concat(el.pipelineVelocity.staleOpps)
                        pipelineVelocity.currentQuarter = el.pipelineVelocity.currentQuarter
                    }

                    if(el.losingTouch){
                        losingTouch = losingTouch+el.losingTouch
                    }

                    if(el.losingTouchImpact){
                        losingTouchImpact = losingTouchImpact+el.losingTouchImpact
                    }

                    if(el.dealsAtRisk){
                        dealsAtRisk = dealsAtRisk+el.dealsAtRisk
                    }

                    if(el.dealsAtRiskAction){
                        dealsAtRiskAction = dealsAtRiskAction.concat(el.dealsAtRiskAction)
                    }

                    if(el.dealsAtRiskImpact){
                        dealsAtRiskImpact = dealsAtRiskImpact+el.dealsAtRiskImpact
                    }

                    if(el.oppREConversion){
                        oppREConversion = oppREConversion+el.oppREConversion
                    }

                    if(el.allInteractions){
                        allInteractions = allInteractions+el.allInteractions
                    }

                    if(el.oppInteractions){
                        oppInteractions = oppInteractions+el.oppInteractions
                    }
                })
            }

        insightsAdminReportsCollection.update({
                companyId:companyId,
                dayString:dayString,
                forTeam:true
            },
            {
                $set: {
                    companyId: companyId,
                    dayString:dayString,
                    date:new Date(date),
                    forTeam:true,
                    losingTouch: losingTouch,
                    allInteractions: allInteractions,
                    oppInteractions: oppInteractions,
                    losingTouchImpact: losingTouchImpact,
                    dealsAtRisk: dealsAtRisk,
                    oppREConversion: oppREConversion,
                    pipelineVelocity: pipelineVelocity,
                    dealsAtRiskImpact:dealsAtRiskImpact,
                    dealsAtRiskAction:uniqBy(dealsAtRiskAction,"opportunityId")
                }
            },{upsert:true},function (err,results) {
                if(err){
                    console.log("---saveInsightsAdminReportsForTeam---");
                    console.log(err);
                }

                usageReportsCompany(companyId,function (err,results) {
                    if(callback){
                        callback();
                    }
                })
            });
    })
}

function saveInsightsAdminReports(user,data,forTeam,callback) {

    var date = moment(),
        dayString = moment(date).format("DDMMYYYY"),
        losingTouch = data[0].contactCount,
        losingTouchImpact = data[0].oppAmount,
        dealsAtRisk = data[1].count,
        dealsAtRiskImpact = data[1].dealsAtRiskImpact,
        oppREConversion = data[2],
        pipelineVelocity = data[3],
        allInteractions = data[4].allInteractions,
        oppInteractions = data[4].oppInteractions;

    getLogReports([String(user._id)],function (logReports) {

        insightsAdminReportsCollection.update({
                companyId:user.companyId,
                dayString:dayString,
                ownerId: user._id,
                ownerEmailId: user.emailId,
                forTeam:forTeam
            },
            {
                $set: {
                    companyId: user.companyId,
                    dayString:dayString,
                    date:new Date(date),
                    ownerId: user._id,
                    ownerEmailId: user.emailId,
                    forTeam:forTeam,
                    losingTouch: losingTouch,
                    losingTouchImpact: losingTouchImpact,
                    dealsAtRisk: dealsAtRisk,
                    oppREConversion: oppREConversion,
                    pipelineVelocity: pipelineVelocity,
                    dealsAtRiskImpact:dealsAtRiskImpact,
                    allInteractions:allInteractions,
                    oppInteractions:oppInteractions,
                    dealsAtRiskAction:data[1].dealsAtRiskAction,
                    totalRegUsers:1,
                    mobActiveUsers:user.isMobileUser?1:0,
                    totalWebLogins:logReports.webLogins,
                    totalMobLogins:logReports.mobileLogins,
                    uniqueMobLogins:logReports.uniqMobLogins,
                    uniqueWebLogins:logReports.uniqWebLogins,
                    totalActiveUsers:1
                }
            },{upsert:true},function (err,results) {
                if(err){
                    console.log("---saveInsightsAdminReports---");
                    console.log(err);
                }
                if(callback){
                    callback();
                }
            });
    });
}

function getOppsForContacts(userId,contacts,callback) {

    var projectQuery = {
        "contactEmailId": "$contactEmailId",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "netGrossMargin": "$netGrossMargin",
        "amount": netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:{
                userId:userId,
                contactEmailId:{$in:contacts},
                relatasStage: {$nin:["Close Won","Close Lost"]}
            }
        },
        {
          $project: projectQuery
        },
        {
            $group: {
                _id: null,
                count: { $sum: "$amount" }
            }
        }
    ]).toArray(function(err, opps) {
        if(!err && opps && opps[0]){
            callback(err,opps[0].count)
        } else {
            callback(err,0)
        }
    });
}

function netGrossAmount() {

    return {
        $cond: [ { $ne: [ "$netGrossMargin", 0] } ,
            {$multiply:[{ "$divide": [ "$netGrossMargin", 100 ] },"$amount"]},
            "$amount" ] }
}

function oppREConversion(userId,callback) {
    callback(null,0)
}

function losingTouch(userId,allUserEmailIds,callback){
    var queryFinal = {
        "contacts.score": { $lte: 50 },
        "contacts.actionTaken": { $ne: true },
        "contacts.personEmailId": { $nin: allUserEmailIds }
    };

    losingTouchCollection.aggregate([
        {
            $match: {
                userId: userId
            }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: queryFinal
        },
        {
            $group: {
                _id: null,
                count: { $sum: 1 },
                contacts: { $addToSet: "$contacts.personEmailId" }
            }
        }
    ]).toArray(function(err, losingTouchCount) {

        if(!err && losingTouchCount && losingTouchCount[0]){
            if(losingTouchCount[0].count){
                getOppsForContacts(userId,losingTouchCount[0].contacts,function (err_o,oppAmount) {
                    callback(err,{contactCount:losingTouchCount[0].count,oppAmount:oppAmount})
                })
            }
        } else {
            callback(err,{contactCount:0,oppAmount:0})
        }

    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId,profileDeactivated:{$ne:true}}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

function pipelineVelocity(userId,companyId,fyMonth,fyRange,netGrossMarginReq,callback){

    var quarters = [
        {
            quarter:1,
            startMonth:0,
            endMonth:2
        },{
            quarter:2,
            startMonth:3,
            endMonth:5
        },{
            quarter:3,
            startMonth:6,
            endMonth:8
        },{
            quarter:4,
            startMonth:9,
            endMonth:11
        }
    ];

    var now = new Date();
    var currentMonth = now.getMonth();

    var dateMax = new Date();
    var dateMin = new Date();

    var currentStartDate = new Date();
    var currentEndDate = new Date();

    var pastStartDate = new Date();
    var pastEndDate = new Date();

    var nextQuarterEndDate = new Date(),nextQuarterStartDate = new Date();

    var currentQuarter = 1; // Init to Jan - March.
    dateMin.setMonth(0)
    dateMax.setMonth(2)

    currentStartDate.setMonth(0)
    currentEndDate.setMonth(2)

    nextQuarterStartDate.setMonth(3)
    nextQuarterEndDate.setMonth(5)

    _.each(quarters,function (q,i,list) {

        if(q.startMonth <= currentMonth && q.endMonth >= currentMonth){

            currentQuarter = q
            currentStartDate.setMonth(q.startMonth)
            currentEndDate.setMonth(q.endMonth)

            var prevQ = list[i-1];
            var nextQ = list[i+1]?list[i+1]:list[0]; // Pick the next quarter. If current quarter is the last quarter, then pick the 1st quarter for next year

            if(prevQ){
                pastStartDate.setMonth(prevQ.startMonth)
                pastEndDate.setMonth(prevQ.endMonth)
            } else {
                pastStartDate.setMonth(new Date(moment(currentStartDate).subtract(2,"month")).getMonth)
                pastEndDate.setMonth(new Date(moment(currentEndDate).subtract(2,"month")).getMonth)
            }
            nextQuarterStartDate.setMonth(nextQ.startMonth)
            nextQuarterEndDate.setMonth(nextQ.endMonth)
        }

    });

    currentStartDate.setDate(1);
    currentStartDate.setHours(0);
    currentStartDate.setMinutes(0);
    currentStartDate.setSeconds(1);

    currentEndDate = new Date(moment(moment(currentStartDate).add(2,"months")).endOf("month"));

    nextQuarterStartDate.setDate(1);
    nextQuarterStartDate.setHours(0);
    nextQuarterStartDate.setMinutes(1);
    nextQuarterStartDate.setSeconds(1);

    nextQuarterEndDate.setDate(31);
    nextQuarterEndDate.setHours(23);
    nextQuarterEndDate.setMinutes(59);
    nextQuarterEndDate.setSeconds(59);

    pastStartDate.setDate(1);
    pastStartDate.setHours(0);
    pastStartDate.setMinutes(1);
    pastStartDate.setSeconds(59);

    pastEndDate.setDate(31);
    pastEndDate.setHours(23);
    pastEndDate.setMinutes(59);
    pastEndDate.setSeconds(59);

    var basedOnAccessControl = false;

    var targetsFor = [userId];

    if(new Date(nextQuarterStartDate) < new Date(currentStartDate)){
        nextQuarterStartDate = moment(currentStartDate).add(3,"months")
        nextQuarterEndDate = moment(currentEndDate).add(3,"months")
    }

    if (!basedOnAccessControl) {
        var regionAccess = null, productAccess = null, verticalAccess = null;
    }

    getTargetForUserByMonthRange(targetsFor,currentStartDate,currentEndDate,regionAccess,productAccess,verticalAccess,fyRange.start,fyRange.end,function (errF,currentTargets) {
        getAllOpportunitiesByStageName(userId,pastStartDate,pastEndDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,function (err,oppPipeline) {
            getAllOpenOpportunities(userId,currentStartDate,currentEndDate,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,function (err,currentOopPipeline) {
                opportunitiesForUsersByDate([userId],nextQuarterStartDate,nextQuarterEndDate,null,regionAccess,productAccess,verticalAccess,null,companyId,function (fErr,oppNextQ) {
                    staleOpportunities(userId,regionAccess,productAccess,verticalAccess,companyId,function (staleErr,staleOpps) {

                        if(!err && !fErr){

                            var totalCount = 0,totalAmount = 0,wonCount = 0,wonAmount = 0;

                            if(oppPipeline.length>0){

                                _.each(oppPipeline,function (el) {
                                    if(el._id == "Close Won"){
                                        wonAmount = el.sumOfAmount
                                        wonCount = el.countOfOpps
                                    }

                                    totalAmount = totalAmount+el.sumOfAmount;
                                    totalCount = totalCount+el.countOfOpps;
                                });
                            }

                            if(wonAmount === 0){
                                wonAmount = 1;
                            }

                            var ft = currentTargets && currentTargets[0]?currentTargets[0].target:0;

                            var expectedPipeline = (ft*totalAmount)/wonAmount;
                            expectedPipeline = expectedPipeline?parseInt(expectedPipeline):expectedPipeline;
                            var openPipeline = 0;

                            if(currentOopPipeline && currentOopPipeline.length>0){
                                _.each(currentOopPipeline,function (el) {
                                    openPipeline = openPipeline+el.sumOfAmount;
                                })
                            }

                            callback(err,{
                                expectedPipeline:expectedPipeline,
                                shortfall:openPipeline>ft?0:ft-openPipeline,
                                totalCount:totalCount,
                                totalAmount:totalAmount,
                                wonCount:wonCount,
                                wonAmount:wonAmount,
                                currentOopPipeline:currentOopPipeline,
                                currentQuarter: monthNames[currentQuarter.startMonth] +' - '+monthNames[currentQuarter.endMonth]+' '+ dateMax.getFullYear(),
                                currentTargets:currentTargets,
                                oppNextQ:oppNextQ,
                                staleOpps:staleOpps
                            })

                        } else {
                            callback(err,{
                                expectedPipeline:0,
                                shortfall:0,
                                totalCount:0,
                                totalAmount:0,
                                wonCount:0,
                                wonAmount:0,
                                currentOopPipeline:[],
                                currentQuarter: monthNames[currentQuarter.startMonth] +' - '+monthNames[currentQuarter.endMonth]+' '+ dateMax.getFullYear(),
                                currentTargets:[],
                                oppNextQ:[],
                                staleOpps:[]
                            })
                        }
                    });
                })
            })
        });
    })

};

function dealsAtRisk(emailId,primaryCurrency,currenciesObj,qStart,qEnd,callback) {

    var from = moment().startOf('day')
    var to = moment().endOf('day')

    dealsAtRiskMetaCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}}).sort({date:1}).toArray(function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null,
            opportunityIds = [];

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                opportunityIds = de.opportunityIds;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        if(el.convertedAmt){
                            el.convertedAmt = parseFloat((parseFloat(el.convertedAmt)).toFixed(2))
                        } else {
                            el.convertedAmt = 0
                        }

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        };

        if(opportunityIds.length>0){
            dealsAtRiskActionTaken(opportunityIds,from,to,function(err,dealsAtRiskAction){
                callback(err,{count:sumBy(deals,"count"),dealsAtRiskImpact:totalDealValueAtRisk,dealsAtRiskAction:dealsAtRiskAction})
            });
        } else {
            callback(err,{count:sumBy(deals,"count"),dealsAtRiskImpact:totalDealValueAtRisk,dealsAtRiskAction:[]})
        }
    })
}

var userFiscalYear = function (company,user,callback) {

    if(company && user){

        var fyMonth = "April"; //Default
        if (!company || !company.fyMonth) {
        } else {
            fyMonth = company.fyMonth
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if(currentMonth<monthNames.indexOf(fyMonth)){
            fromDate.setFullYear(currentYr-1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month');

        var timezone = user.timezone && user.timezone.name?user.timezone.name:"UTC";

        callback(company.fyMonth,
            {start:moment(fromDate).tz(timezone).format(),end: moment(toDate).tz(timezone).format()},
            setQuarter(fyMonth,timezone,fromDate,toDate),
            timezone)
    } else {
        callback(null,null,null,null)
    }
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function setQuarter(startMonth,timezone,fyStartDate,fyEndDate) {

    var qtrObj = {};

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var months = [];
    months.push(fyStartDate)

    _.each(monthNames,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(moment(months[0]).startOf('month')).tz(timezone).format(),end:moment(moment(months[2]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter2 = {start:moment(moment(months[3]).startOf('month')).tz(timezone).format(),end:moment(moment(months[5]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter3 = {start:moment(moment(months[6]).startOf('month')).tz(timezone).format(),end:moment(moment(months[8]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter4 = {start:moment(moment(months[9]).startOf('month')).tz(timezone).format(),end:moment(moment(months[11]).endOf('month')).tz(timezone).format()}

    var currentMonth = moment().month()
    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    return qtrObj
}

var getTargetForUserByMonthRange = function (userIds,dateMin,dateMax,regionAccess,productAccess,verticalAccess,fyStart,fyEnd,callback) {

    var match = {
        userId:{$in:userIds},
        "fy.start":{$gte:new Date(fyStart)},
        "fy.end":{$lte:new Date(fyEnd)}
    }

    opportunitiesTargetCollection.aggregate([
        {
            $match:match
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$gte:new Date(dateMin),$lte:new Date(dateMax)}
            }
        },
        {
            $group:{
                _id:"$userId",
                userEmailId:{$last:"$userEmailId"},
                target:{$sum:"$salesTarget.target"}
            }
        }
    ]).toArray(function (err,targets) {
        callback(err,targets)
    })
}

var getAllOpportunitiesByStageName = function (userId,dateMin,dateMax,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)

    var match = {
        $or:[{userId:userId},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:userId
        }
    }

    if(dateMax && dateMin) {
        match = {
            $or:[{  userId:userId},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:userId,
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        }
    }

    var projectQuery = {
        relatasStage:"$relatasStage",
        amount: "$amount"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match: match
        },
        {
            $project:projectQuery
        },
        {
            $group:{
                _id:"$relatasStage",
                countOfOpps:{$sum:1},
                sumOfAmount:{$sum:"$amount"}
            }
        }
    ]).toArray(function (err, opps) {
        callback(err,opps)
    })
}

var getAllOpenOpportunities = function (userId,dateMin,dateMax,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId)

    var match = {
        $or:[{userId:userId},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:userId
        }
    }

    var projectQuery = {
        relatasStage:"$relatasStage",
        amount: "$amount"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    match.relatasStage = {$nin:["Close Won","Close Lost"]};

    opportunitiesCollection.aggregate([
        {
            $match: match
        },
        {
            $project:projectQuery
        },
        {
            $group:{
                _id:"$relatasStage",
                countOfOpps:{$sum:1},
                sumOfAmount:{$sum:"$amount"}
            }
        }
    ]).toArray(function (err, opps) {
        callback(err,opps)
    })
}

var opportunitiesForUsersByDate = function (userIds,dateMin,dateMax,cEmailId,regionAccess,productAccess,verticalAccess,netGrossMarginReq,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:{$in:userIds}},
            {
                $and:accessControlQuery
            }
        ]
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:{$in:userIds}
        }
    }

    if(dateMax && dateMin) {
        match = {
            $or:[{  userId:{$in:userIds}},
                {
                    $and:accessControlQuery
                }],
            closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
        }

        if(accessControlQuery.length == 0){
            match = {
                userId:{$in:userIds},
                closeDate:{$lte:new Date(dateMax),$gte:new Date(dateMin)}
            }
        }
    }

    if(cEmailId){
        match = {
            "$or": userIds.map(function (el) {
                var obj = {};
                obj["userId"] = el;
                obj["$or"] = [
                    {contactEmailId: cEmailId},
                    {partners:{ $elemMatch: { emailId: cEmailId }}},
                    {decisionMakers:{ $elemMatch: { emailId: cEmailId }}},
                    {influencers:{ $elemMatch: { emailId: cEmailId }}}
                ]
                return obj;
            })
        }
    }

    var projectQuery = {
        "_id": "$_id",
        "contactEmailId": "$contactEmailId",
        "mobileNumber": "$mobileNumber",
        "opportunityId": "$opportunityId",
        "opportunityName": "$opportunityName",
        "stageName": "$relatasStage",
        "relatasStage": "$relatasStage",
        "amount": "$amount",
        "netGrossMargin": "$netGrossMargin",
        "currency": "$currency",
        "closeDate": "$closeDate",
        "createdDate": "$createdDate",
        "geoLocation": "$geoLocation",
        "partners": "$partners",
        "decisionMakers": "$decisionMakers",
        "influencers": "$influencers",
        "productType": "$productType",
        "userEmailId": "$userEmailId",
        "userId": "$userId",
        "lastStageUpdated": "$lastStageUpdated",
        "interactionCount": "$interactionCount"
    }

    if(netGrossMarginReq){
        projectQuery.amount = netGrossAmount()
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group: {
                _id: "$userEmailId",
                opportunities: {
                    $push: {
                        "_id": "$_id",
                        "contactEmailId": "$contactEmailId",
                        "mobileNumber": "$mobileNumber",
                        "opportunityId": "$opportunityId",
                        "opportunityName": "$opportunityName",
                        "stageName": "$relatasStage",
                        "amount": "$amount",
                        "netGrossMargin": "$netGrossMargin",
                        "currency": "$currency",
                        "closeDate": "$closeDate",
                        "createdDate": "$createdDate",
                        "geoLocation": "$geoLocation",
                        "partners": "$partners",
                        "decisionMakers": "$decisionMakers",
                        "influencers": "$influencers",
                        "productType": "$productType",
                        "userEmailId": "$userEmailId",
                        "userId": "$userId",
                        "lastStageUpdated": "$lastStageUpdated",
                        "interactionCount": "$interactionCount"
                    }
                },
                usersAndContacts:{
                    $addToSet: {
                        "contactEmailId": "$contactEmailId",
                        "userEmailId": "$userEmailId"
                    }
                }
            }
        }
    ]).toArray(function(error, result){

        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

var staleOpportunities = function (userId,regionAccess,productAccess,verticalAccess,companyId,callback) {

    var accessControlQuery = accessControlSettings(regionAccess,productAccess,verticalAccess,companyId);

    var match = {
        $or:[{userId:userId},
            {
                $and:accessControlQuery
            }
        ],
        closeDate:{$lte:new Date()},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    if(accessControlQuery.length == 0){
        match = {
            userId:userId,
            closeDate:{$lte:new Date()},
            isClosed:{$ne:true},
            stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
        }
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        }
    ]).toArray(function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

function accessControlSettings(regionAccess,productAccess,verticalAccess,companyId) {

    var accessControlQuery = [];

    if(regionAccess && regionAccess.length>0 && companyId){
        accessControlQuery.push({"geoLocation.zone":{$in:regionAccess}})
    } else {
        accessControlQuery.push({"geoLocation.zone":"no_access"})
    }

    if(productAccess && productAccess.length>0 && companyId){
        accessControlQuery.push({"productType":{$in:productAccess}})
    } else {
        accessControlQuery.push({"productType":"no_access"})
    }

    if(verticalAccess && verticalAccess.length>0 && companyId){
        accessControlQuery.push({vertical:{$in:verticalAccess}})
    } else {
        accessControlQuery.push({vertical:"no_access"})
    }

    accessControlQuery.push({companyId:companyId})

    return accessControlQuery;

}

function dealsAtRiskActionTaken(opportunityIds,from,to,callback) {

    var query = {
        opportunityId: {$in:opportunityIds},
        $or:[{action:{$in:["decisionMakersAdded","influencersAdded"]}},{type:{$in:["stageName","closeDate"]}}],
        date:{$lte:new Date(),$gte:new Date(moment(from).subtract(1,'week'))}
    }

    oppLogCollection.find(query,{opportunityId:1}).toArray(function (err,logs) {
        if(!err && logs && logs.length>0){
            logs.forEach(function (log) {
                log.actionTaken = true;
            });
            logs = uniqBy(logs,"opportunityId")
        }
        callback(err,logs)
    });
}

function usageReportsCompany(companyId,callback) {

    var totalRegUsers = 0,
    totalActiveUsers = 0,
    mobActiveUsers = 0;

    usersCollection.find({companyId:companyId}, {contacts: 0}).toArray(function (err, users) {
        var userIds = []
        if(!err && users && users.length){
            totalRegUsers = users.length;
            _.each(users,function (user) {

                userIds.push(String(user._id));

                if(!user.profileDeactivated){
                    totalActiveUsers++;
                    if(user.isMobileUser){
                        mobActiveUsers++
                    }
                }
            })
        }

        var date = moment(),
            dayString = moment(date).format("DDMMYYYY");

        getLogReports(userIds,function (logReports) {

            insightsAdminReportsCollection.update({
                    companyId:companyId,
                    dayString:dayString,
                    forTeam:true
                },
                {
                    $set: {
                        companyId: companyId,
                        dayString:dayString,
                        date:new Date(date),
                        forTeam:true,
                        totalRegUsers:totalRegUsers,
                        mobActiveUsers:mobActiveUsers,
                        totalWebLogins:logReports.webLogins,
                        totalMobLogins:logReports.mobileLogins,
                        uniqueMobLogins:logReports.uniqMobLogins,
                        uniqueWebLogins:logReports.uniqWebLogins,
                        totalActiveUsers:totalActiveUsers
                    }
                },{upsert:true},function (err,results) {

                    if(callback){
                        callback(err,results)
                    }
                });
        });
    });

}

function getLogReports(userIds,callback){

    userLogsCollection.aggregate([{
        $match:{
            userId:{$in:userIds},
            loginDate: {$gte:new Date(moment(moment().subtract(1,"days")).startOf("week"))}
            }
        },
        { "$project": {
                "userId": 1,
                "loginPage": 1,
                day: { $dateToString: { format: "%Y-%m-%d", date: "$loginDate" }},
                "week": { "$week": "$loginDate" }
            }
        },
        { "$group": {
                // "_id": {day:"$day",loginPage:"$loginPage"},
                "_id": {userId:"$userId",week:"$week",loginPage:"$loginPage"},
                "total": { "$sum": 1 }
            }
        }
        ]).toArray(function(err, data) {
            var mobileLogins = 0;
            var uniqWebLogins = 0;
            var uniqMobLogins = 0;
            var webLogins = 0;

        if(!err && data && data.length>0){
            _.each(data,function (el) {
                // if(el._id.loginPage == "landing-page"){
                if(!_.includes(el._id.loginPage.toLocaleLowerCase(),"mob")){
                    uniqWebLogins++;
                    webLogins = webLogins+el.total;
                // } else if(el._id.loginPage == "mobile-landing"){
                } else {
                    uniqMobLogins++;
                    mobileLogins = mobileLogins+el.total;
                }
            })
        };

        callback({
            uniqWebLogins:uniqWebLogins,
            uniqMobLogins:uniqMobLogins,
            webLogins:webLogins,
            mobileLogins:mobileLogins
        })
    });
}
