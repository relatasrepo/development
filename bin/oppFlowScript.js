var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');

var OppMetaDataCollection;
var opportunitiesCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 0;
rule.minute = 1;
rule.second = 0;
schedule.scheduleJob(rule, function() {
    console.log('oppMetaData is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.33.42:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            OppMetaDataCollection = db.collection('oppMetaData');

            var runForSingleUser = function (skip) {
                // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}, {
                usersCollection.find({corporateUser: true}, {
                    emailId: 1,
                    _id: 1,
                    companyId: 1
                }).skip(skip).limit(1).toArray(function (err, user) {

                    if(!err && user && user.length>0){
                        var companyId = user[0].companyId
                        var userEmailId = user[0].emailId;

                        console.log(userEmailId,skip);
                        updateMetaData(companyId,userEmailId,function () {
                            runForSingleUser(skip+1);
                        })
                    } else {
                        console.log("Done");
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

startScript();

function updateMetaData(companyId,userEmailId,callback){

    var forDate = moment().subtract(1,"month");

    var month = moment(moment().subtract(1,"d")).month()
        ,year = moment(moment().subtract(1,"d")).year()
        ,dateMin = moment().startOf('month')
        ,dateMax = moment().endOf('month');

    var query = {
        userEmailId:userEmailId
    }

    opportunitiesCollection.aggregate([
        {
            $match:query
        },
        {
            $group: {
                _id:"$relatasStage",
                opportunities: {
                    $push: "$$ROOT"
                }
            }
        }
    ]).toArray(function (err,opps) {

        console.log("-----------",userEmailId,"-----------")
        console.log(JSON.stringify(opps,null,1))
        console.log("-----------",userEmailId,"-----------")

        var obj = buildOppMetaDataObj(opps,userEmailId,companyId,month,year);

        OppMetaDataCollection.update({companyId:companyId,userEmailId:userEmailId,month:month,year:year},{$set:obj},{upsert:true},function (err,results) {
            if(callback){
                callback(err,results);
            }
        });

    })

}

function buildOppMetaDataObj(opps,userEmailId,companyId,month,year){
    var obj = {
        data:[]
    };

    _.each(opps,function (el) {
        obj.userEmailId = userEmailId;
        obj.companyId = companyId;
        obj.month = month;
        obj.year = year;

        var amount = 0
            ,amountWithNgm = 0
            ,oppIds = []
            ,regions = []
            ,products = []
            ,verticals = []
            ,reasons = []
            ,types = []
            ,solutions = []
            ,businessUnits = []

        _.each(el.opportunities,function (op) {
            var withNgm = 0;
            var amountWithNgm_original = op.amount;

            if(op.netGrossMargin || op.netGrossMargin == 0){

                withNgm = op.netGrossMargin*op.amount
                amountWithNgm_original = (op.netGrossMargin*op.amount)/100;
                if(withNgm){
                    withNgm = withNgm/100
                }
            }

            amountWithNgm = amountWithNgm+withNgm
            amount = amount+op.amount;
            if(op.geoLocation && (op.geoLocation.zone || op.geoLocation.town)){
                regions.push(op.geoLocation);
            }
            products.push(op.productType);
            reasons.push(op.closeReasons);
            types.push(op.type);
            solutions.push(op.solution);
            verticals.push(op.vertical);
            businessUnits.push(op.businessUnit);
            oppIds.push({
                "closeDate":op.closeDate,
                "createdDate":op.createdDate,
                opportunityId:op.opportunityId,
                amount:amountWithNgm_original,
                amount_original:op.amount,
                fromSnapShot:true,
                userId:op.userId,
                userEmailId:op.userEmailId,
                contactEmailId:op.contactEmailId,
                relatasStage:op.relatasStage,
                stageName:op.stageName,
                opportunityName:op.opportunityName,
                geoLocation:op.geoLocation,
                productType:op.productType,
                createdByEmailId:op.createdByEmailId,
                netGrossMargin: op.netGrossMargin
            });

        });

        obj.data.push({
            stageName:el._id,
            oppIds:oppIds,
            amount:amount,
            amountWithNgm:amountWithNgm,
            numberOfOpps:el.opportunities.length,
            regions:_.compact(_.flatten(regions)),
            reasons:_.compact(_.flatten(reasons)),
            products:_.compact(_.flatten(products)),
            solutions:_.compact(_.flatten(solutions)),
            verticals:_.compact(_.flatten(verticals))
        })
    })

    return obj;
}

initScheduler()

function initScheduler() {

    //Running 12 AM 1st of every month

    var sendDailyAgendaRuleObjectZone4 = schedule.scheduleJob('0 0 1 * *', function(){
        startScript();
    });

}
