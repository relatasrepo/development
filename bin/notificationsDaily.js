var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async');
var admin = require('../config/firebaseConfiguration');
var notificationManager = require('./notificationManager.js');

var tasksCollection;
var usersCollection;
var companyCollection;
var notificationsCollections;
var dealsAtRiskCollection;
var opportunityCollection;
var documentsCollection;
var oppCommits;
var dashboardInsight;

var morningNotificationBuilder = function() {
    notificationManager.getDbInstance(function(db) {

        usersCollection = db.collection('user');
        tasksCollection = db.collection('tasks');
        companyCollection = db.collection('company');
        notificationsCollections = db.collection('notifications');
        dealsAtRiskCollection = db.collection('dealsAtRiskMeta');
        opportunityCollection = db.collection('opportunities');
        documentsCollection = db.collection('documents');
        oppCommits = db.collection('oppCommits');
        dashboardInsight = db.collection('dashboardInsight');

        runForSingleCompany = function(skip) {
            companyCollection.find({}).skip(skip).limit(1).toArray(function(error1, company) {
                if(!error1 && company && company.length > 0) {
                    var companyId = company[0]._id;
                    var companyName = company[0].companyName;

                    console.log("Running for company:", companyName);
                    notificationManager.getUserProfiles(companyId, function(error2, users) {
                        if(!error2 && users[0]) {

                            notificationManager.userFiscalYear(company[0], users[0], function(fyMonth, fyRange, allQuarters) {
                                var commitCutOffDates = notificationManager.getCommitCutOffDates(company[0], users[0].timezone, allQuarters);
                                
                                async.eachSeries(users, function(user, next) {
                                    buildDataForMorningNotifications(user, company[0], fyMonth, fyRange, allQuarters, commitCutOffDates, function() {
                                        next();
                                    })
                                }, function(error3) {
                                    console.log("done for company:", companyName);
                                    runForSingleCompany(skip+1)                            
                                })
                            })
                        } else {
                            runForSingleCompany(skip+1)                            
                        }
                    })
                } else {
                    console.log("-------------------- **ALL DONE** ----------------------------------------");
                }
            })
        }
        runForSingleCompany(0);
    })
}
module.exports.morningNotificationBuilder = morningNotificationBuilder;

var eveningNotificationBuilder = function() {
    notificationManager.getDbInstance(function(db) {

        usersCollection = db.collection('user');
        tasksCollection = db.collection('tasks');
        companyCollection = db.collection('company');
        notificationsCollections = db.collection('notifications');
        dealsAtRiskCollection = db.collection('dealsAtRiskMeta');
        opportunityCollection = db.collection('opportunities');
        documentsCollection = db.collection('documents');
        oppCommits = db.collection('oppCommits');

        runForSingleCompany = function(skip) {
            companyCollection.find({}).skip(skip).limit(1).toArray(function(error1, company) {
                if(!error1 && company && company.length > 0) {
                    var companyId = company[0]._id;
                    var companyName = company[0].companyName;

                    console.log("Running for company:", companyName);
                    notificationManager.getUserProfiles(companyId, function(error2, users) {
                        if(!error2 && users[0]) {

                            async.eachSeries(users, function(user, next) {
                                buildDataForEveningNotifications(user, company[0], function() {
                                    next();
                                })
                            }, function(error3) {
                                console.log("done for company:", companyName);
                                runForSingleCompany(skip+1)                            
                            })
                        } else {
                            runForSingleCompany(skip+1)                            
                        }
                    })
                } else {
                    console.log("ALL DONE");
                }
            })
        }
        runForSingleCompany(0);
    })
}
module.exports.eveningNotificationBuilder = eveningNotificationBuilder;

function buildDataForMorningNotifications(user, company, fyMonth, fyRange, allQuarters, commitCutOffDates, callback) {
    if(user.mobileFirebaseToken || user.webFirebaseSettings) {

        var mobileToken = user.mobileFirebaseToken;
        var webToken = user.webFirebaseSettings ? user.webFirebaseSettings.firebaseToken : null;
        
        notificationManager.getAllReportees(user, function(allReportees,allEmailIds,allUserIds) {
            allReportees = getReportessDictionary(allReportees);
            var monthYear = moment().format('MMM') + " " + moment().year();

            notificationManager.getTargets(allUserIds, monthYear, fyRange.start, fyRange.end, function(usersTargets) {
                
                var primaryCurrency = "USD",
                currenciesObj = {}
        
                company.currency.forEach(function (el) {
                    currenciesObj[el.symbol] = el;
                    if(el.isPrimary){
                        primaryCurrency = el.symbol;
                    }
                });
                
                async.parallel([
                    function(callback) {
                        getTodayTasks(user.emailId, user.timezone, callback);
                    },
                    function(callback) {
                        getOppsForToday(user.emailId, user.companyId, primaryCurrency, currenciesObj, user.timezone, callback);
                    },
                    function(callback) {
                        getDealsAtRisk(user.emailId, primaryCurrency, callback);
                    },
                    function(callback) {
                        checkUserCommits(user._id, user.emailId, user.companyId, commitCutOffDates, webToken, mobileToken, callback);
                    },
                    function (callback) {
                        getTeamMembersCommits(user._id, user.emailId, user.companyId, commitCutOffDates, webToken, mobileToken, allUserIds, primaryCurrency, allReportees, usersTargets, callback);
                    }
                ], function(err, data) {
                    if(err) {
                        console.log(err)
                    }
                    parseNotificationData1(data, company._id, user._id, user.emailId, webToken, mobileToken, function() {
                        callback();
                    })
                })
            })
        })
    } else {
        callback();
    }
}

function buildDataForEveningNotifications(user, company, callback) {
    if(user.mobileFirebaseToken || user.webFirebaseSettings) {
        
        var mobileToken = user.mobileFirebaseToken;
        var webToken = user.webFirebaseSettings ? user.webFirebaseSettings.firebaseToken : null;

        async.parallel([
            function(callback) {
                getDocumentOpenStatus(user.emailId, user.timezone, callback);
            }
        ], function(err, data) {
            if(err) {
                console.log(err)
            }
            parseNotificationData2(data, company._id, user._id, user.emailId, webToken, mobileToken, function() {
                callback();
            })
        })
    } else {
        callback();
    }
}

function parseNotificationData1(notificationData, companyId, userId, emailId, webToken, mobileToken, callback) {
    var dayString = moment().format("DDMMYYYY");

    async.parallel([
        // function to send notification for tasks closing today
        function(callback) {
            if( notificationData[0] && notificationData[0].length <= 0 ) {
                callback(null, null);

            } else {
                var payload = {
                    notification: {
                        body: notificationData[0].length + " task(s) due today",
                        title: "Tasks due today",
                        click_action: "MY_ACTION",
                    },
                    data: {
                        category: "tasksForToday",
                        dayString: dayString,
                        click_action: "/tasks/all?for=today&notifyCategory=tasksForToday&notifyDate=" + dayString 
                    }
                };

                notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                    callback(error, {
                        messageIds: msgId,
                        title: payload.notification.title,
                        body: payload.notification.body,
                        url: payload.data.click_action,
                        payloadData: JSON.stringify(notificationData[0]),
                        notificationType: "daily",
                        category: "tasksForToday"
                    });
                })
            }
        },
        // function to send notification for opps closing today
        function(callback) {
            if( notificationData[1] && notificationData[1].data.length <= 0 ) {
                callback(null, null);

            } else {
                var payload = {
                    notification: {
                        body: "There are " + notificationData[1].totalOpps + " opp(s) worth " + notificationData[1].primaryCurrency + " " + notificationManager.numberWithCommas(notificationData[1].oppValue, notificationData[1].primaryCurrency == "INR") + " closing today",
                        title: "Opportunities closing today",
                        click_action: "MY_ACTION"
                    },
                    data: {
                        category: "oppClosingToday",
                        dayString: dayString,
                        click_action: "/opportunities/all?notifyCategory=oppClosingToday&notifyDate=" + dayString
                    }
                };

                notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                    callback(error, {
                        messageIds: msgId,
                        title: payload.notification.title,
                        body: payload.notification.body,
                        url: payload.data.click_action,
                        payloadData: JSON.stringify(notificationData[1].data),
                        notificationType: "daily",
                        category: "oppClosingToday"
                    });
                })
            }
        },
        // function to send notification for deals at risk
        function(callback) {
            if( notificationData[2] && notificationData[2].count <= 0 ) {
                callback(null, null);

            } else {
                var payload = {
                    notification: {
                        body: "You have deals worth " + notificationData[2].primaryCurrency + " " + notificationManager.numberWithCommas(notificationData[2].amount, notificationData[2].primaryCurrency == "INR") + " at risk",
                        title: "Deals At Risk",
                        click_action: "MY_ACTION"
                    },
                    data: {
                        category: "dealsAtRiskForUser",
                        dayString: dayString,
                        click_action: "/insights?notifyCategory=dealsAtRiskForUser&notifyDate=" + dayString 
                    }
                };

                notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                    callback(error, {
                        messageIds: msgId,
                        title: payload.notification.title,
                        body: payload.notification.body,
                        url: payload.data.click_action,
                        payloadData: JSON.stringify(notificationData[2].data),
                        notificationType: "daily",
                        category: "dealsAtRiskForUser"
                    });
                })
            }
        }
    ], function(err, messageIds) {
        if(err) {
            console.log(err)
        }
        console.log("Sent notification for:",emailId);
        notificationManager.storeNotificationData(messageIds, companyId, userId, emailId, function() {
            callback()
        })
    })
}

function parseNotificationData2(notificationData, companyId, userId, emailId, webToken, mobileToken, callback) {
    var dayString = moment().format("DDMMYYYY");

    async.parallel([
        // function to send notification for document not viewed
        function(callback) {
            if( notificationData[0] && notificationData[0].docNotViewedCount <= 0 ) {
                callback(null, null);

            } else {

                var payload = {
                    notification: {
                        body: notificationData[0].docNotViewedCount + " contacts have not viewed the document that you shared yesterday",
                        title: "Yesterday's shared documents status",
                        click_action: "MY_ACTION"
                    },
                    data: {
                        category: "documentSharingStatus",
                        dayString: dayString,
                        click_action: "/document/index?notifyCategory=documentSharingStatus&notifyDate=" + dayString
                    }
                };

                notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                    callback(error, {
                        messageIds: msgId,
                        title: payload.notification.title,
                        body: payload.notification.body,
                        url: payload.data.click_action,
                        payloadData: JSON.stringify(notificationData[0].data),
                        notificationType: "daily",
                        category: "documentSharingStatus"
                    });
                })
            }
        },
    ], function(err, messageIds) {
        if(err) {
            console.log(err)
        }
        console.log("Sent notificatin for:",emailId);
        notificationManager.storeNotificationData(messageIds, companyId, userId, emailId, function() {
            callback()
        })
    })
}

function getTodayTasks(emailId, timezone, callback) {
    var timezone = timezone && timezone.name?timezone.name:"UTC";
    var fromDate = moment(new Date()).tz(timezone).startOf('day').toDate();
    var toDate = moment(new Date()).tz(timezone).endOf('day').toDate();

    var query = {
        $or:[{ assignedToEmailId:emailId },
            { createdByEmailId:emailId },
            { "participants.emailId":emailId }]
    };

    query.dueDate = {
        $gte: new Date(moment(fromDate)),
        $lte: new Date(moment(toDate))
    }

    query.status = {
        $ne: "complete"
    }

    tasksCollection.find(query, {_id:0}).toArray(function(err, tasks) {
        callback(err, tasks);
    })
}

function getDealsAtRisk(userEmailId, primaryCurrency, callback) {
    dashboardInsight.find({ownerEmailId:userEmailId, forTeam: false}).sort({date: -1}).limit(1).project({"dealsAtRisk": 1}).toArray(function(error, dealsAtRisk) {
        var callbackObj = {};
        
        if(!error && dealsAtRisk[0]) {
            var dealsAtRisk = dealsAtRisk[0].dealsAtRisk;

            if(dealsAtRisk) {
                callbackObj = {
                    count: dealsAtRisk.count,
                    amount: parseFloat(dealsAtRisk.amount.toFixed(2)),
                    primaryCurrency: primaryCurrency,
                    data: []
                }

            } else {
                callbackObj = {
                    count: 0,
                    amount: 0,
                    primaryCurrency: primaryCurrency,
                    data: []
                };
            }

        } else {
            callbackObj = {
                count: 0,
                amount: 0,
                primaryCurrency: primaryCurrency,
                data: []
            };
        }
        callback(null, callbackObj);
    })
}

function getOppsForToday(userEmailId, companyId, primaryCurrency, currenciesObj, timezone, callback) {
    var totalOppValue = 0;    
    var timezone = timezone && timezone.name?timezone.name:"UTC";
    var fromDate = moment(new Date()).tz(timezone).startOf('day').toDate();
    var toDate = moment(new Date()).tz(timezone).endOf('day').toDate();

    var query = {
        $or:[{companyId:companyId},{companyId:companyId.toString()}],
        userEmailId:userEmailId
    }

    query.closeDate = {
        $gte: new Date(fromDate),
        $lte: new Date(toDate)
    }

    query.stageName = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    query.isClosed = {$ne:true}
    query.relatasStage = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    opportunityCollection.find(query).toArray(function(err, opps) {
        if(!err && opps && opps.length>0){
            _.each(opps,function (op) {
                var accName = fetchCompanyFromEmail(op.contactEmailId);
                accName = accName?accName:"Others"

                op.accountName = accName;
                op.amount = parseFloat(op.amount);
                op.amountWithNgm = op.amount;

                if(op.netGrossMargin || op.netGrossMargin == 0){
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                }

                op.convertedAmt = op.amount;

                op.convertedAmtWithNgm = op.amountWithNgm

                if(op.currency && op.currency !== primaryCurrency){

                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                    }

                    if(op.netGrossMargin || op.netGrossMargin == 0){
                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                    }

                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))
                }

                totalOppValue += op.convertedAmtWithNgm;

            });

            callback(err, {
                totalOpps: opps.length,
                oppValue: totalOppValue,
                primaryCurrency: primaryCurrency,
                data: opps
            })
        } else {
            callback(err,{
                totalOpps: 0,
                oppValue: 0,
                primaryCurrency: primaryCurrency,
                data: []
            })
        }
    })
}

function getDocumentOpenStatus(emailId, timezone, callback) {
    var timezone = timezone && timezone.name?timezone.name:"Asia/Kolkata";
    var yesterday = moment().subtract(1, 'days');

    var fromDate = yesterday.tz(timezone).startOf('day').toDate();
    var toDate = yesterday.tz(timezone).endOf('day').toDate();

    var aggregation = [
        {
            $match: {
                "sharedBy.emailId": emailId,
                "isDeleted": {$ne: true}
            }
        }, 
        {
            $addFields: {
                sharedWith: {
                    $filter: {
                        input: "$sharedWith",
                        as: "doc",
                        cond: { $and: [
                            {$eq: ["$$doc.accessed", false]},
                            {$gte: ["$$doc.sharedOn", new Date(fromDate)]},
                            {$lte: ["$$doc.sharedOn", new Date(toDate)]}
                        ]
                            
                        }
                    }
                }
            }
        },
        {
            $addFields: {
                notAccessedDocsCount: {
                    $size:"$sharedWith"
                }
            }
        },
        /* {
            $match: {
                "sharedWith.1": {
                    $exists: true
                }
            }
        } */
    ];

    documentsCollection.aggregate(aggregation).toArray(function(error, docs) {
        var totalDocs = 0;
        _.each(docs, function(el) {
            totalDocs += el.notAccessedDocsCount;
        })

        if(error && docs.length <= 0) {
            callback(null, {
                docNotViewedCount: 0,
                data: []
            })
        } else {
            callback(null, {
                docNotViewedCount: totalDocs,
                data: docs
            })
        }
    })
}

function checkUserCommits(userId, emailId, companyId, commitCutOffDates, webToken, mobileToken, callback) {
    userId = ObjectId(userId);

    var daysLeftForWeekCommit = moment(commitCutOffDates.weeklyCutOffDate).diff(moment(), 'days'),
        daysLeftForMonthCommit = moment(commitCutOffDates.monthlyCutOffDate).diff(moment(), 'days');
        daysLeftForQuarterCommit = moment(commitCutOffDates.quarterlyCutOffDate).diff(moment(), 'days');

    var hoursLeftForWeekCommit = moment(commitCutOffDates.weeklyCutOffDate).diff(moment(), 'hours'),
        hoursLeftForMonthCommit = moment(commitCutOffDates.monthlyCutOffDate).diff(moment(), 'hours');
        hoursLeftForQuarterCommit = moment(commitCutOffDates.quarterlyCutOffDate).diff(moment(), 'hours');

    var commitPendingData = [];
    var notificationStorage = [];

    var commitWeekYear =  moment(commitCutOffDates.weeklyCutOffDate).week()+""+moment(commitCutOffDates.weeklyCutOffDate).year();
    var monthYear = moment().month()+""+moment().year();

    if( (daysLeftForWeekCommit >= 0 && daysLeftForWeekCommit <= 3) && (hoursLeftForWeekCommit > 0 ) ){
        commitPendingData.push({
            commitFor: 'week',
            daysLeft: daysLeftForWeekCommit,
            commitDate: commitCutOffDates.weeklyCutOffDate
        });
    }
    
    if((daysLeftForMonthCommit >= 0 && daysLeftForMonthCommit <= 3) && (hoursLeftForMonthCommit > 0) ) {
        commitPendingData.push({
            commitFor: 'month',
            daysLeft: daysLeftForMonthCommit,
            commitDate: commitCutOffDates.monthlyCutOffDate
        });
    }

    async.eachSeries(commitPendingData, function(data, next) {
        if(data.commitFor == 'week') {
            oppCommits.findOne({userId:userId, commitWeekYear:commitWeekYear, commitFor:"week"}, {projection:{opportunities:0}}, function(error, commits) {
                if(commits) {
                    next();
                } else {
                    var payload = {
                        notification: {
                            title: data.daysLeft + " day(s) left for weekly commit",
                            body: "Commit date: " + moment(data.commitDate).format("DD MMM YYYY"),
                            click_action: "MY_ACTION"
                        },
                        data: {
                            category: "commitReminder",
                            dayString: moment().format("DDMMYYYY"),
                            click_action: "/team/commit/review?notifyCategory=commitPending&notifyDate="+moment().format("DDMMYYYY")
                        }
                    };

                    notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                        notificationStorage.push({
                            messageIds: msgId,
                            title: payload.notification.title,
                            body: payload.notification.body,
                            url: payload.data.click_action,
                            payloadData: data,
                            notificationType: "weekly",
                            category: "commitReminder"
                        });

                        next();
                    })
                }
            });
        } else if(data.commitFor == 'month') {
            oppCommits.findOne({userId:userId, monthYear:monthYear, commitFor:"month"}, {projection:{opportunities:0}}, function(error, commits) {
                if(commits) {
                    next();
                } else {
                    var payload = {
                        notification: {
                            title: data.daysLeft + " day(s) left for monthly commit",
                            body: "Commit date: " + moment(data.commitDate).format("DD MMM YYYY"),
                            click_action: "MY_ACTION"
                        },
                        data: {
                            category: "commitReminder",
                            dayString: moment().format("DDMMYYYY"),
                            click_action: "/team/commit/review?notifyCategory=commitPending&notifyDate="+moment().format("DDMMYYYY")
                        }
                    };

                    notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                        notificationStorage.push({
                            messageIds: msgId,
                            title: payload.notification.title,
                            body: payload.notification.body,
                            url: payload.data.click_action,
                            payloadData: data,
                            notificationType: "weekly",
                            category: "commitReminder"
                        });
                        next();
                    })
                }
            });
        }
    },function(error) {
        if(notificationStorage.length > 0) {
            notificationManager.storeNotificationData(notificationStorage, companyId, userId, emailId, function() {
                callback(null, null)
            })
        } else {
            callback(null, null);
        }
        
    });
}

function getTeamMembersCommits(userId, emailId, companyId, commitCutOffDates, webToken, mobileToken, userIds, primaryCurrency, allReportees, usersTargets, callback) {
    var daysLeftForWeekCommit = moment(commitCutOffDates.weeklyCutOffDate).diff(moment(), 'days'),
        daysLeftForMonthCommit = moment(commitCutOffDates.monthlyCutOffDate).diff(moment(), 'days');
        // daysLeftForQuarterCommit = moment(commitCutOffDates.quarterlyCutOffDate).diff(moment(), 'days');

    var hoursLeftForWeekCommit = moment(commitCutOffDates.weeklyCutOffDate).diff(moment(), 'hours'),
        hoursLeftForMonthCommit = moment(commitCutOffDates.monthlyCutOffDate).diff(moment(), 'hours');

    var commitMetaData = [];
    var notificationStorage = [];

    var commitWeekYear =  moment(commitCutOffDates.weeklyCutOffDate).week()+""+moment(commitCutOffDates.weeklyCutOffDate).year();
    var monthYear = moment(commitCutOffDates.monthlyCutOffDate).month()+""+moment().year();

    if(daysLeftForWeekCommit == -1 || (hoursLeftForWeekCommit <= -1 && hoursLeftForWeekCommit >= -23 )) {
        commitMetaData.push({
            commitFor: 'week',
        });
    }

    if(daysLeftForMonthCommit  == -1 || (hoursLeftForMonthCommit <= -1 && hoursLeftForMonthCommit >= -23 )) {
        commitMetaData.push({
            commitFor: 'month',
        });
    }

    async.eachSeries(commitMetaData, function(data, next) {
        if(data.commitFor == 'week') {
            getTeamWeeklyCommit(userIds, primaryCurrency, allReportees, usersTargets, commitWeekYear, function(commitObj) {
                if(commitObj.data.length <= 0) {
                    next();
                } else {
                    var payload = {
                        notification: {
                            body: commitObj.data.length + " team member(s) commited " + commitObj.primaryCurrency + " " 
                                    + notificationManager.numberWithCommas(commitObj.totalCommitValue, commitObj.primaryCurrency == "INR"),
                            title: "Commit for this week",
                            click_action: "MY_ACTION"
                        },
                        data: {
                            category: "weeklyCommit",
                            teamCommitDataFor: "week",
                            dayString: moment().format("DDMMYYYY"),
                            click_action: "/team/commit/review?notifyCategory=weeklyCommit&notifyDate="+moment().format("DDMMYYYY")
                        }
                    };

                    notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                        notificationStorage.push({
                            messageIds: msgId,
                            title: payload.notification.title,
                            body: payload.notification.body,
                            url: payload.data.click_action,
                            payloadData: JSON.stringify(commitObj.data),
                            notificationType: "weekly",
                            category: "weeklyCommit"
                        });
                        next();
                    })
                }
            })
        } else if(data.commitFor == 'month') {
            getTeamMonthlyCommit(userIds, primaryCurrency, allReportees, usersTargets, monthYear, function(commitObj) {
                if(commitObj.data.length <= 0) {
                    next();
                } else {
                    var payload = {
                        notification: {
                            body: commitObj.data.length + " team member(s) commited " + commitObj.primaryCurrency + " " 
                                    + notificationManager.numberWithCommas(commitObj.totalCommitValue, commitObj.primaryCurrency == "INR"),
                            title: "Commit for this month",
                            click_action: "MY_ACTION"
                        },
                        data: {
                            category: "weeklyCommit",
                            dayString: moment().format("DDMMYYYY"),
                            teamCommitDataFor: "month",
                            click_action: "/team/commit/review?notifyCategory=weeklyCommit&notifyDate="+moment().format("DDMMYYYY")
                        }
                    };

                    notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                        notificationStorage.push({
                            messageIds: msgId,
                            title: payload.notification.title,
                            body: payload.notification.body,
                            url: payload.data.click_action,
                            payloadData: JSON.stringify(commitObj.data),
                            notificationType: "weekly",
                            category: "weeklyCommit"
                        });
                        next();
                    })
                }
            })
        }
    },function(error) {
        if(notificationStorage.length > 0) {
            notificationManager.storeNotificationData(notificationStorage, companyId, userId, emailId, function() {
                callback(null, null)
            })
        } else {
            callback(null, null);
        }
        
    });
}

function getTeamMonthlyCommit(userIds, primaryCurrency, allReportees, usersTargets, monthYear, callback) {
    var query = {
        userId: {$in:userIds},
        monthYear: monthYear,
        commitFor: 'month'
    }

    oppCommits.find(query, {opportunities:0, week:0, quarter:0}).project({opportunities:0}).toArray(function(err, commits) {
        var totalCommits = 0;
        _.each(commits, function(commit) {
            totalCommits += commit.month ? Number(commit.month.userCommitAmount) : 0;
            commit.emailId = allReportees[commit.userId].emailId;
            commit.firstName = allReportees[commit.userId].firstName;
            commit.lastName = allReportees[commit.userId].lastName;
            commit.target = usersTargets[commit.userId];
        });

        if(err) {
            callback({
                data: [],
                totalCommitValue: totalCommits,
                primaryCurrency: primaryCurrency
            })
        } else {
            callback({
                data: commits,
                totalCommitValue: totalCommits,
                primaryCurrency: primaryCurrency
            })
        }
    })
}

function getTeamWeeklyCommit(userIds, primaryCurrency, allReportees, usersTargets, commitWeekYear, callback) {
    var query = {
        userId: {$in:userIds},
        commitWeekYear: commitWeekYear,
        commitFor: 'week'
    }

    oppCommits.find(query, {opportunities:0, month:0, quarter:0}).project({opportunities:0}).toArray(function(err, commits) {
        var totalCommits = 0;
        _.each(commits, function(commit) {
            totalCommits += commit.week ? Number(commit.week.userCommitAmount) : 0;
            commit.emailId = allReportees[commit.userId].emailId;
            commit.firstName = allReportees[commit.userId].firstName;
            commit.lastName = allReportees[commit.userId].lastName;
            commit.target = usersTargets[commit.userId];
        });

        if(err) {
            callback({
                data: [],
                totalCommitValue: totalCommits,
                primaryCurrency: primaryCurrency
            })
        } else {
            callback({
                data: commits,
                totalCommitValue: totalCommits,
                primaryCurrency: primaryCurrency
            })
        }
    })
}

var getReportessDictionary = function(allReportees) {
    var reportees = {};

    _.each(allReportees, function(el) {
        reportees[el._id] = {
            emailId: el.emailId,
            firstName: el.firstName,
            lastName: el.lastName
        }
    });

    return reportees;
}

function fetchCompanyFromEmail(email){

    if(email){
        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        });

        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

if(require.main == module) {
    morningNotificationBuilder();
    // eveningNotificationBuilder();
}