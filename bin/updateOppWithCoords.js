// require('newrelic');
var mongoClient = require('mongodb').MongoClient;
var async = require('async')
var fs = require('fs');
var json2xls = require('json2xls');
var request = require('request');
var oppsCollection;
var usersCollection;
var contactsCollection;
var companyCollection;

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_13Sep?authSource=Relatas_mask", function(error, client) {
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
        // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            // var db = client.db('Relatas_13Sep');
            usersCollection = db.collection('user');
            contactsCollection = db.collection('contacts');
            companyCollection = db.collection('company');
            oppsCollection = db.collection('opportunities');
            var totalFound = 0,data = [];

            var runForSingleCompany = function (skip) {
                companyCollection.find({url:/relatas.relatas/}).skip(skip).limit(1).toArray(function (err, company) {
                    if(!err && company && company.length>0){
                        var companyId = company[0]._id;

                        getUserProfiles(companyId,function (err1,users) {
                            if(users && users[0]){
                                if(!err1 && users && users.length){
                                    async.eachSeries(users, function (user, next){
                                        // updateOpp(user,function(opps){
                                        //
                                        //     if(data){
                                        //         data = data.concat(opps)
                                        //     }
                                        //
                                        //     updateContactsLoca(user,function(){
                                        //         next();
                                        //     })
                                        // });
                                        updateContactsLoca(user,function(){
                                            console.log("user")
                                            next();
                                        })
                                    }, function(err) {
                                        runForSingleCompany(skip+1);
                                    });
                                } else {
                                    runForSingleCompany(skip+1);
                                }
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        var xls = json2xls(data);
                        fs.writeFileSync('opp_loc.xlsx', xls, 'binary');
                        console.log("---All Done---",totalFound);
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

function updateOpp(user,callback){

    var bulk = oppsCollection.initializeUnorderedBulkOp();

    oppsCollection.find({userId:user._id},{opportunityName:1,geoLocation:1,userId:1}).toArray(function (err, opps) {

        var locations = {};

        async.eachSeries(opps, function (op, next){

            if(op.geoLocation && op.geoLocation.town && !op.geoLocation.lat){
                getLocationGoogleAPI(op.geoLocation.town,function (loc) {
                    locations[op.geoLocation.town] = loc
                    next();
                })
            } else {
                next();
            }

        }, function(err) {

            var found = 0,data=[];
            // console.log(locations);
            if(!err && opps && opps.length>0){
                opps.forEach(function (op) {

                    if(op.geoLocation && op.geoLocation.town && !op.geoLocation.lat && locations[op.geoLocation.town]){
                        found++;
                        op.town = op.geoLocation.town;
                        data.push({
                            opportunityName:op.opportunityName,
                            town:op.town,
                            userEmailId:op.userEmailId,
                            createdDate:op.createdDate
                        })
                        bulk.find({_id: op._id, userId:user._id})
                            .update({
                                $set: {
                                    "geoLocation.lat": locations[op.geoLocation.town].lat,
                                    "geoLocation.lng": locations[op.geoLocation.town].lng
                                }
                            });
                    }
                });
                console.log("Found-",found);
                // callback(data);

                if(found){
                    bulk.execute(function(err, result) {
                        if(err){
                            console.log(err)
                        }

                        callback()
                    })
                } else {
                    callback([])
                }
            } else {
                callback(data)
            }
        })
    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

function updateContactsLoca(user,callback) {

    contactsCollection.find({ownerEmailId:user.emailId,location:{$nin:["",null," "]}},{_id:1,location:1}).toArray(function (err, contacts) {
        var bulk = contactsCollection.initializeUnorderedBulkOp();
        var locations = {};

        console.log(user.emailId,contacts.length);

        async.eachSeries(contacts, function (op, next){

            if(op.location){
                getLocationGoogleAPI(op.location,function (loc) {
                    locations[op.location] = loc;
                    next();
                })
            } else {
                next();
            }

        }, function(err) {

            var opsExist = false;
            console.log(locations);
            if(!err && contacts && contacts.length>0){
                contacts.forEach(function (contact) {
                    if(contact.location && locations[contact.location]){
                        opsExist = true;
                        bulk.find({"_id": contact._id,ownerEmailId:user.emailId})
                            .update({
                                $set:{
                                    "lat":locations[contact.location].lat,
                                    "lng":locations[contact.location].lng
                                }
                            });
                    }
                });

                if(opsExist){
                    bulk.execute(function(err,res){
                        callback();
                    });
                } else {
                    callback();
                }
            } else {
                callback()
            }
        });
    })
}

var getLocationGoogleAPI = function(location,callback){
    var loc = location.replace(/[^\w\s]/gi, '');

    request('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAOsrtpWSbB-4PKADa0nMuZifATsvJCvOs&address='+loc,function(e,r,b){

        if(!e) {
            try{
                var gLoc = JSON.parse(b);
            }
            catch(e){
                console.log("Exception Google location Search searchByAddress() ",e, 'search content ',loc);
            }

            var coords = {
                lat:0,
                lng:0
            }

            if(gLoc){
                gLoc.results.forEach(function (el) {
                    if(el.geometry && el.geometry.location){
                        coords = el.geometry.location;
                    }
                })
            }
            callback(coords)

        } else {
            callback(false)
        }
    });
}

startScript();
