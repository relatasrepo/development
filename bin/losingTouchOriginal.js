var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var date = require('date-and-time');
var timediff = require('timediff');
var schedule = require('node-schedule');

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
// rule.hour = 13;
// rule.minute = 00;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('losing Touch is running');
    // losingTouchScheduler();
});

var losingTouchScheduler = function() {
    var weightages = [];
    var now = new Date();
    var success = 0;
    var sixMonthBack = date.addDays(now, -180);
    mongoClient.connect("mongodb://localhost/JulyCopy", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            // db.authenticate("devuserre", "devpasswordre", function(err, res) {
            //     if (err) throw err;
                console.log('authenticated')
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interaction');
                var algoSeedValueCollection = db.collection('algoSeedValues');
                var losingTouchCollection = db.collection('losingTouch');

                async.series([
                    function(callbackSeries1) {
                        algoSeedValueCollection.find({ id: 1 }).toArray(function(err, algoSeedREsult) {
                            if (err) throw err;
                            weightages.push({
                                interaction: algoSeedREsult[0].weightages.interactionWeight.interaction,
                                mails: algoSeedREsult[0].weightages.interactionWeight.mails,
                                calls: algoSeedREsult[0].weightages.interactionWeight.calls,
                                sms: algoSeedREsult[0].weightages.interactionWeight.sms,
                                calendar: algoSeedREsult[0].weightages.interactionWeight.calendar,
                                fb: algoSeedREsult[0].weightages.interactionWeight.fb,
                                linkedin: algoSeedREsult[0].weightages.interactionWeight.linkedin,
                                twitter: algoSeedREsult[0].weightages.interactionWeight.twitter,
                                min: algoSeedREsult[0].weightages.interactionWeight.interactionWeightScale.min,
                                max: algoSeedREsult[0].weightages.interactionWeight.interactionWeightScale.max,
                                favorite: algoSeedREsult[0].weightages.favorite,
                                seniority: algoSeedREsult[0].weightages.seniorityWeight.seniority,
                                cxo: algoSeedREsult[0].weightages.seniorityWeight.cxo,
                                vp: algoSeedREsult[0].weightages.seniorityWeight.vp,
                                director: algoSeedREsult[0].weightages.seniorityWeight.director,
                                manager: algoSeedREsult[0].weightages.seniorityWeight.manager,
                                consultant: algoSeedREsult[0].weightages.seniorityWeight.consultant,
                                others: algoSeedREsult[0].weightages.seniorityWeight.others,
                                recency: algoSeedREsult[0].weightages.recencyOfContactWeight.recencyOfContact,
                                dayOne: algoSeedREsult[0].weightages.recencyOfContactWeight.dayOne,
                                dayTwo: algoSeedREsult[0].weightages.recencyOfContactWeight.dayTwo,
                                dayThree: algoSeedREsult[0].weightages.recencyOfContactWeight.dayThree,
                                contactValue: algoSeedREsult[0].weightages.contactValue,
                                contactType: algoSeedREsult[0].weightages.contactType,
                                relationship: algoSeedREsult[0].weightages.relationship,
                                losingTouch: algoSeedREsult[0].weightages.losingTouch
                            });

                            callbackSeries1();

                        });
                    },
                    function(callbackSeries2) {
                        runForSingleUser(0);
                        callbackSeries2();

                    }
                ], function(err) {
                    if (err) return next(err);
                });
                var success = 0;
                var runForSingleUser = function(skip) {
                    usersCollection.find({emailId:"naveenpaul.markunda@gmail.com"}, { emailId: 1, _id: 0 }).skip(skip).limit(1).toArray(function(err, userEmails) {
                        if (err) throw err;
                        if (userEmails.length > 0) {
                            for (var k = 0; k < userEmails.length; k++) {
                                if (userEmails[k])
                                    getAllInteraction(userEmails[k].emailId, [], function(result) {

                                        console.log("Interactions --")
                                        console.log(JSON.stringify(result.userCollRes.length,null,1))

                                        checkCompanyExistance(result.userCollRes, result.interaction, function(checkResult) {
                                            updateLosingTouch(checkResult.interaction, checkResult.userCollRes, function(updateResult) {

                                                console.log("checkResult --")
                                                console.log(JSON.stringify(checkResult.interaction.length,null,1))
                                                console.log(JSON.stringify(checkResult.userCollRes.length,null,1))
                                                console.log("updateResult --")
                                                console.log(JSON.stringify(updateResult.interaction.length,null,1))

                                                updateLosingTouchCollection(updateResult.interaction, updateResult.userCollRes, function(updated) {
                                                    success++;
                                                    console.log('**********updated for single user*********');
                                                    runForSingleUser(skip + 1);
                                                });
                                            });
                                        });
                                    });
                            }

                        } else {
                            console.log("done");
                            console.log(success + ' records has been updated')
                        }
                    });
                }

                var getAllInteraction = function(emailId, interaction, callbackGetAllInteraction) {
                    // interaction = [];
                    usersCollection.find({ emailId: emailId }, { emailId: 1, _id: 1, contacts: 1, companyId: 1 }).toArray(function(error, userCollRes) {
                        if (error) throw error;

                        if (userCollRes[0].contacts.length > 0) {
                            interactionCollection.aggregate([
                                { $match: { emailId: emailId } },
                                { $unwind: "$interactions" }, {
                                    $match: {
                                        "interactions.interactionDate": { $gt: sixMonthBack, $lt: now }
                                    }
                                }, {
                                    $group: {
                                        _id: {
                                            ne: { $ne: ["$interactions.userId", "$userId"] },
                                            emailId: "$emailId",
                                            uid: "$userId"
                                        },
                                        count: { $sum: 1 },
                                        interactions: {
                                            $push: {
                                                mob: "$interactions.mobileNumber",
                                                d: "$interactions.interactionDate",
                                                emailId: "$interactions.emailId",
                                                iid: "$interactions.userId",
                                                source: "$interactions.source",
                                                interactionType: "$interactions.interactionType",
                                                subType: "$interactions.subType"
                                            }
                                        }
                                    }
                                },
                                { $match: { "_id.ne": true } },
                                { $unwind: "$interactions" }, {
                                    $group: {
                                        _id: { emailId: "$_id.emailId", interactionEmailId: "$interactions.emailId", mob: "$interactions.mob" },
                                        count: { $sum: 1 },
                                        interaction: {
                                            $push: {
                                                d: "$interactions.d",
                                                interactionType: "$interactions.interactionType",
                                                source: "$interactions.source",
                                                subType: "$interactions.subType"
                                            }
                                        }
                                    }
                                }
                            ], function(err, result) {
                                if (err) throw err;
                                if (result.length > 0) {
                                    for (var i = 0; i < result.length; i++) {
                                        interaction.push({
                                            emailId: result[i]._id.interactionEmailId,
                                            mobileNumber: result[i]._id.mob,
                                            mails: 0,
                                            calls: 0,
                                            sms: 0,
                                            calendar: 0,
                                            fb: 0,
                                            linkedin: 0,
                                            twitter: 0,
                                            totalInteraction: 0,
                                            interactionRate: 0,
                                            interactionScore: 0,
                                            favoriteRate: 1,
                                            favoriteScore: 0,
                                            seniorityRate: 0,
                                            seniorityFlag: 0,
                                            seniorityScore: 0,
                                            recencyRate: 0,
                                            recencyScore: 0,
                                            contactValueRate: 0,
                                            contactValue: 0,
                                            contactTypeRate: 1,
                                            contactType: 0,
                                            relationshipRate: 1,
                                            relationship: 0,
                                            total: 0,
                                            losingTouch: 0
                                        });
                                    }
                                    for (var j = 0; j < result.length; j++) {
                                        for (var i = 0; i < result[j].interaction.length; i++) {
                                            switch (result[j].interaction[i].interactionType) {
                                                case 'email':
                                                    interaction[j].mails++;
                                                    break;
                                                case 'meeting':
                                                    interaction[j].mails++;
                                                    break;
                                                case 'meeting-comment':
                                                    interaction[j].mails++;
                                                    break;
                                                case 'document-share':
                                                    interaction[j].mails++;
                                                    break;
                                                case 'message':
                                                    interaction[j].mails++;
                                                    break;
                                                case 'call':
                                                    interaction[j].calls++;
                                                    break;
                                                case 'sms':
                                                    interaction[j].sms++;
                                                    break;
                                                case 'calendar-password':
                                                    interaction[j].calendar++;
                                                    break;
                                                case 'facebook':
                                                    interaction[j].fb++;
                                                    break;
                                                case 'linkedin':
                                                    interaction[j].linkedin++;
                                                    break;
                                                case 'twitter':
                                                    interaction[j].twitter++;
                                                    break;
                                            }
                                        }

                                    }
                                    /*to remove the interaction with same person*/
                                    for (var i = 0; i < interaction.length; i++) {
                                        var found = 0;
                                        for (var j = 0; j < userCollRes[0].contacts.length; j++) {
                                            //userCollRes[0].contacts[j].account.ignore != true
                                            if ((interaction[i].emailId && userCollRes[0].contacts[j].personEmailId) || (interaction[i].mobileNumber && userCollRes[0].contacts[j].mobileNumber)) {
                                                if (interaction[i].emailId && userCollRes[0].contacts[j].personEmailId)
                                                    if (interaction[i].emailId.toLowerCase() === userCollRes[0].contacts[j].personEmailId.toLowerCase()) {
                                                        if(!userCollRes[0].contacts[j].account.ignore || (userCollRes[0].contacts[j].account.ignore != true && typeof userCollRes[0].contacts[j].account.ignore !='string' &&  timediff(userCollRes[0].contacts[j].account.ignore, now, 'D').days >= 0))
                                                            found = 1;
                                                    } else if (interaction[i].mobileNumber && userCollRes[0].contacts[j].mobileNumber)
                                                        if (interaction[i].mobileNumber === userCollRes[0].contacts[j].mobileNumber) {
                                                            if(!userCollRes[0].contacts[j].account.ignore || (userCollRes[0].contacts[j].account.ignore != true && typeof userCollRes[0].contacts[j].account.ignore !='string' && timediff(userCollRes[0].contacts[j].account.ignore, now, 'D').days >= 0))
                                                                found = 1;
                                                        }
                                            }
                                        }
                                        if (found === 0) {
                                            found = 0;
                                            interaction.splice(i, 1);
                                            i = 0;
                                        }
                                    }


                                    /*1.interaction*/
                                    for (var i = 0; i < interaction.length; i++) {
                                        interaction[i].mails = interaction[i].mails * weightages[0].mails;
                                        interaction[i].calls = interaction[i].calls * weightages[0].calls;
                                        interaction[i].sms = interaction[i].sms * weightages[0].sms;
                                        interaction[i].calendar = interaction[i].calendar * weightages[0].calendar;
                                        interaction[i].fb = interaction[i].fb * weightages[0].fb;
                                        interaction[i].linkedin = interaction[i].linkedin * weightages[0].linkedin;
                                        interaction[i].twitter = interaction[i].twitter * weightages[0].twitter;
                                        interaction[i].totalInteraction = interaction[i].mails + interaction[i].calls + interaction[i].sms + interaction[i].calendar + interaction[i].fb + interaction[i].linkedin + interaction[i].twitter;
                                    };

                                    if (interaction.length > 0) {
                                        var minMax = findMinMax(interaction);

                                        weightDiff = weightages[0].max - weightages[0].min;
                                        interactionDiff = minMax.max - minMax.min;

                                        keyVal = interactionDiff / weightDiff;
                                    }

                                    interaction.forEach(function(ele) {
                                        for (var i = 0; i < userCollRes[0].contacts.length; i++) {
                                            if ((ele.emailId && userCollRes[0].contacts[i].personEmailId) || (ele.mobileNumber && userCollRes[0].contacts[i].mobileNumber))
                                                if ((ele.emailId && userCollRes[0].contacts[i].personEmailId && ele.emailId.toLowerCase() === userCollRes[0].contacts[i].personEmailId.toLowerCase()) || ele.mobileNumber === userCollRes[0].contacts[i].mobileNumber) {
                                                    /*2.favorite*/
                                                    if (userCollRes[0].contacts[i].favorite === undefined || userCollRes[0].contacts[i].favorite === null || userCollRes[0].contacts[i].favorite === false)
                                                        ele.favoriteRate = 1;
                                                    else if (userCollRes[0].contacts[i].favorite === true)
                                                        ele.favoriteRate = 0;

                                                    /*3.seniority*/
                                                    if (userCollRes[0].contacts[i].designation === undefined || userCollRes[0].contacts[i].designation === null || userCollRes[0].contacts[i].designation === '') {
                                                        ele.seniorityRate = weightages[0].others.weight;
                                                        ele.seniorityFlag = 1;
                                                    } else {
                                                        for (var j = 0; j < weightages[0].cxo.designation.length; j++) {
                                                            if (weightages[0].cxo.designation[j] === 'cxo')
                                                                var patrn = /\bc.o\b/i;
                                                            else if (weightages[0].cxo.designation[j] === 'c.x.o')
                                                                var patrn = /\bc\..\.o\b/i;
                                                            else
                                                                var patrn = new RegExp(weightages[0].cxo.designation[j], 'i');
                                                            if (patrn.test(userCollRes[0].contacts[i].designation)) {
                                                                ele.seniorityRate = weightages[0].cxo.weight;
                                                                ele.seniorityFlag = 1;
                                                            }
                                                        }

                                                        if (ele.seniorityFlag !== 1)
                                                            for (var j = 0; j < weightages[0].vp.designation.length; j++) {
                                                                if (weightages[0].vp.designation[j] === 'v.p')
                                                                    var patrn = /\bv\.p\b/;
                                                                else if (weightages[0].vp.designation[j] === 's.v.p')
                                                                    var patrn = /\bs\.v\.p\b/;
                                                                else
                                                                    var patrn = new RegExp(weightages[0].vp.designation[j], 'i');
                                                                if (patrn.test(userCollRes[0].contacts[i].designation)) {
                                                                    ele.seniorityRate = weightages[0].vp.weight;
                                                                    ele.seniorityFlag = 1;
                                                                }
                                                            }

                                                        if (ele.seniorityFlag !== 1)
                                                            for (var j = 0; j < weightages[0].director.designation.length; j++) {
                                                                if (weightages[0].director.designation[j] === 'd.g.m')
                                                                    var patrn = /\bd\.g\.m\b/;
                                                                else if (weightages[0].director.designation[j] === 'd.g.m')
                                                                    var patrn = /\bd\.g\.m\b/;
                                                                else if (weightages[0].director.designation[j] === 'g.m')
                                                                    var patrn = /\bg\.m\b/;
                                                                else if (weightages[0].director.designation[j] === 'a.g.m')
                                                                    var patrn = /\ba\.g\.m\b/;
                                                                else if (weightages[0].director.designation[j] === 'd.m')
                                                                    var patrn = /\bd\.m\b/;
                                                                else
                                                                    var patrn = new RegExp(weightages[0].director.designation[j], 'i');
                                                                if (patrn.test(userCollRes[0].contacts[i].designation)) {
                                                                    ele.seniorityRate = weightages[0].director.weight;
                                                                    ele.seniorityFlag = 1;
                                                                }
                                                            }

                                                        if (ele.seniorityFlag !== 1)
                                                            for (var j = 0; j < weightages[0].manager.designation.length; j++) {
                                                                if (weightages[0].manager.designation[j] === 'a.m')
                                                                    var patrn = /\ba\.m\b/;
                                                                else if (weightages[0].manager.designation[j] === 'b.d.m')
                                                                    var patrn = /\bb\.d\.m\b/;
                                                                else
                                                                    var patrn = new RegExp(weightages[0].manager.designation[j], 'i');
                                                                if (patrn.test(userCollRes[0].contacts[i].designation)) {
                                                                    ele.seniorityRate = weightages[0].manager.weight;
                                                                    ele.seniorityFlag = 1;
                                                                }
                                                            }

                                                        if (ele.seniorityFlag !== 1)
                                                            for (var j = 0; j < weightages[0].consultant.designation.length; j++) {
                                                                if (weightages[0].consultant.designation[j] === 'h.r')
                                                                    var patrn = /\bh\.r\b/;
                                                                else
                                                                    var patrn = new RegExp(weightages[0].consultant.designation[j], 'i');
                                                                if (patrn.test(userCollRes[0].contacts[i].designation)) {
                                                                    ele.seniorityRate = weightages[0].consultant.weight;
                                                                    ele.seniorityFlag = 1;
                                                                }
                                                            }

                                                        if (ele.seniorityFlag === 0) {
                                                            ele.seniorityRate = weightages[0].others.weight;
                                                            ele.seniorityFlag = 1;
                                                        }

                                                    }

                                                    /*4.recency*/
                                                    if (userCollRes[0].contacts[i].addedDate !== undefined || userCollRes[0].contacts[i].addedDate !== null) {
                                                        if (timediff(userCollRes[0].contacts[i].addedDate, now, 'D').days >= weightages[0].dayOne.day[0] && timediff(userCollRes[0].contacts[i].addedDate, now, 'D').days < weightages[0].dayOne.day[1]) {
                                                            ele.recencyRate = weightages[0].dayOne.weight;
                                                        } else if (timediff(userCollRes[0].contacts[i].addedDate, now, 'D').days >= weightages[0].dayTwo.day[0] && timediff(userCollRes[0].contacts[i].addedDate, now, 'D').days < weightages[0].dayTwo.day[1]) {
                                                            ele.recencyRate = weightages[0].dayTwo.weight;
                                                        } else if (timediff(userCollRes[0].contacts[i].addedDate, now, 'D').days >= weightages[0].dayThree.day[0]) {
                                                            ele.recencyRate = weightages[0].dayThree.weight;
                                                        }
                                                    } else {
                                                        ele.recencyRate = weightages[0].dayOne.weight;
                                                    }

                                                    /*5.contact value*/
                                                    if (userCollRes[0].contacts[i].contactValue) {
                                                        ele.contactValueRate = 0;
                                                    } else
                                                        ele.contactValueRate = 1;


                                                    /*7.relationship*/
                                                    if (userCollRes[0].contacts[i].contactRelation) {
                                                        if ((userCollRes[0].contacts[i].contactRelation.prospect_customer === undefined || userCollRes[0].contacts[i].contactRelation.prospect_customer === null) && (userCollRes[0].contacts[i].contactRelation.decisionmaker_influencer === undefined || userCollRes[0].contacts[i].contactRelation.decisionmaker_influencer === null)) {
                                                            ele.relationshipRate = 1;
                                                        } else {
                                                            ele.relationshipRate = 0;
                                                        }
                                                    } else
                                                        ele.relationshipRate = 1;

                                                } //if ends (cond. for existence of user)
                                        } //for ends
                                    });

                                    /*interaction rate, interaction score, favorite score, seniority score, recency, contact value and relationship*/
                                    for (var i = 0; i < interaction.length; i++) {
                                        interaction[i].interactionRate = weightages[0].min + ((interaction[i].totalInteraction - minMax.min) / keyVal);
                                        interaction[i].interactionScore = weightages[0].interaction * interaction[i].interactionRate;
                                        interaction[i].favoriteScore = weightages[0].favorite * interaction[i].favoriteRate;
                                        interaction[i].seniorityScore = weightages[0].seniority * interaction[i].seniorityRate;
                                        interaction[i].recencyScore = weightages[0].recency * interaction[i].recencyRate;
                                        interaction[i].relationship = weightages[0].relationship * interaction[i].relationshipRate;
                                        interaction[i].contactValue = weightages[0].contactValue * interaction[i].contactValueRate;
                                    }
                                } //if interaction length is > 0
                                callbackGetAllInteraction({ userCollRes: userCollRes, interaction: interaction, emailId: emailId });
                            }); //end interaction collection
                        } else {
                            callbackGetAllInteraction({ userCollRes: userCollRes, interaction: interaction, emailId: emailId });
                        }
                    }); //end user collection

                } //end getAllInteraction function

                var checkCompanyExistance = function(userCollRes, interaction, callbackcheckCompanyExistance) {
                    // console.log(userCollRes[0]);
                    if (userCollRes[0].companyId === null || userCollRes[0].companyId === undefined) {
                        for (var i = 0; i < interaction.length; i++) {
                            interaction[i].contactType = interaction[i].contactTypeRate * weightages[0].contactType;
                        }
                        callbackcheckCompanyExistance({ interaction: interaction, userCollRes: userCollRes })
                    } else {
                        async.forEach(interaction, function(inter, callbackLocal) {
                            usersCollection.find({ emailId: inter.emailId }, { companyId: 1, _id: 0, emailId: 1 }).toArray(function(err, companyCheck) {
                                if (err) throw err;
                                if (companyCheck.length > 0) {
                                    if (companyCheck[0].companyId && String(companyCheck[0].companyId) === String(userCollRes[0].companyId)) {
                                        inter.contactTypeRate = 0;
                                        inter.contactType = inter.contactTypeRate * weightages[0].contactType;
                                    } else {
                                        inter.contactType = inter.contactTypeRate * weightages[0].contactType;
                                    }
                                } else {
                                    inter.contactType = inter.contactTypeRate * weightages[0].contactType;
                                }
                                callbackLocal();
                            });
                        }, function(err) {
                            if (err) return next(err);
                            callbackcheckCompanyExistance({ interaction: interaction, userCollRes: userCollRes })
                        });
                    }
                } //end checkCompanyExistance function

                var updateLosingTouch = function(interaction, userCollRes, callbackupdateLosingTouch) {
                    for (var i = 0; i < interaction.length; i++) {
                        interaction[i].total = interaction[i].interactionScore + interaction[i].favoriteScore + interaction[i].seniorityScore + interaction[i].recencyScore + interaction[i].contactValue + interaction[i].contactType + interaction[i].relationship;
                    }
                    if (interaction.length > 0) {
                        var totalMinMax = findTotalMinMax(interaction);
                        var losingTouchDiff = weightages[0].losingTouch.max - weightages[0].losingTouch.min;
                        var losingTouchTotalDiff = totalMinMax.max - totalMinMax.min;
                        var losingTouchKeyValue = losingTouchTotalDiff / losingTouchDiff;

                        /*losing Touch*/
                        for (var i = 0; i < interaction.length; i++) {
                            interaction[i].losingTouch = weightages[0].losingTouch.min + ((interaction[i].total - totalMinMax.min) / losingTouchKeyValue);
                        }


                    }
                    callbackupdateLosingTouch({ interaction: interaction, userCollRes: userCollRes });
                } //end updateLosingTouch function

                var updateLosingTouchCollection = function(interaction, userCollRes, callbackupdateLosingTouchCollection) {
                    for (var i = 0; i < interaction.length; i++) {
                        for (var j = 0; j < interaction.length; j++) {
                            if (i !== j) {
                                if (interaction[i].emailId && interaction[j].emailId && interaction[i].emailId.toLowerCase() === interaction[j].emailId.toLowerCase()) {
                                    if (interaction[i].score > interaction[j].score) {
                                        interaction.splice(j, 1);
                                        j = 0;
                                    } else {
                                        interaction.splice(i, 1);
                                        i = 0;
                                    }
                                }
                            }
                        }
                    }

                    var update = { userEmailId: userCollRes[0].emailId, userId: new ObjectId(userCollRes[0]._id), lastUpdatedDate: new Date(), contacts: [] };
                    if (interaction.length == 1)
                        update.contacts.push({ personEmailId: interaction[0].emailId, mobileNumber: interaction[0].mobileNumber, score: 0 });
                    else
                        for (var i = 0; i < interaction.length; i++) {
                            if (interaction[i].emailId === null)
                                var email = null
                            else
                                var email = interaction[i].emailId.toLowerCase()
                            update.contacts.push({ personEmailId: email, mobileNumber: interaction[i].mobileNumber, score: interaction[i].losingTouch });
                        }


                    console.log("What to update ---- *****")
                    console.log(JSON.stringify(update,null,1))

                    // losingTouchCollection.update({ userEmailId: userCollRes[0].emailId.toLowerCase(), userId: new ObjectId(userCollRes[0]._id) }, { $set: { lastUpdatedDate: new Date(), contacts: update.contacts } }, { 'upsert': true }, function(err, re) {
                    //     if (err) throw err;
                    //     console.log(userCollRes);
                    //     console.log(re.result);
                    //     callbackupdateLosingTouchCollection({ interaction: interaction, userCollRes: userCollRes })
                    // });
                }
            // });
        } //end else
    });
} //scheduler end

losingTouchScheduler();

var findMinMax = function(array) {
    var min = array[0].totalInteraction;
    var max = array[0].totalInteraction;

    for (var i = 0; i < array.length; i++) {
        if (array[i].totalInteraction < min)
            min = array[i].totalInteraction;
        if (array[i].totalInteraction > max)
            max = array[i].totalInteraction;
    }

    return { min: min, max: max };
}

var findTotalMinMax = function(array) {
    var min = array[0].total;
    var max = array[0].total;

    for (var i = 0; i < array.length; i++) {
        if (array[i].total < min)
            min = array[i].total;
        if (array[i].total > max)
            max = array[i].total;
    }
    return { min: min, max: max };
}