var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();

rule.hour = 1;
rule.minute = 30;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Relationship Relevance is running');
    start();
});

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
    //     mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/Relatas", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var userRelationCollection = db.collection('userRelation');
                var accountsCollection = db.collection('accounts');
                var companyCollection = db.collection('company');
                var oppsCollection = db.collection('opportunities');

                var runForSingleUser = function (skip) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"},{contacts:1,emailId:1,companyId:1}).skip(0).limit(1).toArray(function (err, user) {
                    // usersCollection.find({emailId: "naveenpaul@relatas.com"},{contacts:1,emailId:1}).skip(skip).limit(1).toArray(function (err, user) {
                    companyCollection.find({}).skip(skip).limit(1).toArray(function (err, company) {
                    // usersCollection.find({corporateUser: true}).skip(skip).limit(1).toArray(function (err, user) {
                        //for contacts

                        if(!err && company && company.length>0 && company[0]){
                            console.log("Completed >",company[0].url,skip)
                            updateStart(company[0],accountsCollection,function (err,result) {
                                runForSingleUser(skip+1)
                            })

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

start();

function updateStart(company,accountsCollection,callback){

    getInvalidAccountObj(company._id,accountsCollection,function () {
        callback()
    })

}

function getInvalidAccountObj(companyId,accountsCollection,callback) {

    accountsCollection.find({companyId:castToObjectId(String(companyId))},{name:1,ownerEmailId:1}).toArray(function (err,existingAccounts) {

        removeInvalidAccounts(checkForInvalidity(existingAccounts),accountsCollection,function () {

        });
        callback(err,existingAccounts);
    });
}

function removeInvalidAccounts(accounts,accountsCollection,callback){

    if(accounts && accounts.length>0){
        var query = { "$or": accounts.map(function(el) {
            var obj = {};
            obj["ownerEmailId"] = el.ownerEmailId;
            obj["name"] = el.name;

            return obj;
        })};

        //accountsCollection.remove(query,function (err,results) {
          //  console.log(JSON.stringify(err,null,1));
            //console.log(JSON.stringify(results,null,1));
        //})
    }

}

function checkForInvalidity(accounts) {

    var invalid = [],
        userList = [];
    _.each(accounts,function (ac) {
        if(ac.name){
            if(phoneNumber(ac.name) || garbageEmailId(ac.name,ac) || checkForCountryCode(ac.name,ac)){
                invalid.push(ac.name)
                userList.push(ac)
            }
        }
    })
    console.log(userList)
    return userList
}

function garbageEmailId(emailId,acc){
    return "/O=" == emailId.substr(0,3)
}

function checkForCountryCode(name){
    return "+91" == name.substr(0,3)
}

function phoneNumber(str) {
    var a = /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/.test(str);
    return a;
}

function isNumber (o) {
    return ! isNaN (o-0) && o !== null && o !== "" && o !== false;
}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        });

        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null;
    }
}

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id)
}
