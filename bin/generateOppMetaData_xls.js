var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var fs = require('fs');
var json2xls = require('json2xls');

var OppMetaDataCollection;
var opportunitiesCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('oppMetaData is running');
    startScript();
});

var startScript = function() {

    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.255:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
    // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            OppMetaDataCollection = db.collection('oppMetaData');

            var runForSingleUser = function (skip) {
                usersCollection.find({emailId: {$in:["chaitra@22by7.in","ashwin@22by7.in","rahul.shukla@ivalue.co.in","navneet.singh@ivalue.co.in"]}}, {
                // usersCollection.find({corporateUser: true}, {
                    emailId: 1,
                    _id: 1,
                    companyId: 1
                }).skip(skip).limit(1).toArray(function (err, user) {

                    if(!err && user && user.length>0){
                        var companyId = user[0].companyId
                        var userEmailId = user[0].emailId;

                        console.log(userEmailId,skip);
                        findMetaData(companyId,userEmailId,function () {
                            runForSingleUser(skip+1);
                        })
                    } else {
                        console.log("Done");
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

startScript();

function findMetaData(companyId,userEmailId,callback){

    var forDate = moment().subtract(1,"month");

    var month = moment().month()
        ,year = moment().year();

    OppMetaDataCollection.findOne({companyId:companyId,userEmailId:userEmailId,month:month,year:year},function (err,results) {

        if(!err && results){
            var data = flattenData(results,userEmailId,companyId,month,year)
            var xls = json2xls(data);
            console.log(data)
            // fs.writeFileSync(userEmailId+'_opp_metadata.xlsx', xls, 'binary');
        }

        if(callback){
            callback(err,results);
        }
    });

}

function flattenData(results,userEmailId,companyId,month,year){
    var oppsData = [];

    if(results.data && results.data.length>0){
        _.each(results.data,function (da) {
            if(da.oppIds && da.oppIds.length>0){
                _.each(da.oppIds,function (op) {
                    oppsData.push({
                        stage:da.stageName,
                        userEmailId:results.userEmailId,
                        month:getMonthName(results.month),
                        year:results.year,
                        opportunityId:op.opportunityId,
                        amount:op.amount
                    })
                })
            }
        })
    }

    return oppsData;
}

function getMonthName(index) {
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

    return months[index]
}