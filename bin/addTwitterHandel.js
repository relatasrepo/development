var mongoose = require('mongoose');
var schedule = require('node-schedule');

var userCollection = require('../databaseSchema/userManagementSchema').User;
var userManagement = require('../dataAccess/userManagementDataAccess');

var userManagementObj = new userManagement();
var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();

var _ = require("lodash");

var argNo = 2;
console.log('bfr:',process.argv[argNo]);
var url = null;
var rule = new schedule.RecurrenceRule();
if(process.argv[argNo] == 'forever')
    argNo = 4;
console.log('afr:',process.argv[argNo]);
if(process.argv[argNo] == 'development'){
    rule.hour = 13;
    rule.minute = 00;
    //rule.second = 59;
    var dbPasswords = appCredential.getDBPasswords();
    url = dbPasswords.relatasDB.dbConnectionUrl;
    mongoose.connect(dbPasswords.relatasDB.dbConnectionUrl, { user: dbPasswords.relatasDB.user, pass: dbPasswords.relatasDB.password, auth: { authdb: 'Relatas' } }, function(error) {
        if (error)
            throw error;
    });
}
else if(process.argv[argNo] == 'staging'){
    rule.hour = 12;
    rule.minute = 00;
    //rule.second = 59;
    url = "mongodb://172.31.32.115/Relatas?poolSize=100";
    mongoose.connect(url);
}
else if(process.argv[argNo] == 'production'){
    rule.hour = 1;
    rule.minute = 00;
    url = "mongodb://172.31.40.17/Relatas?poolSize=100";
    mongoose.connect(url);
}
else if(process.argv[argNo] == 'local'){
    rule.second = 59;
    url = "mongodb://localhost:27017/Relatas?poolSize=100";
    mongoose.connect(url);
}

console.log(url)
console.log(process.argv[argNo+1])

var updateFor = process.argv[argNo+1]
var personTwitterName = [
    {	emailId	:	"sureshhoel@outlook.com"	,	twitterUserName	:	"sureshhoel"	}	,
    {	emailId	:	"neil@neilpatel.com"	,	twitterUserName	:	"neilpatel"	}	,
    {	emailId	:	"sunilgowdabr1993@gmail.com"	,	twitterUserName	:	"sunil_gowda_"	}	,
    {	emailId	:	"sanjayjha3015@gmail.com"	,	twitterUserName	:	"sanjayjhas"	}	,
    {	emailId	:	"jimmytestacc@gmail.com"	,	twitterUserName	:	"jimmytestacc"	}	,
    {	emailId	:	"sumit@relatas.com"	,	twitterUserName	:	"pixelwonderz"	}	,
    {	emailId	:	"sudip@relatas.com"	,	twitterUserName	:	"sudipdutta"	}	,
    {	emailId	:	"naveenpaul.markunda@gmail.com"	,	twitterUserName	:	"iamlifepaul"	}	,
    {	emailId	:	"relatas3@gmail.com"	,	twitterUserName	:	"3JonnyWalker"	}	,
    {	emailId	:	"rahul@lethalbrains.com"	,	twitterUserName	:	"Lethalbrains"	}	,
    {	emailId	:	"ken.krogue@insidesales.com"	,	twitterUserName	:	"kenkrogue"	}	,
    {	emailId	:	"matt@clearbit.com"	,	twitterUserName	:	"mattsornson"	}	,
    {	emailId	:	"shradha@yourstory.com"	,	twitterUserName	:	"SharmaShradha"	}	,
    {	emailId	:	"rustey@gmail.com"	,	twitterUserName	:	"alpharust"	}	,
    {	emailId	:	"abhinav@petasense.com"	,	twitterUserName	:	"akhushraj"	}	,
    {	emailId	:	"vasuta.agarwal@inmobi.com"	,	twitterUserName	:	"VasutaAgarwal"	}	,
    {	emailId	:	"rakesh@hackerrank.com"	,	twitterUserName	:	"rakeshmn"	}	,
    {	emailId	:	"rajiv.jayaraman@knolskape.com"	,	twitterUserName	:	"rajiv_jayaraman"	}	,
    {	emailId	:	"dhruvil.sanghvi@loginextsolutions.com"	,	twitterUserName	:	"dhruvilsanghvi"	}	,
    {	emailId	:	"sureshhoel@gmail.com"	,	twitterUserName	:	"SureshHoel"	}	,
    {	emailId	:	"sasha@kae-capital.com"	,	twitterUserName	:	"SashaMirchi"	}	,
    {	emailId	:	"robbythomas79@gmail.com"	,	twitterUserName	:	"robbythomas79"	}	,
    {	emailId	:	"alok@games2win.com"	,	twitterUserName	:	"rodinhood"	}	,
    {	emailId	:	"kaushik.chakraborty@uti.co.in"	,	twitterUserName	:	"KaushikC"	}	,
    {	emailId	:	"gawruv@yahoo.com"	,	twitterUserName	:	"gawruv"	}	,
    {	emailId	:	"rajani@gmail.com"	,	twitterUserName	:	"rajanir"	}	,
    {	emailId	:	"zishaan@toppr.com"	,	twitterUserName	:	"Zishaan"	}	,
    {	emailId	:	"pallav@fusioncharts.com"	,	twitterUserName	:	"pallavn"	}	,
    {	emailId	:	"sgogate@gmail.com"	,	twitterUserName	:	"sgogate"	}	,
    {	emailId	:	"sshroff@mystartupcfo.com"	,	twitterUserName	:	"ShroffSandeep"	}	,
    {	emailId	:	"hemant.ramnani@gmail.com"	,	twitterUserName	:	"ramnani_hemant"	}	,
    {	emailId	:	"abhishek@tracxn.com"	,	twitterUserName	:	"AbhishekGoyal"	}	,
    {	emailId	:	"alokg@saifpartners.com"	,	twitterUserName	:	"alokg"	}	,
    {	emailId	:	"harshit@appknox.com"	,	twitterUserName	:	"harshwebweaver"	}	,
    {	emailId	:	"anand@persistent.com"	,	twitterUserName	:	"anandesh"	}	,
    {	emailId	:	"sudhir@persistent.com"	,	twitterUserName	:	"kulkarni_sd"	}	,
    {	emailId	:	"kashyap@hypertrack.io"	,	twitterUserName	:	"righthalf"	}	,
    {	emailId	:	"naveen.tewari@inmobi.com"	,	twitterUserName	:	"NaveenTewari"	}	,
    {	emailId	:	"pankaj@500.co"	,	twitterUserName	:	"pjain"	}	,
    {	emailId	:	"dave@500.co"	,	twitterUserName	:	"davemcclure"	}	,
    {	emailId	:	"rajan.anandan@gmail.com"	,	twitterUserName	:	"RajanAnandan"	}	,
    {	emailId	:	"khaitan@gmail.com"	,	twitterUserName	:	"1ndus"	}	,
    {	emailId	:	"rohit@wandake.com"	,	twitterUserName	:	"rohitsingal"	}	,
    {	emailId	:	"ambarish@knowlarity.com"	,	twitterUserName	:	"ambarishKnow"	}
];

var companyTwitterName = [
    {	company	:'Microsoft',	twitterUserName	:'microsoft'}	,
    {	company	:'CrazyEgg',	twitterUserName	:'CrazyEgg'}	,
    {	company	:'InsideSales',	twitterUserName	:'InsideSales'}	,
    {	company	:'Nimble',	twitterUserName	:'Nimble'}	,
    {	company	:'Relatas',	twitterUserName	:'relatasHQ'}	,
    {	company	:'Ferrari',	twitterUserName	:'Ferrari'}	,
    {	company	:'InvisionApp',	twitterUserName	:'InVisionApp'}	,
    {	company	:'BCCI',	twitterUserName	:'BCCI'}	,
    {	company	:'Apple',	twitterUserName	:'apple'}	,
    {	company	:'Sourcebits',	twitterUserName	:'Sourcebits'}	,
    {	company	:'Clearbit',	twitterUserName	:'clearbit'}	,
    {	company	:'Yourstory',	twitterUserName	:'YourStoryCo'}	,
    {	company	:'Petasense',	twitterUserName	:'Petasense'}	,
    {	company	:'InMobi',	twitterUserName	:'InMobi'}	,
    {	company	:'HackerRank',	twitterUserName	:'hackerrank'}	,
    {	company	:'Knolskape',	twitterUserName	:'KNOLSKAPE'}	,
    {	company	:'LogiNext',	twitterUserName	:'LogiNext'}	,
    {	company	:'Apple',	twitterUserName	:'apple'}	,
    {	company	:'Amazon',	twitterUserName	:'amazon'}	,
    {	company	:'Games2win',	twitterUserName	:'Games2win'}	,
    {	company	:'Toppr',	twitterUserName	:'myToppr'}	,
    {	company	:'FusionCharts',	twitterUserName	:'FusionCharts'}	,
    {	company	:'FaichiSolutions',	twitterUserName	:'FaichiSolutions'}	,
    {	company	:'CSSCorp',	twitterUserName	:'CSSCorp'}	,
    {	company	:'Persistent',	twitterUserName	:'Persistentsys'}	,
    {	company	:'Tracxn',	twitterUserName	:'Tracxn'}	,
    {	company	:'SAIFPartners',	twitterUserName	:'SAIFPartners'}	,
    {	company	:'Appknox',	twitterUserName	:'Appknox'}	,
    {	company	:'Hypertrack',	twitterUserName	:'HyperTrack'}	,
    {	company	:'500',	twitterUserName	:'500Startups'}	,
    {	company	:'Google',	twitterUserName	:'google'}	,
    {	company	:'Sirionlabs',	twitterUserName	:'sirionlabs'}	,
    {	company	:'Wandake',	twitterUserName	:'wandakegames'}	,
    {	company	:'Knowlarity',	twitterUserName	:'Knowlarity'}	,
    {	company	:'Microsoft',	twitterUserName	:'microsoft'}	,
    {	company	:'CrazyEgg',	twitterUserName	:'CrazyEgg'}	,
    {	company	:'InsideSales	',	twitterUserName	:'InsideSales'}	,
    {	company	:'Nimble',	twitterUserName	:'Nimble'}	,
    {	company	:'Relatas',	twitterUserName	:'relatasHQ'}	,
    {	company	:'Ferrari',	twitterUserName	:'Ferrari'}	,
    {	company	:'InvisionApp',	twitterUserName	:'InVisionApp'}	,
    {	company	:'BCCI',	twitterUserName	:'BCCI'}	,
    {	company	:'Apple',	twitterUserName	:'apple'}	,
    {	company	:'Sourcebits',	twitterUserName	:'Sourcebits'}
];

addTwitterHandel(0,updateFor);

function addTwitterHandel(skip, updateFor) {
    if(updateFor == 'person' && skip < personTwitterName.length){
        console.log(personTwitterName[skip]);
        userManagementObj.updateTwitterHandelForCompanyOrPerson(personTwitterName[skip].emailId,null,personTwitterName[skip].twitterUserName,updateFor,function(error,result){
            if(!error && result){
                console.log(result);
                skip++;
                addTwitterHandel(skip, updateFor);
            } else {
                // console.log(error)
                console.log('no match found')
                skip++;
                addTwitterHandel(skip, updateFor);
            }
        });
    }
    else if(updateFor == 'company' && skip < companyTwitterName.length){
        console.log(companyTwitterName[skip]);
        userManagementObj.updateTwitterHandelForCompanyOrPerson(null,companyTwitterName[skip].company.toLowerCase(),companyTwitterName[skip].twitterUserName,updateFor,function(error,result){
            if(!error && result){
                console.log(result);
                skip++;
                addTwitterHandel(skip, updateFor);
            } else {
                // console.log(error)
                console.log('no match found')
                skip++;
                addTwitterHandel(skip, updateFor);
            }
        });
    }
    else{
        console.log('---DONE---')
        return;
    }
}