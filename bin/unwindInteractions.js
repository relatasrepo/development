var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  , url = 'mongodb://localhost:27017/Staging'
  , database = null
  , ObjectID = require('mongodb').ObjectID

var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();


MongoClient.connect(url, function(err, db){
    database = db
    fetchInteractions(0)
})  

var fetchInteractions = function(skip){
  database.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
    console.log("Processing records " + (skip + 1) + " - " + (skip +10))
    database.collection("interaction").find().sort({_id: 1}).skip(skip).limit(10).toArray(function(err, interactions){
      if(err)
        console.log(err)
      if(!interactions.length)
      {
        console.log("done")
        return
      }
      var collection = database.collection("interactions")
      var batch = collection.initializeOrderedBulkOp()
      _.each(interactions, function(interaction){
        var ownerEmailId = interaction.emailId
            , ownerId = interaction.userId
        _.each(interaction.interactions, function(interactionObject){
          interactionObject.ownerEmailId = ownerEmailId
          interactionObject.ownerId = ownerId
          interactionObject._id = new ObjectID()
          batch.insert(interactionObject)
        })
      })
      batch.execute(function(err, data){
        if(err)
          console.log(err)
        else{
          console.log(data.nInserted)
          if(data.getWriteErrors()[0])
            console.log(data.getWriteErrors()[0].errmsg)
          fetchInteractions(skip + 10)
        }
      })
    })
  })
}


