var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

//var async = require('async');

var dbUrl = process.argv[2];

console.log("MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connect")
    if (err) {
        console.log(err)
    }

    db.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
        var collection = db.collection("user");

        runForSingleUser(0)
        var i = 0;
        function runForSingleUser(skip){
            collection.find({contacts:{$elemMatch:{mobileNumber:new RegExp(/[^0-9]/g)}}}).sort({emailId:1}).skip(skip).limit(1).toArray(function(error, user){
                if(error){
                    throw error;
                }
                else {
                    if(user.length > 0) {
                        i++;
                        console.log('---------');
                        console.log('PROCESSING FOR "'+user[0].emailId+'"');
                        var contacts = user[0].contacts;
                        var mobileNumbers = 0;
                        if(contacts.length > 0){
                            contacts.forEach(function(a){
                                if(a.mobileNumber && a.mobileNumber.match(new RegExp(/[^0-9]/g))) {
                                    //console.log(a.mobileNumber)
                                    mobileNumbers++;
                                    a.mobileNumber = a.mobileNumber
                                        .replace(new RegExp(/[^0-9]/g), '');
                                    //console.log(a.mobileNumber)
                                }
                            })
                            console.log(mobileNumbers+' mobile number(s) needs to be modified')
                            collection.update({_id:user[0]._id}, {$set:{contacts:contacts}}, function(err, result){
                                if(err){
                                    throw err;
                                }
                                else{
                                    result = result ? 'UPDATED' : 'FAILED';
                                    console.log(result);
                                    runForSingleUser(0)
                                }
                            })
                        }
                        else {
                            runForSingleUser(0)
                        }
                    }
                    else{
                        console.log('*****DONE*****')
                        console.log("number of users:"+i)
                        return
                    }
                }
            })
        }
    })
})