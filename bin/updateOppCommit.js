// require('newrelic');
var mongoClient = require('mongodb').MongoClient;
var async = require('async')

var oppCommitsCollection;
var oppsCollection;
var usersCollection;
var companyCollection;

var startScript = function() {

    mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_13Sep?authSource=Relatas_mask", function(error, client) {
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            // var db = client.db('newDb');
            var db = client.db('Relatas_13Sep');
            usersCollection = db.collection('user');
            oppCommitsCollection = db.collection('oppCommits');
            companyCollection = db.collection('company');
            oppsCollection = db.collection('opportunities');

            var runForSingleCompany = function (skip) {
                companyCollection.find({}).skip(skip).limit(1).toArray(function (err, company) {

                    if(!err && company && company.length>0){
                        console.log(company[0].url)
                        var companyId = company[0]._id;
                        getUserProfiles(companyId,function (err1,users) {
                            if(users[0]){
                                if(!err1 && users && users.length){
                                    async.eachSeries(users, function (user, next){
                                        updateOppCommit(user,function(){
                                            console.log("Done ",user.emailId);
                                            next();
                                        });
                                    }, function(err) {
                                        runForSingleCompany(skip+1);
                                    });
                                } else {
                                    runForSingleCompany(skip+1);
                                }
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---");
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

function updateOppCommit(user,callback){

    var findQuery = {
        userId: user._id
    }

    var updateQuery = {
        $set: {
            userEmailId: user.emailId
        }
    }

    // oppCommitsCollection.update(findQuery,updateQuery,{multi:true},function (err,results) {
    //     if(err){
    //         console.log("Issue for ",user.emailId)
    //         console.log(err)
    //         console.log("-------****---------")
    //     }
    //     callback()
    // });

    oppsCollection.update(findQuery,updateQuery,{multi:true},function (err,results) {
        if(err){
            console.log("Issue for ",user.emailId)
            console.log(err)
            console.log("-------****---------")
        }
        callback()
    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

startScript();
