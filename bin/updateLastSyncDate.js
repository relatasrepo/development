var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  // , url = 'mongodb://localhost:27017/Jan18'
  , url = 'mongodb://172.31.33.173/Relatas'
  , database = null
  , ObjectID = require('mongodb').ObjectID

var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();


MongoClient.connect(url, function(err, db){
    database = db;
    fetchContacts(0)
});

var fetchContacts = function(skip){
  database.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
    console.log("Processing records " + (skip + 1))
    var userCollection = database.collection("user");

    userCollection.find({},{penultimateLoginDate:1,_id:1}).sort({_id: 1}).skip(skip).limit(1).toArray(function(err, userProfile){
    // userCollection.find({emailId: 'naveenpaul.markunda@gmail.com'},{contacts:0}).sort({_id: 1}).skip(skip).limit(1).toArray(function(err, userProfile){

        console.log(userProfile)
      if(!err && userProfile && userProfile[0]){
          var lastDataSyncDate = userProfile[0].penultimateLoginDate;
          userCollection.update({_id:userProfile[0]._id},{$set:{lastDataSyncDate:lastDataSyncDate}},function (err,result) {
              fetchContacts(skip+1)
          })
      } else {
        console.log("Done!")
      }
    })
  })
};

function validateEmail(email) {
  var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(email);
}

