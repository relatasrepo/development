var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  , url = 'mongodb://localhost:27017/Relatas'

var json2csv = require('json2csv');
var fs = require('fs');

MongoClient.connect(url, function(err, db){
  db.authenticate("devuserre", "devpasswordre", function(err, res) {

    var user = db.collection("user");
    var interactions = db.collection("interactions");

    user.find({corporateUser:true},{_id:1,emailId:1}).toArray(function (err,users) {
    // user.find({emailId:"naveenpaul@relatas.com"},{contacts:1,emailId:1}).toArray(function (err,users) {

      var query = { "$or": users.map(function(el) {
        var obj = {};

        obj["ownerId"] = el._id;
        obj["userId"] = {$ne:el._id};
        obj["interactionType"] = "email";
        // obj["interactionDate"] = {$gte: new Date(2017,0,19)}
        return obj;
      })};

      interactions.aggregate([
        {
          $match:query
        },
        {
          $group:{
            _id:{
              owner:"$ownerEmailId",
              refId:"$refId"
            },
            count:{$sum:1}
          }
        },
        {
          $project:{
            _id:"$_id.owner",
            refId:"$_id.refId",
            count:"$count"
          }
        },
        {
          $match:{
            count:{
              "$gt":1
            }
          }
        }
      ],{allowDiskUse: true},function (err,interactionCounts) {

        var fields = ["ownerId","ownerEmailId","userId","refId","emailId","title","emailThreadId",]

        var result = json2csv({ data: interactionCounts, fields: fields });

        fs.writeFile('file.csv', result, function(err) {
          if (err) throw err;
          console.log('file saved');
        });

        // _.each(users,function (user) {
        //   _.each(interactionCounts,function (interactions) {
        //     if(user.emailId == interactions[0]._id){
        //       console.log(interactionCounts.length)
        //     }
        //   })
        // })
      })
    })
  })
});

