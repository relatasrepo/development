var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');

var redis = require('redis');
var redisClient = redis.createClient();

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('dealsAtRisk is running');
    dealsAtRisk();
});

var dealsAtRisk = function() {

}
// mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.33.42:27017/Relatas", function(error, client) { // This is BKUP
// mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.33.42:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
    mongoClient.connect("mongodb://10.150.0.8/Relatas_masked_Apr_04?poolSize=100", function(error, client) {
    if (error)
        throw error;
    else {
        var db = client.db('Relatas');
        // var db = client.db('newDb');
        console.log("Connected to db");
        console.log('authenticated')
        var usersCollection = db.collection('user');
        var interactionCollection = db.collection('interactions');
        var algoSeedValueCollection = db.collection('algoSeedValues');
        var losingTouchCollection = db.collection('losingTouch');
        var opportunitiesCollection = db.collection('opportunities');
        var dealsAtRiskMetaCollection = db.collection('dealsAtRiskMeta');
        var pipelineMetaCollection = db.collection('pipelineMeta');
        var companyCollection = db.collection('company');

        var runForSingleUser = function (skip) {
           // usersCollection.find({emailId: "sureshhoel@gmail.com"}, {
             usersCollection.find({corporateUser: true}, {
                emailId: 1,
                _id: 1,
                companyId: 1
            }).skip(skip).limit(1).toArray(function (err, user) {


                if(!err && user && user.length>0){

                    var hierarchyList = [];
                    var userId = new ObjectId(user[0]._id);
                    var globalUserEmailId = user[0].emailId;
                    hierarchyList.push(userId);

                    var dateMin = moment().add(1, "days");
                    var dateMax = moment().add(1, "days");

                    dateMin.date(1);
                    dateMin.hour(0);
                    dateMin.minute(0);
                    dateMin.second(0);
                    dateMin.millisecond(0);

                    dateMax.date(31);
                    dateMax.hour(23);
                    dateMax.minute(59);
                    dateMax.second(0);
                    dateMax.millisecond(0);

                    getStageWeights(user[0].companyId,companyCollection,function (oppWeights) {

                        dealsAtRiskWeights(algoSeedValueCollection, function (err, weights) {
                            //This function preAverageInteractionsVsOpps() gets interactions data for prev successful deals closure.
                            previousAverageInteractionsVsOpps(hierarchyList,interactionCollection,opportunitiesCollection, function (avgInteractionsVsOpps) {
                                // getOpenDeals() get current open deals for this month
                                getOpenDeals(hierarchyList, dateMin, dateMax,opportunitiesCollection, function (err1, opps) {
                                    // 1. interactionsWithImportantContacts() a) get interactions with these contacts until now,
                                    // b) This function returns data if DMs/Infls have been met
                                    // 2. Checks if DMs/Infls exist

                                    interactionsWithImportantContacts(opps, dateMin, dateMax,interactionCollection, function (data, currentInteractions, forLtQuery, interactionsByCompany) {
                                        getLosingTouch(hierarchyList, forLtQuery,losingTouchCollection, function (err1, ltContacts) {
                                            getTwoWayInteractions(hierarchyList, forLtQuery,interactionCollection, function (err2, twoWayInt) {
                                                calculateRisk(data, weights.weightages,oppWeights,avgInteractionsVsOpps, interactionsByCompany, ltContacts, currentInteractions, null, twoWayInt, function (deals, averageRisk) {
                                                    //test. Remove this
                                                    var counter = 0;
                                                    var atRisk = [];
                                                    var totalDealValueAtRisk = 0;
                                                    var totalPipeline = 0;



                                                    _.each(deals, function (de) {

                                                        var positiveMetric = 0;
                                                        _.forIn(de, function(value, key) {
                                                            if(value === true){
                                                                positiveMetric++
                                                            }
                                                        });

                                                        totalPipeline = totalPipeline+parseFloat(de.amount);

                                                        if ((de.riskMeter >= averageRisk && positiveMetric<3) || de.ltWithOwner) {
                                                            counter++;
                                                            atRisk.push(de);
                                                            totalDealValueAtRisk = totalDealValueAtRisk+parseFloat(de.amount);
                                                        }
                                                    });

                                                    atRisk.sort(function(a, b) {
                                                        return parseFloat(b.riskMeter) - parseFloat(a.riskMeter);
                                                    });

                                                    // var topThree = atRisk.length>3?atRisk.slice(0,3):atRisk

                                                    var opportunityIds = _.pluck(atRisk,"opportunityId");

                                                    var obj = {
                                                        userId: userId,
                                                        userEmailId: globalUserEmailId,
                                                        date: new Date(),
                                                        totalPipeline:totalPipeline,
                                                        opportunities:deals
                                                    }

                                                    processPipeline(pipelineMetaCollection,obj);

                                                    dealsAtRiskMetaCollection.insert({
                                                        userId: userId,
                                                        userEmailId: globalUserEmailId,
                                                        date: new Date(),
                                                        count: counter,
                                                        totalDealValueAtRisk:totalDealValueAtRisk,
                                                        opportunityIds:opportunityIds,
                                                        deals:atRisk,
                                                        totalDeals:deals?deals.length:0,
                                                        averageRisk:averageRisk
                                                    },function (err,result) {
                                                        console.log("Success")
                                                        runForSingleUser(skip+1);
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    })
                } else {
                    console.log("Done");
                }
            });
        };

        runForSingleUser(0);
    }
});

dealsAtRisk();

function dealsAtRiskWeights(algoSeedValueCollection,callback){
    algoSeedValueCollection.findOne({ documentName: "dealsAtRisk" },{weightages:1}, function (err,weights) {
        if(!err && weights){
            callback(err,JSON.parse(JSON.stringify(weights)));
        }
    });
}

function getTwoWayInteractions(hierarchyList,forQuery,interactionCollection,callback){
    getTwoWayInteractionsCount(forQuery,null,null,interactionCollection,function (err,intCount) {
        callback(err,intCount)
    });
}

function getLosingTouch(hierarchyList,userContactGroup,losingTouchCollection,callback){

    getLosingTouchForUsersContacts(hierarchyList,userContactGroup,losingTouchCollection,function (err,ltContacts) {

        if(!err && ltContacts && ltContacts.length>0){
            var ltContactsDictionary = {};
            _.each(ltContacts,function (co) {
                ltContactsDictionary[co._id.ownerEmailId+co._id.emailId] = true;
            })
            callback(err,ltContactsDictionary);
        } else {
            callback(err,[]);
        }

    });
}

function interactionsWithImportantContacts(opps,dateMin,dateMax,interactionCollection,callback) {

    var opportunitiesWithInsights = [];
    var forInteractionsQuery = [];
    var forInteractionsQuery2 = [];

    _.each(opps,function (op) {
        var uniqContacts = [],uniqDmsInfls = [],oppOwner = [],oppCreatedDates = [];
        var opportunities = [];
        _.each(op.opportunities,function (el) {
            if(_.includes(el.stageName,"Won") || _.includes(el.stageName,"won")){

            } else {

                var contacts = [],dmsInfls = [];

                contacts.push(el.contactEmailId)
                oppOwner.push(el.contactEmailId)
                // contacts.push(_.pluck(el.partners,"emailId"))
                contacts.push(_.pluck(el.decisionMakers,"emailId"))
                contacts.push(_.pluck(el.influencers,"emailId"))

                dmsInfls.push(_.pluck(el.decisionMakers,"emailId"))
                dmsInfls.push(_.pluck(el.influencers,"emailId"))

                uniqContacts.push(contacts)
                uniqDmsInfls.push(dmsInfls)

                oppCreatedDates.push(el.createdDate?el.createdDate:el._id.getTimestamp());

                var company = fetchCompanyFromEmail(el.contactEmailId);
                opportunities.push({
                    _id:el._id,
                    opportunityId:el.opportunityId,
                    currency:el.currency?el.currency:"USD",
                    opportunityName:el.opportunityName,
                    closeDate:el.closeDate,
                    formattedCloseDate:moment(el.closeDate).format("DD MMM YYYY"),
                    createdDate:el.createdDate?el.createdDate:el._id.getTimestamp(),
                    userEmailId:el.userEmailId,
                    contactEmailId:el.contactEmailId,
                    amount:el.amount,
                    stageName:el.stageName,
                    productType:el.productType,
                    geoLocation:el.geoLocation,
                    vertical:el.vertical,
                    partnersExist:el.partners?el.partners.length>0:false,
                    influencersExist:el.influencers?el.influencers.length>0:false,
                    decisionMakersExist:el.decisionMakers?el.decisionMakers.length>0:false,
                    decisionMakers:el.decisionMakers,
                    influencers:el.influencers,
                    partners:el.partners,
                    netGrossMargin:el.netGrossMargin,
                    lastStageUpdated:el.lastStageUpdated?el.lastStageUpdated:null,
                    contacts:_.flattenDeep(contacts),
                    dmsInfls:_.flattenDeep(dmsInfls),
                    company:company?company:"Others"
                });

                forInteractionsQuery2.push({
                    userEmailId:el.userEmailId,
                    createdDate:el.createdDate?el.createdDate:el._id.getTimestamp(),
                    contacts:_.flattenDeep(contacts)
                });
            }
        });

        forInteractionsQuery.push({
            userEmailId:op._id,
            contacts:_.uniq(_.flattenDeep(uniqContacts)),
            uniqDmsInfls:_.uniq(_.flattenDeep(uniqDmsInfls)),
            oppOwner:_.uniq(oppOwner),
            oppCreatedDates:oppCreatedDates
        });

        var obj = {};
        obj[op._id] = opportunities
        opportunitiesWithInsights.push(opportunities)
    });

    getInteractionCountByGroupByUserContacts(forInteractionsQuery,dateMin,dateMax,interactionCollection,function (err,interactions) {

        var interactionsByContactAndUser = {},interactionsByCompanyAndUser = {},meetingCount = 0,allCount = 0;
        _.each(interactions,function (el) {

            if(el._id.interactionType == "meeting"){
                meetingCount = meetingCount+el.count
            }

            allCount = allCount+el.count;

            interactionsByContactAndUser[el._id.ownerEmailId+el._id.emailId] = {
                meetingCount:meetingCount,
                allCount:allCount,
                contactEmailId:el._id.emailId
            };
        });

        getInteractionCountByCompany(forInteractionsQuery,dateMin,dateMax,interactionCollection,function (err,interactionsByCompany) {

            _.each(interactionsByCompany,function (el) {

                if(interactionsByCompanyAndUser[el._id.ownerEmailId+fetchCompanyFromEmail(el._id.emailId)]){
                    interactionsByCompanyAndUser[el._id.ownerEmailId+fetchCompanyFromEmail(el._id.emailId)] = el.count+interactionsByCompanyAndUser[el._id.ownerEmailId+fetchCompanyFromEmail(el._id.emailId)];
                } else {
                    interactionsByCompanyAndUser[el._id.ownerEmailId+fetchCompanyFromEmail(el._id.emailId)] = el.count;
                }
            });

            callback(_.flatten(opportunitiesWithInsights),
                interactionsByContactAndUser,
                forInteractionsQuery,
                interactionsByCompanyAndUser);
        });
    });

}

function getOpenDeals(hierarchyList,dateMin,dateMax,oppCollection,callback) {
    getOpenOpportunitiesForUsers(hierarchyList,dateMin,dateMax,null,oppCollection,function (err,opps) {
        callback(err,opps);
    });
}

function previousAverageInteractionsVsOpps(hierarchyList,interactionCollection,opportunitiesCollection,callback) {
    successFullClosures(hierarchyList,opportunitiesCollection,function (err,opps) {

        var usersAndContacts = [],usersAndContactsDictionary = {},usersInteractionsDictionary = {};
        _.each(opps,function (op) {
            var contacts = [], minDate = [], maxDate = [];

            _.each(op.contacts,function (co) {
                contacts.push(co.mainContact)
                // contacts.push(_.pluck(co.partners,"emailId"))
                contacts.push(_.pluck(co.decisionMakers,"emailId"))
                contacts.push(_.pluck(co.influencers,"emailId"))
                minDate.push(new Date(co._id.getTimestamp()))
                maxDate.push(new Date(co.closeDates));
            });

            contacts = _.uniq(_.flatten(contacts));

            usersAndContacts.push({
                userEmailId:op._id,
                oppWonClosed:op.count,
                contacts:contacts,
                minDate:getMinOfArray(minDate),
                maxDate:getMaxOfArray(maxDate)
            });

            var companiesInteracted = 0;
            _.each(contacts,function (co) {
                if(fetchCompanyFromEmail(co)){
                    companiesInteracted++;
                }
            })

            usersAndContactsDictionary[op._id] = {
                contacts:contacts,
                oppWonClosed:op.count,
                companiesInteracted:companiesInteracted
            };

        });

        getInteractionCountByUsersAndContacts(usersAndContacts,null,null,interactionCollection,function (err,interactions) {

            _.each(interactions,function (el) {

                el.oppWonClosed = usersAndContactsDictionary[el._id].oppWonClosed
                el.contactsInteracted = usersAndContactsDictionary[el._id].contacts.length
                el.companiesInteracted = usersAndContactsDictionary[el._id].companiesInteracted
                usersInteractionsDictionary[el._id] = el
            });

            callback(usersInteractionsDictionary)
        });
    });
}

function calculateRisk(opps,weights,oppWeights,avgInteractionsVsOpps,interactionsByCompany,ltContacts,currentInteractions,stages,twoWayInt,callback){

    var today = moment().today;
    var avgIntPerDealCloseCache = 0;

    var twoWayIntDictionary = {};

    _.each(twoWayInt,function (el) {
        twoWayIntDictionary[el._id.ownerEmailId+el._id.contact+el._id.action] = el.count
    })

    _.each(opps,function (op) {

        op.interactionsInCompanyWeight = 0;
        op.decisionMaker_infuencerExistWeight = 0;
        op.metDecisionMaker_infuencerWeight = 0;
        op.losingTouchWithOwnerWeight = 0;
        op.oppStageWeight = 0;
        op.stageLastModifiedWeight = 0;
        op.averageInteractionsPerDealWeight = 0;
        op.staleOppWeight = 0;
        op.twoWayInteractionsWeight = 0;
        op.avgInWithOwnerWeight = 0;

        var totalCompaniesInteracted = 0;

        _.each(op.contacts,function (co) {
            if(fetchCompanyFromEmail(op.contactEmailId)){
                totalCompaniesInteracted++;
            }
        });

        var riskMeter = 0;
        if(op.influencersExist || op.decisionMakersExist){
        } else {
            riskMeter = riskMeter+weights.decisionMaker_infuencerExist

            op.decisionMaker_infuencerExistWeight = weights.decisionMaker_infuencerExist
        }

        if(avgInteractionsVsOpps && avgInteractionsVsOpps[op.userEmailId]){
            if(avgInteractionsVsOpps[op.userEmailId].companiesInteracted >= totalCompaniesInteracted){
                op.interactionsInCompanyWeight = weights.interactionsInCompany
                riskMeter = riskMeter+weights.interactionsInCompany
            }
        }

        if(currentInteractions && currentInteractions[op.userEmailId+op.contactEmailId]){

            var meetingCount = 0;
            _.each(op.dmsInfls,function (el) {
                if(currentInteractions[op.userEmailId+el] && currentInteractions[op.userEmailId+el].meetingCount>0){
                    meetingCount++;
                }
            });

            if(meetingCount>0){
                op.metDecisionMaker_infuencer = true;
            } else {
                riskMeter = riskMeter+weights.metDecisionMaker_infuencer
                op.metDecisionMaker_infuencer = false;
                op.metDecisionMaker_infuencerWeight = weights.metDecisionMaker_infuencer
            }

        } else {
            riskMeter = riskMeter+weights.metDecisionMaker_infuencer
            op.metDecisionMaker_infuencerWeight = weights.metDecisionMaker_infuencer
            op.metDecisionMaker_infuencer = false;
        }

        var daysSince = 90; // Default more than 90 days
        var daysSinceOppCreated = -1*(moment(new Date(op.createdDate)).diff(today, 'days'));

        if(op.lastStageUpdated && op.lastStageUpdated.date){
            daysSince = -1*(moment(new Date(op.lastStageUpdated.date)).diff(today, 'days'));
            if(daysSince>45){
                riskMeter = riskMeter+weights.stageLastModified;
                op.stageLastModifiedWeight = weights.stageLastModified
            } else if(daysSince<45){

            }
        } else {

            daysSince = daysSinceOppCreated;
            if(daysSinceOppCreated>45){
                riskMeter = riskMeter+weights.stageLastModified;
                op.stageLastModifiedWeight = weights.stageLastModified
            }
        }

        op.daysSinceStageUpdated = daysSince;

        if(oppWeights && oppWeights[op.stageName]) {
            riskMeter = riskMeter + (oppWeights[op.stageName] / 100) * weights.oppStage;
            op.oppStageWeight = (oppWeights[op.stageName] / 100) * weights.oppStage
        } else {
            if(op.stageName == "Prospecting"){
                riskMeter = riskMeter+0.10*weights.oppStage;
                op.oppStageWeight = 0.10*weights.oppStage
            }else if(op.stageName == "Evaluation"){
                riskMeter = riskMeter+0.3*weights.oppStage;
                op.oppStageWeight = 0.3*weights.oppStage
            } else if(op.stageName == "Proposal"){
                riskMeter = riskMeter+0.6*weights.oppStage;
                op.oppStageWeight = 0.6*weights.oppStage
            }
        }

        if(ltContacts && ltContacts[op.userEmailId+op.contactEmailId]) {
            riskMeter = riskMeter+weights.losingTouchWithOwner
            op.losingTouchWithOwnerWeight = weights.losingTouchWithOwner
            op.ltWithOwner = true
        } else {
            op.ltWithOwner = false
        }

        //Check if the average interactions are being maintained for the contact owner.
        // Or else the risk will be high. If there are no interactions in the past 30 days, risk is high by default.

        if(avgInteractionsVsOpps && avgInteractionsVsOpps[op.userEmailId] && currentInteractions && currentInteractions[op.userEmailId+op.contactEmailId]){
            // var avgIntPerDealClose = (avgInteractionsVsOpps[op.userEmailId].oppWonClosed/avgInteractionsVsOpps[op.userEmailId].count)*100;

            var avgIntPerDealClose = 0;
            if(!avgIntPerDealCloseCache){
                avgIntPerDealCloseCache = (avgInteractionsVsOpps[op.userEmailId].count/avgInteractionsVsOpps[op.userEmailId].oppWonClosed);
                avgIntPerDealClose = avgIntPerDealCloseCache

            } else {
                avgIntPerDealClose = avgIntPerDealCloseCache
            }

            var currentIntCount = currentInteractions[op.userEmailId+op.contactEmailId].allCount
            if(currentIntCount<avgIntPerDealClose){
                riskMeter = riskMeter+weights.averageInteractionsPerDeal;

                op.averageInteractionsPerDealWeight = weights.averageInteractionsPerDeal;

                op.averageInteractionsPerDeal = false
            } else {
                op.averageInteractionsPerDeal = true
            }

        } else {
            riskMeter = riskMeter+weights.averageInteractionsPerDeal
            op.averageInteractionsPerDealWeight = weights.averageInteractionsPerDeal;
        }

        //Two way interactions
        var liuInit = 0,oppOwnerInit = 0;

        if(twoWayIntDictionary && twoWayIntDictionary[op.userEmailId+op.contactEmailId+'receiver']){
            liuInit = twoWayIntDictionary[op.userEmailId+op.contactEmailId+'receiver']
        }

        if(twoWayIntDictionary && twoWayIntDictionary[op.userEmailId+op.contactEmailId+'sender']){
            oppOwnerInit = twoWayIntDictionary[op.userEmailId+op.contactEmailId+'sender']
        }

        op.skewedTwoWayInteractions = false;

        if(oppOwnerInit/liuInit<0.2){
            op.skewedTwoWayInteractions = true;
            riskMeter = riskMeter+weights.twoWayInteractions;
            op.twoWayInteractionsWeight = weights.twoWayInteractions;
        }


        if(new Date(op.closeDate)< new Date()){
            riskMeter = riskMeter+weights.staleOpp
            op.staleOppWeight = weights.staleOpp;
        }

        op.riskMeter = roundOff(riskMeter,2) ;
        // op.riskMeter = riskMeter ;

    });

    var riskMeterValues = _.pluck(opps,"riskMeter");
    var maxRiskValue = _.max(riskMeterValues)
    var minRiskValue = _.min(riskMeterValues)
    var num = (minRiskValue+maxRiskValue)/2;
    var averageRisk = _.sum(riskMeterValues)/riskMeterValues.length;

    callback(opps,averageRisk)

}

function successFullClosures(userIds,opportunitiesCollection,callback) {

    opportunitiesCollection.aggregate([
        {
            $match: {
                userId: {$in: userIds},
                stageName:{$in:['Close Won','Closed Won']}
            }
        },
        {
            $group:{
                _id:"$userEmailId",
                count:{$sum:1},
                contacts:{
                    $addToSet:{
                        mainContact:"$contactEmailId",
                        partners:"$partners",
                        decisionMakers:"$decisionMakers",
                        influencers:"$influencers",
                        createdDates:"$createdDate",
                        closeDates:"$closeDate",
                        _id:"$_id"
                    }
                }
            }
        }
    ]).toArray(function (err, opps) {
        callback(err,opps)
    })
}

function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}

function getMinOfArray(numArray) {
    return Math.min.apply(null, numArray);
}

function getOpenOpportunitiesForUsers (userIds,dateMin,dateMax,cEmailId,opportunitiesCollection,callback) {

    var match = {
        userId:{$in:userIds},
        // closeDate:{ $gte: new Date(dateMin), $lte:new Date(dateMax) }
        isClosed:{$ne:true}
    }

    if(!dateMin && !dateMax){
        match = {
            userId: {$in: userIds},
            isClosed:{$ne:true}
        }
    }

    if(cEmailId){
        match = {
            "$or": userIds.map(function (el) {
                var obj = {};
                obj["userId"] = el;
                obj["$or"] = [
                    {contactEmailId: cEmailId},
                    {partners:{ $elemMatch: { emailId: cEmailId }}},
                    {decisionMakers:{ $elemMatch: { emailId: cEmailId }}},
                    {influencers:{ $elemMatch: { emailId: cEmailId }}}
                ]
                return obj;
            })
        }
    }

    match["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group: {
                _id: "$userEmailId",
                opportunities: {
                    $push: "$$ROOT"
                },
                usersAndContacts:{
                    $addToSet: {
                        "contactEmailId": "$contactEmailId",
                        "userEmailId": "$userEmailId"
                    }
                }
            }
        }
    ]).toArray(function(error, result){
        if(error){
            callback(error,null);
        }
        else{
            callback(null,result);
        }
    });
};

function getInteractionCountByUsersAndContacts (usersAndContacts,dateMin,dateMax,InteractionsCollection,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {

        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.contacts };

        if(el.minDate && el.maxDate){
            obj["interactionDate"] = {$gte:new Date(el.minDate),$lte:new Date(el.maxDate)}
        }

        if(!el.maxDate && el.minDate){
            obj["interactionDate"] = {$gte:new Date(el.minDate),$lte:new Date()}
        }

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
        } else if(!el.minDate && !el.maxDate) {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:"$ownerEmailId",
                count:{"$sum":1}
            }
        }
    ]).toArray(function (err,results) {
        callback(err,results)
    });
}

function getInteractionCountByGroupByUserContacts (usersAndContacts,dateMin,dateMax,InteractionsCollection,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.contacts };

        if(dateMin && dateMax){
            obj["interactionDate"] = {$gte:new Date(moment().subtract(1, "month")),$lte:new Date(dateMax)}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:{ownerEmailId:"$ownerEmailId",emailId:"$emailId",interactionType:"$interactionType"},
                count:{"$sum":1}
            }
        }
    ]).toArray(function (err,results) {
        callback(err,results)
    });
}

function getTwoWayInteractionsCount (usersAndContacts,dateMin,dateMax,InteractionsCollection,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId
        obj["emailId"] = { "$in": el.oppOwner };

        if(el.oppCreatedDates.length>0){
            obj["interactionDate"] = {$gte:new Date(_.min(el.oppCreatedDates)),$lte:new Date()}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group: {
                _id: {action:"$action",ownerEmailId:"$ownerEmailId",contact:"$emailId"},
                // connectedVia:{$addToSet:"$interactionType"},
                count: {
                    $sum: 1
                }
            }
        }
    ]).toArray(function (err,results) {
        callback(err,results)
    });
}

function getLosingTouchForUsersContacts(userIds,userContactGroup,losingTouch,callback){

    var queryFinal = { "$or": userContactGroup.map(function(el) {
        var obj = {}
        obj["userEmailId"] = el.userEmailId
        obj["contacts.score"] = {$lt:50}
        obj["contacts.personEmailId"] = { "$in": el.contacts };

        return obj;
    })};

    losingTouch.aggregate([
        {
            $match: { userId: { $in: userIds } }
        },
        {
            $unwind: "$contacts"
        },
        {
            $match: queryFinal
        },
        {
            $group: {
                _id:{ownerEmailId:"$userEmailId",emailId:"$contacts.personEmailId"}
            }
        }
        //}
    ]).toArray(function(err, count) {
        callback(err,count)
    })
}

function getInteractionCountByCompany (usersAndContacts,dateMin,dateMax,InteractionsCollection,callback) {

    var queryFinal = { "$or": usersAndContacts.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el.userEmailId;

        var companies = el.contacts.map(function (co) {

            var company = fetchCompanyFromEmail(co);

            if(company){
                return new RegExp('@'+company, "i")
            }

        })

        obj["emailId"] = { "$in": _.uniq(_.compact(companies)) };

        if(dateMin && dateMax){
            // obj["interactionDate"] = {$gte:new Date(dateMin),$lte:new Date(dateMax)}
            obj["interactionDate"] = {$gte:new Date(moment().subtract(1, "month")),$lte:new Date()}
        } else {
            obj["interactionDate"] = {$lte:new Date()}
        }

        return obj;
    })};

    InteractionsCollection.aggregate([
        {
            $match:queryFinal
        },
        {
            $group:{
                _id:{ownerEmailId:"$ownerEmailId",emailId:"$emailId"},
                count:{"$sum":1}
            }
        }
    ]).toArray(function (err,results) {
        callback(err,results)
    });
}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? "Others" : (words[0])
    } else {
        return null;
    }
}

function roundOff(value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function processPipeline(pipelineMetaCollection,pipeline) {

    pipelineMetaCollection.insert(pipeline,function (err,result) {
        // console.log("Success Pipeline")
    });
}

function getStageWeights(companyId,companyCollection,callback) {
    var key = "oppWeights"+String(companyId);

    getCache(key,function (cacheWeights) {
        if(cacheWeights){
            callback(cacheWeights)
        } else {
            companyCollection.findOne({_id:new ObjectId(companyId)},{opportunityStages:1},function (err,weights) {
                if(!err && weights){
                    var hashMap = getHashMap(weights.opportunityStages);
                    setCache(key,hashMap)
                    callback(hashMap)
                } else {
                    callback(null)
                }
            })
        }
    })
}

function getHashMap(array){
    var hashMap = {};

    _.each(array,function (el) {
        hashMap[el.name] = el.weight;
    })

    return hashMap;
}

function setCache(key,data){
    //Storing for 30 min
    redisClient.setex(key, 1800, JSON.stringify(data));
}

function getCache(key,callback){
    redisClient.get(key, function (error, result) {
        try {
            var data = JSON.parse(result);
            callback(data)
        } catch (err) {
            callback(null)
        }
    });
}
