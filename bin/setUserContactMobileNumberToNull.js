var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  , url = 'mongodb://localhost:27017/July'
  , database = null
  , ObjectID = require('mongodb').ObjectID

var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();


MongoClient.connect(url, function(err, db){
    database = db;
    fetchContacts(0)
});

var fetchContacts = function(skip){
  database.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
    console.log("Processing records " + (skip + 1))
    var userCollection = database.collection("user");

    // userCollection.find().sort({_id: 1}).skip(skip).limit(1).toArray(function(err, contacts){
    userCollection.find({emailId: 'naveenpaul.markunda@gmail.com'}).sort({_id: 1}).skip(skip).limit(1).toArray(function(err, contacts){

      if(validateEmail(contacts[0].emailId)){
        var bulk = userCollection.initializeUnorderedBulkOp();

        var userId = new ObjectID(contacts[0]._id.toString())

        _.each(contacts[0].contacts,function (contact) {

          var updateObject = {"contacts.$.mobileNumber":null};

          bulk.find({_id:userId,"contacts.personEmailId": contact.personEmailId,"contacts.mobileNumber": {$in:[false,null,'',' ']}}).update({ $set: updateObject});
        });

        // if(bulk.length>0){
          bulk.execute(function(err,res){
            if(err) {
              console.log(err)
              fetchContacts(skip + 1) // Don't stop for anyone
            }
            else{
              var result = res.toJSON();
              console.log(result)
              fetchContacts(skip + 1)
            }
          });
        // } else {
        //   console.log("No operations")
        //   fetchContacts(skip + 1)
        // }
      }
    })
  })
};

function validateEmail(email) {
  var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(email);
}

