var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var date = require('date-and-time');
var timediff = require('timediff');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
    //     mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/Relatas", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                console.log('authenticated')
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var interactionsV2Collection = db.collection('interactionsV2');
                var companyCollection = db.collection('company');

                var runForSingleUser = function (skip) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}).skip(0).limit(1).toArray(function (err, user) {
                    usersCollection.find({}).skip(skip).limit(1).toArray(function (err, user) {

                        if(!err && user && user.length>0){
                            var userProfile = user[0];

                            console.log("*************    Start   ***********",skip)
                            console.log(userProfile.emailId)

                            updateStart(userProfile,interactionCollection,interactionsV2Collection,function (err,result) {

                                console.log("*************   Completed    *********",skip)
                                runForSingleUser(skip+1)
                            })

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

function updateStart(userProfile,interactionCollection,interactionsV2Collection,callback) {

    var query = {
        ownerId:userProfile._id,
        userId:{$ne:userProfile._id}
    };

    interactionCollection.find(query).toArray(function(err, interactions){

        if(!err && interactions && interactions.length>0){

            interactions.forEach(function(el){
                var reverseAction = "receiver"
                if(el.action == "receiver"){
                    reverseAction = "sender"
                }
                el.action = reverseAction
                delete el.userId;
                delete el._id;
            });

            console.log(interactions.length)

            updateInteractions(interactions,interactionsV2Collection,callback)
        } else {
            callback()
        }
    })

}

function updateInteractions(interactions,interactionsV2Collection,callback) {
    interactionsV2Collection.insert(interactions,{ordered:false},function (err,result) {
        callback()
    })

    // callback()
}

start();