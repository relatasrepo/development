var FeedParser = require('feedparser')
  , request = require('request')
  , _ = require("lodash")
  , Entities = require('html-entities').XmlEntities
  , striptags = require('striptags')
  , url = require('url')
  , MongoClient = require('mongodb').MongoClient
  , entities = new Entities()
  , each = require('async-each')
  , connection = null
  , limit = 50
  , skip = 0
  , dbUrl = 'mongodb://localhost:27017/Relatas'
  
//var appCredentials = require('../config/relatasConfiguration')
  //, appCredential = new appCredentials()
  //, authConfig = appCredential.getAuthCredentials()
  //, dbPasswords = appCredential.getDBPasswords()

Array.prototype.chunk = function(chunkSize) {
    var chunkData = [];
    for (var i=0; i<this.length; i+=chunkSize)
        chunkData.push(this.slice(i,i+chunkSize));
    return chunkData;
}

function processCompanies(skipRecords){

  if(_.isUndefined(skipRecords))
    skip = skip + limit
console.log("processing companies - " + skip)
  companiesCollection = connection.collection('socialfeed')

  companiesCollection.find({entityType: "company", rssFeed: {$exists:true}}, {rssFeed: 1, entityName: 1, _id: 1, newsFeeds: 1}).skip(skip).limit(limit).toArray(function(err, data){
     if(!data || data.length == 0){
       console.log("News feeds population completed") 
       return
     }
     else{
       chunkedCompanies = data.chunk(limit)
       each(chunkedCompanies, iterateCompanies, function(err){
          if(err){
            console.log(err)
          }else
            processCompanies()
       })
     }
  })
}

function iterateCompanies(companies, next){
  each(companies, fetchNewsFeeds, function(err, feeds){
    if(err){
      console.log(err)
    }else{
      groupedFeeds = _.groupBy(_.flatten(feeds), "id")
      console.log(groupedFeeds)
      var batch = companiesCollection.initializeUnorderedBulkOp()
      _.each(companies, function(company){
        feedsForCompany = _.sortBy(groupedFeeds[company._id], function(f){return -f.publishedDate}).slice(0,5)
        batch.find({_id: company._id}).upsert().updateOne({$set: {newsFeeds: feedsForCompany} });
      })
      batch.execute(function(err, result) {
        if(err){
          console.log(err)
          console.log("Error in batch updation")
        }else
          next()
      })
    }

  }) 
}
function fetchNewsFeeds(company, next){
  var req = request(company.rssFeed)
    , feedparser = new FeedParser([])
    , feeds = []

  req.on('error', function (err) {
    next(err, null)
  });

  req.on('response', function (res) {
    var stream = this;
    if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));
    stream.pipe(feedparser);
  });
  req.on('end', function(err){
    if(err){
      console.log("Error while retriving feeds")
      console.log(err)
    }
    next(err, feeds)
  })

  feedparser.on('error', function(error) {
  });
  feedparser.on('readable', function() {
    var stream = this
      , meta = this.meta 
      , item;

    while (item = stream.read()) {
        feeds.push({
          title: striptags(entities.decode(item.title)) ,
          description: striptags(entities.decode(item.description.replace(/&nbsp;\.+$/, "..."))),
          publishedDate:  item.pubdate,
          url: url.parse(item.link,true).query.url,
          guid: item.guid,
          name: company.entityName,
          id: company._id
        })
      }
  });
}

var initate = function(){
  MongoClient.connect(dbUrl, function(err, db) {
    connection = db
    skip = 0
    console.log("Starting")
    //database.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
      if(err){
        console.log("Error authenticating DB")
        console.log(err)
      }else
        processCompanies(0)
    //})
  })
}

module.exports = initate

/* 
 *
 * usage of this script 
 *
 */

//var populateNewFeeds = require("./bin/populateNewFeeds.js")
//var CronJob = require('cron').CronJob;
/* This cron starts at 2 AM everyday*/
//new CronJob('0 2 * * *', function() {
  //populateNewFeeds();
//}, null, true);


/* 
 *
 *
 * Use the following script to add few Companies 
 *
 *
 */

 //db.socialfeed.insert([
  //{entityType: "company", entityName: "Google", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/7839275712476625770"}
//, {entityType: "company", entityName: "Microsoft", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/6836551200689492346"}
//, {entityType: "company", entityName: "Amazon", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/7184857435766245443"} 
//, {entityType: "company", entityName: "Infosys", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/8931703885012386849"} 
//, {entityType: "company", entityName: "Apple", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/6836551200689494276"} 
//, {entityType: "company", entityName: "Wipro", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/7184857435766244878"} 
//, {entityType: "company", entityName: "TCS", rssFeed: "https://www.google.co.in/alerts/feeds/12824586406363221877/8931703885012387118"} 
//])

