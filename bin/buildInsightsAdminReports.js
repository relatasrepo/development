var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async')

var insightsAdminReportsCollection;
var usersCollection;
var companyCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('insightsAdminReports is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@mongodb://10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            // var db = client.db('newDb');
            var db = client.db('Relatas');
            // var db = client.db('Relatas_Prod_01Aug');
            // console.log("Connected to db");
            usersCollection = db.collection('user');
            companyCollection = db.collection('company');
            insightsAdminReportsCollection = db.collection('insightsAdminReports');

            var runForSingleCompany = function (skip) {
                companyCollection.find({}).skip(skip).limit(1).toArray(function (err, company) {
                    if(!err && company && company.length>0){
                        var companyId = company[0]._id;
                        getUserProfiles(companyId,function (err1,users) {
                            if(users && users[0]){
                                userFiscalYear(company[0],users[0],function (fyMonth,fyRange,allQuarters) {
                                    if(!err1 && users && users.length) {
                                        async.eachSeries(users, function (user, next) {
                                            reportForSingleUser(user,company[0],fyMonth,fyRange,allQuarters,function(){
                                                console.log("Done ",user.emailId);
                                                next();
                                            });
                                        }, function(err) {
                                            saveInsightsAdminReportsForTeam(companyId,function () {
                                                runForSingleCompany(skip+1);
                                            })
                                        });
                                    } else {
                                        runForSingleCompany(skip+1);
                                    }
                                })
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---");
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

startScript();

function reportForSingleUser(user,company,fyMonth,fyRange,allQuarters,callback){

    insightsAdminReportsCollection.find({
        companyId:user.companyId,
        ownerId: user._id,
        forTeam:false,
        dayString: moment("10 Dec 2018").format("DDMMYYYY")
        // dayString: moment("04 Jan 2019").format("DDMMYYYY")
    }).toArray(function(err,data){

        if(!err && data && data.length>0){

            var forDates = [];
            var d = new Date(moment("11 Nov 2018"));
            // forDates.push(d);
            var count = 0;
            var toUpdate = [];

            while(count<7){
                count++;
                d = moment(d).add(1,'week');
                toUpdate.push({
                    companyId: data[0].companyId,
                    dayString: moment(d).format("DDMMYYYY"),
                    forTeam: data[0].forTeam,
                    ownerEmailId:data[0].ownerEmailId,
                    ownerId:data[0].ownerId,
                    date:new Date(d),
                    dealsAtRisk:data[0].dealsAtRisk ,
                    dealsAtRiskAction:data[0].dealsAtRiskAction,
                    dealsAtRiskImpact:data[0].dealsAtRiskImpact ,
                    losingTouch:data[0].losingTouch ,
                    losingTouchImpact:data[0].losingTouchImpact ,
                    oppREConversion:data[0].oppREConversion ,
                    pipelineVelocity:data[0].pipelineVelocity
                })
            }

            insightsAdminReportsCollection.insert(toUpdate,function (err,result) {
                callback();
            })
        } else {
            callback();
        }
    });
}

function saveInsightsAdminReportsForTeam(companyId,callback) {
    insightsAdminReportsCollection.find({
        companyId:companyId,
        forTeam:true,
        dayString: moment("10 Dec 2018").format("DDMMYYYY")
        // dayString: moment("04 Jan 2019").format("DDMMYYYY")
    }).toArray(function(err,data){

        if(!err && data && data.length>0){

            var d = new Date(moment("11 Nov 2018"));
            // forDates.push(d);
            var count = 0;
            var toUpdate = [];

            while(count<7){
                count++;
                d = moment(d).add(1,'week');
                toUpdate.push({
                    companyId: data[0].companyId,
                    dayString: moment(d).format("DDMMYYYY"),
                    forTeam: data[0].forTeam,
                    ownerEmailId:data[0].ownerEmailId,
                    ownerId:data[0].ownerId,
                    date:new Date(d),
                    dealsAtRisk:data[0].dealsAtRisk ,
                    dealsAtRiskAction:data[0].dealsAtRiskAction,
                    dealsAtRiskImpact:data[0].dealsAtRiskImpact ,
                    losingTouch:data[0].losingTouch ,
                    losingTouchImpact:data[0].losingTouchImpact ,
                    oppREConversion:data[0].oppREConversion ,
                    pipelineVelocity:data[0].pipelineVelocity
                })
            }

            insightsAdminReportsCollection.insert(toUpdate,function (err,result) {
                callback();
            })
        } else {
            callback();
        }
    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

var userFiscalYear = function (company,user,callback) {

    if(company && user){

        var fyMonth = "April"; //Default
        if (!company || !company.fyMonth) {
        } else {
            fyMonth = company.fyMonth
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if(currentMonth<monthNames.indexOf(fyMonth)){
            fromDate.setFullYear(currentYr-1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month');

        var timezone = user.timezone && user.timezone.name?user.timezone.name:"UTC";

        callback(company.fyMonth,
            {start:moment(fromDate).tz(timezone).format(),end: moment(toDate).tz(timezone).format()},
            setQuarter(fyMonth,timezone,fromDate,toDate),
            timezone)
    } else {
        callback(null,null,null,null)
    }
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function setQuarter(startMonth,timezone,fyStartDate,fyEndDate) {

    var qtrObj = {};

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var months = [];
    months.push(fyStartDate)

    _.each(monthNames,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(moment(months[0]).startOf('month')).tz(timezone).format(),end:moment(moment(months[2]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter2 = {start:moment(moment(months[3]).startOf('month')).tz(timezone).format(),end:moment(moment(months[5]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter3 = {start:moment(moment(months[6]).startOf('month')).tz(timezone).format(),end:moment(moment(months[8]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter4 = {start:moment(moment(months[9]).startOf('month')).tz(timezone).format(),end:moment(moment(months[11]).endOf('month')).tz(timezone).format()}

    var currentMonth = moment().month()
    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    return qtrObj
}