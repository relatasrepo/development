var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  , url = 'mongodb://localhost:27017/Relatas'

MongoClient.connect(url, function(err, db){
  db.authenticate("devuserre", "devpasswordre", function(err, res) {

    var user = db.collection("user");
    // var interactions = db.collection("interactions");

    // var matchString = new RegExp("sudip", 'i')
    var matchString = [/sudip/i];

    user.find({corporateUser:true},{_id:1,emailId:1}).toArray(function (err,users) {
      var userIds = _.pluck(users,"_id");
      
      user.aggregate([
        {
          $match:{
            _id:{$in:userIds}
          }
        },
        {
          $unwind:"$contacts"
        },
        {
          $match:{
            "contacts.personEmailId":"sudip@relatas.com",
            "contacts.personName":{$nin:matchString}
          }
        },
        {
          $project:{
            _id:"$emailId",
            contactEmailId:"$contacts.personEmailId",
            name:"$contacts.personName"
          }
        }
      ],{allowDiskUse: true},function (err,users) {

        console.log(err)
        console.log(users)

      })
    })
  })
});

