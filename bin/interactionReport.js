var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  // , url = 'mongodb://localhost:27017/JulyCopy'
  , url = 'mongodb://172.31.38.246:27017/Relatas'
  , database = null
    , ObjectId = require('mongodb').ObjectID

var interactionsArray = []

var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();


MongoClient.connect(url, function(err, db){

    database = db
    fetchInteractions(0)
})  

var fetchInteractions = function(skip){

    database.collection("user").find({companyId:ObjectId("5819881af58659760546d3fa")},{_id:1,emailId:1}).skip(skip).limit(1).toArray(function(err, userProfile){

      if(userProfile[0] && userProfile[0]._id) {

        database.collection("interactions").aggregate([
          {
            $match:{
              ownerId:userProfile[0]._id,
              userId:{$ne:"$ownerId"}
            }
          },
          { "$project": {
            "ownerEmailId": 1,
            "week": { "$week": "$interactionDate" }
          }},
          { "$group": {
            "_id": {week:"$week",ownerEmailId:"$ownerEmailId"},
            "total": { "$sum": 1 }
          }},
          {
            $project:{
              _id:"$_id.ownerEmailId",
              week:"$_id.week",
              interactionsCount:"$total"
            }
          }
        ],function (err,interactionsByUser) {
          console.log("Skip - ",skip)
          if(!err && interactionsByUser.length>0){
            interactionsArray.push(interactionsByUser)
            // database.collection("aaaaa").insert({emailId:userProfile[0].emailId,report:interactionsByUser}, function(error, result){

            var collection = database.collection("aaaaa")
            var batch = collection.initializeOrderedBulkOp()

            _.each(interactionsByUser, function(interaction){
                interaction._id = new ObjectId()
                interaction.emailId = userProfile[0].emailId
                console.log(interaction)
                batch.insert(interaction)
            })

              batch.execute(function(err, data){

                if(userProfile.length>0 && !err){
                  fetchInteractions(skip+1)
                } else {
                  console.log("Done--")
                  fetchInteractions(skip+1)
                }
              })
          } else {
            fetchInteractions(skip+1)
          }
        })

      } else {
        console.log(interactionsArray.length)
      }
    });
}


