var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var fs = require('fs');
var json2xls = require('json2xls');

var OppMetaDataCollection;
var opportunitiesCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('oppMetaData is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.21.21:27017/Relatas", function(error, client) { // This is LIVE
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
    // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            OppMetaDataCollection = db.collection('oppMetaData');

            var runForSingleUser = function (skip) {
                // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}, {
                usersCollection.find({corporateUser: true}, {
                    emailId: 1,
                    _id: 1,
                    companyId: 1
                }).skip(skip).limit(1).toArray(function (err, user) {

                    if(!err && user && user.length>0){
                        var companyId = user[0].companyId
                        var userEmailId = user[0].emailId;

                        console.log(userEmailId,skip);
                        updateMetaData(companyId,userEmailId,function () {
                            runForSingleUser(skip+1);
                        })
                    } else {
                        console.log("Done");
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

startScript();

function updateMetaData(companyId,userEmailId,callback){

    var forDate = moment().subtract(1,"month");

    var month = moment().month()
        ,year = moment().year()
        ,dateMin = moment().startOf('month')
        ,dateMax = moment().endOf('month');

    var query = {
        userEmailId:userEmailId
    }

    OppMetaDataCollection.find({companyId:companyId,userEmailId:userEmailId,month:month,year:year}).toArray(function (err,oppMeta) {

        var data = [];
        _.each(oppMeta[0].data,function (opM) {
            _.each(opM.oppIds,function (op) {
                op.stageName = opM.stageName;
                data.push(op);
            });
        });

        var xls = json2xls(data);
        console.log(data.length)
        fs.writeFileSync(userEmailId+'_opp_metadata.xlsx', xls, 'binary');
    })

}