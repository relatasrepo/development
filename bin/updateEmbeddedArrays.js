
var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

//var async = require('async');

var emailId = process.argv[2];
var dbUrl = process.argv[3];

console.log("User Email ID is : " + emailId);
console.log("MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connect")
    if (err) {
        console.log(err)
    }

    db.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
        var collection = db.collection("user");
        collection.aggregate(
            {$match: {emailId: emailId}},
            {$unwind:"$contacts"},
            { "$match": {
                "contacts.account.value.inProcess": {$gte:1}}
            },
            { "$group": {
                "_id": "$_id",
                "count": { "$sum": 1 }
            }},
            { "$group": {
                "_id": null,
                "count": { "$max": "$count" }
            }},function(err,res){
                if(res.length>0){
                    console.log("Found "+res[0].count+ " contacts to update.")
                    var max = res[0].count;
                    while ( max-- ) {
                        collection.update(
                            {
                                "contacts.account.value.inProcess": {$gte:1}
                            },
                            { "$set": { "contacts.$.account.value.inProcess": 0 }},
                            { "multi": true },function(err,res){
                                if(err){
                                    console.log("Whoops! " + err)
                                } else {
                                    console.log("Yay! " + res)
                                }
                            }
                        )
                        }
                } else {
                    console.log("Nothing to update. Press 'CTRL C' to exit.")
                }
            }//End Function
        )//End Aggregate
    })
})