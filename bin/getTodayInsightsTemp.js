// require('newrelic');
var mongoose = require('mongoose');
var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var async = require('async')
// var heapdump = require('heapdump')

var interactionsCollection;
var opportunitiesCollection;
var accInteractionCollection;
var usersCollection;
var rhCollection;
var companyCollection;
var dealsAtRiskMetaCollection;
var OppMetaDataCollection;
var dashboardInsightsCollection;
var opportunitiesTargetCollection;
var accountInsightsCollection;
var newCompaniesInteraction;
var scheduleInvitation;
var taskCollection;
var InteractionsCollection;
var todayInsightsCollection;

// var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;
// var opportunitiesCollection = require('../databaseSchema/userManagementSchema').opportunities;
// var taskCollection = require('../databaseSchema/taskSchema').tasks;
// var InteractionsCollection = require('../databaseSchema/userManagementSchema').interactions
// var scheduleInvitation = require('../databaseSchema/userManagementSchema').scheduleInvitation;
// var todayInsightsCollection = require('../databaseSchema/userManagementSchema').todayInsights;

var googleCalendarAPI = require('../common/googleCalendar');
var ContactManagement = require('../dataAccess/contactManagement');
var outlookSync =  require('../routes/office365_bip.js');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');
var _ = require("lodash")
var moment = require("moment")

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Today insights is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@mongodb://10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            // var db = client.db('newDb');
            var db = client.db('Relatas');
            // var db = client.db('Relatas_Prod_01Aug');
            // console.log("Connected to db");
            usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            interactionsCollection = db.collection('interactionsV2');
            accInteractionCollection = db.collection('accInteraction');
            newCompaniesInteraction = db.collection('newCompaniesInteraction');
            rhCollection = db.collection('revenueHierarchy');
            companyCollection = db.collection('company');
            dealsAtRiskMetaCollection = db.collection('dealsAtRiskMeta');
            OppMetaDataCollection = db.collection('oppMetaData');
            opportunitiesTargetCollection = db.collection('opportunitiesTarget');
            dashboardInsightsCollection = db.collection('dashboardInsight');
            accountInsightsCollection = db.collection('accountInsight');
            // taskCollection = db.collection('tasks');
            taskCollection = require('../databaseSchema/taskSchema').tasks;
            InteractionsCollection = db.collection('interactions'); 
            scheduleInvitation = db.collection('scheduleInvitation');
            todayInsightsCollection = db.collection('todayInsights');

            var runForSingleCompany = function (skip) {
                companyCollection.find({companyName:"mobisoft"}).skip(skip).limit(1).toArray(function (err, company) {

                    if(!err && company && company.length>0){
                        console.log(company[0].url)
                        var companyId = company[0]._id;
                        getUserProfiles(companyId,function (err1,users) {
                            if(users[0]){
                                // allQuarters["currentQuarter"] = "quarter1"; //TODO Remove this after qtr1 data
                                console.log("Company Users:",users.length)

                                if(!err1 && users && users.length){
                                    // async.eachSeries(users, function (user, next){
                                    //     insightsForUser(user,company[0],function() {
                                    //         console.log("Done ",user.emailId);
                                    //         next();
                                    //     });

                                    // }, function(err) {
                                    //     console.log('All user done for company',company[0].url);
                                    // });
                                    getTasksOverdue("kemparajutest@outlook.com", function(error, tasks) {
                                        console.log(tasks);
                                    })


                                } else {
                                    // runForSingleCompany(skip+1);
                                }
                            } else {
                                // runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---");
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

startScript();

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId, emailId:"kemparajutest@outlook.com"}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

function insightsForUser(user,company,callback) {

    var primaryCurrency = "USD",
        currenciesObj = {}

    company.currency.forEach(function (el) {
        currenciesObj[el.symbol] = el;
        if(el.isPrimary){
            primaryCurrency = el.symbol;
        }
    });

    // var qStart = allQuarters[allQuarters.currentQuarter].start;
    // var qEnd = allQuarters[allQuarters.currentQuarter].end;

    async.parallel([
        function (callback) {
            dealsAtRisk([user.userEmailId],primaryCurrency,currenciesObj, callback);
        }
    ],function (err,data) {
        if(err){
            console.log("---Today Insights For User---");
            console.log(err);
        }

        // insightsBuildAndUpdate(data,company,primaryCurrency,currenciesObj,fyRange,user,allQuarters,false,[user.emailId],function () {
        //     callback();
        // });

        todayInsightsBuildAndUpdate(user.emailId, data, function() {
            callback();
        })
    })
}


function todayInsightsBuildAndUpdate(userEmailId, data, callback) {
    console.log("Data:", data);
    callback();
}


function getStaleOpportunities(userId, callback) {
    var match = {
        userId:userId,
        closeDate:{$lte:new Date()},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    opportunitiesCollection.aggregate([
        {
            $match:match
        },
        {
            $group:{
                _id:null,
                count:{$sum:1}
            }
        }
    ],function(error, result){
        if(error){
            console.log("Error in stale opps");
            // loggerError.info('Error in staleOpportunitiesCount():opportunitiesManagement ',error)
            callback(error,{count: 0});
        }
        else{
            callback(error,{
                count:result[0] ? result[0].count : 0 
            });
        }
    });
}

function getTasksOverdue(emailId, callback) {
    var query = {
        assignedToEmailId:{$in:emailId}
    };

    query.dueDate = {
        $lt: new Date()
    }

    query.complete = {$nin:['complete']}

    taskCollection.find(query,null,function(err,tasks){
        console.log("Inside the task collection function:", tasks);
        if(err){
            console.log("Error in getting task");
            callback(err, {overdueTasks:0});
        }
        callback(err,{overdueTasks:tasks.length})
    })
}

function getTasksUpcoming(emailId, callback) {
    var query = {
        assignedToEmailId:{$in:emailId},
        dueDate: {$gt: new Date()},
    }

    taskCollection.find(query, null, function(err, tasks) {
        if(err) {
            console.log("error in getting upcoming tasks");
            callback(err, {upcomingTasks:tasks.length});
        }
        callback(err, {upcomingTasks:tasks.length});
    })
}

function getImpMailPendingResponse(userId, callback) {
    var from  = moment().subtract(14, "days");

    getImportantMails(userId, from, function(err, mails) {
        if(!err && mails && mails.length>0){
            var filteredMails = filterByOnlyRecentInteractionThread(mails)

            var threadIds = _.pluck(filteredMails,"emailThreadId");

            var threadsHaveLatestReplies = [],tempMails = [];

            checkLiuRespondedToLastEmail(userId,from,threadIds,function (err,prevEmails) {

                var important = 0,onlyToMe = 0,followUp = 0,positive = 0,negative = 0;
                var emailContentIds = [];
                var response = {};

                _.each(prevEmails,function (el) {
                    if(el.emailId != el.ownerEmailId && el.action == "receiver" ){
                        threadsHaveLatestReplies.push(el.emailThreadId)
                    }
                })

                _.each(filteredMails,function (el) {
                    if(!_.includes(threadsHaveLatestReplies,el.emailThreadId)){
                        tempMails.push(el)
                    }
                })

                _.each(tempMails,function (el) {

                    if((el.eAction && el.eAction == 'important') || el.importance>0){
                        important++;
                        emailContentIds.push(el.emailContentId);
                    }

                    response.important = important;
                    response.contentIds = emailContentIds

                    callback({"Imp mails count:": response.important});
                })

            });
        }
    })
}

function getImportantMails(userId,from, callback) {

    InteractionsCollection.aggregate([
        {
            $match: {
                ownerId:userId,
                userId:{$ne: userId},
                interactionDate:{$gte:new Date(from),$lte:new Date()},
                $or:[
                    {toCcBcc: 'me'},
                    {eAction: 'important'},
                    {importance: {'$gt': 0}},
                    // {"trackInfo.trackResponse":true}
                    {$and:[{"trackInfo.trackResponse":true},{createdDate:{$lte:new Date(moment().subtract(7, "days").toDate())}}]}
                ],
                notImportant:{$ne:true},
                hasUnsubscribe:{$ne:true}
            }
        },
        {
            $project:{
                importance:1,
                toCcBcc:1,
                sentiment:1,
                trackInfo:1,
                interactionDate:1,
                emailThreadId:1,
                title:1,
                action:1
            }
        }
    ]).exec(function(error, interactions){
        callback(error, interactions);
    })
}

function filterByOnlyRecentInteractionThread(interactions){
    var filteredInteractions = _
        .chain(interactions)
        .groupBy('emailThreadId')
        .map(function(value, key) {
            return _.max(value,"interactionDate")
        })
        .value();

    return filteredInteractions;
}

function checkLiuRespondedToLastEmail(userId,from,emailThreadIds,callback) {

    InteractionsCollection.aggregate(
        [{
            $match: {
                ownerId:userId,
                userId:{$ne:userId},
                emailThreadId:{$in:emailThreadIds},
                interactionDate:{$gte:new Date(from),$lte:new Date()},
                interactionType:"email"
            }
        },
        {
            $group:{
                _id:"$emailThreadId",
                interactions:{
                    $push:{
                        ownerEmailId:"$ownerEmailId",
                        emailId: "$emailId",
                        interactionDate: "$interactionDate",
                        emailThreadId: "$emailThreadId",
                        action: "$action",
                        title: "$title",
                        cc:"$cc",
                        to:"$to"
                    }
                }
            }
        }
    ]).exec(function(error, interactions){
        if(!error && interactions){
            getLatestMailInThread(interactions,callback)
        }
    })
}

function getLatestMailInThread(interactions,callback) {

    var latestReplies = [];
    _.each(interactions,function (intThread) {
        intThread.interactions.sort(function (o2, o1) {
            return new Date(o1.interactionDate) > new Date(o2.interactionDate) ? 1 : new Date(o1.interactionDate) < new Date(o2.interactionDate) ? -1 : 0;
        });

        if(intThread.interactions[0] && (intThread.interactions[0].to && intThread.interactions[0].to.length == 1 || intThread.interactions[0].cc && intThread.interactions[0].cc.length == 1)){
            latestReplies = latestReplies.concat(intThread.interactions[0])
        } else {
        }
    })

    callback(null,latestReplies)
}

function getUpcomingMeetings(userId, zone, findOutlookOrGoogle, callback) {
    var timezone;
        if(checkRequired(zone) && checkRequired(zone.name)){
            timezone = zone.name;
        }else timezone = 'UTC';

        var date = moment().add(1, "days");
        if(checkRequired(date)){
            date = new Date(date).toISOString();
        }
        //emailSenderObj.sendUncaughtException('',date,JSON.stringify(req.body),'TODAY_MEETINGS 2 '+user.emailId)
        var dateMin = checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);
        var dateMax = checkRequired(date) ? moment(date).tz(timezone):moment().tz(timezone);

        dateMin.date(dateMin.date())
        dateMin.hour(0)
        dateMin.minute(0)
        dateMin.second(0)
        dateMin.millisecond(0)

        dateMax.date(dateMax.date())
        dateMax.hour(23)
        dateMax.minute(59)
        dateMax.second(59)
        dateMax.millisecond(59)

        dateMin = dateMin.format();
        dateMax = dateMax.format();

        getPendingMeetingsByDate(userId,dateMin,dateMax,findOutlookOrGoogle,function(todayMeetings){
            callback(todayMeetings);
        });

}

function getPendingMeetingsByDate(rId,dateMin,dateMax,findOutlookOrGoogle,callback){
    var projection = {senderId:1,senderName:1,senderEmailId:1}
    dateStart = new Date(dateMin);
    dateEnd = new Date(dateMax);

    if(findOutlookOrGoogle == 'outlook'){
        userCollection.findOne({_id:castToObjectId(rId)},{emailId:1}).exec(function(err, user){
            if(err){
                logger.info('Error in  userMeetingsByDate(): ',err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                deleted: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                senderId: rId,
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            }
                        ]
                    },
                    projection,
                    function(error,invitations){
                        if(error){
                            callback(error, {
                                upcomingMeetings: 0
                            })
                        }
                        callback(error, {
                            upcomingMeetings:invitations.length
                        });
                    });
            }
        })

        } else {

            scheduleInvitation.find({
                    isGoogleMeeting:true,
                    $or: [
                        {
                            "to.receiverId":rId,
                            "to.canceled":{$ne: true},
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            deleted: {$ne: true},
                            toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            senderId: rId,
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}},
                        }
                    ]
                },
                projection,
                function(error,invitations){
                    if(error){
                        callback(error, {
                            upcomingMeetings: 0
                        })
                    }
                    callback(error, {
                        upcomingMeetings:invitations.length
                    });
                });
    }

    // userMeetingsByDate(userId,new Date(dateMin),new Date(dateMax),projection,findOutlookOrGoogle,function(meetings){
    //     if(common.checkRequired(meetings) && meetings.length > 0){
    //         callback(meetings)
    //     }else callback([])
    // })
}

function userMeetingsByDate(rId,dateStart,dateEnd,projection,findOutlookOrGoogle,callback) {

    if(findOutlookOrGoogle == 'outlook'){
        userCollection.findOne({_id:castToObjectId(rId)},{emailId:1}).exec(function(err, user){
            if(err){
                logger.info('Error in  userMeetingsByDate(): ',err);
            }
            else {
                scheduleInvitation.find({
                        "isOfficeOutlookMeeting":true,
                        "LIUoutlookEmailId":user.emailId,
                        $or: [
                            {
                                "to.receiverId":rId,
                                "to.canceled":{$ne: true},
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                deleted: {$ne: true},
                                toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            },
                            {
                                senderId: rId,
                                deleted: {$ne: true},
                                "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                            }
                        ]
                    },
                    projection,
                    function(error,invitations){
                        if(error){
                            logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                        }
                        callback(invitations);
                    });
            }
        })

        } else {

            scheduleInvitation.find({
                    isGoogleMeeting:true,
                    $or: [
                        {
                            "to.receiverId":rId,
                            "to.canceled":{$ne: true},
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            deleted: {$ne: true},
                            toList: {$elemMatch: {receiverId: rId, canceled: {$ne: true}}},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                        },
                        {
                            senderId: rId,
                            deleted: {$ne: true},
                            "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}},
                        }
                    ]
                },
                projection,
                function(error,invitations){
                    if(error){
                        logger.info('Error in  userMeetingsByDate():MeetingManagementClass ',error);
                    }
                    callback(invitations);
                });
    }
};


function dealsAtRisk(emailId,primaryCurrency,currenciesObj,callback) {

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMetaCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}).sort({date:1}).toArray(function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null;

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        }

        callback(err,{
            count:sumBy(deals,"count"),
            amount:totalDealValueAtRisk,
            dealsRiskAsOfDate:dealsRiskAsOfDate
        })
    })
}

function dealsAtRisk(emailId,primaryCurrency,currenciesObj,callback) {

    var from = moment().subtract(0, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskMetaCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}).sort({date:1}).toArray(function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null;

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        el.convertedAmt = parseFloat(el.convertedAmt.toFixed(2))

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        }

        callback(err,{
            count:sumBy(deals,"count"),
            amount:totalDealValueAtRisk,
            dealsRiskAsOfDate:dealsRiskAsOfDate
        })
    })
}

function initScheduler() {

    var sendDailyAgendaRuleZone1 = new schedule.RecurrenceRule(); //00:45 AM IST

    sendDailyAgendaRuleZone1.dayOfWeek = [0,new schedule.Range(1, 6)];
    sendDailyAgendaRuleZone1.hour = 23;  //19:15 UTC
    sendDailyAgendaRuleZone1.minute = 55;

    var sendDailyAgendaRuleObjectZone1 = schedule.scheduleJob(sendDailyAgendaRuleZone1, function(){
        console.log('Agenda mail Kicked off for zone ONE');
        runForSingleUser(0,1);
    });

}

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id);
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined || data == "undefined") {
        return false;
    }
    else{
        return true;
    }
}

function numberWithCommas(x,ins) {

    if(ins){
        return numberWithCommas_ins(x);
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}

function formatNumber(num) {

    if(num && num.toString().length>2){

        if(num % 1 != 0){
            return num.toFixed(2)
        } else {
            return num
        }
    } else {
        return num;
    }
}

function numberWithCommas_ins(x) {
    x=String(x).toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function checkRequired(data){
    if (data == '' || data == null || data == undefined || data == "undefined") {
        return false;
    }
    else{
        return true;
    }
}