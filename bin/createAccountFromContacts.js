var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();

rule.hour = 1;
rule.minute = 30;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Relationship Relevance is running');
    start();
});

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
    //     mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/Relatas", function(error, db) {
    //     mongoClient.connect("mongodb://localhost/Sep", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var userRelationCollection = db.collection('userRelation');
                var accountsCollection = db.collection('accounts');
                var companyCollection = db.collection('company');
                var oppsCollection = db.collection('opportunities');

                var runForSingleUser = function (skip) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"},{contacts:1,emailId:1,companyId:1}).skip(0).limit(1).toArray(function (err, user) {
                    // usersCollection.find({emailId: "naveenpaul@relatas.com"},{contacts:1,emailId:1}).skip(skip).limit(1).toArray(function (err, user) {
                    // companyCollection.find({},{_id:1}).skip(skip).limit(1).toArray(function (err, company) {
                    usersCollection.find({corporateUser: true}).skip(skip).limit(1).toArray(function (err, user) {

                        // if(!err && company && company.length>0){
                        //
                        //     getAccountsByOpp(company[0]._id,oppsCollection,usersCollection,function (contactObjs) {
                        //         console.log(JSON.stringify(contactObjs,null,1));
                        //         // updateStartForInteractions(company[0]._id,contactObjs,accountsCollection,function (err,results) {
                        //         //     runForSingleUser(skip+1)
                        //         // });
                        //
                        //         orderContactsByInteractionsFrequencies(company[0]._id,usersCollection,interactionCollection,skip,function (contactObjs) {
                        //             updateStartForInteractions(company[0]._id,contactObjs,accountsCollection,function (err,results) {
                        //                 runForSingleUser(skip+1)
                        //             });
                        //         });
                        //     });
                        // } else {
                        //     runForSingleUser(skip+1)
                        // }

                        //for contacts

                        if(!err && user && user.length>0 && user[0]){
                            var userProfile = JSON.parse(JSON.stringify(user[0]));
                            console.log("Completed >",userProfile.emailId,skip)

                            if(userProfile.contacts && userProfile.contacts.length>0){
                                updateStart(userProfile,userProfile.contacts,accountsCollection,function (err,result) {
                                    runForSingleUser(skip+1)
                                })
                            } else {
                                runForSingleUser(skip+1)
                            }

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

start();

function orderContactsByInteractionsFrequencies(companyId,usersCollection,interactionCollection,skip,callback){

    getTeamMembers(companyId,usersCollection,function (users) {
        getInteractionsByContactAndUser(users,interactionCollection,companyId,function (err,accUpdateObj) {
            callback(accUpdateObj)
        });
    });
}

function getInteractionsByContactAndUser(users,interactionCollection,companyId,callback){

    var queryFinal = { "$or": users.map(function(el) {
        var obj = {}
        obj["ownerEmailId"] = el;
        obj["$and"] = [{emailId:{"$ne": el}},{emailId:{"$ne": null}}];

        return obj;
    })};

    interactionCollection.aggregate(
        {
            $match:queryFinal
        },{
            $group:{
                _id:{ownerEmailId:"$ownerEmailId",ownerId:"$ownerId",contact:"$emailId"},
                interactionCount:{$sum:-1}
            }
        },{
            $sort:{interactionCount:1}
        },
        function(err, interactionResult){
            if(!err && interactionResult){
                callback(err,removeOthers(interactionResult,companyId))
            } else {
                callback(err,null)
            }
        });
}

function removeOthers(interactions,companyId){
    var validContacts = [],names=[],contacts=[];
    var updateObj = {};

    _.each(interactions,function (el) {

        if(el){
            var acc = fetchCompanyFromEmail(el._id.contact);

            if(acc){
                names.push(acc);
                contacts.push(el._id.contact);

                validContacts.push({
                    companyId:companyId,
                    ownerId:el._id.ownerId,
                    ownerEmailId:el._id.ownerEmailId,
                    name:acc.toLowerCase(),
                    reportingHierarchyAccess:true,
                    createdDate:new Date(),
                    access:[{
                        emailId:el._id.ownerEmailId
                    }]
                })
            }
        }
    });

    updateObj = {
        contacts:contacts,
        names:names,
        accounts:validContacts
    }

    return updateObj;
}

function removeOthersFromOpps(opps,companyId){
    var validContacts = [],names=[],contacts=[];
    var updateObj = {};

    if(opps && opps.length>0){
        _.each(opps,function (el) {

            if(el){
                var acc = fetchCompanyFromEmail(el.contactEmailId);

                if(acc){
                    names.push(acc);
                    contacts.push(el.contactEmailId);
                    validContacts.push({
                        companyId:companyId,
                        ownerId:el.userId,
                        ownerEmailId:el.userEmailId,
                        name:acc.toLowerCase(),
                        reportingHierarchyAccess:true,
                        createdDate:new Date(),
                        access:[{
                            emailId:el.userEmailId
                        }]
                    })
                }
            }
        });
    }

    updateObj = {
        contacts:contacts,
        names:names,
        accounts:validContacts
    }

    return updateObj;
}

function getTeamMembers(companyId,usersCollection,callback){
    usersCollection.find({companyId:companyId},{_id:1,emailId:1}).toArray(function (err, users) {

        if(!err && users){
            callback(_.pluck(users,"emailId"))
        } else {
            callback([])
        }
    });
}

function updateStart(userProfile,contactObjs,accountsCollection,callback) {

    var contacts = _.pluck(contactObjs,"personEmailId");

    getAccountObj(userProfile.companyId,userProfile._id,userProfile.emailId,contacts,function (updateObj) {

        removeAccountDuplicates(accountsCollection,userProfile.companyId,updateObj,function (err,nonExistingAccounts) {

            if(!err && nonExistingAccounts && nonExistingAccounts.length>0){
                updateAccounts(accountsCollection,nonExistingAccounts,function (err,result) {
                    addContactsToAccount(accountsCollection,userProfile.companyId,contacts,function () {
                        callback()
                    })
                })
            } else {
                addContactsToAccount(accountsCollection,userProfile.companyId,contacts,function () {
                    callback()
                })
            }
        })
    })
}

function getAccountsByOpp(companyId,oppsCollection,usersCollection,callback){
    getTeamMembers(companyId,usersCollection,function (users) {

        var queryFinal = { "$or": users.map(function(el) {
            var obj = {}
            obj["userEmailId"] = el;
            return obj;
        })};

        oppsCollection.aggregate(
            {
                $match:queryFinal
            },{
                $project:{
                    userEmailId:1,
                    contactEmailId:1,
                    userId:1,
                    createdDate:1
                }
            },{
                $sort:{createdDate:1}
            },
            function(err, opps){
                if(!err && opps){
                    callback(removeOthersFromOpps(opps,companyId))
                } else {
                    callback(null)
                }
            });
    });
}

function updateStartForInteractions(companyId,contactObjs,accountsCollection,callback){

    if(contactObjs){

        removeAccountDuplicates(accountsCollection,companyId,contactObjs,function (err,nonExistingAccounts) {

            if(!err && nonExistingAccounts){
                updateAccounts(accountsCollection,nonExistingAccounts,function (err,result) {
                    addContactsToAccount(accountsCollection,companyId,contactObjs.contacts,function () {
                        callback()
                    })
                })
            } else {
                addContactsToAccount(accountsCollection,companyId,contactObjs.contacts,function () {
                    callback()
                })
            }
        })
    } else {
        callback()
    }
}

function getAccountObj(companyId,ownerId,ownerEmailId,emailIds,callback) {

    var accounts = [],names=[];
    var updateObj = {};

    _.each(emailIds,function (el) {

        if(el){
            var name = fetchCompanyFromEmail(el);
            if(name){
                names.push(name);

                accounts.push({
                    companyId:companyId,
                    ownerId:ownerId,
                    ownerEmailId:ownerEmailId,
                    name:name.toLowerCase(),
                    reportingHierarchyAccess:true,
                    createdDate:new Date(),
                    access:[{
                        emailId:ownerEmailId
                    }]
                })
            }
        }
    });

    updateObj = {
        names:names,
        accounts:accounts
    }

    callback(updateObj);
}

function removeAccountDuplicates(accountsCollection,companyId,accountsObj,callback){

    if(accountsObj){

        var accounts = accountsObj.accounts
        var names = accountsObj.names

        accountsCollection.find({companyId:castToObjectId(String(companyId)),name:{$in:names}},{name:1}).toArray(function (err,existingAccounts) {
            console.log("Existing",existingAccounts.length);
            var nonExistingAccounts = getNonExistingAccounts(existingAccounts,accounts)
            callback(err,_.uniq(nonExistingAccounts,"name"));
        })
    } else {
        callback(null,[])
    }
}

function getNonExistingAccounts(existingAccounts,toBeUpdatedAccounts){

    var nonExistingAccounts = [];
    var onlyInA = existingAccounts.filter(comparer(toBeUpdatedAccounts));
    var onlyInB = toBeUpdatedAccounts.filter(comparer(existingAccounts));

    nonExistingAccounts = onlyInA.concat(onlyInB);

    return nonExistingAccounts;

}

function updateAccounts(accountsCollection,accounts,callback){

    _.each(accounts,function (el) {
        el.companyId = castToObjectId(String(el.companyId))
        el.ownerId = castToObjectId(String(el.ownerId))
        el.createdDate = new Date(el.createdDate)
    });

    if(accounts && accounts.length>0){
        accountsCollection.insert(accounts,function (err,result) {
            if(callback){
                callback(err,result)
            }
        })
    } else {
        if(callback){
            callback(null,null)
        }
    }
}

function addContactsToAccount(accountsCollection,companyId,contacts,callback) {

    var bulk = accountsCollection.initializeUnorderedBulkOp();

    var counter = 0;
    _.each(contacts,function (co) {

        if(co){
            var accountName = fetchCompanyFromEmail(co);

            if(accountName){
                bulk.find({companyId:castToObjectId(String(companyId)),name:accountName.toLowerCase()})
                    .update({$addToSet:{contacts: co}});

                counter++
            }
        }
    });

    if(counter>1){
        bulk.execute(function (err, result) {

            if(callback){
                callback()
            }
        });
    } else {
        if(callback){
            callback()
        }
    }

}

function fetchCompanyFromEmail(email){

    if(email){

        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        });

        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null;
    }
}

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id)
}

function comparer(otherArray){
    return function(current){
        return otherArray.filter(function(other){
                return other.name == current.name
            }).length == 0;
    }
}