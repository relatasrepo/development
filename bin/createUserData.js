var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var date = require('date-and-time');
var timediff = require('timediff');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
        // mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/JulyCopy", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                console.log('authenticated')
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var losingTouchCollection = db.collection('losingTouch');
                var opportunitiesCollection = db.collection('opportunities');
                var opportunitiesTargetCollection = db.collection('opportunitiesTarget');

                var runForSingleUser = function (skip) {
                    usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}).skip(0).limit(1).toArray(function (err, user) {

                        var oldEmailId = user[0].emailId
                        var oldUserId = user[0]._id
                        var newUserEmail = getEmailId().replace(/\s/g,'');
                        user[0]._id = new ObjectId();
                        user[0].emailId = newUserEmail;

                        copyUserCollection(usersCollection,user,function () {
                            copyInteractionsCollection(interactionCollection,oldEmailId,newUserEmail,user[0]._id,function () {
                                copyOpportunitiesCollection(opportunitiesCollection,oldEmailId,newUserEmail,user[0]._id,function () {
                                    copyOpportunitiesTargetCollection(opportunitiesTargetCollection,oldEmailId,newUserEmail,user[0]._id,oldUserId,function () {
                                        console.log("done",skip)

                                        if(skip<100){
                                            runForSingleUser(skip+1);
                                        }
                                    })
                                })
                            })
                        })
                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

start();

function copyUserCollection(usersCollection,user,callback) {
    usersCollection.insert(user,function (err,result) {
        callback();
    });
}

function copyOpportunitiesCollection(collection,oldEmailId,newUserEmail,newUserId,callback) {
    collection.find({userEmailId:oldEmailId}).toArray(function (err, results) {

        _.each(results,function (el) {
            // console.log(el)
            el._id = new ObjectId()
            el.opportunityId = new ObjectId()
            el.userEmailId = newUserEmail
            el.userId = newUserId
        })

        collection.insert(results,function (err,result) {
            console.log(err)
            callback();
        });

    });
}

function copyOpportunitiesTargetCollection(collection,oldEmailId,newUserEmail,newUserId,oldUserId,callback) {
    collection.find({userId:oldUserId}).toArray(function (err, results) {

        _.each(results,function (el) {
            el._id = new ObjectId()
            el.userId = newUserId
        })

        collection.insert(results,function (err,result) {
            callback();
        });

    });
}


function copyInteractionsCollection(interactionCollection,oldEmailId,newUserEmail,newUserId,callback) {
    interactionCollection.find({ownerEmailId:oldEmailId}).toArray(function (err, interactions) {

        _.each(interactions,function (el) {
            el._id = new ObjectId()
            el.ownerEmailId = newUserEmail
            el.ownerId = newUserId
        })

        interactionCollection.insert(interactions,function (err,result) {
            callback();
        });

    });
}

function getEmailId() {
    var adjectives = ["adamant", "adroit", "amatory", "animistic", "antic", "arcadian", "baleful", "bellicose", "bilious", "boorish", "calamitous", "caustic", "cerulean", "comely", "concomitant", "contumacious", "corpulent", "crapulous", "defamatory", "didactic", "dilatory", "dowdy", "efficacious", "effulgent", "egregious", "endemic", "equanimous", "execrable", "fastidious", "feckless", "fecund", "friable", "fulsome", "garrulous", "guileless", "gustatory", "heuristic", "histrionic", "hubristic", "incendiary", "insidious", "insolent", "intransigent", "inveterate", "invidious", "irksome", "jejune", "jocular", "judicious", "lachrymose", "limpid", "loquacious", "luminous", "mannered", "mendacious", "meretricious", "minatory", "mordant", "munificent", "nefarious", "noxious", "obtuse", "parsimonious", "pendulous", "pernicious", "pervasive", "petulant", "platitudinous", "precipitate", "propitious", "puckish", "querulous", "quiescent", "rebarbative", "recalcitant", "redolent", "rhadamanthine", "risible", "ruminative", "sagacious", "salubrious", "sartorial", "sclerotic", "serpentine", "spasmodic", "strident", "taciturn", "tenacious", "tremulous", "trenchant", "turbulent", "turgid", "ubiquitous", "uxorious", "verdant", "voluble", "voracious", "wheedling", "withering", "zealous"];
    var nouns = ["ninja", "chair", "pancake", "statue", "unicorn", "rainbows", "laser", "senor", "bunny", "captain", "nibblets", "cupcake", "carrot", "gnomes", "glitter", "potato", "salad", "toejam", "curtains", "beets", "toilet", "exorcism", "stick figures", "mermaid eggs", "sea barnacles", "dragons", "jellybeans", "snakes", "dolls", "bushes", "cookies", "apples", "ice cream", "ukulele", "kazoo", "banjo", "opera singer", "circus", "trampoline", "carousel", "carnival", "locomotive", "hot air balloon", "praying mantis", "animator", "artisan", "artist", "colorist", "inker", "coppersmith", "director", "designer", "flatter", "stylist", "leadman", "limner", "make-up artist", "model", "musician", "penciller", "producer", "scenographer", "set decorator", "silversmith", "teacher", "auto mechanic", "beader", "bobbin boy", "clerk of the chapel", "filling station attendant", "foreman", "maintenance engineering", "mechanic", "miller", "moldmaker", "panel beater", "patternmaker", "plant operator", "plumber", "sawfiler", "shop foreman", "soaper", "stationary engineer", "wheelwright", "woodworkers"];

    return randomEl(adjectives)+randomEl(nouns)+'@gmail.com'
}

function randomEl(list) {
    var i = Math.floor(Math.random() * list.length);
    return list[i];
}