var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();

rule.hour = 1;
rule.minute = 30;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Relationship Relevance is running');
    start();
});

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
    //     mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/Fri", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var userRelationCollection = db.collection('userRelation');

                var runForSingleUser = function (skip) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"},{contacts:1,emailId:1}).skip(0).limit(1).toArray(function (err, user) {
                    // usersCollection.find({emailId: "naveenpaul@relatas.com"},{contacts:1,emailId:1}).skip(skip).limit(1).toArray(function (err, user) {
                    usersCollection.find({corporateUser: true}).skip(skip).limit(1).toArray(function (err, user) {

                        if(!err && user && user.length>0 && user[0]){
                            var userProfile = JSON.parse(JSON.stringify(user[0]));
                            console.log("Completed >",userProfile.emailId,skip)

                            if(userProfile.contacts && userProfile.contacts.length>0){
                                updateStart(userProfile,interactionCollection,userRelationCollection,function (err,result) {
                                    runForSingleUser(skip+1)
                                })
                            } else {
                                runForSingleUser(skip+1)
                            }

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

start();

function updateStart(userProfile,interactionCollection,userRelationCollection,callback) {
    getContactsGroupedByRelation(userProfile.contacts,function (allContacts,contactsGrouped) {
        getInteractionCount(interactionCollection,userProfile._id,userProfile.emailId,allContacts,function (ierr,interactions) {
            if(!ierr && allContacts.length>0){
                formatDataForStorage(contactsGrouped,interactions,function (data) {

                    _.each(data,function (dt,index) {
                        dt.userId = ObjectId(userProfile._id)
                        dt.userEmailId = userProfile.emailId
                    })

                    userRelationCollection.remove({userEmailId:userProfile.emailId},function (rmErr,rmResult) {
                        
                        if(!rmErr && (rmResult || rmResult ==0 )){
                            userRelationCollection.insert(data,function (err,result) {
                                
                                callback()
                            })
                        } else {
                            callback()
                        }
                    })
                });
            } else {
                callback()
            }

        })
    });
}

function getContactsGroupedByRelation(contacts,callback) {

    var allContacts = [];
    var obj = {
        important:[],
        partners:[],
        decisionMakers:[],
        influencers:[],
        others:[]
    };

    if(contacts.length>0){
        _.each(contacts,function (co) {

            if(co.personEmailId){

                allContacts.push(co.personEmailId)

                if(co.favorite){
                    obj.important.push(contactFormatSchema(co.personEmailId))
                }

                if(co.contactRelation && co.contactRelation.decision_maker){
                    obj.decisionMakers.push(contactFormatSchema(co.personEmailId))
                }

                if(co.contactRelation && co.contactRelation.influencer){
                    obj.influencers.push(contactFormatSchema(co.personEmailId))
                }

                if(co.contactRelation && co.contactRelation.partner){
                    obj.partners.push(contactFormatSchema(co.personEmailId))
                }

                if(!co.contactRelation || (!co.favorite && co.contactRelation && !co.contactRelation.decision_maker && !co.contactRelation.influencer && !co.contactRelation.partner)){
                    obj.others.push(contactFormatSchema(co.personEmailId))
                }
            }
        })
    }

    callback(allContacts,obj);
}

function contactFormatSchema(contact) {

    return{
        emailId:contact,
        interactionCounter:0
    }
}

function getInteractionCount(interactionCollection,userId,ownerEmailId,allContacts,callback) {

    var notDomain = [new RegExp('@'+fetchCompanyFromEmail(ownerEmailId), "i")];

    interactionCollection.aggregate([
        {
            $match: {
                ownerId:ObjectId(userId),
                userId:{$ne:ObjectId(userId)},
                action:"receiver",
                emailId:{$in:allContacts},
                emailId:{"$nin":notDomain},
                interactionDate:{$lte:new Date()}
            }
        },
        {
            $project:{
                emailId:"$emailId",
                month:{$month:"$interactionDate"},
                year:{$year:"$interactionDate"}
            }
        },
        {
            $group: {
                _id: {
                    emailId:"$emailId",
                    month:"$month",
                    year:"$year"
                },
                count: {$sum: 1}
            }
        }
    ],function(err, data){
        callback(err,data)
    });
}

function formatDataForStorage(contactsGrouped,interactions,callback) {

    var data =  _
        .chain(interactions)
        .groupBy(function (value) {
            return value._id.month+""+value._id.year
        })
        .map(function(values, key) {
            return attachData(contactsGrouped,values,key)
        })
        .value();

    callback(data);
}

function attachData(contactsGrouped,values,key){

    var obj = {
        monthYear:key,
        important:{
            contacts:contactsGrouped.important.length,
            interactionCount:0,
            contactsAndInteractions:[]
        },
        partners:{
            contacts:contactsGrouped.partners.length,
            interactionCount:0,
            contactsAndInteractions:[]
        },
        decisionMakers:{
            contacts:contactsGrouped.decisionMakers.length,
            interactionCount:0,
            contactsAndInteractions:[]
        },
        influencers:{
            contacts:contactsGrouped.influencers.length,
            interactionCount:0,
            contactsAndInteractions:[]
        },
        others:{
            contacts:contactsGrouped.others.length,
            interactionCount:0,
            contactsAndInteractions:[]
        }
    };

    if(contactsGrouped.important.length){
        _.each(contactsGrouped.important,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    obj.important.contactsAndInteractions.push({
                        emailId:va._id.emailId,
                        count:va.count
                    })
                    obj.important.interactionCount = va.count+obj.important.interactionCount
                }
            })
        })
    }

    if(contactsGrouped.partners.length){
        _.each(contactsGrouped.partners,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){

                        obj.partners.contactsAndInteractions.push({
                            emailId:va._id.emailId,
                            count:va.count
                        })

                        obj.partners.interactionCount = va.count+obj.partners.interactionCount
                    }
                }
            })
        })
    }

    if(contactsGrouped.decisionMakers.length){
        _.each(contactsGrouped.decisionMakers,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.decisionMakers.contactsAndInteractions.push({
                            emailId:va._id.emailId,
                            count:va.count
                        })
                        obj.decisionMakers.interactionCount = va.count+obj.decisionMakers.interactionCount
                    }
                }
            })
        })
    }

    if(contactsGrouped.influencers.length){
        _.each(contactsGrouped.influencers,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.influencers.contactsAndInteractions.push({
                            emailId:va._id.emailId,
                            count:va.count
                        })
                        obj.influencers.interactionCount = va.count+obj.influencers.interactionCount
                    }
                }
            })
        })
    }

    if(contactsGrouped.others.length){
        _.each(contactsGrouped.others,function (co) {
            _.each(values,function (va) {
                if(co.emailId == va._id.emailId){
                    if(co.emailId == va._id.emailId){
                        obj.others.contactsAndInteractions.push({
                            emailId:va._id.emailId,
                            count:va.count
                        })
                        obj.others.interactionCount = va.count+obj.others.interactionCount
                    }
                }
            })
        })
    }

    return obj
}

function fetchCompanyFromEmail(email){

    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.remove(words, function(word){
        word = word.toLowerCase();
        return removableTextList.indexOf(word.trim()) > -1
    })
    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
}