var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async');
var admin = require('../config/firebaseConfiguration');

var notificationsDaily = require('./notificationsDaily');
var notificationsWeekly = require('./notificationsWeekly');

var notificationsCollections;
var usersCollection;
var shCollection;
var opportunitiesTargetCollection;

var morningNotificationRule = {hour: 2, minute: 30, second: 0};
var eveningNotificationRule = {hour: 14, minute: 30, second: 0};
var mondayNotificationRule = {hour: 1, minute: 30, second: 0, dayOfWeek:1};
var wednesdayNotificationRule = {hour: 1, minute:30, second: 0, dayOfWeek:3};


function startScheduler() {
    console.log("Starting to schedule jobs");

    schedule.scheduleJob(morningNotificationRule, function() {
        notificationsDaily.morningNotificationBuilder();
    });

    schedule.scheduleJob(eveningNotificationRule, function() {
        notificationsDaily.eveningNotificationBuilder();
    });

    schedule.scheduleJob(mondayNotificationRule, function() {
        notificationsWeekly.mondayNotificationBuilder();
    });

    schedule.scheduleJob(wednesdayNotificationRule, function() {
        notificationsWeekly.wednesdayNotificationBuilder();
    });

    console.log("All jobs are scheduled");
}

if(require.main == module) {
    startScheduler();
}

var getDbInstance = function(callback) {
    // mongoClient.connect("mongodb://reladmin:Hennur123@mongodb://10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //mongoClient.connect("mongodb://localhost", function(error, client) {
        if(error) {
            throw error;
        } else {
            var db = client.db('Relatas');
            notificationsCollections = db.collection('notifications');
            usersCollection = db.collection('user');
            shCollection = db.collection('secondaryHierarchy');
            opportunitiesTargetCollection = db.collection('opportunitiesTarget');

            callback(db );
        }
    })
}
module.exports.getDbInstance = getDbInstance;

function storeNotificationData(notificationsData, companyId, userId, emailId, callback) {
    var notificationObjs = [];
    var dayString = moment().format("DDMMYYYY");

    _.each(notificationsData, function(notification) {
        if(notification && notification.messageIds) {
            if( checkRequired(notification.messageIds[0]) ) {

                notificationObjs.push({
                    messageId: notification.messageIds[0],
                    emailId: emailId,
                    userId: userId,
                    companyId: companyId,
                    notificationTitle: notification.title,
                    notificationBody: notification.body,
                    url: notification.url,
                    notificationType: notification.notificationType,
                    data: notification.payloadData,
                    sentDate: new Date(),
                    dayString: dayString,
                    category: notification.category,
                    dataReferenceType: "notification",
                    dataFor: "web",
                    accessed: false
                });
            }

            if( checkRequired(notification.messageIds[1]) ) {
                notificationObjs.push({
                    messageId: notification.messageIds[1],
                    emailId: emailId,
                    userId: userId,
                    companyId: companyId,
                    notificationTitle: notification.title,
                    notificationBody: notification.body,
                    url: notification.url,
                    notificationType: notification.notificationType,
                    data: notification.payloadData,
                    dayString: dayString,
                    sentDate: new Date(),
                    category: notification.category,
                    dataReferenceType: "notification",
                    dataFor: "mobile",
                    accessed: false
                });
            }
        }
    })

    notificationsCollections.insertMany(notificationObjs, function(error, results) {
        callback();
    })
}
module.exports.storeNotificationData = storeNotificationData;

function sendNotification(payload, webToken, mobileToken, callback) {
    webToken = checkRequired(webToken) ? webToken : "123"; // "123 is random id on not availibility of actual firebase token, which makes firebase to return undefined messageId"
    mobileToken = checkRequired(mobileToken) ? mobileToken : "123"; // "123 is random id on not availibility of actual firebase token, which makes firebase to return undefined messageId"

    async.parallel([function(callback) {
        var webPayload = JSON.parse(JSON.stringify(payload));
        webPayload.notification.icon = webPayload.data.icon ? webPayload.data.icon : '/images/relatasIcon.png';
        webPayload.notification.click_action = webPayload.data.click_action || '';
        admin.messaging().sendToDevice(webToken, webPayload, {})
            .then((response) => {
                var webMsgId = response.results[0].messageId;
                callback(null, webMsgId)
            })
            .catch((error) => {
                console.log("Error while sending notification:", error);
                callback(null, '');
            });

    }, function(callback) {
        admin.messaging().sendToDevice(mobileToken, payload, {})
            .then((response) => {
                var mobileMsgId = response.results[0].messageId;
                callback(null, mobileMsgId)
            })
            .catch((error) => {
                console.log("Error while sending notification:", error);
                callback(null, '');
            });

    }], function(error, msgIds) {
        callback(error, [msgIds[0], msgIds[1]]);
    })
}
module.exports.sendNotification = sendNotification;

var userFiscalYear = function (company,user,callback) {

    if(company && user){

        var fyMonth = "April"; //Default
        if (!company || !company.fyMonth) {
        } else {
            fyMonth = company.fyMonth
        }

        var currentYr = new Date().getFullYear()
        var currentMonth = new Date().getMonth()

        var toDate = null;
        var fromDate = new Date(moment().startOf('month'));
        fromDate.setMonth(monthNames.indexOf(fyMonth))

        if(currentMonth<monthNames.indexOf(fyMonth)){
            fromDate.setFullYear(currentYr-1)
        }

        toDate = moment(fromDate).add(11, 'month');
        toDate = moment(toDate).endOf('month');

        var timezone = user.timezone && user.timezone.name?user.timezone.name:"UTC";

        callback(company.fyMonth,
            {start:moment(fromDate).tz(timezone).format(),end: moment(toDate).tz(timezone).format()},
            setQuarter(fyMonth,timezone,fromDate,toDate),
            timezone)
    } else {
        callback(null,null,null,null)
    }
}
module.exports.userFiscalYear = userFiscalYear;

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}
module.exports.getUserProfiles = getUserProfiles;

function getAllReportees(user,callback){

    async.parallel([
        function (callback) {
            getOrgHierarchy(user._id,user.companyId,callback)
        },
        function (callback) {
            getSecondaryHierarchies(user._id,user.companyId,callback)
        }
    ],function (err,data) {
        var allUsers = [{
            emailId:user.emailId,
            _id:user._id,
            firstName: user.firstName,
            lastName: user.lastName
        }];

        var allEmailIds = [user.emailId],
            allUserIds = [user._id];

        if(!err) {
            if(data[0]){
                _.each(data[0],function(el){
                    allEmailIds.push(el.emailId)
                    allUserIds.push(el._id)
                    allUsers.push({
                        emailId:el.emailId,
                        _id:el._id,
                        firstName: el.firstName,
                        lastName: el.lastName
                    })
                })
            }
            if(data[1]){
                _.each(data[1],function(el){
                    allEmailIds.push(el.ownerEmailId)
                    allUserIds.push(el.userId)
                    allUsers.push({
                        emailId:el.ownerEmailId,
                        _id:el.userId,
                        firstName: el.firstName,
                        lastName: el.lastName
                    })
                })
            }

            callback(uniqBy(allUsers,"emailId"),uniqBy(allEmailIds),allUserIds)
        } else {
            callback([],[],[])
        }
    })
}
module.exports.getAllReportees = getAllReportees;

function getTargets(userIds, monthYear, fyStart, fyEnd, callback) {
    var query = {
        userId:{$in:userIds},
        "fy.start":{$gte:new Date(fyStart)},
        "fy.end":{$lte:new Date(fyEnd)}
    }

    opportunitiesTargetCollection.find(query).toArray(function(error, targets) {
        var usersTargets = {};
        
        if(!error) {
            _.each(targets, function(target) {
                var salesTarget = _.find(target.salesTarget, function(el) {
                    if(el.monthYear == monthYear)
                        return el;
                })
                usersTargets[target.userId] = salesTarget.target;
            });
    
            callback(usersTargets);
        } else {
            callback(usersTargets);
        }
        
    })
}
module.exports.getTargets = getTargets;

function numberWithCommas(x,ins) {
    if(ins){
        return numberWithCommas_ins(x);
    } else {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
}
module.exports.numberWithCommas = numberWithCommas;

function getCommitCutOffDates(company, timezone, allQuarters) {
    timezone = timezone && timezone.name ? timezone.name : 'Asia/Kolkata'
    var monthCommitCutOff = 10;
    var qtrCommitCutOff = 15

    if(company) {
        if(company.monthCommitCutOff)
            monthCommitCutOff = company.monthCommitCutOff
        
        if(company.qtrCommitCutOff)
            qtrCommitCutOff = company.qtrCommitCutOff;
        
            var startOfQuarter =  moment(allQuarters[allQuarters.currentQuarter].start).add(qtrCommitCutOff, "day"),
                weeklyCutOff = getThisWeekCommitCutoffDate(company, timezone),
                monthlyCutOff = new Date(new Date(new Date().setDate(monthCommitCutOff)).setHours(23, 59, 0)),
                quarterlyCutOff = new Date(startOfQuarter);

        return {
            weeklyCutOffDate: weeklyCutOff,
            monthlyCutOffDate: monthlyCutOff,
            quarterlyCutOffDate: quarterlyCutOff
        }

    } else {
        return {}
    }
}
module.exports.getCommitCutOffDates = getCommitCutOffDates;

function getThisWeekCommitCutoffDate(companyDetails,timezone) {

    var dateTime = moment().day("Monday");
    if(companyDetails && companyDetails.commitDay){
        dateTime = moment().day(companyDetails.commitDay)
    }

    dateTime = moment(dateTime).tz(timezone)

    if(companyDetails && companyDetails.commitHour){
        dateTime = dateTime.hour(parseInt(companyDetails.commitHour))
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    } else {
        dateTime = dateTime.hour(18)
        dateTime = dateTime.minute(0)
        dateTime = dateTime.second(0)
    }

    return new Date(dateTime);
}

function numberWithCommas_ins(x) {
    x=String(x).toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

function numberWithCommas_ins(x) {
    x=String(x).toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
        afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
}

var getOrgHierarchy = function(userId,companyId,callBack) {
    usersCollection.find({ corporateUser: true,companyId:companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' },
            resource: { $ne: true } },
        { _id: 1, emailId: 1, designation: 1, hierarchyParent: 1, hierarchyPath: 1, firstName: 1, lastName: 1,companyId:1,regionOwner: 1,productTypeOwner: 1,verticalOwner: 1 }).toArray(function(err, users) {
        callBack(null, users);
    })
};

var getSecondaryHierarchies= function (userId,companyId,callback){
    shCollection.find({ companyId:companyId, hierarchyPath: { $regex: userId.toString(), $options: 'i' }},{monthlyTargets:0}).toArray(function (err,result) {
        callback(err,result)
    });
}

function setQuarter(startMonth,timezone,fyStartDate,fyEndDate) {

    var qtrObj = {};

    if(!timezone){
        timezone = "Asia/Kolkata"
    }

    var months = [];
    months.push(fyStartDate)

    _.each(monthNames,function (el,index) {
        months.push(new Date(moment(fyStartDate).add(index+1,"month")))
    });

    qtrObj.quarter1 = {start:moment(moment(months[0]).startOf('month')).tz(timezone).format(),end:moment(moment(months[2]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter2 = {start:moment(moment(months[3]).startOf('month')).tz(timezone).format(),end:moment(moment(months[5]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter3 = {start:moment(moment(months[6]).startOf('month')).tz(timezone).format(),end:moment(moment(months[8]).endOf('month')).tz(timezone).format()}
    qtrObj.quarter4 = {start:moment(moment(months[9]).startOf('month')).tz(timezone).format(),end:moment(moment(months[11]).endOf('month')).tz(timezone).format()}

    var currentMonth = moment().month()
    var currentQuarter = "quarter4"

    if(startMonth =='January'){

        if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    if(startMonth =='April'){

        if(currentMonth >= 3 && currentMonth<=5){
            currentQuarter = "quarter1"
        } else if(currentMonth >= 6 && currentMonth<=8){
            currentQuarter = "quarter2"
        } else if(currentMonth >= 9 && currentMonth<=11){
            currentQuarter = "quarter3"
        } else if(currentMonth >= 0 && currentMonth<=2){
            currentQuarter = "quarter4"
        }

        qtrObj.currentQuarter = currentQuarter
    }

    return qtrObj
}

var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function checkRequired(data){
    if (data === '' || data === null || data === undefined || data === "undefined") {
        return false;
    }
    else{
        return true;
    }
}
