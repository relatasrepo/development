var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var fs = require('fs');
var json2xls = require('json2xls');

var OppMetaDataCollection;
var opportunitiesCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('oppMetaData is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.255:27017/Relatas", function(error, client) { // This is LIVE
        mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
        // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');

            var runForSingleUser = function (skip) {
                // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}, {
                //     usersCollection.find({corporateUser: true}, {
                    usersCollection.find({}, {
                    emailId: 1,
                    _id: 1,
                    companyId: 1
                }).skip(skip).limit(1).toArray(function (err, user) {

                    if(!err && user && user.length>0){
                        var companyId = user[0].companyId
                        var userEmailId = user[0].emailId;

                        updateEmailId(companyId,userEmailId,function () {
                            runForSingleUser(skip+1);
                        })
                    } else {
                        console.log("Done");
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

startScript();

function updateEmailId(companyId,userEmailId,callback){
    opportunitiesCollection.update({userEmailId:userEmailId},{$set:{createdByEmailId:userEmailId}},{upsert:true,multi:true},function (err,result) {
        if(result){
            console.log(result.result.nModified)
        }
        callback()
    })
}