var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var request=require('request');
var async = require("async");
var mongoose = require('mongoose');


var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

var emailId = process.argv[2];
var dbUrl = process.argv[3];

console.log('\x1b[36m%s\x1b[0m',"User Email ID is : " + emailId);
console.log('\x1b[36m%s\x1b[0m',"MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log('\x1b[36m%s\x1b[0m',"Connection Successful")
    if(err){
        console.log(err)
    }

    db.authenticate(dbPasswords.relatasDB.user,dbPasswords.relatasDB.password, function (err, res) {
        var collection = db.collection("user");
        collection.find({"emailId":emailId},{contacts:0}).toArray(function(err, user){
            getNewGoogleToken(user[0].google[0].token,user[0].google[0].refreshToken,function(token){
                console.log('\x1b[36m%s\x1b[0m',"==>Old Token is:")
                console.log(user[0].google[0].token)
                console.log('\x1b[36m%s\x1b[0m',"==>New Token is:")
                console.log(token)
                if(token){
                    getContactsFromGoogleApi(token,user,collection)
                }
            });
        })
    })
})

function getNewGoogleToken(token,refreshToken,callback){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
}

function getContactsFromGoogleApi(access_token,user,collection) {

    var userId = user[0]._id;
    var qs = {
        'alt': 'json',
        'max-results': 10000,
        'orderby': 'lastmodified'
    }

    request.get({
        url: 'https://www.google.com/m8/feeds/contacts/default/full',

        qs: qs,
        headers: {
            'Authorization': 'Bearer ' + access_token,
            'GData-Version': '3.0'
        }
    }, function (err, res, body) {

        if(!res){
            logger.info("Error get google contacts ",body, err);
        }
        else if (res.statusCode === 401) {
            logger.info("Error get google contacts ",body, err);
        }
        else{

            var feed = JSON.parse(body);

            var emailIdArr = [];
            if(feed && feed.feed && feed.feed.entry){
                var users = feed.feed.entry.map(function (c) {
                    var r = {};
                    if (c['gd$name'] && c['gd$name']['gd$fullName']) {
                        r.name = c['gd$name']['gd$fullName']['$t'];
                    }
                    if(c['gContact$birthday'] && c['gContact$birthday'].when){
                        r.birthday = c['gContact$birthday'].when
                    }
                    if(c['gd$organization'] && c['gd$organization'].length >  0){
                        if(c['gd$organization'][0]['gd$orgName'])
                            r.companyName = c['gd$organization'][0]['gd$orgName']['$t'];
                        if(c['gd$organization'][0]['gd$orgTitle'])
                            r.designation = c['gd$organization'][0]['gd$orgTitle']['$t'];
                    }
                    if (c['gd$email'] && c['gd$email'].length > 0) {

                        r.email = c['gd$email'][0]['address'];
                        if(typeof r.email == 'string'){
                            emailIdArr.push(r.email.trim().toLowerCase());
                        }
                        r.nickname = c['gd$email'][0]['address'].split('@')[0];
                    }

                    if(c['gd$phoneNumber']){
                        if(c['gd$phoneNumber'].length > 0){
                            r.phone = c['gd$phoneNumber'][0]['$t'];
                        }
                    }

                    if(c['gd$where'] && c['gd$where'].valueString){
                        r.location = c['gd$where'].valueString
                    }

                    return r;
                })
            }

            console.log('\x1b[36m%s\x1b[0m',"Fetched " +users.length+ " contacts from Google")

            var con;
            var googleContacts = [];
            if(users && users.length > 0){
                if(users.length > 0){
                    for(var i=0; i<users.length; i++){

                        con = addContacts(userId,users[i],collection);
                        if(con){
                            googleContacts.push(con)
                        }
                    }
                }
            }
            var emailsContacts = _.map(googleContacts,"personEmailId")

            collection.aggregate(
                {$match: {emailId: user[0].emailId}},
                {$unwind:"$contacts"},
                {$project:{"contacts":{$toLower:"$contacts.personEmailId"}}},
                {$group :
                    {
                        "_id":null,
                        "emailsArray" : {"$addToSet" : "$contacts"}
                    }
                },
                function(err, result){

                    if(result.length>0){

                        var nonExistentContacts = emailsContacts.filter( function( el ) {
                            return result[0].emailsArray.indexOf( el ) < 0;
                        })

                        console.log('\x1b[36m%s\x1b[0m',"Existing contacts -" +result[0].emailsArray.length)
                        console.log('\x1b[36m%s\x1b[0m',"Google built " +googleContacts.length+ " contacts")
                        console.log('\x1b[36m%s\x1b[0m',"Non existent contacts - " + nonExistentContacts.length)
                        var addContacts = googleContacts.filter(function(val) {
                            return result[0].emailsArray.indexOf(val.personEmailId) == -1;
                        });

                        var allEmailIds = _.map(addContacts,"personEmailId")
                        collection.find({"emailId":{$in:allEmailIds}},{_id:1,firstName:1,lastName:1,skypeId:1,emailId:1}).toArray(function(err,res){
                            res.forEach(function(user){
                                for(var i=0; i<addContacts.length; i++){
                                    if(addContacts[i].personEmailId == user.emailId){
                                        addContacts[i].personId = user._id.toString()
                                        addContacts[i].personName = user.firstName+' '+user.lastName
                                        addContacts[i].personEmailId = user.emailId
                                        addContacts[i].companyName = user.companyName
                                        addContacts[i].designation = user.designation
                                        addContacts[i].location = user.location
                                        addContacts[i].verified = true
                                        addContacts[i].relatasUser = true
                                        addContacts[i].relatasContact = true
                                        addContacts[i].mobileNumber = user.mobileNumber || null
                                        addContacts[i].skypeId = user.skypeId || null
                                    }
                                }
                            })

                            console.log('\x1b[36m%s\x1b[0m',"Contacts to add - " + addContacts.length);

                            addContacts = _.uniq(addContacts, 'personEmailId');

                            if(addContacts.length>0){
                                pushAllContactsToUserProfile(userId,addContacts,collection)
                            }
                        });
                    }
                    else {
                        console.log('\x1b[36m%s\x1b[0m',"No unique contacts to Update. Please press 'CTRL C' to exit.")
                    }
                })
        }
    });
}

function pushAllContactsToUserProfile(userId,contacts,collection){
    collection.update({ _id:userId},{$pushAll:{contacts:contacts}},function(error,result){

        if(error){
            console.log('\x1b[36m%s\x1b[0m','Error in pushAllContactsToUserProfile():Contact ',error);
        }
        if(result){
            console.log('\x1b[36m%s\x1b[0m',"Successfully Added Contacts. Press 'CTRL C' to exit.")
        }
    });
}

function addContacts(userId,contact){

    var emailId = contact.email;
    var name = contact.name || contact.nickname || contact.email; // Last modified 14 March 16, Naveen. Added 'contact.email'
    var phone = contact.phone || null;
    var birthday = contact.birthday || null;
    var companyName = contact.companyName || null;
    var designation = contact.designation || null;
    var location = contact.location || null;

    if(userId && name && emailId){
        return {
            _id:mongoose.Types.ObjectId(),
            personId:null,
            personName:name,
            personEmailId:emailId.trim().toLowerCase(),
            birthday:birthday,
            companyName:companyName,
            designation:designation,
            count:0,
            addedDate:new Date(),
            verified:false,
            relatasUser:false,
            relatasContact:true,
            mobileNumber:phone,
            location:location,
            source:'google-batch-sync',
            account:{
                name:fetchCompanyFromEmail(emailId),
                selected:false,
                showToReportingManager:false,
                updatedOn:new Date()
            }
        };
    }
    else return null;
}

function fetchCompanyFromEmail(email){
    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.remove(words, function(word){
        return removableTextList.indexOf(word.trim()) > -1
    })
    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : words[0]
}

getInvalidEmailList = function(){
    return invalidEmails;
}

var invalidEmails = [
    /calendar-notification/i,
    /feedproxy/i,
    /techgig/i,
    /team/i,
    /blog/i,
    /info/i,
    /support/i,
    /admin/i,
    /hello/i,
    /no-reply/i,
    /noreply/i,
    /reply/i,
    /help/i,
    /mailer-daemon/i,
    /googlemail.com/i,
    /mail-noreply/i,
    /alert/i,
    /calendar-notification/i,
    /eBay/i,
    /flipkartletters/i,
    /pinterest/i,
    /dobambam.com/i,
    /notify/i,
    /offers/i,
    /iicicibank/i,
    /indiatimes/i,
    /info@relatas.in/i,
    /facebookmail/i,
    /message/i,
    /facebookmail.com/i,
    /notification/i,
    /youcanreply/i,
    /jobs/i,
    /news/i,
    /linkedin/i,
    /list/i,
    /updates/i,
    /verify/i
];
