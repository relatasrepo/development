var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var _ = require('lodash');
var moment = require('moment-timezone');

var oppTrash;
var opportunities;
var accounts;
var actionItems;
var contacts;
var interactions;
var interactionsV2;
var losingTouch;
var meeting;
var oppMetaData;
var opportunityLog;
var user;
var userlog;
var tasks;
var revenueHierarchy;
var opportunitiesTarget;
var oppCommits;
var messages;
var message;
var accountsLogs;
var company;

var startScript = function(){
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.165:27017/Relatas", function(error, client) { // This is LIVE
//     mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
//         mongoClient.connect("mongodb://localhost", function(error, client) {
            if (error) {
                throw error;
            } else {
                var db = client.db('Relatas');
                // var db = client.db('newDb');
                console.log("Connected to db");
                console.log('authenticated')
                user = db.collection('user');
                opportunitiesTarget = db.collection('opportunitiesTarget');
                tasks = db.collection('tasks');
                userlog = db.collection('userlog');
                opportunityLog = db.collection('opportunityLog');
                oppMetaData = db.collection('oppMetaData');
                meeting = db.collection('meeting');
                losingTouch = db.collection('losingTouch');
                interactionsV2 = db.collection('interactionsV2');
                interactions = db.collection('interactions');
                contacts = db.collection('contacts');
                actionItems = db.collection('actionItems');
                accounts = db.collection('accounts');
                revenueHierarchy = db.collection('revenueHierarchy');
                opportunities = db.collection('opportunities');
                oppTrash = db.collection('oppTrash');
                oppCommits = db.collection('oppCommits');
                accountsLogs = db.collection('accountsLogs');
                message = db.collection('message');
                messages = db.collection('messages');
                company = db.collection('company');

                var runForSingleUser = function (skip) {
                    company.find({url: {$ne:"22by7.relatas.com"}}).limit(1).skip(skip).toArray(function (err, company) {
                        var companyId = company[0]._id;

                        userRm(companyId,function (emailIds) {

                            console.log(company[0].url,emailIds.length);

                            if(emailIds.length>0){

                                oppTrashRm(companyId,emailIds)
                                opportunitiesRm(companyId,emailIds)
                                accountsRm(companyId,emailIds)
                                actionItemsRm(companyId,emailIds)
                                contactsRm(companyId,emailIds)
                                interactionsRm(companyId,emailIds)
                                interactionsV2Rm(companyId,emailIds)
                                losingTouchRm(companyId,emailIds)
                                oppMetaDataRm(companyId,emailIds)
                                opportunityLogRm(companyId,emailIds)
                                // userlogRm(companyId,emailIds)
                                tasksRm(companyId,emailIds)
                                revenueHierarchyRm(companyId,emailIds)
                                opportunitiesTargetRm(companyId,emailIds)
                                oppCommitsRm(companyId,emailIds)
                                messagesRm(companyId,emailIds)
                                accountsLogsRm(companyId,emailIds)

                                runForSingleUser(skip+1);
                            } else {
                                runForSingleUser(skip+1);
                            }
                        })

                    });
                };

                runForSingleUser(0);
            }
        });
}

var oppTrashRm = function(companyId,emailIds){
    oppTrash.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---oppTras----")
        console.log(results)
    })
};

var opportunitiesRm = function(companyId,emailIds){
    opportunities.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---opportunitie----")
        console.log(results)
    })
};

var accountsRm = function(companyId,emailIds){
    accounts.remove({$or:[{companyId:companyId},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---account----")
        console.log(results)
    })
};

var actionItemsRm = function(companyId,emailIds){
    actionItems.remove({$or:[{userEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---actionItem----")
        console.log(results)
    })
};

var contactsRm = function(companyId,emailIds){
    contacts.remove({$or:[{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---contact----")
        console.log(results)
    })
};

var interactionsRm = function(companyId,emailIds){
    interactions.remove({$or:[{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---interaction----")
        console.log(results)
    })
};

var interactionsV2Rm = function(companyId,emailIds){
    interactionsV2.remove({$or:[{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---interactionsV----")
        console.log(results)
    })
};

var losingTouchRm = function(companyId,emailIds){
    losingTouch.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---losingTouc----")
        console.log(results)
    })
};

var oppMetaDataRm = function(companyId,emailIds){
    oppMetaData.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---oppMetaDat----")
        console.log(results)
    })
};

var opportunityLogRm = function(companyId,emailIds){
    opportunityLog.remove({$or:[{companyId:companyId},{toEmailId:{$in:emailIds}},{fromEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---opportunityLo----")
        console.log(results)
    })
};

var userRm = function(companyId,callback){
    user.find({$or:[{companyId:companyId}]},{emailId:1}).toArray(function (err,results) {
        if(err){
            console.log(err)
        }
        var emailIds = [];
        for(var key in results){
            emailIds.push(results[key].emailId)
        }

        callback(emailIds)
    })
};

var userlogRm = function(companyId,emailIds){
    userlog.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---userlo----")
        console.log(results)
    })
};

var tasksRm = function(companyId,emailIds){
    tasks.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{createdByEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---task----")
        console.log(results)
    })
};

var revenueHierarchyRm = function(companyId,emailIds){
    revenueHierarchy.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---revenueHierarch----")
        console.log(results)
    })
};

var opportunitiesTargetRm = function(companyId,emailIds){
    opportunitiesTarget.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---opportunitiesTarge----")
        console.log(results)
    })
};

var oppCommitsRm = function(companyId,emailIds){
    oppCommits.remove({$or:[{companyId:companyId},{userEmailId:{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---oppCommit----")
        console.log(results)
    })
};

var messagesRm = function(companyId,emailIds){
    messages.remove({$or:[{companyId:companyId},{"messageOwner.emailId":{$in:emailIds}},{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---message----")
        console.log(results)
    })
};

var accountsLogsRm = function(companyId,emailIds){
    accountsLogs.remove({$or:[{ownerEmailId:{$in:emailIds}}]},function (err,results) {
        if(err){
            console.log(err)
        }
        console.log("---accountsLog----")
        console.log(results)
    })
};

startScript();