var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  , url = 'mongodb://localhost:27017/JulyCopy'

var json2csv = require('json2csv');
var fs = require('fs');
customObjectId = require('mongodb').ObjectID;

MongoClient.connect(url, function(err, db){
  db.authenticate("devuserre", "devpasswordre", function(err, res) {

    var user = db.collection("user");
    var interactions = db.collection("interactions");

    user.find({corporateUser:true},{_id:1,emailId:1}).toArray(function (err,users) {
    // user.find({emailId:"sudip@relatas.com"},{contacts:1,emailId:1}).toArray(function (err,users) {

      var query = { "$or": users.map(function(el) {
        var obj = {};

        obj["ownerId"] = el._id;
        obj["userId"] = {$ne:el._id};
        obj["interactionType"] = "email";
        return obj;
      })};

      var query2 = { "$or": users.map(function(el) {
        var obj = {};

        obj["userId"] = el._id.toString();
        obj["ownerId"] = el._id;
        return obj;
      })};

      interactions.aggregate([
        {
          $match:query
        },
        {
          $match:query2
        },
        {
          $group:{
            _id:{
              owner:"$ownerEmailId",
              refId:"$refId"
            },
            count:{$sum:1},
            refIdList:{
              $addToSet:{
                _id:"$refId"
              }
            }
          }
        },
        {
          $project:{
            _id:"$_id.owner",
            // refId:"$_id.refId",
            refIdList:"$refIdList",
            count:"$count"
          }
        },
        {
          $match:{
            count:{
              "$gt":1
            }
          }
        },
        {
          $group:{
            _id:null,
            allRefIdList:{
              $push:{_id:"$refIdList"}
            }
          }
          
        }
      ],{allowDiskUse: true},function (err,interactionCounts) {
        
        console.log(err);
        // console.log(JSON.stringify(interactionCounts[0].allRefIdList,null,2));

        if(interactionCounts.length>0){

          var ids = _.pluck(interactionCounts[0].allRefIdList,"_id")
          var removeIds = _.pluck(_.flatten(ids),"_id")
          console.log(removeIds.length)
          // interactions.remove({refId:{$in:removeIds}},function (err,response) {
          //   console.log(err)
          //   console.log(response)
          // })
        }
      })
    })
  })
});

