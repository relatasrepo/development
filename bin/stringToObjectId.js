
var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();
var ObjectID = require('mongodb').ObjectID;
var database = null;

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

//var async = require('async');
var dbUrl = process.argv[2];

console.log("MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connect")
    if (err) {
        console.log(err)
    }

    database = db
    fetchInteractions(0)

});

var fetchInteractions = function(skip){
    database.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
        console.log("Processing records " + (skip + 1) + " - " + (skip +10))
        database.collection("interactions_copy").find().sort({_id: 1}).skip(skip).limit(10).toArray(function(err, interactions){
            if(err)
                console.log(err)
            if(!interactions.length)
            {
                console.log("done")
                return
            }

            var collection = database.collection("interactions_copy")
            var batch = collection.initializeUnorderedBulkOp()
            var c = 0;
            _.each(interactions, function(interaction){

                console.log("--batch---",c++)
                if(interaction.userId){
                    var updateObj = {
                        "userId": new ObjectID(interaction.userId.toString())
                    }

                    batch.find({ _id: interaction._id, "userId": interaction.userId.toString()}).update({ $set: updateObj });
                    // batch.toString();
                    // console.log("--batch---")
                }
            });


            batch.execute(function(err, data){
                if(err) {

                    console.log("--err--")
                    console.log(err)
                }
                else{
                    console.log(data)
                    if(data.getWriteErrors()[0])
                        console.log(data.getWriteErrors()[0].errmsg)
                    fetchInteractions(skip + 10)
                }
            })
        })
    })
}
