var mongoClient = require('mongodb').MongoClient;
var schedule = require('node-schedule');
var _ = require('lodash');
var json2csv = require('json2csv');
var fs = require('fs');

var rule = new schedule.RecurrenceRule();

rule.hour = 1;
rule.minute = 30;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Relationship Relevance is running');
    start();
});

var start = function() {


    // mongoClient.connect("mongodb://10.150.0.8/Relatas_masked_Apr_04?poolSize=100", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {

        console.log(error)
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            //     if (err) throw err;
            var db = client.db('Relatas');
            var usersCollection = db.collection('user');
            var oppCommitCollection = db.collection('oppCommits');

            var runForSingleUser = function (skip) {
                oppCommitCollection.aggregate([{$match: {
                        $or:[{
                            "week.userCommitAmount":{$gt:0}
                        },{
                            "month.userCommitAmount":{$gt:0}
                        },{
                            "quarter.userCommitAmount":{$gt:0}
                        }]
                    }},{
                    $project:{
                        week:1,
                        month:1,
                        quarter:1,
                        userId:1,
                        commitWeekYear:1,
                        date:1
                    }
                }]).toArray(function (err,commits) {

                    console.log(err)
                    getUserEmailIdAndOutToNewCollection(commits,usersCollection,function () {
                        console.log("Done")
                    })

                });
            };

            runForSingleUser(0);
        }
    });
}

start();

function getUserEmailIdAndOutToNewCollection(commits,usersCollection,callback) {
    // console.log(JSON.stringify(commits,null,1));
    var userIds = _.pluck(commits,"userId");

    usersCollection.find({_id: {$in:userIds}}, {
        emailId: 1,_id:1
    }).toArray(function (err, users) {
        _.each(users,function (user) {
            _.each(commits,function (co) {

                if(co.week && (co.week.userCommitAmount || co.week.userCommitAmount == 0)){
                    co.week = co.week.userCommitAmount;
                }
                if(co.month && (co.month.userCommitAmount || co.month.userCommitAmount == 0)){
                    co.month = co.month.userCommitAmount;
                }
                if(co.quarter && (co.quarter.userCommitAmount || co.quarter.userCommitAmount == 0)){
                    co.quarter = co.quarter.userCommitAmount;
                }
                if(co && String(co.userId) == String(user._id)){
                    co.emailId = user.emailId;
                }
            });
        });

        saveToCsv(commits)
        callback()
    });
}

function saveToCsv(data) {
    var fields = ["userId","emailId","week","month","quarter","commitWeekYear","date"];
    var result = json2csv({ data: data, fields: fields });

    fs.writeFile('oppCommits.csv', result, function(err) {
        if (err) throw err;
        console.log('file saved');
    });
}