// require('newrelic');
var mongoClient = require('mongodb').MongoClient;
var async = require('async')
var fs = require('fs');
var json2xls = require('json2xls');
var request = require('request');
var _ = require("lodash");

var oppsCollection;
var usersCollection;
var contactsCollection;
var companyCollection;

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_13Sep?authSource=Relatas_mask", function(error, client) {
   // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            // var db = client.db('Relatas_13Sep');
            usersCollection = db.collection('user');
            contactsCollection = db.collection('contacts');
            companyCollection = db.collection('company');
            oppsCollection = db.collection('opportunities');
            var totalFound = 0,data = [];

            var runForSingleCompany = function (skip) {
                companyCollection.find({}).skip(skip).limit(1).toArray(function (err, company) {
                    if(!err && company && company.length>0){
                        var companyId = company[0]._id;

                        getUserProfiles(companyId,function (err1,users) {
                            if(users && users[0]){
                                if(!err1 && users && users.length){
                                    async.eachSeries(users, function (user, next){
                                        updateOpp(user,function(opps){
                                            next();
                                        });
                                    }, function(err) {
                                        runForSingleCompany(skip+1);
                                    });
                                } else {
                                    runForSingleCompany(skip+1);
                                }
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---",totalFound);
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

function updateOpp(user,callback){

    usersCollection.find({_id:user._id},{contacts:1}).toArray(function (err, result) {
        var bulk = contactsCollection.initializeUnorderedBulkOp();

        if(result && result[0].contacts && result[0].contacts.length>0){

            var opsExists = false;
            _.each(result[0].contacts,function (co) {

                if(co.personEmailId && co.contactRelation && co.contactRelation.prospect_customer){
                    opsExists = true;
                    bulk.find({ownerEmailId:user.emailId,"personEmailId": co.personEmailId})
                        .update({
                            $set:{
                                "contactRelation.prospect_customer":co.contactRelation.prospect_customer,
                                "contactRelation.cxo":co.contactRelation.cxo,
                                "contactRelation.influencer":co.contactRelation.influencer,
                                "contactRelation.decision_maker":co.contactRelation.decision_maker,
                                "contactRelation.partner":co.contactRelation.partner
                            }
                        }, { multi: true });
                }
            });

            if(opsExists){
                bulk.execute(function (err,res) {
                    console.log(err);
                    console.log(JSON.stringify(res,null,1));
                    callback()
                })
            } else {
                callback()
            }

        } else {
            callback()
        }

    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

startScript();
