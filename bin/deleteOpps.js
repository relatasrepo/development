var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var fs = require('fs');
var json2xls = require('json2xls');

var OppTrashCollection;
var opportunitiesCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('oppMetaData is running');
    startScript();
});

var startScript = function() {

    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.255:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
            var usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            OppTrashCollection = db.collection('oppTrash');

            var runForSingleUser = function (skip) {
                usersCollection.find({emailId: "bharadwaj.nagendra@ivalue.co.in"}, {
                //     usersCollection.find({corporateUser: true}, {
                // usersCollection.find({}, {
                    emailId: 1,
                    _id: 1,
                    companyId: 1
                }).skip(skip).limit(1).toArray(function (err, user) {

                    if(!err && user && user.length>0){
                        var companyId = user[0].companyId
                        var userEmailId = user[0].emailId;

                        updateEmailId(companyId,userEmailId,function () {
                            runForSingleUser(skip+1);
                        })
                    } else {
                        console.log("Done");
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

startScript();

function updateEmailId(companyId,userEmailId,callback){

    console.log(companyId)
    console.log(userEmailId)

    opportunitiesCollection.find({companyId:companyId,userEmailId:userEmailId}).toArray(function (err,opp) {
        if(!err && opp && opp.length>0){
            opp.forEach(function (op) {
                delete op._id;
                op.deletedBy = userEmailId;
            });

            console.log(opp.length)

            OppTrashCollection.insert(opp,function (iErr,result) {
                if(!iErr && result){
                    opportunitiesCollection.remove({companyId:companyId,userEmailId:userEmailId},function (rErr,rResult) {
                        callback(rErr,rResult)
                    });
                } else {
                    callback(iErr,result)
                }
            })
        } else {
            callback(err,false)
        }
    });
}