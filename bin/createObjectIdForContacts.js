
var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();
var ObjectID = require('mongodb').ObjectID;
var database = null;

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

//var async = require('async');
var dbUrl = 'mongodb://localhost:27017/June';

console.log("MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connect")
    if (err) {
        console.log(err)
    }

    database = db
    fetchInteractions(0)

});

var fetchInteractions = function(skip){
    database.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {
        console.log("Processing records " + (skip + 1) + " - " + (skip +1))
        database.collection("user").aggregate([
            { "$sort": { "_id": 1 }},
            { "$skip": skip },
            { "$limit": 1 },
            // {$match: {corporateUser:true}},
            {$unwind:"$contacts"},
            { "$match": {
                "contacts._id": {$exists:false}}
            },
            { "$group": {
                _id:"$emailId",
                contacts:{"$push":"$contacts"
                },
                count:{$sum:1}
            }}],function(err, contacts){
            if(err)
                console.log(err)
            // if(!contacts.length)
            // {
            //     console.log("done")
            //     return
            // }
            
            // console.log(JSON.stringify(contacts,null,2))
            // fetchInteractions(skip + 1)

            if(!err && contacts.length>0){

                var collection = database.collection("user")
                var batchRemove = collection.initializeUnorderedBulkOp();
                var batchUpdate = collection.initializeUnorderedBulkOp();

                var c = 0;
                var emailId = contacts[0]._id;

                console.log(JSON.stringify(contacts[0],null,2))

                _.each(contacts[0].contacts, function(contact){

                    console.log("--batch---",emailId,contact.personEmailId)
                    if(contact.personEmailId){

                        contact["_id"] = new ObjectID();

                        var find = { emailId: emailId, "contacts.personEmailId": contact.personEmailId,"contacts._id": {$exists:false}}

                        console.log(find)

                        batchRemove.find(find).update({$pull: {'contacts': {personEmailId: contact.personEmailId}}},{ multi: true });
                        batchUpdate.find({emailId: emailId}).update({$push: {'contacts': contact}});
                    }
                });

                batchRemove.execute(function(err, data){
                    if(err) {

                        console.log("--err--")
                        console.log(err)
                    }
                    else{
                        console.log(JSON.stringify(data,null,2))
                        if(data.getWriteErrors()[0])
                            console.log(data.getWriteErrors()[0].errmsg)
                        batchUpdate.execute(function(err, data){
                            fetchInteractions(skip + 1)
                        })
                    }
                })
            } else {
                fetchInteractions(skip + 1)
            }
        })
    })
}
