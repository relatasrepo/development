var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();

rule.hour = 1;
rule.minute = 30;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Relationship Relevance is running');
    start();
});

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
    //     mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/25Aug", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                var usersCollection = db.collection('user');
                var accountsCollection = db.collection('accounts');
                var opportunitiesCollection = db.collection('opportunities');

                var runForSingleUser = function (skip) {
                    usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"},{contacts:1,emailId:1,companyId:1}).skip(0).limit(1).toArray(function (err, user) {
                    // usersCollection.find({emailId: "naveenpaul@relatas.com"},{contacts:1,emailId:1}).skip(skip).limit(1).toArray(function (err, user) {
                    // usersCollection.find({corporateUser: true}).skip(skip).limit(1).toArray(function (err, user) {

                        if(!err && user && user.length>0 && user[0]){
                            var userProfile = JSON.parse(JSON.stringify(user[0]));

                            if(userProfile.contacts && userProfile.contacts.length>0){
                                updateStart(userProfile,opportunitiesCollection,accountsCollection,function (err,result) {
                                    console.log("Completed >",userProfile.emailId,skip);
                                    runForSingleUser(skip+1)
                                })
                            } else {
                                runForSingleUser(skip+1)
                            }

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

start();

function updateStart(userProfile,opportunitiesCollection,accountsCollection,callback) {
    getOppContacts(userProfile,opportunitiesCollection,function (accounts) {
        if(accounts && accounts.length>0){
            setAccAsImportant(userProfile,accountsCollection,accounts,callback)
        } else {
            callback()
        }
    });
}

function setAccAsImportant(userProfile,accountsCollection,accounts,callback) {

    var bulk = accountsCollection.initializeUnorderedBulkOp();

    var counter = 0;

    _.each(accounts,function (el) {
        bulk.find({companyId:castToObjectId(userProfile.companyId),name:el.toLowerCase()})
            .update({$set:{important: true}});

        counter++
    });

    if(counter>1){
        bulk.execute(function (err, result) {
            callback()
        });
    }
}

function getOppContacts(userProfile,opportunitiesCollection,callback){

    opportunitiesCollection.find({userId: castToObjectId(userProfile._id)},{contactEmailId:1}).toArray(function (err, contacts) {

        var accounts = [];

        if(!err && contacts && contacts.length>0){
            _.each(contacts,function (co) {
                if(co.contactEmailId){
                    var acc = fetchCompanyFromEmail(co.contactEmailId);
                    if(acc){
                        accounts.push(acc)
                    }
                }
            })
        }

        callback(_.uniq(accounts))
    });
}

function fetchCompanyFromEmail(email){

    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.remove(words, function(word){
        word = word.toLowerCase();
        return removableTextList.indexOf(word.trim()) > -1
    });

    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
}

function castToObjectId(id) {
    return mongoose.Types.ObjectId(id)
}

function comparer(otherArray){
    return function(current){
        return otherArray.filter(function(other){
                return other.name == current.name
            }).length == 0;
    }
}