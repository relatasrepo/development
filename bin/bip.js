var mongoose = require('mongoose');
var schedule = require('node-schedule');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;

var userCollection = require('../databaseSchema/userManagementSchema').User;
var companyCollection = require('../databaseSchema/corporateCollectionSchema').companyModel;

var googleCalendarAPI = require('../common/googleCalendar');
var ContactManagement = require('../dataAccess/contactManagement');
var outlookSync =  require('../routes/office365_bip.js');
var emailSender = require('../public/javascripts/emailSender');
var appCredentials = require('../config/relatasConfiguration');

var ImitateLoginProcessObj = new googleCalendarAPI();
var emailSenders = new emailSender();
var outlookSyncObj = new outlookSync();
var contactManagementObj = new ContactManagement();
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();

var bipExceptionEmailList = [
    "naveenpaul@relatas.com",
    "naveenpaul.markunda@gmail.com",
    // "sudip@relatas.com",
    // "robby@relatas.com"
]

if (appCredential.getServerEnvironment() == 'LOCAL' || appCredential.getServerEnvironment() == 'DEV' || appCredential.getServerEnvironment() == 'LIVE') {

    bipExceptionEmailList = [
        "naveenpaul@relatas.com",
    ]

    var dbPasswords = appCredential.getDBPasswords();
    mongoose.connect(dbPasswords.relatasDB.dbConnectionUrl, {user: dbPasswords.relatasDB.user, pass: dbPasswords.relatasDB.password, auth: { authdb: 'Relatas' }},function(error){

        if(error)
            emailSenders.sendUncaughtException(error, JSON.stringify(error), "error", appCredential.getServerEnvironment() + ": BIP",bipExceptionEmailList);
    });

    initScheduler();
}
else {
    mongoose.connect("mongodb://localhost/Relatas?poolSize=100");
    // mongoose.connect("mongodb://172.31.21.21:27017/Relatas?poolSize=100",{useMongoClient:true}); //LIVE
    initScheduler();
}

process.on('uncaughtException', function (err) {
    console.log('uncaughtException: message: ' + err.message + ' stack: ' + JSON.stringify(err));
    emailSenders.sendUncaughtException(err,err.message,err.stack,appCredential.getServerEnvironment()+": BIP",bipExceptionEmailList);
});

runForSingleUser(0,0);

function runForSingleUser(skip,zoneNumber) {

    var project = { contacts:0};
    var skip_limit = {skip:skip,limit:1};
    var notMatch = {'$regex' : '^((?!'+'22by7'+').)*$'}

    // userCollection.findOne({},project,skip_limit).lean().exec(function(err, userProfile) {
    userCollection.findOne({emailId:'naveenpaul.markunda@gmail.com'},project,skip_limit).lean().exec(function(err, userProfile) {
    // userCollection.findOne({corporateUser:true,zoneNumber:zoneNumber},project,skip_limit).lean().exec(function(err, userProfile) {
    // userCollection.findOne({corporateUser:true},project,skip_limit).lean().exec(function(err, userProfile) {

        // var userObj = JSON.parse(JSON.stringify(userProfile))
        var userObj = userProfile;

        console.log(err);
        if(userObj && userObj.companyId){
            console.log("\x1b[32m","Starting for user -", userProfile.emailId,userObj.companyId);
            companyCollection.find({_id:userObj.companyId},{url:1,currency:1,salesForceSettings:1},function (err,companyInfo) {

                companyInfo = JSON.parse(JSON.stringify(companyInfo));

                var currency = companyInfo && companyInfo[0] && companyInfo[0].currency?companyInfo[0].currency:"USD"

                if(userObj.google && userObj.google.length>0){
                    kickOffGoogleProcess(skip,zoneNumber,userObj,companyInfo[0].url,currency)
                } else if(userObj.outlook && userObj.outlook.length>0){
                    kickOffOutlookProcess(skip,zoneNumber,userObj,false,currency)
                } else {
                    runForSingleUser(skip + 1, zoneNumber);
                }
            });
        } else if(userObj && userObj.google && userObj.google.length>0){
            kickOffGoogleProcess(skip,zoneNumber,userObj,false)
        } else if(userObj && userObj.outlook && userObj.outlook.length>0){
            kickOffOutlookProcess(skip,zoneNumber,userObj,false)
        } else {
            console.log("\x1b[32m","BIP is done");
            emailSenders.sendUncaughtException("BIP DONE", "BIP DONE", "-----", appCredential.getServerEnvironment() + ": BIP",bipExceptionEmailList);
        }
    });
};

function getNewGoogleToken(token,refreshToken,callback){

    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id: authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, tokenNew) {

        if (err) {
            callback(false)
        }
        else{
            callback(tokenNew)
        }
    })
}

function kickOffOutlookProcess(skip,zoneNumber,user,subDomain,currency) {

    outlookSyncObj.syncAllData(user, function (syncResult) {

        updateAccounts(user._id,function () {

            if(syncResult.newToken){
                updateLastSyncDate(user._id,user);
            }
            runForSingleUser(skip + 1, zoneNumber);
        });
    });
}

function kickOffGoogleProcess(skip,zoneNumber,user,subDomain,currency,test) {

    if(test){
        console.log("Am I testing?")
    } else {
        if (user._id){

            var userId = castToObjectId(user._id);
            var lastLoginDate = user.lastDataSyncDate

            if (user && user.google.length > 0 && user.google[0]) {

                var testString = JSON.stringify(user.google); // To remove all backslashes.
                var parsedTokenArr = JSON.parse(testString);

                var parsedToken = parsedTokenArr[0];
                if (parsedTokenArr && parsedToken) {

                    getNewGoogleToken(parsedToken.token, parsedToken.refreshToken, function (token) {
                        if (token) {
                            console.log("Fetched token successfully for ", user.emailId)

                            ImitateLoginProcessObj.getContactsFromGoogleApi_bip(token, userId, lastLoginDate, function (contactResults) {
                                console.log("Updated Contacts successfully")

                                ImitateLoginProcessObj.getGoogleMeetingInteractions(userId,lastLoginDate,null,false,null,user.serviceLogin, function (calendarEventInteractionsResults) {
                                    console.log("Updated emails & meetings successfully ")
                                    updateLastSyncDate(userId,user);
                                    updateAccounts(userId,function () {
                                        runForSingleUser(skip + 1, zoneNumber)
                                    })
                                });//Meetings & Ints
                            }); // Contacts
                        } else {
                            console.log("No Token", user.emailId)
                            runForSingleUser(skip + 1, zoneNumber)//Google access token is missing.
                        }
                    });//End Token Refresh
                } else {
                    console.log("Parse failed - ", user.emailId)
                    runForSingleUser(skip + 1, zoneNumber)
                }
            } else {
                console.log("Google Object was corrupt - ", user.emailId)
                runForSingleUser(skip + 1, zoneNumber) // Google property is missing.
            }
        } else {
            console.log("User Object was corrupt - ", user.emailId)
            runForSingleUser(skip + 1, zoneNumber);
        }
    }
}

var castToObjectId = function(id){
    return mongoose.Types.ObjectId(id)
};

function initScheduler() {

    var sendDailyAgendaRuleZone1 = new schedule.RecurrenceRule(); //00:45 AM IST

    sendDailyAgendaRuleZone1.dayOfWeek = [0,new schedule.Range(1, 6)];
    sendDailyAgendaRuleZone1.hour = 23;  //19:15 UTC
    sendDailyAgendaRuleZone1.minute = 55;

    var sendDailyAgendaRuleObjectZone1 = schedule.scheduleJob(sendDailyAgendaRuleZone1, function(){
        console.log('Agenda mail Kicked off for zone ONE');
        runForSingleUser(0,1);
    });

}

function updateAccounts(userId,callback){
    contactManagementObj.updateAccounts(userId,function () {

        if(callback){
            callback()
        }
    })
}

function updateLastSyncDate(userId,user){

    userCollection.update({_id: castToObjectId(userId.toString())}, {"$set": {"lastDataSyncDate": new Date()}}, {upsert: true}, function (err, response) {
        if (!err && response) {
            // console.log("Last Data Sync updated for ", user.emailId);
        } else {
            console.log("Last Data sync update failed for ", user.emailId);
        }
    });

}
