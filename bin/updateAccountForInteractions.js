var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var date = require('date-and-time');
var timediff = require('timediff');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;

var start = function() {

    // mongoClient.connect("mongodb://172.31.38.246/Relatas", function(error, db) { // This is LIVE
    //     mongoClient.connect("mongodb://172.31.33.173/Relatas", function(error, db) { // This is Showcase
        mongoClient.connect("mongodb://localhost/Relatas", function(error, db) {
        if (error)
            throw error;
        else {
            console.log("Connected to db");
            db.authenticate("devuserre", "devpasswordre", function (err, res) {
                //     if (err) throw err;
                console.log('authenticated')
                var usersCollection = db.collection('user');
                var interactionCollection = db.collection('interactions');
                var companyCollection = db.collection('company');

                var runForSingleUser = function (skip) {
                    // usersCollection.find({emailId: "naveenpaul.markunda@gmail.com"}).skip(0).limit(1).toArray(function (err, user) {
                    // usersCollection.find({corporateUser: true}).skip(skip).limit(1).toArray(function (err, user) {
                    // companyCollection.find({url:{$ne:"*.relatas.com"}}).skip(skip).limit(1).toArray(function (err, company) {
                    companyCollection.find({url:/relatas.localhost.co/}).skip(skip).limit(1).toArray(function (err, company) {

                        if(!err && company && company.length>0){
                            var companyProfile = company[0];

                            console.log("*************    Start   ***********",skip)
                            console.log(companyProfile.url)

                            updateStart(companyProfile,interactionCollection,usersCollection,function (err,result) {

                                console.log("*************   Completed    *********",skip)
                                runForSingleUser(skip+1)
                            })

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            });
        }
    });
}

function updateStart(companyProfile,interactionCollection,usersCollection,callback) {

    var query = {
        corporateUser:true,
        companyId:companyProfile._id
    };

    usersCollection.find(query,{_id:1,emailId:1}).toArray(function(err, data){

        if(!err && data && data.length>0){
            updateInteractions(_.pluck(data,"_id"),companyProfile._id,interactionCollection,callback)
        } else {
            callback()
        }
    })

}

function updateInteractions(ownerIds,companyId,interactionCollection,callback) {

    var query = {
        ownerId:{$in:ownerIds}
    };

    interactionCollection.update(query,{$set:{companyId:companyId}},{multi:true},function(err,res){
        console.log("=========res");
        console.log(res);
        callback()
    });
}

start();