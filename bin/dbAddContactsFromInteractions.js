var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")
    , url = "mongodb://172.31.33.173:27017/Relatas"


var mongoose = require('mongoose');


var emailId = process.argv[2];
var user;

if(validateEmail(emailId)) {
    user ={"emailId":emailId}
} else if(emailId == "all") {
    user ={}
} else {
    console.log("==>Invalid Input: {" + emailId + "}. Please specify a valid Email ID or search by 'all'")
}
if(user){
    console.log("Starting process for User/s - " + JSON.stringify(user))
}

var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();

var serverEnvironment = appCredential.getServerEnvironment()
var dbPasswords = appCredential.getDbAuthBatch();

if(serverEnvironment == "LOCAL1"){
    console.log("This is LOCAL")
    //console.log("DB URL - " + dbPasswords.relatasDB.dbConnectionUrl)
    //console.log("DB User & Pwd - " + dbPasswords.relatasDB.user,dbPasswords.relatasDB.password)
} else if(serverEnvironment == "DEV"){
    console.log("This is SHOWCASE")
    //console.log("DB URL - " + dbPasswords.relatasDB.dbConnectionUrl)
    //console.log("DB User & Pwd - " + dbPasswords.relatasDB.user,dbPasswords.relatasDB.password)
} else if(serverEnvironment == "STAGING") {
    console.log("This is STAGING")
    //console.log("DB URL - " + dbPasswords.relatasDB.dbConnectionUrl)
} else {
    // console.log("This is LIVE")
    //console.log("DB URL - " + dbPasswords.relatasDB.dbConnectionUrl)
}

MongoClient.connect(url, function(err, db) {

    db.authenticate('devuserre', 'devpasswordre', function (errAuth, res) {
        console.log("Connect DataBase SUCCESS")
        console.log(JSON.stringify(errAuth))

        var collectionUser = db.collection("user")
        var collectionInteractions = db.collection("interaction")

        collectionUser.find(user).toArray(function(err, users){
            _.each(users, function(user){
                collectionInteractions.aggregate(
                    {$match: {emailId: user.emailId}},
                    {$unwind:"$interactions"},
                    {$project:{"interactions":{$toLower:"$interactions.emailId"}}},
                    {$group :
                        {
                            "_id":null,
                            "emailsArray" : {"$addToSet" : "$interactions"}
                        }
                    },
                    function(err, interactionResult){
                        if(err){
                            console.log("Interaction Errors - " + err)
                        }
                        if(user.emailId && interactionResult[0]){
                            var contacts = user.contacts
                            var emailsContacts = _.map(contacts,"personEmailId")

                            console.log("User is - " + user.emailId)
                            console.log("User's Contacts are- " + JSON.stringify(emailsContacts,null,4))
                            console.log("Interactions EmailIds are - " + JSON.stringify(interactionResult[0].emailsArray,null,4))

                            var nonExistentEmails = interactionResult[0].emailsArray.filter( function( el ) {
                                return emailsContacts.indexOf( el ) < 0;
                            })

                            console.log("Non Existent Contacts - " + JSON.stringify(nonExistentEmails,null, 4))

                            if(nonExistentEmails.length>0){
                                for(var k =0;k<nonExistentEmails.length;k++) {
                                    if(nonExistentEmails[k]){
                                        var contactObj = {
                                            personId: null,
                                            personName: nonExistentEmails[k],
                                            personEmailId: nonExistentEmails[k].toLowerCase(),
                                            birthday: null,
                                            companyName: null,
                                            designation: null,
                                            addedDate: new Date(),
                                            verified: false,
                                            relatasUser: false,
                                            relatasContact: true,
                                            mobileNumber: null,
                                            location: null,
                                            source: 'google-email-interactions',
                                            account: {
                                                name: fetchCompanyFromEmail(nonExistentEmails[k]),
                                                selected: false,
                                                showToReportingManager: false,
                                                updatedOn: new Date()
                                            }
                                        };

                                        collectionUser.update(
                                            {"emailId": user.emailId, 'contacts.personEmailId': {$ne: contactObj.personEmailId}},
                                            {
                                                $push: {
                                                    contacts: contactObj
                                                }
                                            },function(err,result){

                                                if(!err){
                                                    console.log("Result of adding a contact - " + JSON.stringify(result))
                                                    console.log("Successfully added contact - " + contactObj.personEmailId + "to User -" + user.emailId)
                                                } else {
                                                    console.log("There was an error while adding contact - " +contactObj.personEmailId + "to User - " + user.emailId)
                                                }
                                            });
                                    }
                                }//End for
                            } else {
                                console.log("Contact List is already updated for User - " + user.emailId)
                                console.log("Press 'CTRL C' to exit")
                            }
                        }
                    });
            })//End for each user
        })
    })
})

function fetchCompanyFromEmail(email){
    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.remove(words, function(word){
        return removableTextList.indexOf(word.trim()) > -1
    })
    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : words[0]
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}