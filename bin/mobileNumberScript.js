/**
 * Created by rajiv on 21/5/16.
 */

var appCredentials = require('../config/relatasConfiguration');
var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();
var _ = require('lodash')

var mongoClient = require('mongodb').MongoClient

var dbUrl = process.argv[2]

var async = require('async')

console.log("***** Running Mobile Number Script :" + new Date());
console.log("***** Connecting to mongoDB @ " + dbUrl)
mongoClient.connect(dbUrl, function (err, db) {

    if (err) {
        console.log("ERROR")
        console.log(err)

    } else {
        db.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {


            /*if (err) {
                console.log("Authentication Failed!")
                console.log(err)
            }*/

            var UserCollection = db.collection("user")
            UserCollection.find().sort({_id: 1}).toArray(function (err, result) {

                if (err) {
                    console.log(err)
                } else {

                    var bulk = UserCollection.initializeUnorderedBulkOp();
                    _.each(result, function (profile) {
                        var mobileNumber
                        var mobileNumberWithoutCC
                        var countryCode

                        var updateObj = {}
                        if (profile.mobileNumber != "" && profile.mobileNumber != "undefined" && profile.mobileNumber != null) {

                            mobileNumber = profile.mobileNumber
                            mobileNumber = mobileNumber.replace(/\D/g, '')

                            if (mobileNumber != "" && mobileNumber != null) {
                                updateObj.mobileNumber = mobileNumber

                                var length = profile.mobileNumber.length
                                if (length > 10) {
                                    mobileNumberWithoutCC = mobileNumber.substring((length - 10), length)
                                    countryCode = mobileNumber.substring(0, mobileNumber.length - 10)
                                    updateObj.mobileNumberWithoutCC = mobileNumberWithoutCC
                                    updateObj.countryCode = countryCode
                                } else {
                                    updateObj.mobileNumberWithoutCC = mobileNumber
                                    updateObj.countryCode = null
                                }
                            }
                            bulk.find({_id: profile._id}).upsert().update({
                                $set: {
                                    mobileNumber: updateObj.mobileNumber,
                                    mobileNumberWithoutCC: updateObj.mobileNumberWithoutCC,
                                    countryCode: updateObj.countryCode
                                }
                            })
                        }
                    })
                    bulk.execute(function (err, res) {
                        console.log(err, res)
                    });
                }
            })
        })
    }
})
