var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var date = require('date-and-time');
var timediff = require('timediff');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var mongoose = require('mongoose');

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;

var start = function() {


    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.255:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@172.31.47.181:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            console.log("Connected to db");
            console.log('authenticated')
                var usersCollection = db.collection('user');
                var opportunitiesCollection = db.collection('opportunities');

                var runForSingleUser = function (skip) {
                    usersCollection.find({companyId:{$nin:[null,""," "]}},{contacts:0}).limit(1).skip(skip).toArray(function (err, users) {
                        if(!err && users && users.length>0){
                            var emailIds = _.pluck(users,"emailId");
                            var userProfile = users[0];

                            updateStart(emailIds,userProfile.companyId,opportunitiesCollection,function (err,result) {
                                console.log("Completed >",userProfile.emailId,skip)
                                runForSingleUser(skip+1)
                            })

                        } else {
                            console.log("Done!")
                        }

                    });
                };

                runForSingleUser(0);

            };
        })
    };

function updateStart(emailIds,companyId,opportunitiesCollection,callback) {
    opportunitiesCollection.update({userEmailId:{$in:emailIds},sourceOpportunityId:{$nin:[null," ",""]}},{$set:{companyId:companyId}},{multi:true},function (err,results) {
        console.log(err)
        console.log(JSON.stringify(results,null,1))
        callback()
    });
}

start();