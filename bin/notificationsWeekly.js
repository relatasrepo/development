var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async');
var admin = require('../config/firebaseConfiguration');
var notificationManager = require('./notificationManager.js');

var usersCollection;
var companyCollection;
var dealsAtRiskCollection;
var opportunityCollection;
var shCollection;
var opportunitiesTargetCollection;

var mondayNotificationBuilder = function() {
    notificationManager.getDbInstance(function(db) {

        usersCollection = db.collection('user');
        companyCollection = db.collection('company');
        dealsAtRiskCollection = db.collection('dealsAtRiskMeta');
        opportunityCollection = db.collection('opportunities');
        shCollection = db.collection('secondaryHierarchy');
        opportunitiesTargetCollection = db.collection('opportunitiesTarget');

        runForSingleCompany = function(skip) {
            companyCollection.find({}).skip(skip).limit(1).toArray(function(error1, company) {
                if(!error1 && company && company.length > 0) {
                    var companyId = company[0]._id;
                    var companyName = company[0].companyName;

                    notificationManager.getUserProfiles(companyId, function(error2, users) {
                        if(!error2 && users[0]) {

                            notificationManager.userFiscalYear(company[0],users[0],function (fyMonth,fyRange,allQuarters) {

                                async.eachSeries(users, function(user, next) {
                                    buildDataForMondayNotification(user, company[0], function() {
                                        next();
                                    })
                                }, function(error3) {
                                    console.log("done for company:", companyName);
                                    runForSingleCompany(skip+1)                            
                                })
                            })
                        } else {
                            runForSingleCompany(skip+1)                            
                        }
                    })
                } else {
                    console.log("-------------------- **ALL DONE** ----------------------------------------");
                }
            })
        }
        runForSingleCompany(0);
    })
}
module.exports.mondayNotificationBuilder = mondayNotificationBuilder;

function buildDataForMondayNotification(user, company, callback) {
    if(user.mobileFirebaseToken || user.webFirebaseSettings) {

        var mobileToken = user.mobileFirebaseToken;
        var webToken = user.webFirebaseSettings ? user.webFirebaseSettings.firebaseToken : null;

        notificationManager.getAllReportees(user,function (allReportees,allEmailIds,allUserIds) {
            // if(allUserIds.length > 1) {

                var primaryCurrency = "USD",
                currenciesObj = {}
    
                company.currency.forEach(function (el) {
                    currenciesObj[el.symbol] = el;
                    if(el.isPrimary){
                        primaryCurrency = el.symbol;
                    }
                });
    
                async.parallel([
                    function(callback) {
                        getOppForWeek(allEmailIds, user.companyId, primaryCurrency, currenciesObj, user.timezone, callback);
                    }
                ], function(err, data) {
                    if(err) {
                        console.log(err)
                    }
                    buildNotificationData1(data, company._id, user._id, user.emailId, webToken, mobileToken, primaryCurrency, function() {
                        callback();
                    })
                })

            /* } else {
                callback();
            } */
        })
        
    } else {
        callback();
    }
}

var wednesdayNotificationBuilder = function() {
    notificationManager.getDbInstance(function(db) {

        usersCollection = db.collection('user');
        companyCollection = db.collection('company');
        dealsAtRiskCollection = db.collection('dealsAtRiskMeta');
        opportunityCollection = db.collection('opportunities');
        shCollection = db.collection('secondaryHierarchy');
        opportunitiesTargetCollection = db.collection('opportunitiesTarget');


        runForSingleCompany = function(skip) {
            companyCollection.find({}).skip(skip).limit(1).toArray(function(error1, company) {
                if(!error1 && company && company.length > 0) {
                    var companyId = company[0]._id;
                    var companyName = company[0].companyName;

                    notificationManager.getUserProfiles(companyId, function(error2, users) {
                        if(!error2 && users[0]) {

                            notificationManager.userFiscalYear(company[0],users[0],function (fyMonth,fyRange,allQuarters) {

                                async.eachSeries(users, function(user, next) {
                                    buildDataForWednesdayNotification(user, company[0], fyMonth, fyRange, allQuarters, function() {
                                        next();
                                    })
                                }, function(error3) {
                                    console.log("done for company:", companyName);
                                    runForSingleCompany(skip+1)                            
                                })
                            })
                        } else {
                            runForSingleCompany(skip+1)                            
                        }
                    })
                } else {
                    console.log("-------------------- **ALL DONE** ----------------------------------------");
                }
            })
        }
        runForSingleCompany(0);
    })
}
module.exports.wednesdayNotificationBuilder = wednesdayNotificationBuilder;

function buildDataForWednesdayNotification(user, company, fyMonth, fyRange, allQuarters, callback) {
    if(user.mobileFirebaseToken || user.webFirebaseSettings) {

        var mobileToken = user.mobileFirebaseToken;
        var webToken = user.webFirebaseSettings ? user.webFirebaseSettings.firebaseToken : null;

        notificationManager.getAllReportees(user,function (allReportees,allEmailIds,allUserIds) {
            if(allUserIds.length > 1) {
                // allReportees = getReporteesDictionary(allReportees);

                var timezone = user.timezone && user.timezone.name ? user.timezone.name : "Asia/Kolkata";
                var primaryCurrency = "USD",
                currenciesObj = {}
    
                company.currency.forEach(function (el) {
                    currenciesObj[el.symbol] = el;
                    if(el.isPrimary){
                        primaryCurrency = el.symbol;
                    }
                });
    
                var monthStart = moment().startOf('month').tz(timezone).format();
                var monthEnd = moment().endOf('month').tz(timezone).format();
    
                async.parallel([
                    function(callback) {
                        getManagerAchievements(allUserIds, allEmailIds, monthStart, monthEnd, fyRange.start, fyRange.end, primaryCurrency, currenciesObj, callback);
                    }
                ], function(err, data) {
                    if(err) {
                        console.log(err)
                    }
                    buildNotificationData2(data, company._id, user._id, user.emailId, webToken, mobileToken, primaryCurrency, function() {
                        callback();
                    })
                })

            } else {
                callback();
            }
        })
        
    } else {
        callback();
    }
}

function getOppForWeek(userEmailId, companyId, primaryCurrency, currenciesObj, timezone, callback) {
    var totalOppValue = 0;

    var timezone = timezone && timezone.name?timezone.name:"UTC";
    var fromDate = moment(new Date()).tz(timezone).startOf('day').toDate();
    var toDate = moment(fromDate).add(7, "days").tz(timezone).endOf('day').toDate();

    var query = {
        $or:[{companyId:companyId},{companyId:companyId.toString()}],
        userEmailId:{$in:userEmailId}
    }

    query.closeDate = {
        $gte: new Date(moment(fromDate)),
        $lte: new Date(moment(toDate))
    }

    query.stageName = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    query.isClosed = {$ne:true}
    query.relatasStage = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    opportunityCollection.find(query).toArray(function(err, opps) {

        if(!err && opps && opps.length>0){  
            _.each(opps,function (op) {
                var accName = fetchCompanyFromEmail(op.contactEmailId);
                accName = accName?accName:"Others"

                op.accountName = accName;
                op.amount = parseFloat(op.amount);
                op.amountWithNgm = op.amount;

                if(op.netGrossMargin || op.netGrossMargin == 0){
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                }

                op.convertedAmt = op.amount;

                op.convertedAmtWithNgm = op.amountWithNgm

                if(op.currency && op.currency !== primaryCurrency){

                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                    }

                    if(op.netGrossMargin || op.netGrossMargin == 0){
                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                    }

                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2))
                }
                totalOppValue += op.amountWithNgm;

            });

            callback(err, {
                totalOpps: opps.length,
                oppValue: totalOppValue,
                primaryCurrency: primaryCurrency,
                data: opps
            })
        } else {
            callback(err,{
                totalOpps: 0,
                oppValue: 0,
                primaryCurrency: primaryCurrency,
                data: []
            })
        }
    })
}

function getManagerAchievements(userIds, emailIds, monthStart, monthEnd, fyStart, fyEnd, primaryCurrency, currenciesObj, callback) {
    async.parallel([
        function(callback) {
            getTeamTargets(userIds, monthStart, monthEnd, fyStart, fyEnd, callback)
        },
        function(callback) {
            getOppWonLostValue(emailIds, monthStart, monthEnd, primaryCurrency, currenciesObj, callback);
        }]
        ,function(error, data) {
            callback(error, {
                target: data[0].target,
                won: data[1].opportunityWon,
                lost: data[1].opportunityLost
            });
        })
}

function getTeamTargets(userId,monthStart,monthEnd,fyStart,fyEnd,callback){
    var aggregation = [
        {
            $match:{
                userId:{$in:userId},
                "fy.start":{$gte:new Date(fyStart)},
                "fy.end":{$lte:new Date(fyEnd)}
            }
        },
        {
            $unwind:"$salesTarget"
        },
        {
            $match:{
                "salesTarget.date":{$lte:new Date(monthEnd),$gte:new Date(monthStart)}
            }
        },
        {
            $project: {
                month: { "$month": "$salesTarget.date" },
                year: { "$year": "$salesTarget.date" },
                target: "$salesTarget.target",
                date:"$salesTarget.date"
            }
        },
        {
            $group:{
                _id:{month:"$month",year:"$year"},
                date:{ $max: "$date" },
                month:{ $max: "$month" },
                year:{ $max: "$year" },
                target:{$sum:"$target"}

            }
        }
    ];

    opportunitiesTargetCollection.aggregate(aggregation).toArray(function (err,targets) {
        if(err && !targets[0]){
            callback(null,{
                target:0
            })
        }
        else{
            callback(err,{
                target:targets[0].target,
            });
        }
    })
}

var getOppWonLostValue = function(emailIds, monthStart, monthEnd, primaryCurrency, currenciesObj, callback) {
    var match = {
        userEmailId: {$in:emailIds},
        stageName: {$in:["Close Won", "Close Lost"]},
        closeDate: {$gte:new Date(monthStart), $lte:new Date(monthEnd)}
    }

    var project = {
        opportunityName:1,
        netGrossMargin:1,
        amount:1,
        stageName:1,
        currency:1,
        _id:0
    }

    opportunityCollection.find(match).project(project).toArray(function(error, opportunities) {
        var closeWonAmount=0, closeLostAmount = 0;

        _.each(opportunities, function(opp) {
            opp.amountWithNgm = opp.amount;

            if(opp.netGrossMargin || opp.netGrossMargin == 0){
                opp.amountWithNgm = (opp.amount*opp.netGrossMargin)/100
            }

            opp.convertedAmt = opp.amount;
            opp.convertedAmtWithNgm = opp.amountWithNgm

            if(opp.currency && opp.currency !== primaryCurrency){

                if(currenciesObj[opp.currency] && currenciesObj[opp.currency].xr){
                    opp.convertedAmt = opp.amount/currenciesObj[opp.currency].xr
                }

                if(opp.netGrossMargin || opp.netGrossMargin == 0){
                    opp.convertedAmtWithNgm = (opp.convertedAmt*opp.netGrossMargin)/100;
                }

                opp.convertedAmt = parseFloat(parseFloat(opp.convertedAmt).toFixed(2))
            }

            if(opp.stageName == "Close Won")
                closeWonAmount += parseFloat(opp.convertedAmtWithNgm);
            else
                closeLostAmount = parseFloat(opp.convertedAmtWithNgm);
        });
        if(error) {
            callback(null, {
                opportunityWon:0,
                opportunityLost:0
            })
        } else {
            callback(error, {
                opportunityWon: parseFloat(closeWonAmount.toFixed(2)),
                opportunityLost: parseFloat(closeLostAmount.toFixed(2))
            });
        }
        
    })
}

// Monday
function buildNotificationData1(notificationData, companyId, userId, emailId, webToken, mobileToken, primaryCurrency, callback) {    
    var dayString = moment().format("DDMMYYYY");

    async.parallel([
        // function to send notification for opportunity closing this week
        function(callback) {
            if( notificationData[0] && notificationData[0].data.length <= 0 ) {
                callback(null, null);

            } else {

                var payload = {
                    notification: {
                        body: "There are " + notificationData[0].totalOpps + " opp(s) worth " + notificationData[0].primaryCurrency + " " +
                                + notificationData[0].oppValue + " closing this week",
                        title: "Opportunity closing this week",
                        click_action: "MY_ACTION"
                    },
                    data: {
                        category: "oppClosingThisWeek",
                        dayString: dayString,
                        click_action: "/opportunities/all?notifyCategory=oppClosingThisWeek&notifyDate=" + dayString
                    }
                };

                notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                    callback(error, {
                        messageIds: msgId,
                        title: payload.notification.title,
                        body: payload.notification.body,
                        url: payload.data.click_action,
                        payloadData: JSON.stringify(notificationData[0].data),
                        notificationType: "weekly",
                        category: "oppClosingThisWeek"
                    });
                })
            }
        }
    ], function(err, messageIds) {
        if(err) {
            console.log(err)
        }
        console.log("Sent notificatin for:",emailId);
        notificationManager.storeNotificationData(messageIds, companyId, userId, emailId, function() {
            callback()
        })
    })
}

// Wednesday
function buildNotificationData2(notificationData, companyId, userId, emailId, webToken, mobileToken, primaryCurrency, callback) {    

    async.parallel([
        // function to send notification for manager tartet/won/lost
        function(callback) {
            var payload = {
                notification: {
                    body: "Target - " + notificationManager.numberWithCommas(notificationData[0].target, primaryCurrency == "INR") + " | " +
                            "Won - " + notificationManager.numberWithCommas(notificationData[0].won, primaryCurrency == "INR") + " | " +
                            "Lost - " + notificationManager.numberWithCommas(notificationData[0].lost, primaryCurrency == "INR"),
                    title: "Team Monthly Achievement",
                    click_action: "MY_ACTION"
                },
                data: {
                    category: "targetWonLostManager",
                    click_action: "/team/commit/review?notifyCategory=targetWonLostManager&notifyDate="+moment().format("DDMMYYYY")
                }
            };

            notificationManager.sendNotification(payload, webToken, mobileToken, function(error, msgId) {
                callback(error, {
                    messageIds: msgId,
                    title: payload.notification.title,
                    body: payload.notification.body,
                    url: payload.data.click_action,
                    payloadData: JSON.stringify(notificationData[0]),
                    notificationType: "weekly",
                    category: "targetWonLostManager"
                });
            })
        }
    ], function(err, messageIds) {
        if(err) {
            console.log(err)
        }
        console.log("Sent notificatin for:",emailId);
        notificationManager.storeNotificationData(messageIds, companyId, userId, emailId, function() {
            callback()
        })
    })
}

function fetchCompanyFromEmail(email){

    if(email){
        var domain = email.substring(email.lastIndexOf("@") +1)
            , words = domain.split(".")
            , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
            , removableTextList = ["com", "org", "edu", "in"]

        _.remove(words, function(word){
            word = word.toLowerCase();
            return removableTextList.indexOf(word.trim()) > -1
        })
        words = _.sortBy(words, function(word){return -(word.length);})
        return (personalMailDomains.indexOf(words[0]) > -1) ? null : (words[0])
    } else {
        return null
    }
}

if(require.main == module) {
    mondayNotificationBuilder();
    // wednesdayNotificationBuilder();
}