var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async')

var interactionsCollection;
var opportunitiesCollection;
var accInteractionCollection;
var usersCollection;
var shCollection;
var companyCollection;
var dealsAtRiskMetaCollection;
var OppMetaDataCollection;
var dashboardInsightsCollection;
var opportunitiesTargetCollection;
var accountInsightsCollection;
var newCompaniesInteraction;
var DUPLICATE_OPPS_COUNT = 5000;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 3;
rule.minute = 59;
rule.second = 59;
schedule.scheduleJob(rule, function() {
    console.log('Dashboard Insights is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_masked?authSource=Relatas_mask", function(error, client) { // This is BKUP_PROD
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    // mongoClient.connect("mongodb://localhost", function(error, client) {
        console.log("client")
        if (error)
            throw error;
        else {
            // var db = client.db('newDb');
            var db = client.db('Relatas');
            opportunitiesCollection = db.collection('opportunities');

            var runForSingleCompany = function (skip) {
                opportunitiesCollection.find({}).toArray(function (err, opps) {
                    var bulk = opportunitiesCollection.initializeUnorderedBulkOp();

                    _.each(opps,function (el) {
                        if(!el.createdDate){
                            el.createdDate = el._id.getTimestamp();
                        }

                        bulk.find({ _id: el._id}).update({ $set: {createdDate:new Date(el.createdDate)} });
                    });

                    bulk.execute(function(err, result) {
                        if (err) {
                            console.log(err)
                        } else {
                            result = result.toJSON();
                        }
                        console.log(err)
                        console.log(result)
                    });
                });
            };

            runForSingleCompany(0);
        }
    });
}

startScript();