var gmailApi = require('node-gmail-api');
var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();

var MongoClient = require('mongodb').MongoClient
    , _ = require("lodash")

var emailId = process.argv[2];
var dbUrl = process.argv[3];
var afterArgV = process.argv[4];
var beforeArgV = process.argv[5];

console.log("User Email ID is : " + emailId);
console.log("MongoDb URL is : " + dbUrl);

MongoClient.connect(dbUrl, function(err, db) {
    console.log("Connection Successful")
    if (err) {
        console.log(err)
    }

    db.authenticate(dbPasswords.relatasDB.user, dbPasswords.relatasDB.password, function (err, res) {

        var interactionsDirect = [];
        var interactionsOther = [];
        var refIdArr = [];
        var emailIdArr = [];

        var filter = '';
        var after = new Date();
        var before = new Date();
        var now = new Date();

        after.setDate(after.getDate() - 90);
        before.setDate(before.getDate() + 2);
        var monthAfter = after.getMonth()+1;
        var monthBefore = before.getMonth()+1;
        filter = 'after:'+after.getFullYear()+'/'+monthAfter+'/'+after.getDate()+' before:'+before.getFullYear()+'/'+monthBefore+'/'+before.getDate();

        console.log("--Filter--")
        console.log(filter)
        var afterBefore = '';
        afterBefore = afterArgV +' '+ beforeArgV;

        console.log(afterBefore)

        var collection = db.collection("user");
        var collectionInteraction = db.collection("interaction");
        collection.find({"emailId":emailId},{contacts:0}).toArray(function(err, user){

            getNewGoogleToken(user[0].google[0].token,user[0].google[0].refreshToken,function(token){
                console.log("==>Old Token is:")
                console.log(user[0].google[0].token)
                console.log("==>New Token is:")
                console.log(token)
                var gmail = new gmailApi(token);
                var s = gmail.messages(afterBefore);

                //console.log(JSON.stringify(s,null,2));

                s.on('end',function(){
                    mapUserDataToInteractions(user[0],interactionsDirect,interactionsOther,refIdArr,emailIdArr,collection,collectionInteraction);
                });

                s.on('error',function(a){
                    mapUserDataToInteractions(user[0],interactionsDirect,interactionsOther,refIdArr,emailIdArr,collection,collectionInteraction);
                });

                s.on('data', function (d) {
                    if(d.payload.headers.length > 0){
                        var headers = d.payload.headers;
                        var email = parseEmailHeader(d.id,headers);

                        if(email && email.id){
                            var obj = generateAndStoreInteraction(user[0],email,false,email,email);
                            interactionsDirect = interactionsDirect.concat(obj.interactionDirect);
                            interactionsOther = interactionsOther.concat(obj.interactionOther);
                            refIdArr.push(obj.refId);
                            emailIdArr = emailIdArr.concat(obj.emailIdArr);
                        }
                    }
                })
            });
        })
    })
})

function mapUserDataToInteractions(user,interactionsDirect,interactionsOther,refIdArr,emailIdArr,collection,collectionInteraction){

    if(emailIdArr.length > 0){
        emailIdArr = removeDuplicates_id(emailIdArr,true);
        var query = {"emailId":{$in:emailIdArr}};

        var projection = {emailId:1,firstName:1,lastName:1,publicProfileUrl:1,profilePicUrl:1,companyName:1,designation:1,mobileNumber:1,skypeId:1,location:1};

        collection.find(query,projection).toArray(function(error,users){

            if(error){
                console.log('Error Linkedin mapUserDataToInteractions():GoogleCalendar selectUserProfilesCustomQuery() ',error);
                addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,collectionInteraction);
            }
            else if(users && users.length > 0){
                for(var u=0; u<users.length; u++){
                    for(var int=0; int<interactionsOther.length; int++) {
                        if (interactionsOther[int].emailId == users[u].emailId) {
                            interactionsOther[int].userId = users[u]._id;
                            interactionsOther[int].firstName = users[u].firstName;
                            interactionsOther[int].lastName = users[u].lastName;
                            interactionsOther[int].publicProfileUrl = users[u].publicProfileUrl;
                            interactionsOther[int].profilePicUrl = users[u].profilePicUrl;
                            interactionsOther[int].companyName =  fetchCompanyFromEmail(users[u].emailId)
                            interactionsOther[int].designation = users[u].designation;
                            interactionsOther[int].location = users[u].location;
                            interactionsOther[int].mobileNumber = users[u].mobileNumber;
                            interactionsOther[int].emailId = users[u].emailId;
                        }
                    }
                }
                addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,collectionInteraction);
            }
            else addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,collectionInteraction);
        })
    }
    else{
        addInteractionsBatch(user,interactionsDirect,interactionsOther,refIdArr,collectionInteraction);
    }

    //Add each contact from the email interactions to the user's contact if they do not exist
    var cleanArray = removeDuplicates_id(emailIdArr,true).filter(function (elem) {
        var ok = true
        getInvalidEmailList().forEach(function (tester) {
            if (tester.test(elem)) {
                ok = false;
                return false;
            }
        });
        return ok
    });

    for(var i =0;i<cleanArray.length;i++){
        var contactObj = {
            personId:null,
            personName:cleanArray[i],
            personEmailId:cleanArray[i].toLowerCase(),
            birthday:null,
            companyName:null,
            designation:null,
            addedDate:new Date(),
            verified:false,
            relatasUser:false,
            relatasContact:true,
            mobileNumber:null,
            location:null,
            source:'google-email-interactions',
            account:{
                name:fetchCompanyFromEmail(cleanArray[i]),
                selected:false,
                updatedOn:new Date()
            }
        };

        addContactFromGoogleInteractions(user.emailId,contactObj,false,collection);
    }
}

function parseEmailHeader(id,headers){

    var email = {emailContentId:id};
    for(var i=0; i<headers.length; i++){
        if(headers[i].name == 'From' || headers[i].name == 'from'){
            email.from = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'To' || headers[i].name == 'to'){
            email.To = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Subject' || headers[i].name == 'subject'){
            email.subject = headers[i].value;
        }
        if(headers[i].name == 'Cc' || headers[i].name == 'cc'){
            email.Cc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Bcc' || headers[i].name == 'bcc'){
            email.Bcc = extractEmails(headers[i].value);
        }
        if(headers[i].name == 'Date' || headers[i].name == 'date'){
            email.date = new Date(headers[i].value);
        }
        if(headers[i].name == 'Message-Id' || headers[i].name == 'Message-ID'){
            email.id = headers[i].value;
        }
    }

    var isValid = true;
    if(email.to && email.to.length > 0){
        for(var to in email.to){
            isValid = !matchInArray(email.to[to].split('@')[0])
        }
    }
    if(email.from && email.from.length > 0){
        for(var from in email.from){
            isValid = !matchInArray(email.from[from].split('@')[0])
        }
    }
    if(email.Cc && email.Cc.length > 0){
        for(var Cc in email.Cc){
            isValid = !matchInArray(email.Cc[Cc].split('@')[0])
        }
    }
    if(email.Bcc && email.Bcc.length > 0){
        for(var Bcc in email.Bcc){
            isValid = !matchInArray(email.Bcc[Bcc].split('@')[0])
        }
    }

    return isValid ? email : null;
}
function generateAndStoreInteraction(user,email,office,officeEmail,googleEmail){

    var interactionDirect = [];
    var interactionOther = [];
    var interactionRefId = [];
    var emailIdArr = [];
    var isSender = false;

    if(email.from){
        for(var from=0; from<email.from.length; from++){
            var emailIdFrom = email.from[from] ? email.from[from] : '';
            if(office && googleEmail == emailIdFrom){
                emailIdFrom = officeEmail;
            }

            if(emailIdFrom == user.emailId){
                isSender = true;
                interactionDirect.push(getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'sender','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else{
                emailIdArr.push(emailIdFrom);
                interactionOther.push(getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'sender','email','email',emailIdFrom,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }

    if(email.To){
        for(var to=0; to<email.To.length; to++){
            var emailIdTo = email.To[to] ? email.To[to] : '';
            if(office && googleEmail == emailIdTo){
                emailIdTo = officeEmail;
            }

            if(emailIdTo == user.emailId){
                interactionDirect.push(getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else if(isSender){
                emailIdArr.push(emailIdTo);
                interactionOther.push(getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',emailIdTo,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }

    if(email.Cc){
        for(var cc=0; cc<email.Cc.length; cc++){
            var emailIdCc = email.Cc[cc] ? email.Cc[cc] : '';
            if(office && googleEmail == emailIdCc){
                emailIdCc = officeEmail;
            }

            if(emailIdCc == user.emailId){
                interactionDirect.push(getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else if(isSender){
                emailIdArr.push(emailIdCc);
                interactionOther.push(getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',emailIdCc,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }
    if(email.Bcc){
        for(var bcc=0; bcc<email.Bcc.length; bcc++){
            var emailIdBcc = email.Bcc[bcc] ? email.Bcc[bcc] : '';
            if(office && googleEmail == emailIdBcc){
                emailIdBcc = officeEmail;
            }

            if(emailIdBcc == user.emailId){
                interactionDirect.push(getInteractionObject(user._id,null,null,user.firstName,user.lastName,user.publicProfileUrl,user.profilePicUrl,user.companyName,user.designation,user.location,user.mobileNumber,'receiver','email','email',user.emailId,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
            else if(isSender){
                emailIdArr.push(emailIdBcc);
                interactionOther.push(getInteractionObject(null,null,null,null,null,null,null,null,null,null,null,'receiver','email','email',emailIdBcc,email.id,'google',email.subject,null,new Date(email.date),null,null,null,email.emailContentId,googleEmail));
            }
        }
    }

    return {
        interactionDirect:interactionDirect,
        interactionOther:interactionOther,
        interactionRefId:interactionRefId,
        refId:email.id,
        emailIdArr:emailIdArr
    }
}

function matchInArray(stringSearch){
    var position = String(getInvalidEmailList()).search(stringSearch);
    return (position > -1);
}

function extractEmails (text)
{
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function getNewGoogleToken(token,refreshToken,callback){
    var tokenProvider = new GoogleTokenProvider({
        refresh_token: refreshToken,
        client_id:     authConfig.GOOGLE_CLIENT_ID,
        client_secret: authConfig.GOOGLE_CLIENT_SECRET,
        access_token: token
    });

    tokenProvider.getToken(function (err, token) { // start of token provider
        if (err) {
            callback(false)
        }
        else{
            callback(token)
        }
    })
}

function removeDuplicates_id(arr,withoutId){

    var end = arr.length;

    for(var i = 0; i < end; i++)
    {
        for(var j = i + 1; j < end; j++)
        {
            if (!withoutId ? arr[i]._id == arr[j]._id : arr[i] == arr[j])
            {
                var shiftLeft = j;
                for(var k = j+1; k < end; k++, shiftLeft++)
                {
                    arr[shiftLeft] = arr[k];
                }
                end--;
                j--;
            }
        }
    }

    var whitelist = [];
    for(var i = 0; i < end; i++){
        whitelist[i] = arr[i];
    }
    //whitelist.reverse();
    return whitelist;
}

var getInvalidEmailList = function(){
    return invalidEmails;
}

var invalidEmails = [
    /calendar-notification/i,
    /feedproxy/i,
    /techgig/i,
    /team/i,
    /blog/i,
    /info@/i,
    /support/i,
    /admin/i,
    /hello/i,
    /no-reply/i,
    /noreply/i,
    /reply/i,
    /help/i,
    /mailer-daemon/i,
    /googlemail.com/i,
    /mail-noreply/i,
    /alert/i,
    /calendar-notification/i,
    /eBay/i,
    /flipkartletters/i,
    /pinterest/i,
    /dobambam.com/i,
    /notify/i,
    /offers/i,
    /iicicibank/i,
    /indiatimes/i,
    /info@relatas.in/i,
    /facebookmail/i,
    /message/i,
    /facebookmail.com/i,
    /notification/i,
    /youcanreply/i,
    /jobs/i,
    /news/i,
    /linkedin/i,
    /list/i,
    /updates/i,
    /verify/i,
    /jira/i
];

function fetchCompanyFromEmail(email){
    var domain = email.substring(email.lastIndexOf("@") +1)
        , words = domain.split(".")
        , personalMailDomains = ["gmail", "yahoo", "hotmail", "outlook", "aol", "rediffmail", "live"]
        , removableTextList = ["com", "org", "edu", "in"]

    _.remove(words, function(word){
        return removableTextList.indexOf(word.trim()) > -1
    })
    words = _.sortBy(words, function(word){return -(word.length);})
    return (personalMailDomains.indexOf(words[0]) > -1) ? null : words[0]
}

function getInteractionObject(userId,user_twitter_id,user_twitter_name,firstName,lastName,publicProfileUrl,profilePicUrl,companyName,designation,location,mobileNumber,action,interactionType,subType,emailId,refId,source,title,description,interactionDate,endDate,trackId,trackInfo,emailContentId,googleAccountEmailId){

    var obj = {
        userId:checkRequired(userId) ? userId : null,
        user_twitter_id:checkRequired(user_twitter_id) ? user_twitter_id : null,
        user_twitter_name:checkRequired(user_twitter_name) ? user_twitter_name : null,
        firstName:checkRequired(firstName) ? firstName : null,
        lastName:checkRequired(lastName) ? lastName : null,
        publicProfileUrl:checkRequired(publicProfileUrl) ? publicProfileUrl : null,
        profilePicUrl:checkRequired(profilePicUrl) ? profilePicUrl : null,
        emailId:checkRequired(emailId) ? emailId : null,
        //companyName:checkRequired(companyName) ? companyName : null,
        companyName: checkRequired(emailId) ? fetchCompanyFromEmail(emailId) : null,
        designation:checkRequired(designation) ? designation : null,
        location:checkRequired(location) ? location : null,
        mobileNumber:checkRequired(mobileNumber) ? mobileNumber : null,
        action:action,
        interactionType:interactionType,
        subType:subType,
        refId:refId,
        source:checkRequired(source) ? source : null,
        title:checkRequired(title) ? title : null,
        description:checkRequired(description) ? description : null,
        interactionDate:checkRequired(interactionDate) ? new Date(interactionDate) : new Date(),
        endDate:checkRequired(endDate) ? new Date(endDate) : null,
        createdDate:new Date(),
        trackId:checkRequired(trackId) ? trackId : null,
        emailContentId:checkRequired(emailContentId) ? emailContentId : null,
        googleAccountEmailId:checkRequired(googleAccountEmailId) ? googleAccountEmailId : null
    };
    if(checkRequired(trackInfo)){
        obj.trackInfo = trackInfo;
    }
    return obj;
}

function checkRequired(data){
    if (data == '' || data == null || data == undefined) {
        return false;
    }
    else{
        return true;
    }
}


function addContactFromGoogleInteractions(userEmailId,contactObj,callback,collection){

    collection.update(
        {"emailId": userEmailId, 'contacts.personEmailId': {$ne: contactObj.personEmailId}},
        {
            $push: {
                contacts: contactObj
            }
        },function(err,result){

            console.log("Result of adding a contact - " + JSON.stringify(result))
            if(!err){
                console.log("Successfully added contact - " + contactObj.personEmailId + "to User -" + userEmailId)
            } else {
                console.log("There was an error while adding contact - " +contactObj.personEmailId + "to User - " + userEmailId)
            }
        });
}

function addInteractionsBatch(user,direct,other,referenceIds,collectionInteraction){

    direct = direct.concat(other);

    console.log("Before Adding");
    console.log(direct.length);

    if(direct.length > 0 && referenceIds.length > 0){
        addInteractionsBulk(user,direct,referenceIds,collectionInteraction,function(isSuccess){
            var match = {userId:common.castToObjectId(user._id.toString())};

            if(!isSuccess){
                console.log("Failed to add interactions bulk EMAIL ",user.emailId);
            }
            else console.log("Add interactions bulk EMAIL Success ",user.emailId);
        })
    }
}

function addInteractionsBulk(user,interactionsArr,refIdArr,collectionInteraction,callback){

    collectionInteraction.aggregate(
    {
        $match:{emailId:user.emailId}
    },
    {
        $unwind:"$interactions"
    },
    {
        $match:{"interactions.refId":{$in:refIdArr}}
    },
    {
        $group:{
            _id:null,
            refIdList:{$addToSet:"$interactions.refId"},
            titleList:{$addToSet:"$interactions.title"}
        }
    },
        function(err,aggRes){

            if(aggRes.length>0 && aggRes[0].titleList.length>0){
                var addInteractions = interactionsArr.filter(function(val) {
                    return aggRes[0].titleList.indexOf(val.title) == -1;
                });

                addAllInteractions(user.emailId,addInteractions,collectionInteraction,function(err,res){
                    console.log("Interactions have been added? ")
                    console.log(res)
                    console.log("Error - ", err)
                });
            } else {
                addAllInteractions(user.emailId,interactionsArr,collectionInteraction,function(err,res){
                    console.log("Else - Interactions have been added? ")
                    console.log(res)
                    console.log("Error - ", err)
                });
            }

        });
};

function addAllInteractions(emailId,addInteractions,collectionInteraction,callback){

    collectionInteraction.update({emailId:emailId},{$push:{interactions:{$each:addInteractions}}},function(error,result){
        if(error){
            console.log('Error in addAllInteractions(): interactionsManagement ',error);
            callback(error)
        }
        else{
            callback(error,result);
        }
    });

}