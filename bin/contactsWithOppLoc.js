// require('newrelic');
var mongoClient = require('mongodb').MongoClient;
var async = require('async')
var fs = require('fs');
var json2xls = require('json2xls');
var request = require('request');
var _ = require("lodash");

var oppsCollection;
var usersCollection;
var contactsCollection;
var companyCollection;

var startScript = function() {

    // mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_13Sep?authSource=Relatas_mask", function(error, client) {
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
        // mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            // var db = client.db('Relatas_13Sep');
            usersCollection = db.collection('user');
            contactsCollection = db.collection('contacts');
            companyCollection = db.collection('company');
            oppsCollection = db.collection('opportunities');
            var totalFound = 0,data = [];

            var runForSingleCompany = function (skip) {
                companyCollection.find({url:/relatas.relatas/}).skip(skip).limit(1).toArray(function (err, company) {
                    if(!err && company && company.length>0){
                        var companyId = company[0]._id;

                        getUserProfiles(companyId,function (err1,users) {
                            if(users && users[0]){
                                if(!err1 && users && users.length){
                                    async.eachSeries(users, function (user, next){
                                        updateOpp(user,function(opps){
                                            next();
                                        });
                                    }, function(err) {
                                        runForSingleCompany(skip+1);
                                    });
                                } else {
                                    runForSingleCompany(skip+1);
                                }
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---",totalFound);
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

function updateOpp(user,callback){

    oppsCollection.find({userId:user._id},{geoLocation:1,userId:1}).toArray(function (err, opps) {
        var bulk = contactsCollection.initializeUnorderedBulkOp();
        var bulk2 = usersCollection.initializeUnorderedBulkOp();

        if(opps && opps.length>0){

            var opsExists = false;
            _.each(opps,function (opportunity) {

                if(opportunity.geoLocation && opportunity.geoLocation.town){

                    var contacts = [opportunity.contactEmailId];

                    if(opportunity.partners && opportunity.partners.length>0){
                        contacts = contacts.concat(_.pluck(opportunity.partners,"emailId"))
                    }

                    if(opportunity.decisionMakers && opportunity.decisionMakers.length>0){
                        contacts = contacts.concat(_.pluck(opportunity.decisionMakers,"emailId"))
                    }

                    if(opportunity.influencers && opportunity.influencers.length>0){
                        contacts = contacts.concat(_.pluck(opportunity.influencers,"emailId"))
                    }

                    opsExists = true;

                    _.each(contacts,function (co) {

                        bulk2.find({ _id: user._id,
                            contacts:{$elemMatch: {
                                personEmailId: co,
                                location: {$in:[""," ",null]}
                            }
                        }}).
                        update({ $set: {
                                "contacts.$.location": opportunity.geoLocation.town,
                                "contacts.$.lat": opportunity.geoLocation.lat,
                                "contacts.$.lng": opportunity.geoLocation.lng
                            }}, { multi: true });

                        bulk.find({ownerEmailId:user.emailId,"personEmailId": co,location:{$in:[""," ",null]}})
                            .update({
                                $set:{
                                    "location":opportunity.geoLocation.town,
                                    "lat":opportunity.geoLocation.lat,
                                    "lng":opportunity.geoLocation.lng
                                }
                            }, { multi: true });
                    });
                }
            });

            if(opsExists){

                bulk2.execute(function (err,results) {
                    if(results){
                        console.log(JSON.stringify(results,null,1));
                    }
                    bulk.execute(function () {
                        callback()
                    })
                })
            } else {
                callback()
            }

        } else {
            callback()
        }

    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

startScript();
