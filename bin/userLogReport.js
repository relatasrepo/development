var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  , url = 'mongodb://localhost:27017/Relatas'
  // , url = 'mongodb://172.31.38.246:27017/Relatas'
  , database = null
    , ObjectId = require('mongodb').ObjectID

var interactionsArray = []
var moment = require('moment-timezone');
var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();


// MongoClient.connect(url, function(err, client){
MongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) {
    database = client.db('Relatas');
    fetchInteractions(0)
})  

var fetchInteractions = function(skip){

    database.collection("user").find({emailId:/relatas/},{_id:1,emailId:1}).skip(skip).limit(1).toArray(function(err, userProfile){

      if(userProfile[0] && userProfile[0]._id) {

        database.collection("userlog").aggregate([
          {
            $match:{
              emailId:userProfile[0].emailId,
              loginDate: {$gte:new Date(moment().subtract(1,"week"))}
            }
          },
          { "$project": {
            "emailId": 1,
            "week": { "$week": "$loginDate" }
          }},
          { "$group": {
            "_id": {week:"$week",ownerEmailId:"$emailId"},
            "total": { "$sum": 1 }
          }},
          {
            $project:{
              _id:"$_id.ownerEmailId",
              week:"$_id.week",
              loginCount:"$total"
            }
          }
        ],function (err,interactionsByUser) {
          // console.log(JSON.stringify(interactionsByUser,null,1));
          if(!err && interactionsByUser.length>0){
            interactionsArray.push(interactionsByUser)
            // database.collection("aaaaa").insert({emailId:userProfile[0].emailId,report:interactionsByUser}, function(error, result){

            // console.log(JSON.stringify(interactionsByUser,null,2))
            var collection = database.collection("tempAggregationResults")
            var batch = collection.initializeOrderedBulkOp()

            _.each(interactionsByUser, function(interaction){
                interaction._id = new ObjectId()
                interaction.emailId = userProfile[0].emailId
                console.log(interaction)
                batch.insert(interaction)
            })

              // batch.execute(function(err, data){
              //
              //   if(userProfile.length>0 && !err){
              //     fetchInteractions(skip+1)
              //   } else {
              //     console.log("Done--")
              //   }
              // })
            fetchInteractions(skip+1)
          } else {
            fetchInteractions(skip+1)
          }
        })

      } else {
        console.log(interactionsArray.length)
      }
    });
}


