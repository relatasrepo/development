// require('newrelic');
var mongoClient = require('mongodb').MongoClient;
var async = require('async')
var _ = require("lodash");

var oppsCollection;
var usersCollection;
var contactsCollection;
var companyCollection;

var startScript = function() {

    console.log("--2--");
    // mongoClient.connect("mongodb://reladmin:Hennur123@10.150.0.8:27017/Relatas_13Sep?authSource=Relatas_mask", function(error, client) {
    mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.157:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.22.243:27017/Relatas", function(error, client) { // This is 22by
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    //     mongoClient.connect("mongodb://localhost", function(error, client) {
            console.log("--1--");
        if (error)
            throw error;
        else {
            var db = client.db('Relatas');
            // var db = client.db('Relatas_13Sep');
            usersCollection = db.collection('user');
            contactsCollection = db.collection('contacts');
            companyCollection = db.collection('company');
            oppsCollection = db.collection('opportunities');
            var totalFound = 0,data = [];

            var runForSingleCompany = function (skip) {
                companyCollection.find({}).skip(skip).limit(1).toArray(function (err, company) {

                    console.log(err);

                    if(!err && company && company.length>0){
                        var companyId = company[0]._id;

                        getUserProfiles(companyId,function (err1,users) {
                            console.log(users.length);
                            if(users && users[0]){
                                if(!err1 && users && users.length){
                                    async.eachSeries(users, function (user, next){
                                        updateOpp(user,function(opps){
                                            next();
                                        });
                                    }, function(err) {
                                        runForSingleCompany(skip+1);
                                    });
                                } else {
                                    runForSingleCompany(skip+1);
                                }
                            } else {
                                runForSingleCompany(skip+1);
                            }
                        });
                    } else {
                        console.log("---All Done---",totalFound);
                    }
                });
            };

            runForSingleCompany(0);
        }
    });
}

function updateOpp(user,callback){

    console.log(user.emailId);

    usersCollection.find({_id:user._id},{contacts:1}).toArray(function (err, result) {
        var bulk = contactsCollection.initializeUnorderedBulkOp();

        if(result && result[0].contacts && result[0].contacts.length>0){
            console.log(result[0].contacts.length);

            var opsExists = false;
            _.each(result[0].contacts,function (co,index) {

                if(co.location){
                    console.log(index);
                    opsExists = true;
                    bulk.find({ownerId:user._id,"personEmailId": co.personEmailId})
                        .update({
                            $set:{
                                location:co.location,
                                lat:co.lat || null,
                                lng:co.lng || null
                            }
                        }, { multi: true });
                }
            });

            if(opsExists){
                console.log("opsExists",opsExists);
                bulk.execute(function (err,res) {
                    console.log(user.emailId,err);
                    callback()
                })
            } else {
                callback()
            }

        } else {
            callback()
        }

    });
}

function getUserProfiles(companyId,callback) {
    usersCollection.find({companyId: companyId}, {contacts: 0}).toArray(function (err, users) {
        callback(err,users)
    })
}

startScript();
