var MongoClient = require('mongodb').MongoClient
  , _ = require("lodash")
  // , url = 'mongodb://localhost:27017/JulyCopy'
  , url = 'mongodb://172.31.38.246:27017/Relatas'
  , database = null
    , ObjectId = require('mongodb').ObjectID

var interactionsArray = []

var appCredentials = require('../config/relatasConfiguration');

var appCredential = new appCredentials();
var authConfig = appCredential.getAuthCredentials();
var dbPasswords = appCredential.getDBPasswords();


MongoClient.connect(url, function(err, db){
    database = db
    fetchInteractions(0)
})  

var fetchInteractions = function(skip){

    database.collection("user").find({companyId:ObjectId("5819881af58659760546d3fa")},{_id:1,emailId:1}).skip(skip).limit(1).toArray(function(err, userProfile){
    // database.collection("user").find({emailId:"naveenpaul.markunda@gmail.com"},{_id:1,emailId:1}).skip(skip).limit(1).toArray(function(err, userProfile){

      if(userProfile[0] && userProfile[0]._id) {

        database.collection("meeting").find({senderEmailId:userProfile[0].emailId,"participants.1":{$exists:false}},{_id:1}).toArray(function(err,selfMeetings) {
        // database.collection("meeting").find({senderEmailId:"naveenpaul.markunda@gmail.com","participants.1":{$exists:false}},{_id:1}).toArray(function(err,selfMeetings) {

          var selfMeetingDocIds = [];
          if(selfMeetings.length>0){
            selfMeetingDocIds = selfMeetings.map(function (obj) {
              return ObjectId(obj._id.toString())
            })
          }

          database.collection("actionItems").find({documentRecordId:{$in:selfMeetingDocIds},itemType:"meeting",userEmailId:userProfile[0].emailId}).toArray(function (err,actionItems1) {

            database.collection("actionItems").find({documentRecordId:{$nin:selfMeetingDocIds},itemType:"meeting",userEmailId:userProfile[0].emailId}).toArray(function (err,actionItems2) {

              database.collection("actionItems").find({itemType:"meeting",userEmailId:userProfile[0].emailId}).toArray(function (err,actionItems) {
                console.log("Not Exists - ",actionItems2.length)
                console.log("All meetings - ",actionItems.length)
                console.log("Yes exists - ",actionItems1.length)
              })
            })
          })

          database.collection("actionItems").aggregate([
            {
              $match:{
                // userEmailId:"naveenpaul.markunda@gmail.com",
                userEmailId:userProfile[0].emailId,
                documentRecordId:{$nin:selfMeetingDocIds}
              }
            },
            { "$project": {
              "userEmailId": 1,"actionTakenSource":1,"actionTakenDate":1,"itemType":1,"documentRecordId":1,
              "week": { "$week": "$itemCreatedDate" }
            }},
            { "$group": {
              "_id": {week:"$week",ownerEmailId:"$userEmailId",actionTakenDate:"$actionTakenDate"},
              "total": { "$sum": 1 },
              interactionType:{$push:"$itemType"},
              actionTakenSource:{$push:"$actionTakenSource"}
            }},
            {
              $project:{
                _id:"$_id.ownerEmailId",
                week:"$_id.week",
                actionTakenDate:"$_id.actionTakenDate",
                actionItemsCount:"$total",
                interactionType:"$interactionType",actionTakenSource:"$actionTakenSource"
              }
            }
          ],function (err,interactionsByUser) {
            console.log("Skip - ",skip)
            if(!err && interactionsByUser.length>0){

              var collection = database.collection("tempAggregationResults")
              var batch = collection.initializeOrderedBulkOp()

              _.each(interactionsByUser, function(interaction){
                interaction._id = new ObjectId()
                interaction.emailId = userProfile[0].emailId
                // console.log(interaction)
                batch.insert(interaction)
              })

              console.log("--------->>",interactionsByUser.length)

              batch.execute(function(err, data){

                if(userProfile.length>0 && !err){
                  fetchInteractions(skip+1)
                } else {
                  console.log("Done--")
                  fetchInteractions(skip+1)
                }
              })
            } else {
              fetchInteractions(skip+1)
            }
          })// Action items end
        }); //Self meeting remove end
      } else {
        console.log(interactionsArray.length)
      }
    });
}


