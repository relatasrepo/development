var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var sumBy = require('lodash.sumby');
var uniqBy = require('lodash.uniqby');
var moment = require('moment-timezone');
var async = require('async');
var admin = require('../config/firebaseConfiguration');
var emailSender = require('../public/javascripts/emailSender.js');
var notificationManager = require('./notificationManager.js');
var notificationDaily = require('./notificationsDaily.js');

var mandrillTemplate = new emailSender();

var tasksCollection;
var usersCollection;
var companyCollection;
var notificationsCollections;
var dealsAtRiskCollection;
var opportunityCollection;
var documentsCollection;
var opportunitiesTargetCollection;
var oppCommits;
var losingTouch;
var meeting;

var generateReportForUser = function() {
    notificationManager.getDbInstance(function(db) {

        usersCollection = db.collection('user');
        tasksCollection = db.collection('tasks');
        companyCollection = db.collection('company');
        notificationsCollections = db.collection('notifications');
        dealsAtRiskCollection = db.collection('dealsAtRiskMeta');
        opportunityCollection = db.collection('opportunities');
        documentsCollection = db.collection('documents');
        oppCommits = db.collection('oppCommits');
        losingTouch = db.collection('losingTouch');
        meeting = db.collection('meeting');
        opportunitiesTargetCollection = db.collection('opportunitiesTarget');

        runForSingleCompany = function(skip) {
            companyCollection.find({}).skip(skip).limit(1).toArray(function(error1, company) {
                if(!error1 && company && company.length > 0) {
                    var companyId = company[0]._id;
                    var companyName = company[0].companyName;

                    console.log("Running for company:", companyName);
                    notificationManager.getUserProfiles(companyId, function(error2, users) {
                        if(!error2 && users[0]) {
                            notificationManager.userFiscalYear(company[0], users[0], function(fyMonth, fyRange, allQuarters) {
                                var commitCutOffDates = notificationManager.getCommitCutOffDates(company[0], users[0].timezone, allQuarters);
                                
                                async.eachSeries(users, function(user, next) {
                                    removeUserAbstractReport(company[0]._id, user.emailId, function() {
                                        abstractReportForUser(user, company[0], commitCutOffDates, fyRange, function() {
                                            next();
                                        })
                                    })
                                    
                                    
                                }, function(error3) {
                                    console.log('All user done for company',company[0].url);
                                    updateAbstractReportAllUsers(users, company[0], fyMonth, fyRange, allQuarters, function() {
                                        runForSingleCompany(skip+1);
                                        console.log('All team done for company',company[0].url);
                                    })
                                })
                            })
                        } else {
                            runForSingleCompany(skip+1)                            
                        }
                    })
                } else {
                    console.log("-------------------- **ALL DONE** ----------------------------------------");
                }
            })
        }
        runForSingleCompany(0);
    })
}

function updateAbstractReportAllUsers(users,company,fyMonth,fyRange,allQuarters,callback) {
    async.eachSeries(users, function (user, next){
        abstractReportForManager(user,company,fyRange,allQuarters,function(){
            next();
        });
    }, function(err) {
        callback()
    });
}

function abstractReportForUser(user, company, commitCutOffDates, fyRange, callback) {
    var from = moment().startOf('day');
    var to = moment(from).endOf('day');

    var monthYear = moment().format('MMM') + " " + moment().year();
    notificationManager.getTargets([user._id], monthYear, fyRange.start, fyRange.end, function(usersTargets) {
        var primaryCurrency = "USD",
        currenciesObj = {};

        company.currency.forEach(function (el) {
            currenciesObj[el.symbol] = el;
            if(el.isPrimary){
                primaryCurrency = el.symbol;
            }
        });

        var commitStage = "";
        company.opportunityStages.forEach(function(stage) {
            if(stage && stage.commitStage)
                commitStage = stage.name;
        })

        async.parallel([
            function(callback) {
                achievement(company._id, user._id, user.emailId, fyRange.start, fyRange.end, commitCutOffDates, primaryCurrency, currenciesObj, callback);
            },
            function(callback) {
                tasksForWeek(user.emailId, user.timezone, callback);
            },
            function(callback) {
                dealsAtRisk(user.emailId, primaryCurrency, currenciesObj, callback);
            },
            function(callback) {
                losingTouchContacts(user.emailId, user.timezone, callback);
            },
            function(callback) {
                meetingsForWeek(user._id, user.emailId, user.timezone, user.serviceLogin, callback);
            },
            function(callback) {
                oppClosingIn30Days(user._id, company._id, primaryCurrency, callback);
            },
            function(callback) {
                staleOpportunities(user.emailId, company._id, primaryCurrency, callback);
            },
            function(callback) {
                pastWeekOpps(company._id, user.emailId, primaryCurrency, currenciesObj, callback);
            }
        ], function(error, data) {
            if(data[0])
                data[0].commitStage = commitStage;

            storeDataForUser(data, company._id, user._id, user.emailId, 'user', function() {
                callback();
            })
        })
    });
    
}

function abstractReportForManager(user, company, fyRange, allQuarters, callback) {
    var primaryCurrency = "USD",
    currenciesObj = {};

    company.currency.forEach(function (el) {
        currenciesObj[el.symbol] = el;
        if(el.isPrimary){
            primaryCurrency = el.symbol;
        }
    });

    notificationManager.getAllReportees(user,function (allReportees,allEmailIds,allUserIds) {
        if(allEmailIds.length > 1) {
            async.parallel([
                function(callback) {
                    achievementTeam(company._id, allEmailIds, callback);
                },
                function(callback) {
                    dealsAtRiskTeam(company._id, allEmailIds, callback);
                },
                function(callback) {
                    oppClosingIn30DaysTeam(company._id, allEmailIds, callback);
                },
                function(callback) {
                    staleOpportunitiesTeam(company._id, allEmailIds, callback);
                },
                function(callback) {
                    pastWeekOppsTeam(company._id, allEmailIds, callback);
                }
            ], function(error, data) {
                sendEmailForManager(data, company._id, company.url, user._id, user.emailId, user.firstName, primaryCurrency, function() {
                    console.log("Sent email for manager:", user.emailId);
                    callback();
                })
            })
        }
        else {
            sendEmailForUser(company._id, user.emailId, user.firstName, primaryCurrency, company.url, function() {
                console.log("Sent email for user:", user.emailId);
                callback();
            })
        }
    })
}

function achievement(companyId, userId, userEmailId, fyStart, fyEnd, commitCutOffDates, primaryCurrency, currenciesObj, callback) {
    var monthYear = moment().month()+""+moment().year();

    getUserTarget(userId, monthYear, fyStart, fyEnd, commitCutOffDates, function(target) {

        var startDate = moment().startOf('month');
        var endDate = moment().endOf('month');

        getOppsForUserMeta(companyId, userEmailId, startDate, endDate, primaryCurrency, currenciesObj, function(oppMetaData) {

            oppCommits.findOne({userId:userId, monthYear:monthYear, commitFor:"month"}, function(error, commit) {
                if(!error && commit) {
                    var monthlyCommit = commit.month.userCommitAmount;
                    var totalCommitOpps = 0;
                    var commitOppsValue = 0;
                    var commitPipeline = 0;

                    totalCommitOpps = commit.opportunities.length;

                    _.each(commit.opportunities, function(op) {
                        op.amount = parseFloat(op.amount);
                        op.amountWithNgm = op.amount;

                        if(op.netGrossMargin || op.netGrossMargin == 0){
                            op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                        }

                        op.convertedAmt = op.amount;
                        op.convertedAmtWithNgm = op.amountWithNgm

                        if(op.currency && op.currency !== primaryCurrency){

                            if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                                op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                            }

                            if(op.netGrossMargin || op.netGrossMargin == 0){
                                op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                            }
                            op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2));
                        };

                        commitOppsValue += op.convertedAmtWithNgm;
                        if(new Date(op.closeDate).getTime() >= new Date(startDate).getTime() && new Date(op.closeDate).getTime() <= new Date(endDate).getTime()) {
                            commitPipeline += op.convertedAmtWithNgm;
                        }
                    });

                    callback(null, {
                        target: target,
                        won: oppMetaData.won,
                        lost: oppMetaData.lost,
                        commit: Number(monthlyCommit),
                        commitPipeline: commitPipeline,
                        totalCommitOpps: totalCommitOpps,
                        commitOppsValue: commitOppsValue
                    })
                } else {
                    callback(null, {
                        target: target,
                        won: oppMetaData.won,
                        lost: oppMetaData.lost,
                        commit: null,
                        commitPipeline: null,
                        totalCommitOpps: null,
                        commitOppsValue: null
                    })
                }
            })
        })
    })
}

function tasksForWeek(emailId, timezone, callback) {
    var timezone = timezone && timezone.name?timezone.name:"UTC";
    var fromDate = moment(new Date()).tz(timezone).startOf('day').toDate();
    var toDate = moment(fromDate).add(7, 'days').tz(timezone).endOf('day').toDate();

    var query = {
        $or:[{ assignedToEmailId:emailId },
            { createdByEmailId:emailId },
            { "participants.emailId":emailId }]
    };

    query.dueDate = {
        $gte: new Date(moment(fromDate)),
        $lte: new Date(moment(toDate))
    }

    query.status = {
        $ne: "complete"
    }

tasksCollection.find(query).toArray(function(err, tasks) {
        if(!err && tasks) {
            callback(null, {
                count: tasks.length,
                tasks: tasks
            })
        } else {
            callback(null, {
                count: 0,
                tasks: []
            })
        }
    })
}

function dealsAtRisk(emailId,primaryCurrency,currenciesObj, callback) {

    var from = moment().subtract(1, "days")
    from = moment(from).startOf('day')
    var to = moment(from).endOf('day')

    dealsAtRiskCollection.find({userEmailId:{$in:emailId},date:{$gte:new Date(from),$lte:new Date(to)}},{opportunityIds:0}).sort({date:-1}).limit(1).toArray(function (err,deals) {

        var totalDealValueAtRisk = 0;
        var dealsAtRiskCount = 0,
            dealsRiskAsOfDate = null;

        if(!err && deals && deals[0]){
            _.each(deals,function (de) {
                var eachUserVal = 0;
                dealsRiskAsOfDate = de.date;
                _.each(de.deals,function (el) {

                    el.amountWithNgm = el.amount;

                    if(el.netGrossMargin || el.netGrossMargin == 0){
                        el.amountWithNgm = (el.amount*el.netGrossMargin)/100
                    }

                    el.convertedAmt = el.amount;
                    el.convertedAmtWithNgm = el.amountWithNgm

                    if(el.currency && el.currency !== primaryCurrency){

                        if(currenciesObj[el.currency] && currenciesObj[el.currency].xr){
                            el.convertedAmt = el.amount/currenciesObj[el.currency].xr
                        }

                        if(el.netGrossMargin || el.netGrossMargin == 0){
                            el.convertedAmtWithNgm = (el.convertedAmt*el.netGrossMargin)/100
                        }

                        el.convertedAmt = parseFloat(parseFloat(el.convertedAmt).toFixed(2))

                    }

                    eachUserVal = eachUserVal+parseFloat(el.convertedAmtWithNgm);
                });

                de.totalDealValueAtRisk = eachUserVal;
                totalDealValueAtRisk = totalDealValueAtRisk+de.totalDealValueAtRisk
                dealsAtRiskCount = dealsAtRiskCount+de.count
            })
        }

        if(err) {
            callback(null,{
                count:0,
                amount:0,
                deals: []
            })
        } else {
            callback(err,{
                count:sumBy(deals,"count"),
                amount:parseFloat(totalDealValueAtRisk.toFixed(2)),
                deals: deals
            })
        }
    })
}

function dealsAtRiskTeam(companyId, userEmailIds, callback) {
    
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: {$in: userEmailIds},
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    var aggregation = [
        {
            $match: match
        },
        {
            $project:{
                dealsAtRisk:1
            }
        },
        {
            $group:{
                _id:null,
                amount:{ $sum: "$dealsAtRisk.amount" },
                count:{ $sum: "$dealsAtRisk.count" },
                deals: { $push: "$dealsAtRisk.deals"}
            }
        }
    ]

    notificationsCollections.aggregate(aggregation).toArray(function (err,deals) {
        if(err){
            console.log("---deals at risk team---")
            console.log(err)
        }

        if(!err && deals && deals.length>0){

            if(deals[0].amount){
                deals[0].amount = parseFloat(deals[0].amount.toFixed(2))
            }
            delete deals[0]._id;
            callback(err,deals[0])
        } else {
            callback(err,{
                "amount": 0,
                "count": 0,
                "deals": []
            })
        }
    })
}

function achievementTeam(companyId, userEmailIds, callback) {
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: {$in: userEmailIds},
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    var aggregation = [
        {
            $match: match
        },
        {
            $project:{
                achievement:1
            }
        },
        {
            $group:{
                _id:null,
                target:{ $sum: "$achievement.target" },
                won:{ $sum: "$achievement.won" },
                lost:{ $sum: "$achievement.lost" },
                commit:{ $sum: "$achievement.commit" },
                commitPipeline:{ $sum: "$achievement.commitPipeline" },
                totalCommitOpps:{ $sum: "$achievement.totalCommitOpps" },
                commitOppsValue:{ $sum: "$achievement.commitOppsValue" },
                commitStage: { $first: "$achievement.commitStage"}
            }
        }
    ]

    notificationsCollections.aggregate(aggregation).toArray(function (err,achievement) {
        if(err){
            console.log("---achievement team---")
            console.log(err)
        }

        if(achievement && achievement[0]){
            achievement = achievement[0];
            delete achievement._id;

            Object.keys(achievement).map(function(key, index) {
                if(!_.includes(['totalCommitOpps', 'commitStage'], key) && achievement[key])
                    achievement[key] = parseFloat(achievement[key].toFixed(2));
            });

            callback(null, achievement);
        } else {
            callback(null, {
                target: 0,
                won: 0,
                lost: 0,
                commit: 0,
                commitPipeline: 0,
                totalCommitOpps: 0,
                commitOppsValue: 0
            })
        }
    })
}

function oppClosingIn30DaysTeam(companyId, userEmailIds, callback) {
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: {$in: userEmailIds},
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    var aggregation = [
        {
            $match: match
        },
        {
            $project:{
                futureOpps:1
            }
        },
        {
            $group:{
                _id:null,
                amount:{ $sum: "$futureOpps.amount" },
                count:{ $sum: "$futureOpps.count" },
                opportunityIds: { $push: "$futureOpps.opportunityIds"}
            }
        }
    ]

    notificationsCollections.aggregate(aggregation).toArray(function (err,opps) {
        if(err){
            console.log("---opp closing in 30 days team---")
            console.log(err)
        }

        if(opps && opps[0]){
            opps = opps[0];
            delete opps._id;

            if(opps.amount) 
                opps.amount = parseFloat(opps.amount.toFixed(2));

            callback(null, opps);
        } else {
            callback(null, {
                count: 0,
                amount: 0,
                opportunityIds: []
            })
        }
    })
}

function staleOpportunitiesTeam(companyId, userEmailIds, callback) {
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: {$in: userEmailIds},
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    var aggregation = [
        {
            $match: match
        },
        {
            $project:{
                staleOpps:1
            }
        },
        {
            $group:{
                _id:null,
                amount:{ $sum: "$staleOpps.amount" },
                count:{ $sum: "$staleOpps.count" },
                opportunityIds: { $push: "$staleOpps.opportunityIds"}
            }
        }
    ]

    notificationsCollections.aggregate(aggregation).toArray(function (err,opps) {
        if(err){
            console.log("---stale opps team---")
            console.log(err)
        }

        if(opps && opps[0]){
            opps = opps[0];

            delete opps._id;
            opps.amount = opps.amount ? parseFloat(opps.amount.toFixed(2)) : opps.amount;

            callback(null, opps);
        } else {
            callback(null, {
                count: 0,
                amount: 0,
                opportunityIds: []
            })
        }
    })
}

function pastWeekOppsTeam(companyId, userEmailIds, callback) {
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: {$in: userEmailIds},
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    var aggregation = [
        {
            $match: match
        },
        {
            $project:{
                pastOpps:1
            }
        },
        {
            $group:{
                _id:null,
                amount:{ $sum: "$pastOpps.amount" },
                count:{ $sum: "$pastOpps.count" },
                wonCount:{ $sum: "$pastOpps.wonCount" },
                opportunityIds: { $push: "$pastOpps.opportunityIds"}
            }
        }
    ]

    notificationsCollections.aggregate(aggregation).toArray(function (err,opps) {
        if(err){
            console.log("---past opps team---")
            console.log(err)
        }

        if(opps && opps[0]){
            opps = opps[0];

            delete opps._id;
            opps.amount = opps.amount ? parseFloat(opps.amount.toFixed(2)) : opps.amount;
            
            callback(null, opps);
        } else {
            callback(null, {
                count: 0,
                amount: 0,
                opportunityIds: []
            })
        }
    })
}

function losingTouchContacts(userEmailId, timezone, callback) {
    var timezone = timezone && timezone.name?timezone.name:"UTC";
    var fromDate = moment(new Date()).tz(timezone).startOf('day').toDate();
    var toDate = moment(new Date()).tz(timezone).endOf('day').toDate();

    var query = {
        userEmailId: userEmailId,
        lastUpdatedDate: {
            $gte: new Date(moment(fromDate)),
            $lte: new Date(moment(toDate))
        }
    }
    losingTouch.find(query, {historicalData:0}).toArray(function(error, losingTouchContacts) {
        
        if(!losingTouchContacts[0] || error) {
            callback(null, {
                totalContacts: 0,
                contacts: []
            });

        } else {
            callback(error, {
                totalContacts: losingTouchContacts[0].contacts.length,
                contacts: losingTouchContacts[0].contacts
            });
        }
    })
}

function meetingsForWeek(userId, userEmailId, timezone, findOutlookOrGoogle, callback) {
    var timezone = timezone && timezone.name?timezone.name:"UTC";
    var dateStart = moment(new Date()).tz(timezone).startOf('day').toDate();
    var dateEnd = moment(dateStart).add(7, 'days').tz(timezone).endOf('day').toDate();

    dateStart = new Date(dateStart);
    dateEnd = new Date(dateEnd);

    var projection = {"recurrenceId":1,invitationId:1,scheduleTimeSlots:1,senderId:1,senderName:1,senderEmailId:1,senderPicUrl:1,isSenderPrepared:1,to:1,toList:1,suggested:1,suggestedBy:1,selfCalendar:1,actionItemSlot:1,actionItemSlotType:1,designation:1,companyName:1};

    if(findOutlookOrGoogle == 'outlook'){
        meeting.find({
            "isOfficeOutlookMeeting":true,
            "LIUoutlookEmailId":userEmailId,
            $or: [
                {
                    "to.receiverId":userId,
                    "to.canceled":{$ne: true},
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                },
                {
                    deleted: {$ne: true},
                    toList: {$elemMatch: {receiverId: userId, canceled: {$ne: true}}},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                },
                {
                    senderId: userId,
                    deleted: {$ne: true},
                    "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                }
            ]
        },
        projection).toArray(function(error, meetings) {
            if(error) {
                callback(null, {
                    count: 0,
                    meetings: []
                })
            } else {
                var upcomingMeetings = [];

                _.each(meetings,function (meeting) {
                    if(!meeting.actionItemSlotType){
                        upcomingMeetings.push(meeting)
                    }
                });

                callback(null, {
                    count: upcomingMeetings.length,
                    meetings: upcomingMeetings
                });
            }
        });

    } else {

        meeting.find({
                isGoogleMeeting:true,
                $or: [
                    {
                        "to.receiverId":userId,
                        "to.canceled":{$ne: true},
                        deleted: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                    },
                    {
                        deleted: {$ne: true},
                        toList: {$elemMatch: {receiverId: userId, canceled: {$ne: true}}},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}}
                    },
                    {
                        senderId: userId,
                        deleted: {$ne: true},
                        "scheduleTimeSlots": {"$elemMatch": {"start.date": {'$gte': dateStart, '$lt': dateEnd}}},
                    }
                ]
            },
            projection).toArray(function(error, meetings) {
                if(error) {
                    callback(null, {
                        count: 0,
                        meetings: []
                    });
                } else {
                    var upcomingMeetings = [];
    
                    _.each(meetings,function (meeting) {
                        if(!meeting.actionItemSlotType){
                            upcomingMeetings.push(meeting)
                        }
                    });
    
                    callback(null, {
                        count: upcomingMeetings.length,
                        meetings: upcomingMeetings
                    });
                }
            });
    }
}

function getUserTarget(userId, monthYear, fyStart, fyEnd, commitCutOffDates, callback) {
    monthYear = moment(commitCutOffDates.monthlyCutOffDate).format('MMM') + " " + moment().year();
    
    var query = {
        userId:userId,
        "fy.start":{$gte:new Date(fyStart)},
        "fy.end":{$lte:new Date(fyEnd)}
    }

    opportunitiesTargetCollection.findOne(query, function(error, target) {
        
        if(!error && target) {
            var salesTarget = _.find(target.salesTarget, function(el) {
                if(el.monthYear == monthYear)
                    return el;
            });

            callback(salesTarget.target);
        } else {
            callback(null);
        }
        
    })
}

function getMonthlyCommit(userId, callback) {
    var monthYear = moment().month()+""+moment().year();

    var query = {
        userId: userId,
        monthYear: monthYear,
        commitFor: 'month'
    }

    oppCommits.findOne(query, {opportunities:0, week:0, quarter:0, opportunities:0}, function(err, commit) {

        if(err || !commit) {
            callback(null, {
                userCommitAmount: 0,
            })

        } else {
            callback(err, {
                userCommitAmount: commit.month ? commit.month.userCommitAmount : 0
            })
        }
    })
}

function oppClosingIn30Days(uId, companyId, primaryCurrency, callback) {
    var match = {
        userId:uId,
        companyId:companyId
    };

    match["closeDate"] = {$lte:new Date(moment().add(30, "days")),$gte:new Date(moment().startOf('day'))};
    match["stageName"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    match["isClosed"] = {$ne:true}
    match["relatasStage"] = {$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}

    var projectQuery = {
        "_id": "$_id",
        "opportunityId": "$opportunityId",
        "netGrossMargin": "$netGrossMargin",
        "amount": "$amount",
        "amount": netGrossAmount()
    };

    opportunityCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group:{
                _id:null,
                amount:{ $sum: "$amount" },
                count:{ $sum: 1 },
                opportunityIds: {$push: "$opportunityId"}
            }
        }
    ]).toArray(function(error, opps) {
        if(!error && opps[0]) {
            callback(null, {
                closingInDays: 30,
                count: opps[0].count,
                amount: parseFloat(opps[0].amount.toFixed(2)),
                opportunityIds: opps[0].opportunityIds
            });

        } else {
            callback(null, {
                closingInDays: 30,
                count: 0,
                amount: 0,
                opportunityIds: []
            });
        }
    })

}

function staleOpportunities(userEmailId, companyId, primaryCurrency, callback) {
    var minDate = moment("Jan 01 2015", "MMM DD YYYY").startOf('day').toDate();
    var maxDate = moment().subtract(1, 'days').endOf('day').toDate();

    var match = {
        companyId:companyId,
        userEmailId:userEmailId,
        closeDate:{$gte:minDate, $lte:maxDate},
        isClosed:{$ne:true},
        stageName:{$nin:['Close Won','Close Lost','Closed Won','Closed Lost']}
    }

    var projectQuery = {
        "_id": "$_id",
        "opportunityId": "$opportunityId",
        "netGrossMargin": "$netGrossMargin",
        "amount": "$amount",
        "amount": netGrossAmount()
    };

    opportunityCollection.aggregate([
        {
            $match:match
        },
        {
            $project:projectQuery
        },
        {
            $group:{
                _id:null,
                amount:{ $sum: "$amount" },
                count:{ $sum: 1 },
                opportunityIds: {$push: "$opportunityId"}
            }
        }
    ]).toArray(function(error, opps){
        if(!error && opps[0]) {
            callback(null, {
                count: opps[0].count,
                amount: parseFloat(opps[0].amount.toFixed(2)),
                opportunityIds: opps[0].opportunityIds
            });

        } else {
            callback(null, {
                count: 0,
                amount: 0,
                opportunityIds: []
            });
        }
    });
}

function pastWeekOpps(companyId, userEmailId, primaryCurrency, currenciesObj, callback) {

    var minDate = new Date(moment().subtract(7, "days").startOf('day'));
    var maxDate = new Date(moment().subtract(1, "days").endOf('day'));
    var returnObj = {
        count: 0,
        wonCount: 0,
        lostCount: 0,
        amount: 0,
        wonAmount: 0,
        lostAmount: 0,
        opportunityIds: []
    }

    var match = {
        $or:[{companyId:companyId},{companyId:companyId.toString()}],
        userEmailId:userEmailId,
        createdDate:{$gte:minDate, $lte:maxDate},
    }

    opportunityCollection.find(match).toArray(function(error, opps){
        if(error) {
            console.log("------- Past Week Opps Error -------");
            console.log(error);
            callback(null, returnObj);

        } else if(opps && opps.length > 0) {
            returnObj.count = opps.length;

            _.each(opps, function(op) {
                returnObj.opportunityIds.push(op.opportunityId);

                op.amount = parseFloat(op.amount);
                op.amountWithNgm = op.amount;
    
                if(op.netGrossMargin || op.netGrossMargin == 0){
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                }
    
                op.convertedAmt = op.amount;
                op.convertedAmtWithNgm = op.amountWithNgm
    
                if(op.currency && op.currency !== primaryCurrency){
    
                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                    }
    
                    if(op.netGrossMargin || op.netGrossMargin == 0){
                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                    }
                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2));
                };
    
                returnObj.amount += op.convertedAmtWithNgm;
    
                if(_.includes(['Closed Won', 'Close Won'], op.stageName) || _.includes(['Close Won', 'Closed Won'], op.relatasStage)) {
                    returnObj.wonAmount += op.convertedAmtWithNgm;
                    returnObj.wonCount += 1;
    
                } else if(_.includes(['Closed Lost', 'Close Lost'], op.stageName) || _.includes(['Close Lost', 'Closed Lost'], op.relatasStage)) {
                    returnObj.lostAmount += op.convertedAmtWithNgm;
                    returnObj.lostCount += 1
                }
            });

            callback(null, returnObj);
        } else {
            callback(null, returnObj);
        }
    });
}

function getOppsForUserMeta(companyId, emailId, fromDate, toDate, primaryCurrency, currenciesObj, callback) {
    fromDate = new Date(fromDate);
    toDate = new Date(toDate);
    
    var oppMetaData = {
        won: 0,
        lost: 0,
        pipeline: 0,
        total: 0
    }

    var match = {
        userEmailId:emailId,
        $or:[{companyId:companyId},{companyId:companyId.toString()}],
        closeDate: {
            $lte: toDate,
            $gte: fromDate
        }
    };

    opportunityCollection.find(match).toArray(function(error, opps) {
        if(!error && opps) {
            _.each(opps, function(op) {
                op.amount = parseFloat(op.amount);
                op.amountWithNgm = op.amount;

                if(op.netGrossMargin || op.netGrossMargin == 0){
                    op.amountWithNgm = (op.amount*op.netGrossMargin)/100
                }

                op.convertedAmt = op.amount;
                op.convertedAmtWithNgm = op.amountWithNgm

                if(op.currency && op.currency !== primaryCurrency){

                    if(currenciesObj[op.currency] && currenciesObj[op.currency].xr){
                        op.convertedAmt = op.amount/currenciesObj[op.currency].xr
                    }

                    if(op.netGrossMargin || op.netGrossMargin == 0){
                        op.convertedAmtWithNgm = (op.convertedAmt*op.netGrossMargin)/100
                    }
                    op.convertedAmt = parseFloat(op.convertedAmt.toFixed(2));
                };

                if(_.includes(['Closed Won', 'Close Won'], op.stageName) || _.includes(['Close Won', 'Closed Won'], op.relatasStage)) {
                    oppMetaData.won += op.convertedAmtWithNgm;

                } else if(_.includes(['Closed Lost', 'Close Lost'], op.stageName) || _.includes(['Close Lost', 'Closed Lost'], op.relatasStage)) {
                    oppMetaData.lost += op.convertedAmtWithNgm
                } else {
                    oppMetaData.pipeline += op.convertedAmtWithNgm;
                }
            });

            oppMetaData.total = opps.length;
            callback(oppMetaData);

        } else {
            oppMetaData.won = oppMetaData.lost = oppMetaData.pipeline = oppMetaData.total = null;
            callback(oppMetaData);
        }
    }) 
}

function netGrossAmount() {

    return {
        $cond: [ { $ne: [ "$netGrossMargin", 0] } ,
            {$multiply:[{ "$divide": [ "$netGrossMargin", 100 ] },"$amount"]},
            "$amount" ] }
}

function sendEmailForUser(companyId, userEmailId, userFn, primaryCurrency, companyUrl, callback) {
    
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: userEmailId,
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    userFn = userFn || "";
    var userStats = {
        "userInfo": {
            emailId: userEmailId,
            firstName: userFn
        }
    };

    notificationsCollections.findOne(match, function(error, userReport) {
        if(!error && userReport) {

            userStats.I_Target = userReport.achievement.target ? userReport.achievement.target : 0;
            userStats.I_Won = userReport.achievement.won ? parseFloat(userReport.achievement.won.toFixed(2)) : 0;
            userStats.I_Commit = userReport.achievement.commit ? parseFloat(userReport.achievement.commit.toFixed(2)) : 0;
            userStats.I_CommitPipeline = userReport.achievement.commitPipeline ? parseFloat(userReport.achievement.commitPipeline.toFixed(2)) : 0;
        
            userStats.I_Won_percent = userStats.I_Target ? (userStats.I_Won*100/userStats.I_Target) : "-"
            userStats.I_Commit_percent = userStats.I_Target ? (userStats.I_Commit*100/userStats.I_Target) : "-"
            userStats.I_CommitPipeline_percent = userStats.I_Target ? (userStats.I_CommitPipeline*100/userStats.I_Target) : "-"
        
            userStats.I_RiskDealsCount = userReport.dealsAtRisk.count;
            userStats.I_RiskDealsValue = userReport.dealsAtRisk.amount ? parseFloat(userReport.dealsAtRisk.amount.toFixed(2)) : 0;
            userStats.I_30DaysCount = userReport.futureOpps.count;
            userStats.I_30DaysValue = userReport.futureOpps.amount ? parseFloat(userReport.futureOpps.amount.toFixed(2)) : 0; 
            userStats.I_StaleOppsCount = userReport.staleOpps.count;
            userStats.I_StaleOppsValue = userReport.staleOpps.amount ? parseFloat(userReport.staleOpps.amount.toFixed(2)) : 0;
            userStats.I_Tasks = userReport.tasks.count;
            userStats.I_Meetings = userReport.meetings.count;
            userStats.LosingTouchCount = userReport.losingTouchContacts.totalContacts;
        
            userStats.I_DealWonPastWeek = userReport.pastOpps.wonCount ? parseFloat(userReport.pastOpps.wonCount.toFixed(2)) : 0;
            userStats.I_CommitOppCount = userReport.achievement.totalCommitOpps ? userReport.achievement.totalCommitOpps : 0;
            userStats.I_CommitStage = userReport.achievement.commitStage;
            userStats.I_CommitOppValue = userReport.achievement.commitOppsValue ? parseFloat(userReport.achievement.commitOppsValue.toFixed(2)) : 0;
            userStats.I_LastWeekOppCount = userReport.pastOpps.count;
            userStats.I_OppClosingThisMonth = userReport.futureOpps.count;
            userStats.I_CommitOppValuePerc = userStats.I_Target ? (userStats.I_CommitOppValue*100/userStats.I_Target) : "-";
        
            userStats.LosingTouchValue = "-";
            userStats.startYourWeek = 'https://'+companyUrl + "/war/user?fromDate=" + moment().format("DD-MMM-YYYY");
        
            mandrillTemplate.sendWarUser(userStats, notificationManager.numberWithCommas, primaryCurrency, function() {
                callback();
            })
        } else {
            callback();
        }
    })

    
}

function sendEmailForManager(userInsightData, companyId, companyUrl, userId, userEmailId, userFn, primaryCurrency, callback) {
    
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: userEmailId,
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    userFn = userFn || "";
    var userStats = {
        "userInfo": {
            emailId: userEmailId,
            firstName: userFn
        }
    };

    notificationsCollections.findOne(match, function(error, userReport) {
        userStats.T_Target = userInsightData[0].target ? userInsightData[0].target : 0;
        userStats.T_Won = userInsightData[0].won ? parseFloat(userInsightData[0].won.toFixed(2)) : 0;
        userStats.T_Commit = userInsightData[0].commit ? parseFloat(userInsightData[0].commit.toFixed(2)) : 0;
        userStats.T_CommitPipeline = userInsightData[0].commitPipeline ? parseFloat(userInsightData[0].commitPipeline.toFixed(2)) : 0;

        userStats.T_Won_percent = userStats.T_Target ? (userStats.T_Won*100/userStats.T_Target) : "-"
        userStats.T_Commit_percent = userStats.T_Target ? (userStats.T_Commit*100/userStats.T_Target) : "-"
        userStats.T_CommitPipeline_percent = userStats.T_Target ? (userStats.T_CommitPipeline*100/userStats.T_Target) : "-"

        userStats.T_RiskDealsCount = userInsightData[1].count;
        userStats.T_RiskDealsValue = userInsightData[1].amount ? parseFloat(userInsightData[1].amount.toFixed(2)) : 0;
        userStats.T_30DaysCount = userInsightData[2].count;
        userStats.T_30DaysValue = userInsightData[2].amount ? parseFloat(userInsightData[2].amount.toFixed(2)) : 0;
        userStats.T_StaleOppsCount = userInsightData[3].count;
        userStats.T_StaleOppsValue = userInsightData[3].amount ? parseFloat(userInsightData[3].amount.toFixed(2)) : 0;

        if(!error && userReport) {
            userStats.I_Tasks = userReport.tasks.count;
            userStats.I_Meetings = userReport.meetings.count;
            userStats.LosingTouchCount = userReport.losingTouchContacts.totalContacts;
        } else {
            userStats.I_Tasks = 0;
            userStats.I_Meetings = 0;
            userStats.LosingTouchCount = 0;
        }

        userStats.T_DealWonPastWeek = userInsightData[4].wonCount ? parseFloat(userInsightData[4].wonCount.toFixed(2)) : 0;
        userStats.T_CommitOppCount = userInsightData[0].totalCommitOpps;
        userStats.T_CommitStage = userInsightData[0].commitStage;
        userStats.T_CommitOppValue = userInsightData[0].commitOppsValue ? parseFloat(userInsightData[0].commitOppsValue.toFixed(2)) : 0;
        userStats.T_LastWeekOppCount = userInsightData[4].count;
        userStats.T_OppClosingThisMonth = userInsightData[2].count;
        userStats.T_CommitOppValuePerc = userStats.T_Target ? (userStats.T_CommitOppValue*100/userStats.T_Target) : "-";

        userStats.LosingTouchValue = "-";
        userStats.startYourWeek = 'https://'+companyUrl + "/war/user?fromDate=" + moment().format("DD-MMM-YYYY");

        mandrillTemplate.sendWarManager(userStats, notificationManager.numberWithCommas, primaryCurrency, function() {
            storeDataForManager(userInsightData, userReport, companyId, userId, userEmailId, 'manager', function() {
                callback();
            })
        })
    })
}

function storeDataForUser(userInsightData, companyId, userId, userEmailId, abstractReportFor, callback) {
    
    var warObj = {
        emailId: userEmailId,
        userId: userId,
        companyId: companyId,
        dataReferenceType: "war",
        abstractReportFor: abstractReportFor,
        sentDate: new Date(),
    }

    if(userInsightData[0]) {
        warObj.achievement = userInsightData[0];
    }

    if(userInsightData[1]) {
        warObj.tasks = userInsightData[1];
    }

    if(userInsightData[2]) {
        warObj.dealsAtRisk = userInsightData[2]
    }

    if(userInsightData[3]) {
        warObj.losingTouchContacts = userInsightData[3]
    }

    if(userInsightData[4]) {
        warObj.meetings = userInsightData[4]
    }

    if(userInsightData[5]) {
        warObj.futureOpps = userInsightData[5]
    }

    if(userInsightData[6]) {
        warObj.staleOpps = userInsightData[6]
    }

    if(userInsightData[7]) {
        warObj.pastOpps = userInsightData[7]
    }

    notificationsCollections.insert(warObj, function(error, results) {
        callback();
    })
}

function storeDataForManager(userInsightData, userReport, companyId, userId, userEmailId, abstractReportFor, callback) {
    
    var warObj = {
        emailId: userEmailId,
        userId: userId,
        companyId: companyId,
        dataReferenceType: "war",
        abstractReportFor: abstractReportFor,
        sentDate: new Date(),
    }

    if(userInsightData[0]) {
        warObj.achievement = userInsightData[0];
    }

    if(userInsightData[1]) {
        warObj.dealsAtRisk = userInsightData[1]
    }

    if(userInsightData[2]) {
        warObj.futureOpps = userInsightData[2]
    }

    if(userInsightData[3]) {
        warObj.staleOpps = userInsightData[3]
    }

    if(userInsightData[4]) {
        warObj.pastOpps = userInsightData[4]
    }

    if(userReport) {
        warObj.tasks = userReport.tasks;
        warObj.losingTouchContacts = userReport.losingTouchContacts;
        warObj.meetings = userReport.meetings;
    }

    notificationsCollections.insert(warObj, function(error, results) {
        callback();
    })
}

function removeUserAbstractReport(companyId, userEmailId, callback) {
    var start = new Date(moment().startOf('day'));
    var end = new Date(moment().endOf('day'));

    var match = {
        companyId: companyId,
        emailId: userEmailId,
        dataReferenceType: "war",
        abstractReportFor: "user",
        sentDate: {
            $gte: start,
            $lte: end
        }
    }

    notificationsCollections.remove(match, function(error, result) {
        callback();
    })
}

generateReportForUser();