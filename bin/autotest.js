var gaze = require('gaze')
  , exec = require('child_process').exec
  , isThere = require('is-there')
  , canExecuteLocally = process.argv[2] == "l" ? true : false 
  , chalk = require('chalk')

function executeTestCases(filepath) {
  if(isFilePartOfSysLink(filepath))
    return

  console.log(filepath + ' was changed');
  if(!canExecuteLocally)
    executeTestsGlobally()
  else{
    var filepathSplit = filepath.match(/(.*\/(relatas)\/)(.*)$/)
      , isTestFile = filepath.match(/.*\/test\/.*/) != null

    if(isTestFile)
      executeTestsLocally(filepath)
    else{
      if(filepathSplit == null)
        return
      var fileName =  filepathSplit[3].match(/\/?(?!.*\/)(.*)\./)[1]
        , testFilePath = filepathSplit[1] + "test/" + filepathSplit[3].replace(fileName, fileName + "-test")

      if(isThere(testFilePath))
        executeTestsLocally(testFilePath)
    }

  }
}

function executeTestsGlobally(){
  executeTests("npm test")
}

function executeTestsLocally(filepath){
  console.log(chalk.inverse.gray("Running tests at " + filepath))
  executeTests("mocha " + filepath)
}

function executeTests(command){
  exec(command, function (error, stdout, stderr) {
    console.log(stdout)
    console.log(stderr)
  })
}

function isFilePartOfSysLink(filepath){
  return filepath.match(/(.*)node_modules(.*)/) != null
}

gaze([__dirname + '/../**/*.*', __dirname + '/../*.*'], function(err, watcher) {
  this.on('changed', function(filepath) {
    executeTestCases(filepath)
  });
})
