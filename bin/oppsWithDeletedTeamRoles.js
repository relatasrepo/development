var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');

var OppMetaDataCollection;
var opportunitiesCollection;
var orgRolesCollection;

var rule = new schedule.RecurrenceRule();
//rule.dayOfWeek = [1,2,3,4,5,6,0];
rule.hour = 0;
rule.minute = 1;
rule.second = 0;
schedule.scheduleJob(rule, function() {
    console.log('oppMetaData is running');
    startScript();
});

var startScript = function() {

    // mongoClient.connect("mongodb://ozymandias:1pe07is0289972@172.31.33.42:27017/Relatas", function(error, client) { // This is LIVE
    // mongoClient.connect("mongodb://devRelatasAdmin:vcxz7890@10.150.0.2:27017/Relatas", function(error, client) { // This is Showcase
    mongoClient.connect("mongodb://localhost", function(error, client) {
        if (error)
            throw error;
        else {
            // var db = client.db('Relatas');
            var db = client.db('newDb');
            console.log("Connected to db");
            console.log('authenticated');
            var usersCollection = db.collection('user');
            opportunitiesCollection = db.collection('opportunities');
            orgRolesCollection = db.collection('orgRoles');

            var runForSingleUser = function (skip) {
                usersCollection.find({corporateUser: true,emailId:'sureshhoel@gmail.com'}).skip(skip).limit(1).toArray(function (err, user) {
                    if(!err && user && user.length>0){
                        var companyId = user[0].companyId;
                        var userEmailId = user[0].emailId;

                        console.log(userEmailId,skip);
                        updateData(companyId,userEmailId,function () {
                            runForSingleUser(skip+1);
                        })
                    } else {
                        console.log("Done");
                    }
                });
            };

            runForSingleUser(0);
        }
    });
}

startScript();

function updateData(companyId,userEmailId,callback){
    getRoles(companyId,function(err,roles){
        console.log("roles--",roles.length)
        console.log(roles);

        getOpps(companyId,userEmailId,roles,function(err,opps,deleteRoles){
            console.log(deleteRoles)
            callback()
        })
    });
}

function getRoles(companyId,callback){
    orgRolesCollection.find({companyId:companyId},{roleName:1}).toArray(function (err, data) {
        var roles = [];
        if(!err && data && data.length>0){
            roles = _.pluck(data,"roleName");
        }
        callback(err,roles)
    });
}

function getOpps(companyId,userEmailId,roles,callback){
    opportunitiesCollection.find({
        userEmailId:userEmailId,
        'usersWithAccess.accessGroup': { $nin: roles }})
        .toArray(function (err, opps) {
            var deleteRoles = [],
            deleteOppIds = [];
            if(!err && opps  && opps.length>0){
                _.each(opps,function (op) {
                    _.each(op.usersWithAccess,function (ac) {
                        if(ac.accessGroup !== 'Others'){
                            deleteRoles.push(ac.accessGroup);
                        }
                    })
                });
            }
        callback(err,opps,deleteRoles)
    });
}
